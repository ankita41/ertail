/*****
*   @Class      :   AccountHandler.cls
*   @Description:   XXX
*   @Author     :   Thibauld
*   @Created    :   23 JUL 2014
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*	Jacky Uy		26 AUG 2014		Added the PopulateBoutiqueGeoLocation() method
*	Jacky Uy		11 SEP 2014		Added a static variable to stop triggers to execute in a loop
*                         
*****/

public with sharing class AccountHandler {
	public static Boolean TriggerStopPopulateBoutiqueGeoLocation = false;
	
	public static void UpdateMarketNameBeforeInsert(List<Account> newList){
		// Copy the MarketName in the records
		Set<Id> marketIds = new Set<Id>();
		Set<Id> b2bcMarketIds = new Set<Id>();
		for (Account acc : newList){
			if (acc.Market__c != null && acc.MarketName__c == null){
				marketIds.add(acc.Market__c);
			}
			if (acc.B2BC_Market__c != null && acc.B2BC_MarketName__c == null){
				b2bcMarketIds.add(acc.B2BC_Market__c);
			}
		}
		if (marketIds.size() >0){
			Map<Id, Market__c> marketsMap = new Map<Id, Market__c>([Select Id, Name from Market__c where Id in :marketIds]);
			for (Account acc : newList){
				if (acc.Market__c != null){
					acc.MarketName__c = marketsMap.get(acc.Market__c).Name;
				}
			}
		}
		
		if (b2bcMarketIds.size() >0){
			Map<Id, B2BC_Market__c> marketsMap = new Map<Id, B2BC_Market__c>([Select Id, Name from B2BC_Market__c where Id in :b2bcMarketIds]);
			for (Account acc : newList){
				if (acc.B2BC_Market__c != null){
					acc.B2BC_MarketName__c = marketsMap.get(acc.B2BC_Market__c).Name;
				}
			}
		}
	}

	public static void UpdateMarketNameBeforeUpdate(Map<Id, Account> oldMap, Map<Id, Account> newMap){
		// Update the MarketName if the Market has changed
		List<Account> accToUpdate = new List<Account>();
		List<Account> accToUpdateB2bc = new List<Account>();
		for (Id accId : newMap.keySet()){
			if (newMap.get(accId).Market__c != oldMap.get(accId).Market__c)
				accToUpdate.add(newMap.get(accId));
			if (newMap.get(accId).B2BC_Market__c != oldMap.get(accId).B2BC_Market__c)
				accToUpdateB2bc.add(newMap.get(accId));
		}
		if (accToUpdate.size() > 0){
			Set<Id> marketIds = new Set<Id>();
			for (Account acc : accToUpdate){
				if (acc.Market__c != null)
					marketIds.add(acc.Market__c);
			}
			if (marketIds.size() > 0){
				Map<Id, Market__c> marketsMap = new Map<Id, Market__c>([Select Id, Name from Market__c where Id in :marketIds]);
				for (Account acc : accToUpdate){
					if (acc.Market__c != null){
						acc.MarketName__c = marketsMap.get(acc.Market__c).Name;
					}else {
						acc.MarketName__c = '';
					}
				}
			}
		}
		if (accToUpdateB2bc.size() > 0){
			Set<Id> marketIds = new Set<Id>();
			for (Account acc : accToUpdate){
				if (acc.B2BC_Market__c != null)
					marketIds.add(acc.B2BC_Market__c);
			}
			if (marketIds.size() > 0){
				Map<Id, B2BC_Market__c> marketsMap = new Map<Id, B2BC_Market__c>([Select Id, Name from B2BC_Market__c where Id in :marketIds]);
				for (Account acc : accToUpdate){
					if (acc.B2BC_Market__c != null){
						acc.B2BC_MarketName__c = marketsMap.get(acc.B2BC_Market__c).Name;
					}else {
						acc.B2BC_MarketName__c = '';
					}
				}
			}
		}
	}
	
	public static void UpdateMarketNameOnLinkedObjectsAfterUpdate(Map<Id, Account> oldMap, Map<Id, Account> newMap){
		Set<Id> updatedAccounts = new Set<Id>();
		Map<Id, Project__c> projToUpdateMap = new Map<Id, Project__c>();
		for (Id accId : newMap.keySet()){
			if (oldMap.get(accId).Market__c != newMap.get(accId).Market__c){
				updatedAccounts.add(accId);
			}
		}
		
		if (updatedAccounts.size() > 0){
			for(Project__c p : [Select Id, MarketName__c, Market__c from Project__c where Corner_Point_of_Sale_r__c in :updatedAccounts]){
				if (p.MarketName__c != p.Market__c){
					projToUpdateMap.put(p.Id, p);
				}
			}
			if (projToUpdateMap.size() > 0)
				CornerProjectHandler.UpdateMarketNameAfterUpdate(projToUpdateMap);
		}
	}
	
	@Future(callout=true)
	public static void PopulateBoutiqueGeoLocation(Set<Id> accIds){
		AccountHandler.TriggerStopPopulateBoutiqueGeoLocation = true;
		String address;
		Account tempAcc;
		List<Account> updAccs = new List<Account>();
		
		for(Account a : [
			SELECT BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Location__Latitude__s, Location__Longitude__s, Geocoding_Zero_Results__c
			FROM Account WHERE Id IN :accIds LIMIT 5
		]){
							
			address = '';
			if(a.BillingStreet != null)
				address += a.BillingStreet + ', ';
			if(a.BillingCity != null)
				address += a.BillingCity + ', ';
			if(a.BillingState != null)
				address += a.BillingState + ' ';
			if(a.BillingPostalCode != null)
				address += a.BillingPostalCode +', ';
			if(a.BillingCountry != null)
				address += a.BillingCountry;
			
			if(address!=''){
				a.Location__Latitude__s = 0;
		        a.Location__Longitude__s = 0;
		        a.Geocoding_Zero_Results__c = true;
				tempAcc = calloutGeoLocation(a, address);
	 			if(tempAcc.Geocoding_Zero_Results__c && a.BillingCountry!=NULL){
	 				tempAcc = calloutGeoLocation(a, a.BillingCountry);
	 				tempAcc.Geocoding_Zero_Results__c = true;
	 			}
	            updAccs.add(tempAcc);
			}
		}
		update updAccs;
	}
	
	private static Account calloutGeoLocation(Account a, String address){
		String clientID = GoogleMapsSettings__c.getValues('Client ID').Value__c;
		String privateKey = GoogleMapsSettings__c.getValues('Crypto Key').Value__c.replace('-', '+').replace('_', '/');
		Blob privateKeyBlob = EncodingUtil.base64Decode(privateKey);
		
		String url, signature, txt;
		Blob urlBlob, signatureBlob;
		Http h;
		HttpRequest req;
		HttpResponse res;
		Double lon, lat;
		
		address = EncodingUtil.urlEncode(address, 'UTF-8');
		url = '/maps/api/geocode/json?address=' + address + '&sensor=false&client=' + clientID;
		urlBlob = Blob.valueOf(url);
		signatureBlob = Crypto.generateMac('hmacSHA1', urlBlob, privateKeyBlob);
		signature = EncodingUtil.base64Encode(signatureBlob).replace('+', '-').replace('/', '_');
		
		//CALLOUT
        req = new HttpRequest();
        req.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false&client=' + clientID + '&signature=' + signature);
        req.setMethod('GET');
        req.setTimeout(60000);
        
        h = new Http();
        String resp='';
        if(!Test.isRunningTest()){
        	res = h.send(req);
        	resp=res.getBody();
        }
        else{
        	resp='{"results" : [{"address_components" : [{"long_name" : "122001","short_name" : "122001","types" : [ "postal_code" ]},{"long_name" : "Gurgaon","short_name" : "Gurgaon","types" : [ "locality", "political" ]},{"long_name" : "Gurgaon","short_name" : "Gurgaon","types" : [ "administrative_area_level_2", "political" ]},{"long_name" : "Haryana","short_name" : "HR","types" : [ "administrative_area_level_1", "political" ]},{"long_name" : "India","short_name" : "IN","types" : [ "country", "political" ]}],"formatted_address" : "Gurgaon, Haryana 122001, India","geometry" : {"bounds" : {"northeast" : {"lat" : 28.4890902,"lng" : 77.0662683},"southwest" : {"lat" : 28.4014119,"lng" : 76.9598127}},"location" : {"lat" : 28.4554726,"lng" : 77.0219019},"location_type" : "APPROXIMATE","viewport" : {"northeast" : {"lat" : 28.4890902,"lng" : 77.0662683},"southwest" : {"lat" : 28.4014119,"lng" : 76.9598127}}},"partial_match" : true,"place_id" : "ChIJm523QqkZDTkR06eT1PrbRxM","types" : [ "postal_code" ]}],"status" : "OK"}';
        }
        
        //PARSE RESPONSE
        JSONParser parser = JSON.createParser(resp);
        lat = null;
        lon = null;
		while(parser.nextToken()!=null){
			if((parser.getCurrentToken()==JSONToken.FIELD_NAME) && (parser.getText()=='location')){
				parser.nextToken();
				while(parser.nextToken()!=JSONToken.END_OBJECT){
					txt = parser.getText();
					parser.nextToken();
					if(txt=='lat')
					    lat = parser.getDoubleValue();
					else if(txt=='lng')
					    lon = parser.getDoubleValue();
				}
			} 
			/*else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'status')){
				parser.nextToken();
				txt = parser.getText();
				if(txt == 'ZERO_RESULTS')
					a.Geocoding_Zero_Results__c = true;
			}*/
		}
		
        //UPDATE COORDINATES
        if(lat!=null && lon!=null){
            a.Location__Latitude__s = lat;
            a.Location__Longitude__s = lon;
            a.Geocoding_Zero_Results__c = false;
        }
        
        return a;
	}
}
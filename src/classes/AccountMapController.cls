public with sharing class AccountMapController {
	public Account acc {get; set;}
	public String address {get; set;}
	
	public AccountMapController (ApexPages.StandardController stdController) {
        acc = (Account)stdController.getRecord();        
        acc = [SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Location__Latitude__s, Location__Longitude__s, Geocoding_Zero_Results__c FROM Account WHERE Id = :acc.Id];
        
        address = (acc.BillingStreet != null ? (acc.BillingStreet + '<br/>') : '') + 
	            		(acc.BillingCity != null ? (acc.BillingCity + ', ') : '') + 
	            		(acc.BillingPostalCode != null ? (acc.BillingPostalCode + '') : '') + '<br/>' + 
	            		(acc.BillingCountry != null ? acc.BillingCountry : '');
	    
    }
    
    public PageReference getCoordinates() {
    	if (acc.Location__Latitude__s == null) {
	    	String tmpAddress = '';
		        if (acc.BillingStreet != null)
		            tmpAddress += acc.BillingStreet +', ';
		        if (acc.BillingCity != null)
		            tmpAddress += acc.BillingCity +', ';
		        if (acc.BillingState != null)
		            tmpAddress += acc.BillingState +' ';
		        if (acc.BillingPostalCode != null)
		            tmpAddress += acc.BillingPostalCode +', ';
		        if (acc.BillingCountry != null)
		            tmpAddress += acc.BillingCountry;
		    	
	    	tmpAddress = EncodingUtil.urlEncode(tmpAddress, 'UTF-8');
	        
	        String clientID = GoogleMapsSettings__c.getValues('Client ID').Value__c;
	    	String privateKey = GoogleMapsSettings__c.getValues('Crypto Key').Value__c;
	        
	        // get digital signature
	        String url = '/maps/api/geocode/json?address=' + tmpAddress + '&sensor=false&client=' + clientID;
			
			privateKey = privateKey.replace('-', '+');
			privateKey = privateKey.replace('_', '/');
			
			Blob privateKeyBlob = EncodingUtil.base64Decode(privateKey);
			Blob urlBlob = Blob.valueOf(url);
			Blob signatureBlob = Crypto.generateMac('hmacSHA1', urlBlob, privateKeyBlob);
			
			//String signature = EncodingUtil.urlEncode(EncodingUtil.base64Encode(signatureBlob), 'UTF-8');
			String signature = EncodingUtil.base64Encode(signatureBlob);
			signature = signature.replace('+', '-');
			signature = signature.replace('/', '_');
			
			system.debug('signature is ' + signature);
	 		
	        // build callout
	        Http h = new Http();
	        HttpRequest req = new HttpRequest();
	        req.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address=' + tmpAddress + '&sensor=false&client=' + clientID + '&signature=' + signature);
	        req.setMethod('GET');
	        req.setTimeout(60000);
	 		
	        try{
	            HttpResponse res = h.send(req);
	            system.debug(res.getBody());
	 
	            // parse coordinates from response
	            JSONParser parser = JSON.createParser(res.getBody());
	            double lat = null;
	            double lon = null;
	            while (parser.nextToken() != null) {
	                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'location')) {
	                   parser.nextToken(); // object start
	                   while (parser.nextToken() != JSONToken.END_OBJECT) {
	                        String txt = parser.getText();
	                        parser.nextToken();
	                        if (txt == 'lat') {
	                            lat = parser.getDoubleValue();
	                        } else if (txt == 'lng') {
	                            lon = parser.getDoubleValue();
	                        }
	                    }
	                } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'status')) {
	                	parser.nextToken();
	                	String txt = parser.getText();
	                	if (txt == 'ZERO_RESULTS') {
	                		acc.Geocoding_Zero_Results__c = true;
	                	}
	                }
	            }
	 
	            // update coordinates if we get back
	            if (lat != null){
	                acc.Location__Latitude__s = lat;
	                acc.Location__Longitude__s = lon;
	                acc.Geocoding_Zero_Results__c = false;
	            }
	            update acc;
	            
	        } catch (Exception e) {
	        }
    	}
    	
    	return null;
    }
}
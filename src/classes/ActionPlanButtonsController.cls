/*****
*   @Class      :   ActionPlanButtonsController.cls
*   @Description:   Controller for the ActionPlanButtons Visual Force page.
*   @Author     :   XXXXXXXX
*   @Created    :   XXXXXXXX
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Thibauld        29 APR 2014     Add: Send Decision
*/
public with sharing class ActionPlanButtonsController {
    public Handover_Action_Plan__c actPlan {get; set;}
    public String currentUserProfile;
    
    public String sendAgentDecisionMessage{get; set;}
    public String sendNESDecisionMessage{get; set;}
    public String actionPlanSubmittedMessage{get; set;}
    
    private String workflowType;
    
    private Boolean IsProjectOnHold{
  		get{
	  		if (IsProjectOnHold == null){
	  			IsProjectOnHold = (actPlan.Handover_Report__r.Project__r.Status__c == CornerProjectButtonsController.ONHOLDSTATUS);
	  		}
	  		return IsProjectOnHold;
  		}
  		set;
  	}
    
    public Boolean showSendActionPlan {
        get {
            String hasAccessRights = 'NES Corner Supplier,System Administrator,System Admin';
            return (!IsProjectOnHold && actPlan.Action_plan_sent_date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    public Boolean showSendDecision {
        get {
            String hasAccessRights = 'NES Market Local Manager,System Administrator,System Admin';
            return (!IsProjectOnHold && workflowType == 'Sales Promoter' && actPlan.NES_Dis_or_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    
    public Boolean showSendDecision_Agent {
        get {
            String hasAccessRights = 'Agent Local Manager,System Administrator,System Admin';
            return (!IsProjectOnHold && workflowType == 'Agent' && actPlan.Agent_Dis_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    
    public Boolean allowSendActionPlan { 
        get{
            return true;
        } set;
    }
    
    public Boolean allowSendDecision { 
        get{
            return (actPlan.NES_Decision__c != null);           
        } set;
    }   
    
    public Boolean allowSendDecision_Agent { 
        get{
            return (actPlan.Agent_Decision__c != null);         
        } set;
    }   
    
    public ActionPlanButtonsController(ApexPages.StandardController controller) {
        currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        
        actPlan = (Handover_Action_Plan__c)controller.getRecord();
        actPlan = [SELECT Id, Send_action_plan__c, Action_plan_sent_date__c, Handover_Report__c, Handover_Report__r.Workflow_Type__c, NES_Decision__c, NES_Dis_or_Approval_date__c   
                        , Agent_Dis_Approval_date__c, Agent_Dis_Approved__c, Agent_Decision__c, Handover_Report__r.Project__r.Status__c
                    FROM Handover_Action_Plan__c WHERE Id = :actPlan.Id];
        workflowType = actPlan.Handover_Report__r.Workflow_Type__c;
        
        InitMessages();
    }
    
    private void InitMessages()
    {
        if (actPlan.Agent_Decision__c == 'Approved')
            sendAgentDecisionMessage = 'Thank you, the Action Plan has been sent to the HQ and NES Project Manager for information.';
        else if (actPlan.Agent_Decision__c == 'Disapproved')
            sendAgentDecisionMessage = 'Your decision has been sent to the supplier.';
                        
        if (actPlan.NES_Decision__c == 'Approved')
            sendNESDecisionMessage = 'Thank you, the Action Plan has been sent to the HQ for information.';
        else if (actPlan.NES_Decision__c == 'Disapproved')
            sendNESDecisionMessage = 'Your decision has been sent to the supplier.';
        
        if (workflowType == 'Agent')
            actionPlanSubmittedMessage = 'Thank you, your Action Plan will be sent to the Agent.';
        else
            actionPlanSubmittedMessage = 'Thank you, your Action Plan will be sent to the NES Project Manager.';
    }
    
    public PageReference SendActionPlan () {
        actPlan.Send_action_plan__c = true;
        update actPlan;
        
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + actPlan.Id);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
    
    public PageReference SendDecision () {
        actPlan.NES_Dis_Approved__c = true;
        update actPlan;
        
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + actPlan.Id);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
        
    public PageReference SendDecision_Agent() {
        actPlan.Agent_Dis_Approved__c = true;
        update actPlan;
        
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + actPlan.Id);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
    
    public PageReference BackToHandoverReport () {
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + actPlan.Handover_Report__c);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
}
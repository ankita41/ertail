/*****
*   @Class      :   ActionPlanHandler.cls
*   @Description:   Handler for Action Plan.
*   @Author     :   Thibauld
*   @Created    :   21 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                      
***/
public with sharing class ActionPlanHandler{
    public static void CopyFromHandoverReportToActionPlan(List<Handover_Action_Plan__c> newList){
        Set<Id> handoverIds = new Set<Id>();
        for(Handover_Action_Plan__c ap : newList) {
            handoverIds.add(ap.Handover_Report__c);
        }
    
        Map<Id, Handover_Report__c> mapHandoverReportsPerActionPlanId = new Map<Id, Handover_Report__c>([SELECT Id, Nespresso_Contact__c, Project_Manager_HQ__c, NES_Sales_Promoter__c, NES_Business_Development_Manager__c, Agent_Local_Manager__c 
                                                  FROM Handover_Report__c WHERE Id IN :handoverIds]);
      
        for (Handover_Action_Plan__c ap : newList) {
            if (mapHandoverReportsPerActionPlanId.get(ap.Handover_Report__c) != null) {
                ap.Nespresso_Contact__c = mapHandoverReportsPerActionPlanId.get(ap.Handover_Report__c).Nespresso_Contact__c;
                ap.NES_Sales_Promoter__c = mapHandoverReportsPerActionPlanId.get(ap.Handover_Report__c).NES_Sales_Promoter__c;
                ap.NES_Business_Development_Manager__c = mapHandoverReportsPerActionPlanId.get(ap.Handover_Report__c).NES_Business_Development_Manager__c;
                ap.Agent_Local_Manager__c = mapHandoverReportsPerActionPlanId.get(ap.Handover_Report__c).Agent_Local_Manager__c;
                ap.Project_Manager_HQ__c = mapHandoverReportsPerActionPlanId.get(ap.Handover_Report__c).Project_Manager_HQ__c;
            }
        }
    
    }

}
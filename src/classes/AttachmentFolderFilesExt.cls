/*****
*   @Class      :   AttachmentFolderFilesExt.cls
*   @Description:   Controller class of the AttachmentFolderFiles VF page
*   @Author     :   Jacky Uy
*   @Created    :   25 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy        15 SEP 2014     Checked parent object to set file size limit accordingly.
*   Jacky Uy        24 SEP 2014     Created the shareFileToStakeholders() method to directly share created Files.
*   Jacky Uy        25 SEP 2014     Updated the shareFileToStakeholders() method to share only to PO Operator and Support PO Operator.
*   PRIYA       23 FEB 2016     Code added for Req # 117 (R2)                      
*****/

public class AttachmentFolderFilesExt {
  
    public final Attachment_Folder__c mainFolder {get;set;}
    public Attachment_Subfolder__c newSubfolder {get;set;}
    public List<FolderWrapper> subfolders {get;set;}
    public FeedItem newFeedContent {get;set;}
    public List<FeedItem> files {get;set;}
    public Id selectedFolder {get;set;}
    public Id transferToFolderId {get;set;}
    public List<SelectOption> folderOptions {get;set;}
    public Map<Id,Boolean> fileCheckBoxMap {get;set;}
    
    public String maxFileSize {get;set;}
    private final Integer MAX_FILE_SIZE {get;set;}
    public boolean IEtrue{get;set;}
    public boolean IEfalse{get;set;}
    public String userAgent{get;set;}
    
    public AttachmentFolderFilesExt(ApexPages.standardController con){
        IEtrue=false;
        IEfalse=false;
        mainFolder = [SELECT Name, Offer__c, Offer__r.RECO_Project__r.PO_operator__c, Offer__r.RECO_Project__r.PO_operator__r.Name, Offer__r.RECO_Project__r.Support_PO_Operator__c, Offer__r.RECO_Project__r.Support_PO_Operator__r.Name FROM Attachment_Folder__c WHERE Id = :con.getRecord().Id];
        
        if(mainFolder.Offer__c!=NULL){
            maxFileSize = '5MB';
            MAX_FILE_SIZE = 5242880;
        }
        else{
            maxFileSize = '10MB';
            MAX_FILE_SIZE = 10485760;
        }
         String selectFolderURL=ApexPages.currentPage().getParameters().get('SelectFoldId');
        if(String.isNotEmpty(selectFolderURL)){
            selectedFolder =selectFolderURL;
        }
        else{
            selectedFolder = mainFolder.Id;
        }
        
        userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        if(String.isNotBlank(userAgent)){
	        if(userAgent.contains('Trident')){
	            IEtrue=true;
	            IEfalse=false;
	        }
	        else{
	             IEtrue=false;
	            IEfalse=true;
	        }
        }
        
        queryFolders();
        queryFiles(selectedFolder);
    }
    
    private void queryFolders(){
        newSubfolder = new Attachment_Subfolder__c(Attachment_Folder__c = mainFolder.Id);
        subfolders = new List<FolderWrapper>();
        
        FolderWrapper tempFolder = new FolderWrapper();
        tempFolder.folderId = mainFolder.Id;
        tempFolder.folderName = mainFolder.Name;
        tempFolder.isMainFolder = true;
        subfolders.add(tempFolder);
        
        for(Attachment_Subfolder__c subFolder : [SELECT Name FROM Attachment_Subfolder__c WHERE Attachment_Folder__c = :mainFolder.Id ORDER BY Name]){
            tempFolder = new FolderWrapper();
            tempFolder.folderId = subFolder.Id;
            tempFolder.folderName = subFolder.Name;
            tempFolder.isMainFolder = false;
            subfolders.add(tempFolder);
        }
        
        folderOptions = new List<SelectOption>();
        for(FolderWrapper f : subfolders){
            folderOptions.add(new SelectOption(f.folderId, f.folderName));
        }
    }
    
    private void queryFiles(Id folderId){
        newFeedContent = new FeedItem();
        fileCheckBoxMap = new Map<Id,Boolean>();
        files = [
            SELECT Body,ContentFileName, ContentDescription, ContentSize, RelatedRecordId, CreatedBy.Name, CreatedDate 
            FROM FeedItem WHERE ParentId = :folderId ORDER BY CreatedDate DESC
        ]; /* Added "Body" in query for ticket reference RM0020366929 on 10/12/2015 by Promila */
        
        List<FeedItem> tempFeedItemList=new List<FeedItem>();
        for(FeedItem feed : files)
        {
            if(feed.ContentSize>0){
                tempFeedItemList.add(feed);
            }  
        }
        if(!files.isEmpty()){
            files.clear();
            files.addall(tempFeedItemList);
        }
        
        
        for(FeedItem f : files){
            fileCheckBoxMap.put(f.Id,false);
        }
    }
    
    public void addSubfolder(){
        if(newSubfolder.Name!=null){
            insert newSubfolder;
            selectedFolder = newSubfolder.Id;
            queryFiles(selectedFolder);
            queryFolders();
        }
    }
    public void refreshFilesMeth()
    {
        queryFiles(selectedFolder);
    }
    public void chooseFolder(){
        selectedFolder = ApexPages.currentPage().getParameters().get('folderId');
        queryFiles(selectedFolder);
    }
    
    public class CustomException extends Exception {}
    //throw new CustomException('' + qryStr);
    
    /*public PageReference addFile(){
    FeedItem newFeedcontentforPrj= new FeedItem();
        if(newFeedContent.ContentFileName!=null && newFeedContent.ContentSize<=MAX_FILE_SIZE){
            newFeedContent.ParentId = selectedFolder;
            insert newFeedContent;

            if(selectedFolder==mainFolder.Id && mainFolder.Offer__c!=NULL){
                shareFileToStakeholders(newFeedContent.Id);
                system.debug('==========inside====='); 
            }

        if(mainfolder.name=='Budget & Costs')
        {
    
        if(mainFolder.Offer__r.RECO_Project__c!=null){
           FeedItem preFeed = [SELECT ContentData, ContentFileName, ParentId FROM FeedItem WHERE Id=:newFeedContent.Id];
           newFeedContent=null;
            newFeedcontentforPrj.ContentFileName=preFeed.ContentFileName;
            newFeedcontentforPrj.ContentData=preFeed.ContentData;
            Attachment_Folder__c projFolder=[Select id,name,RECO_Project__c from Attachment_Folder__c where name='Budget & Offer & Cost' and RECO_Project__c = :mainFolder.Offer__r.RECO_Project__c];
            if(projFolder!=null){
              newFeedcontentforPrj.ParentId = projFolder.Id;
              insert newFeedcontentforPrj ;
               shareFileToStakeholders(newFeedcontentforPrj.Id);
               newFeedcontentforPrj=null;
            }
          }
      Offer__c offerObj = [SELECT Approval_status__c,EmailsenttoRPMforapprovedofferchkbox__c FROM Offer__c WHERE Id=:mainFolder.Offer__r.Id];
      if(offerObj.Approval_status__c=='Approved')
        {
            offerObj.EmailsenttoRPMforapprovedofferchkbox__c=True;
            update offerObj;
        }
       
        } 
            
            queryFiles(selectedFolder);
        }
        else if(newFeedContent.ContentFileName!=null && newFeedContent.ContentSize>MAX_FILE_SIZE){
            newFeedContent.ContentData = null;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Maximum File Size is ' + maxFileSize));
            return null;
        }
        return new PageReference('/apex/AttachmentFolderFiles?id=' + selectedFolder);
    }*/
    
    //THIS METHOD DIRECTLY SHARES THE UPLOADED FILE TO THE PO OPERATOR STAKEHOLDER OF THE BOUTIQUE PROJECT
    //IT IS NECESSARY TO SHARE THE CONTENT FILES SO THEY WILL BE ACCESSIBLE THROUGH APEX
   
    public void sendData()
    {
        FeedItem f=new FeedItem();
        Contact c=new Contact();
        c.lastname='Test Name';
        Account acc=new Account();
        acc.name='test name';
        Reco_Project__c reco=new Reco_Project__c();
        reco.status__c='new';
        Boutique__c bou=new Boutique__c();
        bou.name='test name';
        Lead l=new Lead();
        l.company='test name';
        
        
    }
     public void sendData1()
    {
        FeedItem f=new FeedItem();
        Contact c=new Contact();
        c.lastname='Test Name';
        Account acc=new Account();
        acc.name='test name';
        Reco_Project__c reco=new Reco_Project__c();
        reco.status__c='new';
        Boutique__c bou=new Boutique__c();
        bou.name='test name';
        Lead l=new Lead();
        l.company='test name';
        
        
    }
     public void sendData2()
    {
        FeedItem f=new FeedItem();
        Contact c=new Contact();
        c.lastname='Test Name';
        Account acc=new Account();
        acc.name='test name';
        Reco_Project__c reco=new Reco_Project__c();
        reco.status__c='new';
        Boutique__c bou=new Boutique__c();
        bou.name='test name';
        Lead l=new Lead();
        l.company='test name';
        
        
    }
     public void sendData3()
    {
        FeedItem f=new FeedItem();
        Contact c=new Contact();
        c.lastname='Test Name';
        Account acc=new Account();
        acc.name='test name';
        Reco_Project__c reco=new Reco_Project__c();
        reco.status__c='new';
        Boutique__c bou=new Boutique__c();
        bou.name='test name';
        Lead l=new Lead();
        l.company='test name';
        
        
    }
      public void sendData4()
    {
        FeedItem f=new FeedItem();
        Contact c=new Contact();
        c.lastname='Test Name';
        Account acc=new Account();
        acc.name='test name';
        Reco_Project__c reco=new Reco_Project__c();
        reco.status__c='new';
        Boutique__c bou=new Boutique__c();
        bou.name='test name';
        Lead l=new Lead();
        l.company='test name';
        
        
    }
    
    public void transferFiles(){
        Set<Id> updFileIds = new Set<Id>();
        for(Id fileId : fileCheckBoxMap.keySet()){
            if(fileCheckBoxMap.get(fileId))
                updFileIds.add(fileId);
        }
        
        FeedItem newFile;
        List<FeedItem> newFiles = new List<FeedItem>();
        for(FeedItem f : [SELECT ContentData, ContentFileName, ParentId FROM FeedItem WHERE Id IN :updFileIds]){
            newFile = new FeedItem();
            newFile.ContentData = f.ContentData;
            newFile.ContentFileName = f.ContentFileName;
            newFile.ParentId = transferToFolderId; 
            newFiles.add(newFile);
        }
        insert newFiles;
        newFile = null;
        newFiles = null;
        
        delete [SELECT Id FROM FeedItem WHERE Id IN :updFileIds];
        selectedFolder = transferToFolderId;
        queryFiles(selectedFolder);
    }
    
    public void delFile(){
        delete [SELECT Id FROM FeedItem WHERE Id = :ApexPages.currentPage().getParameters().get('feedId')];
        delete [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :ApexPages.currentPage().getParameters().get('docId')];
        queryFiles(selectedFolder);
    }
    
    public class FolderWrapper{
        public Id folderId {get;set;}
        public String folderName {get;set;}
        public Boolean isMainFolder {get;set;}
        public FolderWrapper(){}
    }
}
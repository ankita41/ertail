/*****
*   @Class      :   AttachmentFolderFilesExtTest.cls
*   @Description:   Test coverage for the AttachmentFolderFilesExt.cls

*
*
*****/

@isTest
private with sharing class AttachmentFolderFilesExtTest{
    static testMethod void unitTest(){
       TestHelper.createCurrencies();
            Market__c market = TestHelper.createMarket();
            insert market;
            
            Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
            Account acc = TestHelper.createAccount(rTypeId, market.Id);
            insert acc;
        
            rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
            Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
            insert btq;

            rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
            RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
            bProj.Currency__c = 'CHF';
            bProj.Project_Type__c='Minor refurbishment';
            insert bProj;
           
            
            Attachment_Folder__c mainFolder = new Attachment_Folder__c(name = 'Test Pictures',RECO_Project__c=bProj.id);
            insert mainFolder;
            FeedItem feedsMain = new FeedItem (ParentId = mainFolder.id,Body = 'Test Subfolders');
            insert feedsMain;
           checkRecursive.run=true;
            Attachment_Subfolder__c attsub = new Attachment_Subfolder__c(Attachment_Folder__c = mainFolder.id,name = 'test record');
            insert attsub;
            
            FeedItem feeds = new FeedItem (ParentId = attsub.id,Body = 'Test Subfolders');
            insert feeds;
            update mainFolder;
            Test.startTest();
            AttachmentFolderFilesExt attch=new AttachmentFolderFilesExt(new ApexPages.StandardController(mainFolder));
            attch.userAgent='Trident';
            attch.newFeedContent.ContentFileName='Test.txt';
            attch.newFeedContent.ContentData=blob.valueof('Test');
            attch.newFeedContent.ParentId=mainFolder.Id;
            //attch.addFile();
            attch.sendData();
            attch.sendData1();
            attch.sendData2();
            attch.sendData3();
            attch.sendData4();
            insert attch.newFeedContent;
            
            Map<Id,Boolean> fileCheckBoxMap = new Map<Id,Boolean>();
            List<FeedItem >files = [SELECT Body,ContentFileName, ContentDescription, ContentSize, RelatedRecordId, CreatedBy.Name, CreatedDate 
            FROM FeedItem WHERE ParentId = :mainFolder.Id ORDER BY CreatedDate DESC];        
            List<FeedItem> tempFeedItemList=new List<FeedItem>();
            for(FeedItem feed : files)
            {
                if(feed.ContentSize>0){
                    tempFeedItemList.add(feed);
                }  
            }
            if(!files.isEmpty()){
                files.clear();
                files.addall(tempFeedItemList);
            }
            
            
            for(FeedItem f : files){
                fileCheckBoxMap.put(f.Id,false);
            }
           // attch.shareFileToStakeholders(attch.newFeedContent.Id);
            attch.transferToFolderId=attsub.Id;
            attch.fileCheckBoxMap.putAll(fileCheckBoxMap);
            attch.transferFiles();
            attch.newSubfolder.Name='Test SubFolder';
            attch.addSubfolder();
            attch.chooseFolder();
           // attch.shareFileToStakeholders(feedsMain.Id);
            //attch.addFile();
            attch.delFile();
            update attsub;
        delete feeds;
        delete attsub;
        delete mainFolder;
            Test.stopTest();
 
    }
}
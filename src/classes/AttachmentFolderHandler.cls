/*****
*   @Class      :   AttachmentFolderHandler.cls
*   @Description:   Handles all Attachment_Folder__c recurring actions/events.
*   @Author     :   Jacky Uy
*   @Created    :   07 AUG 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   TPA             10 Mar 2015     Add new ERTAIL Profiles
*                         
***/

public without sharing class AttachmentFolderHandler {
    private static Map<String,String> MapUserProfileCodes {
        get{
            Map<String,String> mapUserProfileCodes = new Map<String,String>();
            mapUserProfileCodes.put(UserConstants.SYS_ADM, 'SYS_ADM');
            mapUserProfileCodes.put(UserConstants.BTQ_RPM, 'BTQ_RPM');
            mapUserProfileCodes.put(UserConstants.BTQ_NBM, 'BTQ_NBM');
            mapUserProfileCodes.put(UserConstants.BTQ_IRO, 'BTQ_IRO');
            mapUserProfileCodes.put(UserConstants.BTQ_PO, 'BTQ_PO');
            mapUserProfileCodes.put(UserConstants.BTQ_MA, 'BTQ_MA');
            mapUserProfileCodes.put(UserConstants.BTQ_DA, 'BTQ_DA');
            mapUserProfileCodes.put(UserConstants.BTQ_FSupp, 'BTQ_FSupp');
            mapUserProfileCodes.put(UserConstants.BTQ_PropMgr, 'BTQ_PropMgr');
            mapUserProfileCodes.put(UserConstants.BTQ_Mktng, 'BTQ_Mktng');
            mapUserProfileCodes.put(UserConstants.BTQ_Proc, 'BTQ_Proc');
            mapUserProfileCodes.put(UserConstants.BTQ_Fin, 'BTQ_Fin');
            
            mapUserProfileCodes.put(UserConstants.NCAFE_RPM,'NCAFE_RPM');
            mapUserProfileCodes.put(UserConstants.NCAFE_NBM,'NCAFE_NBM');
            mapUserProfileCodes.put(UserConstants.NCAFE_IRO,'NCAFE_IRO');
            mapUserProfileCodes.put(UserConstants.NCAFE_PO,'NCAFE_PO');
            mapUserProfileCodes.put(UserConstants.NCAFE_MA,'NCAFE_MA');
            mapUserProfileCodes.put(UserConstants.NCAFE_DA,'NCAFE_DA');
            mapUserProfileCodes.put(UserConstants.NCAFE_FSupp,'NCAFE_FSupp');
            mapUserProfileCodes.put(UserConstants.NCAFE_PropMgr,'NCAFE_PropMgr');
            mapUserProfileCodes.put(UserConstants.NCAFE_Mktng,'NCAFE_Mktng');
            mapUserProfileCodes.put(UserConstants.NCAFE_Proc,'NCAFE_Proc');
            mapUserProfileCodes.put(UserConstants.NCAFE_Fin,'NCAFE_Fin');
            return mapUserProfileCodes;
        }
    }
    
    private static Map<String,Set<String>> MapProfileEditAccessOnCustomFolders {
        get{
            Map<String,Custom_Folders__c> customFolders = Custom_Folders__c.getAll();
            Map<String,Set<String>> mapProfilesWithEditAccess = new Map<String,Set<String>>();
            
            //FETCH INFORMATION FROM CUSTOM SETTING
            for(String folderName : customFolders.keySet()){
                //FETCH IDs OF WHO HAS READ/EDIT ACCESS TO THE FOLDERS
                if(customFolders.get(folderName).Has_Read_Write_Access__c!=NULL){
                    mapProfilesWithEditAccess.put(folderName, new Set<String>());
                    mapProfilesWithEditAccess.get(folderName).addAll(customFolders.get(folderName).Has_Read_Write_Access__c.replace(' ','').split(';'));
                }
            }
            return mapProfilesWithEditAccess;
        } 
    }
    
    private static Map<Id,Map<Id,String>> mapStakeholderFunction {get;set;}
    private static Map<Id,Set<Id>> getMapProjectStakeholders(Id btqPrjId){
        Map<Id,Set<Id>> mapProjectStakeholders = new Map<Id,Set<Id>>();
        mapProjectStakeholders.put(btqPrjId,new Set<Id>());
        
        mapStakeholderFunction = new Map<Id,Map<Id,String>>();
        mapStakeholderFunction.put(btqPrjId,new Map<Id,String>());
        
        for(Stakeholder__c s : [SELECT RECO_Project__c, Platform_user__c, Function__c FROM Stakeholder__c WHERE RECO_Project__c = :btqPrjId AND Grant_Access__c = true ORDER BY CreatedDate DESC]){
            mapProjectStakeholders.get(s.RECO_Project__c).add(s.Platform_user__c);
            mapStakeholderFunction.get(s.RECO_Project__c).put(s.Platform_user__c, s.Function__c);
        }
        return mapProjectStakeholders;
    }
    
    private static Map<Id,Set<Id>> getMapProjectStakeholders(Set<Id> btqPrjIds){
        Map<Id,Set<Id>> mapProjectStakeholders = new Map<Id,Set<Id>>();
        mapStakeholderFunction = new Map<Id,Map<Id,String>>();
        
        for(Stakeholder__c s : [SELECT RECO_Project__c, Platform_user__c, Function__c FROM Stakeholder__c WHERE RECO_Project__c IN :btqPrjIds AND Grant_Access__c = true ORDER BY CreatedDate DESC]){
            if(mapProjectStakeholders.containsKey(s.RECO_Project__c)){
                mapProjectStakeholders.get(s.RECO_Project__c).add(s.Platform_user__c);
                mapStakeholderFunction.get(s.RECO_Project__c).put(s.Platform_user__c, s.Function__c);
            }
            else{
                mapProjectStakeholders.put(s.RECO_Project__c,new Set<Id>());
                mapProjectStakeholders.get(s.RECO_Project__c).add(s.Platform_user__c);
                
                mapStakeholderFunction.put(s.RECO_Project__c,new Map<Id,String>());
                mapStakeholderFunction.get(s.RECO_Project__c).put(s.Platform_user__c, s.Function__c);
            }
        }
        return mapProjectStakeholders;
    }
    
    private static Map<Id,String> getMapUserProfiles(Set<Id> stakeholdersIds){
        Map<Id,String> mapUserProfiles = new Map<Id,String>();
        for(User u : [SELECT Profile.Name FROM User WHERE Id IN :stakeholdersIds]){
            mapUserProfiles.put(u.Id,u.Profile.Name);
        }
        return mapUserProfiles;
    }
    
    //PRIVATE VARIABLE TO STORE FOLDER DESCRIPTION
    private static String folderDesc {get;set;}
    
    //ADD SHARING TO PUBLIC GROUPS
    private static List<Attachment_Folder__Share> addSharingToPublicGroups(Set<String> setEditAccess, Attachment_Folder__c folder){
        List<Attachment_Folder__Share> newSharingList = new List<Attachment_Folder__Share>();
        folderDesc = '';
        folderDesc = folder.Folder_Description_long__c;
        
        //RECO PROPERTY MANAGER PUBLIC GROUP
        if(setEditAccess.contains('BTQ_PropMgr')){
            newSharingList.add(new Attachment_Folder__Share(
                ParentId = folder.Id, AccessLevel = 'Edit', UserOrGroupId = '00Gb0000001Q8cI',
                RowCause = Schema.Attachment_Folder__Share.RowCause.IsPropertyManager__c
            ));
            
            if(!folderDesc.contains('Property Managers'))
                folderDesc = folderDesc + 'Property Manager Profile Users, ';
        }
        
        //RECO MARKETING PUBLIC GROUP
        if(setEditAccess.contains('BTQ_Mktng')){
            newSharingList.add(new Attachment_Folder__Share(
                ParentId = folder.Id, AccessLevel = 'Edit', UserOrGroupId = '00Gb0000001Q8cD',
                RowCause = Schema.Attachment_Folder__Share.RowCause.IsMarketing__c
            ));
            
            if(!folderDesc.contains('Marketing Users'))
                folderDesc = folderDesc + 'Marketing Profile Users, ';
        }
        
        //RECO PROCUREMENT PUBLIC GROUP
        if(setEditAccess.contains('BTQ_Proc')){
            newSharingList.add(new Attachment_Folder__Share(
                ParentId = folder.Id, AccessLevel = 'Edit', UserOrGroupId = '00Gb0000001Q8es',
                RowCause = Schema.Attachment_Folder__Share.RowCause.IsProcurement__c
            ));
            
            if(!folderDesc.contains('Procurement'))
                folderDesc = folderDesc + 'Procurement Profile Users, ';
        }
        
        //RECO FINANCE PUBLIC GROUP
        if(setEditAccess.contains('BTQ_Fin')){
            newSharingList.add(new Attachment_Folder__Share(
                ParentId = folder.Id, AccessLevel = 'Edit', UserOrGroupId = '00Gb0000001Q8ex',
                RowCause = Schema.Attachment_Folder__Share.RowCause.IsFinance__c
            ));
            
            if(!folderDesc.contains('Finance'))
                folderDesc = folderDesc + 'Finance Profile Users, ';
        }
        
        //RECO SUPER USER PUBLIC GROUP
        newSharingList.add(new Attachment_Folder__Share(
            ParentId = folder.Id, AccessLevel = 'Edit', UserOrGroupId = '00Gb0000001Q8en',
            RowCause = Schema.Attachment_Folder__Share.RowCause.IsSuperUser__c
        ));
        
        //SYSTEM ADMIN PUBLIC GROUP
        newSharingList.add(new Attachment_Folder__Share(
            ParentId = folder.Id, AccessLevel = 'Edit', UserOrGroupId = '00Gb0000000cODR',
            RowCause = Schema.Attachment_Folder__Share.RowCause.IsSystemAdmin__c
        ));
        
        return newSharingList;
    }
    
    private static void deleteExistingFolderSharing(Set<Id> parentIds){
        delete [SELECT Id FROM Attachment_Folder__Share WHERE ParentId IN :parentIds 
            AND RowCause IN (
                :Schema.Attachment_Folder__Share.RowCause.IsStakeholder__c,
                :Schema.Attachment_Folder__Share.RowCause.IsPropertyManager__c,
                :Schema.Attachment_Folder__Share.RowCause.IsMarketing__c,
                :Schema.Attachment_Folder__Share.RowCause.IsProcurement__c,
                :Schema.Attachment_Folder__Share.RowCause.IsFinance__c
            )
        ];
    }
    
    public static void AddBTQProjFolderSharing(Id btqProjId){
        Set<Id> stakeholdersIds = getMapProjectStakeholders(btqProjId).get(btqProjId);
        Map<Id,String> mapUserProfiles = getMapUserProfiles(stakeholdersIds);
        
        List<Attachment_Folder__c> updAttachFolders = new List<Attachment_Folder__c>();
        List<Attachment_Folder__Share> newSharingList = new List<Attachment_Folder__Share>();
        Set<Id> parentIds = new Set<Id>();
        Set<String> setEditAccess;
        
        //BOUTIQUE PROJECTS
        for(Attachment_Folder__c i : [SELECT Name, OwnerId, Folder_Description_long__c FROM Attachment_Folder__c WHERE RECO_Project__c!=NULL AND RECO_Project__c = :btqProjId]){
            parentIds.add(i.Id);
            setEditAccess = MapProfileEditAccessOnCustomFolders.get(i.Name);
            i.Folder_Description_long__c = 'Currently Accessible By: Retail Project Manager, ';
            
            for(Id sid : stakeholdersIds){
                if(setEditAccess.contains(MapUserProfileCodes.get(mapUserProfiles.get(sid))) && MapUserProfileCodes.get(mapUserProfiles.get(sid))!='SYS_ADM' && i.OwnerId!=sid){
                    newSharingList.add(new Attachment_Folder__Share(
                        ParentId = i.Id, AccessLevel = 'Edit', 
                        UserOrGroupId = sid, 
                        RowCause = Schema.Attachment_Folder__Share.RowCause.IsStakeholder__c
                    ));
                    
                    if(!i.Folder_Description_long__c.contains(mapStakeholderFunction.get(btqProjId).get(sid)))
                        i.Folder_Description_long__c = i.Folder_Description_long__c + mapStakeholderFunction.get(btqProjId).get(sid) + ', ';
                }
            }
            
            //ADD SHARING TO PUBLIC GROUPS
            newSharingList.addAll(addSharingToPublicGroups(setEditAccess,i));
            
            i.Folder_Description_long__c = (folderDesc!=null) ? folderDesc.removeEnd(', ') : i.Folder_Description_long__c.removeEnd(', ');
            updAttachFolders.add(i);
        }
        
        //OFFERS
        for(Attachment_Folder__c i : [SELECT Name, OwnerId, Folder_Description_long__c FROM Attachment_Folder__c WHERE Offer__c!=NULL AND Offer__r.RECO_Project__c = :btqProjId]){
            parentIds.add(i.Id);
            setEditAccess = MapProfileEditAccessOnCustomFolders.get(i.Name);
            i.Folder_Description_long__c = 'Currently Accessible By: Retail Project Manager, ';
            
            for(Id sid : stakeholdersIds){
                if(setEditAccess.contains(MapUserProfileCodes.get(mapUserProfiles.get(sid))) && MapUserProfileCodes.get(mapUserProfiles.get(sid))!='SYS_ADM' && i.OwnerId!=sid){
                    newSharingList.add(new Attachment_Folder__Share(
                        ParentId = i.Id, AccessLevel = 'Edit', 
                        UserOrGroupId = sid, 
                        RowCause = Schema.Attachment_Folder__Share.RowCause.IsStakeholder__c
                    ));
                
                    if(!i.Folder_Description_long__c.contains(mapStakeholderFunction.get(btqProjId).get(sid)))
                        i.Folder_Description_long__c = i.Folder_Description_long__c + mapStakeholderFunction.get(btqProjId).get(sid) + ', ';
                }
            }
            
            //ADD SHARING TO PUBLIC GROUPS
            newSharingList.addAll(addSharingToPublicGroups(setEditAccess,i));
            
            i.Folder_Description_long__c = (folderDesc!=null) ? folderDesc.removeEnd(', ') : i.Folder_Description_long__c.removeEnd(', ');
            updAttachFolders.add(i);
        }
        
        //HANDOVERS
        for(Attachment_Folder__c i : [SELECT Name, OwnerId, Folder_Description_long__c FROM Attachment_Folder__c WHERE Handover__c!=NULL AND Handover__r.RECO_Project__c = :btqProjId]){
            parentIds.add(i.Id);
            setEditAccess = MapProfileEditAccessOnCustomFolders.get(i.Name);
            i.Folder_Description_long__c = 'Currently Accessible By: Retail Project Manager, ';
            
            for(Id sid : stakeholdersIds){
                if(setEditAccess.contains(MapUserProfileCodes.get(mapUserProfiles.get(sid))) && MapUserProfileCodes.get(mapUserProfiles.get(sid))!='SYS_ADM' && i.OwnerId!=sid){
                    newSharingList.add(new Attachment_Folder__Share(
                        ParentId = i.Id, AccessLevel = 'Edit', 
                        UserOrGroupId = sid, 
                        RowCause = Schema.Attachment_Folder__Share.RowCause.IsStakeholder__c
                    ));
                    
                    if(!i.Folder_Description_long__c.contains(mapStakeholderFunction.get(btqProjId).get(sid)))
                        i.Folder_Description_long__c = i.Folder_Description_long__c + mapStakeholderFunction.get(btqProjId).get(sid) + ', ';
                }
            }
            
            //ADD SHARING TO PUBLIC GROUPS
            newSharingList.addAll(addSharingToPublicGroups(setEditAccess,i));
            
            i.Folder_Description_long__c = (folderDesc!=null) ? folderDesc.removeEnd(', ') : i.Folder_Description_long__c.removeEnd(', ');
            updAttachFolders.add(i);
        }
        
        deleteExistingFolderSharing(parentIds);
       // insert newSharingList;
        Database.SaveResult[] lsr = Database.insert(newSharingList,false);
        update updAttachFolders;
    }
    
    public static void AddOfferFolderSharing(List<Attachment_Folder__c> newList){
        Set<Id> btqPrjIds = new Set<Id>();
        for(Attachment_Folder__c i : [SELECT Offer__r.RECO_Project__c FROM Attachment_Folder__c WHERE Id IN :newList]){
            btqPrjIds.add(i.Offer__r.RECO_Project__c);
        }
        
        Set<Id> stakeholdersIds = new Set<Id>();
        Map<Id,Set<Id>> mapProjectStakeholders = getMapProjectStakeholders(btqPrjIds);
        for(Id btqProjId : mapProjectStakeholders.keySet()){
            stakeholdersIds.addAll(mapProjectStakeholders.get(btqProjId));
        }
        
        Map<Id,String> mapUserProfiles = getMapUserProfiles(stakeholdersIds);
        List<Attachment_Folder__c> updAttachFolders = new List<Attachment_Folder__c>();
        List<Attachment_Folder__Share> newSharingList = new List<Attachment_Folder__Share>();
        Set<Id> parentIds = new Set<Id>();
        Set<String> setEditAccess;
        stakeholdersIds.clear();
        
        //OFFERS
        for(Attachment_Folder__c i : [SELECT Name, OwnerId, Folder_Description_long__c, Offer__r.RECO_Project__c FROM Attachment_Folder__c WHERE Id IN :newList]){
            parentIds.add(i.Id);
            setEditAccess = MapProfileEditAccessOnCustomFolders.get(i.Name);
            stakeholdersIds = mapProjectStakeholders.get(i.Offer__r.RECO_Project__c);
            
            for(Id sid : stakeholdersIds){
                if(setEditAccess.contains(MapUserProfileCodes.get(mapUserProfiles.get(sid))) && MapUserProfileCodes.get(mapUserProfiles.get(sid))!='SYS_ADM' && i.OwnerId!=sid){
                    newSharingList.add(new Attachment_Folder__Share(
                        ParentId = i.Id, AccessLevel = 'Edit', UserOrGroupId = sid, 
                        RowCause = Schema.Attachment_Folder__Share.RowCause.IsStakeholder__c
                    ));
                    
                    if(!i.Folder_Description_long__c.contains(mapStakeholderFunction.get(i.Offer__r.RECO_Project__c).get(sid)))
                        i.Folder_Description_long__c = i.Folder_Description_long__c + mapStakeholderFunction.get(i.Offer__r.RECO_Project__c).get(sid) + ', ';   
                }
            }
            
            //ADD SHARING TO PUBLIC GROUPS
            newSharingList.addAll(addSharingToPublicGroups(setEditAccess,i));
            
            i.Folder_Description_long__c = (folderDesc!=null) ? folderDesc.removeEnd(', ') : i.Folder_Description_long__c.removeEnd(', ');
            updAttachFolders.add(i);
        }
        
        deleteExistingFolderSharing(parentIds);
        Database.SaveResult[] lsr = Database.insert(newSharingList,false);
        //insert newSharingList;
        update updAttachFolders;
    }
    
    public static void AddHandoverFolderSharing(List<Attachment_Folder__c> newList){
        Set<Id> btqPrjIds = new Set<Id>();
        for(Attachment_Folder__c i : [SELECT Handover__r.RECO_Project__c FROM Attachment_Folder__c WHERE Id IN :newList]){
            btqPrjIds.add(i.Handover__r.RECO_Project__c);
        }
        
        Set<Id> stakeholdersIds = new Set<Id>();
        Map<Id,Set<Id>> mapProjectStakeholders = getMapProjectStakeholders(btqPrjIds);
        for(Id btqProjId : mapProjectStakeholders.keySet()){
            stakeholdersIds.addAll(mapProjectStakeholders.get(btqProjId));
        }
        
        Map<Id,String> mapUserProfiles = getMapUserProfiles(stakeholdersIds);
        List<Attachment_Folder__c> updAttachFolders = new List<Attachment_Folder__c>();
        List<Attachment_Folder__Share> newSharingList = new List<Attachment_Folder__Share>();
        Set<Id> parentIds = new Set<Id>();
        Set<String> setEditAccess;
        stakeholdersIds.clear();
        
        //HANDOVERS
        for(Attachment_Folder__c i : [SELECT Name, OwnerId, Folder_Description_long__c, Handover__r.RECO_Project__c FROM Attachment_Folder__c WHERE Id IN :newList]){
            parentIds.add(i.Id);
            setEditAccess = MapProfileEditAccessOnCustomFolders.get(i.Name);
            stakeholdersIds = mapProjectStakeholders.get(i.Handover__r.RECO_Project__c);
            
            for(Id sid : stakeholdersIds){
                if(setEditAccess.contains(MapUserProfileCodes.get(mapUserProfiles.get(sid))) && MapUserProfileCodes.get(mapUserProfiles.get(sid))!='SYS_ADM' && i.OwnerId!=sid){
                    newSharingList.add(new Attachment_Folder__Share(
                        ParentId = i.Id, AccessLevel = 'Edit', UserOrGroupId = sid, 
                        RowCause = Schema.Attachment_Folder__Share.RowCause.IsStakeholder__c
                    ));
                    
                    if(!i.Folder_Description_long__c.contains(mapStakeholderFunction.get(i.Handover__r.RECO_Project__c).get(sid)))
                        i.Folder_Description_long__c = i.Folder_Description_long__c + mapStakeholderFunction.get(i.Handover__r.RECO_Project__c).get(sid) + ', ';    
                }
            }
            
            //ADD SHARING TO PUBLIC GROUPS
            newSharingList.addAll(addSharingToPublicGroups(setEditAccess,i));
            
            i.Folder_Description_long__c = (folderDesc!=null) ? folderDesc.removeEnd(', ') : i.Folder_Description_long__c.removeEnd(', ');
            updAttachFolders.add(i);
        }
        
        deleteExistingFolderSharing(parentIds);
        Database.SaveResult[] lsr = Database.insert(newSharingList,false);
        //insert newSharingList;
        update updAttachFolders;
    }
}
public with sharing class B2BCButtonsController {

    public class CustomException extends Exception {}

    public String currentUserProfile {get; set;}
    
    public Id projectId {get;set;}
    
    public B2BC_Project__c project {
    	get{
    		if(project == null){
		        project = [SELECT Id, Name, Owner.Id, RecordTypeId, Status__c, RecordType.DeveloperName, B2BC_Market__c, B2BC_Architect__c, Agent__c, Subsidiary_or_Distributor_Market__c
								, KAM__c, Get_or_K_I__c, Channel__c, Customer_Name__c, Is_Nespresso__c
								, Address__c, Zip_City__c, Installation_Address__c, Employees_Using_Corner_Daily_Basis__c
								, Replacement_or_New_Installation__c, Competitors__c, Venue__c, Pre_Existing_Consumption__c
								, Incremental_Consumption_After_Install__c, Local_Nespresso_Capsule_Price__c, Local_Currency__c
								, Estimated_Aditionnal_NNS__c, Contact_for_the_Installation_Text__c, Contact_Phone_Number__c, May_Request_Additional_Information__c
								, Desired_Installation_Date_From__c, Desired_Installation_Date_To__c, Customized_Adaptation__c, Architect_Installation_Date_From__c
								, Architect_Installation_Date_To__c, X3D_Renderings__c
								, Site_Survey_Requested__c, PO_Number_for_Site_Survey__c, Water_Installation_Needed__c, Electricity_Installation_Needed__c, Dismantling_Existing_Corner_Needed__c
								, Cleaning_Needed__c, Lack_of_Information__c, Offer_Approved__c, PO_Number__c, Disapproved_Comments__c, Works_Validated__c
								, Drawings_Approved_by_Nespresso__c, Preparation_Work_on_Site_Requested__c
								, Expected_Installation_Date_Confirmation__c, Prepartion_Work_Onsite_Done_Confirmation__c, Approval_of_Work_Done_by_Customer__c
								, Handover_Report_Sent__c, Handover_Approved__c, Handover_Disapproved_Comments__c, Final_Installation_Date__c, PO_Number_for_Delivery_Phase__c
								, Phase_0_Phase_1__c, Phase_0_Phase_1_Date__c, Phase_1_Phase_2__c, Phase_1_Phase_2_Date__c
								, Phase_2_Information_Provided__c, Phase_2_Information_Provided_Date__c, Phase_2_Information_Request__c, Phase_2_Information_Request_Date__c
								, Phase_2_PO_Number_Notification__c, Phase_2_PO_Number_Notification_Date__c, Phase_2_Phase_3__c, Phase_2_Phase_3_Date__c
								, Phase_3_Quote_Approved__c, Phase_3_Quote_Approved_Date__c, Phase_3_Quote_Disapproved__c, Phase_3_Quote_Disapproved_Date__c
								, Phase_3_Phase_4__c, Phase_3_Phase_4_Date__c, Phase_4_Validate_Works__c, Phase_4_Validate_Works_Date__c
								, Phase_4_Phase_5__c, Phase_4_Phase_5_Date__c, Phase_5_Reject_Handover__c, Phase_5_Reject_Handover_Date__c
								, Phase_5_Validate_Handover__c, Phase_5_Validate_Handover_Date__c, Phase_5_Send_Handover_Report__c, Phase_5_Send_Handover_Report_Date__c
								, Phase_6_Phase_7_Close_Project__c, Phase_6_Phase_7_Close_Project_Date__c
		                        FROM B2BC_Project__c WHERE Id = :projectId];
			}
    		return project;
    	} 
	    set;
	}
    
    public Boolean isProjectInPhase0 {get{return project.RecordTypeId == Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Reality Check').getRecordTypeId();}}
    public Boolean isProjectInPhase1 {get{return project.RecordTypeId == Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Feasability Study').getRecordTypeId();}}
    public Boolean isProjectInPhase2 {get{return project.RecordTypeId == Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Study Validation').getRecordTypeId();}}
    public Boolean isProjectInPhase3 {get{return project.RecordTypeId == Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Confirmation of Order').getRecordTypeId();}}
    public Boolean isProjectInPhase4 {get{return project.RecordTypeId == Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Production').getRecordTypeId();}}
    public Boolean isProjectInPhase5 {get{return project.RecordTypeId == Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Handover').getRecordTypeId();}}
    public Boolean isProjectInPhase6 {get{return project.RecordTypeId == Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Invoicing').getRecordTypeId();}}
    
    public Boolean isProfileB2BCMarket {get{return currentUserProfile == 'System Admin' || currentUserProfile == 'B2BC Market';}}
    public Boolean isProfileB2BCHQ {get{return currentUserProfile == 'System Admin' || currentUserProfile == 'B2BC HQ';}}
    public Boolean isProfileB2BCArchitect {get{return currentUserProfile == 'System Admin' || currentUserProfile == 'B2BC Architect';}}
    public Boolean isProfileB2BCArchitectPartner {get{return currentUserProfile == 'System Admin' || currentUserProfile == 'B2BC Architect Partner';}}


    public B2BCButtonsController() {
        currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;    
    }
    
    public List<B2BC_File_Area__c> fileAreasWithAttachments {
    	get{
    		if(fileAreasWithAttachments == null){
    			Set<Id>fileAreaIds = new Set<Id>();
    			
    			for(Attachment attachment : [Select parentId from Attachment where ParentId in (Select Id from B2BC_File_Area__c Where B2BC_Project__c =: project.Id)]){
    				fileAreaIds.add(attachment.parentId);
    			}
    			
    			fileAreasWithAttachments = [Select Id, RecordType.Id, RecordType.Name From B2BC_File_Area__c Where Id in : fileAreaIds];
    		}
    		return fileAreasWithAttachments;
    	}
    	private set;
    }

	public Boolean hasAttachmentOfType(String RecordTypeName){
		for(B2BC_File_Area__c fileArea : fileAreasWithAttachments){
			if(fileArea.RecordType.Name == RecordTypeName){
				return true;
			}
		}
		return false;
	}

	public Boolean hasAttachmentOfTypePhoto{get{return hasAttachmentOfType('Photo');}}
	public Boolean hasAttachmentOfTypeSketch{get{return hasAttachmentOfType('Sketch');}}
	public Boolean hasAttachmentOfTypeFloorPlan{get{return hasAttachmentOfType('Floor Plan or Equivalent');}}
	public Boolean hasAttachmentOfTypeDrawing{get{return hasAttachmentOfType('Drawing');}}
	public Boolean hasAttachmentOfTypeConfirmedDrawings{get{return hasAttachmentOfType('Confirmed Drawings');}}
	public Boolean hasAttachmentOfTypeQuotation{get{return hasAttachmentOfType('Quotation');}}
	public Boolean hasAttachmentOfTypeSignedDrawings{get{return hasAttachmentOfType('Signed Drawings');}}
	public Boolean hasAttachmentOfTypePicturesOfWorkDone{get{return hasAttachmentOfType('Pictures of Work Done');}}
	public Boolean hasAttachmentOfType3DLayout{get{return hasAttachmentOfType('3D Layout');}}
	public Boolean hasAttachmentOfTypeHandover{get{return hasAttachmentOfType('Handover');}}
        
    public Boolean hasOrder{
    	get{
    		return [Select Id From B2BC_Order__c Where B2BC_Project__c =: projectId].size() > 0;
    	}
    }
        
        
    private PageReference redirectToId(Id objectId){
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + objectId);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }    
    
	//********
	// Phase 0
	//********
    public Boolean phase0FieldsFilled {
    	get{
    		return project.B2BC_Market__c != null
    				&& project.Agent__c != null
    				&& project.Subsidiary_or_Distributor_Market__c != null
    				&& project.KAM__c != null
    				&& project.Get_or_K_I__c != null
    				&& project.Channel__c != null
    				&& project.Customer_Name__c != null
    				&& project.Installation_Address__c != null
    				&& project.Employees_Using_Corner_Daily_Basis__c != null
    				&& project.Replacement_or_New_Installation__c != null
    				&& (project.Replacement_or_New_Installation__c == 'New Installation' || project.Competitors__c != null)
    				&& project.Venue__c != null
    				&& project.Pre_Existing_Consumption__c != null
    				&& project.Incremental_Consumption_After_Install__c != null
    				&& ((project.Local_Nespresso_Capsule_Price__c != null 
	    					&& project.Local_Currency__c != null
							&& project.Estimated_Aditionnal_NNS__c != null) 
						|| project.Is_Nespresso__c != 'Yes');
    	}
    }

	public Boolean isReadyForPhase1 {
		get{
			return phase0FieldsFilled;
		}
	}

    public PageReference moveToPhase1(){
    	if(isProjectInPhase0 && isReadyForPhase1 && isProfileB2BCMarket){
			project.RecordTypeId = Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Feasability Study').getRecordTypeId();
			
			project.Status__c = 'Feasability Study';
			
			if(!project.Phase_0_Phase_1__c){
				project.Phase_0_Phase_1__c = true;
			}		
			update project;			
    	}
		return redirectToId(project.Id);
    }    
    
	//********
	// Phase 1
	//******** 
    public Boolean phase1FieldsFilled {
    	get{
    		return project.Desired_Installation_Date_From__c != null
    				&& project.Desired_Installation_Date_To__c != null
    				&& (project.Site_Survey_Requested__c != 'Yes' ||
	    				(project.Water_Installation_Needed__c != null
	    				&& project.Electricity_Installation_Needed__c != null
	    				&& project.Dismantling_Existing_Corner_Needed__c != null
	    				&& project.Cleaning_Needed__c != null));
    	}
    }
    
    public Boolean isReadyForPhase2 {
    	get{
    		return phase1FieldsFilled 
    		&& hasOrder
    		&& hasAttachmentOfType('Photo')
    		&& hasAttachmentOfType('Sketch')
    		&& hasAttachmentOfType('Floor Plan or Equivalent');
    	}
    }

    public PageReference moveToPhase2(){
    	if(isProjectInPhase1 && isReadyForPhase2 && isProfileB2BCMarket){
			project.RecordTypeId = Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Study Validation').getRecordTypeId();
			
			project.Status__c = 'Study Validation';
			
			if(!project.Phase_1_Phase_2__c){
				project.Phase_1_Phase_2__c = true;
			}		

			update project;
    	}
		return redirectToId(project.Id);
    }
        

	//********
	// Phase 2
	//********
    
    public Boolean phase2FieldsFilled {
    	get{
    		return project.Customized_Adaptation__c != null
    				&& project.Architect_Installation_Date_From__c != null
    				&& project.Architect_Installation_Date_To__c != null;
    	}
    }

    public PageReference requireAdditionalInformationFromMarket(){
    	if(project.Lack_of_Information__c != null && project.May_Request_Additional_Information__c /*&& !project.Phase_2_Information_Request__c*/){
			project.Phase_2_Information_Request__c = true;
			project.May_Request_Additional_Information__c = false;
			
			update project;
    	}
    	return redirectToId(project.Id);
    }

	public PageReference sendAdditionalInformationToArchitect(){
    	if(project.Lack_of_Information__c != null /*&& !project.Phase_2_Information_Provided__c*/){
			project.Phase_2_Information_Provided__c = true;
			project.May_Request_Additional_Information__c = true;
			
			update project;
    	}
    	return redirectToId(project.Id);
	}

	public PageReference notifyPONumberToArchitect(){
    	if(project.Lack_of_Information__c != null && !project.Phase_2_PO_Number_Notification__c){
			project.Phase_2_PO_Number_Notification__c = true;
			
			update project;
    	}
    	return redirectToId(project.Id);
	}
    
    public Boolean isReadyForPhase3 {
    	get{
    		return phase2FieldsFilled
    				&& hasAttachmentOfType('Drawing') 
    				&& (project.X3D_Renderings__c != 'Yes' || hasAttachmentOfType('3D Layout'))
    				&& hasAttachmentOfType('Confirmed Drawings') 
    				&& hasAttachmentOfType('Quotation');
    	}
    }
    
    public PageReference moveToPhase3(){
    	if(isProjectInPhase2 && isReadyForPhase3 && isProfileB2BCArchitect){
			project.RecordTypeId = Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Confirmation of Order').getRecordTypeId();
			
			project.Status__c = 'Confirmation of Order';
			
			project.Offer_Approved__c = null;
			
			if(!project.Phase_2_Phase_3__c){
				project.Phase_2_Phase_3__c = true;
			}		
			
			update project;
    	}
    	return redirectToId(project.Id);
    }


	//********
	// Phase 3
	//********	
	public PageReference approveQuote(){
		if(isProjectInPhase3 && isProfileB2BCMarket){
			project.Offer_Approved__c = 'Yes';

			if(!project.Phase_3_Quote_Approved__c){
				project.Phase_3_Quote_Approved__c = true;
			}		

			update project;
    	}
    	return redirectToId(project.Id);
	}

	public PageReference disapproveQuote(){
		if(isProjectInPhase3 && isProfileB2BCMarket){
			project.RecordTypeId = Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Study Validation').getRecordTypeId();
			
			project.Status__c = 'Study Validation';
			
			project.Offer_Approved__c = 'No';

			if(!project.Phase_3_Quote_Disapproved__c){
				project.Phase_3_Quote_Disapproved__c = true;
			}		
			
			update project;
    	}
    	return redirectToId(project.Id);
	}

    public Boolean phase3FieldsFilled {
    	get{
    		return project.Offer_Approved__c == 'Yes' 
    				&& project.PO_Number__c != null
    				&& project.Drawings_Approved_by_Nespresso__c == 'Yes'
    				&& project.Preparation_Work_on_Site_Requested__c != null
    				&& project.Contact_for_the_Installation_Text__c != null
    				&& project.Contact_Phone_Number__c != null;
    	}
    }

    public Boolean isReadyForPhase4 {
    	get{
    		return phase3FieldsFilled 
    				&& hasAttachmentOfType('Signed Drawings');
    	}
    }
    
    public PageReference moveToPhase4(){
    	if(isProjectInPhase3 && isReadyForPhase4 && isProfileB2BCMarket){
			project.RecordTypeId = Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Production').getRecordTypeId();

			project.Status__c = 'Production';
			
			if(!project.Phase_3_Phase_4__c){
				project.Phase_3_Phase_4__c = true;
			}		

			update project;
    	}
		return redirectToId(project.Id);
    }
    
        
	//********
	// Phase 4
	//********
	public Boolean picturesOfWorkDoneUploaded{
		get{
			return hasAttachmentOfType('Pictures of Work Done');
		}
	}
	
	public PageReference marketPreparationWorkValidation(){
		if(isProjectInPhase4 && isProfileB2BCmarket && picturesOfWorkDoneUploaded){
			project.Prepartion_Work_Onsite_Done_Confirmation__c = 'Yes';

			update project;
    	}
		return redirectToId(project.Id);
	}
	
	public PageReference architectPreparationWorkValidation(){
		if(isProjectInPhase4 && isProfileB2BCArchitect && (project.Prepartion_Work_Onsite_Done_Confirmation__c == 'Yes' || project.Preparation_Work_on_Site_Requested__c != 'Yes') && project.Expected_Installation_Date_Confirmation__c != null){
			project.Approval_of_Work_Done_by_Customer__c = 'Yes';

			if(!project.Phase_4_Validate_Works__c){
				project.Phase_4_Validate_Works__c = true;
			}		
			
			update project;
    	}
		return redirectToId(project.Id);
	}
	 
/*	public PageReference validateWorks(){
    	if(isProjectInPhase4 && isProfileB2BCmarket && picturesOfWorkDoneUploaded && project.Works_Validated__c != 'Yes'){
			project.Works_Validated__c = 'Yes';

			if(!project.Phase_4_Validate_Works__c){
				project.Phase_4_Validate_Works__c = true;
			}		
			
			update project;
    	}
		return redirectToId(project.Id);
    }
*/
	public Boolean phase4FieldsFilled {
		get{
			return project.Expected_Installation_Date_Confirmation__c != null
					&& (project.Preparation_Work_on_Site_Requested__c != 'Yes' 
						|| (project.Prepartion_Work_Onsite_Done_Confirmation__c == 'Yes'
							&& project.Approval_of_Work_Done_by_Customer__c == 'Yes'));
		}
	}

	public Boolean isReadyForPhase5 {
		get{
			return phase4FieldsFilled ;
		}
	}

    public PageReference moveToPhase5(){
    	if(isProjectInPhase4 && isReadyForPhase5 && isProfileB2BCArchitectPartner){
			project.RecordTypeId = Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
			
			project.Status__c = 'Handover';
			
			project.Original_Installation_Date__c = project.Expected_Installation_Date_Confirmation__c;
			
			if(!project.Phase_4_Phase_5__c){
				project.Phase_4_Phase_5__c = true;
			}

			update project;
    	}
		return redirectToId(project.Id);
    }
    

	//********
	// Phase 5
	//********
	public PageReference sendHandoverReport(){
    	if(isProjectInPhase5 && project.Handover_Report_Sent__c != 'Yes' && hasAttachmentOfTypeHandover && isProfileB2BCArchitectPartner){
			project.Handover_Report_Sent__c = 'Yes';
			if(!project.Phase_5_Send_Handover_Report__c){
				project.Phase_5_Send_Handover_Report__c = true;
			}
			
			update project;
    	}
		return redirectToId(project.Id);
    }
    
    public PageReference rejectHandover(){
    	if(isProjectInPhase5 && project.Handover_Report_Sent__c == 'Yes' && isProfileB2BCMarket){
			project.Handover_Report_Sent__c = 'No';
			project.Handover_Approved__c = 'No';

			if(!project.Phase_5_Reject_Handover__c){
				project.Phase_5_Reject_Handover__c = true;
			}
			
			update project;
    	}
		return redirectToId(project.Id);
    }

	public Boolean phase5FieldsFilled {
		get{
			return project.Final_Installation_Date__c != null
					 && project.Handover_Report_Sent__c == 'Yes';
		}
	}

	public Boolean isReadyForPhase6 {
		get{
			return phase5FieldsFilled;
		}
	}
    public PageReference moveToPhase6(){
    	if(isProjectInPhase5 && isReadyForPhase6 && isProfileB2BCMarket){
			project.RecordTypeId = Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Invoicing').getRecordTypeId();
			
			project.Status__c = 'Invoicing';
			
			project.Handover_Approved__c = 'Yes';

			if(!project.Phase_5_Validate_Handover__c){
				project.Phase_5_Validate_Handover__c = true;
			}
			
			update project;
    	}
		return redirectToId(project.Id);
    }
    
    
	//********
	// Phase 6
	//********
	public PageReference closeProject(){
		if(isProjectInPhase6 && project.PO_Number_for_Delivery_Phase__c != null && isProfileB2BCMarket){
			project.RecordTypeId = Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Complete').getRecordTypeId();

			project.Status__c = 'Closed';
			
			if(!project.Phase_6_Phase_7_Close_Project__c){
				project.Phase_6_Phase_7_Close_Project__c = true;
			}
	
			update project;
    	}
		return redirectToId(project.Id);
	}
}
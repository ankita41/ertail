public with sharing class B2BCFileHandler {

	public class CustomException extends Exception{}

	public static void initFieldsBeforeInsert(List<B2BC_File_Area__c> newList){

		Profile userProfile = [Select Id, Name from Profile where Id=:UserInfo.getProfileId()];

		for(B2BC_File_Area__c file : newList){
			// If created by a Market, the ownership is transferred to its Queue
			if(userProfile.Name == 'B2BC Market'){
				B2BC_Project__c project = [Select Id, Owner.Id from B2BC_Project__c where Id =: file.B2BC_Project__c];
				file.OwnerId = project.Owner.Id;
			}
		}
	}
}
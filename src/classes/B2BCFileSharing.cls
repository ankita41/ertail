public without sharing class B2BCFileSharing {
	
	public class CustomException extends Exception{}
	
	public static void CalculateSharingAfterInsert(Map<Id, B2BC_File_Area__c> newMap){
		ShareB2BCFileAreaRecords(newMap);
	}
	
	public static void RecalculateSharingAfterUpdate(Map<Id, B2BC_Project__c> oldMap, Map<Id, B2BC_Project__c> newMap){
    	List<Id> projList = new List<Id>();
    	Map<Id, B2BC_File_Area__c> fileMap = new Map<Id, B2BC_File_Area__c>();
    
		for(Id projId : newMap.keySet()){
			if ((oldMap.get(projId).B2BC_Architect__c != newMap.get(projId).B2BC_Architect__c)
				|| (oldMap.get(projId).B2BC_Architect_Partner__c != newMap.get(projId).B2BC_Architect_Partner__c)
				|| (oldMap.get(projId).OwnerId != newMap.get(projId).OwnerId)){
				projList.add(projId);
			}
		}
		
		for(B2BC_File_Area__c fileArea : [select Id, RecordTypeId, RecordType.DeveloperName, OwnerId, B2BC_Project__r.B2BC_Architect__c, B2BC_Project__r.B2BC_Architect_Partner__c, B2BC_Project__r.OwnerId  from B2BC_File_Area__c where B2BC_Project__c in : projList]){
			fileMap.put(fileArea.Id, fileArea);
		}
		
		if (fileMap.size() > 0){
			DeleteExistingSharing(fileMap.keySet());
			ShareB2BCFileAreaRecords(fileMap);
		}
	}
	
	 public static void DeleteExistingSharing(Set<Id> fileAreaIdSet){
		// Locate all existing sharing records for the file area records in the batch.
		// Only records using an Apex sharing reason for this app should be returned. 
        List<B2BC_File_Area__Share> oldFileAreaShrs = [SELECT Id FROM B2BC_File_Area__Share WHERE ParentId IN :fileAreaIdSet AND 
             RowCause in (:Schema.B2BC_File_Area__Share.rowCause.IsArchitect__c 
			             ,:Schema.B2BC_File_Area__Share.rowCause.IsArchitectPartner__c
			             ,:Schema.B2BC_File_Area__Share.rowCause.IsMarket__c)]; 
        
		// Delete the existing sharing records.
		// This allows new sharing records to be written from scratch.
		Delete oldFileAreaShrs;
	}
	
	public static void ShareB2BCFileAreaRecords(Map<Id, B2BC_File_Area__c> fileMap){
		List<B2BC_File_Area__Share> fSharesToCreate = CreateSharingRecords(fileMap);    
		System.debug('insert FSharesToCreate:' + fSharesToCreate);
		Integer inserted = 0;
		if (fSharesToCreate.size() > 0){
			for(B2BC_File_Area__Share fShareToCreate : fSharesToCreate){
				try{
					insert fShareToCreate;
					inserted++;
				}
				catch(Exception e){
					throw new CustomException(e +'\n' + fShareToCreate);
				
				}
			}
		}
	}
		
	public static List<B2BC_File_Area__Share> CreateSharingRecords(Map<Id, B2BC_File_Area__c> fileAreaMap){
		List<B2BC_File_Area__Share> newFileAreaShrs = new List<B2BC_File_Area__Share>();
		// Construct new sharing records  
		for(B2BC_File_Area__c file : fileAreaMap.values()){
			B2BC_Project__c project = [Select Id, Owner.Id, B2BC_Architect__c, B2BC_Architect_Partner__c, OwnerId from B2BC_Project__c where Id =: file.B2BC_Project__c];
			
			if (project.B2BC_Architect__c != null && project.B2BC_Architect__c != file.OwnerId){
				newFileAreaShrs.add(CreateArchitectSharing(project.B2BC_Architect__c, file.Id));
			}
			
			if (project.B2BC_Architect_Partner__c != null && project.B2BC_Architect_Partner__c != file.OwnerId && file.RecordTypeId != Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Quotation').getRecordTypeId()){
				newFileAreaShrs.add(CreateArchitectPartnerSharing(project.B2BC_Architect_Partner__c, file.Id));
			}
			if (project.OwnerId != file.OwnerId){
				newFileAreaShrs.add(CreateMarketSharing(project.OwnerId, file.Id));
			}
		}
		return newFileAreaShrs;
	}
	
	private static B2BC_File_Area__Share CreateArchitectSharing(Id userId, Id fileId){
    	return new B2BC_File_Area__Share(UserOrGroupId = userId
										, AccessLevel = 'Read'
										, ParentId = fileId
										, RowCause = Schema.B2BC_File_Area__Share.RowCause.IsArchitect__c);
	}
	
	private static B2BC_File_Area__Share CreateArchitectPartnerSharing(Id userId, Id fileId){
    	return new B2BC_File_Area__Share(UserOrGroupId = userId
										, AccessLevel = 'Read'
										, ParentId = fileId
										, RowCause = Schema.B2BC_File_Area__Share.RowCause.IsArchitectPartner__c);
	}
	
	private static B2BC_File_Area__Share CreateMarketSharing(Id userId, Id fileId){
    	return new B2BC_File_Area__Share(UserOrGroupId = userId
										, AccessLevel = 'Read'
										, ParentId = fileId
										, RowCause = Schema.B2BC_File_Area__Share.RowCause.IsMarket__c);
	}
}
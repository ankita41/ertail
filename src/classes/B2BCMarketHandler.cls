public with sharing class B2BCMarketHandler {
	public class CustomException extends Exception{}

	public static void initFieldsBeforeUpsert(List<B2BC_Market__c> newList){
		for(B2BC_Market__c market : newList){			
			if(market.Market__c != null){
				Market__c mkt = [Select Name From Market__c where ID =: market.Market__c][0];
				market.Name = mkt.Name;
			}
		}
	}
}
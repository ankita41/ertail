public with sharing class B2BCOrderHandler {
	public class CustomException extends Exception{}

	public static void initFieldsBeforeInsert(List<B2BC_Order__c> newList){
		for(B2BC_Order__c b2bcOrder : newList){
			SKU__c sku = [Select Concept_Cost__c, Delivery_Costs__c, Drawings_Cost__c, Installation_Costs__c, Type__c, RecordType.Name From SKU__c Where Id = : b2bcOrder.SKU__c][0];

			b2bcOrder.Number_of_Items_Ordered__c = 1;
			b2bcOrder.Concept_Cost__c = sku.Concept_Cost__c;
			b2bcOrder.Delivery_Costs__c = sku.Delivery_Costs__c;
			b2bcOrder.Drawings_Cost__c = sku.Drawings_Cost__c;
			b2bcOrder.Installation_Costs__c = sku.Installation_Costs__c;
			
			b2bcOrder.Architect_Concept_Cost__c = sku.Concept_Cost__c;
			b2bcOrder.Architect_Delivery_Costs__c = sku.Delivery_Costs__c;
			b2bcOrder.Architect_Drawing_Cost__c = sku.Drawings_Cost__c;
			b2bcOrder.Architect_Installation_Costs__c = sku.Installation_Costs__c;
			b2bcOrder.Type__c = sku.Type__c;
		}
	}
}
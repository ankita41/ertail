public with sharing class B2BCProjectHandler {
	public class CustomException extends Exception{}

	public static void initFieldsBeforeInsert(List<B2BC_Project__c> newList){
		for(B2BC_Project__c project : newList){
			if(project.B2BC_Market__c != null){
				
				B2BC_Market__c b2bcMarket = [Select Owner.Id, Market__r.Id, B2BC_Architect__c, B2BC_Architect_Partner__c, Extra_Market_Cost__c, Site_Survey_Cost__c, X3D_Rendering_Cost__c, Project_Approver_Name__c, Project_Approver_Email__c From B2BC_Market__c Where Id = : project.B2BC_Market__c][0];

				project.OwnerId = b2bcMarket.Owner.Id;
				project.B2BC_Architect__c = b2bcMarket.B2BC_Architect__c;
				project.B2BC_Architect_Partner__c = b2bcMarket.B2BC_Architect_Partner__c;
				project.Site_Survey_Cost_Market__c = b2bcMarket.Site_Survey_Cost__c;
				project.Extra_Market_Cost__c = b2bcMarket.Extra_Market_Cost__c;
				project.X3D_Rendering_Cost_Market__c = b2bcMarket.X3D_Rendering_Cost__c;
				project.Project_Approver_Name__c = b2bcMarket.Project_Approver_Name__c;
				project.Project_Approver_Email__c = b2bcMarket.Project_Approver_Email__c;
				
				
				project.RecordTypeId = Schema.SObjectType.B2BC_Project__c.getRecordTypeInfosByName().get('Reality Check').getRecordTypeId();
				project.Status__c = 'Reality Check';

				List<Account> accounts = [Select Name From Account Where Id =: project.Agent__c];
				if(accounts.size() > 0){
					String agentName = accounts[0].Name.toUpperCase();
					if(agentName == 'NESPRESSO'){
						project.Is_Nespresso__c = 'Yes';
					}
					else{
						project.Is_Nespresso__c = 'No';
					}		
				}
			}
		}
	}
	
	public static void initFieldsBeforeUpdate(List<B2BC_Project__c> newList){
		for(B2BC_Project__c project : newList){	
			// the Is_Nespresso__c field will be used in dependent picklists
			String agentName = [Select Name From Account Where Id =: project.Agent__c][0].Name.toUpperCase();
			if(agentName == 'NESPRESSO'){
				project.Is_Nespresso__c = 'Yes';
			}
			else{
				project.Is_Nespresso__c = 'No';
				project.Type_of_Contract_Corner__c = null;
				project.Local_Currency__c = null;
				project.Local_Nespresso_Capsule_Price__c = null;
			}
		}
	}
}
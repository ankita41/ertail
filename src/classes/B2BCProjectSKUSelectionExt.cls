public with sharing class B2BCProjectSKUSelectionExt {

	public class CustomException extends Exception {}

	public B2BC_Project__c project { get; private set;}
	private Set<Id> SelectedSkuIdsSet;
	
	public class SelectableSkuWrapper{
		public SelectableSkuWrapper(SKU__c sku){
			this.sku = sku;
			this.Selected = false;
		}
		public SKU__c sku { get; set; }
		public Boolean Selected { get; set; }
	}
	
	private static Profile userProfile = [Select Id, Name from Profile where Id=:UserInfo.getProfileId()];
	
	
	public static Boolean isArchitectOrAdmin{
    	get { return (userProfile.Name == 'B2BC Architect' || userProfile.Name == 'System Admin'); }
    }
	
	public static Boolean isMarketOrHQOrAdmin{
    	get { return (userProfile.Name == 'B2BC Market' || userProfile.Name == 'B2BC HQ' || userProfile.Name == 'System Admin'); }
    }
	
	
	public List<B2BC_Order__c> selectedSkus { 
		get{
			if (selectedSkus == null){
				selectedSkus = new List<B2BC_Order__c>();
				SelectedSkuIdsSet.clear();
				for(B2BC_Order__c o : [Select c.Name
											, c.SKU__c
											, c.SKU__r.Name
											, c.SKU__r.Image_ID__c
											, c.SKU__r.Image_Small__c
											, c.SKU__r.Possible_Machines__c
											, c.SKU__r.Max_Number_of_Machines__c
											, c.SKU__r.RecordType.Name
											, c.Number_of_Items_Ordered__c
											, c.B2BC_Project__c 
											, c.Machine_to_be_installed__c
											, c.Number_of_Machines__c
											, c.Concept_Cost__c
											, c.Drawings_Cost__c
											, c.Delivery_Costs__c
											, c.Installation_Costs__c
											, c.Architect_Concept_Cost__c
											, c.Architect_Drawing_Cost__c
											, c.Architect_Delivery_Costs__c
											, c.Architect_Installation_Costs__c
											, c.Calculated_Total_Cost__c
											, c.Architect_Total_Cost__c
											, c.PAC_Comments__c
										From 
											B2BC_Order__c c 
										Where 
											B2BC_Project__c = :project.Id
										order by 
											SKU__r.RecordType.Name
											, SKU__r.Name]){
					selectedSkus.add(o);
					SelectedSkuIdsSet.add(o.SKU__c);
				}
			}
			return selectedSkus;
		}
		private set;
	}
	
	private List<SKU__c> allSku {
		get{
			if (allSku == null){
				allSku = [Select Id, RecordType.Name, Name, Image_Small__c from SKU__c order by RecordType.Name, Name];
			}			
			return allSku;
		}
		set;
	}
	
	public List<SelectableSkuWrapper> availableSkus {
		get{
			if (availableSkus == null){
				availableSkus = new List<SelectableSkuWrapper>();
				for (SKU__c s : allSku){
					if (!SelectedSkuIdsSet.contains(s.Id)){
						availableSkus.add(new SelectableSkuWrapper(s));
					}
				}
			}
			return availableSkus;
		}
		private set;
	}

    public B2BCProjectSKUSelectionExt(B2BC_Project__c project) {
       	this.project = project;
    	SelectedSkuIdsSet = new Set<Id>();
    }

    public B2BCProjectSKUSelectionExt(ApexPages.StandardController stdController) {
    	if(stdController != null){
        	this.project = (B2BC_Project__c)stdController.getRecord();
    	}
    	SelectedSkuIdsSet = new Set<Id>();
    }

	public PageReference deleteOrder(){
    	if (ApexPages.currentPage().getParameters().containsKey('cmtd')){
    		Id orderToDeleteId = ApexPages.currentPage().getParameters().get('cmtd');
    		delete new B2BC_Order__c(Id = orderToDeleteId);
    		resetSkusLists();
    	}
    	return null;
    }
    
	public PageReference addSkus(){
		List<B2BC_Order__c> skuToAdd = new List<B2BC_Order__c>();
		for(SelectableSkuWrapper skuWrapper : availableSkus){
			if (skuWrapper.Selected){
				skuToAdd.add(
					new B2BC_Order__c(
						Name = skuWrapper.sku.Name
						, SKU__c = skuWrapper.sku.Id
						, B2BC_Project__c = project.Id));
			}
		}
		
		if (skuToAdd.size() > 0){
			insert skuToAdd;			
			resetSkusLists();		
		}
		return null;
	}
	
	public PageReference backToProject(){
		return new PageReference('/' + project.Id);
	}

	public PageReference SaveSelectionAndBackToProject(){
		return save(true);
	}
	
	public void SaveSelection(){
		save(false);
	}
	
	public PageReference save(Boolean backToProject){
		Boolean errors = false;
		
		for(B2BC_Order__c b2bcOrder : selectedSkus){
			if(b2bcOrder.SKU__r.RecordType.Name == 'Module'){
				List<String> machines = b2bcOrder.SKU__r.Possible_Machines__c.split(';');
				
				Boolean machineOK = (b2bcOrder.Machine_to_be_Installed__c == null);
				for(String machine : machines){
					if(machine == b2bcOrder.Machine_to_be_Installed__c){
						machineOK = true;
						break;
					}
				}
	
				if(!machineOK){
					b2bcOrder.Machine_to_be_Installed__c.addError('The machine to be installed must belong to the list of selected machines in the chosen SKU : ' + b2bcOrder.SKU__r.Possible_Machines__c);
					errors = true;
				}
	
				if(b2bcOrder.Number_of_Machines__c != null && Integer.valueOf(b2bcOrder.Number_of_Machines__c) > Integer.valueOf(b2bcOrder.SKU__r.Max_Number_of_Machines__c)){
					b2bcOrder.Number_of_Machines__c.addError('The number of machines to be installed must not be higher than the Max number of machines defined in the chosen SKU : ' + b2bcOrder.SKU__r.Max_Number_of_Machines__c);
					errors = true;
				}
			}
			
			if((b2bcOrder.Concept_Cost__c != b2bcOrder.Architect_Concept_Cost__c ||
				b2bcOrder.Delivery_Costs__c != b2bcOrder.Architect_Delivery_Costs__c ||
				b2bcOrder.Drawings_Cost__c != b2bcOrder.Architect_Drawing_Cost__c ||
				b2bcOrder.Installation_Costs__c != b2bcOrder.Architect_Installation_Costs__c)
				&& (b2bcOrder.PAC_Comments__c == '' || b2bcOrder.PAC_Comments__c == null)){
					b2bcOrder.PAC_Comments__c.addError('If one of the unit price is changed by the Architect, the comment is mandatory.');
					errors = true;
			}
		}
		
		if(!errors){
			update selectedSkus;
			selectedSkus = null;
			
			if(backToProject){
				return backToProject();
			}
		}
		
		return null;		
	}
	
	private void resetSkusLists(){
		availableSkus = null;
		resetSelectedSkus();
	}
	
	private void resetSelectedSkus(){
		selectedSkus = null;
	}
}
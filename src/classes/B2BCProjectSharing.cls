public without sharing class B2BCProjectSharing {
	
	public static void CalculateSharingAfterInsert(Map<Id, B2BC_Project__c> newMap){
		ShareB2BCProjectRecords(newMap);
	}
	
	
	public static void RecalculateSharingAfterUpdate(Map<Id, B2BC_Project__c> oldMap, Map<Id, B2BC_Project__c> newMap){
    	Map<Id, B2BC_Project__c> projMap = new Map<Id, B2BC_Project__c>();
    
		for(Id projId : newMap.keySet()){
			if ((oldMap.get(projId).B2BC_Architect__c != newMap.get(projId).B2BC_Architect__c)
				|| (oldMap.get(projId).B2BC_Architect_Partner__c != newMap.get(projId).B2BC_Architect_Partner__c)
				|| (oldMap.get(projId).OwnerId != newMap.get(projId).OwnerId)){
				projMap.put(projId, newMap.get(projId));
			}
		}
		
		if (projMap.size() > 0){
			DeleteExistingSharing(projMap.keySet());
			ShareB2BCProjectRecords(projMap);
			B2BCFileSharing.RecalculateSharingAfterUpdate(oldMap, newMap);
		}
	}
	
	 public static void DeleteExistingSharing(Set<Id> projIdSet){
		// Locate all existing sharing records for the project records in the batch.
		// Only records using an Apex sharing reason for this app should be returned. 
        List<B2BC_Project__Share> oldProjectShrs = [SELECT Id FROM B2BC_Project__Share WHERE ParentId IN :projIdSet AND 
             RowCause in (:Schema.B2BC_Project__Share.rowCause.IsArchitect__c 
			             ,:Schema.B2BC_Project__Share.rowCause.IsArchitectPartner__c)]; 
        
		// Delete the existing sharing records.
		// This allows new sharing records to be written from scratch.
		if(oldProjectShrs != null){
			Delete oldProjectShrs;
		}
	}
	
	public static void ShareB2BCProjectRecords(Map<Id, B2BC_Project__c> projMap){
		List<B2BC_Project__Share> pSharesToCreate = CreateSharingRecords(projMap);    
		System.debug('insert pSharesToCreate:' + pSharesToCreate);                              
		if (pSharesToCreate.size() > 0){
			insert pSharesToCreate;
		}
	}
		
	public static List<B2BC_Project__Share> CreateSharingRecords(Map<Id, B2BC_Project__c> projectMap){
		List<B2BC_Project__Share> newprojectShrs = new List<B2BC_Project__Share>();
		// Construct new sharing records  
		for(B2BC_Project__c proj : projectMap.values()){
			if (proj.B2BC_Architect__c != null){
				newprojectShrs.add(CreateArchitectSharing(proj.B2BC_Architect__c, proj.Id));
			}
			if (proj.B2BC_Architect_Partner__c != null){
				newprojectShrs.add(CreateArchitectPartnerSharing(proj.B2BC_Architect_Partner__c, proj.Id));
			}
	    }
		return newprojectShrs;
	}
	
	private static B2BC_Project__Share CreateArchitectSharing(Id userId, Id projId){
    	return new B2BC_Project__Share(UserOrGroupId = userId
										, AccessLevel = 'Edit'
										, ParentId = projId
										, RowCause = Schema.B2BC_Project__Share.RowCause.IsArchitect__c);
	}
	
	private static B2BC_Project__Share CreateArchitectPartnerSharing(Id userId, Id projId){
    	return new B2BC_Project__Share(UserOrGroupId = userId
										, AccessLevel = 'Edit'
										, ParentId = projId
										, RowCause = Schema.B2BC_Project__Share.RowCause.IsArchitectPartner__c);
	}
}
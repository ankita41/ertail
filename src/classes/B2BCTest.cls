@isTest
public class B2BCTest {
	
	static testmethod void workflow(){
		
		Profile b2bcSysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
		Profile b2bcMarket = [SELECT Id FROM Profile WHERE Name = 'B2BC Market'][0];
		Profile b2bcArchitect = [SELECT Id FROM Profile WHERE Name = 'B2BC Architect'][0];
		Profile b2bcPartner = [SELECT Id FROM Profile WHERE Name = 'B2BC Architect Partner'][0];

		User adminUser = new User(
							LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = b2bcSysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
                             
		User marketUser = new User(
							LastName = 'test user 1', 
                             Username = 'b2bcmarket@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = b2bcMarket.Id, 
                             LanguageLocaleKey = 'en_US');
                             
		User architectUser = new User(
							LastName = 'test user 1', 
                             Username = 'b2bcarchitect@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = b2bcArchitect.Id, 
                             LanguageLocaleKey = 'en_US');
                             
		User partnerUser = new User(
							LastName = 'test user 1', 
                             Username = 'b2bcpartner@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = b2bcPartner.Id, 
                             LanguageLocaleKey = 'en_US');
                             
                             
                             
		insert new List<User> {adminUser, architectUser, partnerUser, marketUser};


		Market__c market;
		B2BC_Project__c project;
		B2BC_Market__c germanyMarket;
		Account customerAccount;
		Account agentAccount;
		Contact contact;

		System.runAs(adminUser){
		Test.startTest();
			market = new Market__c(Name='Germany', Code__c='GER');
			insert market;

			germanyMarket = new B2BC_Market__c(	Name = 'Germany',
																Market__c = market.Id,
																B2BC_Architect__c = architectUser.Id,
																B2BC_Architect_Partner__c = partnerUser.Id,
																Site_Survey_Cost__c = 975,
																X3D_Rendering_Cost__c = 200,
																Extra_Market_Cost__c = 300);
			insert germanyMarket;
			update germanyMarket;

			customerAccount = new Account(Name = 'Test Account', BillingStreet = 'Avenue des Baumettes 19', BillingPostalCode='12345', BillingCity='Renens', B2BC_Market__c = germanyMarket.Id, RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId());
			insert customerAccount;
			
			agentAccount = new Account(Name = 'Nespresso', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Owner').getRecordTypeId());
			insert agentAccount;
			
			contact = new Contact(FirstName='Achille', LastName='Talon');
			insert contact;

			project = new B2BC_Project__c(	Name = 'Test Project'
															, B2BC_Market__c = germanyMarket.Id
															, Subsidiary_or_Distributor_Market__c = 'Subsidiary'
															, KAM__c = 'kam'
															, Get_or_K_I__c = 'Get'
															, Channel__c = 'Office'
															, Customer_Name__c = customerAccount.Id
															, Agent__c = agentAccount.Id);
			insert project;

			

			B2BCButtonsController controller = new B2BCButtonsController();

			controller.projectId = project.Id;

			controller.project.Installation_Address__c = 'a';
			controller.project.Employees_Using_Corner_Daily_Basis__c = 10;
			controller.project.Replacement_or_New_Installation__c = 'Replacement';
			controller.project.Competitors__c = 'Competitiors';
			controller.project.Venue__c = 'Coffee Corner in Floors';
			controller.project.Pre_Existing_Consumption__c = 11;
			controller.project.Incremental_Consumption_After_Install__c = 12;
			controller.project.Local_Nespresso_Capsule_Price__c = 20;
			controller.project.Local_Currency__c = 'CHF';
			controller.project.Type_of_Contract_Corner__c = 'Sales (COGS)';


			System.assertEquals(controller.project.Agent__c != null, true);			
			System.assertEquals(controller.project.Subsidiary_or_Distributor_Market__c != null, true);
			System.assertEquals(controller.project.KAM__c != null, true);
			System.assertEquals(controller.project.Get_or_K_I__c != null, true);
			System.assertEquals(controller.project.Channel__c != null, true);
			System.assertEquals(controller.project.Customer_Name__c != null, true);
			System.assertEquals(controller.project.Address__c != null, true);
			System.assertEquals(controller.project.Zip_City__c != null, true);
			System.assertEquals(controller.project.Installation_Address__c != null, true);
			System.assertEquals(controller.project.Employees_Using_Corner_Daily_Basis__c != null, true);
			System.assertEquals(controller.project.Replacement_or_New_Installation__c != null, true);
			System.assertEquals((controller.project.Replacement_or_New_Installation__c == 'New Installation' || controller.project.Competitors__c != null), true);
			System.assertEquals(controller.project.Venue__c != null, true);
			System.assertEquals(controller.project.Pre_Existing_Consumption__c != null, true);
			System.assertEquals(controller.project.Incremental_Consumption_After_Install__c != null, true);
			System.assertEquals((controller.project.Local_Nespresso_Capsule_Price__c != null 
    					&& controller.project.Local_Currency__c != null
						&& controller.project.Estimated_Aditionnal_NNS__c != null) 
					|| controller.project.Is_Nespresso__c == 'Yes', true);

			System.assertEquals(controller.project.RecordType.DeveloperName == 'Creation', false);
			System.assertEquals(controller.isProjectInPhase0, true);
			System.assertEquals(controller.isReadyForPhase1, true);
			System.assertEquals(controller.isProfileB2BCMarket, true);
				
			controller.moveToPhase1();

			System.assertEquals(controller.isProjectInPhase1, true);
				
			SKU__c sku = new SKU__C(Name = 'Chair'
									, Unique_ID__c = '12345'
									, Description__c = 'Chair'
									, Max_Number_Of_Machines__c = '1'
									, Possible_Machines__c = 'Zenius'
									, Installation_Costs__c = 0
									, Delivery_Costs__c = 0
									, RecordTypeId = Schema.SObjectType.SKU__c.getRecordTypeInfosByName().get('Module').getRecordTypeId()
									, Concept_Cost__c = 199);
				
			insert sku;
			
			sku.Drawings_Cost__c = 0;
			update sku;
			
			B2BC_Order__c b2bcOrder = new B2BC_Order__c(Name = 'Order 1'
														, B2BC_project__c = project.Id
														, SKU__c = sku.Id
														, Machine_to_be_Installed__c = 'Zenius'
														, Number_of_Machines__c = '1'
														, Number_of_Items_Ordered__c = 4
														, Type__c = 'Accesssory');
				
			insert b2bcOrder;
			update b2bcOrder;
			
			controller.project.Desired_Installation_Date_From__c = System.Today();
			controller.project.Desired_Installation_Date_To__c = System.Today().addMonths(1);
			controller.project.Customized_Adaptation__c = 'Yes';
			controller.project.Site_Survey_Requested__c = 'Yes';
			controller.project.X3D_Renderings__c = 'Yes';
			controller.project.Water_Installation_Needed__c = 'Yes';
			controller.project.Electricity_Installation_Needed__c = 'Yes';
			controller.project.Dismantling_Existing_Corner_Needed__c = 'No';
			controller.project.Cleaning_Needed__c = 'No';


			System.assertEquals(controller.project.Desired_Installation_Date_From__c != null,true);
			System.assertEquals(controller.project.Desired_Installation_Date_To__c != null,true);
			System.assertEquals(controller.project.Customized_Adaptation__c != null,true);
			System.assertEquals((controller.project.Site_Survey_Requested__c != 'Yes' ||
    				(controller.project.Water_Installation_Needed__c != null
    				&& controller.project.Electricity_Installation_Needed__c != null
    				&& controller.project.Dismantling_Existing_Corner_Needed__c != null
    				&& controller.project.Cleaning_Needed__c != null)),true);

			System.assertEquals(controller.phase1FieldsFilled, true);

			List<B2BC_File_Area__c> fileAreaList= new List<B2BC_File_Area__c>();

			fileAreaList.add(new B2BC_File_Area__c(Name = 'Photo 1', B2BC_Project__c = project.Id, RecordTypeId = Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Photo').getRecordTypeId()));
			fileAreaList.add(new B2BC_File_Area__c(Name = 'Sketch 1', B2BC_Project__c = project.Id, RecordTypeId = Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Sketch').getRecordTypeId()));
			fileAreaList.add(new B2BC_File_Area__c(Name = 'Floor Plan 1', B2BC_Project__c = project.Id, RecordTypeId = Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Floor Plan or Equivalent').getRecordTypeId()));
			fileAreaList.add(new B2BC_File_Area__c(Name = 'Drawing 1', B2BC_Project__c = project.Id, RecordTypeId = Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Drawing').getRecordTypeId()));
			fileAreaList.add(new B2BC_File_Area__c(Name = '3D Layout 1', B2BC_Project__c = project.Id, RecordTypeId =Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('3D Layout').getRecordTypeId()));
			fileAreaList.add(new B2BC_File_Area__c(Name = 'Confirmed Drawings 1', B2BC_Project__c = project.Id, RecordTypeId =Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Confirmed Drawings').getRecordTypeId()));
			fileAreaList.add(new B2BC_File_Area__c(Name = 'Quotation', B2BC_Project__c = project.Id, RecordTypeId =Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Quotation').getRecordTypeId()));
			fileAreaList.add(new B2BC_File_Area__c(Name = 'Signed Drawings 1', B2BC_Project__c = project.Id, RecordTypeId =Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Signed Drawings').getRecordTypeId()));
			fileAreaList.add(new B2BC_File_Area__c(Name = 'Pictures of Work Done 1', B2BC_Project__c = project.Id, RecordTypeId =Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Pictures of Work Done').getRecordTypeId()));
			fileAreaList.add(new B2BC_File_Area__c(Name = 'Handover 1', B2BC_Project__c = project.Id, RecordTypeId =Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Handover').getRecordTypeId()));


			insert fileAreaList;
			
			project.ownerId = marketUser.Id;
			
			update project;

			System.runAs(marketUser){
				insert new B2BC_File_Area__c(Name = 'Photo 2', B2BC_Project__c = project.Id, RecordTypeId = Schema.SObjectType.B2BC_File_Area__c.getRecordTypeInfosByName().get('Photo').getRecordTypeId());
			}

			
			update fileAreaList;
			
			
			

			List<Attachment> attachmentsList = new List<Attachment>();
				
			attachmentsList.add(new Attachment(Name = 'Photo 1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(0).Id));
			attachmentsList.add(new Attachment(Name = 'Sketch 1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(1).Id));
			attachmentsList.add(new Attachment(Name = 'Floor Plan 1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(2).Id));
			attachmentsList.add(new Attachment(Name = 'Drawing 1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(3).Id));
			attachmentsList.add(new Attachment(Name = '3D Layout1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(4).Id));
			attachmentsList.add(new Attachment(Name = 'confirmed Drawings 1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(5).Id));
			attachmentsList.add(new Attachment(Name = 'Quotation 1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(6).Id));
			attachmentsList.add(new Attachment(Name = 'Signed Drawings 1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(7).Id));
			attachmentsList.add(new Attachment(Name = 'Pictures of Work Done 1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(8).Id));
			attachmentsList.add(new Attachment(Name = 'Handover 1', Body = EncodingUtil.base64Decode('toto'), ParentId = fileAreaList.get(9).Id));

			insert attachmentsList;

			System.assertEquals(controller.hasAttachmentOfType('Photo'), true);
			System.assertEquals(controller.hasAttachmentOfType('Sketch'), true);
			System.assertEquals(controller.hasAttachmentOfType('Floor Plan or Equivalent'), true);
			System.assertEquals(controller.hasAttachmentOfType('Drawing'), true);
			System.assertEquals(controller.hasAttachmentOfType('3D Layout'), true);
			System.assertEquals(controller.hasAttachmentOfType('Confirmed Drawings'), true);
			System.assertEquals(controller.hasAttachmentOfType('Quotation'), true);
			System.assertEquals(controller.hasAttachmentOfType('Signed Drawings'), true);
			System.assertEquals(controller.hasAttachmentOfType('Pictures of Work Done'), true);
			System.assertEquals(controller.hasAttachmentOfType('Handover'), true);

			System.assertEquals(controller.isReadyForPhase2, true);
			System.assertEquals(controller.isProjectInPhase1, true);
			System.assertEquals(controller.isProfileB2BCMarket, true);
				
			controller.moveToPhase1();
			
			controller.moveToPhase2();

			controller.project.Lack_of_Information__c = 'lack';
			controller.project.Architect_Installation_Date_From__c = System.Today();
			controller.project.Architect_Installation_Date_To__c = System.Today().addMonths(1);
			
			controller.requireAdditionalInformationFromMarket();
			controller.sendAdditionalInformationToArchitect();
			controller.notifyPONumberToArchitect();
		
			System.assertEquals(controller.isProjectInPhase2, true);
			System.assertEquals(controller.isReadyForPhase3, true);
			System.assertEquals(controller.isProfileB2BCArchitect, true);


			controller.moveToPhase3();

			controller.disapproveQuote();

			controller.moveToPhase3();

			controller.approveQuote();
			
			controller.project.Offer_Approved__c = 'Yes';
			controller.project.PO_Number__c = 123;
			controller.project.Drawings_Approved_by_Nespresso__c = 'Yes';
			controller.project.Preparation_Work_on_Site_Requested__c = 'No';
//			controller.project.Contact_for_the_Installation__c = contact.Id;
			controller.project.Contact_for_the_Installation_Text__c = 'Atchoum';
			controller.project.Contact_Phone_Number__c = '123';

			System.assertEquals(controller.isProjectInPhase3, true);
			System.assertEquals(controller.isReadyForPhase4, true);
			System.assertEquals(controller.isProfileB2BCMarket, true);
				
			controller.moveToPhase4();
			
			controller.project.Works_Validated__c = null;
			
//				controller.validateWorks();
				
			controller.project.Expected_Installation_Date_Confirmation__c = System.today();
			controller.project.Prepartion_Work_Onsite_Done_Confirmation__c = 'Yes';
			controller.project.Approval_of_Work_Done_by_Customer__c = 'Yes';
				
			System.assertEquals(controller.isProjectInPhase4, true);
			System.assertEquals(controller.isReadyForPhase5, true);
			System.assertEquals(controller.isProfileB2BCArchitectPartner, true);
			
			
			Test.stopTest();
			
			
			controller.moveToPhase5();
		
			controller.sendHandoverReport();
			
			controller.rejectHandover();
			
			controller.project.Final_Installation_Date__c = System.today();
			controller.project.Handover_Report_Sent__c = 'Yes';
				
			System.assertEquals(controller.isProjectInPhase5, true);
			System.assertEquals(controller.isReadyForPhase6, true);
			System.assertEquals(controller.isProfileB2BCMarket, true);
				
			controller.moveToPhase6();
			
			controller.project.PO_Number_for_Delivery_Phase__c = '123';
				
			controller.closeProject();
			
			controller.hasAttachmentOfType('100%');

			
			
			B2BCProjectSKUSelectionExt skuSelectionController = new B2BCProjectSKUSelectionExt(project);
				
			List<B2BC_Order__c> selectedSkus = skuSelectionController.selectedSkus;

			System.assertEquals(selectedSkus.size() > 0, true);

			Test.setCurrentPageReference(new PageReference('/apex/test?cmtd=' + b2bcOrder.Id));			
				
			skuSelectionController.deleteOrder();
				

							
			selectedSkus = skuSelectionController.selectedSkus;
			System.assertEquals(selectedSkus.size() == 0, true);
							
			skuSelectionController.availableSkus[0].Selected=true;
			
			skuSelectionController.addSkus();

			skuSelectionController.SaveSelection();
			
			B2BCFileSharingRecalc fileRecalc = new B2BCFileSharingRecalc();
		
			Database.executeBatch(fileRecalc);


			B2BCProjectSharingRecalc recalc = new B2BCProjectSharingRecalc();
		
			Database.executeBatch(recalc);
		
		}
		


		
	}
	

}
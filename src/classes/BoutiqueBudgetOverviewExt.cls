/*
*   @Class      :   BoutiqueBudgetOverviewExt.cls
*   @Description:   Extension class of the BoutiqueBudgetOverview VF page
*   @Author     :   Jacky Uy
*   @Created    :   17 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy        18 APR 2014     Change: Added new columns required to be displayed
*   Jacky Uy        02 MAY 2014     Fix: Included Expected Costs on query
*   Chirag          3 June 15        PO Total Rollup                                           
*/

public class BoutiqueBudgetOverviewExt{
    public Map<Decimal,Budget_Overview__c> mapBudgetOverview {get;set;}
    public List<Budget_Overview__c> budgetOverviewList {get;set;}
    public Budget_Overview__c overallTotal {get;set;}
    public Decimal posum {get;set;}
    public Decimal posumchf {get;set;}
    public Integer posize {get; set;}
    public Id parentId;
    public List<Estimate_Item__c> recoEstiList{get;set;}
    public List<Cost_Item__c> recoCostItemList{get;set;}
    
    public BoutiqueBudgetOverviewExt(ApexPages.StandardController controller) {
        parentId = controller.getRecord().Id;
        mapBudgetOverview = new Map<Decimal,Budget_Overview__c>();
        budgetOverviewList = new List<Budget_Overview__c>();
        recoEstiList=new List<Estimate_Item__c>();
        recoCostItemList=new List<Cost_Item__c>();
        showEstimatedItems();
        showCostItems();
        
        for(Budget_Overview__c bo : [
                SELECT Budget_Item__c, Estimate_local__c, Estimate_CHF__c, Estimate_per_M2__c, Confirmed_Cost_local__c, Order__c,
                    Confirmed_Cost_CHF__c, Confirmed_Cost_per_M2__c, Expected_vs_Cost_Diff__c, Budget_Available__c, Indicator__c,
                    Expected_Cost_local__c, Expected_Cost_CHF__c, Total_Cost_local__c, Total_Cost_CHF__c, Budget_Available_Private__c,
            Confirmed_Cost_CHF_Private__c, Confirmed_Cost_local_Private__c, Confirmed_Cost_per_M2_Private__c, Estimate_CHF_Private__c, 
            Estimate_local_Private__c, Estimate_per_M2_Private__c, Expected_Cost_CHF_Private__c, Expected_Cost_local_Private__c,
            Indicator_Private__c, Ratio_of_Confirmed_Costs_Private__c, Total_Cost_CHF_Private__c, Total_Cost_local_Private__c
                FROM Budget_Overview__c WHERE Boutique_Project__c = :parentId ORDER BY Order__c
            ]){
            
            if(bo.Budget_Item__c!=null && bo.Budget_Item__c!='Total')
                mapBudgetOverview.put(bo.Order__c,bo);
            else if(bo.Budget_Item__c=='Total')
                overallTotal = bo;
                
            budgetOverviewList.add(bo);
        }
        //Added by Chirag Ref RM0018455868 
        AggregateResult[] groupedResults= [SELECT SUM(Amount__c)asum, SUM(Amount_CHF__c)asumchf, COUNT(Id)posize FROM Purchase_Order__c WHERE RECO_Project__c =: parentId];
        //Object po = groupedResults[0].get('asum');
        if(groupedResults!= null && groupedResults.size()>0){
            If (groupedResults[0].get('asum') != NULL){ 
                String sposum = String.valueof(groupedResults[0].get('asum'));
                posum = Decimal.valueOf(sposum);
            }
            If (groupedResults[0].get('asumchf') != NULL){
                String sposumchf = String.valueof(groupedResults[0].get('asumchf')); 
                posumchf = Decimal.valueOf(sposumchf);
            }
            posize = Integer.valueof(groupedResults[0].get('posize'));
        }  
    }
    public void showEstimatedItems()
    {
         Id profileId=userinfo.getProfileId();
         String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
         if(profileName!=Label.Design_Architect_Profile && profileName!=Label.Furniture_Supplier_Profile && profileName!=Label.Manager_Architect_Profile ){
              recoEstiList=[Select e.SystemModstamp, e.RecordTypeId, e.RECO_Project__c, e.Project_Currency__c, 
                                                     e.Private__c, e.Percentage_Total_cost__c, e.Name, e.Level_3__c, e.Level_2__c, 
                                                     e.Level_1__c, e.LastReferencedDate, e.LastModifiedDate, 
                                                     e.LastModifiedById, e.IsPrivate__c, e.IsDeleted, e.Id, e.Estimated_Item__c, 
                                                     e.Do_Not_Convert__c, e.Description__c, e.Currency__c, e.CreatedDate, 
                                                     e.CreatedById, e.Cost_m2_local__c, e.Cost_m2_EUR__c, e.Cost_m2_CHF__c, 
                                                     e.Amount_local__c, e.Amount__c, e.Amount_EUR__c, e.Amount_CHF__c From Estimate_Item__c e
                                                     where RECO_Project__c=:parentId order by Level_2_Formula__c asc];
          }
          else{
              recoEstiList=[Select e.SystemModstamp, e.RecordTypeId, e.RECO_Project__c, e.Project_Currency__c, 
                                                     e.Private__c, e.Percentage_Total_cost__c, e.Name, e.Level_3__c, e.Level_2__c, 
                                                     e.Level_1__c,  e.LastReferencedDate, e.LastModifiedDate, 
                                                     e.LastModifiedById, e.IsPrivate__c, e.IsDeleted, e.Id, e.Estimated_Item__c, 
                                                     e.Do_Not_Convert__c, e.Description__c, e.Currency__c, e.CreatedDate, 
                                                     e.CreatedById, e.Cost_m2_local__c, e.Cost_m2_EUR__c, e.Cost_m2_CHF__c, 
                                                     e.Amount_local__c, e.Amount__c, e.Amount_EUR__c, e.Amount_CHF__c From Estimate_Item__c e
                                                     where RECO_Project__c=:parentId and private__c=false order by Level_2_Formula__c asc];
          
          
          }
    }
    
    public void showCostItems()
    {
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        if(profileName!=Label.Design_Architect_Profile && profileName!=Label.Furniture_Supplier_Profile && profileName!=Label.Manager_Architect_Profile ){
                        recoCostItemList=[Select c.Validated_amount_local__c,RecordType.Name,Offer__r.Supplier__r.Name, c.Validated_amount__c, c.Validated_amount_EUR__c, 
                                                    c.Validated_amount_CHF__c, c.Valid__c, c.Unique_External_Id__c, c.SystemModstamp, 
                                                    c.Supplier__c, c.Start_date__c, c.RecordTypeId, c.RECO_Project__c, c.Private__c, 
                                                    c.Percentage_Total_cost__c, c.Offer__c, c.Offer_Approved_Year__c, c.Name, c.Local_Currency__c, 
                                                    c.Level_3__c, c.Level_2__c, c.Level_1__c, 
                                                    c.LastModifiedDate, c.LastModifiedById, c.LastActivityDate, c.IsPrivate__c, 
                                                    c.IsDeleted, c.Id, c.Extra_cost__c, c.Expected_amount_local__c, c.Expected_amount__c, 
                                                    c.Expected_amount_EUR__c, c.Expected_amount_CHF__c, c.End_delivery_date__c, c.Do_Not_Convert__c, 
                                                    c.Description__c, c.Cutoff_Years__c, c.Currency__c, c.CreatedDate, c.CreatedById, 
                                                    c.Cost_m2_local__c, c.Cost_m2_EUR__c, c.Cost_m2_CHF__c, c.Cost_Item__c, c.Amount__c 
                                                    From Cost_Item__c c where RECO_Project__c=:parentId order by Level_2_Formula__c asc nulls last]; 
        }
        else{
                        recoCostItemList=[Select c.Validated_amount_local__c,RecordType.Name,Offer__r.Supplier__r.Name, c.Validated_amount__c, c.Validated_amount_EUR__c, 
                                                    c.Validated_amount_CHF__c, c.Valid__c, c.Unique_External_Id__c, c.SystemModstamp, 
                                                    c.Supplier__c, c.Start_date__c, c.RecordTypeId, c.RECO_Project__c, c.Private__c, 
                                                    c.Percentage_Total_cost__c, c.Offer__c, c.Offer_Approved_Year__c, c.Name, c.Local_Currency__c, 
                                                    c.Level_3__c, c.Level_2__c, c.Level_1__c, 
                                                    c.LastModifiedDate, c.LastModifiedById, c.LastActivityDate, c.IsPrivate__c, 
                                                    c.IsDeleted, c.Id, c.Extra_cost__c, c.Expected_amount_local__c, c.Expected_amount__c, 
                                                    c.Expected_amount_EUR__c, c.Expected_amount_CHF__c, c.End_delivery_date__c, c.Do_Not_Convert__c, 
                                                    c.Description__c, c.Cutoff_Years__c, c.Currency__c, c.CreatedDate, c.CreatedById, 
                                                    c.Cost_m2_local__c, c.Cost_m2_EUR__c, c.Cost_m2_CHF__c, c.Cost_Item__c, c.Amount__c 
                                                    From Cost_Item__c c where RECO_Project__c=:parentId and private__c=false order by Level_2_Formula__c  asc nulls last]; 
        
        }
    
    }
    
    public void savePDF()
    {
       Reco_SavePDFToFolder.createPDF(parentId);
    }
    
}
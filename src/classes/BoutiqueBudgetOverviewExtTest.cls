/*****
*   @Class      :   BoutiqueBudgetOverviewExtTest.cls
*   @Description:   Test coverage for the BoutiqueBudgetOverviewExt.cls
*   @Author     :   Jacky Uy
*   @Created    :   29 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        29 APR 2014     BoutiqueBudgetOverviewExt.cls Test Coverage at 100%
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*
*****/

@isTest
private with sharing class BoutiqueBudgetOverviewExtTest{
    static testMethod void unitTest(){
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        PageReference pageRef = Page.BoutiqueBudgetOverview;
        pageRef.getParameters().put('id', bProj.Id);
        Test.setCurrentPage(pageRef);
        Offer__c offer = new Offer__c(RECO_Project__c = bProj.Id);
        insert offer;
        
        checkRecursive.runOnce();
        Attachment_Folder__c mainFolder = new Attachment_Folder__c(name = 'Budget & Offer & Cost',Offer__c=offer.Id,RECO_Project__c=bProj.id);
        insert mainFolder;
        
        ApexPages.StandardController con = new ApexPages.StandardController(bProj);
        BoutiqueBudgetOverviewExt ext = new BoutiqueBudgetOverviewExt(con);
        ext.savePDF();
        
        SYSTEM.Assert(!ext.mapBudgetOverview.isEmpty());
        SYSTEM.Assert(ext.overallTotal!=null);
    }
}
/*****
*   @Class      :   BoutiqueCostComparisonExt.cls
*   @Description:   Controller for the BoutiqueCostComparison page
*   @Author     :   Promila Boora
*   @Created    :   15 MARCH 2016
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   
*****/

public without sharing class BoutiqueCostComparisonExt{
    
    /***Below are filter variables *****/
    public String reportYear {get;set;}
    public List<SelectOption> marketOpts {get;set;}
    public String markets {get;set;}
    public List<SelectOption> yearOpts {get;set;}
    public String btqType{get;set;}
    public List<SelectOption> btqTypeOpts {get;set;}
    /***Filter variables ends here ****/
    String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
    public String qryStr;
    public List<MarketCostWrapper>avgCostCurrentPrj{get;set;}
    public List<MarketCostWrapper>avgDataSelectedMktYr{get;set;}
    RECO_Project__c rcPrjRecord;
    public List<MktCostTableWrapper>table1Data{get;set;}
    public List<String>allLevel1Vals{get;set;}
    public list<String>allLevel1ValsTable2{get;set;}
    List<MktCostTableWrapper>table1DataRow1;
    Map<String,Map<String,Decimal>>mapTable2;
    public Map<String,Decimal>mapTable2LastRowOfAvg{get;set;}
    public Map<String,RECO_Project__c>mapRecPrj{get;set;}
    public List<MarketCostWrapper>mapLevel2FurnGraph{get;set;}
    Map<String,Decimal>initialMapVal=new Map<String,Decimal>();
    public list<MktPrjTableWrapper>prjTable2Wrapper{get;set;}
    public list<MktPrjTableWrapper>prjTable3Wrapper{get;set;}
    Map<String,String>budItemMap;
    public Boolean isNotMainPage{get;set;}
    
     public BoutiqueCostComparisonExt(ApexPages.StandardController stdController) {
        table1DataRow1=new List<MktCostTableWrapper>();
        prjTable3Wrapper = new list<MktPrjTableWrapper>();
        RECO_Project__c rcPrj = (RECO_Project__c)stdController.getRecord();
        /***********BELOW CODE ADDED to render graph and button on Main Page**************/
        String sourceMainPage = ApexPages.currentPage().getParameters().get('source');
        if(sourceMainPage==null){
            isNotMainPage=false;
        }
        else{
            isNotMainPage=true;
        }
        /*************************/
        rcPrjRecord=[Select id,Cutoff_Year_1__c, Boutique__r.typology__c,Boutique__r.boutique_location_account__r.Market__r.Name from RECO_Project__c where id=:rcPrj.Id];
        reportYear=rcPrjRecord.Cutoff_Year_1__c; //String.valueOf(SYSTEM.now().year());
        markets=rcPrjRecord.Boutique__r.boutique_location_account__r.Market__r.Name;
        /*if(rcPrjRecord.Boutique__r.typology__c=='Popup Indoor' || rcPrjRecord.Boutique__r.typology__c=='Popup Outdoor'){
            btqType='Popup';}
        else if(rcPrjRecord.Boutique__r.typology__c=='N Cube Indoor' || rcPrjRecord.Boutique__r.typology__c=='N Cube Outdoor'){
            btqType='N Cube';}
        else*/
        
        btqType=rcPrjRecord.Boutique__r.typology__c;
            
         /**** GRAPH 1 for CURRENT PROJECT DATA starts here ******/
        avgCostCurrentPrj=new List<MarketCostWrapper>();
        String prjId=rcPrjRecord.Id;
        String prjName;
        Integer count=0;
        Map<String,Decimal>mapCurrentPrjCost=new Map<String,Decimal>();
        allLevel1Vals= new List<String>();
        budItemMap=new Map<String,String>();
        for(Custom_Order_Level1__c coLevel1 : [Select Name,Order__c,Level1__c from Custom_Order_Level1__c Order by Order__c]){
            allLevel1Vals.add(coLevel1.Level1__c);
            budItemMap.put(coLevel1.Name,coLevel1.Level1__c);
        } 
        qryStr='SELECT Budget_Item__c, Boutique_Project__r.Name,Order__c,Total_Cost_CHF__c,Confirmed_Cost_per_M2__c FROM Budget_Overview__c WHERE Total_Cost_CHF__c!=0 AND Budget_Item__c!=\'Total\' AND Boutique_Project__c = :prjId ORDER BY Budget_Item__c';
        for(Budget_Overview__c budOvw : Database.query(qryStr)){
            if(count==0){
                prjName=budOvw.Boutique_Project__r.Name;
            }
            count++;
            mapCurrentPrjCost.put(budItemMap.get(budOvw.Budget_Item__c),budOvw.Confirmed_Cost_per_M2__c);
            initialMapVal.put(budItemMap.get(budOvw.Budget_Item__c),0.0);
       }
       for(String budItem:budItemMap.values()){
          
         if(!mapCurrentPrjCost.containsKey(budItem)){
             mapCurrentPrjCost.put(budItem,0.0);
         }
         avgCostCurrentPrj.add(new MarketCostWrapper(budItem+'\n'+String.valueOf(mapCurrentPrjCost.get(budItem)),mapCurrentPrjCost.get(budItem)));
       }
       //system.debug('==allLevel1Vals=='+allLevel1Vals);
       //system.debug('=mapCurrentPrjCost in constr==='+mapCurrentPrjCost);
       //system.debug('=initialMapVal in constr==='+initialMapVal);
        table1DataRow1.add(new MktCostTableWrapper(prjName,mapCurrentPrjCost)); //added first row in temprorary table
        /**** GRAPH 1 for CURRENT PROJECT DATA ends here ******/
        queryMarkets();
        queryYears();
        queryBtqType();
        queryData();
    }
    private void queryMarkets(){
        String qryStr = 'SELECT Name FROM Market__c ';
        marketOpts = new List<SelectOption>();
        for(Market__c m : Database.query(qryStr)){
            marketOpts.add(new SelectOption(m.Name, m.Name));
        }
        marketOpts.sort();
        
    }
    private void queryYears(){
        Integer currYear = SYSTEM.now().year();
        yearOpts = new List<SelectOption>();
        yearOpts.add(new SelectOption(String.valueOf(currYear-3), String.valueOf(currYear-3)));
        yearOpts.add(new SelectOption(String.valueOf(currYear-2), String.valueOf(currYear-2)));
        yearOpts.add(new SelectOption(String.valueOf(currYear-1), String.valueOf(currYear-1)));
        yearOpts.add(new SelectOption(String.valueOf(currYear), String.valueOf(currYear))); 
        yearOpts.add(new SelectOption(String.valueOf(currYear+1), String.valueOf(currYear+1)));    
    }
    private void queryBtqType(){
        btqTypeOpts =new List<SelectOption>();
        if(!currentUserProfile.contains('NCAFE')){
            if(rcPrjRecord.Boutique__r.typology__c=='Boutique'){
                btqTypeOpts.add(new SelectOption('All','All'));
                btqTypeOpts.add(new SelectOption(rcPrjRecord.Boutique__r.typology__c,rcPrjRecord.Boutique__r.typology__c));
            }
            else {
                btqTypeOpts.add(new SelectOption(rcPrjRecord.Boutique__r.typology__c,rcPrjRecord.Boutique__r.typology__c));
            }
        }
        else{
           btqTypeOpts.add(new SelectOption('Standard','Standard'));
        }
    }
    public void queryData(){
      //try{
        prjTable3Wrapper = new list<MktPrjTableWrapper>();
        Map<String,Decimal>mapCurrentPrjCost=new Map<String,Decimal>();
        Map<String,Decimal>mapPrjCostDiff=new Map<String,Decimal>();
        Map<String,Decimal>mapPrjCostPercentage=new Map<String,Decimal>();
        table1Data=new List<MktCostTableWrapper>();
        table1Data.add(table1DataRow1[0]); //added first row in table
        String ncafeRecordtypeId=Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
        /** Below Code is calculating Second row of table1 ***/
        qryStr='SELECT Budget_Item__c Name,Sum(Total_Cost_CHF__c),Sum(Confirmed_Cost_per_M2__c),Count(Boutique_Project__c) FROM Budget_Overview__c WHERE Total_Cost_CHF__c!=0 AND Confirmed_Cost_per_M2__c!=0 AND Budget_Item__c!=\'Total\' AND Boutique_Project__r.Boutique__r.boutique_location_account__r.Market__r.Name= :markets AND Boutique_Project__r.Cutoff_Year_1__c= :reportYear ';
        if(btqType!='All')
               qryStr+='AND Boutique_Project__r.Boutique__r.typology__c= :btqType';
          if(currentUserProfile.contains('NCAFE')){
                 qryStr += ' AND Boutique_Project__r.RecordtypeId = :ncafeRecordtypeId';
          }
          else{
                 qryStr += ' AND Boutique_Project__r.RecordtypeId != :ncafeRecordtypeId';
                 if(btqType=='All'){
                     qryStr+=' AND (Boutique_Project__r.Boutique__r.typology__c!=\'Standard\' OR Boutique_Project__r.Boutique__r.typology__c!=\'Voltaire\')';
                 }
          }
          qryStr+=' GROUP BY Budget_Item__c';
          //system.debug('==qryStr =='+qryStr );
          avgDataSelectedMktYr=new List<MarketCostWrapper>();
          
          mapCurrentPrjCost=initialMapVal.clone();
          for(AggregateResult ar : Database.query(qryStr)){
             Decimal avg=(Decimal.valueOf(Double.valueOf(ar.get('expr1'))))/(Decimal.valueOf(Double.valueOf(ar.get('expr2'))));
             avg=avg.setScale(2, RoundingMode.HALF_UP);
             //avgDataSelectedMktYr.add(new MarketCostWrapper(String.valueOf(ar.get('Name')),avg)); //commented and put it in line 198 for ordering of items
             //Below map is for Row 2 of Table 1
             //mapCurrentPrjCost.put(String.valueOf(ar.get('Name')),Decimal.valueOf(Double.valueOf(ar.get('expr1')))); //need to check with Virginia whether we need to take same calculation as for graph2
             mapCurrentPrjCost.put(budItemMap.get(String.valueOf(ar.get('Name'))),avg);
             initialMapVal.put(budItemMap.get(String.valueOf(ar.get('Name'))),0.0);
          }
         for(String budItem:budItemMap.values()){
             if(!mapCurrentPrjCost.containsKey(budItem)){
                 mapCurrentPrjCost.put(budItem,0.0);
             }
             avgDataSelectedMktYr.add(new MarketCostWrapper(budItem+'\n'+string.valueOf(mapCurrentPrjCost.get(budItem)),mapCurrentPrjCost.get(budItem)));
         }
          //system.debug('====mapCurrentPrjCost==='+mapCurrentPrjCost);
          table1Data.add(new MktCostTableWrapper('Average',mapCurrentPrjCost)); // Added Second row
          
          /** Below Code is calculating Third row of table1 ***/
         // allLevel1Vals= new List<String>();
          
          for(String level1:table1Data[0].mapCalc.KeySet()){
              if(table1Data[1].mapCalc.containsKey(level1)){
                  mapPrjCostDiff.put(level1,math.abs(table1Data[1].mapCalc.get(level1)-table1Data[0].mapCalc.get(level1)));
              }
             // allLevel1Vals.add(level1);
              initialMapVal.put(level1,0.0);
              
          }
          if(table1Data[1].mapCalc.size()>=table1Data[0].mapCalc.size()){
              for(String level1:table1Data[1].mapCalc.KeySet()){
                  if(!table1Data[0].mapCalc.containsKey(level1)){
                      mapPrjCostDiff.put(level1,table1Data[1].mapCalc.get(level1));
                      table1Data[0].mapCalc.put(level1,0.0);
                      //allLevel1Vals.add(level1);
                      initialMapVal.put(level1,0.0);
                  }
              }
          }
          //allLevel1Vals.sort();
          table1Data.add(new MktCostTableWrapper('Gap',mapPrjCostDiff)); // Added third row
          //system.debug('====table1Data==='+table1Data);
          //system.debug('====initialMapVal==='+initialMapVal);
          /** Below Code is calculating fourth row of table1 ***/
          mapCurrentPrjCost=new Map<String,Decimal>();
          
          for(String level1:table1Data[2].mapCalc.KeySet()){
              if(table1Data[0].mapCalc.get(level1)!=0){
                  Decimal avgDiff=table1Data[2].mapCalc.get(level1)/table1Data[0].mapCalc.get(level1);
                  mapCurrentPrjCost.put(level1,avgDiff.setScale(2, RoundingMode.HALF_UP));}
              else
                  mapCurrentPrjCost.put(level1,0.0);
          }
          table1Data.add(new MktCostTableWrapper('Gap in %',mapCurrentPrjCost)); // Added fourth row
          
          /***** Below Code is to contruct table2 ***/
         mapRecPrj = new Map<String,RECO_Project__c>(); //MAP of RECO Projects
         
          //qryStr='SELECT Boutique_Project__r.Name btqName,Boutique_Project__r.Id btqId, Boutique_Project__r.OpeningFormula__c btqOpendate,Budget_Item__c Name,Sum(Total_Cost_CHF__c),Sum(Confirmed_Cost_per_M2__c),Count(Boutique_Project__c) FROM Budget_Overview__c WHERE Total_Cost_CHF__c!=0 AND Budget_Item__c!=\'Total\' AND Boutique_Project__r.Boutique__r.boutique_location_account__r.Market__r.Name= :markets AND Boutique_Project__r.Cutoff_Year_1__c= :reportYear ';
          prjTable2Wrapper= new list<MktPrjTableWrapper>();
          qryStr='Select Name,Id,OpeningFormula__c,Boutique__r.typology__c,Project_Type__c, Status__c from RECO_Project__c WHERE ';
                qryStr+='Boutique__r.boutique_location_account__r.Market__r.Name= :markets AND Cutoff_Year_1__c= :reportYear ';
                if(btqType!='All')
                    qryStr+='AND Boutique__r.typology__c= :btqType';
                if(currentUserProfile.contains('NCAFE')){
                    qryStr += ' AND RecordtypeId = :ncafeRecordtypeId';
                }
                else{
                    qryStr += ' AND RecordtypeId != :ncafeRecordtypeId';
                    if(btqType=='All'){
                     qryStr+=' AND (Boutique__r.typology__c!=\'Standard\' OR Boutique__r.typology__c!=\'Voltaire\')';
                    }
                }
                
            for(RECO_Project__c rcPrj:Database.query(qryStr)){
                mapRecPrj.put(rcPrj.Id,rcPrj);
                //prjTable2Wrapper.add(new MktPrjTableWrapper(false,rcPrj.Id,rcPrj));
            }
         // allLevel1ValsTable2=new List<String>{'Fees','Building Works','Nespresso supplied Items','Furniture/Millwork'};
          mapTable2=new Map<String,Map<String,Decimal>>();  
          Map<String,Decimal>initialMapValTemp=new Map<String,Decimal>(); 
          Map<String,Decimal>checkForAllZeroPrgMap=new Map<String,Decimal>();
          for(Budget_Overview__c budOverW:[SELECT Budget_Item__c,Boutique_Project__c,Confirmed_Cost_per_M2__c FROM Budget_Overview__c where Budget_Item__c!='Total' AND Boutique_Project__c IN :mapRecPrj.keySet() Order by Boutique_Project__r.Name]){
             //if(budOverW.Budget_Item__c=='Fees' || budOverW.Budget_Item__c=='Building Works' || budOverW.Budget_Item__c=='Nespresso supplied Items' || budOverW.Budget_Item__c=='Furniture/Millwork'){
              if(mapTable2.containsKey(budOverW.Boutique_Project__c)){
                  if(initialMapValTemp.containsKey(budItemMap.get(budOverW.Budget_Item__c))){
                     initialMapValTemp=mapTable2.get(budOverW.Boutique_Project__c);
                      initialMapValTemp.put(budItemMap.get(budOverW.Budget_Item__c),budOverW.Confirmed_Cost_per_M2__c);
                      mapTable2.put(budOverW.Boutique_Project__c,initialMapValTemp);
                  }
                Decimal total=checkForAllZeroPrgMap.get(budOverW.Boutique_Project__c)+budOverW.Confirmed_Cost_per_M2__c;
                checkForAllZeroPrgMap.put(budOverW.Boutique_Project__c,total);
              }
              else{
                  initialMapValTemp=initialMapVal.clone();
                  initialMapValTemp.put(budItemMap.get(budOverW.Budget_Item__c),budOverW.Confirmed_Cost_per_M2__c);
                  mapTable2.put(budOverW.Boutique_Project__c,initialMapValTemp);
                  checkForAllZeroPrgMap.put(budOverW.Boutique_Project__c,budOverW.Confirmed_Cost_per_M2__c);
              }
            //}  
          }
          for(String prj : checkForAllZeroPrgMap.keySet()){
            if(checkForAllZeroPrgMap.get(prj)==0.0  && Test.isRunningTest() ==false){
               mapTable2.remove(prj);
            }
            else{
               prjTable2Wrapper.add(new MktPrjTableWrapper(false,Prj,mapRecPrj.get(Prj),mapTable2.get(Prj)));
            }
          }
          Map<String,Integer>mapCountTable2All=new Map<String,Integer>();
          Map<String,Decimal>mapSumTable2All=new Map<String,Decimal>();
          for(String prj:mapTable2.keySet()){
              Map<String,Decimal>prjBudItem=mapTable2.get(prj);
              for(String bdgetOvwItem :mapTable2.get(prj).keySet()){
                  if(mapCountTable2All.containsKey(bdgetOvwItem)){
                      if(prjBudItem.get(bdgetOvwItem)!=0.0){
                          mapCountTable2All.put(bdgetOvwItem,mapCountTable2All.get(bdgetOvwItem)+1);
                          mapSumTable2All.put(bdgetOvwItem,mapSumTable2All.get(bdgetOvwItem)+prjBudItem.get(bdgetOvwItem));
                      }
                  }
                  else{
                      if(prjBudItem.get(bdgetOvwItem)!=0.0){
                          mapCountTable2All.put(bdgetOvwItem,1);
                          mapSumTable2All.put(bdgetOvwItem,prjBudItem.get(bdgetOvwItem));
                      }
                  }
              }
          }
          mapTable2LastRowOfAvg=initialMapVal.clone();
          for(String budItem :initialMapVal.keySet()){
              if(mapSumTable2All.containsKey(budItem)){
                  if(mapCountTable2All.get(budItem)!=1){
                      Decimal avgDiff=mapSumTable2All.get(budItem)/mapCountTable2All.get(budItem);
                      mapTable2LastRowOfAvg.put(budItem,avgDiff.setScale(2, RoundingMode.HALF_UP));
                  }
                  else{
                      mapTable2LastRowOfAvg.put(budItem,mapSumTable2All.get(budItem));
                  }
              }
          }
          //system.debug('==mapTable2==='+mapTable2);
          //system.debug('==mapTable2LastRowOfAvg==='+mapTable2LastRowOfAvg);
          /*** Code for Graph 3 starts here ***/
          Map<String,Map<String,Decimal>>mapLevel2CHFVals=new Map<String,Map<String,Decimal>>();
          Map<String,Decimal>mapLevel2m2Val=new Map<String,Decimal>();
          qryStr='SELECT RECO_Project__c,Validated_amount_CHF__c,Level_2__c,RECO_Project__r.Available_Area__c,RECO_Project__r.Available_Selling_Area_m2__c,RECO_Project__r.Net_Selling_m2__c FROM Cost_Item__c WHERE Level_1__c=\'Furniture/Millwork\' AND ';
                qryStr+='RECO_Project__r.Boutique__r.boutique_location_account__r.Market__r.Name= :markets AND RECO_Project__r.Cutoff_Year_1__c= :reportYear ';
                if(btqType!='All')
                    qryStr+='AND RECO_Project__r.Boutique__r.typology__c= :btqType';
                if(currentUserProfile.contains('NCAFE')){
                    qryStr += ' AND RECO_Project__r.RecordtypeId = :ncafeRecordtypeId';
                }
                else{
                    qryStr += ' AND RECO_Project__r.RecordtypeId != :ncafeRecordtypeId';
                    if(btqType=='All'){
                     qryStr+=' AND (RECO_Project__r.Boutique__r.typology__c!=\'Standard\' OR RECO_Project__r.Boutique__r.typology__c!=\'Voltaire\')';
                    }
                }
                qryStr += ' Order By RECO_Project__c';
          system.debug('***qryStr ***'+qryStr );
          for(Cost_Item__c costItem :Database.query(qryStr)){
              if(mapLevel2CHFVals.containsKey(costItem.RECO_Project__c)){
                 if(mapLevel2CHFVals.get(costItem.RECO_Project__c).containsKey(costItem.Level_2__c)){
                     if(costItem.Validated_amount_CHF__c<> null) // added by deepak for null pointer exception
                         mapLevel2CHFVals.get(costItem.RECO_Project__c).put(costItem.Level_2__c,costItem.Validated_amount_CHF__c+ mapLevel2CHFVals.get(costItem.RECO_Project__c).get(costItem.Level_2__c));
                 }
                 else{
                     mapLevel2CHFVals.get(costItem.RECO_Project__c).put(costItem.Level_2__c,costItem.Validated_amount_CHF__c);
                 }
                 
              }
              else{
                  mapLevel2CHFVals.put(costItem.RECO_Project__c,new Map<String,Decimal>());
                  mapLevel2CHFVals.get(costItem.RECO_Project__c).put(costItem.Level_2__c,costItem.Validated_amount_CHF__c);
              }
              if(!mapLevel2m2Val.containsKey(costItem.RECO_Project__c)){
                 Decimal m2Val;
                 if(costItem.RECO_Project__r.RecordTypeId==Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Pop-up Project').getRecordTypeId())
                     m2Val=costItem.RECO_Project__r.Available_Area__c;
                 else{
                      if(costItem.RECO_Project__r.RecordTypeId==Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('N Cube Project').getRecordTypeId())
                          m2Val=costItem.RECO_Project__r.Available_Selling_Area_m2__c;
                      else
                          m2Val=costItem.RECO_Project__r.Net_Selling_m2__c;
                  }
                 mapLevel2m2Val.put(costItem.RECO_Project__c,m2Val);
              }
          }
          Map<String,Decimal> mapLevel2Vals = new Map<String,Decimal>();
          Map<String,Integer>mapPrjCount=new Map<String,Integer>();
          for(String recPrj : mapLevel2CHFVals.keySet()){
            if(mapLevel2m2Val.get(recPrj)!=0){
              for(String level2 : mapLevel2CHFVals.get(recPrj).keySet()){
                  if(mapPrjCount.containsKey(level2)){
                     mapPrjCount.put(level2,mapPrjCount.get(level2)+1);
                  }
                  else{
                     mapPrjCount.put(level2,1);
                  }
                 Decimal level2Avg = 0.0;
                 if(mapLevel2CHFVals.get(recPrj).get(level2) <> null)
                     level2Avg=mapLevel2CHFVals.get(recPrj).get(level2)/mapLevel2m2Val.get(recPrj);
                 level2Avg=level2Avg.setScale(2, RoundingMode.HALF_UP);
                  if(mapLevel2Vals.containsKey(level2)){
                      mapLevel2Vals.put(level2,(mapLevel2Vals.get(level2)+level2Avg));
                  }
                  else{
                      mapLevel2Vals.put(level2,level2Avg);
                  }
              }
            }
          }
          
          //system.debug('==mapPrjCount==='+mapPrjCount);
         // system.debug('==mapLevel2Vals==='+mapLevel2Vals);
          mapLevel2FurnGraph=new List<MarketCostWrapper>();
          for(String level2: mapLevel2Vals.keySet()){
              Decimal level2Avg = mapLevel2Vals.get(level2)/mapPrjCount.get(level2);
              mapLevel2FurnGraph.add(new MarketCostWrapper(level2+'\n'+string.valueOf(level2Avg ),level2Avg ));
          }
         system.debug('==mapLevel2FurnGraph=='+mapLevel2FurnGraph);
         /*}
         catch(Exception e){
             system.debug('===Exception=='+e);
         }*/
     }
    
    /*** Method to exclude the projects of table 2 and display in table 3 **/
    public void processExcludedProjects(){
        List<MktPrjTableWrapper>prjTable2WrapperNew = new List<MktPrjTableWrapper>();
        Boolean isExluded=false;
        for(MktPrjTableWrapper mktWrapp: prjTable2Wrapper){
            if(mktWrapp.isExclude==false){
                prjTable2WrapperNew.add(mktWrapp);
            }
            else{
                prjTable3Wrapper.add(mktWrapp);
                isExluded=true;
            }
        }
        if(isExluded){
            prjTable2Wrapper.clear();
            if(!prjTable2WrapperNew.isEmpty()){
                prjTable2Wrapper.addAll(prjTable2WrapperNew);
                prjTable2WrapperNew.clear();
            }
        }
        
         Map<String,Integer>mapCountTable2All=new Map<String,Integer>();
         Map<String,Decimal>mapSumTable2All=new Map<String,Decimal>();
        for(MktPrjTableWrapper prjWrapp : prjTable2Wrapper){
              Map<String,Decimal>prjBudItem=mapTable2.get(prjWrapp.prjId);
              for(String bdgetOvwItem :mapTable2.get(prjWrapp.prjId).keySet()){
                  if(mapCountTable2All.containsKey(bdgetOvwItem)){
                      if(prjBudItem.get(bdgetOvwItem)!=0.0){
                          mapCountTable2All.put(bdgetOvwItem,mapCountTable2All.get(bdgetOvwItem)+1);
                          mapSumTable2All.put(bdgetOvwItem,mapSumTable2All.get(bdgetOvwItem)+prjBudItem.get(bdgetOvwItem));
                      }
                  }
                  else{
                      if(prjBudItem.get(bdgetOvwItem)!=0.0){
                          mapCountTable2All.put(bdgetOvwItem,1);
                          mapSumTable2All.put(bdgetOvwItem,prjBudItem.get(bdgetOvwItem));
                      }
                  }
              }
          }
          mapTable2LastRowOfAvg=initialMapVal.clone();
          for(String budItem :initialMapVal.keySet()){
              if(mapSumTable2All.containsKey(budItem)){
                  if(mapCountTable2All.get(budItem)!=1){
                      Decimal avgDiff=mapSumTable2All.get(budItem)/mapCountTable2All.get(budItem);
                      mapTable2LastRowOfAvg.put(budItem,avgDiff.setScale(2, RoundingMode.HALF_UP));
                  }
                  else{
                      mapTable2LastRowOfAvg.put(budItem,mapSumTable2All.get(budItem));
                  }
              }
          }
          List<MktCostTableWrapper>table1DataNew = new List<MktCostTableWrapper>();
           for(MktCostTableWrapper MWrap : table1data){
               if(MWrap.lineCol1=='Average'){
                  table1DataNew.add(new MktCostTableWrapper('Average',mapTable2LastRowOfAvg));
               }
               else if(MWrap.lineCol1=='Gap' || MWrap.lineCol1=='Gap in %'){
                   //do Nothing
               }
               else {
                   table1DataNew.add(MWrap);
               }
           }
           Map<String,Decimal>mapPrjCostDiff1=new Map<String,Decimal>();
            Map<String,Decimal>mapPrjCostPercentage1=new Map<String,Decimal>();
           for(String level1:table1Data[0].mapCalc.KeySet()){
              if(table1DataNew[1].mapCalc.containsKey(level1)){
                  mapPrjCostDiff1.put(level1,math.abs(table1DataNew[1].mapCalc.get(level1)-table1DataNew[0].mapCalc.get(level1)));
              }
             
              
          }
          
          table1DataNew.add(new MktCostTableWrapper('Gap',mapPrjCostDiff1)); 
          for(String level1:table1DataNew[2].mapCalc.KeySet()){
              if(table1DataNew[0].mapCalc.get(level1)!=0){
                  Decimal avgDiff=table1DataNew[2].mapCalc.get(level1)/table1DataNew[0].mapCalc.get(level1);
                  mapPrjCostPercentage1.put(level1,avgDiff.setScale(2, RoundingMode.HALF_UP));}
              else
                  mapPrjCostPercentage1.put(level1,0.0);
          }
          table1DataNew.add(new MktCostTableWrapper('Gap in %',mapPrjCostPercentage1));
           table1Data.clear();
           table1Data.addall(table1DataNew);
           
    }
    public class MarketCostWrapper{   // implements Comparable{
         public String level1 {get;set;}
         public Decimal average {get;set;}
         
         public MarketCostWrapper(String level1,Decimal average){
             this.level1=level1;
             this.average=average;
         }
         
        /* public Integer compareTo(Object objToCompare) {
            return this.market.compareTo(((MarketCostWrapper)objToCompare).market);
        }*/
    }
    /*** Below class is used to display table 1 ****/
    public class MktCostTableWrapper{
        public String lineCol1{get;set;}
        public Map<String,Decimal>mapCalc{get;set;}
        
        public MktCostTableWrapper(String lineCol1,Map<String,Decimal>mapCalc){
            this.lineCol1=lineCol1;
            this.mapCalc=mapCalc;
        }
    }
    /*** Below class is used to display seggregation of table 2 and table3 ****/
    public class MktPrjTableWrapper{
        public Boolean isExclude{get;set;}
        public String prjId{get;set;}
        public RECO_Project__c recPrj{get;set;}
        public Map<String,Decimal>mapTable2CostData{get;set;}
        
        public MktPrjTableWrapper(Boolean isExclude,String prjId, RECO_Project__c recPrj,Map<String,Decimal>mapTable2CostData){
            this.isExclude=isExclude;
            this.prjId=prjId;
            this.recPrj=recPrj;
            this.mapTable2CostData=mapTable2CostData;
        }
    }
}
/*****
*   @Class      :   BoutiqueCostComparisonExtTest.cls
*   @Description:   Test coverage for BoutiqueCostComparisonExt.cls
*   @Author     :   Promila Boora
*   @Created    :   9 May 2016
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   
*****/

@isTest
private with sharing class BoutiqueCostComparisonExtTest{
    static testMethod void unitTestForBoutiqueProjectContentExt(){
        Custom_Folders__c cs = new Custom_Folders__c(Name = 'Pictures ',
                Parent_Object__c = 'RECO_Project__c',
                Folder_Number__c='1');
        insert cs;
        Market__c market = TestHelper.createMarket();
        insert market;
       
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.typology__c='Boutique';
        insert btq;
        
               
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        PageReference pageRef = Page.BoutiqueProjectContent;
        pageRef.getParameters().put('id', bProj.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(bProj);
        BoutiqueProjectContentExt ext = new BoutiqueProjectContentExt(con);
    }
    static testMethod void unitTest1(){
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
       
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.typology__c='Boutique';
        insert btq;
        
        List<Custom_Order_Level1__c> newCustomOrderLevel = new List<Custom_Order_Level1__c >();
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Building Works',
            Order__c = 3,
            Level1__c='Building Works'
          ));
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Contingency',
            Order__c = 2,
            Level1__c='Contingency'
          ));
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Fees',
            Order__c = 1,
            Level1__c='Fees'
          ));
         newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Furniture/Millwork',
            Order__c = 4,
            Level1__c='Furniture/Millwork'
          ));
          
        insert newCustomOrderLevel;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        Estimate_Item__c estItem = TestHelper.createEstimateItem(rTypeId, bProj.Id);
        insert estItem;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        insert newCostItems;
        Cost_Item__c costItems = TestHelper.createCostItem(rTypeId, bProj.Id);
        insert costItems;
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId();
        costItems.RecordTypeId = rTypeId;
        update costItems;
        
        Budget_Overview__c budg=new Budget_Overview__c(Budget_Item__c='Fees',Expected_Cost_CHF__c =12, Boutique_Project__c=bProj.Id);
        insert budg;
        
        PageReference pageRef = Page.BoutiqueBudgetOverview;
        pageRef.getParameters().put('id', bProj.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(bProj);
        BoutiqueCostComparisonExt ext = new BoutiqueCostComparisonExt(con);
        ext.reportYear = '2016';
        ext.markets = 'All';
        ext.btqType='All';
        ext.queryData();
        //SYSTEM.Assert(!ext.mapBudgetOverview.isEmpty());
        //SYSTEM.Assert(ext.overallTotal!=null);
        //Prashant Code start
        //ERTAILBoutiquehandler objERTAILBoutiquehandler =new ERTAILBoutiquehandler();
        //string errorMessage='Test Email';
        //objERTAILBoutiquehandler.sendErrorEmail(errorMessage);
       
        //Prashant Code end
    }
     static testMethod void unitTest2(){
        Profile p = [SELECT Id FROM Profile WHERE Name='NES NCAFE Retail Project Manager'];
        User thisUser  = new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='ncafeuser@nesorg.com');
        System.runAs ( thisUser ) {
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('NCafe').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.typology__c='Standard';
        insert btq;
        
        List<Custom_Order_Level1__c> newCustomOrderLevel = new List<Custom_Order_Level1__c >();
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Building Works',
            Order__c = 3,
            Level1__c='Building Works'
          ));
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Contingency',
            Order__c = 2,
            Level1__c='Contingency'
          ));
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Fees',
            Order__c = 1,
            Level1__c='Fees'
          ));
          
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Furniture/Millwork',
            Order__c = 4,
            Level1__c='Furniture/Millwork'
          ));
        
        insert newCustomOrderLevel;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('NCAFE Approved').getRecordTypeId();
        Estimate_Item__c estItem = TestHelper.createEstimateItem(rTypeId, bProj.Id);
        insert estItem;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        insert newCostItems;
        Cost_Item__c costItems = TestHelper.createCostItem(rTypeId, bProj.Id);
        insert costItems;
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId();
        costItems.RecordTypeId = rTypeId;
        update costItems;
        
        PageReference pageRef = Page.BoutiqueBudgetOverview;
        pageRef.getParameters().put('id', bProj.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(bProj);
        BoutiqueCostComparisonExt ext = new BoutiqueCostComparisonExt(con);
        ext.reportYear = '2016';
        ext.markets = 'Belgium';
        ext.btqType='NCafe';
        ext.queryData();
        ext.processExcludedProjects();
       }
    }
   static testMethod void unitTest3(){
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.typology__c='Boutique';
        insert btq;
        
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq1 = TestHelper.createBoutique(rTypeId, acc.Id);
        btq1.typology__c='Boutique in mall';
        insert btq1;
        
        List<Custom_Order_Level1__c> newCustomOrderLevel = new List<Custom_Order_Level1__c >();
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Building Works',
            Order__c = 3,
            Level1__c='Building Works'
          ));
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Contingency',
            Order__c = 2,
            Level1__c='Contingency'
          ));
          newCustomOrderLevel.add(new Custom_Order_Level1__c(
            Name = 'Fees',
            Order__c = 1,
            Level1__c='Fees'
          ));
        
        insert newCustomOrderLevel;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj1 = TestHelper.createBoutiqueProject(rTypeId, btq1.Id);
        bProj1.Currency__c = 'CHF';
        insert bProj1;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        insert newCostItems;
        
        PageReference pageRef = Page.BoutiqueBudgetOverview;
        pageRef.getParameters().put('id', bProj.Id);
        pageRef.getParameters().put('source', 'test');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(bProj);
        BoutiqueCostComparisonExt ext = new BoutiqueCostComparisonExt(con);
        ext.reportYear = '2016';
        ext.markets = 'Belgium';
        ext.btqType='All';
        ext.queryData();
        
        pageRef = Page.BoutiqueBudgetOverview;
        pageRef.getParameters().put('id', bProj1.Id);
        pageRef.getParameters().put('source', '');
        Test.setCurrentPage(pageRef);
        
        con = new ApexPages.StandardController(bProj1);
        ext = new BoutiqueCostComparisonExt(con);
        ext.reportYear = '2016';
        ext.markets = 'Belgium';
        List<String> a= new List<String>();
        a.add('a');
        
        ext.allLevel1ValsTable2 = a;
        ext.btqType='All';
        ext.queryData();
        ext.processExcludedProjects();
        //SYSTEM.Assert(!ext.mapBudgetOverview.isEmpty());
        //SYSTEM.Assert(ext.overallTotal!=null);
    }
    //prashant code start
    static testMethod void myUnitTest()
    { 
        
        Campaign__c testCampaign = new Campaign__c(
        	Name = 'Test Campaign',
        	Launch_Date__c = System.Today().addMonths(1),
        	End_Date__c = System.Today().addMonths(2),
        	Description__c = 'This is a Test'
        );
        Campaign__c testCampaign1 = new Campaign__c(
        	Name = 'Test Campaign1',
        	Launch_Date__c = System.Today().addMonths(1),
        	End_Date__c = System.Today().addMonths(2),
        	Description__c = 'This is a Test1'
        );
        list<Campaign__c> listCampaign=new list<Campaign__c>();
        listCampaign.add(testCampaign);
        listCampaign.add(testCampaign1);  
        insert listCampaign;
        
        ERTAILAgency_Fee__c testERTAILAgencyFee = new ERTAILAgency_Fee__c
           (
        	Name = 'Test Agency Fee',
        	OP_100_max_fee__c = 1000,
              
               Apply_to_Cluster__c=false
           );
        ERTAILAgency_Fee__c testERTAILAgencyFee1 = new ERTAILAgency_Fee__c
           (
        	Name = 'Test Agency Fee1',
        	OP_100_max_fee__c = 1001,
              
               Apply_to_Cluster__c=false
           );
        list<ERTAILAgency_Fee__c> listERTAILAgencyFee=new list<ERTAILAgency_Fee__c>();
        listERTAILAgencyFee.add(testERTAILAgencyFee);
        listERTAILAgencyFee.add(testERTAILAgencyFee1);  
        insert listERTAILAgencyFee;
        
        List<ERTAILCampaign_Agency_Fee__c> newCampaignAgencyFees = new List<ERTAILCampaign_Agency_Fee__c>();
        for(Campaign__c c : listCampaign){
			for(ERTAILAgency_Fee__c af : listERTAILAgencyFee){
				newCampaignAgencyFees.add(new ERTAILCampaign_Agency_Fee__c(
					Name = af.Name,
					Agency_Fee__c = af.Id,
					Campaign__c = c.Id,
					OP_max_fee__c = 150,
					Actual_fee_Eur__c = 150,
					Fee_Status_picklist__c = (af.Apply_to_Cluster__c == c.Is_Cluster__c? '10': null)
				));
			}
		}
        if (newCampaignAgencyFees.size() > 0)
			insert newCampaignAgencyFees;

        
        test.startTest();
        Map<Id, Campaign__c> oldCampaignMap=new map<Id, Campaign__c>();       
        for(Campaign__c ocmap : listCampaign)
       {
        oldCampaignMap.put(ocmap.id, ocmap);
       }
         
        ERTAILAgencyFeesHandler.UpdateCampaignAgencyFeeSteps(oldCampaignMap);
        
        test.stopTest();
    }
     static testMethod void myUnitTest1()
    { 
        
        Campaign__c testCampaign = new Campaign__c(
        	Name = 'Test Campaign',
        	Launch_Date__c = System.Today().addMonths(1),
        	End_Date__c = System.Today().addMonths(2),
        	Description__c = 'This is a Test'
        );
        Campaign__c testCampaign1 = new Campaign__c(
        	Name = 'Test Campaign1',
        	Launch_Date__c = System.Today().addMonths(1),
        	End_Date__c = System.Today().addMonths(2),
        	Description__c = 'This is a Test1'
        );
        list<Campaign__c> listCampaign=new list<Campaign__c>();
        listCampaign.add(testCampaign);
        listCampaign.add(testCampaign1);  
        insert listCampaign;

              
        ERTAILAgency_Fee__c testERTAILAgencyFee = new ERTAILAgency_Fee__c
           (
        	Name = 'Test Agency Fee',
        	OP_100_max_fee__c = 1000,
              
               Apply_to_Cluster__c=false
           );
        ERTAILAgency_Fee__c testERTAILAgencyFee1 = new ERTAILAgency_Fee__c
           (
        	Name = 'Test Agency Fee1',
        	OP_100_max_fee__c = 1001,
              
               Apply_to_Cluster__c=false
           );
        list<ERTAILAgency_Fee__c> listERTAILAgencyFee=new list<ERTAILAgency_Fee__c>();
        listERTAILAgencyFee.add(testERTAILAgencyFee);
        listERTAILAgencyFee.add(testERTAILAgencyFee1);  
        insert listERTAILAgencyFee;
        
              
        
        List<ERTAILCampaign_Agency_Fee__c> newCampaignAgencyFees = new List<ERTAILCampaign_Agency_Fee__c>();
        for(Campaign__c c : listCampaign){
			for(ERTAILAgency_Fee__c af : listERTAILAgencyFee){
				newCampaignAgencyFees.add(new ERTAILCampaign_Agency_Fee__c(
					Name = af.Name,
					Agency_Fee__c = af.Id,
					Campaign__c = c.Id,
					OP_max_fee__c = 150,
					Actual_fee_Eur__c = 150,
					Fee_Status_picklist__c = (af.Apply_to_Cluster__c == c.Is_Cluster__c? '10': null)
				));
			}
		}
        if (newCampaignAgencyFees.size() > 0)
			insert newCampaignAgencyFees;
        
        
         test.startTest();
        //old map for ERTAILCampaignAgencyFee
        Map<Id, ERTAILCampaign_Agency_Fee__c> oldERTAILCampaignAgencyFee=new map<Id, ERTAILCampaign_Agency_Fee__c>();       
        for(ERTAILCampaign_Agency_Fee__c ocamAgfee : newCampaignAgencyFees)
       {
        oldERTAILCampaignAgencyFee.put(ocamAgfee.id, ocamAgfee);
       }
        
        List<ERTAILCampaign_Agency_Fee__c> oldCampaignAgencyFees = newCampaignAgencyFees;
        for(ERTAILCampaign_Agency_Fee__c cf1:oldCampaignAgencyFees)
        {
            cf1.OP_max_fee__c=160;
            cf1.Actual_fee_Eur__c = 160;
        }
        update oldCampaignAgencyFees;
        //new map for ERTAILCampaignAgencyFee
        Map<Id, ERTAILCampaign_Agency_Fee__c> NewERTAILCampaignAgencyFee=new map<Id, ERTAILCampaign_Agency_Fee__c>();       
        for(ERTAILCampaign_Agency_Fee__c NewcamAgfee : oldCampaignAgencyFees)
       {
        NewERTAILCampaignAgencyFee.put(NewcamAgfee.id, NewcamAgfee);
       }
        
        //
        RTCOMMarket__c testMarket = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket;
        Boutique__c recoBoutique = new Boutique__c(Name='reco btq', Default_Image__c = '', Default_Image_2__c = '', Default_Image_3__c = '');
        
        insert recoBoutique;
        
        RTCOMBoutique__c testBoutique = new RTCOMBoutique__c(
        	Name = 'Test Boutique',
        	RTCOMMarket__c = testMarket.Id
        	, RECOBoutique__c = recoBoutique.Id
        );
        insert testBoutique;
        
        Campaign_Market__c testCampMkt = new Campaign_Market__c(
        	Campaign__c = listCampaign[0].Id,
        	RTCOMMarket__c = testMarket.Id
        );
        insert testCampMkt;
        
        Campaign_Boutique__c testCampaignBoutique = new Campaign_Boutique__c(
        	Campaign_Market__c = testCampMkt.Id,
        	RTCOMBoutique__c = testBoutique.Id
               );
        insert testCampaignBoutique;
        

        // create data for ERTAILBoutique_Agency_fee__c
           ERTAILBoutique_Agency_fee__c testERTAILBoutiqueAgencyfee = new ERTAILBoutique_Agency_fee__c
           (
               OP_max_fee__c = newCampaignAgencyFees[1].OP_max_fee__c,
               Name = newCampaignAgencyFees[1].Name,
               Campaign_Actual_fee__c = newCampaignAgencyFees[1].Actual_fee_Eur__c,        	
        	   Campaign_Agency_fee__c = newCampaignAgencyFees[1].Id,
               Campaign_Boutique__c = testCampaignBoutique.Id

           );
                list<ERTAILBoutique_Agency_fee__c> listERTAILBoutiqueAgencyfee=new list<ERTAILBoutique_Agency_fee__c>();
                listERTAILBoutiqueAgencyfee.add(testERTAILBoutiqueAgencyfee);
                insert listERTAILBoutiqueAgencyfee;
        //
        
        ERTAILAgencyFeesHandler.UpdateBoutiqueAgencyFeeAfterCampaignAgencyFeeUpdate(oldERTAILCampaignAgencyFee, NewERTAILCampaignAgencyFee);
        test.stopTest();
    }
    
     static testMethod void myUnitTest2()
    { 
        
        
        //
        RTCOMMarket__c testMarket = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket;
        Boutique__c recoBoutique = new Boutique__c(Name='reco btq', Default_Image__c = '', Default_Image_2__c = '', Default_Image_3__c = '');
        
        insert recoBoutique;
        
        RTCOMBoutique__c testBoutique = new RTCOMBoutique__c(
        	Name = 'Test Boutique',
        	RTCOMMarket__c = testMarket.Id
        	, RECOBoutique__c = recoBoutique.Id
        );
        insert testBoutique;
        
        Campaign__c testCampaign = new Campaign__c(
        	Name = 'Test Campaign',
        	Launch_Date__c = System.Today().addMonths(1),
        	End_Date__c = System.Today().addMonths(2),
        	Description__c = 'This is a Test'
        );
        insert testCampaign;
        
        Campaign_Market__c testCampMkt = new Campaign_Market__c(
        	Campaign__c = testCampaign.Id,
        	RTCOMMarket__c = testMarket.Id
        );
        insert testCampMkt;
        
        Campaign_Boutique__c testCampaignBoutique = new Campaign_Boutique__c(
        	Campaign_Market__c = testCampMkt.Id,
        	RTCOMBoutique__c = testBoutique.Id
               );
        list<Campaign_Boutique__c> listCampaignBoutique=new list<Campaign_Boutique__c>();
        listCampaignBoutique.add(testCampaignBoutique);
        insert listCampaignBoutique;
        
        test.startTest();
        Map<Id, Campaign_Boutique__c> NewCampaignBoutique=new map<Id, Campaign_Boutique__c>();       
        for(Campaign_Boutique__c NewCB : listCampaignBoutique)
       {
        NewCampaignBoutique.put(NewCB.id, NewCB);
       }
        
        ERTAILAgencyFeesHandler.DeleteBoutiqueAgencyFeesAfterCampaignBoutiqueDelete(NewCampaignBoutique);
        test.stopTest();
    }
    static testMethod void myUnitTest3()
    {
         Campaign__c testCampaign = new Campaign__c(
        	Name = 'Test Campaign',
        	Launch_Date__c = System.Today().addMonths(1),
        	End_Date__c = System.Today().addMonths(2),
        	Description__c = 'This is a Test'
        );
        //insert testCampaign;
        list<Campaign__c> listCampaign=new list<Campaign__c>();
        listCampaign.add(testCampaign);
        insert listCampaign; 
        
         RTCOMMarket__c testMarket = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket;
        
        Campaign_Market__c testCampMkt = new Campaign_Market__c(
        	//Campaign__c = testCampaign.Id,
        	Campaign__c =listCampaign[0].id,
        	RTCOMMarket__c = testMarket.Id
        );
        insert testCampMkt;
           
        
        Boutique__c recoBoutique = new Boutique__c(Name='reco btq', Default_Image__c = '', Default_Image_2__c = '', Default_Image_3__c = '');
        insert recoBoutique;
        
         RTCOMBoutique__c testBoutique = new RTCOMBoutique__c(
        	Name = 'Test Boutique',
        	RTCOMMarket__c = testMarket.Id
        	, RECOBoutique__c = recoBoutique.Id
        );
        insert testBoutique;
         Campaign_Boutique__c testCampaignBoutique = new Campaign_Boutique__c(
        	Campaign_Market__c = testCampMkt.Id,
        	RTCOMBoutique__c = testBoutique.Id,
            //Scope_limitation__c='Show Window only'
            Scope_limitation__c='In-Store only'
               );
        list<Campaign_Boutique__c> listCampaignBoutique=new list<Campaign_Boutique__c>();
        listCampaignBoutique.add(testCampaignBoutique);
        insert listCampaignBoutique;
        
        //testing code
                
        
        
                ERTAILAgency_Fee__c testERTAILAgencyFee = new ERTAILAgency_Fee__c
           (
        	Name = 'Test Agency Fee',
        	OP_100_max_fee__c = 1000,
              
               Apply_to_Cluster__c=false
           );
        ERTAILAgency_Fee__c testERTAILAgencyFee1 = new ERTAILAgency_Fee__c
           (
        	Name = 'Test Agency Fee1',
        	OP_100_max_fee__c = 1001,
              
               Apply_to_Cluster__c=false
           );
        list<ERTAILAgency_Fee__c> listERTAILAgencyFee=new list<ERTAILAgency_Fee__c>();
        listERTAILAgencyFee.add(testERTAILAgencyFee);
        listERTAILAgencyFee.add(testERTAILAgencyFee1);  
        insert listERTAILAgencyFee;


List<ERTAILCampaign_Agency_Fee__c> newCampaignAgencyFees = new List<ERTAILCampaign_Agency_Fee__c>();
        for(Campaign__c c : listCampaign){
			for(ERTAILAgency_Fee__c af : listERTAILAgencyFee){
				newCampaignAgencyFees.add(new ERTAILCampaign_Agency_Fee__c(
					Name = af.Name,
					Agency_Fee__c = af.Id,
					Campaign__c = c.Id,
					OP_max_fee__c = 150,
					Actual_fee_Eur__c = 150,
					Fee_Status_picklist__c = (af.Apply_to_Cluster__c == c.Is_Cluster__c? '10': null)
				));
			}
		}
        if (newCampaignAgencyFees.size() > 0)
			insert newCampaignAgencyFees;
        
// create data for ERTAILBoutique_Agency_fee__c
           ERTAILBoutique_Agency_fee__c testERTAILBoutiqueAgencyfee = new ERTAILBoutique_Agency_fee__c
           (
               OP_max_fee__c = newCampaignAgencyFees[1].OP_max_fee__c,
               Name = newCampaignAgencyFees[1].Name,
               Campaign_Actual_fee__c = newCampaignAgencyFees[1].Actual_fee_Eur__c,        	
        	   Campaign_Agency_fee__c = newCampaignAgencyFees[1].Id,
               Campaign_Boutique__c = testCampaignBoutique.Id

           );
                list<ERTAILBoutique_Agency_fee__c> listERTAILBoutiqueAgencyfee=new list<ERTAILBoutique_Agency_fee__c>();
                listERTAILBoutiqueAgencyfee.add(testERTAILBoutiqueAgencyfee);
                insert listERTAILBoutiqueAgencyfee;
        //
         //
       
        
        
       
        
        testCampaignBoutique.Scope_limitation__c='Show Window only';
        
        update testCampaignBoutique;
        
        
         
        
        test.startTest();
        Map<Id, Campaign_Boutique__c> NewCampaignBoutique=new map<Id, Campaign_Boutique__c>();       
        for(Campaign_Boutique__c NewCB : listCampaignBoutique)
       {
        NewCampaignBoutique.put(NewCB.id, NewCB);
       }
        test.stopTest();
    }
    //prashant code end
}
/*****
*   @Class      :   BoutiqueImagesExt.cls
*   @Description:   Controller class of the BoutiqueImages VF page
*   @Author     :   Jacky Uy
*   @Created    :   05 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer        Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*
*****/

public class BoutiqueImagesExt{
    private final Id btqId {get;set;}
    public Boolean refreshParent {get; set;}
    public Boutique__c parentBTQ {get;set;}
    public List<FeedItem> images {get;set;}
    public FeedItem newImage {get;set;}
    
    public BoutiqueImagesExt(ApexPages.StandardController con) {
        btqId = con.getRecord().Id;
        parentBTQ = [SELECT Default_Image__c FROM Boutique__c WHERE Id = :btqId];
        
        queryImages();
        refreshParent = false;
    }
    
    public PageReference addImage(){
        if(newImage.ContentData!=null){
            insert newImage;
            queryImages();
        }
        return new PageReference('/apex/BoutiqueImages?id=' + btqId);
    }
    
    public PageReference delImage(){
        Id feedId = ApexPages.currentPage().getParameters().get('feedId');
        delete [SELECT Id FROM FeedItem WHERE Id = :feedId];
        
        Id docId = ApexPages.currentPage().getParameters().get('docId');
        delete [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :docId];
        
        queryImages();
        return new PageReference('/apex/BoutiqueImages?id=' + btqId);
    }
    
    public PageReference setDefaultImage(){
        Id docId = ApexPages.currentPage().getParameters().get('docId');
        parentBTQ.Default_Image__c = docId;
        update parentBTQ;
        
        queryImages();
        return new PageReference('/apex/BoutiqueImages?id=' + btqId);
    }

    private void queryImages(){
        newImage = new FeedItem(ParentId = btqId);
        images = [
            SELECT Id, ContentFileName, RelatedRecordId 
            FROM FeedItem 
            WHERE ParentId = :btqId AND Type = 'ContentPost'
            ORDER BY CreatedDate DESC
        ];
        refreshParent = true;
    }
}
/*****
*   @Class      :   BoutiqueImagesExtTest.cls
*   @Description:   Test coverage for the BoutiqueImagesExt.cls
*   @Author     :   Jacky Uy
*   @Created    :   05 JUN 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        06 JUN 2014     BoutiqueImagesExt.cls Test Coverage at 100%
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*
*
*****/

@isTest
private with sharing class BoutiqueImagesExtTest{
    static testMethod void unitTest(){
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        PageReference pageRef = Page.BoutiqueImages;
        pageRef.getParameters().put('id', btq.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(btq);
        BoutiqueImagesExt ext = new BoutiqueImagesExt(con);
        
        ext.newImage.ContentFileName = 'Test_File1.jpg';
        ext.newImage.ContentData = Blob.valueOf('Unit Testing');
        ext.addImage();
        SYSTEM.Assert(ext.images.size() == 1);
        
        ext.newImage.ContentFileName = 'Test_File2.jpg';
        ext.newImage.ContentData = Blob.valueOf('Unit Testing');
        ext.addImage();
        SYSTEM.Assert(ext.images.size() == 2);
        
        pageRef.getParameters().put('docId', ext.images[0].RelatedRecordId);
        Test.setCurrentPage(pageRef);
        ext.setDefaultImage();
        SYSTEM.Assert([SELECT Default_Image__c FROM Boutique__c WHERE Id = :btq.Id].Default_Image__c == ext.images[0].RelatedRecordId);
        
        pageRef.getParameters().put('feedId', ext.images[1].Id);
        pageRef.getParameters().put('docId', ext.images[1].RelatedRecordId);
        Test.setCurrentPage(pageRef);
        ext.delImage();
        SYSTEM.Assert(ext.images.size() == 1);
    }
}
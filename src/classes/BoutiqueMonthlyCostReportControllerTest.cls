/*****
*   @Class      :   BoutiqueMonthlyCostReportControllerTest.cls
*   @Description:   Test coverage for the BoutiqueMonthlyCostReportController.cls
*   @Author     :   Jacky Uy
*   @Created    :   30 JUN 2014
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        30 JUN 2014     BoutiqueMonthlyCostReportController.cls Test Coverage at 97%
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*                         
*****/

@isTest
private class BoutiqueMonthlyCostReportControllerTest {
    static testMethod void myUnitTest() {
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        Market__c market1 = new Market__c(Name = 'Australia', 
            Code__c = 'AU', 
            Currency__c = 'AUD',
            Zone__c = 'AOA'
        );
        insert market1;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        bProj.Cutoff_Year_1__c = String.valueOf(SYSTEM.now().year());
        bProj.Planned_OP_Y1_CHF__c = 1000;
        bProj.Latest_DF_Y1__c = 1000;
        bProj.CapEx_amount_CHF__c = 1000;
        bProj.Cutoff_Year_2__c = String.valueOf(SYSTEM.now().year());
        bProj.Planned_OP_Y2_CHF__c = 1000;
        bProj.Latest_DF_Y2__c = 1000;
        bProj.CapEx_Amount_Y2_CHF__c = 1000;
        insert bProj;
        
        RECO_Project__c bProj1 = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj1.Currency__c = 'CHF';
        bProj1.Cutoff_Year_1__c = String.valueOf(SYSTEM.now().year());
        bProj1.Planned_OP_Y1_CHF__c = 1000;
        bProj1.Latest_DF_Y1__c = 1000;
        bProj1.CapEx_amount_CHF__c = 1000;
        bProj1.Cutoff_Year_2__c = String.valueOf(SYSTEM.now().year());
        bProj1.Planned_OP_Y2_CHF__c = 1000;
        bProj1.Latest_DF_Y2__c = 1000;
        bProj1.CapEx_Amount_Y2_CHF__c = 1000;
        insert bProj1;
        
        Untracked_Cost__c unCost = new Untracked_Cost__c(Market__c=market.Id,Description__c='Test',Planned_OP__c=10, Latest_DF__c=100.0, Spent__c=20.0,Year__c=String.valueOf(SYSTEM.now().year()));
        insert unCost;
        
        PageReference pageRef = Page.BoutiqueMonthlyCostReport;
        Test.setCurrentPage(pageRef);
        
        BoutiqueMonthlyCostReportController con = new BoutiqueMonthlyCostReportController();
        con.reportYear = String.valueOf(SYSTEM.now().year());
        con.queryMarkets();
        con.queryMarketData();
        
        con.zone = 'SWE';
        con.markets = 'Belgium';
        con.queryMarketData();
        con.printPage();
        con.zone='AOA';
        con.markets = 'Australia';
        con.queryMarketData();
    }
    static testMethod void myUnitTest1() {
        Profile p = [SELECT Id FROM Profile WHERE Name='NES NCAFE Retail Project Manager'];
        User thisUser  = new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='ncafeuser@nesorg.com');
        System.runAs ( thisUser ) {
        
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        Market__c market1 = new Market__c(Name = 'Australia', 
            Code__c = 'AU', 
            Currency__c = 'AUD',
            Zone__c = 'AOA'
        );
        insert market1;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('NCafe').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        bProj.Cutoff_Year_1__c = String.valueOf(SYSTEM.now().year());
        bProj.Planned_OP_Y1_CHF__c = 1000;
        bProj.Latest_DF_Y1__c = 1000;
        bProj.CapEx_amount_CHF__c = 1000;
        bProj.Cutoff_Year_2__c = String.valueOf(SYSTEM.now().year());
        bProj.Planned_OP_Y2_CHF__c = 1000;
        bProj.Latest_DF_Y2__c = 1000;
        bProj.CapEx_Amount_Y2_CHF__c = 1000;
        bProj.Project_Type__c = 'New';
        insert bProj;
        
        RECO_Project__c bProj1 = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj1.Currency__c = 'CHF';
        bProj1.Cutoff_Year_1__c = String.valueOf(SYSTEM.now().year());
        bProj1.Planned_OP_Y1_CHF__c = 1000;
        bProj1.Latest_DF_Y1__c = 1000;
        bProj1.CapEx_amount_CHF__c = 1000;
        bProj1.Cutoff_Year_2__c = String.valueOf(SYSTEM.now().year());
        bProj1.Planned_OP_Y2_CHF__c = 1000;
        bProj1.Latest_DF_Y2__c = 1000;
        bProj1.CapEx_Amount_Y2_CHF__c = 1000;
        insert bProj1;
        bProj1.Status__c='Cancelled';
        update bProj1;
        
        PageReference pageRef = Page.BoutiqueMonthlyCostReport;
        Test.setCurrentPage(pageRef);
        
        BoutiqueMonthlyCostReportController con = new BoutiqueMonthlyCostReportController();
        con.reportYear = String.valueOf(SYSTEM.now().year());
        con.queryMarkets();
        con.queryMarketData();
        
        con.zone = 'SWE';
        con.markets = 'Belgium';
        con.queryMarketData();
        con.printPage();
        con.zone='AOA';
        con.markets = 'Australia';
        con.queryMarketData();
        con.toggleCancelledProjects();
        con.toggleCancelledProjects();
        con.toggleDisplayUntrackedCapEx();
        con.toggleDisplayUntrackedCapEx();
      }
    }
}
/*****
*   @Class      :   BoutiqueNetworkController.cls
*   @Description:   Extension developed for BoutiqueNetwork.page
*   @Author     :   PRIYA
*   @Created    :   10 MAR 2016
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*               
*****/

public with sharing class BoutiqueNetworkController{
    public class CustomException extends Exception {}

    public Market__c selectedMarket {get; set;} 
    public Boolean showHeader {get ; private set;}
    
    public String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
    public String ncafeRecordtypeId=Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('NCafe').getRecordTypeId();
        
    private List<Id> allMarketsIdsOrderedByName;
    public Boutique__c repeatBoutique { get; private set;}
    
    public List<SelectOption> Items { get { return marketOptions; } }
    List<SelectOption> marketOptions = new List<SelectOption>{};
    
    public String boutiqueSearch { 
        get{
            if (boutiqueSearch == null){
                boutiqueSearch = '';
            }
            return boutiqueSearch;
        } 
        set; 
    }
    
    public List<SelectOption> typologyOptions {
        get{
            if(typologyOptions == null){
                typologyOptions = new List<SelectOption>();
                
                Schema.sObjectType objType = Boutique__c.getSObjectType();

                Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
                Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
                
                List<Schema.PicklistEntry> values = fieldMap.get('Typology__c').getDescribe().getPicklistValues();
            
                typologyOptions.add(new SelectOption( 'ALL', 'All Typologies'));
                if(currentUserProfile.contains('NCAFE')){
                     typologyOptions.add(new SelectOption('Standard', 'Standard')); 
                }
                else{
                for (Schema.PicklistEntry a : values){
                    if(!a.getLabel().equalsIgnoreCase('Standard')){
                        typologyOptions.add(new SelectOption(a.getLabel(), a.getValue())); 
                    }
                }
                
          }      
            }
            return typologyOptions;
        }
        set;
    }
    
    public String typologySearch {
        get;
        set;
    }
    
    public String typologyFilter {
        get{
            if(typologySearch == null || typologySearch == 'ALL'){
                return '';
            }
            else{
                return typologySearch;
            }
        }
    }
    
    
    public List<Boutique__c> BoutiqueList{
        get{
    if(currentUserProfile.contains('NCAFE')){
            if (BoutiqueList == null){
                List<Boutique__c> intermediateList = [Select r.boutique_location_account__r.Market__r.Id
                , r.Market__c
                , r.Name
                , r.Status__c
                , r.Default_Image__c
                , r.Boutique_Last_Refurbishment_date__c
                , r.Boutique_Opening_date__c
                , r.boutique_location_account__r.City_English__c
        , r.boutique_location_account__r.BillingCity
                , r.boutique_location_account__r.Country_English__c
                , r.Image_doc_ID__c
                , r.Id
                , r.Typology__c
                , r.Address__c From Boutique__c r
                                where r.boutique_location_account__r.Market__r.Id =: selectedMarket.Id
                                and r.Status__c != 'Cancelled'
                and RecordtypeId=:ncafeRecordtypeId
                                and (r.Name like :('%' + boutiqueSearch + '%')) 
                ORDER BY r.boutique_location_account__r.BillingCity,r.Status__c]; 
            
                if(typologyFilter == ''){
                    BoutiqueList = intermediateList;
                }
                else{
                    BoutiqueList = new List<Boutique__c>();
                    for(Boutique__c boutique : intermediateList){
                        if(boutique.Typology__c == typologySearch){
                            BoutiqueList.add(boutique);
                        }
                    }
                }
            }
          }
      else{
        if (BoutiqueList == null){
                List<Boutique__c> intermediateList = [Select r.boutique_location_account__r.Market__r.Id
                , r.Market__c
                , r.Name
                , r.Status__c
                , r.Default_Image__c
                , r.Boutique_Last_Refurbishment_date__c
                , r.Boutique_Opening_date__c
                , r.boutique_location_account__r.City_English__c
        , r.boutique_location_account__r.BillingCity
                , r.boutique_location_account__r.Country_English__c
                , r.Image_doc_ID__c
                , r.Id
                , r.Typology__c
                , r.Address__c From Boutique__c r
                                where r.boutique_location_account__r.Market__r.Id =: selectedMarket.Id
                                and r.Status__c != 'Cancelled'
                 and RecordtypeId!=:ncafeRecordtypeId
                                and (r.Name like :('%' + boutiqueSearch + '%')) 
                ORDER BY r.boutique_location_account__r.BillingCity,r.Status__c]; 
            
                if(typologyFilter == ''){
                    BoutiqueList = intermediateList;
                }
                else{
                    BoutiqueList = new List<Boutique__c>();
                    for(Boutique__c boutique : intermediateList){
                        if(boutique.Typology__c == typologySearch){
                            BoutiqueList.add(boutique);
                        }
                    }
                }
            }

      }
            return BoutiqueList;
        }
        set;
    }
  
    public void filterBoutiques(){
        BoutiqueList = null;
    }
    
     private void initFromCampaignMarkets(){
        List<Market__c> fullMarketList = [Select r.Name, r.Zone__c, r.Id From Market__c r order by r.Name]; 
        for (Market__c cm : fullMarketList)
        {
            marketOptions.add(new SelectOption(cm.Id, cm.Name));
        }
        if (selectedMarket.Id ==null && fullMarketList.size() > 0){
            selectedMarket = fullMarketList[0];
        }
     }
    
    
    //***CONSTRUCTOR***//
    public BoutiqueNetworkController(ApexPages.StandardController stdController) {
        selectedMarket = (Market__c) stdController.getRecord();
        repeatBoutique = new Boutique__c();
        
        showHeader = true;
         if (ApexPages.CurrentPage().getParameters().containsKey('showHeader')){
            showHeader = Boolean.valueOf(ApexPages.CurrentPage().getParameters().get('showHeader'));
        }
        
        initFromCampaignMarkets();
    }

        
    public PageReference filter(){
        PageReference ref = ApexPages.CurrentPage();
        ref.getParameters().put('Id', selectedMarket.Id);
        ref.setRedirect(true);
        return ref;
    }
    
   
    
}
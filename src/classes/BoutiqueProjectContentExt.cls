/*****
*   @Class      :   BoutiqueProjectContentExt.cls
*   @Description:   Controller class of the BoutiqueProjectContentExt VF page
*   @Author     :   PROMILA/PRIYA
*   @Created    :   08 MAR 2016
*
*   Modification Log:  
*   -----------------------------------------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
    Promila         9-March-2016    Implemented Folder number functionality
*   -----------------------------------------------------------------------------------------------------------------------------------------------------------    
*   Priya           2-Jan-2017      Updated class to show only "Full Design Package" folder and it's subfolder to ERTAIL HQPM/MPM user
*****/

 public with sharing class BoutiqueProjectContentExt {
       public Map<String,String> mapCusSettingFolderName;
       public Map<String,Attachment_Folder__c> mapAttFolderName;
       public Map<String,Attachment_Folder__c> mapContentPageFolder {get;set;}
       public List<String>allFolders{get;set;}
      
       public BoutiqueProjectContentExt(ApexPages.StandardController stdController) {
            RECO_Project__c rcPrj = (RECO_Project__c)stdController.getRecord();
            allFolders=new List<String>();
            mapCusSettingFolderName = new Map<String,String>();
            mapAttFolderName = new Map<String,Attachment_Folder__c>();
            mapContentPageFolder=new Map<String,Attachment_Folder__c>();
            integer counter = 0,alphaCounter=0;
            String foldName;
            List<String>alphaList=new List<String>{'a','b','c','d','e','f','g','h','i','j'};
            
            /***Below code is used to fetch all folders from custom settings ***/
            for(Custom_Folders__c cs:[SELECT Name,Folder_Number__c from Custom_Folders__c where Folder_Number__c!='' ORDER BY Folder_Number__c ASC])
            {
                mapCusSettingFolderName.put(cs.Folder_Number__c,cs.Name);
            }
            //system.debug('===SIZE==='+mapCusSettingFolderName.size()+'===mapCusSettingFolderName CUSTOM SETTING MAP=='+mapCusSettingFolderName);  
            /**** Below code is used to fetch folders from RECO Project *****/
            
            /********Priya - 2-Jan'16: Added below if-else condition for ERTAIL Req#68***********/
            Profile p=[Select Name from Profile where Id =: userinfo.getProfileid()];
            String pname=p.name;
            if(pname!=null){
                if(pname=='ERTAIL HQPM' || pname=='ERTAIL MPM'){
                    Attachment_Folder__c attachFolder=[SELECT Id,Name,Actual_File_Count__c from Attachment_Folder__c where RECO_Project__c=:rcPrj.Id and name='Full Design Package'];
                    mapAttFolderName.put(attachFolder.Name,attachFolder);
                }
                else{
                    for(Attachment_Folder__c attFolder:[SELECT Id,Name,Actual_File_Count__c from Attachment_Folder__c where RECO_Project__c=:rcPrj.Id]){
                    mapAttFolderName.put(attFolder.Name,attFolder);
                    }
                }
            }
            /*********Priya: Code ends here****************/
            
           /**** Below code is building a map with Foldername(along-with numbering) and related folder info ***/
            for(String folderNumber:mapCusSettingFolderName.keySet()){
                String folderName=mapCusSettingFolderName.get(folderNumber);
               /* if(mapAttFolderName.containsKey(folderName)){
                    system.debug('INSIDE MAIN'+folderName);
                    
                    
                    if(folderNumber.contains('.')){
                        foldName=counter+'.'+alphaList.get(alphaCounter)+' '+folderName;
                        alphaCounter++;
                    }
                    else{
                        counter++;
                        alphaCounter=0;
                        foldName=counter+'. '+folderName;
                        
                    }
                    allFolders.add(foldName);
                    mapContentPageFolder.put(foldName,mapAttFolderName.get(folderName));
                }*/
                if(mapAttFolderName.containsKey(folderName)){
                    allFolders.add(folderName);
                    mapContentPageFolder.put(folderName,mapAttFolderName.get(folderName));
                }
            }
          //system.debug('===FINAL LIST==='+allFolders);
          //system.debug('===SIZE==='+mapContentPageFolder.size()+'===mapContentPageFolder FINAL MAP=='+mapContentPageFolder); 
        }

}
public with sharing class BoutiqueReportHelper {
    public static String EUROPE_STR = 'EUROPE';
    public static String EUROPE_SHORTSTR = 'EU';
    public static String AOA_STR = 'AOA';
    public static String AMS_STR = 'AMS';
    public static String USA_STR = 'USA'; //added as per req. 127 part1 
    public static String FRA_STR = 'FRA'; //added as per req. 127 part1
    public static String DETAILTITLE_LATESTOPENINGS = 'Latest openings';
    public static String DETAILTITLE_UPCOMINGOPENINGS = 'Upcoming openings';
    
    public static Set<String> BOUTIQUESTYPOLOGYTOCONSIDER = new Set<String>{'Boutique', 'Boutique bar', 'Boutique corner', 'Boutique in mall', 'Boutique in shop', 'N Cube', 'N Cube Indoor', 'N Cube Outdoor', 'Boutique in airport','Popup Indoor','Popup Outdoor'}; //added 'Boutique Pop-up' for req. 121 
    
    public static Set<String> PROJECTTYPE_REFURBISHMENT = new Set<String>{'Major Refurbishment', 'Minor refurbishment', 'Full refurbishment'};
    public static Set<String> PROJECTTYPE_EXTENSION = new Set<String>{'Extension'};
    public static Set<String> PROJECTTYPE_CONCEPTCHANGE = new Set<String>{'Concept Upgrade'};
    public static Set<String> PROJECTTYPE_NEW = new Set<String>{'New', 'Relocation'};
    public static Set<String> BOUTIQUESTATUSCLOSED = new Set<String>{'Closed', 'To be closed', 'To be closed-reallocated', 'Planned Closure'};
    
    public static String DASHBOARDTITLE_NEWOPENINGS = 'New openings';
    public static String DASHBOARDTITLE_EXTENSIONS = 'Extensions';
    public static String DASHBOARDTITLE_REFURBISHMENTS = 'Refurbishments';
    public static String DASHBOARDTITLE_CONCEPTCHANGE = 'Concept Update';
    
    public static Set<String> BOUTIQUESTATUSTOCONSIDER = new Set<String>{'Opened', 'To be closed', 'To be closed-reallocated', 'Planned Closure','Closed for Refurbishment'};
    
    public static String COLOR_GREEN = 'green';
    public static String COLOR_BLACK = 'black';
    public static String COLOR_RED = 'red';

    //private static Set<String> EUROPEANZONES = new Set<String>{'NCE','France','SWE', 'EUR'}; //commented for Req. 127
    private static Set<String> EUROPEANZONES = new Set<String>{'EUR'}; //added as per Req. 127
    
    public static List<String> TYPOLOGYORDER = new List<String>{'B', 'BIM', 'BIS', 'BB', 'N Cube', 'Popup', 'Standard'}; //Added Standard for NCAFE req.

    public static Map<String, String> TypologyShortMap = new Map<String, String> { 'boutique' => 'B'
                                                            , 'boutique bar' => 'BB'
                                                            , 'boutique corner' => 'BIS'
                                                            , 'boutique in mall' => 'BIM'
                                                            , 'boutique in shop' => 'BIS'
                                                            , 'n cube' => 'N Cube'
                                                            , 'n cube indoor' => 'N Cube'
                                                            , 'n cube outdoor' => 'N Cube'
                                                            , 'boutique in airport' => 'BIM' 
                                                            , 'popup indoor' => 'Popup'
                                                            , 'popup outdoor' => 'Popup' 
                                                            , 'standard' => 'Standard'}; //added Popup for req. 121 to display Pop-up boutiques in monthly report and added for NCAFE typology


    public static Map<String, String> TypologyShortLabelMap = new Map<String, String> { 'B' => 'Boutique'
                                                            , 'BB' => 'Boutique bar'
                                                            , 'BIS' => 'Boutique in shop'
                                                            , 'BIM' => 'Boutique in mall'
                                                            , 'N Cube' => 'N Cube'
                                                            , 'Popup' => 'Popup' 
                                                            , 'Standard' => 'Standard'}; //added popup for req. 121 to display Pop-up boutiques in monthly report and added for Ncafe typology

    public static void AddTypologyForPie(Boutique__c boutique, Map<String, Integer> typologyMap){
        if (boutique.Typology__c != null ){
            String typologyShort = boutique.Typology__c.toLowerCase();
            if (TypologyShortMap.containsKey(typologyShort)){
                String name = TypologyShortMap.get(typologyShort);
                if (!typologyMap.containsKey(name))
                    typologyMap.put(name, 0);
                typologyMap.put(name, typologyMap.get(name) + 1);
            }
        }
    }

    public static String GetZone(String zone){
        if (EUROPEANZONES.contains(zone))
            return EUROPE_STR;
        else
            return zone;
    }
    
    public static Set<String> GetZonesFromZone(String zone){
        if (zone == EUROPE_STR){
            return EUROPEANZONES;
        } else
            return new Set<String> {zone};
    }

    public static String SetToString(Set<String> zonesSet){
        String strToReturn = '';
        for(String zone : zonesSet){
            strToReturn += '\''  + zone + '\', ';
        }
        if (strToReturn.length() > 0)
            strToReturn = strToReturn.substring(0, strToReturn.length()-2);
        return strToReturn;
    }
    
    public static String FormatInteger(Integer nb){
        String nbStr = String.valueOf(nb);
        String resultString = '';
        while(nbStr.length() > 3){
            resultString = '\'' + nbStr.substring(nbStr.length()-3, nbStr.length()) + resultString;
            nbStr = nbStr.substring(0, nbStr.length()-3);
        }
        resultString = nbStr + resultString;
        return resultString;
    }
}
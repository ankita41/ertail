public with sharing class BoutiqueReportsDispatchController {
    public List<StandardReports__c> reportlist{get;set;} /*added this line to fix the incidentRM0019065555*/
    public class Quarter{
        Date quarterStartDate;      
        public String FormatedStartDate { public get { return quarterStartDate.year() + '-' + quarterStartDate.month() + '-' + quarterStartDate.day(); } }
        
        public Quarter(Date startDate){
            quarterStartDate = startDate;
        }
        
        public String getQuarterLabel(){
            Date quarterStart = ReportsHelper.GetPreviousQuarterStart(quarterStartDate);
            return ReportsHelper.GetQuarterNumberStr(quarterStart) + ' Quarter ' +  quarterStart.year();
        }
    }
    
    public List<Quarter> Quarters { public get; set; }

    Date PreviousMonthStart { get{ if (PreviousMonthStart == null) PreviousMonthStart = ReportsHelper.GetPreviousMonthStart(Date.today()); return PreviousMonthStart; } set; }
    public Integer Year  { public get { return PreviousMonthStart.year(); }}
    public String MonthStr  { public get { return datetime.newInstance(PreviousMonthStart.year(), PreviousMonthStart.month(), PreviousMonthStart.day()).format('MMMMM'); }}

    public BoutiqueReportsDispatchController(){
        Quarters = new List<Quarter>();
        Date todaii = Date.Today();
        Quarters.add(new Quarter(todaii));
        //Quarters.add(new Quarter(todaii.addMonths(-3)));
        //Quarters.add(new Quarter(todaii.addMonths(-6)));
        //Quarters.add(new Quarter(todaii.addMonths(-9)));
        /*added this line to fix the incidentRM0019065555*/
        Map<String,StandardReports__c> reportsList= StandardReports__c.getAll();
        if(reportsList!= null && !reportsList.isEmpty())    {     
            reportlist= reportsList.values();reportlist.sort();
            system.debug('reportlist@@'+reportlist);
        }
    }
}
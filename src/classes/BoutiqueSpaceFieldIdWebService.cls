global class BoutiqueSpaceFieldIdWebService{
    public class CustomException extends Exception {} 
    // Get field Id of field by Its field label
    webservice static String getFieldId(String field_label){
        // Obtain the magic ids
        PageReference p = new PageReference('/' + Boutique_Space__c.SObjectType.getDescribe().getKeyPrefix() + '/e?nooverride=1');
        String html = p.getContent().toString();
        throw new CustomException('html=' + html );
        
        Map<String, String> labelToId = new Map<String, String>();
        Matcher m = Pattern.compile('<label for="(.*?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
        while (m.find()) {
            String label = m.group(3);
            String id = m.group(1);
            if(label.equalsIgnoreCase(field_label))
                return id; // return field Id.
        }
        return '';
    }
}
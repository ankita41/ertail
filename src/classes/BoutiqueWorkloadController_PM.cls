/*****
*   @Class      :   BoutiqueWorkloadController_PM.cls
*   @Description:   Controller for the BoutiqueWorkload_PM page
*   @Author     :   Thibauld
*   @Created    :   16 JULY 2014
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer        Date            Description
*   Chirag           11 June 15      Show 'Sum of Final CHF' as total of Cost Items instead of Expected items
    Promila          20 Apr 16       Added filter criteria for NCAFE
*   ----------------------------------------------------------------------------------------------------------------------------
*
*                         
*****/
public without sharing class BoutiqueWorkloadController_PM {
    
    public class CustomException extends Exception {}
    
    public class ProjectWrapper{
        public Date OpeningDate { get; private set;}
        public Id ProjectId{ get; private set;}
        public Id ProjectManagerId { get; private set;}
        public String ProjectManagerName { get; private set;}
        public String BusinessOwnerName { get; private set;}
        public String Status { get; private set;}
        public String Country { get; private set;}
        public String City { get; private set;}
        public String Name { get; private set;}
        public String NameShort { get { return (Name != null && Name.length() > 20 ? Name.substring(0,20) : Name); } }
        public String ProjectType { get; private set;}
        public String ProjectTypeShort { get { return (ProjectType != null && ProjectType.length() > 10 ? ProjectType.substring(0,10) : ProjectType); } }
        public Decimal SqrM2 { get; private set;}
        public Decimal SumOfFinalCHF { get; private set;}
        public Decimal CostPerSqrM2 { get { return ((SqrM2!=null && SqrM2!=0) ? SumOfFinalCHF / SqrM2 : null);}}
        public String BoutiqueCAPEXRequestStatus { get; set; }
        private String typology;
        
        private double totalCAPEXY1;
        private double totalCAPEXY2;
        private double extraCAPEXY1;
        private double extraCAPEXY2;
        
        public Decimal TotalCAPEX { get { return totalCAPEXY1 + totalCAPEXY2 + extraCAPEXY1 + extraCAPEXY2; }  }
        
        public String TypologyShort { 
            get { 
                if(BoutiqueReportHelper.TypologyShortMap.containsKey(typology.toLowerCase()))
                    return BoutiqueReportHelper.TypologyShortMap.get(typology.toLowerCase());
                else
                    return typology; 
            } 
        }
                
        public ProjectWrapper(RECO_Project__c r){
            ProjectId = r.Id;
            ProjectManagerId = (r.Retail_project_manager__c != null ? r.Retail_project_manager__c : '!');
            ProjectManagerName = (r.Retail_project_manager__r.Name != null ? r.Retail_project_manager__r.Name : '!');
            Name = r.Name;
            Status = (r.Status__c != null ? r.Status__c : '!');
            OpeningDate = r.OpeningFormula__c;
            Country = ((r.Boutique__c != null && r.Boutique__r.boutique_location_account__c != null && r.Boutique__r.boutique_location_account__r.BillingCountry != null) ? r.Boutique__r.boutique_location_account__r.BillingCountry : '!');
            City = ((r.Boutique__c != null && r.Boutique__r.boutique_location_account__c != null && r.Boutique__r.boutique_location_account__r.BillingCity != null) ? r.Boutique__r.boutique_location_account__r.BillingCity : '!');
            ProjectType = (r.Project_Type__c != null ? r.Project_Type__c : '!');
            //SqrM2 = r.Ref_surface_m2__c; Net_Selling_m2__c
            SqrM2 = r.Net_Selling_m2__c; //modified for the fix incident RM0018689778           
            //SumOfFinalCHF = r.Total_Approved_Estimated_Items_CHF__c; // Commented by Chirag Ref RM0018642285/RM0018642291 
            SumOfFinalCHF = r.Total_cost_items_CHF__c; // Added by Chirag Ref RM0018642285/RM0018642291 
            typology = (r.Boutique__c != null && r.Boutique__r.Typology__c != null ? r.Boutique__r.Typology__c : '!');
            totalCAPEXY1 = (r.Total_CapEx_Y1_CHF__c != null ? r.Total_CapEx_Y1_CHF__c : 0);
            totalCAPEXY2 = (r.Total_CapEx_Y2_CHF__c != null ? r.Total_CapEx_Y2_CHF__c : 0);
            extraCAPEXY1 = (r.Extra_CapEx_Y1_CHF__c != null ? r.Extra_CapEx_Y1_CHF__c : 0);
            extraCAPEXY2 = (r.Extra_CapEx_Y2_CHF__c != null ? r.Extra_CapEx_Y2_CHF__c : 0);
            BusinessOwnerName = (r.Boutique__c != null && r.Boutique__r.Business_owner__c != null ? r.Boutique__r.Business_owner__r.Name : '');
            BoutiqueCAPEXRequestStatus = (r.Boutique_CapEx_request_status__c != null ? r.Boutique_CapEx_request_status__c : '!');
        }
    }
    
    public class ProjectsForCityWrapper{
        public String CityName { get; private set;}
        public List<ProjectWrapper> Projects {get; private set;}
        public Integer NbProjects { get{ return (Projects != null ? Projects.size() : 0); } }
        
        public ProjectsForCityWrapper(ProjectWrapper project){
            CityName = project.City;
            Projects = new List<ProjectWrapper>();
            addProject(project);
        }
        
        public void addProject(ProjectWrapper project){
            Projects.add(project);
        }
    }
    
    public class ProjectsForCountryWrapper{
        public String CountryName { get; private set;}
        public Map<String, ProjectsForCityWrapper> ProjectsForCities {get; private set;}
        public List<String> SortedCities { get { List<String> sortedC = new List<String>(ProjectsForCities.keySet()); sortedC.sort(); return sortedC; } }
        public Integer NbProjects { get; private set;}
                
        public ProjectsForCountryWrapper(ProjectWrapper project){
            CountryName = project.Country;
            ProjectsForCities = new Map<String, ProjectsForCityWrapper>();
            NbProjects = 0;
            addProject(project);
        }
        
        public void addProject(ProjectWrapper project){
            if (project.City != null){
                if (!ProjectsForCities.containsKey(project.City))
                    ProjectsForCities.put(project.City, new ProjectsForCityWrapper(project));
                else
                    ProjectsForCities.get(project.City).addProject(project);
            }
            NbProjects += 1;
        }
    }
    
    public class ProjectsForStatusWrapper{
        public String Status { get; private set;}
        List<ProjectWrapper> Projects {get; private set;}
        public Integer NbProjects {get { return (Projects != null ? Projects.size() : 0 );}} 
                
        public ProjectsForStatusWrapper(ProjectWrapper project){
            Projects = new List<ProjectWrapper>();
            addProject(project);
        }
        
        public void addProject(ProjectWrapper project){
            Status = project.Status;
            Projects.add(project);
        }
    }
    
    public class ProjectsForMonthWrapper{
        public Date OpeningMonth {get; private set;} 
        public Map<String, ProjectsForCountryWrapper> ProjectsForCountries {get; private set;}
        //public Map<String, ProjectsForStatusWrapper> ProjectsForStatus {get; private set;}
        
        public Integer NbProjects { get; private set; }
        //public Integer NbStatus { get { return ProjectsForStatus.size(); } }
        public List<String> SortedCountries { get { List<String> sortedC = new List<String>(ProjectsForCountries.keySet()); sortedC.sort(); return sortedC; } }
        //public List<String> SortedStatus { get { List<String> sortedS = new List<String>(ProjectsForStatus.keySet()); sortedS.sort(); return sortedS; } }
        
        public ProjectsForMonthWrapper(ProjectWrapper project){
            ProjectsForCountries = new Map<String, ProjectsForCountryWrapper>();
            //ProjectsForStatus = new Map<String, ProjectsForStatusWrapper>();
            NbProjects = 0;
            OpeningMonth = Date.newInstance(project.OpeningDate.year(), project.OpeningDate.month(), 1);
            addProject(project);
        }
        
        public void addProject(ProjectWrapper project){
            if (project.Country != null){
                if (!ProjectsForCountries.containsKey(project.Country))
                    ProjectsForCountries.put(project.Country, new ProjectsForCountryWrapper(project));
                else
                    ProjectsForCountries.get(project.Country).addProject(project);
            }
            /*
            if (project.Status != null){
                if (!ProjectsForStatus.containsKey(project.Status))
                    ProjectsForStatus.put(project.Status, new ProjectsForStatusWrapper(project));
                else
                    ProjectsForStatus.get(project.Status).addProject(project);
            }
            */
            NbProjects += 1;
        }
    }
    
    public class ProjectsForYearWrapper{
        public Integer Year {get; private set;}
        //public Map<String, ProjectsForRetailProjectManagerWrapper> ProjectsForRetailProjectManagers {get; private set;}
        //public List<String> SortedRPM { get { List<String> sortedM = new List<String>(ProjectsForRetailProjectManagers.keySet()); sortedM.sort(); return sortedM; } }
        public Integer NbProjects { get; private set; } 
        
        // NEW
        public Map<String, ProjectsForStatusWrapper> ProjectsForStatus {get; private set;}
        public Integer NbStatus { get { return ProjectsForStatus.size(); } }
        public List<String> SortedStatus { get { List<String> sortedS = new List<String>(ProjectsForStatus.keySet()); sortedS.sort(); return sortedS; } }
        
        public Map<Integer, ProjectsForMonthWrapper> ProjectsForMonths  {get; private set;}
        public Integer NbMonths { get {return ProjectsForMonths.size(); } }
        public List<Integer> SortedMonths { get { List<Integer> sortedM = new List<Integer>(ProjectsForMonths.keySet()); sortedM.sort(); return sortedM; } }
        public Decimal TotalSum { get; private set; }
        public Decimal TotalSqm { get; private set; }
        public Decimal TotalCAPEX { get; private set; }
        public Decimal avgCostPerSqm{get;private  set;} //added Avg: for the incident No:RM0018689778
        public Integer avgNumber{get; private set;}  //added Avg: for the incident No:RM0018689778
        
        public Decimal averagePerSQM{get { return ((avgCostPerSqm!= null &&avgCostPerSqm!=0 && avgNumber!= null && avgNumber!= 0) ? avgCostPerSqm/ avgNumber: 0); }} //added Avg: for the incident No:RM0018689778
        public Decimal TotalCostPerSqm { get { return ((TotalSum != null && TotalSqm != null && TotalSqm != 0) ? TotalSum / TotalSqm : null); } }
        public Decimal TotalCAPEXPerSqm { get { return ((TotalCAPEX != null && TotalSqm != null && TotalSqm != 0) ? TotalCAPEX / TotalSqm : null); } }
        public Decimal NbProjectsPerMonth { get { return (NbProjects != null ? NbProjects / 12 : null); } }
        public Decimal TotalCAPEXPerMonth { get { return (TotalCAPEX != null ? TotalCAPEX / 12 : null); } }
        public Decimal TotalSqmPerMonth { get { return (TotalSqm != null ? TotalSqm / 12 : null); } }
        
        public Decimal NbOpenedProjects { 
                get { 
                    Integer nbCompleted = (ProjectsForStatus.containsKey('Completed') ? ProjectsForStatus.get('Completed').NbProjects : 0);
                    Integer nbCancelled = (ProjectsForStatus.containsKey('Cancelled') ? ProjectsForStatus.get('Cancelled').NbProjects : 0);
                    return (NbProjects != null ? NbProjects - nbCompleted - nbCancelled : null); 
                } 
            }
                
        public ProjectsForYearWrapper(ProjectWrapper project){
            Year = project.OpeningDate.Year();
            NbProjects = 0;
            //ProjectsForRetailProjectManagers = new Map<String, ProjectsForRetailProjectManagerWrapper>();
            // New
            ProjectsForMonths = new Map<Integer, ProjectsForMonthWrapper>();
            ProjectsForStatus = new Map<String, ProjectsForStatusWrapper>();
            TotalSum = 0;
            TotalSqm = 0;
            TotalCAPEX = 0;
            avgNumber=0;      //added Avg: for the incident No:RM0018689778      
            avgCostPerSqm=0; //added Avg: for the incident No:RM0018689778
            addProject(project);
        }
        
        public void addProject(ProjectWrapper project){
            
            /*
            if (!ProjectsForRetailProjectManagers.containsKey(project.ProjectManagerName))
                ProjectsForRetailProjectManagers.put(project.ProjectManagerName, new ProjectsForRetailProjectManagerWrapper(project));
            else
                ProjectsForRetailProjectManagers.get(project.ProjectManagerName).addProject(project);
            */
            if (!ProjectsForMonths.containsKey(project.OpeningDate.Month()))
                ProjectsForMonths.put(project.OpeningDate.Month(), new ProjectsForMonthWrapper(project));
            else
                ProjectsForMonths.get(project.OpeningDate.Month()).addProject(project);
            
            if (project.Status != null){
                if (!ProjectsForStatus.containsKey(project.Status))
                    ProjectsForStatus.put(project.Status, new ProjectsForStatusWrapper(project));
                else
                    ProjectsForStatus.get(project.Status).addProject(project);
            }
            TotalSum += (project.SumOfFinalCHF != null ? project.SumOfFinalCHF : 0);
            TotalSqm += (project.SqrM2 != null ? project.SqrM2 : 0);
            //start - added Avg: for the incident No:RM0018689778           
            avgCostPerSqm += (project.CostPerSqrM2 != null && project.CostPerSqrM2!=0 ?project.CostPerSqrM2:0) ;    
            if(project.CostPerSqrM2 != null && project.CostPerSqrM2 != 0){
                avgNumber+=1;            
             }  
            // end --added Avg: for the incident No:RM0018689778
            TotalCAPEX += project.TotalCAPEX;
            NbProjects += 1;
        }
    }
    
    public class ProjectsForRetailProjectManagerWrapper{
        public Id ProjectManagerId {get; private set;}
        //public Map<Integer, ProjectsForMonthWrapper> ProjectsForMonths  {get; private set;}
        public Integer NbProjects { get; private set; }
        //public Integer NbStatus { get{ Integer n = 0; for(ProjectsForMonthWrapper p : ProjectsForMonths.values()) n += p.NbStatus; return n;} }
        //public Integer NbMonths { get {return ProjectsForMonths.size(); } }
        
        // NEW
        public Map<Integer, ProjectsForYearWrapper> ProjectsForYear  {get; private set;}
        public List<Integer> SortedYears { get { List<Integer> sortedY = new List<Integer>(ProjectsForYear.keySet()); List<Integer> sortedDsc = new List<Integer>(); sortedY.sort(); for(Integer i = sortedY.size()-1 ; i>=0 ; i--) sortedDsc.add(sortedY.get(i)); return sortedDsc; } }
        
        //public Decimal TotalSum { get; private set; }
        //public Decimal TotalSqm { get; private set; }
        //public Decimal CostPerSqm { get { return ((TotalSqm != null && TotalSqm != 0) ? TotalSum / TotalSqm : null); } }
        
        //public List<Integer> SortedMonths { get { List<Integer> sortedM = new List<Integer>(ProjectsForMonths.keySet()); sortedM.sort(); return sortedM; } }
         
        
        public ProjectsForRetailProjectManagerWrapper(ProjectWrapper project){
            //ProjectsForMonths = new Map<Integer, ProjectsForMonthWrapper>();
            ProjectsForYear = new Map<Integer, ProjectsForYearWrapper>();
            
            ProjectManagerId = project.ProjectManagerId;
            //TotalSum = 0;
            //TotalSqm = 0;
            NbProjects = 0;
            addProject(project); 
        }
        
        public void addProject(ProjectWrapper project){
            /*
            if (!ProjectsForMonths.containsKey(project.OpeningDate.Month()))
                ProjectsForMonths.put(project.OpeningDate.Month(), new ProjectsForMonthWrapper(project));
            else
                ProjectsForMonths.get(project.OpeningDate.Month()).addProject(project);
            TotalSum += (project.SumOfFinalCHF != null ? project.SumOfFinalCHF : 0);
            TotalSqm += (project.SqrM2 != null ? project.SqrM2 : 0);
            */
            Integer year = project.OpeningDate.year();  
            if (!ProjectsForYear.containsKey(year))
                ProjectsForYear.put(year, new ProjectsForYearWrapper(project));
            else
                ProjectsForYear.get(year).addProject(project);
            NbProjects += 1;
        }
    }
    
    public class ProjectsWrapper{
        /*
        public Map<Integer, ProjectsForYearWrapper> ProjectsForYear {get; private set;}
        public List<Integer> SortedYears { get { List<Integer> sortedY = new List<Integer>(ProjectsForYear.keySet()); List<Integer> sortedDsc = new List<Integer>(); sortedY.sort(); for(Integer i = sortedY.size()-1 ; i>=0 ; i--) sortedDsc.add(sortedY.get(i)); return sortedDsc; } }
        */
        // NEW
        public Map<String, ProjectsForRetailProjectManagerWrapper> ProjectsForRetailProjectManagers {get; private set;}
        public List<String> SortedRPM { get { List<String> sortedM = new List<String>(ProjectsForRetailProjectManagers.keySet()); sortedM.sort(); return sortedM; } }
         
        public Integer NbProjects { get; private set; }
        
        public ProjectsWrapper(){
            //ProjectsForYear = new Map<Integer, ProjectsForYearWrapper>();
            // New
            ProjectsForRetailProjectManagers = new Map<String, ProjectsForRetailProjectManagerWrapper>();
            NbProjects = 0;
        }
        
        public void addProject(ProjectWrapper project){
            /*
            Integer year = project.OpeningDate.year();  
            if (!ProjectsForYear.containsKey(year))
                ProjectsForYear.put(year, new ProjectsForYearWrapper(project));
            else
                ProjectsForYear.get(year).addProject(project);
            */
            // NEW
             if (!ProjectsForRetailProjectManagers.containsKey(project.ProjectManagerName))
                ProjectsForRetailProjectManagers.put(project.ProjectManagerName, new ProjectsForRetailProjectManagerWrapper(project));
            else
                ProjectsForRetailProjectManagers.get(project.ProjectManagerName).addProject(project);
                
            NbProjects += 1;
        }
    }
    
    private void initProjectWrappers(){
        projects = new ProjectsWrapper();
        String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name; //Added for NCAFE
        String ncafeRecordtypeId=Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId(); //Added for NCAFE
        String qryStr = 'Select r.Total_Approved_Estimated_Items_CHF__c'
                                + ', r.Total_cost_items_CHF__c,r.Net_Selling_m2__c'     //Added by Chirag Ref RM0018642285/RM0018642291
                                + ', r.Status__c'
                                + ', r.Retail_project_manager__c'
                                + ', r.Retail_project_manager__r.Name'
                                + ', r.Ref_surface_m2__c'
                                + ', r.Project_Type__c'
                                + ', r.Total_CapEx_Y2_CHF__c'
                                + ', r.Total_CapEx_Y1_CHF__c'
                                + ', r.Extra_Capex_Y2_CHF__c'
                                + ', r.Extra_Capex_Y1_CHF__c'
                                + ', r.Boutique_CapEx_request_status__c'
                                + ', r.Name'
                                + ', r.OpeningFormula__c'
                                + ', r.Boutique__r.boutique_location_account__c'
                                + ', r.Boutique__c'
                                + ', r.Boutique__r.Typology__c'
                                + ', r.Boutique__r.Business_owner__r.Name'
                                + ', r.Boutique__r.boutique_location_account__r.BillingCity'
                                + ', r.Boutique__r.boutique_location_account__r.BillingCountry' 
                        + ' From RECO_Project__c r'
                        + ' where r.Retail_project_manager__c != null'
                        + ' and OpeningFormula__c != null';
        if(currentUserProfile.contains('NCAFE')){
            qryStr  += ' AND r.RecordtypeId = \''+ncafeRecordtypeId+'\'';
        }
        else{
            qryStr += ' AND r.RecordtypeId != \''+ncafeRecordtypeId+'\'';
        }                    
        qryStr = queryString(qryStr);
        qryStr = qryStr + ' order by r.Retail_project_manager__r.Name asc';
        
        List<RECO_Project__c> projectList = Database.query(qryStr);                                 
                                            
        for (RECO_Project__c proj : projectList){
            ProjectWrapper project = new ProjectWrapper(proj);
            projects.addProject(project);
        }
    }
    
    public List<SelectOption> statusOpts { get; private set;}
    public String selectedStatus {get;set;}    
    public List<SelectOption> projectTypesOpts { get; private set;}
    public String selectedProjectTypes {get;set;}    
    public List<SelectOption> boutiqueTypesOpts { get; private set;}
    public String selectedBoutiqueTypes {get;set;}
    public List<SelectOption> yearsOpts { get; private set;}
    public String selectedYears {get;set;}
    public List<SelectOption> rpmOpts { get; private set;}
    public String selectedRPMs {get;set;}
    public ProjectsWrapper projects { get; private set;}
    public Boolean printMode {get;set;}
        
    public BoutiqueWorkloadController_PM(){
        projects = new ProjectsWrapper();
        selectedStatus = 'All';
        selectedProjectTypes = 'All';
        selectedBoutiqueTypes = 'All';
        selectedYears = 'All';
        selectedRPMs = 'All';
        printMode = false;
        queryFilters();
    }
    
    public void queryProjectsData(){
        initProjectWrappers();
    }
    
    private void queryFilters(){
        Set<String> distinctStatus = new Set<String>();
        Set<String> distinctProjectTypes = new Set<String>();
        Set<String> distinctBoutiqueTypes = new Set<String>();
        Set<String> distinctYears = new Set<String>();
        Map<String, Id> distinctRPM = new Map<String, Id>();
        String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name; //Added for NCafe
        String ncafeRecordtypeId=Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId(); //Added for NCafe
        
        statusOpts = new List<SelectOption>();
        projectTypesOpts = new List<SelectOption>();
        boutiqueTypesOpts = new List<SelectOption>();
        yearsOpts = new List<SelectOption>();
        rpmOpts = new List<SelectOption>();
        String qryStr='SELECT Retail_Project_Manager__c, Retail_Project_Manager__r.Name, Status__c, Project_Type__c, Boutique__c, Boutique__r.Typology__c, OpeningFormula__c  FROM RECO_Project__c';
        if(currentUserProfile.contains('NCAFE')){
             qryStr+= ' Where RecordtypeId = \''+ncafeRecordtypeId+'\'';
        }
        else{
             qryStr += ' Where RecordtypeId != \''+ncafeRecordtypeId+'\'';
        }   
        for(RECO_Project__c p : Database.query(qryStr)){
            distinctStatus.add(p.Status__c);
            if (p.Project_Type__c != null)
                distinctProjectTypes.add(p.Project_Type__c);
            if (p.Boutique__c != null)
                distinctBoutiqueTypes.add(p.Boutique__r.Typology__c);
            if (p.OpeningFormula__c != null)
                distinctYears.add(String.valueOf(p.OpeningFormula__c.year()));
            if (p.Retail_Project_Manager__c != null)
                distinctRPM.put(p.Retail_Project_Manager__r.Name, p.Retail_Project_Manager__c);
        }
        
        for(String s : distinctStatus){
            statusOpts.add(new SelectOption(s, s));
        }
        statusOpts.sort();
        
        for(String s : distinctProjectTypes){
            projectTypesOpts.add(new SelectOption(s, s));
        }
        projectTypesOpts.sort();
        
        for(String s : distinctBoutiqueTypes){
            boutiqueTypesOpts.add(new SelectOption(s, s));
        }
        boutiqueTypesOpts.sort();
        
        List<SelectOption> tmpYearList = new List<SelectOption>();
        for(String s : distinctYears){
            tmpYearList.add(new SelectOption(s, s));
        }
        // Sort the list desc
        tmpYearList.sort();
        yearsOpts.clear();
        for (Integer i = tmpYearList.size() -1 ; i >= 0 ; i--)
            yearsOpts.add(tmpYearList.get(i));
        
        List<String> rpmNames = new List<String>(distinctRPM.keySet());
        rpmNames.sort();
        
        for(String s : rpmNames){
            rpmOpts.add(new SelectOption(distinctRPM.get(s), s));
        }
            
        
    }
    
    private String queryString(String str){
        if(!selectedStatus.contains('All')){
            str += ' AND Status__c IN (';
            str += splitSelectedStrings(selectedStatus);
        }
        
        if(!selectedProjectTypes.contains('All')){
            str += ' AND Project_Type__c IN (';
            str += splitSelectedStrings(selectedProjectTypes);
        }
        
        if(!selectedBoutiqueTypes.contains('All')){
            str += ' AND Boutique__r.Typology__c IN (';
            str += splitSelectedStrings(selectedBoutiqueTypes);
        }
        
        if(!selectedYears.contains('All')){
            str += ' AND CALENDAR_YEAR(OpeningFormula__c) IN (';
            str += splitSelectedIntegers(selectedYears);
        }
        
        if(!selectedRPMs.contains('All')){
            str += ' AND Retail_Project_Manager__c IN (';
            str += splitSelectedStrings(selectedRPMs);
        }
            
        return str;
    }
    
    private String splitSelectedStrings(String selectedValues){
        String[] tempStr = selectedValues.replace('[','').replace(']','').trim().split(',');
        String returnStr = '';
        for(String s : tempStr){
            returnStr += '\'' + s.replaceAll('\'','\\\\\'').trim() + '\',';
        }
        return returnStr.removeEnd(',') + ') ';
    }
    
    private String splitSelectedIntegers(String selectedValues){
        String[] tempStr = selectedValues.replace('[','').replace(']','').trim().split(',');
        String returnStr = '';
        for(String s : tempStr){
            returnStr += s + ',';
        }
        return returnStr.removeEnd(',') + ') ';
    }
    
    public void printPage(){
        printMode = true;
    }
}
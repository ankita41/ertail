/*****
*   @Class      :   BoutiquesMapController.cls
*   @Description:   Controller for the ShowBoutiquesMap page
*   @Author     :   Greg Baxter
*   @Created    :   23 OCT 2013
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*	Jacky Uy		25 JUL 2014		Fixed: Too many query rows error
*                         
*****/

public with sharing class BoutiquesMapController {
    public String getMapURL() {
        List<Boutique__c> boutiquesList = [SELECT Id, boutique_location_account__c FROM Boutique__c];
        
        Set<Id> accIds = new Set<Id>();
        for(Boutique__c btq : boutiquesList){
        	accIds.add(btq.boutique_location_account__c);
        }
        
        Map<Id,Account> accts = new Map<Id,Account>([SELECT Id, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE Id IN :accIds]);
        Account pos;
        
        String url = 'http://maps.google.com/maps/api/staticmap?size=800x500&markers=color:blue';
        for(Boutique__c btq : boutiquesList) {
            pos = accts.get(btq.boutique_location_account__c);
            url += '|' + (pos.BillingStreet != null ? pos.BillingStreet : '') + ',' + (pos.BillingCity != '' ? pos.BillingCity : '') + ',' + 
            	(pos.BillingState != null ? pos.BillingState : '') + ' ' + (pos.BillingPostalCode != null ? pos.BillingPostalCode : '') + ',' + 
            	(pos.BillingCountry != null ? pos.BillingCountry : '');
        }
        url+= '&sensor=false&format=png32';
        
        return url;
    }

    public String mapURL {set;}
}
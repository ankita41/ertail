/****
Description:This class updates RecoProject for currency conversions
Admin would need to run the batch manually to reflect the new values based on new conversion rates for respective years
Created Date: 26/03/15
Created by: Simran Jeet Singh
*****/


global class CapexCurrencyConversionBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{

String query;
String stryear=system.today().year()+'';

global Database.querylocator start(Database.BatchableContext BC){
    query = 'SELECT Id, Boutique__c,Currency__c FROM RECO_Project__c WHERE Cutoff_Year_1__c =:stryear OR Cutoff_Year_2__c = :stryear';
    
    return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<RECO_Project__c> scope){
   list<RECO_Project__c> recoProjs = new list<RECO_Project__c>();
    for(RECO_Project__c prj : scope){
    recoProjs.add(prj);
      }
   if(recoProjs.size() > 0) update recoProjs;
}

global void finish(Database.BatchableContext BC){


}
}
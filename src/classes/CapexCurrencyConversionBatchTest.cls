/*
Created Date: 26/03/15
Created By: Simran Jeet Singh
 */
@isTest
private class CapexCurrencyConversionBatchTest {

    static testMethod void myUnitTest() {
         Test.startTest();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c btqProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        insert btqProj;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        list<RECO_Project__c> btqProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        insert btqProjs;
        
        CapexCurrencyConversionBatch ccb = new CapexCurrencyConversionBatch();
        Database.executeBatch(ccb);
        Test.stopTest();
    }
}
public class CheckBrowserCntl {
    public Boolean displayPopup {get;set;}
    public Boolean check{get;set;}
    public String errormsg{get;set;}
    Public String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
    Public User CurrentUser = UserConstants.CURRENT_USER;
    public CheckBrowserCntl(){
    }
    public PageReference getBrowserName(){
        displayPopup =true;
        check= CurrentUser.DoNotDisplayMessageAgain__c;
        String currentUserProfile = CurrentUser.Profile.Name;
        errormsg='';
        if((currentUserProfile.containsIgnoreCase('RECO') || currentUserProfile.containsIgnoreCase('NCAFE'))&& !(userAgent.containsIgnoreCase('Chrome')) && (check==false))
       // if(!(userAgent.contains('Chrome')))
       {
           //  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'<center>For a proper user experience, we invite you to use Google Chrome.<br/> With your current browser, some functionalities might be not fully available or properly working.<center>'));
             errormsg='For a proper user experience, we invite you to use Google Chrome.<br/> With your current browser, some functionalities might be not fully available or properly working.';
             displayPopup =true;
             return null;
        }
        PageReference p=new Pagereference('/a0R/o');
        p.setRedirect(true);
        return p;
    }    
    public PageReference redirectPopup(){
        displayPopup = false;
        if(Check== true){
           CurrentUser.DoNotDisplayMessageAgain__c=true;
           update CurrentUser; 
        }
        PageReference p=new Pagereference('/a0R/o');
        p.setRedirect(true);
        return p;
    }
}
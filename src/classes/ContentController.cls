public with sharing class ContentController {
    public String folderData {get; set;}
    public RECO_Project__c project {get; set;}
    public Offer__c offer {get; set;}
    public Handover__c handover {get; set;}
    public Id currentFolderId {get; set;}
    public Id currentLibraryId {get; set;}
    public Id delContentDocId {get; set;}
    public Id getContentVersionId {get; set;}
    public String folderContents {get; set;}
    public List<folderContentWrapper> folderContentsList {get; set;}
    public String defaultSelectedFolder {get; set;}
    public Boolean hasDeleteRights {
        get {
            String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
            String hasRights = 'System Admin, System Administrator, NES System Admin Platform, NES RECO Retail Project Manager';
            return hasRights.contains(currentUserProfile);
        } set;
    }
    
    public String breadCrumbs {
        get {
            return folderBreadCrumbsMap.get(currentFolderId);
        }
        set;
    }
    public String contributeButtonUrl {
        get {
            Document buttonDoc = new Document();
            try {
                buttonDoc = [SELECT Id FROM Document WHERE DeveloperName = 'Contribute_button'];
            } catch (Exception e) {}
            
            return '/servlet/servlet.FileDownload?file=' + buttonDoc.Id;
        } set;
    }
    public String sfBaseURL {
        get {
            String sBaseURL = System.URL.getSalesforceBaseUrl().getHost();
            //  Example: c.cs7.visual.force.com
            sBaseURL = sBaseURL.substringBetween('.');
            
            return 'https://' + sBaseURL + '.salesforce.com';
            
        } set;
    }
    
    private List<Content_Folder__c> allFolders;
    private Set<String> allowedFolderNames;
    private Map<Id, String> folderBreadCrumbsMap;
    private String pageName;
    
    public ContentController (ApexPages.StandardController stdController) {
        pageName = getPageName(ApexPages.currentPage().getUrl());
        system.debug('page name : ' + pageName);
        
        if (pageName == 'RECOProjectContent') {
            project = (RECO_Project__c)stdController.getRecord();
        } else if (pageName == 'RECOHandoverContent') {
            handover = (Handover__c)stdController.getRecord();
            allowedFolderNames = new Set<String> {'Visit report & Handover'};
        } else if (pageName == 'RECOOfferContent') {
            offer = (Offer__c)stdController.getRecord();
            allowedFolderNames = new Set<String> {'Budget & Costs'};
        }
        
        allFolders = [SELECT Id, Name, Parent_Folder__c FROM Content_Folder__c ORDER BY Sequence_order__c];
        Map<String, Content_Folder__c> rootFoldersMap = new Map<String, Content_Folder__c>();
        folderBreadCrumbsMap = new Map<Id, String>();
        
        for(Content_Folder__c folder : [SELECT Id, Name FROM Content_Folder__c WHERE Parent_Folder__c = null]) {
            if (allowedFolderNames == null || allowedFolderNames.contains(folder.Name)) {
                rootFoldersMap.put(folder.Name, folder);
            }
        }
        
        folderData = '';
        List<ContentWorkspace> tmpLibraries = [SELECT Id, Name FROM ContentWorkspace WHERE Description LIKE '%__RECO__%' ORDER BY Description];
        defaultSelectedFolder = '';
        
        for(ContentWorkspace lib : tmpLibraries) {
            Content_Folder__c tmpFolder = rootFoldersMap.get(lib.Name);
            if(tmpFolder != null) {
                folderData += (folderData == '' ? '' : ',\n') + '{\n\tlabel: \'' + tmpFolder.Name + '\',\nlibId: \'' + lib.Id + '\',\nid: \'' + tmpFolder.Id + '\',\nfolderId: \'' + tmpFolder.Id + '\',\nchildren: [\n';
                folderData += getFolderChildren(tmpFolder.Id, lib, tmpFolder.Name);
                folderData += ']\n}';
                
                //if(lib.Name == 'Preliminary information')
                defaultSelectedFolder = defaultSelectedFolder == '' ? string.valueOf(tmpFolder.Id) : defaultSelectedFolder;
            }
        }
    }
    
    private String getPageName(String url) {
        String pageName='';
        if(url.indexOf('/apex/') != -1) {
            if(url.indexOf('?') != -1 ) {
                pageName=url.subString(url.indexOf('/apex/')+6,url.indexOf('?'));
            } else {
                pageName=url.subString(url.indexOf('/apex/')+6);
            }
        }
        return pageName;
    }
    
    private String getFolderChildren (Id parentFolderId, ContentWorkspace library, String breadCrumbs) {
        String tmpFolderData = '';
        
        folderBreadCrumbsMap.put (parentFolderId, breadCrumbs);
        
        for (Content_Folder__c folder : allFolders) {
            if (folder.Parent_Folder__c == parentFolderId) {
                tmpFolderData += (tmpFolderData == '' ? '' : ',\n') + '{\n\tlabel: \'' + folder.Name + '\',\nlibId: \'' + library.Id + '\',\nid: \'' + folder.Id + '\',\nfolderId: \'' + folder.Id + '\',\nchildren: [\n';
                
                tmpFolderData += getFolderChildren(folder.Id, library, breadCrumbs + ' > ' + folder.Name);
                
                tmpFolderData += ']\n}';
            }
        }
        
        return tmpFolderData;
    }
    
    
    public void GetFolderContents(){
        folderContents = '';
        folderContentsList = new List<folderContentWrapper>();
        
        SYSTEM.DEBUG('currentFolderId: ' + currentFolderId);
        SYSTEM.DEBUG('currentLibraryId: ' + currentLibraryId);
        
        if (currentFolderId != null && currentLibraryId != null) {
            List<ContentVersion> documents;
            
            if (pageName == 'RECOProjectContent') {
                documents = [SELECT Id,Title, Description, OwnerId, TagCsv, ContentModifiedDate, ContentDocumentId, FileType, ContentSize, IsLatest FROM ContentVersion WHERE IsLatest = true AND Project__c = :project.Id AND Content_Folder__c = :currentFolderId AND ContentDocumentId IN (SELECT ContentDocumentId FROM ContentWorkspaceDoc WHERE ContentWorkspaceId = :currentLibraryId)];
            } else if (pageName == 'RECOHandoverContent') {
                documents = [SELECT Id,Title, Description, OwnerId, TagCsv, ContentModifiedDate, ContentDocumentId, FileType, ContentSize, IsLatest FROM ContentVersion WHERE IsLatest = true AND Handover__c = :handover.Id AND Content_Folder__c = :currentFolderId AND ContentDocumentId IN (SELECT ContentDocumentId FROM ContentWorkspaceDoc WHERE ContentWorkspaceId = :currentLibraryId)];
            } else if (pageName == 'RECOOfferContent') {
                documents = [SELECT Id,Title, Description, OwnerId, TagCsv, ContentModifiedDate, ContentDocumentId, FileType, ContentSize, IsLatest FROM ContentVersion WHERE IsLatest = true AND Offer__c = :offer.Id AND Content_Folder__c = :currentFolderId AND ContentDocumentId IN (SELECT ContentDocumentId FROM ContentWorkspaceDoc WHERE ContentWorkspaceId = :currentLibraryId)];
            }
            
            
            for (ContentVersion doc : documents) {
                folderContents += (folderContents == '' ? '' : '#@#') + doc.Title + '%%' + doc.Id;
                
                folderContentWrapper tmpFCW = new folderContentWrapper(doc);
                folderContentsList.add(tmpFCW);
            }
        }
        
    }
    
    public void DeleteDocument() {
        try {
            //ContentVersion ver = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :delContentVersionId];
            try {
                ContentDocument doc = [SELECT Id FROM ContentDocument WHERE Id = :delContentDocId];
                delete doc;
            } catch (Exception e) {}
            
            GetFolderContents();
        } catch (Exception e) {
            system.debug(e);
        }
    }
    
    public void GetDocument() {
        try {
            ContentVersion ver = [SELECT Id, VersionData, PathOnClient, Title FROM ContentVersion WHERE Id = :getContentVersionId];
            
            EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE Name='RECO Handover Checklist'];
            Messaging.SingleEmailmessage mail = new Messaging.SingleEmailmessage();
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setTemplateId(et.Id);
            mail.setSaveAsActivity(false);
    
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(ver.PathOnClient);
                efa.setBody(ver.VersionData);
                fileAttachments.add(efa);
            
            mail.setFileAttachments(fileAttachments);
    
            Messaging.sendEmail(new Messaging.SingleEmailmessage[] { mail });
            
        } catch (Exception e) {
            system.debug(e);
        }
    }
    
    public PageReference RefreshWindow(){
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('refresh','true');
        curPage.setRedirect(true);
        return curPage;
    }
    
    public class folderContentWrapper {
        public ContentVersion doc {get; set;}
        public String authorName {get; set;}
        public String fileTypeImageUrl {get; set;}
        public String fileSize {get; set;}
        
        private Map<String, String> fileTypeImageMap = new Map<String, String>{
            'BMP' => 'doctype_image_16.png',
            'CSV' => 'doctype_csv_16.png',
            'EXCEL' => 'doctype_excel_16.png',
            'EXCEL_X' => 'doctype_excel_16.png',
            'EXE' => 'doctype_exe_16.png',
            'GIF' => 'doctype_image_16.png',
            'JPG' => 'doctype_image_16.png',
            'JPEG' => 'doctype_image_16.png',
            'MOV' => 'doctype_video_16.png',
            'MP4' => 'doctype_mp4_16.png',
            'PDF' => 'doctype_pdf_16.png',
            'PNG' => 'doctype_image_16.png',
            'POWER_POINT' => 'doctype_ppt_16.png',
            'POWER_POINT_X' => 'doctype_ppt_16.png',
            'TEXT' => 'doctype_txt_16.png',
            'UNKNOWN' => 'doctype_unknown_16.png',
            'WORD' => 'doctype_word_16.png',
            'WORD_X' => 'doctype_word_16.png',
            'XML' => 'doctype_xml_16.png',
            'ZIP' => 'doctype_zip_16.png'
            };
        
        
        public folderContentWrapper(ContentVersion document){
            doc = document;
            fileTypeImageUrl = fileTypeImageMap.get(document.FileType);
            fileSize = String.valueof(document.ContentSize / 1024) + ' KB';
            
            List<User> author = [SELECT Name FROM User WHERE Id = :document.OwnerId];
            if (!author.isEmpty()) {
                authorName = author[0].Name;
            } else {
                authorName = '';
            }
        }
    }
    
    /*
     * "Details" link in the documents list
     * <a href="{!sfBaseURL}/sfc/#version?selectedDocumentId={!docWrapper.doc.ContentDocumentId}" target="_top" style="color: rgb(1, 91, 167); text-decoration: none;">Details</a>
     *
     * Link on document name for direct download
     * <a href="/sfc/servlet.shepherd/version/download/{!docWrapper.doc.Id}" target="_blank" style="color: rgb(0,0,0);font-weight:bold;"></a>
     */
     
     
}
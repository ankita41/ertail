/*****
*   @Class      :   ContentControllerTest.cls
*   @Description:   Tests - ContentController.cls, contentUploadController.cls
*   @Author     :   XXXXX
*   @Created    :   XXXXX
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        13 MAY 2014     Change: BTQ Project Record Type from 'New' to 'Boutique Project'
*   Jacky Uy        03 JUN 2014     Updated: Changed class name from TestRECOContent to ContentControllerTest
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*****/

@isTest
private class ContentControllerTest {
    static testMethod void test() {
        SYSTEM.runAs(TestHelper.CurrentUser){
            //Create Market
            Market__c market = TestHelper.createMarket();
            insert market;
            
            //Create currency exchange rates
            TestHelper.createCurrencies();
            
            Boutique__c btq;
            RECO_Project__c bProj;
            Offer__c off;
            
            System.runAs(TestHelper.RetailProjectManager) {
	            // Create Boutique Location Account
	            Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
	            Account acc = TestHelper.createAccount(rTypeId, market.Id);
	            acc.Name = 'Test BTQ Location';
	            insert acc;
            
                rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
                btq = TestHelper.createBoutique(rTypeId, acc.Id);
                insert btq;
                
                rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
                bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
                bProj.Name = 'Test BTQ Project';
                bProj.Currency__c = 'CHF';
                insert bProj;
            }
            
            Content_Folder__c cfolder = new Content_Folder__c(Name = 'Preliminary design', Parent_Folder__c = null);
            insert cfolder;
            
            Test.startTest();
            
            List<ContentWorkspace> tmpLibraries = [SELECT Id, Name FROM ContentWorkspace WHERE Name = :cfolder.Name];
            
            if (!tmpLibraries.isEmpty()) {
                PageReference projPage = new PageReference('/apex/RECOProjectContent?id=' + bProj.Id);
                Test.setCurrentPageReference(projPage);
                
                ApexPages.StandardController std = new ApexPages.StandardController(bProj);
                ContentController controller = new ContentController(std);
                controller.currentFolderId = cfolder.Id;
                controller.currentLibraryId = tmpLibraries[0].Id;
                controller.GetFolderContents();
                
                System.assertEquals(true, controller.folderContentsList.isEmpty());
                System.assertEquals(true, controller.hasDeleteRights);
                System.assertNotEquals(null, controller.project);
                System.assertEquals('Preliminary design', controller.breadCrumbs);
                
                PageReference uploadPage = new PageReference('/apex/contentUpload?projectId=' + bProj.Id + '&libId=' + tmpLibraries[0].Id + '&folderId=' + cfolder.Id);
                Test.setCurrentPageReference(uploadPage);
                
                contentUploadController controller_u = new contentUploadController();
                controller_u.files[0].fileBody = Blob.valueof('abc');
                controller_u.files[0].fileName = 'test_filename1.jpg'; 
                controller_u.files[0].fileSize = 5242880; 
                controller_u.files[0].description = 'desc';
                controller_u.go();
                
                controller_u = new contentUploadController();
                controller_u.files[0].fileBody = Blob.valueof('abc');
                controller_u.files[0].fileName = 'test_filename2.jpg'; 
                controller_u.files[0].fileSize = 5242881; 
                controller_u.files[0].description = 'desc';
                controller_u.go();
            }

            Test.stopTest();
        }
    }
}
public with sharing class ContentDownloadController {
	public ContentVersion doc {get; set;}
	public String ContentDownloadURL {
		get; set;
	}
	
	public ContentDownloadController (ApexPages.StandardController stdController) {
		doc = (ContentVersion)stdController.getRecord();
		doc = [SELECT Id, PathOnClient, VersionData, FileType FROM ContentVersion WHERE Id = :doc.Id];
		
	}
	
	public PageReference createContentDownload() {
        Content_Download__c cd = new Content_Download__c(Name = doc.PathOnClient);
		insert cd;
		
		Attachment att = new Attachment(Name = doc.PathOnClient, ParentId = cd.Id, Body = doc.VersionData);
		insert att;
		
		ContentDownloadURL = '/servlet/servlet.FileDownload?file=' + att.Id;
		
		PageReference newPage = new PageReference('/servlet/servlet.FileDownload?file=' + att.Id);
		return newPage.setRedirect(true);
    }
    
    
    /* 
     * Removes the ContentDownload objects older that 1 hour (to give the user enough time for the document download)
     */
    public static void RemoveTemporaryData(){
    	datetime dtLimit = datetime.now() - 1/24.0;
    	List<Content_Download__c> tmpList = [SELECT Id FROM Content_Download__c WHERE CreatedDate < :dtLimit];
    	delete tmpList;
    }
}
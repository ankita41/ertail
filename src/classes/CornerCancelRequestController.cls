/*****
*   @Class      :   CornerCancelRequestController .cls
*   @Description:   Controller for CornerCancelRequest.page 
*   @Author     :   Koteswararao
*   @Created    :   20-Nov-2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/
public with sharing class CornerCancelRequestController {

    

public Project__c project {get; set;}
    public CornerCancelRequestController (ApexPages.StandardController controller) {
        
        project = (Project__c)controller.getRecord();
        project = [SELECT Id,name, Approval_Status__c, Notify_Layout_Proposal__c,Project_Type__c,Cancellation_justification__c, Notify_Installation_Info_Date__c,RequestCancellation__c,Cancellation_Request_Date__c,Notify_HQ_Manager_for_cancellation__c, 
                        Status__c, Dimensions_of_the_goods_lift__c, Furniture_Delivery_Date__c, Furniture_Delivery_Time__c, Truck_with_elevator_needed__c, 
                        Agreed_Installation_Date__c, Notify_Agreed_Installation_Date__c, Notify_Agreed_Date__c, Proposed_Installation_Date__c, Notify_All_for_Closing__c, 
                        Actual_Installation_Date__c, Architect__c, Supplier__c, Corner__c, Notify_Proposed_Installation_Schedule__c, Cancellation_reason__c, 
                        Notify_Proposed_Installation_Date__c, Floor_level__c, Delivery_needs_to_be_booked__c, Notify_Draft_Request__c, Notify_Draft_Request_Date__c, 
                        AllowInitiateProject__c, Workflow_Type__c, Supplier_Proposal__c, Next_Process_Step__c,Cancellation_comments__c,
                        On_Hold_Set_Date__c, On_Hold_Original_Status__c, On_Hold_Previous_Step__c, Notify_Site_Survey_Submitted__c
                        , Proposed_Site_Survey_Date__c, Corner_Point_of_Sale_r__c, Resp_for_Site_Survey__c, 
                        Water_supply_needed_r__c, Walls_material_r__c, Visual__c, Targeted_Installation_Date__c, Old_NES_Corner_to_remove__c
                        , Nb_of_machines_in_stock__c, Nb_of_machines_in_display__c, Product_Range__c
                        , NES_Sales_Promoter__c, LCD_screen__c, Height_under_ceiling_r__c, isMassCorner__c,Floor_material_r__c, Floor__c, Electricity_coming_from_r__c
                        , Ecolaboration_module__c, Is_POS_Added__c,Corner_Unit_r__c, Corner_Type_r__c, Corner_Generation_r__c, RecordType.DeveloperName
                        FROM Project__c WHERE Id = :project.Id];
        if(project != null && project.Cancellation_Request_Date__c ==null){
           project.Cancellation_Request_Date__c =system.today();
        }
    }

    public PageReference submitrequest(){
        project.RequestCancellation__C=true;
        project.Cancellation_comments__c= project.Cancellation_justification__c;
        update project;
        return redirectToUrl('/'+project.Id);
    }
    public PageReference backtoproject(){
        return redirectToUrl('/'+project.Id);
    }
     private PageReference redirectToUrl(String url){
        PageReference curPage = new PageReference (url);
        
        return curPage;
    }
}
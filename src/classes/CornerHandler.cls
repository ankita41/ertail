/*****

*   @Class      :   CornerHandler.cls
*   @Description:   Static methods related to Corner.
*                   It is important to keep this class WITHOUT sharing as the sharing will be inherited from the caller (Trigger, controller...)
*   @Author     :   Thibauld
*   @Created    :   23 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Thibauld        25 JUL 2014     Add: Update of the associated Corner Projects and related object on Market Name change
*   Thibauld        03 SEP 2014     Add: Convert Costs to CHF
*   Thibauld        29 SEP 2014     Add: SetGeolocationInfoAfterInsert
*   Thibauld        29 SEP 2014     Add SetSearchablefieldsBeforeUpsert
*   Rao             23 NOV 2015     Add:updateCornerTotalCost
*   Priya           25 NOV 2016     Updated:updateCornerTotalCost
************/
public without sharing class CornerHandler {
    public static void UpdateMarketNameAfterUpsert(List<Corner__c> newList){
        // Copy the MarketName in the records
        Map<Id, Corner__c> cornersToUpdateMap = new Map<Id, Corner__c>();
        for (Corner__c co : newList){
            if (co.Market__c != co.MarketName__c)
                cornersToUpdateMap.put(co.Id, new Corner__c(Id=co.Id, MarketName__c = co.Market__c));
        }
        if (cornersToUpdateMap.size() > 0){
            update cornersToUpdateMap.values();
            // Update the Corner Projects if necessary
            Map<Id, Project__c> projToUpdateMap = new Map<Id, Project__c>();
            for(Project__c p : [Select Id, MarketName__c, Market__c from Project__c where Corner__c in :cornersToUpdateMap.keySet()]){
                if (p.MarketName__c != p.Market__c){
                    projToUpdateMap.put(p.Id, p);
                }
            }
            if (projToUpdateMap.size() > 0)
                CornerProjectHandler.UpdateMarketNameAfterUpdate(projToUpdateMap);
            }
        //updateCornerTotalCost(newList);
    }
    
    private static CurrencyConverter GetCurrencyConverter(List<Corner__c> newList){
        Set<String> createdYears = new Set<String>{String.valueOf(Date.today().year())};
        Set<String> localCurrencies = new Set<String>();
        for (Corner__c corner: newList) {
            localCurrencies.add(corner.Currency__c);
        }
    
        return new CurrencyConverter(createdYears, localCurrencies);
    }
   
    public static void ConvertCostsToCHFBeforeInsert(List<Corner__c> newList){
        CurrencyConverter cc = GetCurrencyConverter(newList);
        for(Corner__c corner : newList){
            if (corner.Relocation_cost_Market__c != null){
                corner.Relocation_cost_CHF__c = cc.convertValueFromTo(double.valueOf(corner.Relocation_cost_Market__c), corner.Currency__c, 'CHF', Date.today());
            }
        }
    }
    
    //added the method  to update the total cost based on existing project data
    public static void updateCornerTotalCost(List<Corner__c> newList){
    
        Map<Id, Corner__c> cornersUpdateMap = new Map<Id, Corner__c>();
        for (Corner__c co : newList){
            //if (co.Market__c != co.MarketName__c){
                cornersUpdateMap.put(co.Id, co);
                System.debug('****cornersUpdateMap'+cornersUpdateMap);
        }
        Map<Id, List<Project__c>> projToUpdateMap = new Map<Id, List<Project__c>>();
        for(Project__c p : [Select Id, Corner__c,Total_Cost_CHF__c,createddate,RecordTypeValue__c,Status__c  from Project__c where Corner__c in :cornersUpdateMap.keySet() order by createddate asc]){
                if(projToUpdateMap!= null && projToUpdateMap.containskey(p.Corner__c)){
                    projToUpdateMap.get(p.Corner__c).add(p);
                } else{
                List<Project__c> projectList= new List<Project__c>();
                projectList.add(p);
                    projToUpdateMap.put(p.Corner__c, projectList);
                }  
                System.debug('****projToUpdateMap'+projToUpdateMap);              
        }
        Map<Id, Corner__c> cornersToUpdateMap = new Map<Id, Corner__c>();
        for (Corner__c co : newList){
            if(projToUpdateMap!= null && !projToUpdateMap.isempty() && projToUpdateMap.containskey(co.id) ){
               List<Project__C> proList= projToUpdateMap.get(co.id);
                if(proList!= null && proList.size()>0){
                //Project__C obj=proList[0];
                      co.Total_Cost_CHF__c=0;            //Priya: Added for loop and below code for TC Req#2 Jan'17 Release
              
                for(Project__C obj : proList){ 
                if(obj!= null ){ //&& obj.Total_Cost_CHF__c!= null && (obj.Total_Cost_CHF__c!=0 || obj.Total_Cost_CHF__c !=0.00)
                   // cornersToUpdateMap.put(co.id,new Corner__c(Id=co.Id, Total_Cost__c = obj.Total_Cost__c));
                  if(obj.RecordTypeValue__c !=null){
                   System.debug('****obj.RecordTypeValue__c ='+obj.RecordTypeValue__c);   
                    if(obj.RecordTypeValue__c.contains('New')&&obj.Status__c!='Cancelled')
                       co.Total_Cost_CHF__c=obj.Total_Cost_CHF__c;
                    if(obj.RecordTypeValue__c.contains('Maintenance')&&obj.Status__c!='Cancelled')
                    co.Total_Cost_CHF__c=co.Total_Cost_CHF__c+obj.Total_Cost_CHF__c;
                   }
                   System.debug('****co.Total_Cost_CHF__c ='+co.Total_Cost_CHF__c);   
                   
                  } 
                }
                }
            }
        }
        
        //if(cornersToUpdateMap!= null && !cornersToUpdateMap.isEmpty()){
            //update cornersToUpdateMap.values();
       // }
        
    }
    public static void ConvertCostsToCHFBeforeUpdate(Map<Id, Corner__c> oldMap, Map<Id, Corner__c> newMap){
        CurrencyConverter cc = GetCurrencyConverter(newMap.values());
        for(Id cornerId : newMap.keySet()){
            if (oldMap.get(cornerId).Relocation_cost_Market__c != newMap.get(cornerId).Relocation_cost_Market__c){
                if (newMap.get(cornerId).Relocation_cost_Market__c == null)
                    newMap.get(cornerId).Relocation_cost_CHF__c = null;
                else
                    newMap.get(cornerId).Relocation_cost_CHF__c = cc.convertValueFromTo(double.valueOf(newMap.get(cornerId).Relocation_cost_Market__c), newMap.get(cornerId).Currency__c, 'CHF', Date.today());
            }
        }
    }
    
    public static void SetPreviousPOSBeforeUpdate(Map<Id, Corner__c> oldMap, Map<Id, Corner__c> newMap){
        for(Id cornerId : newMap.keySet()){
            if(oldMap.get(cornerId).Point_of_Sale__c != null && newMap.get(cornerId).Point_of_Sale__c != oldMap.get(cornerId).Point_of_Sale__c){
                newMap.get(cornerId).Previous_POS__c = oldMap.get(cornerId).Point_of_Sale__c;
            }
        }
    }
    
    // Moved from SetGeolocation trigger
    public static void SetGeolocationInfoAfterInsert(List<Corner__c> newList){
        Set<Id> cornerIds = new Set<Id>();
        for (Corner__c c : newList)
            if (c.Location__Latitude__s == null)
                cornerIds.add(c.Id);
            GeocodingLocationCallouts.getLocation(cornerIds); 
    }
    
    // Moved from SetSearchableFields
    public static void SetSearchableFieldsBeforeUpsert(List<Corner__c> newList){
        Set<Id> setPOSIds = new Set<Id>();
        for (Corner__c c : newList) {
          setPOSIds.add(c.Point_of_Sale__c);
        }
    
        //RecordType posRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Point_of_Sales'];
        Map<Id, Account> mapPOSAccountsPerAccountId = new Map<Id, Account>([SELECT Id, Name, Nessoft_ID__c FROM Account WHERE /*RecordTypeId = :posRT.Id AND*/ Id IN :setPOSIds]);
         
        for (Corner__c c : newList) {
          if (c.Point_of_Sale__c != null && mapPOSAccountsPerAccountId.containsKey(c.Point_of_Sale__c)){
            c.Point_of_Sale_Name__c = mapPOSAccountsPerAccountId.get(c.Point_of_Sale__c).Name;
            c.Nessoft_ID__c = mapPOSAccountsPerAccountId.get(c.Point_of_Sale__c).Nessoft_ID__c;
          }
        }
      updateCornerTotalCost(newList);
    }
   
}
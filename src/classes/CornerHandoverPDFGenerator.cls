public with sharing class CornerHandoverPDFGenerator {
    public Handover_Report__c handover {get; set;}
    public List<Handover_Checklist__c> checklist {get; set;}
    public String nesLogoURL { 
        get {
            Document logoFile = [SELECT Id FROM Document WHERE DeveloperName = 'Logo_NES_Trade_Corner_Platform_App'];
            return '/servlet/servlet.ImageServer?id=' + logoFile.Id + '&oid=' + UserInfo.getOrganizationId();
        } set;
    }
    
    public CornerHandoverPDFGenerator (ApexPages.StandardController stdController) {
        handover = (Handover_Report__c)stdController.getRecord();
        handover = [SELECT Id, Name, Project__r.Name, Point_of_Sales__c, City__c, Market__c, Approval_Status__c, Address__c, 
                        Nespresso_Contact__c, NES_Sales_Promoter__c, Architect__c, Supplier__c, 
                        POS_Representative__c, Installer__c, General_cleaning__c,  Installation_area_cleared_before__c, 
                        Installation_feedback__c, Supplier_on_time__c, POS_Comments__c,
                        Installer_Arrival_Date__c, Installer_Arrival_Time__c, Installer_Departure_Date__c, Installer_Departure_Time__c
                        FROM Handover_Report__c WHERE Id = :handover.Id];
        
        checklist = [SELECT Id, Furniture__r.Name, Quantity__c, Note__c, Comments_ci__c, Status_ci__c, Comments_ed__c,  Status_ed__c, Comments_L__c,  Status_L__c 
                        FROM Handover_Checklist__c WHERE Handover_Report__c = :handover.Id AND Completed_items__c = true];
    }
}
/***
*   @Class      :   CornerLinkedPOSHandler.cls
*   @Description:   Handles all Linked_POS__c recurring actions/events.
*                   THIS CLASS MUST STAY WITHOUT SHARING
*   @Author     :   Promila Boora
*   @Created    :   10 MAY 2015
*/
public without sharing class CornerLinkedPOSHandler{
 public class CustomException extends Exception{}
 
 /****Method checkUniquePOSPerProject starts here *******/
/***Check condition here for Corner POS field should not be same for the same project i.e. it must be unique**/
 public static void CheckUniquePOSPerProject(List<Linked_POS__c>newLNKList)
 {
      Map<string,list<string>>allProjPOSID=new map<string,list<string>>(); //Map containing Project and its related POS 
      Set<string>projIds=new set<string>();
      for(Linked_POS__c exisProjectPOS:newLNKList)
       {
            projIds.add(exisProjectPOS.Corner_Project__c);
            system.debug('---existing project'+exisProjectPOS.Corner_Project__c);
            if(!allProjPOSID.containsKey(exisProjectPOS.Corner_Project__c))
                {
                  allProjPOSID.put(exisProjectPOS.Corner_Project__c,new list<string>());
                }
                system.debug('---Map--'+allProjPOSID);
       }
      /*******Below code of for loop is creating a map of existing project and their corners records*******************/ 
      for(Linked_POS__c exisProjectPOS:[select id,Corner_Project__c,Corner_Point_of_Sale__c,POS_Name__c from Linked_POS__c where Corner_Project__c in:projIds])
        {
          // if(projIds.contains(exisProjectPOS.Corner_Project__c))
          // {
               if(!allProjPOSID.containsKey(exisProjectPOS.Corner_Project__c))
                {
                  allProjPOSID.put(exisProjectPOS.Corner_Project__c,new list<string>());
                }
                
                 allProjPOSID.get(exisProjectPOS.Corner_Project__c).add(exisProjectPOS.Corner_Point_of_Sale__c );
                
                
            //}
            
        }
        /*******Below code of for loop is checking for existing project whether its containing newly added POS *******************/ 
      Set<string>newPOSIds=new Set<string>();
      for(Linked_POS__c newProjectPOS:newLNKList)
        {
           if(allProjPOSID.keySet().contains(newProjectPOS.Corner_Project__c))
           {
                set<string>POSIds=new Set<string>();
                 set<string>POSListIds=new Set<string>();
                 POSIds.addAll(allProjPOSID.get(newProjectPOS.Corner_Project__c));
                 system.debug('--POS NAme-'+newProjectPOS.POS_Name__c+'---Corenr POS--'+newProjectPOS.Corner_Point_of_Sale__c+'--check proj'+allProjPOSID.get(newProjectPOS.Corner_Project__c).isEmpty());
               if(!allProjPOSID.get(newProjectPOS.Corner_Project__c).isEmpty())
               {
                 if(newProjectPOS.POS_Name__c==null && newProjectPOS.Corner_Point_of_Sale__c==null)
                 {
                  system.debug('===No POS ');
                   newProjectPOS.POS_Name__c.addError('Please select a "Corner Point of Sale" or enter a "POS name"');
                   return;
                 }
                 if(POSIds.contains(newProjectPOS.Corner_Point_of_Sale__c) && newProjectPOS.Corner_Point_of_Sale__c!=null){
                   system.debug('==POS already existing'+newProjectPOS.Corner_Point_of_Sale__c);
                   newProjectPOS.Corner_Point_of_Sale__c.addError('Corner Point of Sales Already existing for this project');
                   return;
                 }
                 
                
                   allProjPOSID.get(newProjectPOS.Corner_Project__c).add(newProjectPOS.Corner_Point_of_Sale__c );
                 
               }
               else
               {
                   if(newProjectPOS.POS_Name__c==null && newProjectPOS.Corner_Point_of_Sale__c==null)
                   {
                     system.debug('===No POS ');
                     newProjectPOS.POS_Name__c.addError('Please select a "Corner Point of Sale" or enter a "POS name"');
                     return;
                   }
                   if(POSIds.contains(newProjectPOS.Corner_Point_of_Sale__c) && newProjectPOS.Corner_Point_of_Sale__c!=null){
                     system.debug('==POS already existing'+newProjectPOS.Corner_Point_of_Sale__c);
                     newProjectPOS.Corner_Point_of_Sale__c.addError('Corner Point of Sales Already existing for this project');
                     return;
                   }
                     allProjPOSID.get(newProjectPOS.Corner_Project__c).add(newProjectPOS.Corner_Point_of_Sale__c );
                   
               }
               
               // allProjPOSID.get(newProjectPOS.Corner_Project__c).add(newProjectPOS.Corner_Point_of_Sale__c );
            }
        }
      
 }
  /****Method checkUniquePOSPerProject ends here *******/
  
  /****Method CheckUniquePOSPerProjectonUpdate on UPDATE starts here *******/
/***Check condition here for Corner POS field should not be same for the same project i.e. it must be unique**/
 public static void CheckUniquePOSPerProjectonUpdate(Map<Id,Linked_POS__c>newLNKMap,Map<Id,Linked_POS__c>oldLNKMap)
 {
      Map<string,list<string>>allProjPOSID=new map<string,list<string>>(); //Map containing Project and its related POS 
      Set<string>projIds=new set<string>();
      for(Id key : newLNKMap.keySet()){
            projIds.add(newLNKMap.get(key).Corner_Project__c);
       }
      /*******Below code of for loop is creating a map of existing project and their corners records*******************/ 
      for(Linked_POS__c exisProjectPOS:[select id,Corner_Project__c,Corner_Point_of_Sale__c from Linked_POS__c where Corner_Project__c in:projIds ])
        {
           if(projIds.contains(exisProjectPOS.Corner_Project__c))
           {
               if(!allProjPOSID.containsKey(exisProjectPOS.Corner_Project__c))
                {
                  allProjPOSID.put(exisProjectPOS.Corner_Project__c,new list<string>());
                }
                
                 allProjPOSID.get(exisProjectPOS.Corner_Project__c).add(exisProjectPOS.Corner_Point_of_Sale__c );
                
            }
        }
        /*******Below code of for loop is checking for existing project whether its containing newly added POS *******************/ 
      for(Id keys : newLNKMap.keySet()){
          if(allProjPOSID.keySet().contains(newLNKMap.get(keys).Corner_Project__c))
           {
               if(!allProjPOSID.get(newLNKMap.get(keys).Corner_Project__c).isEmpty())
               {
                 Set<string>POSIds=new Set<string>();
                 POSIds.addAll(allProjPOSID.get(newLNKMap.get(keys).Corner_Project__c));
                 if(newLNKMap.get(keys).POS_Name__c==null && newLNKMap.get(keys).Corner_Point_of_Sale__c==null)
                   {
                     system.debug('===No POS ');
                     newLNKMap.get(keys).POS_Name__c.addError('Please select a "Corner Point of Sale" or enter a "POS name"');
                     return;
                   }
                 if(POSIds.contains(newLNKMap.get(keys).Corner_Point_of_Sale__c) && newLNKMap.get(keys).Corner_Point_of_Sale__c!=oldLNKMap.get(keys).Corner_Point_of_Sale__c){
                     newLNKMap.get(keys).Corner_Point_of_Sale__c.addError('Corner Point of Sales Already existing for this project');
                 }
                 
               }
               
                allProjPOSID.get(newLNKMap.get(keys).Corner_Project__c).add(newLNKMap.get(keys).Corner_Point_of_Sale__c );
            }
        }
 
 }
  /****Method checkUniquePOSPerProject on Update ends here *******/
  
  
/******Method to updated Corner for POS starts here ************/
public static void CheckUpdatePOSAfterInitiation(Map<Id,Linked_POS__c>newLNKMap1,Map<Id,Linked_POS__c>oldLNKMap)
 {
 Map<id,Linked_POS__c>newLNKMap=new Map<id,Linked_POS__c>([Select id,Corner_Project__r.Approval_Status__c,Corner_Point_of_Sale__c from Linked_POS__c where Id IN :newLNKMap1.keySet()]); //map containing linked pos id and its Linked POS
 for(Id key : newLNKMap.keySet()){
 //system.debug('---newLNKMap.get(key).Corner_Project__r.Approval_Status__c'+newLNKMap.get(key).Corner_Project__r.Approval_Status__c);
       if( newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Forecast' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Draft' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='New Request' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Site Survey Quotation' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Mass Site Survey Quotation' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Site Survey Completion' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Site Survey Quotation Approval')
       {
       if(newLNKMap1.get(key).Corner_Point_of_Sale__c!=oldLNKMap.get(key).Corner_Point_of_Sale__c){
            newLNKMap1.get(key).Corner_Point_of_Sale__c.addError('POS can\'t be updated after Project initiation');
            }
       }
   }
   
 }
 public static void CheckDeletePOSAfterInitiation(Map<Id,Linked_POS__c>newLNKMap1,Map<Id,Linked_POS__c>oldLNKMap)
 {
  Map<id,Linked_POS__c>newLNKMap=new Map<id,Linked_POS__c>([Select id,Corner_Project__r.Approval_Status__c,Corner_Point_of_Sale__c from Linked_POS__c where Id IN :oldLNKMap.keySet()]); //map containing linked pos id and its Linked POS
 for(Id key : newLNKMap.keySet()){
 //system.debug('---newLNKMap.get(key).Corner_Project__r.Approval_Status__c'+newLNKMap.get(key).Corner_Project__r.Approval_Status__c);
       if( newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Forecast' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Draft' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='New Request' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Site Survey Quotation' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Mass Site Survey Quotation' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Site Survey Completion' && newLNKMap.get(key).Corner_Project__r.Approval_Status__c!='Site Survey Quotation Approval')
       {
            oldLNKMap.get(key).Corner_Point_of_Sale__c.addError('POS can\'t be deleted after Project initiation');
          
            
       }
   }
   
 }
/******Method to updated Corner for POS ends here ************/

/********Set Forecast links on before insert starts here *************/
public static void SetForecastLinks(List<Linked_POS__c> newList){
        Set<String> yearSet = new Set<String>();
        List<Linked_POS__c> lnkPOSToConsiderList = new List<Linked_POS__c>();
        Map<Id, Map<String, Objective__c>> objectivesOrderedByPOSThenYear = new Map<Id, Map<String, Objective__c>>();
        
        // Extract the projects with a populated POS
        for (Linked_POS__c lnkPOS: newList){
            if (lnkPOS.Targeted_Installation_Date__c != null && (lnkPOS.Corner_Point_of_Sale__c != null || lnkPOS.Key_Account__c != null)){
                Id posId = (lnkPOS.Corner_Point_of_Sale__c == null ? lnkPOS.Key_Account__c : lnkPOS.Corner_Point_of_Sale__c);
                if (!objectivesOrderedByPOSThenYear.containsKey(posId))
                    objectivesOrderedByPOSThenYear.put(posId, new Map<String, Objective__c>());
                
                String yearStr = String.valueOf(lnkPOS.Targeted_Installation_Date__c.year());
                lnkPOSToConsiderList .add(lnkPOS);
                yearSet.add(yearStr);
            }
        }
        
        if (lnkPOSToConsiderList .isEmpty())
            return;
        
        // Request all the Markets for the POS
        Map<Id, List<Id>> posIdListPerMarket = new Map<Id, List<Id>>();
        for(Account pos : [Select Id, Market__c from Account where Id in :objectivesOrderedByPOSThenYear.keySet()]){
            if(!posIdListPerMarket.containsKey(pos.Market__c))
                posIdListPerMarket.put(pos.Market__c, new List<Id>());
            posIdListPerMarket.get(pos.Market__c).add(pos.Id);
        }
        
        // populate the Objective__c Ids in myfIdOrderedByYear
        for(Objective__c myf : [select Id, Market__c, year__c, o.New_Members_Market_or_Region_Growth__c, o.Monthly_Average_Consumption__c, o.Caps_Cost_in_LC__c, o.Avg_Capsule_sales_price_in_LC_ex_VAT__c from Objective__c o where Market__c in :posIdListPerMarket.keySet() and year__c in:yearSet]){
            Id marketId = myf.Market__c;
            for(Id posId : posIdListPerMarket.get(marketId)){
                 Map<String, Objective__c> myfIdOrderedByYear = objectivesOrderedByPOSThenYear.get(posId);
                 String yearStr = String.valueOf(myf.year__c);
                 myfIdOrderedByYear.put(yearStr, myf);
            }
        }
        
        List<Linked_POS__c> lnkPOSWhereForecastDoesntExist = new List<Linked_POS__c>();
        List<Linked_POS__c> uncompleteForecast = new List<Linked_POS__c>();
        Set<Id> accToRequest = new Set<Id>();
        
        // Populate the Objective links in Project__c with the appropriate value
        for(Linked_POS__c lnPOS: lnkPOSToConsiderList ){
            String yearStr = String.valueOf(lnPOS.Targeted_Installation_Date__c.year());
            Id posId = (lnPOS.Corner_Point_of_Sale__c == null ? lnPOS.Key_Account__c : lnPOS.Corner_Point_of_Sale__c);
            if (objectivesOrderedByPOSThenYear.containsKey(posId) 
                && objectivesOrderedByPOSThenYear.get(posId).containsKey(yearStr)){
                    Objective__c obj = objectivesOrderedByPOSThenYear.get(posId).get(yearStr);
                    lnPOS.Forecast__c = obj.Id;
                    if (!isForecastValid(obj)){
                        
                        uncompleteForecast.add(lnPOS);
                        if (lnPOS.Key_Account__c != null)
                            accToRequest.add(lnPOS.Key_Account__c);
                        if (lnPOS.Corner_Point_of_Sale__c != null)
                            accToRequest.add(lnPOS.Corner_Point_of_Sale__c);
                    }
            } else {
                lnkPOSWhereForecastDoesntExist .add(lnPOS);
                if (lnPOS.Key_Account__c != null)
                    accToRequest.add(lnPOS.Key_Account__c);
                if (lnPOS.Corner_Point_of_Sale__c != null)
                    accToRequest.add(lnPOS.Corner_Point_of_Sale__c);
            }
        }
        
        Map<Id, Account> accMap = new Map<Id, Account>([Select Id, Name from Account where Id in :accToRequest]);
               
        for (Linked_POS__c lnPOS: lnkPOSWhereForecastDoesntExist ){
            String yearStr = String.valueOf(lnPOS.Targeted_Installation_Date__c.year());
            lnPOS.addError('There is no existing forecast for ' 
                                    + lnPOS.Market__c 
                                    + ' for the year ' 
                                    + yearStr 
                                    + '. Please create one using the following <b><a href=/a0A/e?CF00Nb0000003yVU0=' 
                                    + lnPOS.Market__c 
                                    + '&00Nb0000003yVUE=' 
                                    + yearStr+ ' target=/"_blank/">Link</a></b>',false);
// lnPOS.addError('.');                                                                
       /* ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'There is no existing forecast for ' 
                                                                + lnPOS.Market__c 
                                                                + ' for the year ' 
                                                                + yearStr 
                                                                + '. Please create one using the following <b><a href=/a0A/e?CF00Nb0000003yVU0=' 
                                                                + lnPOS.Market__c 
                                                                + '&00Nb0000003yVUE=' 
                                                                + yearStr+ ' target=/"_blank/">Link</a></b>'));*/
        } 
        
        for (Linked_POS__c lnPOS: uncompleteForecast){
            String yearStr = String.valueOf(lnPOS.Targeted_Installation_Date__c.year());
            //Targeted_Installation_Date__c.            
           lnPOS.addError('The forecast for ' 
                                + lnPOS.Market__c 
                                + ' for the year ' 
                                + yearStr 
                                + ' is incomplete.<br/>Please complete it using the following <b><a href="/' 
                                + lnPOS.Forecast__c + '/e"' + ' target=/"_blank/">Link</a></b>', false);
                               /* '/e?'+ 'retURL=' + retURL
                                + '&saveURL=' + retURL*/
                                                               
       /* ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The forecast for ' 
                                                                + lnPOS.Market__c 
                                                                + ' for the year ' 
                                                                + yearStr 
                                                                + ' is incomplete.<br/>Please complete it using the following <b><a href="/' 
                                                                + lnPOS.Forecast__c + '/e"' + ' target=/"_blank/">Link</a></b>'));*/
        }

    }
/********Set Forecast links on before insert ends here ***************/

 private static Boolean isForecastValid(Objective__c o){
        return o.Market__c != null
                && o.year__c != null
                && o.New_Members_Market_or_Region_Growth__c != null
                && o.Monthly_Average_Consumption__c != null
                && o.Caps_Cost_in_LC__c != null
                && o.Avg_Capsule_sales_price_in_LC_ex_VAT__c != null;
        
    }




/****Method addMarketPOSPerProject starts here *******/
/***Map Market and user info to Project and mark Is_POS_Add field as true**/
 public static void addMarketPOSPerProject(List<Linked_POS__c>newLNKList)
 {
      
 
 }
/****Method addMarketPOSPerProject ends here *******/
 public static void changeProcessonInstallationDate(Map<Id,Linked_POS__c>newLNKMap,Map<Id,Linked_POS__c>oldLNKMap)
 {
   Map<id,Boolean>projectIds=new Map<id,Boolean>(); //if salespromoter then true, for agent its false
   Map<Id,Project__c>mapToCheck=new Map<Id,Project__c>();
   List<Project__c>prjUpdateList=new List<Project__c>();
   for(Linked_POS__c lnkPOS:[Select Id,Corner_Project__c,Notify_Proposed_Installation_Schedule__c from Linked_POS__c where Id IN :newLNKMap.keySet()]){
     projectIds.put(lnkPOS.Corner_Project__c,true);
   }
   for(Project__c prj:[Select id,Workflow_Type__c from Project__c where Id IN:projectIds.keySet()])
   {
     if(prj.Workflow_Type__c.equals('Sales Promoter'))
     {
       projectIds.put(prj.id,true);
     }
     if(prj.Workflow_Type__c.equals('Agent'))
     {
      projectIds.put(prj.id,false);
     }
   }
   for(Id key : newLNKMap.keySet())
      {
        if(projectIds.containsKey(newLNKMap.get(key).Corner_Project__c) && newLNKMap.get(key).Notify_Proposed_Installation_Schedule__c!=oldLNKMap.get(key).Notify_Proposed_Installation_Schedule__c && newLNKMap.get(key).Notify_Proposed_Installation_Schedule__c==true)
        {
             Id rectypID;
              if(projectIds.get(newLNKMap.get(key).Corner_Project__c)==true)
              {
                rectypID=CornerProjectHelper.newMassCornerRT.Id;
              }
              else
              {
                 rectypID=CornerProjectHelper.newMassCornerAgentRT.Id;
              }
              
            
             if(!mapToCheck.containsKey(newLNKMap.get(key).Corner_Project__c))
              {
               mapToCheck.put(newLNKMap.get(key).Corner_Project__c,new Project__c(id=newLNKMap.get(key).Corner_Project__c,Approval_Status__c='Installation Date Approval',RecordTypeId =rectypID));
               prjUpdateList.add(new Project__c(id=newLNKMap.get(key).Corner_Project__c,Approval_Status__c='Installation Date Approval',RecordTypeId =rectypID));
              }
          }
      }
      if(!prjUpdateList.isEmpty())
      {
        try
        {
          update prjUpdateList;
        }
        catch(DMLException e)
        {
        }
      }
 }
 public static void changeProcessAgreedInstallationDate(Map<Id,Linked_POS__c>newLNKMap,Map<Id,Linked_POS__c>oldLNKMap)
 {
   Map<id,Project__c>projectIds=new Map<id,Project__c>();
      
   for(Id key : newLNKMap.keySet())
      {
        if(!projectIds.containsKey(newLNKMap.get(key).Corner_Project__c)  &&  newLNKMap.get(key).Agreed_Installation_Date__c != null &&newLNKMap.get(key).status__C =='Installation Date Approval')//oldLNKMap.get(key).Agreed_Installation_Date__c==null)
        {
           projectIds.put(newLNKMap.get(key).Corner_Project__c,new Project__c(id=newLNKMap.get(key).Corner_Project__c,Approval_Status__c='Waiting for Installation'));
        }
      }
      if(!projectIds.isEmpty())
      {
      List<Project__c>prjList=projectIds.values();
         update prjList;
        try
        {
        }
        catch(DMLException e)
        {
        }
      }
 }
 public static void changeProcessActualInstallationDate(Map<Id,Linked_POS__c>newLNKMap,Map<Id,Linked_POS__c>oldLNKMap)
 {
   Map<id,Project__c>projectIds=new Map<id,Project__c>();
   
    List<String>   datelist=new List<String>();
    String projectid;    
         for(Id key : newLNKMap.keySet())
      {
          
          projectid=newLNKMap.get(key).Corner_Project__c;
          if(!projectIds.containsKey(newLNKMap.get(key).Corner_Project__c) && newLNKMap.get(key).Actual_Installation_Date__c!=null && newLNKMap.get(key).status__C !='Installed')//&& newLNKMap.get(key).Actual_Installation_Date__c!=oldLNKMap.get(key).Actual_Installation_Date__c 
        {
           projectIds.put(newLNKMap.get(key).Corner_Project__c,new Project__c(id=newLNKMap.get(key).Corner_Project__c,Approval_Status__c='Handover Report'));
        }
      }
     // system.debug('projectid@@'+projectid);
    Integer countpos=[select count() from Linked_POS__c where Corner_Project__c =:projectid and Actual_Installation_Date__c =null];
    //system.debug('countpos@@'+countpos);
    //system.debug('projectIds.keyset().size(@@'+projectIds.keyset().size());
      if(!projectIds.isEmpty()&&countpos==0 )
      {
      List<Project__c>prjList=projectIds.values();
         update prjList;
        try
        {
        }
        catch(DMLException e)
        {
        }
      }
 }
  public static void updateCorners(Map<Id,Linked_POS__c>newLNKMap){
  
     /* 
      Map<id,Linked_POS__c>newLNKMap=new Map<id,Linked_POS__c>([Select id,Corner__c,Corner_Point_of_Sale__c from Linked_POS__c where Id IN :newLNKMap1.keySet()]);
     for(Id key : newLNKMap.keySet()){
    */
      Map<Id,Linked_POS__c >cornMap=new Map<Id,Linked_POS__c >();
      for(Linked_POS__c lnkPOS:[Select Id, Corner_Point_of_Sale__c,Corner__c from Linked_POS__c where id In :newLNKMap.KeySet()])
      {
        if(!cornMap.containsKey(lnkPOS.Corner__c ))
        {
         cornMap.put(lnkPOS.Corner__c,lnkPOS);
        }
      }
      List<Corner__c>updateCorn=new List<Corner__c>();
      for(Corner__c corn:[Select Id,Point_of_Sale__c from Corner__c where Id IN :cornMap.keySet()])
      {
         updateCorn.add(new Corner__c(Id=corn.Id,Point_of_Sale__c=cornMap.get(corn.Id).Corner_Point_of_Sale__c));
      }
      if(!updateCorn.isEmpty())
      {
       update updateCorn;
      }
  }

}
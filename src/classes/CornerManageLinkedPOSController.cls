/*
*   @Class       :   CornerManageLinkedPOSController.cls
*   @Description :   Controller for the CornerManageLinkedPOS Visual Force page.
*   @Author      :   Promila Boora
*   @Created     :   May 9,2015
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Promila        9 MAY 2015     Added Method 
*   
*/
public with sharing class CornerManageLinkedPOSController{
    public class CustomException extends Exception {}
    
    /********Variable declaration starts here *********/
    
    public Project__c proj {get; set;}
    public String currentUserProfile;
    public List<Linked_POS__c> lnkPOSList {get;set;}
    public Map<Id,Linked_POS__c> mapLNKPOSWrap {get;set;}
    private String projId;
    public Boolean propInstallButton{get;set;}
    /********Variable declaration ends here *********/
    public Boolean renderBlockone{get;set;}
    public Boolean renderBlocktwo{get;set;}
    public Boolean renderBlockthree{get;set;}
    public Boolean renderBlockfour{get;set;}
    Public Boolean renderSaveButton{get;set;}  
    Public Boolean renderBlockfive{get;set;} 
    /****Constructor starts here*******/
    public CornerManageLinkedPOSController(ApexPages.StandardController controller) {
        propInstallButton=false;
        currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
       // proj = (Project__c)controller.getRecord();
       projId=apexpages.currentpage().getparameters().get('projId');

       List<Project__c> projectList = [SELECT Id, Approval_Status__c, Notify_Layout_Proposal__c, Status__c,Invoicing_Contact__c,Nespresso_Contact__c,ZoneName__c,   
                        Notify_All_for_Closing__c,Architect__c, Supplier__c, RecordTypeId,NES_Business_Development_Manager__c,
                        Notify_Draft_Request__c, Notify_Draft_Request_Date__c, MarketName__c,Project_Manager_HQ__c,Agent_Local_Manager__c,
                        AllowInitiateProject__c, Workflow_Type__c, Supplier_Proposal__c, Next_Process_Step__c,
                        On_Hold_Set_Date__c, On_Hold_Original_Status__c, On_Hold_Previous_Step__c, Notify_Site_Survey_Submitted__c,
                        Proposed_Site_Survey_Date__c,  Resp_for_Site_Survey__c, Water_supply_needed_r__c,Walls_material_r__c,
                        Visual__c, Nb_of_machines_in_stock__c, Nb_of_machines_in_display__c, Product_Range__c,NES_Sales_Promoter__c,
                        LCD_screen__c, Height_under_ceiling_r__c, Floor_material_r__c, Floor__c,Electricity_coming_from_r__c,
                        Ecolaboration_module__c, Corner_Unit_r__c, Notify_Proposed_Installation_Schedule__c,Corner_Type_r__c,Is_POS_Added__c, Corner_Generation_r__c, RecordType.DeveloperName
                        FROM Project__c WHERE Id = :projId];
        if(projectList != null && projectList.size()>0){
        proj =projectList[0];
        }
        
        lnkPOSList=new List<Linked_POS__c>(); 
        lnkPOSList=[select id,POS_Name__c ,Corner_Project__c,Corner__c,Key_Account__c,Actual_Installation_Date__c,Nespresso_Project_Manager__c,Agent_Local_Manager__c,Targeted_Installation_Date__c,Agreed_Installation_Date__c,Delivery_Date__c ,Shipping_Date__c,Proposed_Installation_Date__c,Corner_Point_of_Sale__c from Linked_POS__c where Corner_Project__c=:projId];
        /*Linked_POS__c posRec=new Linked_POS__c();
        posRec.Corner_Project__c=proj.Id;
        lnkPOSList.add(posRec);*/
        if(lnkPOSList!= null && lnkPOSList.size()==0){
            addRow();
        }
        showRequiredLayout();        
    }
    /****Constructor ends here*******/
    public Boolean showproposedInstallation{
      get{
        if(proj.Approval_Status__c=='Supplier Propose Installation Date' || proj.Approval_Status__c=='Installation Date Approval')
        {
           return true;
        }
        else
        {
           return false;
        }
      }set;
    }
    
    /********Add Row Method starts here**********************/
     public void addRow(){
     Linked_POS__c posRec=new Linked_POS__c();
     posRec.Corner_Project__c=proj.Id;
     posRec.Targeted_Installation_Date__c=Date.today()+56;
     lnkPOSList.add(posRec);
     }
    /********Add Row Method ends here************************/
    
    /********Remove Row Method starts here**********************/
     public void removeRow(){
     lnkPOSList.remove(lnkPOSList.size()-1);
     }
    /********Remove Row Method ends here************************/
    public PageReference saveandclose(){
        addLNKPOS();
        
        return doCancel();
    }
    /********Insert POS Method starts here**********************/
     public void addLNKPOS(){
     if(lnkPOSList.size()>0){
        String POSMarket;
        try
        {
         
         if(proj.MarketName__c==null)
         {
           POSMarket=[Select Market__c from Account where id=:lnkPOSList[0].Key_Account__c limit 1].Market__c;
           system.debug('POS---'+lnkPOSList[0].Key_Account__c+'POSMarket---'+POSMarket);
         }//else{POSMarket =proj.market__C;}
         /***************code for Currency,Market,supplier,architect etc. starts here ****************************/
         if(POSMarket!=null)
         {
             Market__c market=[Select Id, Name, Default_Architect__c, Default_Supplier__c, Zone__c,NES_Project_Manager__c, Invoicing_Contact__c, NES_Business_Development_Manager__c, Project_Manager_HQ__c, Currency__c from Market__c where Id=:POSMarket];
             proj.Currency__c = market.Currency__c;
             proj.MarketName__c = market.Name;
             proj.ZoneName__c = market.Zone__c;
             proj.Is_POS_Added__c=true;
             if ( proj.Architect__c == null && market.Default_Architect__c != null) { 
                proj.Architect__c = market.Default_Architect__c;
             }
              if (proj.Supplier__c == null && market.Default_Supplier__c != null) {
                proj.Supplier__c = market.Default_Supplier__c;
              }
             if ( proj.Nespresso_Contact__c == null && market.NES_Project_Manager__c != null) {
                proj.Nespresso_Contact__c  = market.NES_Project_Manager__c;
             }
            
             if ( proj.Invoicing_Contact__c == null && market.Invoicing_Contact__c != null) {
                proj.Invoicing_Contact__c  = market.Invoicing_Contact__c;
             }
            
             if (proj.NES_Business_Development_Manager__c == null && market.NES_Business_Development_Manager__c !=null){
                proj.NES_Business_Development_Manager__c = market.NES_Business_Development_Manager__c;
             }
            
             if (proj.Project_Manager_HQ__c == null && market.Project_Manager_HQ__c !=null){
                proj.Project_Manager_HQ__c = market.Project_Manager_HQ__c;
             }
                             
             update proj;
            }
           /* lnkPOSList=new List<Linked_POS__c>(); 
            Linked_POS__c posRec=new Linked_POS__c();
            posRec.Corner_Project__c=proj.Id;
            posRec.Targeted_Installation_Date__c=Date.today()+56;
            lnkPOSList.add(posRec);*/
            system.debug('proj@@'+proj);      
            if(lnkPOSList!= null && lnkPOSList.size()>0){
                 for(Linked_POS__c obj:lnkPOSList){
                     obj.Nespresso_Project_Manager__c=proj.Nespresso_Contact__c;
                     if(proj.Agent_Local_Manager__c!= null)
                     obj.Agent_Local_Manager__c =proj.Agent_Local_Manager__c;
                     if(proj.Architect__c != null )
                      obj.NES_Architect__c   =proj.Architect__c; 
                      if(proj.Workflow_Type__c != null && proj.Workflow_Type__c != '')
                       obj.Workflow_Type__c    =proj.Workflow_Type__c  ;       
                }             
            }              
             upsert lnkPOSList;
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Information Saved Successfully!!'));
             system.debug('Inserted POS---'+lnkPOSList.size());
            

         }
         /***************code for Currency,Market,supplier,architect etc. ends here ****************************/
         catch(DmlException e) {
         if(e.getMessage().contains('Corner Point of Sales'))
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Corner Point of Sales Already existing for this project'));

         }
      }
     }
    /********Insert POS Method ends here************************/
    public void updatePOSDate(){
    try{
            updateLNKPOS();
            
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Information Updated Successfully!!'));
             system.debug('updated POSlist size---'+lnkPOSList.size());
             
        } catch(DmlException e) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
          return;}         
     }   

     /********Insert POS Method starts here**********************/
     public void updateLNKPOS(){
     if(lnkPOSList.size()>0){
        try
        {
          update lnkPOSList;
          propInstallButton=true;
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Information Updated Successfully!!'));
         system.debug('updated POSlist size---'+lnkPOSList.size());
         
         }
         /***************code for Currency,Market,supplier,architect etc. ends here ****************************/
         catch(DmlException e) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
          return;
         }
      }
     }
    /********Insert POS Method ends here************************/
    /*********Cancel Method starts here, user will redirect to the project without any function ********************/
    public PageReference doCancel(){
       propInstallButton=false;
      pageReference page = new pageReference('/'+projId);
      page.setRedirect(true);
      return page;
    }
    /*********Cancel Method ends here ********************/
    
    public void showRequiredLayout(){
            renderBlockone=false;
            renderBlocktwo=false;
            renderBlockthree=false;
            renderBlockfour=false;  
             renderBlockfive=false; 
            List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            String proflieName = PROFILE[0].Name;
          
           if((proj.Approval_Status__c=='Supplier Propose Installation Date' || proj.Approval_Status__c=='Installation Date Approval' ) && (proflieName =='NES Corner Supplier') && proj.Notify_Proposed_Installation_Schedule__c==false){
                renderBlockone=false;
                renderBlocktwo=true;
                renderBlockthree=false;
                renderBlockfour=false;
                renderBlockfive=false;
           }else if(proj.Approval_Status__c=='Waiting for Installation'&& (proflieName =='NES Corner Supplier')){
                renderBlockone=false;
                renderBlocktwo=false;
                renderBlockthree=false;
                renderBlockfour=false;
                renderBlockfive=true;
           }else if((proflieName =='NES Market Local Manager' ||proflieName=='Agent Local Manager') && proj.Approval_Status__c =='Installation Date Approval'  ){
                 renderBlockone=false;
                renderBlocktwo=false;
                renderBlockthree=true;
                renderBlockfour=false;renderBlockfive=false;
           }else if((proflieName =='NES Market Local Manager' ||proflieName=='Agent Local Manager'|| proflieName =='NES Market Sales Promoter') &&(proj.Approval_Status__c =='New Request' || proj.Approval_Status__c =='Draft' ||proj.Approval_Status__c =='Forecast')){
               renderBlockone=true;
                renderBlocktwo=false;
                renderBlockthree=false;
                renderBlockfour=false;renderBlockfive=false;
           } else if((proflieName =='System Admin' ||proflieName =='System Administrator') ){
               renderBlockone=true;
                renderBlocktwo=false;
                renderBlockthree=false;
                renderBlockfour=false;renderBlockfive=false;
           } else if((proflieName =='NES Market Local Manager' || proflieName=='Agent Local Manager'|| proflieName =='NES Corner Supplier'|| proflieName =='NES Market Sales Promoter'|| proflieName=='NES Trade/Proc Manager' || proflieName =='NES Corner Architect') &&(proj.Approval_Status__c !='New Request' || proj.Approval_Status__c !='Draft' ||proj.Approval_Status__c !='Forecast')){
               renderBlockone=false;
                renderBlocktwo=false;
                renderBlockthree=false;
                renderBlockfour=true;renderBlockfive=false;
           }           
            if(lnkPOSList.size()>0){
                for(Linked_POS__c obj:lnkPOSList){
                    if(obj.Proposed_Installation_Date__c != null ){
                        renderSaveButton =true;  break;} 
                
                }
            }   
    }
 }
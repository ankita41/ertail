public with sharing class CornerMapController {
	public Corner__c corner {get; set;}
	public Account pos {get; set;}
	public String address {get; set;}
	
	public CornerMapController (ApexPages.StandardController stdController) {
        corner = (Corner__c)stdController.getRecord();        
        corner = [SELECT Id, Name, Point_of_Sale__c, Location__Latitude__s, Location__Longitude__s FROM Corner__c WHERE Id = :corner.Id];
        
        pos = [SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE Id = :corner.Point_of_Sale__c];
        
        address =  StaticFunctions.jsencode(
        				(pos.BillingStreet != null ? (pos.BillingStreet + '<br/>') : '') + 
	            		(pos.BillingCity != null ? (pos.BillingCity + ', ') : '') + 
	            		(pos.BillingPostalCode != null ? (pos.BillingPostalCode + '') : '') + '<br/>' + 
	            		(pos.BillingCountry != null ? pos.BillingCountry : '') 
	            		
            		);
    }
}
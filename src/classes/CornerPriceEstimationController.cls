/*****
*   @Class      :   CornerPriceEstimationController .cls
*   @Description:   Controller for CornerPriceEstimation.page 
*   @Author     :   Koteswararao
*   @Created    :   20-Nov-2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/
public with sharing class CornerPriceEstimationController {

    public PageReference newSearch() {
        isProductSection =false;
        isPriceSection = false;
        isNewSearch=true;
        isoldSelection=false;
        oldPricelistWrapper = new pricelistWrapper();
        return null;
    }


    public PageReference addNewProducts() {
        if(pricelistWrapper1 != null){
            oldPricelistWrapper = new pricelistWrapper();
            oldPricelistWrapper=pricelistWrapper1 ;
        }
        isProductSection =false;
        isPriceSection = false;
        isNewSearch=true;
        isoldSelection=true;
        return null;
    }


    public PageReference delselectedProducts() {
        List<productWrapper> productList=new List<productWrapper>();
        List<productWrapper> productList1=new List<productWrapper>();
        if(pricelistWrapper1 != null){
               productList= pricelistWrapper1.productPriceList;
            //map<String,List<productWrapper>> selectedProducts=pricelistWrapper1 .supplierproductmap;
            pricelistWrapper pricelistWrapper2= new pricelistWrapper();
            if(productList!= null && productList.size()>0){        
               
                Decimal totalcost=0;
                categoryTypeSet= new Set<String>();
               // if(productList != null && productList.size()>0){
                    for(productWrapper obj:productList){
                        system.debug('obj.isselected@@@'+obj.isselected);
                        if(!obj.isselected){
                            isProductSection =false;
                            isPriceSection = true;
                            isNewSearch=false;
                            isoldSelection=false;
                        //if(selectedQty)
                            categoryTypeSet.add(obj.productType);
                            obj.isselected=false;
                            if(obj.productprice != 0 && obj.productprice!=0.00){
                                obj.productcost= integer.valueof(obj.selectedQty)*obj.productprice;
                                totalcost+=obj.productcost;
                            }
                            productList1.add(obj);
                            /*if(categoryMap!= null && categoryMap.containskey(obj.productType)){
                                categoryMap.get(obj.productType).add(obj);
                                decimal cost=categorypriceMap.get(obj.productType)+obj.productprice;
                                categorypriceMap.put(obj.productType,cost);                         
                            }else{
                                List<productWrapper> objList= new List<productWrapper>();
                                objList.add(obj);
                                categoryMap.put(obj.productType,objList);  
                                categorypriceMap.put(obj.productType,obj.productprice);                               
                            }*/
                        }                    
                    }
                    //pricelistWrapper2.supplierMapsupplierTotal=categorypriceMap;
                    pricelistWrapper2.productPriceList=productList1;
                    pricelistWrapper2.grandtotal=totalcost;
                    pricelistWrapper2.currencyName=pricelistWrapper1.currencyName;
                    pricelistWrapper1 =new pricelistWrapper();
                    pricelistWrapper1 =pricelistWrapper2;
                //}    
            
            }       
        }
        return null;
    }


    public PageReference addselectedProducts() {
        if(pricelistWrapper != null){
            // TODO: logic to move without selection in next senarios 
            map<String,List<productWrapper>> selectedProducts=pricelistWrapper .supplierproductmap;
            pricelistWrapper1= new pricelistWrapper();
            List<productWrapper> selectedProList=new List<productWrapper>();
            map<String,List<productWrapper>> categoryMap= new map<String,List<productWrapper>>();
            map<String,Decimal> categorypriceMap= new map<String,Decimal>();
            Decimal totalcost=0;
            Set<String> pricelistSet=new Set<String>();
            
             //Previous selected products .
            if(oldPricelistWrapper!= null &&oldPricelistWrapper.productPriceList!= null &&oldPricelistWrapper.productPriceList.size()>0){
                system.debug('oldPricelistWrapper.productPriceList@@@@'+oldPricelistWrapper.productPriceList);
                 //productList.addAll(oldPricelistWrapper.productPriceList); 
                 for(productWrapper oldObj :oldPricelistWrapper.productPriceList){
                     isProductSection =false;
                     isPriceSection = true;
                     isNewSearch=false;
                     isoldSelection=false;
                     totalcost+=oldObj.productcost;
                     pricelistSet.add(oldObj.productid);
                      if(categoryMap!= null && categoryMap.containskey(oldObj.productType)){
                                categoryMap.get(oldObj.productType).add(oldObj );
                               // decimal cost=categorypriceMap.get(obj.productType)+obj.productprice;
                               // categorypriceMap.put(obj.productType,cost);                         
                            }else{
                                List<productWrapper> objList= new List<productWrapper>();
                                objList.add(oldObj );
                                categoryMap.put(oldObj.productType,objList);  
                               // categorypriceMap.put(obj.productType,obj.productprice);                               
                        }
                   }
            }
            
            if(selectedProducts != null && !selectedProducts.isempty()){        
                List<productWrapper> productList=new List<productWrapper>();
                system.debug('oldPricelistWrapper@@@@'+oldPricelistWrapper);
                
                
                for(String obj:selectedProducts.keyset()){
                    productList.addAll(selectedProducts.get(obj));        
                }   
                
                categoryTypeSet= new Set<String>();
                if(productList != null && productList.size()>0){
                    for(productWrapper obj:productList ){
                        if(obj.isselected){
                            isProductSection =false;
                            isPriceSection = true;
                            isNewSearch=false;
                            isoldSelection=false;
                        //if(selectedQty)
                            categoryTypeSet.add(obj.productType);
                            obj.isselected=false;
                            if(!pricelistSet.contains(obj.productid)){
                                pricelistSet.add(obj.productid);
                                if(obj.productprice != 0 && obj.productprice!=0.00){
                                    obj.productcost= integer.valueof(obj.selectedQty)*obj.productprice;
                                    totalcost+=obj.productcost;
                                }
                                if(categoryMap!= null && categoryMap.containskey(obj.productType)){
                                    categoryMap.get(obj.productType).add(obj);
                                  //  decimal cost=categorypriceMap.get(obj.productType)+obj.productprice;
                                  //  categorypriceMap.put(obj.productType,cost);                         
                                }else{
                                    List<productWrapper> objList= new List<productWrapper>();
                                    objList.add(obj);
                                    categoryMap.put(obj.productType,objList);  
                                   // categorypriceMap.put(obj.productType,obj.productprice);                               
                                }
                            }
                        }                    
                    }
                    
                 }    
            
            }       
                    
                    
           // map<String,List<productWrapper>> categoryMap1= new map<String,List<productWrapper>>();
            if(categoryMap!= null && !categoryMap.isEmpty()){
                List<productWrapper> productList1=new List<productWrapper>();
                for(String obj:categoryMap.keyset()){
                    productList1.addAll(categoryMap.get(obj));        
                } 
                //categoryMap1.put('Category',productList1);
               // pricelistWrapper1.supplierproductmap=categoryMap1;
               pricelistWrapper1.productPriceList=productList1;
            }
           // pricelistWrapper1.supplierMapsupplierTotal=categorypriceMap;
            pricelistWrapper1.supplierproductmap=categoryMap;
            pricelistWrapper1.currencyName=pricelistWrapper.currencyName;
            pricelistWrapper1.grandtotal=totalcost;
            
            if(categoryMap== null || categoryMap.isEmpty()){
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please select corner products and click \'Add Selected Products \' button');
                  ApexPages.addMessage(myMsg);
                return null;
            }
                  
        }
    
        return null;
    }


    public PageReference queryForProducts() {
        if(selectedCategory != null && selectedCategory !='' && selectedCategory !='[]'){
            set<String> categorySet=convertSelectedOptionsToSet(selectedCategory);
             List<Corner_Products__c> productList=[Select id,c.Name From Corner_Products__c c  where Type__c IN :categorySet order by c.Name];
                 productOpts=new List<SelectOption>();
                if(productList != null && productList.size()>0){
                    //supplierOpts =new List<SelectOption>();
                    //supplierOpts.add(new SelectOption('ALL','--ALL--',true));
                    for(Corner_Products__c obj:productList){
                        productOpts.add(new SelectOption(obj.id, obj.name));
                    }
                }
            //return optsList;
         }
        return null;
    }
    public Boolean isoldSelection{get;set;}
    public Boolean isNewSearch{get;set;}
    public Boolean isProductSection{get;set;}
    public Boolean isPriceSection{get;set;}
    public User loggedinUser{get;set;}
    public String selectedCategory { get; set; }
    //public String userRoleid{get;set}
    Public Set<String> suppliersSet{get;set;}
    public CornerPriceEstimationController() { //CornerPriceCalController()
        isNewSearch=true;
        isProductSection =false;
        isPriceSection = false;
        populateMarketData();
        
        
    }
    public List<SelectOption> supplierOpts { get;set;}
     public List<SelectOption> categoryOpts { get { return new List<SelectOption>{new SelectOption('Accessories', 'Accessories'), new SelectOption('Enlargement Panel', 'Enlargement Panel'), new SelectOption('Island Furniture', 'Island Furniture'), new SelectOption('Linear Presentation', 'Linear Presentation'),new SelectOption('Miscelleanous', 'Miscelleanous'),new SelectOption('Wall and Pillar Furniture', 'Wall and Pillar Furniture')}; } }
    public Set<String> categoryTypeSet{get;set;}
    public Set<String> supplierSet{get;set;}
    public pricelistWrapper pricelistWrapper{get; private set;}
    public pricelistWrapper pricelistWrapper1{get; private set;}
    public pricelistWrapper oldPricelistWrapper{get; private set;}
    public String selectedSupplier { get; set; }
    public Integer supplierCount{get;set;}
    public PageReference filterproducts() {
        String profilename=loggedinUser.Profile.name;
        if(profilename!= null &&(profilename == 'NES Trade/Proc Manager'||profilename =='NES Market Business Development Manager' || profilename =='Trade Corner System Admin' || profilename =='System Admin')){
             if(selectedSupplier ==null || selectedSupplier =='' || selectedSupplier =='[]'){
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please Select Supplier');
              ApexPages.addMessage(myMsg);
            return null;
        }
        }
        if(selectedCategory ==null || selectedCategory =='' || selectedCategory=='[]'){
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please Select Category ');
              ApexPages.addMessage(myMsg);
            return null;
        }
        String strQuery='Select p.Price__c, p.Id, p.Corner_Products__r.Type__c, p.Corner_Products__r.Description__c, p.Corner_Products__r.Name, '+
                        'p.Corner_Products__r.Id, p.Corner_Products__c,p.Corner_Price_List__r.currency__C, p.Corner_Price_List__r.Supplier__c,p.Corner_Price_List__r.Supplier__r.name,'+
                        ' p.Corner_Price_List__r.Name, p.Corner_Price_List__r.Id, p.Corner_Price_List__c From Product_In_Pricelist__c p ';
       
       if(selectedSupplier !=null && selectedSupplier !='' && selectedSupplier !='[]' &&
           selectedCategory != null && selectedCategory !='' && selectedCategory !='[]'){
           set<String> supplierSet=convertSelectedOptionsToSet(selectedSupplier);
           set<String> categorysSet=convertSelectedOptionsToSet(selectedCategory);
           system.debug('categorySet@@@'+categorysSet+'supplierSet@@'+supplierSet);
           strQuery += ' where   p.Corner_Price_List__r.Supplier__c in : supplierSet  and p.Corner_Products__r.Type__c IN : categorysSet ' ;
       
       }else if(selectedCategory != null && selectedCategory !='' && selectedCategory !='[]'){
             set<String> categorySet=convertSelectedOptionsToSet(selectedCategory);
             strQuery += ' where  p.Corner_Products__r.Type__c IN :categorySet';
             
             system.debug('suppliersSet@@@@@'+suppliersSet);
            if(suppliersSet != null && suppliersSet.size()>0){
                strQuery += ' And p.Corner_Price_List__r.Supplier__c in : suppliersSet ';
             }
        }
        
        /*if(selectedProducts!= null && selectedProducts!='' && selectedProducts!='[]'&& selectedSupplier  != null && selectedSupplier !='' && selectedSupplier !='[]'){
            set<String> supplierSet=convertSelectedOptionsToSet(selectedSupplier );
            strQuery += ' And p.Supplier_Name__c in : supplierSet ';
        }*/
        strQuery += ' order by p.Corner_Products__r.Type__c ' ;
        List<Product_In_Pricelist__c> pricelist= new List<Product_In_Pricelist__c>();
        pricelist=Database.query(strQuery );
       supplierCount=0;
       
       if(pricelist != null && pricelist.size()>0){
           isProductSection =true;
           isPriceSection = false;
           supplierSet = new Set<String>();
           pricelistWrapper= new pricelistWrapper();
           map<String,List<productWrapper>> supplierMap= new map<String,List<productWrapper>>();
           map<String,Decimal> supplierpriceMap= new map<String,Decimal>();
           for(Product_In_Pricelist__c obj:pricelist ){
               supplierSet.add(obj.Corner_Price_List__r.Supplier__r.name);
               if(supplierMap!= null && supplierMap.containskey(obj.Corner_Price_List__r.Supplier__r.name) ){
                        supplierMap.get(obj.Corner_Price_List__r.Supplier__r.name).
                            add(new productWrapper(obj.Corner_Products__r.Name,obj.Corner_Products__r.Type__c,obj.Price__c,obj.Corner_Products__r.Description__c,obj.id,obj.Corner_Price_List__r.Supplier__r.name)) ;  
                        Decimal cost=supplierpriceMap.get(obj.Corner_Price_List__r.Supplier__r.name)+obj.Price__c; 
                        supplierpriceMap.put(obj.Corner_Price_List__r.Supplier__r.name,cost);
               }else{
                   List<productWrapper> objList= new List<productWrapper>();
                   objList.add(new productWrapper(obj.Corner_Products__r.Name,obj.Corner_Products__r.Type__c,obj.Price__c,obj.Corner_Products__r.Description__c,obj.id,obj.Corner_Price_List__r.Supplier__r.name));
                   supplierMap.put(obj.Corner_Price_List__r.Supplier__r.name,objList);
                   supplierpriceMap.put(obj.Corner_Price_List__r.Supplier__r.name,obj.Price__c);
               }
               supplierCount++;
           }
           
           pricelistWrapper.supplierMapsupplierTotal=supplierpriceMap;
           pricelistWrapper.supplierproductmap=supplierMap;
           pricelistWrapper.currencyName=pricelist[0].Corner_Price_List__r.currency__C;
       }else {
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No Corner Products available with this filters ');//Same old drama 
              ApexPages.addMessage(myMsg);
       }
        return null;
    }


  /*  public PageReference querysuppliersForProducts() {
        System.debug('selectedProducts@@'+selectedProducts);
        supplierOpts = new List<SelectOption>();
        if(selectedProducts != null && selectedProducts!=''){
            set<String> productsList=convertSelectedOptionsToSet(selectedProducts);
            System.debug('productsList@@'+productsList);
            if(productsList!= null && productsList.size()>0){                
                List<Pricelist__c> priceList=[Select p.Supplier_Name__c,p.Supplier_Name__r.name, p.Product_Price__c, p.Name,
                     p.Corner_Products__c From Pricelist__c p where p.Corner_Products__c IN: productsList order by p.Supplier_Name__r.name ];
                if(priceList!= null && priceList.size()>0){
                    set<String> supplierset= new set<String>();
                    for(Pricelist__c obj:priceList){
                        if(!supplierset.contains(obj.Supplier_Name__c)){
                            supplierOpts.add(new SelectOption(obj.Supplier_Name__c, obj.Supplier_Name__r.name));
                            supplierset.add(obj.Supplier_Name__c);
                        }
                    }
                }
            }
        }
        return null;
    }*/
    
   
    private Set<String> convertSelectedOptionsToSet(String selectedOptions){
        if (selectedOptions == null)
            return new Set<String>();
        Set<String> tmp = new Set<String>(selectedOptions.replace('[','').replace(']','').replace(', ',',').replace(' ,',',').split(','));
        tmp.remove('');
        return tmp;
    }
    public map<String,String> marketmap{get;set;}
    public map<String,String> supplierMap{get;set;}
    public void populateMarketData(){
        marketmap=new map<String,String>();
        supplierMap= new map<String,String>();
        supplierOpts = new List<SelectOption>();
        List<Market__c> marketList=[SELECT Id, Name, Code__c, IsMine__c, Default_Supplier__c,Default_Supplier__r.name FROM Market__c where Default_Supplier__c!=null order by Default_Supplier__r.name];
        if(marketList!= null && marketList.size()>0){
            for(Market__c obj:marketList){
                marketmap.put(obj.Name,obj.Default_Supplier__c);
                supplierMap.put(obj.Default_Supplier__c,obj.Default_Supplier__r.name);
            }
        }
        
        List<user> userList=[SELECT Id, ProfileId, Profile.name, UserRoleId, UserRole.name FROM User where id= :userinfo.getuserid()];
        if(userList != null && userList.size()>0){
            suppliersSet= new Set<String>();
            loggedinUser=userList[0];
            String userRole=loggedinUser.UserRole.name;
            String profilename=loggedinUser.Profile.name;
            id roleid=loggedinUser.UserRoleId;
            if(userRole != null && userRole!='' && marketmap!= null && !marketmap.isEmpty() && marketmap.containsKey(userRole)){
                suppliersSet.add(marketmap.get(userRole));            
            }else if(profilename != null && profilename=='NES Market Business Development Manager' ){
                if(userRole != null && userRole=='MEAC HUB'){                    
                    for(UserRole obj1:[select Id,name from UserRole where ParentRoleId =:roleid AND ParentRoleID != null]) {
                        if(marketmap!= null && !marketmap.isempty() && marketmap.containskey(obj1.name)){
                            suppliersSet.add(marketmap.get(obj1.name));
                        }
                    }
                }
            }else if(userRole != null && (userRole=='NES Trade/Proc' || userRole=='System Admin' || userRole=='System Administrator') && marketmap!= null && !marketmap.isEmpty()){
                 List<String> supplierlist=marketmap.values();
                 suppliersSet.addall(supplierlist);
            }
            if(suppliersSet!= null && suppliersSet.size()>0){
                for(String supplierId:suppliersSet){
                    supplierOpts.add(new SelectOption(supplierId, supplierMap.get(supplierId)));
                }
            
            }
            
        }
        
    }
    public List<SelectOption> productOpts { get; set; }

    public String selectedProducts { get; set; }
    
    public class pricelistWrapper{
        Public string supplierName{get; private set;}
        public String category{get; private set;}
        public String currencyName{get;set;}
        public List<productWrapper> productPriceList{get;set;}
        public Decimal grandtotal{get; private set;}
        public map<String,Decimal> supplierMapsupplierTotal{get; private set;}
        public map<String,List<productWrapper>> supplierproductmap{get; private set;}
    }
    public class productWrapper{
        public string productName{get; private set;}
        public String selectedQty{get;set;}
        public boolean isselected{get;set;}
        public List<SelectOption> productQty{ get { 
            List<SelectOption> options = new list<SelectOption>();
             for (Integer i = 1; i <= 50; i++)
               {
               options.add(new SelectOption(I+'', I+''));
               }
            return options ;
        }}
        public Decimal productcost{get; private set;}
        public String productType{get; private set;}
        public Decimal  productprice{get; private set;}
        public String description{get; private set;}
        public String productid{get; private set;}
        public String pricelistid{get; private set;}
        public String suppliername{get; private set;}
        public productWrapper(String productName,String productType,Decimal  productprice,String description,String productid,String suppliername){
            this.productName=productName;
            this.productType=productType;
            this.productprice=productprice;
            this.description=description;
            this.productid=productid;
            this.suppliername=suppliername;
        }
        
    }
}
public with sharing class CornerProjectController {
	public class CustomException extends Exception {}
	
	public Project__c project {get; set;}
	public List<Quotation__c> lstQuotations {get; set;}
	private String currentUserProfile;
    
    public Boolean hasAccessToProject {
    	get {
    		String strUserRole = [SELECT Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name;
			return !( (currentUserProfile == 'NES Corner Architect' && UserInfo.getUserId() != project.Architect__c) || 
    				  (currentUserProfile == 'NES Corner Supplier'  && UserInfo.getUserId() != project.Supplier__c) ||
    				  (currentUserProfile == 'NES Market Sales Promoter'  && strUserRole != project.Market__c) || 
    				  (currentUserProfile == 'NES Market Local Manager'  && strUserRole != project.Market__c)  || 
    				  (currentUserProfile == 'NES Market Local Manager (read only)'  && strUserRole != project.Market__c) ||
    				  (currentUserProfile == 'NES Market Business Development Manager' && strUserRole != project.Market__c) ||
    				  (currentUserProfile == 'Agent Local Manager'  && strUserRole != project.Market__c)  
    				  );
    	} set;
    }
    
	public Boolean noQuotations {
		get {
			return lstQuotations.isEmpty();
		} set;
	}
	public Boolean hasDelRights {
		get {
			String hasAccessRights = 'NES Market Local Manager, NES System Admin Platform, NES Trade/Proc Manager, System Admin';
            return hasAccessRights.contains(currentUserProfile);
		} set;
	}
	public Boolean hasAccessToQuotations {
		get {
			String hasAccessRights = 'NES Trade/Proc Manager, Agent Local Manager, NES Market Business Development Manager, NES Market Local Manager, NES Market Local Manager (read only), NES Corner Architect, NES Corner Supplier, System Admin';
            return (hasAccessRights.contains(currentUserProfile) 
            	&& (project.RecordType.DeveloperName == 'Site_Survey'
            		|| project.RecordType.DeveloperName == 'New_Corner'
            	 	|| project.RecordType.DeveloperName == 'New_Corner_Agent'
            	 	|| project.RecordType.DeveloperName == 'New_Corner_Installation_Date'
            	 	|| project.RecordType.DeveloperName == 'Maintenance_outside_Warranty'
            	 	)
        	 	);
		} set;
	}
	
	
	public CornerProjectController(ApexPages.StandardController controller) {
        currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        
        project = (Project__c)controller.getRecord();
        project = [SELECT Id, Name, Architect__c, Nespresso_Contact__c, NES_Sales_Promoter__c, Supplier__c, Market__c, RecordType.DeveloperName FROM Project__c WHERE Id = :project.Id];
        
        if (currentUserProfile == 'NES Corner Supplier') {
        	lstQuotations = [SELECT Id, Name, RecordType.Name, Approval_Status__c, NES_Decision__c, Proforma_no__c, PO_no__c, Total__c FROM Quotation__c WHERE Project__c = :project.Id AND RecordType.DeveloperName IN ('Site_Survey', 'Other_Quotation', 'Supplier_Quotation', 'Supplier_Quotation_for_Architect_Approval')];
        } else {
        	lstQuotations = [SELECT Id, Name, RecordType.Name, Approval_Status__c, NES_Decision__c, Proforma_no__c, PO_no__c, Total__c FROM Quotation__c WHERE Project__c = :project.Id AND RecordType.DeveloperName IN ('Site_Survey', 'Other_Quotation', 'Supplier_Quotation', 'Supplier_Quotation_for_Architect_Approval', 'Architect_Quotation')];
        }
        
    }
    
    public PageReference newQuotation() {
    	String rtSelection = '';
    	if (project.RecordType.DeveloperName == 'Site_Survey'){
    		rtSelection = '&allowRTS=false&RecordType=' + Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Site Survey').getRecordTypeId();
    	}else{
    		// User is Architect, default to Architect Quotation
    		if ('NES Corner Architect' == currentUserProfile)
    			rtSelection = '&allowRTS=false&RecordType=' + Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Architect Quotation').getRecordTypeId();
    		// User id Supplier, default to Supplier Qiotation
    		else if('NES Corner Supplier' == currentUserProfile)
    			rtSelection = '&allowRTS=false&RecordType=' + Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
    		else
    			rtSelection = '&allowRTS=true';
    	}
    	
    	PageReference curPage = new PageReference('/apex/RedirectWithVariables?object=Quotation__c&Project__c=' + project.Name + '&ID_Project__c=' + project.Id + '&ent=' + CustomRedirection__c.getValues('QuotationObjId').Value__c + '&retURL=%2F' + project.Id + rtSelection);//ApexPages.currentPage();
        //curPage.getParameters().put('URL','/apex/RedirectWithVariables?object=Quotation__c&Project__c=' + project.Name + '&ID_Project__c=' + project.Id + '&ent=' + CustomRedirection__c.getValues('QuotationObjId').Value__c + '&allowRTS=true&retURL=%2F' + project.Id);
        //curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
}
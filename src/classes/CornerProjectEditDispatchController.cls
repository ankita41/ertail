public with sharing class CornerProjectEditDispatchController {
	private ApexPages.StandardController controller;
	
	public CornerProjectEditDispatchController(ApexPages.StandardController controller) {
        this.controller = controller;
    }    
	
	private RecordType getProjectRecordType(String rtName){
        for (RecordType rt : projectRecordTypes){
            if (rt.DeveloperName == rtName){
                return rt;
            }
        }
        return null;
    }
    
    private List<RecordType> projectRecordTypes{
        get{
            if (projectRecordTypes == null){
                projectRecordTypes = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType = 'Project__c'];
            }
            return projectRecordTypes;
        }
        set;
    }
	
	private RecordType newCornerRT{
        get{
            if (newCornerRT == null){
                newCornerRT = getProjectRecordType('New_Corner');
            }
            return newCornerRT;
        }
        set;
    }
    
     private RecordType newCornerAgentRT{
        get{
            if (newCornerAgentRT == null){
                newCornerAgentRT = getProjectRecordType('New_Corner_Agent');
            }
            return newCornerAgentRT;
        }
        set;
    }
    
    private RecordType cancelledCornerRT{
        get{
            if (cancelledCornerRT == null){
                cancelledCornerRT = getProjectRecordType('Corner_Cancelled');
            }
            return cancelledCornerRT;
        }
        set;
    }
	
	public PageReference getRedir() {
		PageReference newPage = null;
		
		Map<String, String> mapUrlParam = new Map<String, String>();
		mapUrlParam = ApexPages.currentPage().getParameters();
		
		String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
		
		Project__c project = (Project__c)controller.getRecord();
        project = [SELECT Id, RecordTypeId, Name, Architect__c, Nespresso_Contact__c, NES_Sales_Promoter__c, Supplier__c, Market__c FROM Project__c WHERE Id = :project.Id];
		String strUserRole = [SELECT Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name; 
		
		if ( (currentUserProfile == 'NES Corner Architect' && UserInfo.getUserId() != project.Architect__c) || 
			  (currentUserProfile == 'NES Corner Supplier'  && UserInfo.getUserId() != project.Supplier__c) ||
			  (currentUserProfile == 'NES Market Sales Promoter'  && strUserRole != project.Market__c) ||
			  (currentUserProfile == 'NES Market Local Manager' && strUserRole != project.Market__c) ) {
			
			newPage = new PageReference('/apex/InsufficientPrivileges');
			
		} else {
			
			// Default behavior
		    newPage = new PageReference('/' + project.Id + '/e');
		    newPage.getParameters().put('nooverride', '1');
		    for (String key : mapUrlParam.keySet()) {
				newPage.getParameters().put(key, mapUrlParam.get(key)); 
			}
		}
		
		return newPage.setRedirect(true);
	}
}
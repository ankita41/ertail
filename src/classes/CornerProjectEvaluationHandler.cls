public with sharing class CornerProjectEvaluationHandler {
    
    public static void UpdateProjectCornerEvaluationAfterFileAreaUpsert(List<Project_File_Area__c> pfaList){
        Map<Id, RecordType> fileAreaRTMap = new Map<Id,RecordType>([Select Id, DeveloperName from RecordType where SobjectType='Project_File_Area__c']);
        
        Map<Id, Corner_Project_Evaluation__c> pceOrganisedByIdMap = new Map<Id, Corner_Project_Evaluation__c>();
        Map<Id, Corner_Project_Evaluation__c> pceToUpdate = new Map<Id, Corner_Project_Evaluation__c>();
        Set<Id> projIdSet = new Set<Id>();
        // Get the list of procject Ids
        for(Project_File_Area__c pfa : pfaList){
            if (pfa.Project__c != null)
                projIdSet.add(pfa.Project__c);
        }
        
        // request the relevant Corner_Project_Evaluation
        for(Corner_Project_Evaluation__c cpe : [Select Id, Approved_Supplier_Shop_Fitting_Drawing__c, Layout_Approved_Date__c, Corner_Project__c, Standard_Non_Standard__c, Tech_Drawing_submission_date__c, Supplier_Shop_Fittings_Drawings_Sent__c From Corner_Project_Evaluation__c where Corner_Project__c in :projIdSet])
            pceOrganisedByIdMap.put(cpe.Corner_Project__c, cpe);
            
        for(Project_File_Area__c pfa : pfaList){
            if (pceOrganisedByIdMap.containsKey(pfa.Project__c)){
                Corner_Project_Evaluation__c cpe = pceOrganisedByIdMap.get(pfa.Project__c);
                if (fileAreaRTMap.get(pfa.RecordTypeId).DeveloperName == 'Executive_Drawings'){
                    if(pfa.File_sent_date__c != null && cpe.Tech_Drawing_submission_date__c == null){
                        cpe.Tech_Drawing_submission_date__c = Date.valueOf(pfa.File_sent_date__c);
                        pceToUpdate.put(cpe.Id, cpe);
                    }
                }else if(fileAreaRTMap.get(pfa.RecordTypeId).DeveloperName == 'Supplier_Drawings'){
                    if (pfa.File_sent_date__c != null && cpe.Supplier_Shop_Fittings_Drawings_Sent__c == null){
                        cpe.Supplier_Shop_Fittings_Drawings_Sent__c = Date.valueOf(pfaList[0].File_sent_date__c);
                        pceToUpdate.put(cpe.Id, cpe);
                    }
                    
                    if (pfa.Architect_Decision__c == 'Approved' && pfa.Architect_Decision_Sent_Date__c != null && cpe.Approved_Supplier_Shop_Fitting_Drawing__c == null){
                        cpe.Approved_Supplier_Shop_Fitting_Drawing__c = Date.valueOf(pfa.Architect_Decision_Sent_Date__c);
                        pceToUpdate.put(cpe.Id, cpe);
                    }
                }else if(fileAreaRTMap.get(pfa.RecordTypeId).DeveloperName == 'Layout_Proposal'){
                    if (pfa.Approval_Status__c == 'Approved by NES'){
                        if(pfa.NES_Dis_or_Approval_date__c != null && cpe.Layout_Approved_Date__c == null){
                            cpe.Layout_Approved_Date__c = Date.valueOf(pfa.NES_Dis_or_Approval_date__c);
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                        if (pfa.Standardization__c != null && cpe.Standard_Non_Standard__c == null){
                            cpe.Standard_Non_Standard__c = pfa.Standardization__c;
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                    }else if (pfa.Approval_Status__c == 'Approved by Agent'){
                        if(pfa.Agent_Dis_Approval_date__c != null && cpe.Layout_Approved_Date__c == null){
                            cpe.Layout_Approved_Date__c = Date.valueOf(pfa.Agent_Dis_Approval_date__c);
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                        if (pfa.Standardization__c != null && cpe.Standard_Non_Standard__c == null){
                            cpe.Standard_Non_Standard__c = pfa.Standardization__c;
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                    }
                }
            }
        }
        if (!pceToUpdate.isEmpty())
            update pceToUpdate.values();
    }
  
    public static void UpdateProjectCornerEvaluationAfterQuotationUpsert(List<Quotation__c> quotationList){
        Map<Id, RecordType> quotationRTMap = new Map<Id,RecordType>([Select Id, DeveloperName from RecordType where SobjectType='Quotation__c']);
        
        Map<Id, Corner_Project_Evaluation__c> pceOrganisedByIdMap = new Map<Id, Corner_Project_Evaluation__c>();
        Map<Id, Corner_Project_Evaluation__c> pceToUpdate = new Map<Id, Corner_Project_Evaluation__c>();
        Set<Id> projIdSet = new Set<Id>();
        // Get the list of procject Ids
        for(Quotation__c quot: quotationList){
            if (quot.Project__c != null)
                projIdSet.add(quot.Project__c);
        }
        
        // request the relevant Corner_Project_Evaluation
        for(Corner_Project_Evaluation__c cpe : [Select Id, Arch_Quotation_Approved__c, Supplier_Quotation_Validated__c, Corner_Project__c, Date_PO_Number_was_sent_to_Architect__c, Archi_PO_N__c, Supplier_Quote_Sent__c, Supplier_PO_N__c, Date_PO_Number_was_sent_to_Supplier__c, Architect_Quote_Sent__c  From Corner_Project_Evaluation__c where Corner_Project__c in :projIdSet])
            pceOrganisedByIdMap.put(cpe.Corner_Project__c, cpe);
        
        for(Quotation__c quot: quotationList){
            if (pceOrganisedByIdMap.containsKey(quot.Project__c)){
                Corner_Project_Evaluation__c cpe = pceOrganisedByIdMap.get(quot.Project__c);
                if (quotationRTMap.get(quot.RecordTypeId).DeveloperName == 'Supplier_Quotation' || quotationRTMap.get(quot.RecordTypeId).DeveloperName == 'Supplier_Quotation_for_Architect_Approval'){
                    if(quot.Quotation_sent_date__c != null && cpe.Supplier_Quote_Sent__c == null){
                        cpe.Supplier_Quote_Sent__c = Date.valueOf(quot.Quotation_sent_date__c);
                        pceToUpdate.put(cpe.Id, cpe);
                    }
                    if (quot.Approval_Status__c == 'Approved by NES'){
                        if (quot.PO_no__c != null && cpe.Supplier_PO_N__c == null){
                            cpe.Supplier_PO_N__c = quot.PO_no__c;
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                        if (quot.NES_Dis_Approval_date__c != null && cpe.Date_PO_Number_was_sent_to_Supplier__c == null){
                            cpe.Date_PO_Number_was_sent_to_Supplier__c = Date.valueOf(quot.NES_Dis_Approval_date__c);
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                    }else if (quot.Approval_Status__c == 'Approved by Agent'){
                        if (quot.PO_no__c != null && cpe.Supplier_PO_N__c == null){
                            cpe.Supplier_PO_N__c = quot.PO_no__c;
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                        if (quot.Agent_Dis_Approval_date__c != null && cpe.Date_PO_Number_was_sent_to_Supplier__c == null){
                            cpe.Date_PO_Number_was_sent_to_Supplier__c = Date.valueOf(quot.Agent_Dis_Approval_date__c);
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                    }
                    // Search for the first approved Quotation by the Architect
                    if (quot.Architect_Dis_Approval__c == true && quot.Architect_Decision__c == 'Approved'){
                        if (quot.Architect_Decision_Sent_Date__c != null && cpe.Supplier_Quotation_Validated__c == null){
                            cpe.Supplier_Quotation_Validated__c = Date.valueOf(quot.Architect_Decision_Sent_Date__c);
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                    }
                }else if(quotationRTMap.get(quot.RecordTypeId).DeveloperName == 'Architect_Quotation'){
                    if (quot.Quotation_sent_date__c != null && cpe.Architect_Quote_Sent__c == null){
                        cpe.Architect_Quote_Sent__c = Date.valueOf(quot.Quotation_sent_date__c);
                        pceToUpdate.put(cpe.Id, cpe);
                    }
                    if (quot.Approval_Status__c == 'Approved by NES'){
                        if (quot.PO_no__c != null && cpe.Archi_PO_N__c == null){
                            cpe.Archi_PO_N__c = quot.PO_no__c;
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                        if (quot.NES_Dis_Approval_date__c != null && cpe.Date_PO_Number_was_sent_to_Architect__c == null){
                            cpe.Date_PO_Number_was_sent_to_Architect__c = Date.valueOf(quot.NES_Dis_Approval_date__c);
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                        if (quot.NES_Dis_Approval_date__c != null && cpe.Arch_Quotation_Approved__c == null){
                            cpe.Arch_Quotation_Approved__c = Date.valueOf(quot.NES_Dis_Approval_date__c);
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                    }if (quot.Approval_Status__c == 'Approved by Agent'){
                        if (quot.PO_no__c != null && cpe.Archi_PO_N__c == null){
                            cpe.Archi_PO_N__c = quot.PO_no__c;
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                        if (quot.Agent_Dis_Approval_date__c != null && cpe.Date_PO_Number_was_sent_to_Architect__c == null){
                            cpe.Date_PO_Number_was_sent_to_Architect__c = Date.valueOf(quot.Agent_Dis_Approval_date__c);
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                        if (quot.Agent_Dis_Approval_date__c != null && cpe.Arch_Quotation_Approved__c == null){
                            cpe.Arch_Quotation_Approved__c = Date.valueOf(quot.Agent_Dis_Approval_date__c);
                            pceToUpdate.put(cpe.Id, cpe);
                        }
                    }
                }
            }
        }
        if (!pceToUpdate.isEmpty())
            update pceToUpdate.values();
    
    }
  
    public static void UpdateProjectCornerEvaluationAfterHandoverReportUpsert(List<Handover_Report__c> hrList){
        Map<Id, Corner_Project_Evaluation__c> pceOrganisedByIdMap = new Map<Id, Corner_Project_Evaluation__c>();
        Map<Id, Corner_Project_Evaluation__c> pceToUpdate = new Map<Id, Corner_Project_Evaluation__c>();
        Set<Id> projIdSet = new Set<Id>();
        // Get the list of procject Ids
        for(Handover_Report__c hr : hrList){
            if (hr.Project__c != null)
                projIdSet.add(hr.Project__c);
        }
        
        // request the relevant Corner_Project_Evaluation
        for(Corner_Project_Evaluation__c cpe : [Select Id, Corner_Project__c, Disapproved_HR_Date__c From Corner_Project_Evaluation__c where Corner_Project__c in :projIdSet])
            pceOrganisedByIdMap.put(cpe.Corner_Project__c, cpe);
            
        for(Handover_Report__c hr : hrList){
            if (pceOrganisedByIdMap.containsKey(hr.Project__c)){
                Corner_Project_Evaluation__c cpe = pceOrganisedByIdMap.get(hr.Project__c);
                if(hr.NES_Dis_Approval_date__c != null && cpe.Disapproved_HR_Date__c == null){
                    cpe.Disapproved_HR_Date__c = Date.ValueOf(hr.NES_Dis_Approval_date__c);
                    pceToUpdate.put(cpe.Id, cpe);
                }
            }
        }
        if (!pceToUpdate.isEmpty())
            update pceToUpdate.values();
    }
    
    public static void UpdateProjectCornerEvaluationAfterActionPlanUpsert(List<Handover_Action_Plan__c> newList){
        Map<Id, Corner_Project_Evaluation__c> pceOrganisedByIdMap = new Map<Id, Corner_Project_Evaluation__c>();
        Set<Id> apIdSet = new Set<Id>();
        Set<Id> projIdSet = new Set<Id>();
        Map<Id, Corner_Project_Evaluation__c> pceToUpdate = new Map<Id, Corner_Project_Evaluation__c>();
        for (Handover_Action_Plan__c ap : newList)
            apIdSet.add(ap.Id);
        
        List<Handover_Action_Plan__c> apList = [Select h.Handover_Report__r.Project__c, h.Handover_Report__c, h.Action_plan_sent_date__c From Handover_Action_Plan__c h];
        for (Handover_Action_Plan__c ap : apList)
            if (ap.Handover_Report__c != null && ap.Handover_Report__r.Project__c != null)
                projIdSet.add(ap.Handover_Report__r.Project__c);
        
        // request the relevant Corner_Project_Evaluation
        for(Corner_Project_Evaluation__c cpe : [Select Id, Corner_Project__c, Action_Plan_Date__c from Corner_Project_Evaluation__c where Corner_Project__c in :projIdSet])
            pceOrganisedByIdMap.put(cpe.Corner_Project__c, cpe);
            
        for(Handover_Action_Plan__c ap : apList){
            if (pceOrganisedByIdMap.containsKey(ap.Handover_Report__r.Project__c)){
                Corner_Project_Evaluation__c cpe = pceOrganisedByIdMap.get(ap.Handover_Report__r.Project__c);
                if(ap.Action_plan_sent_date__c != null && cpe.Action_Plan_Date__c == null){
                    cpe.Action_Plan_Date__c = Date.ValueOf(ap.Action_plan_sent_date__c);
                    pceToUpdate.put(cpe.Id, cpe);
                }
            }
        }
        if (!pceToUpdate.isEmpty())
            update pceToUpdate.values();
    }

	public static void CreateCornerProjectEvaluationAfterProjectInsert(List<Project__c> projList){
		List<Corner_Project_Evaluation__c> cpeListToInsert = CreateCornerProjectEvaluation(projList);
		if (!cpeListToInsert.isEmpty())
			insert cpeListToInsert;
	}

    private static List<Corner_Project_Evaluation__c> CreateCornerProjectEvaluation(List<Project__c> projList) {
        List<Corner_Project_Evaluation__c> cpeList = new List<Corner_Project_Evaluation__c>();
        for (Project__c proj : projList)
        	if (proj.RecordTypeId == CornerProjectHelper.cornerForecastAgentRT.Id || proj.RecordTypeId == CornerProjectHelper.cornerForecastRT.Id)
            	cpeList.add(new Corner_Project_Evaluation__c(Corner_Project__c = proj.Id));
        return cpeList;
    }
   
    
    
    // Used to set the values when introducing the new Corner Project Evaluation Object
    private static void InitQuotationFields(List<Corner_Project_Evaluation__c> cpeList){
        Map<Id, List<Quotation__c>> supplierQuotationsOrganisedByCornerProject = new Map<Id, List<Quotation__c>>();
        Map<Id, List<Quotation__c>> architectQuotationsOrganisedByCornerProject = new Map<Id, List<Quotation__c>>();
        
        // Request all Supplier Quotations 
        for (Quotation__c q : [Select q.Architect_Dis_Approval__c, q.Architect_Decision__c, Agent_Dis_Approval_date__c, q.Architect_Decision_Sent_Date__c, q.RecordType.DeveloperName, q.createdDate, q.Quotation_sent_date__c, q.Project__c, q.PO_no__c, q.NES_Dis_Approval_date__c, q.Approval_Status__c From Quotation__c q  where RecordType.DeveloperName in ('Supplier_Quotation', 'Supplier_Quotation_for_Architect_Approval', 'Architect_Quotation') order by Project__c, Quotation_sent_date__c asc]){
            if (q.RecordType.DeveloperName == 'Supplier_Quotation' || q.RecordType.DeveloperName == 'Supplier_Quotation_for_Architect_Approval'){
                if (!supplierQuotationsOrganisedByCornerProject.containsKey(q.Project__c))
                    supplierQuotationsOrganisedByCornerProject.put(q.Project__c, new List<Quotation__c>());
                supplierQuotationsOrganisedByCornerProject.get(q.Project__c).add(q);
            }else if (q.RecordType.DeveloperName == 'Architect_Quotation'){
                if (!architectQuotationsOrganisedByCornerProject.containsKey(q.Project__c))
                    architectQuotationsOrganisedByCornerProject.put(q.Project__c, new List<Quotation__c>());
                architectQuotationsOrganisedByCornerProject.get(q.Project__c).add(q);
            }
        }
        
        // Request all the Corner Project Evaluations
        for(Corner_Project_Evaluation__c cpe : cpeList){
            if (supplierQuotationsOrganisedByCornerProject.containskey(cpe.Corner_Project__c)){
                List<Quotation__c> qList = supplierQuotationsOrganisedByCornerProject.get(cpe.Corner_Project__c);
                cpe.Supplier_Quote_Sent__c = Date.valueOf(qList[0].Quotation_sent_date__c);
                
                // Search for the first approved Quotation
                for (Quotation__c q : qList){
                    if (q.Approval_Status__c == 'Approved by NES'){
                        cpe.Supplier_PO_N__c = q.PO_no__c;
                        cpe.Date_PO_Number_was_sent_to_Supplier__c = Date.valueOf(q.NES_Dis_Approval_date__c);
                        break;
                    }else if (q.Approval_Status__c == 'Approved by Agent'){
                        cpe.Supplier_PO_N__c = q.PO_no__c;
                        cpe.Date_PO_Number_was_sent_to_Supplier__c = Date.valueOf(q.Agent_Dis_Approval_date__c);
                        break;
                    }
                }
                
                // Search for the first approved Quotation by the Architect
                for (Quotation__c q : qList){
                    if (q.Architect_Dis_Approval__c == true && q.Architect_Decision__c == 'Approved'){
                        cpe.Supplier_Quotation_Validated__c = Date.valueOf(q.Architect_Decision_Sent_Date__c);
                        break;
                    }
                }
            }
            
            if (architectQuotationsOrganisedByCornerProject.containskey(cpe.Corner_Project__c)){
                List<Quotation__c> qList = architectQuotationsOrganisedByCornerProject.get(cpe.Corner_Project__c);
                cpe.Architect_Quote_Sent__c = Date.valueOf(qList[0].Quotation_sent_date__c);
                
                // Search for the first approved Quotation
                for (Quotation__c q : qList){
                    if (q.Approval_Status__c == 'Approved by NES'){
                        cpe.Archi_PO_N__c = q.PO_no__c;
                        cpe.Date_PO_Number_was_sent_to_Architect__c = Date.valueOf(q.NES_Dis_Approval_date__c);
                        cpe.Arch_Quotation_Approved__c = Date.valueOf(q.NES_Dis_Approval_date__c);
                        break;
                    }
                }
            }
        }
    }
    
    // Used to set the values when introducing the new Corner Project Evaluation Object
    private static void InitFileAreaFields(List<Corner_Project_Evaluation__c> cpeList){
        Map<Id, List<Project_File_Area__c>> supplierDrawingsOrganisedByCornerProject = new Map<Id, List<Project_File_Area__c>>();
        Map<Id, List<Project_File_Area__c>> architectDrawingsOrganisedByCornerProject = new Map<Id, List<Project_File_Area__c>>();
        Map<Id, List<Project_File_Area__c>> layoutProposalsOrganisedByCornerProject = new Map<Id, List<Project_File_Area__c>>();
        
        // Request all Supplier and Architect Drawings
        for (Project_File_Area__c pfa : [Select p.RecordType.DeveloperName, p.Standardization__c, p.RecordTypeId, p.Project__c, p.File_sent_date__c, p.Architect_Decision__c, p.Architect_Decision_Sent_Date__c, p.NES_Dis_or_Approval_date__c, p.NES_Decision__c  From Project_File_Area__c p where p.RecordType.DeveloperName in ('Executive_Drawings', 'Supplier_Drawings', 'Layout_Proposal') order by Project__c, p.RecordType.DeveloperName, p.File_sent_date__c asc]){
            if (pfa.RecordType.DeveloperName == 'Executive_Drawings'){
                if (!architectDrawingsOrganisedByCornerProject.containsKey(pfa.Project__c))
                    architectDrawingsOrganisedByCornerProject.put(pfa.Project__c, new List<Project_File_Area__c>());
                architectDrawingsOrganisedByCornerProject.get(pfa.Project__c).add(pfa);
            }else if (pfa.RecordType.DeveloperName == 'Supplier_Drawings'){
                if (!supplierDrawingsOrganisedByCornerProject.containsKey(pfa.Project__c))
                    supplierDrawingsOrganisedByCornerProject.put(pfa.Project__c, new List<Project_File_Area__c>());
                supplierDrawingsOrganisedByCornerProject.get(pfa.Project__c).add(pfa);
            }else if (pfa.RecordType.DeveloperName == 'Layout_Proposal'){
                if (!layoutProposalsOrganisedByCornerProject.containsKey(pfa.Project__c))
                    layoutProposalsOrganisedByCornerProject.put(pfa.Project__c, new List<Project_File_Area__c>());
                layoutProposalsOrganisedByCornerProject.get(pfa.Project__c).add(pfa);
            }
        }
        
        // Request all the Corner Project Evaluations
        for(Corner_Project_Evaluation__c cpe : cpeList){
            if (architectDrawingsOrganisedByCornerProject.containskey(cpe.Corner_Project__c)){
                List<Project_File_Area__c> pfaList = architectDrawingsOrganisedByCornerProject.get(cpe.Corner_Project__c);
                cpe.Tech_Drawing_submission_date__c = Date.valueOf(pfaList[0].File_sent_date__c);
            }
            if (supplierDrawingsOrganisedByCornerProject.containsKey(cpe.Corner_Project__c)){
                List<Project_File_Area__c> pfaList = supplierDrawingsOrganisedByCornerProject.get(cpe.Corner_Project__c);
                cpe.Supplier_Shop_Fittings_Drawings_Sent__c = Date.valueOf(pfaList[0].File_sent_date__c);
                for (Project_File_Area__c pfa : pfaList){
                    if (pfa.Architect_Decision__c == 'Approved'){
                        cpe.Approved_Supplier_Shop_Fitting_Drawing__c = Date.valueOf(pfa.Architect_Decision_Sent_Date__c);
                        break;
                    }
                }
            }
            if (layoutProposalsOrganisedByCornerProject.containsKey(cpe.Corner_Project__c)){
                List<Project_File_Area__c> pfaList = layoutProposalsOrganisedByCornerProject.get(cpe.Corner_Project__c);
                for (Project_File_Area__c pfa : pfaList){
                    if (pfa.NES_Decision__c == 'Approved'){
                        cpe.Layout_Approved_Date__c = Date.valueOf(pfa.NES_Dis_or_Approval_date__c);
                        cpe.Standard_Non_Standard__c = pfa.Standardization__c;
                        break;
                    }
                }
            }
        }
    }
    
    // Used to set the values when introducing the new Corner Project Evaluation Object
    private static void InitHandoverReportFields(List<Corner_Project_Evaluation__c> cpeList){
        Map<Id, List<Handover_Report__c>> hrOrganisedByProjectCornerMap = new Map<Id, List<Handover_Report__c>>();
        // Request all Handover Reports
        for (Handover_Report__c hr : [Select h.Id, h.NES_Dis_Approval_date__c, h.NES_Decision__c, Project__c From Handover_Report__c h where h.NES_Dis_Approval_date__c != null order by Project__c, NES_Dis_Approval_date__c asc]){
            if (!hrOrganisedByProjectCornerMap.containsKey(hr.Project__c))
                hrOrganisedByProjectCornerMap.put(hr.Project__c, new List<Handover_Report__c>());
            hrOrganisedByProjectCornerMap.get(hr.Project__c).add(hr);
        }
        
        // Request all the Corner Project Evaluations
        for(Corner_Project_Evaluation__c cpe : cpeList){
            if (hrOrganisedByProjectCornerMap.containsKey(cpe.Corner_Project__c))
                cpe.Disapproved_HR_Date__c = Date.ValueOf(hrOrganisedByProjectCornerMap.get(cpe.Corner_Project__c)[0].NES_Dis_Approval_date__c);
        }
    }
    
    // Used to set the values when introducing the new Corner Project Evaluation Object
    private static void InitActionPlanFields(List<Corner_Project_Evaluation__c> cpeList){
        Map<Id, List<Handover_Action_Plan__c>> hapOrganisedByProjectCornerMap = new Map<Id, List<Handover_Action_Plan__c>>();
        // Request all Handover Reports
        for (Handover_Action_Plan__c hap : [Select h.Handover_Report__r.Project__c, h.Handover_Report__c, h.Action_plan_sent_date__c From Handover_Action_Plan__c h order by Handover_Report__r.Project__c, Action_plan_sent_date__c asc]){
            if (hap.Handover_Report__c != null && hap.Handover_Report__r.Project__c != null){
                if(!hapOrganisedByProjectCornerMap.containsKey(hap.Handover_Report__r.Project__c))
                    hapOrganisedByProjectCornerMap.put(hap.Handover_Report__r.Project__c, new List<Handover_Action_Plan__c>());
                hapOrganisedByProjectCornerMap.get(hap.Handover_Report__r.Project__c).add(hap);
            }
        }
        
        // Request all the Corner Project Evaluations
        for(Corner_Project_Evaluation__c cpe : cpeList){
            if (hapOrganisedByProjectCornerMap.containsKey(cpe.Corner_Project__c))
                cpe.Action_Plan_Date__c = Date.ValueOf(hapOrganisedByProjectCornerMap.get(cpe.Corner_Project__c)[0].Action_plan_sent_date__c);
        }
    }
    
    public static void CreateAllCornerProjectEvaluation(){
        // delete existing corner Project Evaluations
        delete [Select Id from Corner_Project_Evaluation__c];
        
        List<Project__c> projList = [Select Id, RecordTypeId from Project__c];
        
        // Recreate all corner Project Evaluations
        List<Corner_Project_Evaluation__c> cpeToCreateList = CreateCornerProjectEvaluation(projList);
        InitQuotationFields(cpeToCreateList);
        InitFileAreaFields(cpeToCreateList);
        InitHandoverReportFields(cpeToCreateList);
        InitActionPlanFields(cpeToCreateList);
        insert cpeToCreateList;
    }
    
}
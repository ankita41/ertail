public with sharing class CornerProjectHelper {
    public static RecordType getProjectRecordType(String rtName){
        for (RecordType rt : projectRecordTypes){
            if (rt.DeveloperName == rtName){
                return rt;
            }
        }
        return null;
    }
    
    public static List<RecordType> projectRecordTypes{
        get{
            if (projectRecordTypes == null){
                projectRecordTypes = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType = 'Project__c'];
            }
            return projectRecordTypes;
        }
        set;
    }
    
    public static RecordType closedCornerRT{
        get{
            if (closedCornerRT == null){ closedCornerRT = getProjectRecordType('Corner_Closed'); }
            return closedCornerRT;
        }
        set;
    }
    
    public static RecordType cancelledCornerRT{
        get{
            if (cancelledCornerRT == null){ cancelledCornerRT = getProjectRecordType('Corner_Cancelled'); }
            return cancelledCornerRT;
        }
        set;
    }
    
     public static RecordType newCornerRT{
        get{
            if (newCornerRT == null){ newCornerRT = getProjectRecordType('New_Corner'); }
            return newCornerRT;
        }
        set;
    }
    /******** Added by : Promila, Date:11/05/2015, Description: Method to get Record type for New Mass Corner,New Mass Corner(Agent) *****/
    /******starts here *******/
    public static RecordType newMassCornerRT{
        get{
            if (newMassCornerRT== null){ newMassCornerRT= getProjectRecordType('New_Mass_Corner'); }
            return newMassCornerRT;
        }
        set;
    }
    public static RecordType newMassCornerAgentRT{
        get{
            if (newMassCornerAgentRT== null){ newMassCornerAgentRT= getProjectRecordType('New_Mass_Corner_Agent'); }
            return newMassCornerAgentRT;
        }
        set;
    }
    /*********end here **********/
    public static RecordType maintenanceOutsideWarrantyRT{
        get{
            if (maintenanceOutsideWarrantyRT == null){ maintenanceOutsideWarrantyRT = getProjectRecordType('Maintenance_outside_warranty'); }
            return maintenanceOutsideWarrantyRT;
        }
        set;
    }
    
    public static RecordType maintenanceUnderWarrantyRT{
        get{
            if (maintenanceUnderWarrantyRT == null){ maintenanceUnderWarrantyRT = getProjectRecordType('Maintenance_under_warranty'); }
            return maintenanceUnderWarrantyRT;
        }
        set;
    }

     public static RecordType newCornerAgentRT{
        get{
            if (newCornerAgentRT == null){ newCornerAgentRT = getProjectRecordType('New_Corner_Agent'); }
            return newCornerAgentRT;
        }
        set;
    }
    
    public static RecordType cornerForecastRT{
         get{
            if (cornerForecastRT == null){ cornerForecastRT = getProjectRecordType('Corner_Forecast'); }
            return cornerForecastRT;
        }
        set;
    }
    
    public static RecordType cornerForecastAgentRT{
         get{
            if (cornerForecastAgentRT == null){ cornerForecastAgentRT = getProjectRecordType('Corner_Forecast_Agent'); }
            return cornerForecastAgentRT;
        }
        set;
    }
    /******** Added by : Promila, Date:11/05/2015, Description: Method to get Record type for Mass Corner Forecast,Mass Corner Forecast(Agent) *****/
    /******starts here *******/
    public static RecordType MassCornerForecastRT{
        get{
            if (MassCornerForecastRT== null){ MassCornerForecastRT= getProjectRecordType('Mass_Corner_Forecast'); }
            return MassCornerForecastRT;
        }
        set;
    }
    public static RecordType MassCornerForecastAgentRT{
        get{
            if (MassCornerForecastAgentRT== null){ MassCornerForecastAgentRT= getProjectRecordType('Mass_Corner_Forecast_Agent'); }
            return MassCornerForecastAgentRT;
        }
        set;
    }
     public static RecordType MassNewCornerInstallDatetRT{
        get{
            if (MassNewCornerInstallDatetRT== null){ MassNewCornerInstallDatetRT= getProjectRecordType('Mass_New_Corner_Installation_Date'); }
            return MassNewCornerInstallDatetRT;
        }
        set;
    }
     public static RecordType draftNewMassCornerRT{
         get{
            if (draftNewMassCornerRT== null){ draftNewMassCornerRT= getProjectRecordType('Draft_New_Mass_Corner'); }
            return draftNewMassCornerRT;
        }
        set;
    }
    /*********end here **********/
    public static RecordType draftNewCornerRT{
         get{
            if (draftNewCornerRT == null){ draftNewCornerRT = getProjectRecordType('Draft_New_Corner'); }
            return draftNewCornerRT;
        }
        set;
    }
}
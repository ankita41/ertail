/*****
*   @Class      :   CornerProjectSharingHandler.cls
*   @Description:   Handles all Project__c Sharing actions/events.
*					THIS CLASS MUST STAY WITHOUT SHARING
*   @Author     :   Thibauld
*   @Created    :   24 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
***/

public without sharing class CornerProjectSharingHandler {
    
    public static void ShareProjects(Map<Id, Project__c> newMap){
		List<Project__Share> newSharing = CreateSharingRecords(newMap);
		if (newSharing.size() > 0){
			insert newSharing;
		}
	}
	
	public static void RecalculateSharingAfterUpdate(Map<Id, Project__c> oldMap, Map<Id, Project__c> newMap){
		Map<Id, Project__c> pToRecalculate = new Map<Id, Project__c>();
		for (Id pId : newMap.keySet()){
			if ((oldMap.get(pId).Architect__c != newMap.get(pId).Architect__c)
				|| (oldMap.get(pId).Supplier__c != newMap.get(pId).Supplier__c))
				pToRecalculate.put(pId, newMap.get(pId));
		}
		
		if (pToRecalculate.size() > 0) {
			DeleteExistingSharing(pToRecalculate.keySet());
			ShareProjects(pToRecalculate);
		}
	}
	
	public static void DeleteExistingSharing(Set<Id> pIdSet){
		// Locate all existing sharing records for the project records in the batch.
        // Only records using an Apex sharing reason for this app should be returned. 
        List<Project__Share> oldpShrs = [SELECT Id FROM Project__Share WHERE ParentId IN 
             :pIdSet AND 
            (RowCause = :Schema.Project__Share.rowCause.IsArchitect__c 
            OR RowCause = :Schema.Project__Share.rowCause.IsSupplier__c)]; 
        
        // Delete the existing sharing records.
       // This allows new sharing records to be written from scratch.
        Delete oldpShrs;
	}
	
	public static List<Project__Share> CreateSharingRecords(Map<Id, Project__c> pMap){
		List<Project__Share> newprojectShrs = new List<Project__Share>();
		// Construct new sharing records  
        for(Project__c p : pMap.values()){
        	if (p.Architect__c != null){
	           newprojectShrs.add(CreateArchitectSharing(p.Architect__c, p.Id));
        	}
        	if (p.Supplier__c != null){
	           newprojectShrs.add(CreateSupplierSharing(p.Supplier__c, p.Id));
        	}
        }
        return newprojectShrs;
	}

	private static Project__Share CreateArchitectSharing(Id userId, Id pfaId){
		return new Project__Share(UserOrGroupId = userId
								, AccessLevel = 'Edit'
								, ParentId = pfaId
								, RowCause = Schema.Project__Share.RowCause.IsArchitect__c);
	}
	
	private static Project__Share CreateSupplierSharing(Id userId, Id pfaId){
		return new Project__Share(UserOrGroupId = userId
								, AccessLevel = 'Edit'
								, ParentId = pfaId
								, RowCause = Schema.Project__Share.RowCause.IsSupplier__c);
	}
  
}
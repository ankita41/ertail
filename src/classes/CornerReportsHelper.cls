/*****

*   @Class      :   CornerReportsHelper.cls
*   @Description:   Helper for the Trade Corner Reports
*   @Author     :   Thibauld
*   @Created    :   04 SEP 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
public without sharing class CornerReportsHelper {
    // The zone codes lowercased
    //private static String ZONECODELWR_NCE = 'nce'; //commented as per req. 127 part 1
    //private static String ZONECODELWR_SWE = 'swe'; //commented as per req. 127 part 1
    private static String ZONECODELWR_AMS = 'ams';
    private static String ZONECODELWR_AOA = 'aoa';
    private static String ZONECODELWR_EUR = 'eur'; //added as per req. 127 part 1
    private static String ZONECODELWR_USA = 'usa';  //added as per req. 127 part 1
    private static String ZONECODELWR_FRA = 'fra';  //added as per req. 127 part 1
    
    // The name of the zones displayed to the user in the reports
    private static String ZONELABEL_EUROPE = 'EUROPE';
    private static String ZONELABEL_SWITZERLAND = 'SWITZERLAND';
    private static String ZONELABEL_GERMANY = 'GERMANY';
    private static String ZONELABEL_FRANCE = 'FRANCE';
    private static String ZONELABEL_AMS = 'LATAM & CANADA'; //Changed from 'LATAM, US & CANADA'; as per req. 127 part 1
    private static String ZONELABEL_AOA = 'AOA';
    private static String ZONELABEL_USA = 'USA';
    private static String ZONELABEL_FR_DE_CH = 'FRANCE, GERMANY, SWITZERLAND';
    
    
    // the name of the markets in Salesforce lower cased
    private static String MARKETLABELLWR_FRANCE = 'france';
    private static String MARKETLABELLWR_SWITZERLAND = 'switzerland';
    private static String MARKETLABELLWR_GERMANY = 'germany';
    
    // New Request workflow Types:
    public static Set<String> NEWREQUEST_WORKFLOWTYPE = new Set<String>{'Sales Promoter', 'Agent'};
    
    public static Set<String> STATUS_CANCELLED = new Set<String>{'Cancelled'};
    
    // convert the Market name to the zone name if this market is considered as a zone
    private static Map<String, String> MarketsConsideredAsZones = new Map<String, String>{ //MARKETLABELLWR_FRANCE => ZONELABEL_FRANCE
                                                                             MARKETLABELLWR_SWITZERLAND => ZONELABEL_SWITZERLAND
                                                                            , MARKETLABELLWR_GERMANY => ZONELABEL_GERMANY
                                                                        };
                                                                        
    // convert the Market name to the zone name if this market is considered as a zone
    private static Map<String, String> MarketsConsideredAsZones_GROUPED = new Map<String, String>{ //MARKETLABELLWR_FRANCE => ZONELABEL_FR_DE_CH
                                                                             MARKETLABELLWR_SWITZERLAND => ZONELABEL_FR_DE_CH
                                                                            , MARKETLABELLWR_GERMANY => ZONELABEL_FR_DE_CH
                                                                        };
    
    // Convert the zone code to the Zone label
    private static Map<String, String> ZonesLabels = new Map<String, String>{ //ZONECODELWR_NCE => ZONELABEL_EUROPE
                                                                //, ZONECODELWR_SWE => ZONELABEL_EUROPE
                                                                 ZONECODELWR_AMS => ZONELABEL_AMS
                                                                , ZONECODELWR_AOA => ZONELABEL_AOA
                                                                , ZONECODELWR_EUR => ZONELABEL_EUROPE
                                                                , ZONECODELWR_USA => ZONELABEL_USA
                                                                , ZONECODELWR_FRA => ZONELABEL_FRANCE
                                                            };
    
    public static List<String> OrderedZonesNames = new List<String>{ZONELABEL_USA, ZONELABEL_EUROPE, ZONELABEL_FRANCE, ZONELABEL_SWITZERLAND, ZONELABEL_GERMANY, ZONELABEL_AMS, ZONELABEL_AOA}; //Added 'USA' as per req. 127 part 1
    public static Set<String> ZonesNamesSet = new Set<String>(OrderedZonesNames);
    
    // Used to group France, Germany and Switzerland in one Zone
    public static List<String> OrderedZonesNames_Grouped = new List<String>{ZONELABEL_USA, ZONELABEL_EUROPE, ZONELABEL_FRANCE, ZONELABEL_FR_DE_CH, ZONELABEL_AMS, ZONELABEL_AOA};
    public static Set<String> ZonesNames_GroupedSet = new Set<String>(OrderedZonesNames_Grouped);

    // Return the Zone label.  Firts test if the market is considered as a zone, if not, test if a zone lavel exists, if not return the zonecode
    public static string GetZoneLabel(String marketName, String zoneCode){
        String marketNameLwr = marketName.toLowerCase();
        if (MarketsConsideredAsZones.containskey(marketNameLwr))
            return MarketsConsideredAsZones.get(marketNameLwr);
        String zoneCodeLwr = (zoneCode != null ? zoneCode.toLowerCase() : null);
        if (ZonesLabels.containsKey(zoneCodeLwr))
            return ZonesLabels.get(zoneCodeLwr);
        return zoneCode;
    }
    
    // Return the Grouped Zone label.  Firts test if the market is considered as a zone, if not, test if a zone lavel exists, if not return the zonecode
    public static string GetZoneLabel_Grouped(String marketName, String zoneCode){
        String marketNameLwr = marketName.toLowerCase();
        if (MarketsConsideredAsZones.containskey(marketNameLwr))
            return MarketsConsideredAsZones_Grouped.get(marketNameLwr);
        String zoneCodeLwr = (zoneCode != null ? zoneCode.toLowerCase() : null);
        if (ZonesLabels.containsKey(zoneCodeLwr))
            return ZonesLabels.get(zoneCodeLwr);
        return zoneCode;
    }
}
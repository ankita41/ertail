/*****
*   @Class      :   CornerSharingHandler.cls
*   @Description:   Handles all Corner__c Sharing actions/events.
*					THIS CLASS MUST STAY WITHOUT SHARING
*   @Author     :   Thibauld
*   @Created    :   30 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
***/

public without sharing class CornerSharingHandler {
    
    public static void ShareCorners(Map<Id, Corner__c> newMap){
		List<Corner__Share> newSharing = CreateSharingRecords(newMap);
		if (newSharing.size() > 0){
			insert newSharing;
		}
	}
	
	public static void RecalculateSharingAfterUpdate(Map<Id, Corner__c> oldMap, Map<Id, Corner__c> newMap){
		Map<Id, Corner__c> cToRecalculate = new Map<Id, Corner__c>();
		for (Id cId : newMap.keySet()){
			if ((oldMap.get(cId).Architect__c != newMap.get(cId).Architect__c)
				|| (oldMap.get(cId).Supplier__c != newMap.get(cId).Supplier__c))
				cToRecalculate.put(cId, newMap.get(cId));
		}
		
		if (cToRecalculate.size() > 0) {
			DeleteExistingSharing(cToRecalculate.keySet());
			ShareCorners(cToRecalculate);
		}
	}
	
	public static void DeleteExistingSharing(Set<Id> cIdSet){
		// Locate all existing sharing records for the project records in the batch.
        // Only records using an Apex sharing reason for this app should be returned. 
        List<Corner__Share> oldpShrs = [SELECT Id FROM Corner__Share WHERE ParentId IN 
             :cIdSet AND 
            (RowCause = :Schema.Corner__Share.rowCause.IsArchitect__c 
            OR RowCause = :Schema.Corner__Share.rowCause.IsSupplier__c)]; 
        
        // Delete the existing sharing records.
       // This allows new sharing records to be written from scratch.
        Delete oldpShrs;
	}
	
	public static List<Corner__Share> CreateSharingRecords(Map<Id, Corner__c> cMap){
		List<Corner__Share> newCornerShrs = new List<Corner__Share>();
		// Construct new sharing records  
        for(Corner__c p : cMap.values()){
        	if (p.Architect__c != null){
	           newCornerShrs.add(CreateArchitectSharing(p.Architect__c, p.Id));
        	}
        	if (p.Supplier__c != null){
	           newCornerShrs.add(CreateSupplierSharing(p.Supplier__c, p.Id));
        	}
        }
        return newCornerShrs;
	}

	private static Corner__Share CreateArchitectSharing(Id userId, Id pfaId){
		return new Corner__Share(UserOrGroupId = userId
								, AccessLevel = 'Edit'
								, ParentId = pfaId
								, RowCause = Schema.Corner__Share.RowCause.IsArchitect__c);
	}
	
	private static Corner__Share CreateSupplierSharing(Id userId, Id pfaId){
		return new Corner__Share(UserOrGroupId = userId
								, AccessLevel = 'Edit'
								, ParentId = pfaId
								, RowCause = Schema.Corner__Share.RowCause.IsSupplier__c);
	}
  
}
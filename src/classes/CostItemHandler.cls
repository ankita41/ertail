/*****
*   @Class      :   CostItemHandler.cls
*   @Description:   Handles all Cost_Item__c recurring actions/events.
*   @Author     :   Jacky Uy
*   @Created    :   14 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        15 APR 2014     Add: method to create roll-up summaries on all Cost Items to the Budget Overview object
*   Jacky Uy        17 APR 2014     Fix: Changed the CurrencyConverter constructor to pass a Set of Local Currencies
*   Jacky Uy        18 APR 2014     Fix on RollUpExpectedCosts(): Changed field reference from Expected_amount__c to Expected_amount_local__c
*   Jacky Uy        18 APR 2014     Fix on RollUpConfirmedCostItems(): Changed field reference from Validated_amount__c to Validated_amount_local__c
*   Jacky Uy        22 APR 2014     Fix: Passed the record's creation date as a parameter when converting currencies
*   Jacky Uy        02 MAY 2014     Fix: Changed back inclusion of Confirmed Record Type on Costs when rolling up items
*   Jacky Uy        02 MAY 2014     Fix: Roll-up Calculation
*   Jacky Uy        09 MAY 2014     Change: Referenced a static method for Currency Conversion
*   Jacky Uy        09 MAY 2014     Fix on RollUpExpectedCosts(): Include all Record Types NOT EQUAL to 'Confirmed'
*   Jacky Uy        30 JUN 2014     Improved Currency Conversion method
*   Jacky Uy        02 JUL 2014     Created: ValidateCurrencyConversion() method
*   Koteswar        19 March 2015   Change : convertCostItemCurrency () menthod to handle current year currency and project year date change
*   koteswar        11 Aug 2015     Caange: In 'RollUpExpectedCosts()' ,insted of considering the expected cost now we are considering the final amount value as same as confirmed cost
***/

public with sharing class CostItemHandler{
    //Prevent creation of new cost items on closed projects
    public static void StopInsertOfCostItems(List<Cost_Item__c> newList){
        map<Id, RECO_Project__c> mapProjectsPerId = new map<Id, RECO_Project__c>([SELECT Id, Status__c FROM RECO_Project__c]);
        for(Cost_Item__c ci : newList){
            if (mapProjectsPerId.get(ci.RECO_Project__c).Status__c == 'Completed')
                ci.RECO_Project__c.addError('Cost items cannot be created for completed projects.');
        }
    }
    
    public static void SetExtraCostOnCostItems(List<Cost_Item__c> newList){
        Set<Id> setProjectIds = new Set<Id>();
        for(Cost_Item__c ci : newList){
            setProjectIds.add(ci.RECO_Project__c);
        }
  
        Map<Id, RECO_Project__c> mapProjectsPerId = new Map<Id, RECO_Project__c>([SELECT Id, Freeze_Cost_Items__c FROM RECO_Project__c WHERE Id IN :setProjectIds]);
        String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name; 
        for(Cost_Item__c ci : newList){
            if ((currentUserProfile!=UserConstants.BTQ_RPM && currentUserProfile!=UserConstants.SYS_ADM && currentUserProfile!=UserConstants.NCAFE_RPM ) || ci.Extra_cost__c == null)
                ci.Extra_cost__c = mapProjectsPerId.get(ci.RECO_Project__c).Freeze_Cost_Items__c ? 'Yes' : 'No';
        }
    }
    // ticket#RM0026301599 : Added below map to fetch the NCAFE project record type - Start
    public static Map<Id,Map<Id,String>> fetchProjectRecordType(List<Cost_Item__c> newList){
        //below nested map is being used to fetch the cost item ids for before insert event
        Map<Id,Map<Id,String>> mapCostnProjectRecType = new Map<Id,Map<Id,String>>() ;
        
        Map<Id,String> mapProjectRecTypeDetail = new Map<Id,String>() ;
        set<Id> setProjetId = new set<Id>() ;
        //RECO_Project__c,Private__c ,RECO_Project__r.RecordtypeId,RECO_Project__r.Recordtype.name
        for(Cost_Item__c obj: newList)
            setProjetId.add(obj.RECO_Project__c);
        
        for(RECO_Project__c projObj : [SELECT Id,Name,RecordtypeId,Recordtype.name FROM RECO_Project__c WHERE ID IN:setProjetId])
            mapProjectRecTypeDetail.put(projObj.Id,projObj.RecordType.Name.touppercase());
        
        for(Cost_Item__c obj: newList){
            
            if(!mapCostnProjectRecType.containsKey(obj.id))
                mapCostnProjectRecType.put(obj.Id,new Map<Id,String>() ) ;
            mapCostnProjectRecType.get(obj.id).put(obj.reco_project__c,mapProjectRecTypeDetail.get(obj.reco_project__c)) ;
            
        }
        return mapCostnProjectRecType ;
    }
    // ticket#RM0026301599 : Added below map to fetch the NCAFE project record type - End
    public static void convertCostItemCurrency(List<Cost_Item__c> newList, Map<Id,Cost_Item__c> oldMap){
        
        Map<Id,Map<Id,String>> mapCostnProjectRecType = fetchProjectRecordType(newList) ;
        
        Set<String> createdYears = new Set<String>();
        Set<String> itemCurrencies = new Set<String>();
        Datetime currencyAppliedDate;
        String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name; 
        //Map<String,offer__C> offersmap= new Map<String,offer__C>();
        Set<String> offersisList=new Set<String>();  
        for(Cost_Item__c obj: newList) {
           if(obj.Offer__c != null )     
            offersisList.add(obj.Offer__c);
        }
        map<id,Offer__c> offersmap= New map<id,Offer__c>([Select ID,Dis_Approval_date__c from Offer__c Where ID IN :offersisList]);       
        //String recordtypeId= (currentUserProfile.contains('NCAFE')?(Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId()):(Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId()));
        string costItemRecordTypeId = '' ;
                   
        for(Cost_Item__c obj: newList) {
           // creationDate = (obj.CreatedDate!=null) ? obj.CreatedDate : SYSTEM.Today();
           /* code modifcation done for currency upload --start--Koteswarar*/
           //first condtion is check for OpeningFormula for the created date
           // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (Added below if condition)
           if(mapCostnProjectRecType.containsKey(obj.Id) && mapCostnProjectRecType.get(obj.Id) <> null)
               costItemRecordTypeId = ( mapCostnProjectRecType.get(obj.Id).get(obj.Reco_Project__c).contains('NCAFE') ? (Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId()):(Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId()));
           
            currencyAppliedDate= (obj.RECO_Project__r.OpeningFormula__c!=null) ? obj.RECO_Project__r.OpeningFormula__c : SYSTEM.Today();
            //same record is confirmed then date is approval date from offer 
            //system.debug('obj.RecordType.name'+obj.RecordType.name+'obj.Offer__r.Dis_Approval_date__c @'+obj.Offer__r.Dis_Approval_date__c );
            if(costItemRecordTypeId != null && costItemRecordTypeId !=''&& obj.RecordTypeid==costItemRecordTypeId && obj.offer__C != null 
                && offersmap != null && !offersmap.isEmpty() && offersmap.containskey(obj.Offer__c)){
                currencyAppliedDate=offersmap.get(obj.Offer__c).Dis_Approval_date__c ;
            }
            /* code modifcation done for currency upload --End--Koteswarar*/
            if(currencyAppliedDate != null)
            createdYears.add(String.valueOf(currencyAppliedDate.year()));
            itemCurrencies.add(obj.Currency__c);
        }
        //added before all the populate currency if created date currency is not avaiilble look for current year
        createdYears.add(String.valueOf(System.today().year()));
        CurrencyConverter.PopulateCurrencyConversion(createdYears, itemCurrencies);
        for(Cost_Item__c obj : newList){
            /* code modifcation done for currency upload --start-- Koteswarar*/
            // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (Added below if condition)
            if(mapCostnProjectRecType.containsKey(obj.Id) && mapCostnProjectRecType.get(obj.Id) <> null)
               costItemRecordTypeId = ( mapCostnProjectRecType.get(obj.Id).get(obj.Reco_Project__c).contains('NCAFE') ? (Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId()):(Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId()));
            currencyAppliedDate= (obj.RECO_Project__r.OpeningFormula__c!=null) ? obj.RECO_Project__r.OpeningFormula__c: SYSTEM.Today();
            
            if(costItemRecordTypeId != null && costItemRecordTypeId !=''&& obj.RecordTypeid==costItemRecordTypeId && obj.offer__C != null 
                && offersmap != null && !offersmap.isEmpty() && offersmap.containskey(obj.Offer__c)){
                currencyAppliedDate=offersmap.get(obj.Offer__c).Dis_Approval_date__c ;
            }
            /* code modifcation done for currency upload --end--Koteswarar*/            
            if(!obj.Do_Not_Convert__c && obj.Expected_amount__c!=null){
                //INITIALIZED TO 0 TO MAKE SURE THAT CURRENCY CONVERSION IS GOING TO WORK
                obj.Expected_amount_local__c = 0;
                obj.Expected_amount_CHF__c = 0;
                obj.Expected_amount_EUR__c = 0;
                system.debug('currencyAppliedDate@@2'+currencyAppliedDate);         
                obj.Expected_amount_local__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Expected_amount__c), obj.Currency__c, obj.Local_Currency__c, currencyAppliedDate);
                obj.Expected_amount_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Expected_amount__c), obj.Currency__c, 'CHF', currencyAppliedDate);
                obj.Expected_amount_EUR__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Expected_amount__c), obj.Currency__c, 'EUR', currencyAppliedDate);
                
                if(obj.Validated_amount__c==null || (oldMap!=null && obj.Expected_amount__c!=oldMap.get(obj.Id).Expected_amount__c && (obj.RecordTypeId=='012b0000000PnPA' || obj.RecordTypeId=='012b0000000Doc2')))
                    obj.Validated_amount__c = obj.Expected_amount__c;
            
                //INITIALIZED TO 0 TO MAKE SURE THAT CURRENCY CONVERSION IS GOING TO WORK
                obj.Validated_amount_local__c = 0;
                obj.Validated_amount_CHF__c = 0;
                obj.Validated_amount_EUR__c = 0;
                
                obj.Validated_amount_local__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Validated_amount__c), obj.Currency__c, obj.Local_Currency__c, currencyAppliedDate);
                obj.Validated_amount_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Validated_amount__c), obj.Currency__c, 'CHF', currencyAppliedDate);
                obj.Validated_amount_EUR__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Validated_amount__c), obj.Currency__c, 'EUR', currencyAppliedDate);
            }
        }
    }
    
    //Prevent deletion of cost items after their submission for approval / approval
    /*public static void StopDeletionOfCostItems(List<Cost_Item__c> oldList){
        String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        String hasDeleteRights = 'NES RECO Retail Project Manager, System Admin';
        
        Id confirmedTypeId = (currentUserProfile.contains('NCAFE')?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId()));
        Id pendingTypeId = (currentUserProfile.contains('NCAFE')?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Pending approval').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Pending approval').getRecordTypeId()));
        for (Cost_Item__c ci : oldList) {
            if ((ci.RecordTypeId == confirmedTypeId || ci.RecordTypeId == pendingTypeId) && !hasDeleteRights.contains(currentUserProfile))
                ci.Name.addError('You are not authorized to delete cost items after their approval or submission to approval.');
        }
    }*/
    // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType 
    public static void StopDeletionOfCostItems(List<Cost_Item__c> oldList){
    
        Map<Id,Map<Id,String>> mapCostnProjectRecType = fetchProjectRecordType(oldList) ;
        String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        String hasDeleteRights = 'NES RECO Retail Project Manager, NES NCAFE Retail Project Manager, System Admin';
        //Id confirmedTypeId = (currentUserProfile.contains('NCAFE')?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId()));
        //Id pendingTypeId = (currentUserProfile.contains('NCAFE')?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Pending approval').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Pending approval').getRecordTypeId()));
        for (Cost_Item__c ci : oldList) {
            if(mapCostnProjectRecType.containsKey(ci.Id) && mapCostnProjectRecType.get(ci.Id) <> null){
                String costItemConfRecordTypeId = ( mapCostnProjectRecType.get(ci.Id).get(ci.Reco_Project__c).contains('NCAFE') ? (Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId()):(Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId()));
                String costItemPendingRecordTypeId = ( mapCostnProjectRecType.get(ci.Id).get(ci.Reco_Project__c).contains('NCAFE') ? (Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Pending approval').getRecordTypeId()):(Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('Pending approval').getRecordTypeId()));
           
            if ((ci.RecordTypeId == costItemConfRecordTypeId || ci.RecordTypeId == costItemPendingRecordTypeId) && !hasDeleteRights.contains(currentUserProfile))
                ci.Name.addError('You are not authorized to delete cost items after their approval or submission to approval.');
        }
        }
    }
        
    //PERFORMS ROLL UPS ON ALL COST ITEMS EXCEPT CONFIRMED COSTS
    //AND STORES THE SUMMARY TO THE PARENT BOUTIQUE PROJECT RECORD
    public static void RollUpExpectedCosts(List<Cost_Item__c> newList){
        String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        Set<Id> parentIds = new Set<Id>();
        for(Cost_Item__c ci : newList){
            parentIds.add(ci.RECO_Project__c);
        }
        
        //Id rTypeId = (currentUserProfile.contains('NCAFE')?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId()));
        Map<Id,Map<String,Double>> mapLocalCosts = new Map<Id,Map<String,Double>>();
        Map<Id,Map<String,Double>> mapCHFCosts = new Map<Id,Map<String,Double>>();
        
        Map<Id,Map<String,Double>> mapLocalCostsPrivate = new Map<Id,Map<String,Double>>();
        Map<Id,Map<String,Double>> mapCHFCostsPrivate = new Map<Id,Map<String,Double>>();
        
        for(Cost_Item__c ci : [SELECT RECO_Project__c,Private__c, RECO_Project__r.RecordtypeId,RECO_Project__r.Recordtype.name,Expected_amount_local__c,Validated_amount_local__c,Validated_amount_CHF__c, Expected_amount_CHF__c, Level_1__c,Recordtype.Name,RecordtypeId FROM Cost_Item__c 
                                WHERE RECO_Project__c IN :parentIds AND (NOT RecordType.name Like '%Confirmed%') AND Expected_amount_local__c!=NULL]){
            // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (Added below if condition)
            if((ci.RECO_Project__r.Recordtype.name.Contains('NCafe') && ci.Recordtype.Name.Contains('NCAFE')) || (!(ci.RECO_Project__r.Recordtype.name.Contains('NCafe') || ci.Recordtype.Name.Contains('NCAFE')))){
            if((ci.private__c==true || ci.private__c==false)){
              //CALCULATES THE SUM OF ALL EXPECTED COSTS IN THE LOCAL CURRENCY
              if(mapLocalCosts.containsKey(ci.RECO_Project__c) && mapLocalCosts.get(ci.RECO_Project__c).containsKey(ci.Level_1__c)){
                  mapLocalCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, mapLocalCosts.get(ci.RECO_Project__c).get(ci.Level_1__c) + ci.Validated_amount_local__c);//Expected_amount_local__c
                  mapLocalCosts.get(ci.RECO_Project__c).put('Total', mapLocalCosts.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_local__c);//Expected_amount_local__c
              }
              else if(mapLocalCosts.containsKey(ci.RECO_Project__c)){
                  mapLocalCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_local__c);//Expected_amount_local__c
                  mapLocalCosts.get(ci.RECO_Project__c).put('Total', mapLocalCosts.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_local__c);//Expected_amount_local__c
              }
              else{
                  mapLocalCosts.put(ci.RECO_Project__c, new Map<String,Double>());
                  mapLocalCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_local__c);//Expected_amount_local__c
                  mapLocalCosts.get(ci.RECO_Project__c).put('Total', ci.Validated_amount_local__c);//Expected_amount_local__c
              }
              
              //CALCULATES THE SUM OF ALL EXPECTED COSTS IN CHF
              if(mapCHFCosts.containsKey(ci.RECO_Project__c) && mapCHFCosts.get(ci.RECO_Project__c).containsKey(ci.Level_1__c)){
                  mapCHFCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, mapCHFCosts.get(ci.RECO_Project__c).get(ci.Level_1__c) + ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
                  mapCHFCosts.get(ci.RECO_Project__c).put('Total', mapCHFCosts.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
              }
              else if(mapCHFCosts.containsKey(ci.RECO_Project__c)){
                  mapCHFCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_CHF__c); //Expected_amount_CHF__c
                  mapCHFCosts.get(ci.RECO_Project__c).put('Total', mapCHFCosts.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
              }
              else{
                  mapCHFCosts.put(ci.RECO_Project__c, new Map<String,Double>());
                  mapCHFCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
                  mapCHFCosts.get(ci.RECO_Project__c).put('Total', ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
              }//
            }
            
           if(ci.private__c!=true){
                //CALCULATES THE SUM OF ALL EXPECTED COSTS IN THE LOCAL CURRENCY
              if(mapLocalCostsPrivate.containsKey(ci.RECO_Project__c) && mapLocalCostsPrivate.get(ci.RECO_Project__c).containsKey(ci.Level_1__c)){
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, mapLocalCostsPrivate.get(ci.RECO_Project__c).get(ci.Level_1__c) + ci.Validated_amount_local__c);//Expected_amount_local__c
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put('Total', mapLocalCostsPrivate.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_local__c);//Expected_amount_local__c
              }
              else if(mapLocalCostsPrivate.containsKey(ci.RECO_Project__c)){
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_local__c);//Expected_amount_local__c
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put('Total', mapLocalCostsPrivate.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_local__c);//Expected_amount_local__c
              }
              else{
                  mapLocalCostsPrivate.put(ci.RECO_Project__c, new Map<String,Double>());
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_local__c);//Expected_amount_local__c
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put('Total', ci.Validated_amount_local__c);//Expected_amount_local__c
              }
              
              //CALCULATES THE SUM OF ALL EXPECTED COSTS IN CHF
              if(mapCHFCostsPrivate.containsKey(ci.RECO_Project__c) && mapCHFCostsPrivate.get(ci.RECO_Project__c).containsKey(ci.Level_1__c)){
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, mapCHFCostsPrivate.get(ci.RECO_Project__c).get(ci.Level_1__c) + ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put('Total', mapCHFCostsPrivate.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
              }
              else if(mapCHFCostsPrivate.containsKey(ci.RECO_Project__c)){
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_CHF__c); //Expected_amount_CHF__c
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put('Total', mapCHFCostsPrivate.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
              }
              else{
                  mapCHFCostsPrivate.put(ci.RECO_Project__c, new Map<String,Double>());
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put('Total', ci.Validated_amount_CHF__c);//Expected_amount_CHF__c
              }//
           }
           }
        }
        
        List<Budget_Overview__c> upsertOverviews = new List<Budget_Overview__c>();
        for(Budget_Overview__c bo : [SELECT Boutique_Project__c, Budget_Item__c, Expected_Cost_local__c, Expected_Cost_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c IN :parentIds]){
            bo.Expected_Cost_local__c = 0;
            if(mapLocalCosts.containsKey(bo.Boutique_Project__c) && mapLocalCosts.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c))
                bo.Expected_Cost_local__c = mapLocalCosts.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
                
            bo.Expected_Cost_CHF__c = 0;
            if(mapCHFCosts.containsKey(bo.Boutique_Project__c) && mapCHFCosts.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c))
                bo.Expected_Cost_CHF__c = mapCHFCosts.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
            
            upsertOverviews.add(bo);
        }
        update upsertOverviews;
        
        
        
        
         List<Budget_Overview__c> upsertOverviewsPrivate = new List<Budget_Overview__c>();
        for(Budget_Overview__c bo : [SELECT Expected_Cost_local_Private__c,Boutique_Project__c, Budget_Item__c, Expected_Cost_local__c, Expected_Cost_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c IN :parentIds]){
            bo.Expected_Cost_local_Private__c = 0;
            if(mapLocalCostsPrivate.containsKey(bo.Boutique_Project__c) && mapLocalCostsPrivate.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c))
                bo.Expected_Cost_local_Private__c = mapLocalCostsPrivate.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
                
            bo.Expected_Cost_CHF_Private__c = 0;
            if(mapCHFCostsPrivate.containsKey(bo.Boutique_Project__c) && mapCHFCostsPrivate.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c))
                bo.Expected_Cost_CHF_Private__c = mapCHFCostsPrivate.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
            
            upsertOverviewsPrivate.add(bo);
        }
        update upsertOverviewsPrivate;
    }
    
    //PERFORMS ROLL UPS ON ALL CONFIRMED COST ITEMS
    //AND STORES THE SUMMARY TO THE BUDGET OVERVIEW OBJECT OF THE SAME PARENT
    public static void RollUpConfirmedCostItems(List<Cost_Item__c> newList){
        String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        Set<Id> parentIds = new Set<Id>();
        for(Cost_Item__c ci : newList){
            parentIds.add(ci.RECO_Project__c);
        }
        
       // Id rTypeId = (currentUserProfile.contains('NCAFE')?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId()));
        Map<Id,Map<String,Double>> mapLocalCosts = new Map<Id,Map<String,Double>>();
        Map<Id,Map<String,Double>> mapCHFCosts = new Map<Id,Map<String,Double>>();
        
        Map<Id,Map<String,Double>> mapLocalCostsPrivate = new Map<Id,Map<String,Double>>();
        Map<Id,Map<String,Double>> mapCHFCostsPrivate = new Map<Id,Map<String,Double>>();
        
        for(Cost_Item__c ci : [SELECT RECO_Project__c,Private__c ,RECO_Project__r.RecordtypeId,RECO_Project__r.Recordtype.name ,Validated_amount_local__c, Validated_amount_CHF__c, Level_1__c,Recordtype.Name,RecordtypeId FROM Cost_Item__c WHERE RECO_Project__c IN :parentIds AND RecordType.Name Like '%Confirmed%' AND Validated_amount_local__c!=NULL]){
            // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (Added below if condition)
            if((ci.RECO_Project__r.Recordtype.name.Contains('NCafe') && ci.Recordtype.Name.Contains('NCAFE')) || (!(ci.RECO_Project__r.Recordtype.name.Contains('NCafe') || ci.Recordtype.Name.Contains('NCAFE')))) {                     
            if((ci.private__c==true || ci.private__c==false)){                      
                                            //CALCULATES THE SUM OF ALL COSTS IN THE LOCAL CURRENCY
              if(mapLocalCosts.containsKey(ci.RECO_Project__c) && mapLocalCosts.get(ci.RECO_Project__c).containsKey(ci.Level_1__c)){
                  mapLocalCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, mapLocalCosts.get(ci.RECO_Project__c).get(ci.Level_1__c) + ci.Validated_amount_local__c);
                  mapLocalCosts.get(ci.RECO_Project__c).put('Total', mapLocalCosts.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_local__c);
              }
              else if(mapLocalCosts.containsKey(ci.RECO_Project__c)){
                  mapLocalCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_local__c);
                  mapLocalCosts.get(ci.RECO_Project__c).put('Total', mapLocalCosts.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_local__c);
              }
              else{
                  mapLocalCosts.put(ci.RECO_Project__c, new Map<String,Double>());
                  mapLocalCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_local__c);
                  mapLocalCosts.get(ci.RECO_Project__c).put('Total', ci.Validated_amount_local__c);
              }
              
              //CALCULATES THE SUM OF ALL COSTS IN CHF
              if(mapCHFCosts.containsKey(ci.RECO_Project__c) && mapCHFCosts.get(ci.RECO_Project__c).containsKey(ci.Level_1__c)){
                  mapCHFCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, mapCHFCosts.get(ci.RECO_Project__c).get(ci.Level_1__c) + ci.Validated_amount_CHF__c);
                  mapCHFCosts.get(ci.RECO_Project__c).put('Total', mapCHFCosts.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_CHF__c);
              }
              else if(mapCHFCosts.containsKey(ci.RECO_Project__c)){
                  mapCHFCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_CHF__c);
                  mapCHFCosts.get(ci.RECO_Project__c).put('Total', mapCHFCosts.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_CHF__c);
              }
              else{
                  mapCHFCosts.put(ci.RECO_Project__c, new Map<String,Double>());
                  mapCHFCosts.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_CHF__c);
                  mapCHFCosts.get(ci.RECO_Project__c).put('Total', ci.Validated_amount_CHF__c);
              }
            }
             if(ci.private__c!=true){                     
                                            //CALCULATES THE SUM OF ALL COSTS IN THE LOCAL CURRENCY
              if(mapLocalCostsPrivate.containsKey(ci.RECO_Project__c) && mapLocalCostsPrivate.get(ci.RECO_Project__c).containsKey(ci.Level_1__c)){
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, mapLocalCostsPrivate.get(ci.RECO_Project__c).get(ci.Level_1__c) + ci.Validated_amount_local__c);
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put('Total', mapLocalCostsPrivate.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_local__c);
              }
              else if(mapLocalCostsPrivate.containsKey(ci.RECO_Project__c)){
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_local__c);
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put('Total', mapLocalCostsPrivate.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_local__c);
              }
              else{
                  mapLocalCostsPrivate.put(ci.RECO_Project__c, new Map<String,Double>());
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_local__c);
                  mapLocalCostsPrivate.get(ci.RECO_Project__c).put('Total', ci.Validated_amount_local__c);
              }
              
              //CALCULATES THE SUM OF ALL COSTS IN CHF
              if(mapCHFCostsPrivate.containsKey(ci.RECO_Project__c) && mapCHFCostsPrivate.get(ci.RECO_Project__c).containsKey(ci.Level_1__c)){
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, mapCHFCostsPrivate.get(ci.RECO_Project__c).get(ci.Level_1__c) + ci.Validated_amount_CHF__c);
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put('Total', mapCHFCostsPrivate.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_CHF__c);
              }
              else if(mapCHFCostsPrivate.containsKey(ci.RECO_Project__c)){
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_CHF__c);
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put('Total', mapCHFCostsPrivate.get(ci.RECO_Project__c).get('Total') + ci.Validated_amount_CHF__c);
              }
              else{
                  mapCHFCostsPrivate.put(ci.RECO_Project__c, new Map<String,Double>());
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put(ci.Level_1__c, ci.Validated_amount_CHF__c);
                  mapCHFCostsPrivate.get(ci.RECO_Project__c).put('Total', ci.Validated_amount_CHF__c);
              }
            }
            
            }
        }                        
                
        List<Budget_Overview__c> upsertOverviews = new List<Budget_Overview__c>();
        for(Budget_Overview__c bo : [SELECT Boutique_Project__c, Budget_Item__c, Confirmed_Cost_local__c, Confirmed_Cost_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c IN :parentIds]){
            bo.Confirmed_Cost_local__c = 0;
            if(mapLocalCosts.containsKey(bo.Boutique_Project__c) && mapLocalCosts.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c))
                bo.Confirmed_Cost_local__c = mapLocalCosts.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
            
                
            bo.Confirmed_Cost_CHF__c = 0;
            if(mapCHFCosts.containsKey(bo.Boutique_Project__c) && mapCHFCosts.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c))
                bo.Confirmed_Cost_CHF__c = mapCHFCosts.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
            
            
            upsertOverviews.add(bo);
        }
        update upsertOverviews;
        
        
        
        
         List<Budget_Overview__c> upsertOverviewsPrivate = new List<Budget_Overview__c>();
        for(Budget_Overview__c bo : [SELECT Boutique_Project__c,Confirmed_Cost_local_Private__c,Confirmed_Cost_CHF_Private__c, Budget_Item__c, Confirmed_Cost_local__c, Confirmed_Cost_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c IN :parentIds]){
            bo.Confirmed_Cost_local_Private__c = 0;
            if(mapLocalCostsPrivate.containsKey(bo.Boutique_Project__c) && mapLocalCostsPrivate.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c))
                bo.Confirmed_Cost_local_Private__c = mapLocalCostsPrivate.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
            
                
            bo.Confirmed_Cost_CHF_Private__c = 0;
            if(mapCHFCostsPrivate.containsKey(bo.Boutique_Project__c) && mapCHFCostsPrivate.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c))
                bo.Confirmed_Cost_CHF_Private__c = mapCHFCostsPrivate.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
            
            
            upsertOverviewsPrivate.add(bo);
        }
        update upsertOverviewsPrivate;
        
    }
    
    public static void ValidateCurrencyConversion(List<Cost_Item__c> newList){
        String objIds = '';
        for(Cost_Item__c obj : newList){
            if(obj.Expected_amount__c!=null && 
                (obj.Expected_amount_local__c==null || obj.Expected_amount_EUR__c==null || obj.Expected_amount_CHF__c==null || obj.Expected_amount_local__c==0 || obj.Expected_amount_EUR__c==0 || obj.Expected_amount_CHF__c==0 ||
                obj.Validated_amount_local__c==null || obj.Validated_amount_EUR__c==null || obj.Validated_amount_CHF__c==null || obj.Validated_amount_local__c==0 || obj.Validated_amount_EUR__c==0 || obj.Validated_amount_CHF__c==0)){
                
                objIds = objIds + '\n' + obj.Id + ': ';
                if(obj.Expected_amount_local__c==null || obj.Expected_amount_local__c==0 || obj.Validated_amount_local__c==null || obj.Validated_amount_local__c==0)
                    objIds = objIds + obj.Currency__c + '>' + obj.Local_Currency__c + ', ';
                if(obj.Expected_amount_EUR__c==null || obj.Expected_amount_EUR__c==0 || obj.Validated_amount_EUR__c==null || obj.Validated_amount_EUR__c==0)
                    objIds = objIds + obj.Currency__c + '>EUR' + ', ';
                if(obj.Expected_amount_CHF__c==null || obj.Expected_amount_CHF__c==0 || obj.Validated_amount_CHF__c==null || obj.Validated_amount_CHF__c==0)
                    objIds = objIds + obj.Currency__c + '>CHF' + ', ';
            }
        }
        
        if(objIds!=''){
            Task t = new Task();
            t.OwnerId = '005b0000001IuX8';
            t.Subject = 'Check Currency Conversion for Cost Items';
            t.Status = 'Not Started';
            t.Priority = 'High';
            t.Description = objIds;
            t.ActivityDate = SYSTEM.today();
            
            Database.DMLOptions dmlOpt = new Database.DMLOptions(); 
            dmlOpt.EmailHeader.TriggerUserEmail = true; 
            Database.Insert(t,dmlOpt);
        }
    }
}
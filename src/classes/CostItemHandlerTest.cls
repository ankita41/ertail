/*****
*   @Class      :   CostItemHandlerTest.cls
*   @Description:   Test coverage for the CostItemTriggers.trigger/CostItemHandler.cls
*   @Author     :   Jacky Uy
*   @Created    :   22 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        25 APR 2014     CostItemTriggers.trigger Test Coverage at 100%
*   Jacky Uy        25 APR 2014     CostItemHandler.cls Test Coverage at 93%
*   Jacky Uy        28 APR 2014     CurrencyConverter.cls Test Coverage at 93%
*   Jacky Uy        13 MAY 2014     Change: BTQ Project Record Type from 'New' to 'Boutique Project'
*   Jacky Uy        02 JUL 2014     CostItemHandler.cls Test Coverage at 94%
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*****/

@isTest
private with sharing class CostItemHandlerTest{
    static testMethod void unitTest1(){
        List<Cost_Item__c> updCostItems;
        
        SYSTEM.runAs(TestHelper.CurrentUser){
            TestHelper.createCurrencies();
            Market__c market = TestHelper.createMarket();
            insert market;
            
            Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
            Account acc = TestHelper.createAccount(rTypeId, market.Id);
            insert acc;
        
            rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
            Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
            insert btq;
            
            rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
            RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
            bProj.Currency__c = 'CHF';
            insert bProj;
            
            rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
            List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
            rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId();
            newCostItems.addAll(TestHelper.createCostItems(rTypeId, bProj.Id));
            insert newCostItems;
            
            Double totalCostAmtLocal = 0;
            Double totalCostAmtCHF = 0;
            updCostItems = new List<Cost_Item__c>();
            
            for(Cost_Item__c ci : [SELECT Extra_cost__c, Expected_amount__c, Expected_amount_CHF__c, Validated_amount__c, Validated_amount_CHF__c FROM Cost_Item__c WHERE Id IN :newCostItems]){
                //TEST SetExtraCostOnCostItems() ON INSERT
                SYSTEM.Assert(ci.Extra_cost__c == 'No');
                
                //TEST convertCostItemCurrency() ON INSERT
               // SYSTEM.Assert(ci.Expected_amount_CHF__c == (ci.Expected_amount__c * 1.50));
                //SYSTEM.Assert(ci.Validated_amount__c == ci.Expected_amount__c);
                //SYSTEM.Assert(ci.Validated_amount_CHF__c == ci.Expected_amount_CHF__c);
    
                totalCostAmtLocal = totalCostAmtLocal + ci.Expected_amount__c;
                totalCostAmtCHF = totalCostAmtCHF + ci.Expected_amount_CHF__c;
                
                ci.Expected_amount__c = 2000;
                ci.Validated_amount__c = 2000;
                updCostItems.add(ci);
            }
            
            Budget_Overview__c bo = [
                SELECT Expected_Cost_local__c, Expected_Cost_CHF__c, Confirmed_Cost_local__c, Confirmed_Cost_CHF__c 
                FROM Budget_Overview__c WHERE Boutique_Project__c = :bProj.Id AND Budget_Item__c = 'Fees'
            ];
            
            //TEST RollUpExpectedCosts ON INSERT
            SYSTEM.AssertEquals(totalCostAmtLocal/2, bo.Expected_Cost_local__c);
            SYSTEM.AssertEquals(totalCostAmtCHF/2, bo.Expected_Cost_CHF__c);
            
            //TEST RollUpConfirmedCostItems ON INSERT
            SYSTEM.AssertEquals(totalCostAmtLocal/2, bo.Confirmed_Cost_local__c);
            SYSTEM.AssertEquals(totalCostAmtCHF/2, bo.Confirmed_Cost_CHF__c);
            
            update updCostItems;
            totalCostAmtLocal = 0;
            totalCostAmtCHF = 0;
            
            for(Cost_Item__c ci : [SELECT Extra_cost__c, Expected_amount__c, Expected_amount_CHF__c FROM Cost_Item__c WHERE Id IN :updCostItems]){
                //TEST convertCostItemCurrency() ON UPDATE
               // SYSTEM.Assert(ci.Expected_amount_CHF__c == (ci.Expected_amount__c * 1.50));
    
                totalCostAmtLocal = totalCostAmtLocal + ci.Expected_amount__c;
                totalCostAmtCHF = totalCostAmtCHF + ci.Expected_amount_CHF__c;
            }
            
            bo = [SELECT Expected_Cost_local__c, Expected_Cost_CHF__c, Confirmed_Cost_local__c, Confirmed_Cost_CHF__c 
                    FROM Budget_Overview__c WHERE Boutique_Project__c = :bProj.Id AND Budget_Item__c = 'Fees'];
            
            //TEST RollUpExpectedCosts ON UPDATE
            SYSTEM.AssertEquals(totalCostAmtLocal/2, bo.Expected_Cost_local__c);
            SYSTEM.AssertEquals(totalCostAmtCHF/2, bo.Expected_Cost_CHF__c);
            
            //TEST RollUpConfirmedCostItems ON UPDATE
            SYSTEM.AssertEquals(totalCostAmtLocal/2, bo.Confirmed_Cost_local__c);
            SYSTEM.AssertEquals(totalCostAmtCHF/2, bo.Confirmed_Cost_CHF__c);
            
            bProj = [SELECT Status__c FROM RECO_Project__c WHERE Id = :bProj.Id];
            bProj.Status__c = 'Completed';
            update bProj;
            
            try{
                newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
                insert newCostItems;
            }
            catch(Exception e){
                //TEST StopInsertOfCostItems() ON INSERT
                SYSTEM.Assert(e.getMessage().contains('Cost items cannot be created for completed projects.'));
            }
        }

        SYSTEM.runAs(TestHelper.NationalBoutiqueManager){
            try{
                delete updCostItems;
            }
            catch(Exception e){
                //TEST StopDeletionOfCostItems() ON DELETE
                //SYSTEM.Assert(e.getMessage().contains('You are not authorized to delete cost items after their approval or submission to approval.'));
            }
        }
    }
    
    //TESTING ValidateCurrencyConversion()
    static testMethod void unitTest2(){
        List<Cost_Item__c> updCostItems;
        
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        newCostItems[0].Do_Not_Convert__c = true;
        insert newCostItems;
        
        //SYSTEM.AssertEquals(1, [SELECT Id FROM Task WHERE Subject = 'Check Currency Conversion for Cost Items'].size());
    }
}
/*
*   @Class  :   CurrencyConverter.cls
*   @Description:   Class for currency conversion
*   @param  :   Value in current currency, current currency (3-letter code), output currency (3-letter code)
*   @return :   -1 if any error arises, converted value otherwise
*   @Author :   XXXXX
*   @Created:   22 OCT 2013
*
*   Modification Log:   
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer        Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy         11 APR 2014     Fix: page error 'LimitException: Too many query rows: 50001'
*   Jacky Uy         17 APR 2014     Fix: removed condition which returns a -1 when amount is less than 0
*   Jacky Uy         17 APR 2014     Fix: removed limit condition when querying all Currency records
*   Jacky Uy         17 APR 2014     Fix: Added a CurrencyConverter constructor to pass a Set of Local Currencies
*   Jacky Uy         22 APR 2014     Add: New constructors to pass the record's creation year
*   Jacky Uy         28 APR 2014     Refactor: Removed the CurrencyConverter(), CurrencyConverter(x), convertValueFromTo(x,y,z) constructors
*   Jacky Uy         02 MAY 2014     Change: Return value should be 0 if conversion fails
*   Jacky Uy         09 MAY 2014     Add: Static variable and methods to address 'Too many sql queries limitation'
*   koteswar         19 March 2015   Add : condition on convertCurrencyFromTo method for currennt year currency check     
*/

public with sharing class CurrencyConverter {
    private Map<String, Double> mapExchangeRatePerCurrencyPair {get; set;}
    
    //***THIS CONSTURCTOR SHOULD BE OBSOLETE***PLEASE CHANGE OTHER CODES TO USE THE STATIC METHODS***//
    public CurrencyConverter(Set<String> createdYears, Set<String> localCurrencies) {
        mapExchangeRatePerCurrencyPair = new Map<String, Double>();
        for(Currency__c c : [SELECT Rate__c, From__c, To__c, Year__c FROM Currency__c WHERE Year__c IN :createdYears AND From__c IN :localCurrencies ORDER BY CreatedDate DESC]){
            if(!mapExchangeRatePerCurrencyPair.containsKey(c.Year__c + '>' + c.From__c + '>' + c.To__c))
                mapExchangeRatePerCurrencyPair.put(c.Year__c + '>' + c.From__c + '>' + c.To__c, double.valueOf(c.Rate__c));
        }
    }
    
    //***THIS CONSTURCTOR SHOULD BE OBSOLETE***PLEASE CHANGE OTHER CODES TO USE THE STATIC METHODS***//
    public Double convertValueFromTo (Double dblValue, String strFromCurrencyCode, String strToCurrencyCode, Datetime createdDate) {
        if(strFromCurrencyCode == null || strFromCurrencyCode == '' || strToCurrencyCode == null || strToCurrencyCode == '' || dblValue == null)
            return 0;
        
        if(strFromCurrencyCode == strToCurrencyCode)
            return dblValue;
        else{
            String yearCreated = (createdDate!=null) ? String.valueOf(createdDate.year()) : String.valueOf(SYSTEM.Today().year());
            Double rate = mapExchangeRatePerCurrencyPair.get(yearCreated + '>' + strFromCurrencyCode + '>' + strToCurrencyCode);

            if(rate == null)
                return 0;
            else
                return dblValue*rate;
        }
    }
    
    //STATIC METHODS TO PREVENT ERROR ON 'Too many sql queries invoked'
    //BUILDS A CURRENCY CONVERSION MAP BY TAKING INTO ACCOUNT THE RECORD CREATION YEAR
    //ONLY QUERIES THE LOCAL CURRENCIES IN USE FOR CONVERSION INSTEAD OF QUERYING ALL CURRENCIES
    //THIS FIXES THE ISSUE ON 'LimitException: Too many query rows: 50001' ERROR
    
    public static Map<String, Double> mapCurrencyConversion {
        get{
            if(mapCurrencyConversion == null){
                mapCurrencyConversion = new Map<String, Double>();
            }
            return mapCurrencyConversion;
        }
        set;
    }
    
    public static void PopulateCurrencyConversion(Set<String> createdYears, Set<String> itemCurrencies){
        for(Currency__c c : [SELECT Rate__c, From__c, To__c, Year__c FROM Currency__c WHERE Year__c IN :createdYears AND From__c IN :itemCurrencies ORDER BY CreatedDate DESC]){
            if(!mapCurrencyConversion.containsKey(c.Year__c + '>' + c.From__c + '>' + c.To__c))
                mapCurrencyConversion.put(c.Year__c + '>' + c.From__c + '>' + c.To__c, double.valueOf(c.Rate__c));
        }
    }
    
    public static Double convertCurrencyFromTo(Double itemAmount, String fromCurrency, String toCurrency, Datetime createdDate) {
        if(mapCurrencyConversion==null || fromCurrency==null || fromCurrency=='' || toCurrency==null || toCurrency=='' || itemAmount==null)
            return 0;
        
        if(fromCurrency == toCurrency)
            return itemAmount;
        else{
            String yearCreated = (createdDate!=null) ? String.valueOf(createdDate.year()) : String.valueOf(SYSTEM.Today().year());
            //added else condition to check the current year currency
            if(mapCurrencyConversion.containsKey(yearCreated + '>' + fromCurrency + '>' + toCurrency))
                return itemAmount * mapCurrencyConversion.get(yearCreated + '>' + fromCurrency + '>' + toCurrency);                
            else if(mapCurrencyConversion.containsKey(System.today().year()+ '>' + fromCurrency + '>' + toCurrency))
                return itemAmount * mapCurrencyConversion.get(system.today().year()+ '>' + fromCurrency + '>' + toCurrency);                
            else
                return 0;
        }
    }
}
/*
Created Date: 3/3/15
Created By: Chirag Gulati
 */
@isTest
private class CurrencyUploadBatchTest {

    static testMethod void myUnitTest() {
    	//TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c btqProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        insert btqProj;
        
        rTypeId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        Estimate_Item__c eitem = TestHelper.createEstimateItem(rTypeId, btqProj.Id);
        insert eitem;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId();
        Cost_Item__c citem = TestHelper.createCostItem(rTypeId, btqProj.Id);
        insert citem;
        
        rTypeId = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        Offer__c offer = TestHelper.createOffer(rTypeId, btqProj.Id);
        offer.Dis_Approval_date__c = System.today();
        offer.Approval_status__c = 'Approved';
        insert offer;
        
        citem.offer__c = offer.Id;
        update citem;
        
        String query = 'SELECT Id, Cost_estimate_approval_date__c, OpeningFormula__c, (SELECT Id FROM Estimate_Items__r), (SELECT Id, RecordTypeId, Offer__r.Dis_Approval_date__c, Offer__r.Approval_status__c FROM Cost_Items__r) FROM RECO_Project__c WHERE Cost_estimate_approval_date__c = THIS_YEAR OR OpeningFormula__c= THIS_YEAR';
    	Test.startTest();
        PostCurrencyUploadBatch c = new PostCurrencyUploadBatch();
        Database.executeBatch(c);
        Test.stopTest();
    }
}
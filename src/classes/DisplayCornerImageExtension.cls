public class DisplayCornerImageExtension {
    private ApexPages.StandardController stdController;
    public Corner__c corner {get; set;}
    public String imageSrc {get; set;}
    
    public DisplayCornerImageExtension (ApexPages.StandardController stdController) {
        this.stdController = stdController;
        corner = (Corner__c)stdController.getRecord();
        
        try {
        	Document doc = [SELECT Id FROM Document WHERE DeveloperName = 'No_Image_Logo'];
        	imageSrc = '/servlet/servlet.FileDownload?file=' + String.valueOf(doc.Id);  // No_Image_Logo by default
        } catch(Exception e) {
        	imageSrc = ''; // for testing purposes
        }
        
        List<Project_File_Area__c> imgs = [SELECT Id FROM Project_File_Area__c WHERE Main_Project_Image__c = true AND Project__c IN (SELECT Id FROM Project__c WHERE Corner__c = :corner.Id) ORDER BY LastModifiedDate DESC LIMIT 1];
        for (Project_File_Area__c img : imgs) {
           List<Attachment> files = [SELECT Id FROM Attachment WHERE ParentId = :img.Id LIMIT 1];
           for (Attachment file : files) {
               imageSrc = '/servlet/servlet.FileDownload?file=' + file.Id;
           }           
        }
    }

}
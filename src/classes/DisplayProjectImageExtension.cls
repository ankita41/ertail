public class DisplayProjectImageExtension {
    private ApexPages.StandardController stdController;
    public Project__c project {get; set;}
    public String imageSrc {get; set;}
    
    // Constructor for extending the standard controller
    public DisplayProjectImageExtension (ApexPages.StandardController stdController) {
        this.stdController = stdController;
        project = (Project__c)stdController.getRecord();
        
        try {
        	Document doc = [SELECT Id FROM Document WHERE DeveloperName = 'No_Image_Logo'];
        	imageSrc = '/servlet/servlet.FileDownload?file=' + String.valueOf(doc.Id);  // No_Image_Logo by default
        } catch(Exception e) {
        	imageSrc = ''; // for testing purposes
        }
        
        List<Project_File_Area__c> imgs = [SELECT Id FROM Project_File_Area__c WHERE Project__c = :project.Id AND Main_Project_Image__c = true LIMIT 1];
        for (Project_File_Area__c img : imgs) {
           List<Attachment> files = [SELECT Id FROM Attachment WHERE ParentId = :img.Id LIMIT 1];
           for (Attachment file : files) {
               imageSrc = '/servlet/servlet.FileDownload?file=' + file.Id;
           }           
        }
    }
}
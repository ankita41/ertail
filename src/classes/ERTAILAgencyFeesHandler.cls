/*
*   @Class      :   ERTAILAgencyFeesHandler.cls
*   @Description:   Static methods Related to Agency fees & Boutique Agency Fees
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
    Ankita Singhal  2 Apr 2017   Ertail Enhancement 2017
*   ------------------------------------------------------------------------------------------------------------------------    
          
*/
public with sharing class ERTAILAgencyFeesHandler {
 public class CustomException extends Exception{}
    
    /* Retrieve the Agency fee records for the campaign Ids passed as parameters */
    private static Map<Id, List<ERTAILCampaign_Agency_Fee__c>> CampaignAgencyFeesGroupedByCampaignId(Set<Id> campaignIds){
        Map<Id, List<ERTAILCampaign_Agency_Fee__c>> cafGroupedByCampaignId = new Map<Id, List<ERTAILCampaign_Agency_Fee__c>>();
        for (ERTAILCampaign_Agency_Fee__c caf : [Select Id
                                                        , Name
                                                        , OP_max_fee__c
                                                        , Actual_fee_Eur__c
                                                        , Campaign__c
                                                        , Agency_Fee__r.Flag_or_Key__c
                                                        , Agency_Fee__r.Scope_limitation__c
                                                        , Agency_Fee__r.Apply_to_Cluster__c
                                                        , Campaign__r.Is_Cluster__c 
                                                from ERTAILCampaign_Agency_Fee__c 
                                                where Campaign__c in :campaignIds 
                                                and status__c='Active']){
            if (!cafGroupedByCampaignId.containsKey(caf.Campaign__c)){
                cafGroupedByCampaignId.put(caf.Campaign__c, new List<ERTAILCampaign_Agency_Fee__c>());
            }
            cafGroupedByCampaignId.get(caf.Campaign__c).add(caf);
        }
        
        return cafGroupedByCampaignId;
    }
    
    /* Create Boutique Agency fees whenever a Boutique is added to a Campaign (aka a Campaign Boutique is inserted) */
    public static void CreateBoutiqueAgencyFeesAfterCampaignBoutiqueInsert(List<Campaign_Boutique__c> newCampaignBoutiques){
        // Extract all the Campaign Ids
        Set<Id> campaignIds = new Set<Id>();
        
        for (Campaign_Boutique__c cb : newCampaignBoutiques){
            campaignIds.add(cb.CampaignId__c);
        }
        
        // request all the Campaign Agency fees and group them by Campaign Id
        Map<Id, List<ERTAILCampaign_Agency_Fee__c>> cafGroupedByCampaignId = CampaignAgencyFeesGroupedByCampaignId(campaignIds);
        
        // for each new Campaign Boutique identify the applicable Campaign Agency fee and create a Boutique Agency fee record
        List<ERTAILBoutique_Agency_fee__c> newBtqAgencyFees = new List<ERTAILBoutique_Agency_fee__c>();
        for (Campaign_Boutique__c cb : newCampaignBoutiques){
            newBtqAgencyFees.addAll(GetBoutiqueAgencyFeesThatApplyToCampaignBoutique(cb, cafGroupedByCampaignId).values());
        }
        if (newBtqAgencyFees.size() >0){
            insert newBtqAgencyFees;
        }
    }
    
    private static Map<Id, ERTAILBoutique_Agency_fee__c> GetBoutiqueAgencyFeesThatApplyToCampaignBoutique(Campaign_Boutique__c cb, Map<Id, List<ERTAILCampaign_Agency_Fee__c>> campaignAgencyFeesGroupedByCampaignId){
        Map<Id, ERTAILBoutique_Agency_fee__c> bafListToReturn = new Map<Id, ERTAILBoutique_Agency_fee__c>();
        // get the appropriate list of Campaign Agency fees
        if (!campaignAgencyFeesGroupedByCampaignId.containsKey(cb.CampaignId__c))
            return bafListToReturn;

        for(ERTAILCampaign_Agency_Fee__c caf : campaignAgencyFeesGroupedByCampaignId.get(cb.CampaignId__c)){
            // In store case
            if (ERTAILMasterHelper.SCOPE_INSTORE.equals(cb.Scope_limitation__c)){
                if(ERTAILMasterHelper.SCOPE_INSTORE.equals(caf.Agency_Fee__r.Scope_limitation__c)){
                    bafListToReturn.put(caf.Id, new ERTAILBoutique_Agency_fee__c(
                                            OP_max_fee__c = caf.OP_max_fee__c
                                            , Name = caf.Name
                                            , Campaign_Actual_fee__c = caf.Actual_fee_Eur__c
                                            , Campaign_Agency_fee__c = caf.Id
                                            , Campaign_Boutique__c = cb.Id
                                        ));
                }
            }
            //Start Added by Ankita singhal
            else if(ERTAILMasterHelper.SCOPE_INSTOREDISPLAY.equals(cb.Scope_limitation__c)){
                if(ERTAILMasterHelper.SCOPE_INSTOREDISPLAY.equals(caf.Agency_Fee__r.Scope_limitation__c)){
                    bafListToReturn.put(caf.Id, new ERTAILBoutique_Agency_fee__c(
                                            OP_max_fee__c = caf.OP_max_fee__c
                                            , Name = caf.Name
                                            , Campaign_Actual_fee__c = caf.Actual_fee_Eur__c
                                            , Campaign_Agency_fee__c = caf.Id
                                            , Campaign_Boutique__c = cb.Id
                                        ));
                }
            }
            //End
            else{
                if (!ERTAILMasterHelper.SCOPE_INSTORE.equals(caf.Agency_Fee__r.Scope_limitation__c) && !ERTAILMasterHelper.SCOPE_INSTOREDISPLAY.equals(caf.Agency_Fee__r.Scope_limitation__c)){
                    // If Agency fee and btq is flag OR if Agency fee and Btq are NOT flag
                    if (caf.Agency_Fee__r.Flag_or_Key__c == (ERTAILMasterHelper.TYPOLOGY_KEY.contains(cb.Typology__c) || ERTAILMasterHelper.TYPOLOGY_KEY.contains(cb.Typology__c))){
                        bafListToReturn.put(caf.Id, new ERTAILBoutique_Agency_fee__c(
                                                OP_max_fee__c = caf.OP_max_fee__c
                                                , Name = caf.Name
                                                , Campaign_Actual_fee__c = caf.Actual_fee_Eur__c
                                                , Campaign_Agency_fee__c = caf.Id
                                                , Campaign_Boutique__c = cb.Id
                                            ));
                    }
                }
            }
        }
        return bafListToReturn;
    }
    
    /* Delete Boutique Agency fees linked to a deleted Campaign Boutique */
    public static void DeleteBoutiqueAgencyFeesAfterCampaignBoutiqueDelete(Map<Id, Campaign_Boutique__c> oldCampaignBoutiquesMap){
        delete [Select Id from ERTAILBoutique_Agency_fee__c where Campaign_Boutique__c in :oldCampaignBoutiquesMap.keySet()];
    }
    
    // if the Campaign Boutique scope has changed, the Boutique Agency fee is updated
    public static void UpdateBoutiqueAgencyFeesAfterCampaignBoutiquesUpdate(Map<Id, Campaign_Boutique__c> oldCampaignBoutiquesMap, Map<Id, Campaign_Boutique__c> newCampaignBoutiquesMap){
        
        Set<Id> campaignIds = new Set<Id>();
        Map<Id, Campaign_Boutique__c> cbWithUpdatedScopeMap = new Map<Id, Campaign_Boutique__c>();
        // Extract the boutiques where Scope has changed
        for (Id campaignBtqId : newCampaignBoutiquesMap.keySet()){
            if (oldCampaignBoutiquesMap.get(campaignBtqId).Scope_limitation__c != newCampaignBoutiquesMap.get(campaignBtqId).Scope_limitation__c){
                cbWithUpdatedScopeMap.put(campaignBtqId, newCampaignBoutiquesMap.get(campaignBtqId));
                campaignIds.add(newCampaignBoutiquesMap.get(campaignBtqId).CampaignId__c);
            }
        }
        
        if (cbWithUpdatedScopeMap.size() == 0)
            return;
        
        UpdateBoutiqueAgencyFeesIfRequired(campaignIds, cbWithUpdatedScopeMap);
    }
    
    
    private static void UpdateBoutiqueAgencyFeesIfRequired(Set<Id> campaignIds, Map<Id, Campaign_Boutique__c> campaignBoutiqueMap){
        // Get the Campaign Agency fees for the Campaign Ids
        Map<Id, List<ERTAILCampaign_Agency_Fee__c>> cafGroupedByCampaignId = CampaignAgencyFeesGroupedByCampaignId(campaignIds);
        
        // Request the existing Boutique Agency fees
        Map<Id, Map<Id, Set<Id>>> bafIdsGroupedByCampaignBtqIdThenCampaignAgencyFeeId = new Map<Id, Map<Id, Set<Id>>>();
        for (ERTAILBoutique_Agency_fee__c baf : [Select Id, Campaign_Boutique__c, Campaign_Agency_fee__c from ERTAILBoutique_Agency_fee__c where Campaign_Boutique__c in :campaignBoutiqueMap.keySet()]){
            if (!bafIdsGroupedByCampaignBtqIdThenCampaignAgencyFeeId.containsKey(baf.Campaign_Boutique__c))
                bafIdsGroupedByCampaignBtqIdThenCampaignAgencyFeeId.put(baf.Campaign_Boutique__c, new Map<Id, Set<Id>>());
            Map<Id, Set<Id>> bafIdsGroupedByCampaignAgencyFeeId = bafIdsGroupedByCampaignBtqIdThenCampaignAgencyFeeId.get(baf.Campaign_Boutique__c);
            if (!bafIdsGroupedByCampaignAgencyFeeId.containsKey(baf.Campaign_Agency_fee__c))
                bafIdsGroupedByCampaignAgencyFeeId.put(baf.Campaign_Agency_fee__c, new Set<Id>());
            bafIdsGroupedByCampaignAgencyFeeId.get(baf.Campaign_Agency_fee__c).add(baf.Id);
        }       
        
        List<ERTAILBoutique_Agency_fee__c> boutiqueAgencyFeesToCreate = new List<ERTAILBoutique_Agency_fee__c>();
        Set<Id> boutiqueAgencyFeeIdsToDelete = new Set<Id>();
        // For each updated CampaignBoutique, identify the BoutiqueAgency fees to create and the Boutique Agency fees to delete
        for(Campaign_Boutique__c cb : campaignBoutiqueMap.values()){
            //Get the campaign Agency that apply to the Campaign Boutique
            Map<Id, ERTAILBoutique_Agency_Fee__c> boutiqueAgencyFeesToCreateGroupedByCafId = GetBoutiqueAgencyFeesThatApplyToCampaignBoutique(cb,cafGroupedByCampaignId);
            //throw new CustomException('' + boutiqueAgencyFeesToCreateGroupedByCafId);
            // Get the existing Boutique Agency fees for the campaign
            
            Map<Id, Set<Id>> bafIdsGroupedByCampaignAgencyFeeId = bafIdsGroupedByCampaignBtqIdThenCampaignAgencyFeeId.containsKey(cb.Id) ? bafIdsGroupedByCampaignBtqIdThenCampaignAgencyFeeId.get(cb.Id) : new Map<Id, Set<Id>>();
                    
            for (Id cafCandidateId : boutiqueAgencyFeesToCreateGroupedByCafId.keySet()){
                // if there is already a Boutique Agency fee between cafCandidate and bafIdsGroupedByAgencyFeeId, it is removed from bafIdsGroupedByAgencyFeeId
                if (bafIdsGroupedByCampaignAgencyFeeId.containsKey(cafCandidateId)){
                    bafIdsGroupedByCampaignAgencyFeeId.remove(cafCandidateId);
                } // if there is no Boutique Agency fee between cafCandidate and bafIdsGroupedByAgencyFeeId, it is added in the list of Boutique Agency fee to create
                else {
                    boutiqueAgencyFeesToCreate.add(boutiqueAgencyFeesToCreateGroupedByCafId.get(cafCandidateId));
                }
            }
            
            // If bafIdsGroupedByAgencyFeeId is not empty, These boutique agency fees need to be deleted        
            if (bafIdsGroupedByCampaignAgencyFeeId.size() > 0){
                for(Set<Id> bafIds : bafIdsGroupedByCampaignAgencyFeeId.values()){
                    boutiqueAgencyFeeIdsToDelete.addAll(bafIds);
                }
            }
        }    
        //throw new CustomException('' + boutiqueAgencyFeeIdsToDelete);
        if (boutiqueAgencyFeeIdsToDelete.size() > 0)
            delete [Select Id from ERTAILBoutique_Agency_fee__c where Id in :boutiqueAgencyFeeIdsToDelete];
        
        if (boutiqueAgencyFeesToCreate.size() > 0)
            insert boutiqueAgencyFeesToCreate;
    }
    
    // if the Campaign "Is Cluster" has changed, the Boutique Agency fees are updated
    public static void UpdateBoutiqueAgencyFeesAfterCampaignUpdate(Map<Id, Campaign__c> oldCampaignMap, Map<Id, Campaign__c> newCampaignMap){
        // Extract the Campaigns where Is_Cluster__c has changed
        Map<Id, Campaign__c> updatedCampaignIds = new Map<Id, Campaign__c>();
        for (Id campaignId : newCampaignMap.keySet()){
            if (oldCampaignMap.get(campaignId).Is_Cluster__c != newCampaignMap.get(campaignId).Is_Cluster__c){
                updatedCampaignIds.put(campaignId, newCampaignMap.get(campaignId));
            }
        }
        if (updatedCampaignIds.size() == 0)
            return;
            
        // Request the Campaign Boutiques linked to the campaigns
        Map<Id, Campaign_Boutique__c> cbMap = new Map<Id, Campaign_Boutique__c>([Select Id
                                                                                    , CampaignId__c
                                                                                    , Scope_limitation__c
                                                                                    , Typology__c
                                                                                 from Campaign_Boutique__c 
                                                                                 where Campaign_Market__r.Campaign__c in :updatedCampaignIds.keySet()]);
    
        // Update the Boutique Agency fees if required
        UpdateBoutiqueAgencyFeesIfRequired(updatedCampaignIds.keySet(), cbMap);
        
        UpdateCampaignAgencyFeeSteps(updatedCampaignIds);
        
    }
    
    public static void UpdateCampaignAgencyFeeSteps(Map<Id, Campaign__c> campaigns){
        List<ERTAILCampaign_Agency_Fee__c> cafList = [Select Id, Fee_Status_picklist__c, Agency_Fee__r.Apply_to_Cluster__c, Campaign__c from ERTAILCampaign_Agency_Fee__c where Campaign__c in :campaigns.keySet()];
        for (ERTAILCampaign_Agency_Fee__c caf : cafList) {
            caf.Fee_Status_picklist__c = (caf.Agency_Fee__r.Apply_to_Cluster__c == campaigns.get(caf.Campaign__c).Is_Cluster__c? '10': null);
        }
        update cafList;
    }
    
    public static void UpdateBoutiqueAgencyFeeAfterCampaignAgencyFeeUpdate(Map<Id, ERTAILCampaign_Agency_Fee__c> oldCampaignAgencyFeeMap, Map<Id, ERTAILCampaign_Agency_Fee__c> newCampaignAgencyFeeMap){
        // Extract the list of Campaign Agency fee where OP max fee or Actual fee has been updated
        Map<Id, ERTAILCampaign_Agency_Fee__c> updatedOPMaxFeeInCampaignAgencyFee = new Map<Id, ERTAILCampaign_Agency_Fee__c>();
        Map<Id, ERTAILCampaign_Agency_Fee__c> updatedActualFeeInCampaignAgencyFee = new Map<Id, ERTAILCampaign_Agency_Fee__c>();
        
        for (Id campaignAgencyFeeId : newCampaignAgencyFeeMap.keySet()){
            if (oldCampaignAgencyFeeMap.get(campaignAgencyFeeId).OP_max_fee__c != newCampaignAgencyFeeMap.get(campaignAgencyFeeId).OP_max_fee__c){
                updatedOPMaxFeeInCampaignAgencyFee.put(campaignAgencyFeeId, newCampaignAgencyFeeMap.get(campaignAgencyFeeId));
            }
            if (oldCampaignAgencyFeeMap.get(campaignAgencyFeeId).Actual_fee_Eur__c != newCampaignAgencyFeeMap.get(campaignAgencyFeeId).Actual_fee_Eur__c){
                updatedActualFeeInCampaignAgencyFee.put(campaignAgencyFeeId, newCampaignAgencyFeeMap.get(campaignAgencyFeeId));
            }
        }
        
        Set<Id> updatedCampaignAgencyFeeIds = new Set<Id>();
        updatedCampaignAgencyFeeIds.addAll(updatedOPMaxFeeInCampaignAgencyFee.keySet());
        updatedCampaignAgencyFeeIds.addAll(updatedActualFeeInCampaignAgencyFee.keySet());
        
        // Request all the Boutique Agency fees linked to these Campaign Agency Fees
        List<ERTAILBoutique_Agency_fee__c> bafList = [Select Id
                                                            , OP_max_fee__c
                                                            , Campaign_Actual_fee__c 
                                                            , Campaign_Agency_fee__c
                                                        from ERTAILBoutique_Agency_fee__c 
                                                        where Campaign_Agency_fee__c in :updatedCampaignAgencyFeeIds];
        
        for (ERTAILBoutique_Agency_fee__c baf : bafList){
            if (updatedOPMaxFeeInCampaignAgencyFee.containsKey(baf.Campaign_Agency_fee__c))
                baf.OP_max_fee__c = updatedOPMaxFeeInCampaignAgencyFee.get(baf.Campaign_Agency_fee__c).OP_max_fee__c;
            if (updatedActualFeeInCampaignAgencyFee.containsKey(baf.Campaign_Agency_fee__c)){
                baf.Campaign_Actual_fee__c = updatedActualFeeInCampaignAgencyFee.get(baf.Campaign_Agency_fee__c).Actual_fee_Eur__c;
            }
        }
        
        update bafList;
    }
}
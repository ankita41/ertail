/*****
*   @Class      :   ERTAILBoutiqueHandler.cls
*   @Description:   Static methods called by triggers.  Please note that no sharing is specified for the class to inherit from the caller.
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   TPA             13 Aug 2015     Set the Boutique account ownership to the first available admin   
    Ankita Singhal  2 Apr 2017      Ertail Enhancement 2017             
*****/
public class ERTAILBoutiqueHandler {
    public class CustomException extends Exception {}
    
    private static Id firstAdminId = [Select Id, Name From User u where IsActive=true and Profile.Name='System Admin' order by lastlogindate desc limit 1].Id;
    
    public static void CreateBoutiqueLocationBeforeInsert(List<RTCOMBoutique__c> newList){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERTAIL Location').getRecordTypeId();
        List<Account> accToInsert = new List<Account>();
        
        for(RTCOMBoutique__c newBtq : newList){
            accToInsert.add(new Account(Name=newBtq.Name, RecordTypeId=rtId, OwnerId=firstAdminId));
        }
        
        insert accToInsert;

        for(Integer i=0 ; i<newList.size() ; i++){
            newList[i].Location__c = accToInsert[i].Id;
        }
    }
    
    
    // If the Boutique has never been used, the boutique spaces are deleted
    // If the Boutique is referenced by a Campaign Boutique, the Boutique is not deleted but marked as "Excluded from future campaigns"
    public static void ControlBeforeERTAILBoutiqueDelete(Map<Id, RTCOMBoutique__c> oldBoutiqueMap){
        Set<Id> boutiquesToHide = new Set<Id>();
        
        // Request the Campaign Boutiques linked to one of the deleted Boutiques
        for (Campaign_Boutique__c cb : [Select Id, RTCOMBoutique__c from Campaign_Boutique__c where RTCOMBoutique__c in :oldBoutiqueMap.keySet()]){
            boutiquesToHide.add(cb.RTCOMBoutique__c);
        }

        if (boutiquesToHide.size() == 0){
            return;
        }
        
        for(Id btqId : boutiquesToHide){
            oldBoutiqueMap.get(btqId).addError('Boutique is referenced in a Campaign and cannot be deleted.  Please mark it as excluded from future Campaigns instead.');
        }
    }
    
    // Create an ERTAIL Boutique after a RECO Boutique insert and set the owner of the Boutique as ERTAIL Market owner 
    public static void InsertERTAILBoutiqueAfterRECOBoutiqueInsert(Map<Id, Boutique__c> newRECOMap){
        Map<Id, RTCOMMarket__c> ERTAILMarketsGroupedByRECOMarketIds = new Map<Id, RTCOMMarket__c>();
        Map<Id, Id> RECOMarketIdsGroupedByRECOBoutiqueIds = new Map<Id, Id>();
        
        // Request the RECO Markets
        for (Boutique__c btq : [Select boutique_location_account__c, boutique_location_account__r.Market__c from Boutique__c where Id in :newRECOMap.keySet()]){
            if (btq.boutique_location_account__c != null && btq.boutique_location_account__r.Market__c != null){
                ERTAILMarketsGroupedByRECOMarketIds.put(btq.boutique_location_account__r.Market__c, null);
                RECOMarketIdsGroupedByRECOBoutiqueIds.put(btq.Id, btq.boutique_location_account__r.Market__c);
            }
        }
        
        // Request the ERTAILMarkets
        for (RTCOMMarket__c ertailMarket : [Select Id, RECOMarket__c from RTCOMMarket__c where RECOMarket__c in :ERTAILMarketsGroupedByRECOMarketIds.keySet()]){
            if (!ERTAILMarketsGroupedByRECOMarketIds.containsKey(ertailMarket.RECOMarket__c)){
                continue;
            }
            // remove RECO Markets linked to multiple ERTAIL Markets
            if (ERTAILMarketsGroupedByRECOMarketIds.get(ertailMarket.RECOMarket__c) != null){
                ERTAILMarketsGroupedByRECOMarketIds.remove(ertailMarket.RECOMarket__c);
            }else{
                ERTAILMarketsGroupedByRECOMarketIds.put(ertailMarket.RECOMarket__c, ertailMarket);
            }
        }
        
        // Create new ERTAIL Boutiques and link them to the appropriate RECO Markets
        List<RTCOMBoutique__c> ertailBoutiquesToInsert = new List<RTCOMBoutique__c>();
        // Due to the Master detail relationships between RTCOMMatrket and RTCOMBoutique, the Boutique cannot be created if the Market doesn not exists
        String errorMessage = '';
        Id rectypeId = Schema.SObjectType.RTCOMBoutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        for (Boutique__c btq : newRECOMap.values()){
            Id recoMarketId = RECOMarketIdsGroupedByRECOBoutiqueIds.get(btq.Id);
            if (recoMarketId != null && ERTAILMarketsGroupedByRECOMarketIds.containsKey(recoMarketId)){
                ertailBoutiquesToInsert.add(new RTCOMBoutique__c(Name = btq.Name, RECOBoutique__c = btq.Id, RecordTypeId=rectypeId, RTCOMMarket__c = ERTAILMarketsGroupedByRECOMarketIds.get(recoMarketId).Id));
            } else {
                // send email to system admin
                errorMessage += 'Boutique name in RECO: ' + btq.Name + '<br/>';
            }
        }
        
        if (!''.equals(errorMessage)){
            errorMessage = 'The system failed to create ERTAIL Boutiques for the following RECO Boutiques<br/><br/>' + errorMessage;
            if (!Test.isRunningTest())
                sendErrorEmail(errorMessage);
        }
        
        // Insert the new ERTAIL Boutiques  
        if (ertailBoutiquesToInsert.size() >0)
            insert ertailBoutiquesToInsert;
        
    }
    @TestVisible 
    private static void sendErrorEmail(String errorMessage){
    //private static void sendErrorEmail(String errorMessage){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage() ;
        List<String> toAddresses = new List<String>();
        for (User adminUser : [Select email from User where Profile.Name = :ERTAILMasterHelper.ERT_HQPM and IsActive=true]){
            toAddresses.add(adminUser.email);
        }
        
        if(toAddresses.size() == 0){
            return;
        }
        mail.setToAddresses(toAddresses) ;
        mail.setSubject('Exception occured');
        
        String body = '<html><body>'+
                         '<br><br>'+
                         errorMessage +
                         '</body></html>';
        mail.setHtmlBody(body);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
    }
}
@isTest
public class ERTAILBoutiqueHandlerTest 
{
   static testMethod void myUnitTest()
    { 
        RTCOMMarket__c testMarket = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket;        
        RTCOMMarket__c testMarket2 = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket2;        
        RTCOMBoutique__c testBoutique = new RTCOMBoutique__c(
        Name = 'Test Boutique',
        RTCOMMarket__c = testMarket.Id
        );
               
        RTCOMBoutique__c testBoutique2 = new RTCOMBoutique__c(
        Name = 'Test Boutique 2',
        RTCOMMarket__c = testMarket.Id
        );
        //insert testBoutique2;
        list<RTCOMBoutique__c> listRTCOMBoutique=new list<RTCOMBoutique__c>();
        listRTCOMBoutique.add(testBoutique);
        listRTCOMBoutique.add(testBoutique2);  
        insert listRTCOMBoutique;
        test.startTest();
        Map<Id, RTCOMBoutique__c> oldBoutiqueMap=new map<Id, RTCOMBoutique__c>();
       
        for(RTCOMBoutique__c rbtq : listRTCOMBoutique)
       {
        oldBoutiqueMap.put(rbtq.id, rbtq);
       }
        Campaign__c testCampaign = new Campaign__c(
        	Name = 'Test Campaign',
        	Launch_Date__c = System.Today().addMonths(1),
        	End_Date__c = System.Today().addMonths(2),
        	Description__c = 'This is a Test'
        );
        insert testCampaign;
        Campaign_Market__c testCampMkt = new Campaign_Market__c(
        	Campaign__c = testCampaign.Id,
        	RTCOMMarket__c = testMarket.Id
        );
        insert testCampMkt;
        
        insert new Campaign_Boutique__c(
        	Campaign_Market__c = testCampMkt.Id,
        	RTCOMBoutique__c = testBoutique.Id
        );
      
        ERTAILBoutiqueHandler.ControlBeforeERTAILBoutiqueDelete(oldBoutiqueMap);
        test.stopTest();
    }
    static testMethod void myUnitTest1()
    { 
        RTCOMMarket__c testMarket = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket;        
        RTCOMMarket__c testMarket2 = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket2;        
        RTCOMBoutique__c testBoutique = new RTCOMBoutique__c(
        Name = 'Test Boutique',
        RTCOMMarket__c = testMarket.Id
        );
               
        RTCOMBoutique__c testBoutique2 = new RTCOMBoutique__c(
        Name = 'Test Boutique 2',
        RTCOMMarket__c = testMarket.Id
        );
        //insert testBoutique2;
        list<RTCOMBoutique__c> listRTCOMBoutique=new list<RTCOMBoutique__c>();
        listRTCOMBoutique.add(testBoutique);
        listRTCOMBoutique.add(testBoutique2);  
        insert listRTCOMBoutique;
        test.startTest();
        Map<Id, RTCOMBoutique__c> oldBoutiqueMap=new map<Id, RTCOMBoutique__c>();
       
        for(RTCOMBoutique__c rbtq : listRTCOMBoutique)
       {
        oldBoutiqueMap.put(rbtq.id, rbtq);
       }
              
        
        ERTAILBoutiqueHandler.ControlBeforeERTAILBoutiqueDelete(oldBoutiqueMap);
        ERTAILBoutiqueHandler.sendErrorEmail('test');
        test.stopTest();
    }
}
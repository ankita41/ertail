/*****
*   @Class      :   ERTAILBoutiqueOrderDetailsExt.cls
*   @Description:   Extension developed for ERTAILOrdersDetail.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILBoutiqueOrderDetailExt extends ERTailMasterHandler{
    public class CustomException extends Exception {}
	
	public Campaign_Boutique__c CampaignBoutique { get; private set;}
	public String selectedView { get; set;}
    public List<SelectOption> viewOptions{ get{ return ERTAILMasterHelper.viewOptions; } }
    
    Map<Id, String> btqSpaceMap = new Map<Id, String>();
    
    public Map<Id, Boutique_Order_Item__c> BtqOrderItemsOrderedByBtqSpacesId{ get; private set;}
	
	public Boolean showHeader {get ; private set;}

	private List<Boutique_Space__c> btqSpaces {
		get{
			if (btqSpaces == null)
				btqSpaces = BoutiqueSpacesForRTCOMBtqIdsOrderedByRTCOMBtqNameThenSpaceThenCreativity(new List<Id>{CampaignBoutique.RTCOMBoutique__c});
			return btqSpaces;
		}
		set;
	}
	
	private void InitSpacesList(){
		btqSpacesWithCreativity = new List<Boutique_Space__c>();
		btqSpacesWithoutCreativity = new List<Boutique_Space__c>();
		for(Boutique_Space__c bs : btqSpaces){
			Space__c space = AllSpacesMap.get(bs.Space__c);
			if (space.Has_Creativity__c){
				btqSpacesWithCreativity.add(bs);
			}else{
				btqSpacesWithoutCreativity.add(bs);
			}
		}	
	}
	
	public List<Boutique_Space__c> btqSpacesWithCreativity {
		get{
			if (btqSpacesWithCreativity == null){
				InitSpacesList();
			}
			return btqSpacesWithCreativity;
		}
		set;
	}

	public List<Boutique_Space__c> btqSpacesWithoutCreativity {
		get{
			if (btqSpacesWithoutCreativity == null){
				InitSpacesList();
			}
			return btqSpacesWithoutCreativity;
		}
		set;
	}

/***** Wrappers *****/
    public class BoutiqueWrapper{
        public RTCOMBoutique__c btq { get; private set;}
        public List<Boutique_Space__c> btqSpaces  { get; private set;}
        
        public BoutiqueWrapper(Campaign_Boutique__c campaignBtq){
            this.btq = campaignBtq.RTCOMBoutique__r;
            btqSpaces = new List<Boutique_Space__c>();
        }
    }
    
    public BoutiqueWrapper Boutique { get; private set;}
    
    public class CompleteRowWrapper{
        public List<Integer> complete_spaces{ get; private set;}
        public CompleteRowWrapper(Integer my_int){
            complete_spaces= new List<Integer>{my_int};
        }
    }

/***** End Wrappers *****/

/***** CODE *****/
    public ERTAILBoutiqueOrderDetailExt(ApexPages.StandardController stdController) {
        this.CampaignBoutique = (Campaign_Boutique__c)stdController.getRecord();
        CampaignBoutique = [Select c.RTCOMBoutique__r.Image_medium__c, c.Name, c.Campaign_Market__r.Campaign__r.Name, c.Scope_Limitation__c, c.Launch_Date__c, c.End_Date__c, c.Campaign_Market__r.Campaign__r.Description__c, c.Campaign_Market__r.Campaign__c, /*c.Creativity_Step__c,*/ c.RTCOMBoutique__c, c.RTCOMBoutique__r.Name, c.RTCOMBoutique__r.RTCOMMarket__c, c.RTCOMBoutique__r.RTCOMMarket__r.Name From Campaign_Boutique__c c where Id = :CampaignBoutique.Id];
        campaign = this.CampaignBoutique.Campaign_Market__r.Campaign__r;
        // init objects

        showHeader = true;
        if (ApexPages.CurrentPage().getParameters().containsKey('showHeader')){
        	showHeader = Boolean.valueOf(ApexPages.CurrentPage().getParameters().get('showHeader'));
        }
        
        selectedView = 'HQPM_Prop';
        if (ApexPages.CurrentPage().getParameters().containsKey('selectedView')){
        	selectedView = ApexPages.CurrentPage().getParameters().get('selectedView');
        }
        
        Boutique = new BoutiqueWrapper(CampaignBoutique);
       for (Boutique_Space__c bs : BoutiqueSpacesForRTCOMBtqIdOrderedByRTCOMBtqNameThenCreativityOrder(CampaignBoutique.RTCOMBoutique__c))
       { 
        	Boutique.btqSpaces.add(bs);
        	btqSpaceMap.put(bs.Id, bs.Space__r.Decoration_Alias__c);  
       }

       initBtqOrderItemsOrderedByBtqSpacesId();

    }
     
/***** Creativity Part *****/ 
	public CreativityChangeResult creativityResult { get; private set; }
    public String creativityResultJSON { get { return JSON.serialize(creativityResult); } }
    
//	MME comment for test -- private Map <Id, Boutique_Order_Item__c> boiWithCreativityGroupedByIdMap{
	public Map <Id, Boutique_Order_Item__c> boiWithCreativityGroupedByIdMap{
		get{
			if (boiWithCreativityGroupedByIdMap == null)
				boiWithCreativityGroupedByIdMap = new Map<Id, Boutique_Order_Item__c>(BoisWithCreativityForBoutiqueIds(new Set<Id>{CampaignBoutique.Id}));
			return boiWithCreativityGroupedByIdMap;
		}
		set;
	}

    private void initBtqOrderItemsOrderedByBtqSpacesId(){
    	btqOrderItemsOrderedByBtqSpacesId = new Map<Id, Boutique_Order_Item__c>();
       // Create a Map of the Boutique Order Items ordered by Boutique_Space__c
       for (Boutique_Order_Item__c boi : boiWithCreativityGroupedByIdMap.values())
       {
        	BtqOrderItemsOrderedByBtqSpacesId.put(boi.Boutique_Space__c, boi);
       }
       // Create empty Boutique Order items for Boutique Spaces without order items
       for (Boutique_Space__c btqSpace : btqSpacesWithCreativity) {
       	Id btqSpaceId = btqSpace.Id;
       	if (!BtqOrderItemsOrderedByBtqSpacesId.containsKey(btqSpaceId))
       		BtqOrderItemsOrderedByBtqSpacesId.put(btqSpaceId, new Boutique_Order_Item__c(Boutique_Space__c = btqSpaceId, Boutique_Space__r = new Boutique_Space__c(Id = btqSpaceId, Name = btqSpaceMap.get(btqSpaceId))));
       }
    }
    
    public class CreativityChangeResult{
        public Boolean IsChanged;
        public String Title;
        public Id BoutiqueOrderItemId;
        public Integer VisualStep;
        public Id imageId;
        public CreativityChangeResult(){
        }
    }

	public void saveCreativityProposal(){
		creativityResult = new CreativityChangeResult();
        creativityResult.IsChanged = false;
        Id boiId = Apexpages.currentPage().getParameters().get('btqOrderItemId');
        Id campaignItemId = Apexpages.currentPage().getParameters().get('campaignItemId');
        Campaign_Item__c ci = CampaignItemsGroupedById.get(campaignItemId);
        if (isHQPMorAdmin && 'HQPM_Prop'.equals(selectedView)){
            Boutique_Order_Item__c boi = boiWithCreativityGroupedByIdMap.get(boiId);
            boi.HQPM_Campaign_Item__c = campaignItemId;
            boi.HQPM_Campaign_Item__r = ci;
            boi.MPM_Campaign_Item__c = campaignItemId;
            boi.MPM_Campaign_Item__r = ci;
            update new Boutique_Order_Item__c(Id = boiId, HQPM_Campaign_Item__c = campaignItemId);
            creativityResult.Title = 'Modified by HQPM from ' + boi.Recommended_Campaign_Item__r.Alias__c + ' => ' + ci.Alias__c;
            creativityResult.IsChanged = boi.HQPM_Campaign_Item__c != boi.Recommended_Campaign_Item__c;
            creativityResult.imageId = ci.Image_Id__c;
            creativityResult.VisualStep = (ci.Visual_Step_Value__c != null ? Integer.valueOf(ci.Visual_Step_Value__c) : null);
            creativityResult.BoutiqueOrderItemId = boi.Id;
            BtqOrderItemsOrderedByBtqSpacesId.put(boi.Boutique_Space__c, boi);
        }else if (isMPMorAdmin && 'MPM_Prop'.equals(selectedView)){
            Boutique_Order_Item__c boi = boiWithCreativityGroupedByIdMap.get(boiId);
            boi.MPM_Campaign_Item__c = campaignItemId;
            boi.MPM_Campaign_Item__r = ci;
            update new Boutique_Order_Item__c(Id = boiId, MPM_Campaign_Item__c = campaignItemId);
            creativityResult.Title = 'Modified by MPM from ' + boi.HQPM_Campaign_Item__r.Alias__c + ' => ' + ci.Alias__c;
            creativityResult.IsChanged = boi.MPM_Campaign_Item__c != boi.HQPM_Campaign_Item__c;
            creativityResult.VisualStep = (ci.Visual_Step_Value__c != null ? Integer.valueOf(ci.Visual_Step_Value__c) : null);
            creativityResult.imageId = ci.Image_Id__c;
            creativityResult.BoutiqueOrderItemId = boi.Id;
            BtqOrderItemsOrderedByBtqSpacesId.put(boi.Boutique_Space__c, boi);
        }
    }


/***** Quantity Part *****/ 
	public QuantityChangeResult quantityResult { get; private set; }
    public String quantityResultJSON { get { return JSON.serialize(quantityResult); } }
    
    public Map<Id, Boutique_Order_Item__c> boiWithoutCreativityGroupedByCampaignId = new Map<Id, Boutique_Order_Item__c>();
    
    public List<Boutique_Order_Item__c> boiWithQuantityOrderedByCampaignItemName {
		get{
			if (boiWithQuantityOrderedByCampaignItemName == null){
				boiWithQuantityOrderedByCampaignItemName = new List<Boutique_Order_Item__c>();
				Map<Id, Boutique_Order_Item__c> boiMap = BoisWithQuantityForCampaignBtqIdGroupedByCampaignItemId(CampaignBoutique.Id);
				Boutique_Order_Item__c boi;
				for (Campaign_Item__c ci :CampaignItemsWithOutCreativity){
					if(!boiMap.containsKey(ci.Id)){
						boi = new Boutique_Order_Item__c(Campaign_Item_to_order__r = ci, Campaign_Boutique__c = CampaignBoutique.Id, Campaign_Item_to_order__c = ci.Id, Quantity_Step_Picklist__c='10', Campaign__c=campaign.Id);
					}else{
						boi = boiMap.get(ci.Id);
					}
					boiWithQuantityOrderedByCampaignItemName.add(boi);
					boiWithoutCreativityGroupedByCampaignId.put(ci.Id, boi);
				}
				
			}
			return boiWithQuantityOrderedByCampaignItemName;
		}
		private set;
	}
    
    public class QuantityChangeResult{
        public Boolean IsChanged;
        public String Title;
        public Id Id;
        public Id BoutiqueOrderItemId;
        public String Quantity;
        public QuantityChangeResult(){
            
        }
    }
    
    public void saveQuantityCIProposal(){
		quantityResult = new QuantityChangeResult();
        quantityResult.IsChanged = false;
        Id campaignItemId = Apexpages.currentPage().getParameters().get('campaignItemId');
        Integer quantity = (Apexpages.currentPage().getParameters().get('quantity') != ''  ? Integer.valueOf(Apexpages.currentPage().getParameters().get('quantity')) : null);
        if (isHQPMorAdmin && 'HQPM_Prop'.equals(selectedView)){
            Boutique_Order_Item__c boi = boiWithoutCreativityGroupedByCampaignId.get(campaignItemId);
            boi.HQPM_Quantity__c = quantity;
            boi.Quantity_to_Order__c = quantity;
            upsert boi; 
            Integer recommendedQty = Integer.valueOf(boi.Recommended_Quantity__c == null ? 0 : boi.Recommended_Quantity__c);
            Integer HQPMQty = Integer.valueOf(boi.HQPM_Quantity__c == null ? 0 : boi.HQPM_Quantity__c);
            quantityResult.IsChanged =  HQPMQty != recommendedQty;
            if (quantityResult.IsChanged)
                quantityResult.Title = 'Modified by HQPM from ' + recommendedQty + ' => ' + HQPMQty;
            else
                quantityResult.Title = '';
            quantityResult.BoutiqueOrderItemId = boi.Id;
            quantityResult.Id = campaignItemId;
            quantityResult.Quantity = (quantity == null ? '' : String.valueOf(quantity));
        }else if (isMPMorAdmin && 'MPM_Prop'.equals(selectedView)){
            Boutique_Order_Item__c boi = boiWithoutCreativityGroupedByCampaignId.get(campaignItemId);
            boi.MPM_Quantity__c = quantity;
            upsert boi; 
            Integer MPMQty = Integer.valueOf(boi.MPM_Quantity__c == null ? 0 : boi.MPM_Quantity__c);
            Integer HQPMQty = Integer.valueOf(boi.HQPM_Quantity__c == null ? 0 : boi.HQPM_Quantity__c);
            quantityResult.IsChanged =  HQPMQty != MPMQty;
            if (quantityResult.IsChanged)
                quantityResult.Title = 'Modified by MPM from ' + HQPMQty + ' => ' + MPMQty ;
            else
                quantityResult.Title = '';
            quantityResult.BoutiqueOrderItemId = boi.Id;
            quantityResult.Id = campaignItemId;
            quantityResult.Quantity = (quantity == null ? '' : String.valueOf(quantity));                
        }
    }

/***** Fixture & Furnitures Part *****/
	
	public Map<Id, Boutique_Order_Item__c> boiForFFGroupedByFFId = new Map<Id, Boutique_Order_Item__c>();
	public List<Boutique_Order_Item__c> boiOrderedByFixtureName {
		public get{
			if (boiOrderedByFixtureName == null){
				boiOrderedByFixtureName = new List<Boutique_Order_Item__c>();
				Map<Id, Boutique_Order_Item__c> boiMap = BoisWithFixtureForCampaignBtqIdGroupedByFixtureId(CampaignBoutique.Id);
				Boutique_Order_Item__c boi;
				for (Permanent_Fixture_And_Furniture__c ff :PermanentFixturesAndFurnitures){
					if(!boiMap.containsKey(ff.Id)){
						boi = new Boutique_Order_Item__c(Permanent_Fixture_And_Furniture__c = ff.Id, Permanent_Fixture_And_Furniture__r = ff, Campaign_Boutique__c = CampaignBoutique.Id);
					}else{
						boi = boiMap.get(ff.Id);
					}
					boiOrderedByFixtureName.add(boi);
					boiForFFGroupedByFFId.put(ff.Id, boi);
				}
			}
			//throw new CustomException('' + boiOrderedByFixtureName);
			return boiOrderedByFixtureName;
		}
		private set;
	}	
	
	 public void saveQuantityFFProposal(){
		quantityResult = new QuantityChangeResult();
        quantityResult.IsChanged = false;
        Id ffId = Apexpages.currentPage().getParameters().get('ffId');
        Integer quantity = (Apexpages.currentPage().getParameters().get('quantity') != ''  ? Integer.valueOf(Apexpages.currentPage().getParameters().get('quantity')) : null);
        Boutique_Order_Item__c boi = boiForFFGroupedByFFId.get(ffId);
        boi.Quantity_to_Order__c = quantity;
        upsert boi; 
        quantityResult.Id = ffId;
        quantityResult.Quantity = (quantity == null ? '' : String.valueOf(quantity));
        
    }


    public PageReference refresh(){
    	return null;
    }

}
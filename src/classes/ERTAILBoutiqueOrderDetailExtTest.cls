/*
*   @Class      :   ERTAILBoutiqueOrderDetailExtTest.cls
*   @Description:   Test methods for class ERTAILBoutiqueOrderDetailExt.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/ 

@isTest
private class ERTAILBoutiqueOrderDetailExtTest {
    
    static testMethod void myUnitTest() {
        Test.startTest();
                
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
        
        User adminUser = new User(
                            LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
        
        System.runAs(adminUser){
            Id testCampaignId = ERTAILTestHelper.createCampaignMME();

            Campaign_Boutique__c cmpBtq = [select Id from Campaign_Boutique__c where Campaign_Market__r.Campaign__r.Id =: testCampaignId][0];
            
            Boutique_Order_Item__c boi = [select Id, Campaign_item_To_ORder__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:testCampaignId and boutique_Space__r.Space__r.has_Creativity__c = true][0];
            Boutique_Order_Item__c boi2 = [select Id, Campaign_item_To_ORder__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:testCampaignId and boutique_Space__r.Space__r.has_Creativity__c = false][0];
            Boutique_Order_Item__c boi3 = [select Id, Permanent_Fixture_And_Furniture__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:testCampaignId and Campaign_item_To_Order__c = null][0];

            System.assertEquals(boi != null && boi2 != null && boi3 != null, true);

            PageReference pgRef = Page.ERTAILOrdersDetail;
            pgRef.getParameters().put('showHeader', 'true');
            pgRef.getParameters().put('selectedView', 'HQPM_Prop');
            Test.setCurrentPage(pgRef);

            ApexPages.StandardController stdController = new ApexPages.StandardController(cmpBtq);
                
            ERTAILBoutiqueOrderDetailExt ctrl = new ERTAILBoutiqueOrderDetailExt(stdController);
            
            List<Boutique_Space__c> btqSpacesWithoutCreativity = ctrl.btqSpacesWithoutCreativity;
            
            String s = ctrl.creativityResultJSON;
            s = ctrl.quantityResultJSON;
                    
            List<Boutique_Order_Item__c> boiWithQuantityOrderedByCampaignItemName = ctrl.boiWithQuantityOrderedByCampaignItemName;
                    
            ctrl.selectedView = 'HQPM_Prop';
        
            pgRef = Page.ERTAILOrdersDetail;
            pgRef.getParameters().put('btqOrderItemId', boi.Id);
            pgRef.getParameters().put('campaignItemId', boi.Campaign_item_To_ORder__r.Id);
            pgRef.getParameters().put('quantity', '10');
            Test.setCurrentPage(pgRef);
            
            ctrl.saveCreativityProposal();


            pgRef.getParameters().put('btqOrderItemId', boi2.Id);
            pgRef.getParameters().put('campaignItemId', boi2.Campaign_item_To_ORder__r.Id);
            
          //  System.assertEquals(ctrl.boiWithoutCreativityGroupedByCampaignId.get(boi2.Campaign_item_To_ORder__r.Id) != null, true);
            
            
            
            ctrl.selectedView = 'MPM_Prop';
        
            pgRef = Page.ERTAILOrdersDetail;
            pgRef.getParameters().put('btqOrderItemId', boi.Id);
            pgRef.getParameters().put('campaignItemId', boi.Campaign_item_To_ORder__r.Id);
            pgRef.getParameters().put('quantity', '15');
            
            Test.setCurrentPage(pgRef);
            //ctrl.saveQuantityCIProposal();
            
            ctrl.saveCreativityProposal();


            pgRef.getParameters().put('btqOrderItemId', boi2.Id);
            pgRef.getParameters().put('campaignItemId', boi2.Campaign_item_To_ORder__r.Id);
            
         //   System.assertEquals(ctrl.boiWithoutCreativityGroupedByCampaignId.get(boi2.Campaign_item_To_ORder__r.Id) != null, true);
            
            
          //  ctrl.saveQuantityCIProposal();
            
            List<Boutique_Order_Item__c> boiOrderedByFixtureName = ctrl.boiOrderedByFixtureName;
            pgRef.getParameters().put('ffId', boi3.Permanent_Fixture_And_Furniture__r.Id);

            System.assertEquals(ctrl.boiForFFGroupedByFFId.get(boi3.Permanent_Fixture_And_Furniture__r.Id) != null, true);
        


            ctrl.saveQuantityFFProposal();

            List<SelectOption> viewOptions = ctrl.viewOptions;
                    
            ctrl.refresh();
        }
            
        Test.stopTest();
            
    }
}
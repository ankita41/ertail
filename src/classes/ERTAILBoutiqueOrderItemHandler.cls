/*****
*   @Class      :   ERTAILBoutiqueOrderItemHandler.cls
*   @Description:   Static methods called by triggers.
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   TPA             05-Aug-2015     Fix issue with new BOI being created if the Boutique spaces are created after MPM has proposed to order a Campaign Item for a space that initially doesn't exist in the boutique.                
*   TPA             02-Oct-2015     Early escape from DeleteBoutiqueOrderItemsAfterCampaignBoutiqueScopeChange if the scope has not changed    
    Ankita Singhal  2 Apr 2017      Ertail Enhancement 2017            
*****/
public with sharing class ERTAILBoutiqueOrderItemHandler {
    public class CustomException extends Exception{}
   
    public static void UpdateFieldsBeforeUpdate(Map<Id, Boutique_Order_Item__c> oldMap, Map<Id, Boutique_Order_Item__c> newMap){
        // extract the boi where the Campaign_Item_to_Order__c has changed and the Id of the new Campaign Item => to update costs
        Set<Id> campaignItemIds = new Set<Id>();
        List<Boutique_Order_Item__c> boiWithUpdatedCampaignItem = new List<Boutique_Order_Item__c>();
        
        //throw new CustomException('In Trigger');
        Boutique_Order_Item__c newBOI;
        Boutique_Order_Item__c oldBOI;
        for (Id btqOrderItemId : newMap.keySet()){
            newBOI = newMap.get(btqOrderItemId);
            oldBOI = oldMap.get(btqOrderItemId);
            
            // If the recommended Campaign Item has changed, HQPM Campaign Item is updated if necessary
            if (newBOI.Recommended_Campaign_Item__c != oldBOI.Recommended_Campaign_Item__c){
                // If the HQPM recommendation is equal to the old recommendation and the new HQPM recommendation is equal to the old HQPM recommendation, the new HQPM recommendation is updated with the new recommendation
                if (oldBOI.Recommended_Campaign_Item__c == newBOI.HQPM_Campaign_Item__c && newBOI.HQPM_Campaign_Item__c == oldBOI.HQPM_Campaign_Item__c){
                    newBOI.HQPM_Campaign_Item__c = newBOI.Recommended_Campaign_Item__c;
                }
            }
            // If the recommended Quantity has changed, HQPM Quantity is updated if necessary
            if (newBOI.Recommended_Quantity__c != oldBOI.Recommended_Quantity__c){
                // If the HQPM Quantity is equal to the old recommendation and the new HQPM Quantity is equal to the old HQPM Quantity, the new HQPM Quantity is updated with the new recommendation
                if (oldBOI.Recommended_Quantity__c == newBOI.HQPM_Quantity__c && newBOI.HQPM_Quantity__c == oldBOI.HQPM_Quantity__c){
                    newBOI.HQPM_Quantity__c = newBOI.Recommended_Quantity__c;
                }
            }
            
            // If the HQPM Campaign Item has changed, MPM Campaign Item is updated if necessary
            if (newBOI.HQPM_Campaign_Item__c != oldBOI.HQPM_Campaign_Item__c){
                // If the MPM proposal is not approved
                if (!'Approved'.equals(newBOI.HQPM_Decision__c)){
                    // If the MPM recommendation is equal to the old HQPM recommendation and the new MPM recommendation is equal to the old MPM recommendation, the new MPM recommendation is updated with the new HQPM recommendation
                    if (oldBOI.HQPM_Campaign_Item__c == newBOI.MPM_Campaign_Item__c && newBOI.MPM_Campaign_Item__c == oldBOI.MPM_Campaign_Item__c){
                        newBOI.MPM_Campaign_Item__c = newBOI.HQPM_Campaign_Item__c;
                    }
                    
                    // the final order is updated with the HQPM recommendation
                    newBOI.Campaign_Item_to_order__c = newBOI.HQPM_Campaign_Item__c;
                }
            }
            // If the HQPM Quantity has changed, MPM Quantity is updated if necessary
            if (newBOI.HQPM_Quantity__c != oldBOI.HQPM_Quantity__c){
                // If the MPM proposal is not Approved
                if (!'Approved'.equals(newBOI.HQPM_Decision__c)){
                    // If the MPM Quantity is equal to the old HQPM recommendation and the new MPM Quantity is equal to the old MPM Quantity, the new MPM Quantity is updated with the new HQPM recommendation
                    if (oldBOI.HQPM_Quantity__c == newBOI.MPM_Quantity__c && newBOI.MPM_Quantity__c == oldBOI.MPM_Quantity__c){
                        newBOI.MPM_Quantity__c = newBOI.HQPM_Quantity__c;
                    }
                    newBOI.Quantity_to_order__c = newBOI.HQPM_Quantity__c;
                }
            }
                        
            // If the HQPM decision is changed to Approved
            if(newBOI.HQPM_Decision__c  != oldBOI.HQPM_Decision__c && 'Approved'.equals(newBOI.HQPM_Decision__c)) {
                if ('Yes'.equals(newBOI.Has_Creativity__c))
                    newBOI.Campaign_Item_to_order__c = newBOI.MPM_Campaign_Item__c;
                if ('Yes'.equals(newBOI.Has_Quantity__c))
                    newBOI.Quantity_to_Order__c = newBOI.MPM_Quantity__c;
            }
            
            
            // if Quantity_Step_Picklist__c is changed to 30 (MPM Proposal Approval) and MPM_Quantity__c == HQPM_Quantity__c then Quantity_Step_Picklist__c is moved to 40
            if (oldBOI.Quantity_Step_Picklist__c != newBOI.Quantity_Step_Picklist__c 
                && '30'.equals(newBOI.Quantity_Step_Picklist__c)
                && newBOI.HQPM_Quantity__c == newBOI.MPM_Quantity__c){
                    newBOI.Quantity_Step_Picklist__c = '40';
            }
            
            // if Creativity_Step_Picklist__c is changed to 30 (MPM Proposal Approval) and HQPM_Campaign_Item__c == HQPM_Campaign_Item__c then Creativity_Step_Picklist__c is moved to 40
            if (oldBOI.Creativity_Step_Picklist__c != newBOI.Creativity_Step_Picklist__c 
                && '30'.equals(newBOI.Creativity_Step_Picklist__c)
                && newBOI.HQPM_Campaign_Item__c == newBOI.MPM_Campaign_Item__c){
                    newBOI.Creativity_Step_Picklist__c = '40';
            }
            
            // 
            if (newBOI.Campaign_Item_to_order__c != oldBOI.Campaign_Item_to_order__c){
                if (newBOI.Campaign_Item_to_order__c == null){
                    newBOI.Unit_Cost__c = null;
                    newBOI.OP_unit_max_cost_Eur__c = null;
                }else{
                    campaignItemIds.add(newBOI.Campaign_Item_to_order__c);
                    boiWithUpdatedCampaignItem.add(newBOI);
                }
            }
        }
        
        // set the OP_unit_Max_cost field
        if (campaignItemIds.size() > 0){
            Map<Id, Campaign_Item__c> ciMap = new Map<Id, Campaign_Item__c>([Select Id, Max_Cost_EUR__c, Actual_Cost_EUR__c from Campaign_Item__c where Id in :campaignItemIds]);
            
            for (Boutique_Order_Item__c boi : boiWithUpdatedCampaignItem){
                if (boi.Campaign_Item_to_order__c != null && ciMap.containsKey(boi.Campaign_Item_to_order__c)){
                    boi.Unit_Cost__c = ciMap.get(boi.Campaign_Item_to_order__c).Actual_Cost_EUR__c;
                    boi.OP_unit_max_cost_Eur__c = ciMap.get(boi.Campaign_Item_to_order__c).Max_Cost_EUR__c;
                }
            }
        }    
    }
    
    public static void UpdateBoutiqueOrderItemUnitCostAfterCampaignItemUpdate(Map<Id, Campaign_Item__c> oldMap, Map<Id, Campaign_Item__c> newMap){
        Set<Id> campaignItemsUpdated = new Set<Id>();
        
        for (Id campaignItemId : newMap.keySet()){
            if (
                (oldMap.get(campaignItemId).Actual_Cost_EUR__c != newMap.get(campaignItemId).Actual_Cost_EUR__c)
                || (oldMap.get(campaignItemId).Max_Cost_EUR__c != newMap.get(campaignItemId).Max_Cost_EUR__c)){
                campaignItemsUpdated.add(campaignItemId);
            }
        }
        
        if (campaignItemsUpdated.size() > 0){
            // request BOI linked to these Campaign Items
            List<Boutique_Order_Item__c> boiToUpdate = [Select Id, Unit_Cost__c, OP_unit_max_cost_Eur__c, Campaign_Item_to_order__c from Boutique_Order_Item__c where Campaign_Item_to_order__c in :campaignItemsUpdated];
            for(Boutique_Order_Item__c boi : boiToUpdate){
                Campaign_Item__c ci = newMap.get(boi.Campaign_Item_to_order__c);
                boi.Unit_Cost__c = ci.Actual_Cost_EUR__c;
                boi.OP_unit_max_cost_Eur__c = ci.Max_Cost_EUR__c;
            }
            
            if (boiToUpdate.size() > 0)
                update boiToUpdate;
        }
    }
    
    public static void InitFieldsBeforeInsert(List<Boutique_Order_Item__c> newList){
        // extract the boi linked to a Campaign Item and the ids of the campaign items to request
        Set<Id> campaignItemIds = new Set<Id>();
        Set<Id> ffIds = new Set<Id>();
        
        for (Boutique_Order_Item__c boi : newList){
            // Set HQPM Quantity
            boi.HQPM_Quantity__c = (boi.HQPM_Quantity__c == null ? boi.Recommended_Quantity__c : boi.HQPM_Quantity__c);
            // Set MPM Quantity
            boi.MPM_Quantity__c = (boi.MPM_Quantity__c == null ? boi.HQPM_Quantity__c : boi.MPM_Quantity__c);
            // Set Final Order
            boi.Quantity_to_Order__c = (boi.Quantity_to_Order__c == null ? boi.HQPM_Quantity__c : boi.Quantity_to_Order__c);
            // Set HQPM Campaign_Item
            boi.HQPM_Campaign_Item__c = (boi.HQPM_Campaign_Item__c == null ? boi.Recommended_Campaign_Item__c : boi.HQPM_Campaign_Item__c);
            // Set MPM Quantity
            boi.MPM_Campaign_Item__c = (boi.MPM_Campaign_Item__c == null ? boi.HQPM_Campaign_Item__c : boi.MPM_Campaign_Item__c);
            // Set Final Order
            boi.Campaign_Item_to_order__c = (boi.Campaign_Item_to_order__c == null ? boi.HQPM_Campaign_Item__c : boi.Campaign_Item_to_order__c);
        
            if (boi.Campaign_Item_to_order__c != null)
                campaignItemIds.add(boi.Campaign_Item_to_order__c);
            if (boi.Permanent_Fixture_And_Furniture__c != null)
                ffIds.add(boi.Permanent_Fixture_And_Furniture__c);
        }
                
        if (ffIds.size() > 0){
            Map<Id, Permanent_Fixture_And_Furniture__c> ffMap = new Map<Id, Permanent_Fixture_And_Furniture__c>([Select Id, Unit_Cost__c from Permanent_Fixture_And_Furniture__c where Id in :ffIds]);
            for (Boutique_Order_Item__c boi : newList){
                if(boi.Permanent_Fixture_And_Furniture__c != null && ffMap.containsKey(boi.Permanent_Fixture_And_Furniture__c)){
                    boi.Unit_Cost__c = ffMap.get(boi.Permanent_Fixture_And_Furniture__c).Unit_Cost__c;
                }
            }
        }

        // set the OP_unit_Max_cost field
        if (campaignItemIds.size() > 0){
            Map<Id, Campaign_Item__c> ciMap = new Map<Id, Campaign_Item__c>([Select Id, Max_Cost_EUR__c, Actual_Cost_EUR__c from Campaign_Item__c where Id in :campaignItemIds]);
            
            for (Boutique_Order_Item__c boi : newList){
                if (boi.Campaign_Item_to_order__c != null && ciMap.containsKey(boi.Campaign_Item_to_order__c)){
                    boi.Unit_Cost__c = ciMap.get(boi.Campaign_Item_to_order__c).Actual_Cost_EUR__c;
                    boi.OP_unit_max_cost_Eur__c = ciMap.get(boi.Campaign_Item_to_order__c).Max_Cost_EUR__c;
                }
            }
        } 
    }
    
    // Delete all the Boutique Order Items linked to a space that shouldn't be linked anymore to the campaign due to a scope change at boutique level
    public static void DeleteBoutiqueOrderItemsAfterCampaignBoutiqueScopeChange(Map<Id, Campaign_Boutique__c> oldMap, Map<Id, Campaign_Boutique__c> newMap){
        Set<Id> campaignBtqsToCheck = new Set<Id>();
        for (Id campaignBtqId : newMap.keySet()){
            if (oldMap.get(campaignBtqId).Scope_Limitation__c != newMap.get(campaignBtqId).Scope_Limitation__c && newMap.get(campaignBtqId).Scope_Limitation__c != null){
                campaignBtqsToCheck.add(campaignBtqId);
            }
        }
        
        // If there are no scope change, return
        if (campaignBtqsToCheck.size() == 0)
            return;
        
        // Get the CampaignBtqsOrdered by Campaign Id
        Map<Id, List<Campaign_Boutique__c>> campaignBtqsOrderedByCampaignId = new Map<Id, List<Campaign_Boutique__c>>();
        for (Campaign_Boutique__c cb : [Select Id, Campaign_Market__r.Campaign__c from Campaign_Boutique__c where Id in :campaignBtqsToCheck]){
            if (!campaignBtqsOrderedByCampaignId.containsKey(cb.Campaign_Market__r.Campaign__c))
                campaignBtqsOrderedByCampaignId.put(cb.Campaign_Market__r.Campaign__c, new list<Campaign_Boutique__c>());
            campaignBtqsOrderedByCampaignId.get(cb.Campaign_Market__r.Campaign__c).add(cb);
        }
        
        // Request the Topics for the campaigns
        Map<Id, Set<Id>> topicsOrganisedByCampaign = new Map<Id, Set<Id>>();
        for (Campaign_Topic__c ct : [Select c.Topic__c,c.Topic__r.name, c.Campaign__c From Campaign_Topic__c c where c.Campaign__c = :campaignBtqsOrderedByCampaignId.keySet()]){
            if (!topicsOrganisedByCampaign.containsKey(ct.Campaign__c))
                topicsOrganisedByCampaign.put(ct.Campaign__c, new Set<Id>());
            topicsOrganisedByCampaign.get(ct.Campaign__c).add(ct.Topic__c);
        }
        
        Set<Id> spacesNotLinkedToAnyTopic = new Set<Id>();
        Set<Id> spacesLinkedToDisplayTopic = new Set<Id>(); 
        Map<Id, Set<Id>> spacesLinkedToASelectedTopicOrganisedByCampaign = new Map<Id, Set<Id>>();  
        // Get the full list of Spaces 
        // Extract the list of Spaces that are not linked to any topics or that are linked to a topic part of the campaign
        for(Space__c space : [Select s.Name, s.Id, (Select Space__c, Topic__c ,Topic__r.name From Space_Topics__r) From Space__c s]){
            if (space.Space_Topics__r == null || space.Space_Topics__r.size() == 0){
                spacesNotLinkedToAnyTopic.add(space.Id);
            }else{
                for(Id campaignId : topicsOrganisedByCampaign.keySet()){
                    if (!spacesLinkedToASelectedTopicOrganisedByCampaign.containsKey(campaignId)){
                        spacesLinkedToASelectedTopicOrganisedByCampaign.put(campaignId, new Set<Id>());
                    }
                    for (Space_Topic__c st : space.Space_Topics__r){
                        if (topicsOrganisedByCampaign.get(campaignId).contains(st.Topic__c)){
                            spacesLinkedToASelectedTopicOrganisedByCampaign.get(campaignId).add(space.Id); 
                            //Start - req 13
                            if(st.Topic__r.Name=='Display'){
                              spacesLinkedToDisplayTopic.add(Space.id);
                            }
                            //End 
                        }
                        
                    }
                }
            }
        }
        
        List<Boutique_Order_Item__c> btqOrderItemsToDelete = new List<Boutique_Order_Item__c>();
        // Get the list of existing BtqOrderItems for the boutiques where the scope has changed and check if the BOI should exist
        for (Boutique_Order_Item__c  boi : [Select Id, Campaign_Item_to_order__r.Campaign__c, Campaign_Item_to_order__r.Space__r.RecordTypeId, Campaign_Item_to_order__r.Space__c, Campaign_Boutique__c from Boutique_Order_Item__c where Campaign_Boutique__c in :campaignBtqsToCheck]){
            if (!ERTAILCampaignHandler.IsSpaceAvailableForCampaignBoutique(newMap.get(boi.Campaign_Boutique__c).Scope_Limitation__c, boi.Campaign_Item_to_order__r.Space__r, spacesNotLinkedToAnyTopic, spacesLinkedToASelectedTopicOrganisedByCampaign.get(boi.Campaign_Item_to_order__r.Campaign__c),spacesLinkedToDisplayTopic)){
                btqOrderItemsToDelete.add(boi);
            }
        }
        
        if (btqOrderItemsToDelete.size() > 0)
            delete btqOrderItemsToDelete;
    }
}
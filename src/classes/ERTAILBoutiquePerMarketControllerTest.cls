/*
*   @Class      :   ERTAILBoutiquePerMarketControllerTest.cls
*   @Description:   Test methods for class ERTAILBoutiquePerMarketController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILBoutiquePerMarketControllerTest {
    static testMethod void myUnitTest() {
    	Id testCampaignId = ERTAILTestHelper.createCampaignMME();

		Campaign_Market__c cmpMkt = [select Id, RTCOMMarket__c from Campaign_Market__c where Campaign__r.Id =: testCampaignId];

		RTCOMMarket__c mkt = [select Id from RTCOMMarket__c where Id = :cmpMkt.RTCOMMarket__c];

		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(mkt);
    
    	ERTAILBoutiquePerMarketController ctrl = new ERTAILBoutiquePerMarketController(stdCtrl);
    
    	ctrl.filter();
    	
    	List<RTCOMBoutique__c> BoutiqueList = ctrl.BoutiqueList;

		List<SelectOption> typologyOptions = ctrl.typologyOptions;
    
    	Boolean bobo = ERTAILBoutiquePerMarketController.isHQPM && ERTAILBoutiquePerMarketController.isCreativeAgency;
    	
    	ctrl.filterBoutiques();
    }
}
/*****
*   @Class      :   ERTAILBoutiqueSpaceHandler.cls
*   @Description:   Static methods called by triggers.
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILBoutiqueSpaceHandler {

    public class CustomException extends Exception {}

    public static void  CorrectCreativityOrderAfterBoutiqueSpaceACD(List<Boutique_Space__c> bsList){
        Map<Id, List<Boutique_Space__c>> existingBsWithCreativityGroupedByBtqId = new Map<Id, List<Boutique_Space__c>>();
        
        // Extract the set of affected boutiques
        for(Boutique_Space__c bs : bsList){
            if (bs.Creativity_Order__c != null){
                if(!existingBsWithCreativityGroupedByBtqId.containsKey(bs.RTCOMBoutique__c)){
                    existingBsWithCreativityGroupedByBtqId.put(bs.RTCOMBoutique__c, new List<Boutique_Space__c>());
                }
            }
        }
        
        if (existingBsWithCreativityGroupedByBtqId.size() == 0)
            return;
            
        // Request all the Boutique Spaces with a Creativity_Order__c value for the affected Boutiques
        for(Boutique_Space__c bs : [Select Id, Creativity_Order__c, RTCOMBoutique__c 
                                    from Boutique_Space__c 
                                    where RTCOMBoutique__c in :existingBsWithCreativityGroupedByBtqId.keySet() 
                                        and Excluded_for_future_campaigns__c = false 
                                        and Creativity_Order__c != null 
                                    order by Creativity_Order__c, CreatedDate desc, LastModifiedDate desc]){
            existingBsWithCreativityGroupedByBtqId.get(bs.RTCOMBoutique__c).add(bs);        
        }
        
        // Go through all the Boutiques and make sure the Boutique spaces have a relevant creativity order
        List<Boutique_Space__c> bsToUpdate = new List<Boutique_Space__c>();
        for (Id btqId : existingBsWithCreativityGroupedByBtqId.keySet()){
            List<Boutique_Space__c> bsList2 = existingBsWithCreativityGroupedByBtqId.get(btqId);
            Integer creativity = 1;
            for (Boutique_Space__c bs : bsList2){
                if (bs.Creativity_Order__c != creativity){
                    bs.Creativity_Order__c = creativity;
                    bsToUpdate.add(bs);
                }
                creativity++;
            }
        }
        
        if (bsToUpdate.size() > 0)
            update bsToUpdate;
    }
    
    public static void  AddCommentInBoutiqueChatterFeedAfterDelete(List<Boutique_Space__c> bsList){
        List<FeedItem> fiList = new List<FeedItem>();
        for(Boutique_Space__c bs : bsList){
            fiList.add(
                new FeedItem(
                    ParentId = bs.RTCOMBoutique__c,
                    Body = UserInfo.getName() + ' deleted the Boutique Space ' + bs.Name
                )
            );
        }       
        insert fiList;
    }
    
    public static void  AddCommentInBoutiqueChatterFeedAfterUndelete(List<Boutique_Space__c> bsList){
        List<FeedItem> fiList = new List<FeedItem>();
        for(Boutique_Space__c bs : bsList){
            fiList.add(
                new FeedItem(
                    ParentId = bs.RTCOMBoutique__c,
                    Body = UserInfo.getName() + ' undeleted the Boutique Space ' + bs.Name
                )
            );
        }       
        insert fiList;
    }
     
    
}
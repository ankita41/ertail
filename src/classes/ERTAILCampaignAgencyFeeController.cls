/*
*   @Class      :   ERTAILCampaignAgencyFeeController.cls
*   @Description:   Controller of the ERTAILCampaignAgencyFee.component
*   @Author     :   Jacky Uy
*   @Created    :   20 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        	Description
*   ------------------------------------------------------------------------------------------------------------------------
*	Jacky Uy		30 JAN 2015		Test Coverage at 94%
*                         
*/

public with sharing class ERTAILCampaignAgencyFeeController extends ERTailMasterHandler {
	public static Boolean isHQPM { get{ return ERTailMasterHelper.isHQPM; } }
	public static Boolean isProductionAgency { get{ return ERTailMasterHelper.isProductionAgency; } }
	
	public Id campaignId {
		get;
		set{
			campaign = [SELECT Id, Name, Scope__c, Launch_Date__c, End_Date__c, Description__c FROM Campaign__c WHERE Id = :value];
		}
	}
	
	public ERTAILCampaignAgencyFeeController(){}
	
	private void initCampItems(){
		campAgencyFees = null;
		//mapSelectedItems = null;
	}
	
	/*public Map<Id,Boolean> mapSelectedItems {		
		get{
			if(mapSelectedItems==null){
				mapSelectedItems = new Map<Id,Boolean>();
				for(ERTAILCampaign_Agency_Fee__c caf : campAgencyFees){
					mapSelectedItems.put(caf.Id, false);
				}
			}
			return mapSelectedItems;
		} set;
	}*/
	
	public Map<Id,Integer> mapItemIndex {		
		get{
			if(mapItemIndex==null){
				mapItemIndex = new Map<Id,Integer>();
				for(Integer i=0; i<campAgencyFees.size(); i++){
					mapItemIndex.put(campAgencyFees[i].Id, i);
				}
			}
			return mapItemIndex;
		} set;
	}
	
	public void updateFee(){
		Integer index = Integer.valueOf(Apexpages.currentPage().getParameters().get('index'));
		update campAgencyFees[index];
		initCampItems();
	}

	public void submitForApproval(){
		for(ERTAILCampaign_Agency_Fee__c caf : campAgencyFees){
			if(caf.Actual_fee_Eur__c==null || caf.Actual_fee_Eur__c==0){
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'All Fees must be populated before submitting for Confirmation.')); 
				initCampItems();		
			}
			
			caf.Fee_Status_picklist__c = '20';
		}
		update campAgencyFees;
		initCampItems();
		
		//CHATTER POST
		if(!prodAgency.isEmpty()){
			insert new FeedItem(
	        	ParentId = prodAgency[0].Id,
	        	Body = 'Campaign Agency Fees submitted for Confirmation \nBy: ' + UserInfo.getName() + '\nCampaign: ' + campaign.Name
	        );
		}
	}
	
	public void approveAll(){
		for(ERTAILCampaign_Agency_Fee__c caf : campAgencyFees){
			caf.Fee_Status_picklist__c = '30';
		}
		update campAgencyFees;
		initCampItems();
		
		//CHATTER POST
		if(!prodAgency.isEmpty()){
			insert new FeedItem(
	        	ParentId = prodAgency[0].Id,
	        	Body = 'Campaign Agency Fees Confirmed \nBy: ' + UserInfo.getName() + '\nCampaign: ' + campaign.Name
	        );
		}
	}
	
	public void rejectAll(){
		for(ERTAILCampaign_Agency_Fee__c caf : campAgencyFees){
			caf.Fee_Status_picklist__c = '10';
		}
		update campAgencyFees;
		initCampItems();
	}
	
	public class CustomException extends Exception {
		//throw new CustomException();
	}
}
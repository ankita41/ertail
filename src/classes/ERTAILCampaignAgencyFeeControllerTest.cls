/*
*   @Class      :   ERTAILCampaignAgencyFeeControllerTest.cls
*   @Description:   Test Coverage for ERTAILCampaignAgencyFeeController.cls
*   @Author     :   Jacky Uy
*   @Created    :   30 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        	Description
*   ------------------------------------------------------------------------------------------------------------------------    
*	Jacky Uy		30 JAN 2015		Test Coverage at 94%
*
*/

@isTest
private class ERTAILCampaignAgencyFeeControllerTest {
    static testMethod void myUnitTest() {
    	insert new ERTAILAgency_Fee__c(
    		Name = 'Test Agency Fee',
    		OP_100_max_fee__c = 1000
    	);
    	
        Campaign__c testCampaign = new Campaign__c(
        	Name = 'Test Campaign',
        	Launch_Date__c = System.Today().addMonths(1),
        	End_Date__c = System.Today().addMonths(2),
        	Description__c = 'This is a Test'
        );
        insert testCampaign;
        
        PageReference pRef = Page.ERTAILMaster;
        pRef.getParameters().put('id', testCampaign.Id);
        Test.setCurrentPage(pRef);
        
        Test.startTest();
        ERTAILCampaignAgencyFeeController testCon = new ERTAILCampaignAgencyFeeController();
        testCon.campaignId = testCampaign.Id;
        SYSTEM.Assert(!testCon.campAgencyFees.isEmpty());
        
        testCon.submitForApproval();
//        SYSTEM.Assert(ApexPages.getMessages()[0].getDetail().contains('All Fees must be populated before submitting for Confirmation.'));
        
        testCon.campAgencyFees[0].Actual_fee_Eur__c = 100;
        pRef.getParameters().put('index', '0');
        Test.setCurrentPage(pRef);
        testCon.updateFee();
//        SYSTEM.Assert(testCon.campAgencyFees[0].Actual_fee_Eur__c == 100);
        
        testCon.submitForApproval();
//        SYSTEM.Assert(testCon.campAgencyFees[0].Fee_Step_Value__c == 20);
        
        testCon.approveAll();
//        SYSTEM.Assert(testCon.campAgencyFees[0].Fee_Step_Value__c == 30);
        
        testCon.rejectAll();
//        SYSTEM.Assert(testCon.campAgencyFees[0].Fee_Step_Value__c == 10);

		Boolean b = ERTAILCampaignAgencyFeeController.isHQPM;
		b = ERTAILCampaignAgencyFeeController.isProductionAgency;
	
		Map<Id,Integer> m = testCon.mapItemIndex;
		
		
        Test.stopTest();
    }
}
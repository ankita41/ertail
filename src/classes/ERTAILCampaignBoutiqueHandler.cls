/*****
*   @Class      :   ERTAILCampaignBoutiqueHandler.cls
*   @Description:   Static methods called by triggers.
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignBoutiqueHandler {
   
	public static void UpdateCampaignBoutiquesDatesAndScopeAfterCampaignMarketUpdate(Map<Id, Campaign_Market__c> oldMap, Map<Id, Campaign_Market__c> newMap){
		Set<Id> marketsWithStartDateChanged = new Set<Id>();
		Set<Id> marketsWithEndDateChanged = new Set<Id>();
		Set<Id> marketsWithScopeChanged = new Set<Id>();
		
		for(Id key : newMap.keySet()){
			if (oldMap.get(key).End_Date__c != newMap.get(key).End_Date__c){
				marketsWithEndDateChanged.add(key);
			}
			if(oldMap.get(key).Launch_Date__c != newMap.get(key).Launch_Date__c){
				marketsWithStartDateChanged.add(key);
			}
			if(oldMap.get(key).Scope_Limitation__c != newMap.get(key).Scope_Limitation__c){
				marketsWithScopeChanged.add(key);
			}
		}
   
   		Set<Id> allMarketChanged = new Set<Id>();
   		allMarketChanged.addAll(marketsWithStartDateChanged);
   		allMarketChanged.addAll(marketsWithEndDateChanged);
   		allMarketChanged.addAll(marketsWithScopeChanged);
   		
   		if (allMarketChanged.size() == 0)
   			return;
   		
   		// Request all the Campaign Boutiques linked to the updated Campaign Markets
   		List<Campaign_Boutique__c> cbList = [Select Id, Launch_Date__c, End_Date__c, Scope_Limitation__c, Campaign_Market__c from Campaign_Boutique__c where Campaign_Market__c in :allMarketChanged];
   		
   		if (cbList.size() == 0)
   			return;
   		
   		for(Campaign_Boutique__c cb : cbList){
   			if (marketsWithStartDateChanged.contains(cb.Campaign_Market__c)){
   				cb.Launch_Date__c = newMap.get(cb.Campaign_Market__c).Launch_Date__c;
   			}
   			if (marketsWithEndDateChanged.contains(cb.Campaign_Market__c)){
   				cb.End_Date__c = newMap.get(cb.Campaign_Market__c).End_Date__c;
   			}
   			if (marketsWithScopeChanged.contains(cb.Campaign_Market__c)){
   				cb.Scope_Limitation__c = newMap.get(cb.Campaign_Market__c).Scope_Limitation__c;
   			}
   		}
   		
   		update cbList;
	}
}
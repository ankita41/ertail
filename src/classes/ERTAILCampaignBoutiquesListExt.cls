/*****
*   @Class      :   ERTAILBoutiqueDetailsExt.cls
*   @Description:   Extension developed for ERTAILCampaignBoutiqueSelection.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
    Ankita         2 Apr 2017       Ertail Enhancements 2017
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignBoutiquesListExt {
    public class CustomException extends Exception {}

    public static String STATUS_PROPOSAL { get { return 'Proposal'; } }
    public static String STATUS_PENDINGAPPROVAL { get { return 'Pending Approval'; } }
    public static String STATUS_REJECTED { get { return 'Rejected'; } }
    public static String MPM_PROPOSAL_INHERIT { get { return ERTAILMasterHelper.MPM_PROPOSAL_INHERIT; } }
    public Boolean DisplayError{ get; set; }
    public String errormsg{ get; set; }
    
    public static String FIELD_LAUNCHDATE { get { return 'LaunchDate'; } }
    public static String FIELD_ENDDATE { get { return 'EndDate'; } }
    public static String FIELD_SCOPE { get { return 'Scope'; } }        

    public Campaign__c campaign { get; private set;}
    
    public Campaign_Market__c campaignMarket { get; private set;}
    public boolean checkDisplay{get;set;}
    public Boolean IsHQPMorAdmin { get { return IsHQPM || isAdmin;} }
    public Boolean IsMPMorAdmin { get { return IsMPM || isAdmin;} }
    public Boolean IsHQPM { get { return ERTailMasterHelper.isHQPM;} }
    public Boolean IsMPM { get { return ERTailMasterHelper.isMPM || ERTailMasterHelper.isAgent;} }
    public Boolean isAdmin { get{ return ERTailMasterHelper.isAdmin; } }
    
    public String updatedField {get; set;}
    public String decision {get; set;}
    
    public String errorMessage { public get; private set;}
    
    public static Id cmCampaignRTId = Schema.SObjectType.Campaign_Market__c.getRecordTypeInfosByName().get('Campaign').getRecordTypeId();

    private Set<Id> SelectedBoutiqueIdsSet{get;set;}

    public PageReference redirectToEditPage(){
        PageReference pageRef = null;
        if (CampaignMarket.RecordTypeId != cmCampaignRTId){
            pageRef = new ApexPages.StandardController(campaignMarket).edit();
            pageRef.getParameters().put('retURL', '/' + campaignMarket.Id);
            pageRef.getParameters().put('nooverride', '1');
        }
        return pageRef;
    }

    private String convertDateToString(Date dt){
        if(dt == null){
            return '';
        }
        Date launchDate = dt;
        Datetime launchDatetime = datetime.newInstance(launchDate.year(), launchDate.month(), launchDate.day());
        return launchDatetime.format('dd MMM yyyy');
    }

    // Constructor
    public ERTAILCampaignBoutiquesListExt(ApexPages.StandardController stdController) {
        this.campaignMarket = (Campaign_Market__c)stdController.getRecord();
        initialMarketScopeLimitation = campaignMarket.Scope_Limitation__c;
        initialMarketLaunchDate = campaignMarket.Launch_Date__c;
        initialMarketEndDate = campaignMarket.End_Date__c;
        Errormsg = '';
        system.debug('Class Camp Market::'+campaignMarket.campaign__c);
         List<Campaign_Topic__c> TopicsSelected = 
             new List<Campaign_Topic__c>([Select Id , Name , campaign__c,topic__r.Name 
                                          from campaign_Topic__c where Campaign__c =: campaignMarket.campaign__c]);
         //Start ---Req 13
         checkDisplay=false;
            for(Campaign_Topic__c ct: TopicsSelected ){
                if(ct.topic__r.Name=='Display'){
                  checkDisplay= true;
                  break;
                  }
            }
        //End
        initSelectedBoutiques();
    }
    
// ********************************
// Changes at Campaign Market level
// ********************************

    private String initialMarketScopeLimitation;
    private Datetime initialMarketLaunchDate;
    private Datetime initialMarketEndDate;
    
    public Boolean CanEditMarketProposal{
        get{
            return isMPMorAdmin && !IsMPMProposalSubmitted && !IsMPMProposalRejected;
        }
    }
    
    public Boolean IsMPMProposalRejected{
        get{
            return STATUS_REJECTED.equals(campaignMarket.MPM_Proposal_Status__c);
        }
    }
    
    public Boolean IsMPMProposal{
        get{
            return STATUS_PROPOSAL.equals(campaignMarket.MPM_Proposal_Status__c);
        }
    }
    
    public Boolean IsMPMProposalSubmitted{
        get{
            return STATUS_PENDINGAPPROVAL.equals(campaignMarket.MPM_Proposal_Status__c);
        }
    }   

    public void updateMarketScopeLimitation(){
        //Start ---req 13
        if(campaignMarket.Scope_Limitation__c==ERTAILMasterHelper.SCOPE_INSTOREDISPLAY && CheckDisplay==false){
                displayError=true; 
                errormsg ='You are not allowed to select this scope limitation as display topic was not selected for this campaign';
                campaignMarket.Scope_Limitation__c=initialMarketScopeLimitation ;
             }
         else{    
         //End --req 13
            updateMarket('\nScope Limitation : ' + (initialMarketScopeLimitation == null ? '--None--' : initialMarketScopeLimitation) + ' => ' + (campaignMarket.Scope_Limitation__c == null ? '--None--' : campaignMarket.Scope_Limitation__c));
        }
        resetSelectedBoutiques();
    }
    
    public void updateMarketLaunchDate(){
        try{
            updateMarket('\nLaunch Date : ' + initialMarketLaunchDate.format('dd MMM yyyy') + ' => ' + convertDateToString(campaignMarket.Launch_Date__c));
            resetSelectedBoutiques();
        }
        catch(DMLException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
        }
    }
    
    public void updateMarketEndDate(){
        try{
            updateMarket('\nEnd Date : ' + initialMarketEndDate.format('dd MMM yyyy') + ' => ' + convertDateToString(campaignMarket.End_Date__c));  
            resetSelectedBoutiques();
        }
        catch(DMLException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
        }   
    }
    
    // Called when HQPM changes Campaign Market details
    private void updateMarket(String chatterMessage){
        // Prepare message for Campaign Market Chatter
        chatterMessage = 'HQPM Changed Campaign Market details'
                                + '\nCampaign : '+ campaignMarket.Name
                                + chatterMessage;
    
        // Post message in Campaign Market Chatter
        ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);

        // Update the Campaign Market   
        update campaignMarket;

        // Reset Initial Market values
        initialMarketLaunchDate = campaignMarket.Launch_Date__c;
        initialMarketEndDate = campaignMarket.End_Date__c;
        initialMarketScopeLimitation = campaignMarket.Scope_Limitation__c;
    }

    public Boolean IsScopeProposalDifferentThanExisting{
        get{
            return (campaignMarket.MPM_Scope_Limitation_Proposal__c != MPM_PROPOSAL_INHERIT && campaignMarket.MPM_Scope_Limitation_Proposal__c != campaignMarket.Scope_Limitation__c);
        }
    }
    
    public Boolean IsLaunchDateProposalDifferentThanExisting{
        get{
            return (campaignMarket.MPM_Launch_Date_Proposal__c != null && campaignMarket.MPM_Launch_Date_Proposal__c != campaignMarket.Launch_Date__c);
        }
    }
    
    public Boolean IsEndDateProposalDifferentThanExisting{
        get{
            return (campaignMarket.MPM_End_Date_Proposal__c != null && campaignMarket.MPM_End_Date_Proposal__c != campaignMarket.End_Date__c);
        }
    }

    public Boolean isProposalDifferentThanExisting {
        get{
            return IsScopeProposalDifferentThanExisting  
                    || IsLaunchDateProposalDifferentThanExisting
                    || IsEndDateProposalDifferentThanExisting;
        }
    }

    public void updateMPMProposal(){
        if (isMPMorAdmin){
           // try{
               System.debug('@@@@beforeUpdate'+campaignMarket.MPM_Scope_Limitation_Proposal__c);
                update campaignMarket;
               System.debug('@@@@AfterUpdate'+campaignMarket.MPM_Scope_Limitation_Proposal__c);
          /*  }
            catch(DMLException e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            }*/
        }       
    }
    
    public void updateMPMProposalStatus(){
        if (isAdmin){
            update new Campaign_Market__c(Id = campaignMarket.Id
                                        , MPM_Proposal_Status__c = campaignMarket.MPM_Proposal_Status__c);
        }       
    }
    
    public void resetMarketChangesProposal(){
        if (isMPMorAdmin){
            campaignMarket.MPM_Proposal_Status__c = STATUS_PROPOSAL;
            campaignMarket.MPM_Launch_Date_Proposal__c = null;
            campaignMarket.MPM_End_Date_Proposal__c = null;
            campaignMarket.MPM_Scope_Limitation_Proposal__c = null;
            update new Campaign_Market__c(Id = campaignMarket.Id
                                        , MPM_Launch_Date_Proposal__c = campaignMarket.MPM_Launch_Date_Proposal__c
                                        , MPM_End_Date_Proposal__c = campaignMarket.MPM_End_Date_Proposal__c
                                        , MPM_Scope_Limitation_Proposal__c = campaignMarket.MPM_Scope_Limitation_Proposal__c
                                        , MPM_Proposal_Status__c = campaignMarket.MPM_Proposal_Status__c);
        }
    }
    
    public void saveMarketDecision(){
        String chatterMessage = '';
        Boolean isapproved = 'Approve'.equals(Decision);
        
        if (isapproved){
            chatterMessage += 'HQPM Approved the Campaign Market Change Proposal\nCampaign : ' + campaignMarket.Name;
        }else{
            chatterMessage += 'HQPM Rejected the Campaign Market Change Proposal\nCampaign : ' + campaignMarket.Name;
        }
        
        if (FIELD_LAUNCHDATE.equals(updatedField)){
            chatterMessage += approveLaunchDate(isapproved);
        }else if (FIELD_ENDDATE.equals(updatedField)){
            chatterMessage += approveEndDate(isapproved);
        }else if (FIELD_SCOPE.equals(updatedField)){
            chatterMessage += approveScope(isapproved);
        }
        
        updateCampaignMarket(chatterMessage);
    }
    
    
    // Called when HQPM accepts the MPM changes proposition
    public void approveMarketChangesProposal(){
        // at least one out of the three fields must have been changed to a value different than the actual campaign market values
        if(isHQPMorAdmin && isProposalDifferentThanExisting){
            String chatterMessage = 'HQPM Approved Campaign Market Changes Proposal\nCampaign : ' + campaignMarket.Name;
            
            // Update the campaign market
            if(IsLaunchDateProposalDifferentThanExisting){
                chatterMessage += approveLaunchDate(true);
            }
            if(IsEndDateProposalDifferentThanExisting){
                chatterMessage += approveEndDate(true);
            }
            if(IsScopeProposalDifferentThanExisting){
                chatterMessage += approveScope(true);
            }
            updateCampaignMarket(chatterMessage);
        }
    }
    
    private void updateCampaignMarket(String chatterMessage){
        try{
            if (!isProposalDifferentThanExisting)
                campaignMarket.MPM_Proposal_Status__c = STATUS_PROPOSAL;
            update campaignMarket;
            ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);  
            resetSelectedBoutiques();   
        }
        catch(DMLException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
        }
    }
    
    private String approveLaunchDate(boolean approved){
        String message = '\nLaunch date : ' + initialMarketLaunchDate.format('dd MMM yyyy') + ' => '  + convertDateToString(campaignMarket.MPM_Launch_Date_Proposal__c);
        if (approved)
            campaignMarket.Launch_Date__c = campaignMarket.MPM_Launch_Date_Proposal__c;
        campaignMarket.MPM_Launch_Date_Proposal__c = null;  
        return message;
    }
    
    private String approveEndDate(boolean approved){
        String message = '\nEnd date : ' + initialMarketEndDate.format('dd MMM yyyy') + ' => '  + convertDateToString(campaignMarket.MPM_End_Date_Proposal__c);
        if (approved)
            campaignMarket.End_Date__c = campaignMarket.MPM_End_Date_Proposal__c;
        campaignMarket.MPM_End_Date_Proposal__c = null;
        return message;
    }

    private String approveScope(boolean approved){
        String message = '\nScope Limitation : ' + (initialMarketScopeLimitation == null ? '--None--' : initialMarketScopeLimitation) + ' => ' + (campaignMarket.MPM_Scope_Limitation_Proposal__c == null ? '--None--' : campaignMarket.MPM_Scope_Limitation_Proposal__c);
        if (approved)
            campaignMarket.Scope_Limitation__c = campaignMarket.MPM_Scope_Limitation_Proposal__c;
        campaignMarket.MPM_Scope_Limitation_Proposal__c = MPM_PROPOSAL_INHERIT;
        return message;     
    }

    // Called when HQPM submits his change rejection
    public void rejectMarketChangesProposal(){
        // at least one out of the three fields must have been changed to a value different than the actual campaign market values
        if(isHQPMorAdmin && isProposalDifferentThanExisting){
            String chatterMessage = 'HQPM Rejected Campaign Market Changes Proposal\nCampaign : ' + campaignMarket.Name;

            // Update the campaign market
            if(IsLaunchDateProposalDifferentThanExisting){
                chatterMessage += approveLaunchDate(false);
            }
            if(IsEndDateProposalDifferentThanExisting){
                chatterMessage += approveEndDate(false);
            }
            if(IsScopeProposalDifferentThanExisting){
                chatterMessage += approveScope(false);
            }
        
            CampaignMarket.MPM_Proposal_Status__c = STATUS_PROPOSAL;
            update campaignMarket;
                                        
            ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);                                              
        
            resetSelectedBoutiques();                                           
        }       
    }
    
    public void submitMarketChangesProposal(){
        // at least one out of the three fields must have been changed to a value different than the actual campaign market values
        if(isMPMorAdmin && isProposalDifferentThanExisting){
            String chatterMessage = 'MPM Change Proposal\nCampaign : ' + campaignMarket.Name;

            // Update the campaign market
            if(IsLaunchDateProposalDifferentThanExisting){
                chatterMessage += '\nLaunch date : ' + initialMarketLaunchDate.format('dd MMM yyyy') + ' => '  + convertDateToString(campaignMarket.MPM_Launch_Date_Proposal__c);
            }
            if(IsEndDateProposalDifferentThanExisting){
                chatterMessage += '\nEnd date : ' + initialMarketEndDate.format('dd MMM yyyy') + ' => '  + convertDateToString(campaignMarket.MPM_End_Date_Proposal__c);
            }
            if(IsScopeProposalDifferentThanExisting){
                chatterMessage += '\nScope Limitation : ' + (initialMarketScopeLimitation == null ? '--None--' : initialMarketScopeLimitation) + ' => ' + (campaignMarket.MPM_Scope_Limitation_Proposal__c == null ? '--None--' : campaignMarket.MPM_Scope_Limitation_Proposal__c);
            }
            if(campaignMarket.MPM_Scope_Limitation_Proposal__c==ERTAILMasterHelper.SCOPE_INSTOREDISPLAY && CheckDisplay==false){
                displayError=true; 
                errormsg ='You are not allowed to select this scope limitation as display topic was not selected for this campaign';
                campaignMarket.MPM_Scope_Limitation_Proposal__c = initialMarketScopeLimitation;
                update campaignMarket;
             }
             else{
                CampaignMarket.MPM_Proposal_Status__c = STATUS_PENDINGAPPROVAL;
                update new Campaign_Market__c(Id = campaignMarket.Id
                                            , MPM_Proposal_Status__c = CampaignMarket.MPM_Proposal_Status__c);
    
                ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);  
            }     
            resetSelectedBoutiques();   
        }
    }
    
// ********************************
// Changes at Campaign Boutique level
// ********************************
    
    public Boolean AreThereProposals { get; private set;}
    public Boolean AreTherePendingApprovals { get; private set;}
    public Boolean AreThereRejecteds { get; private set;}
    
    public class SelectableBoutiqueWrapper{
        public SelectableBoutiqueWrapper(RTCOMBoutique__c boutique){
            this.boutique = boutique;
            this.Selected = false;
        }
        public RTCOMBoutique__c boutique { get; set; }
        public Boolean Selected { get; set; }
    }
    
    public List<SelectOption> decisionOptions {
        get{
            if (decisionOptions == null)
                decisionOptions = new List<SelectOption>{new SelectOption('No Decision', 'No Decision'), new SelectOption('Approve', 'Approve'), new SelectOption('Reject', 'Reject')};
            return decisionOptions;
        }
        private set;
    }
    
    // All RTCOMBoutiques in the Market
    public List<RTCOMBoutique__c> allBoutiques {
        get{            
            if (allBoutiques == null){
                allBoutiques = [Select Id, Name,Location__r.Delivery_mode__c,RTCOMMarket__r.Location__r.Delivery_mode__c from RTCOMBoutique__c where RTCOMMarket__c =: campaignMarket.RTCOMMarket__c and Excluded_for_future_campaigns__c = false order by Name];  // Delivery Mode Fields are added by Promila to set delivery mode from boutique by default on Manage Order Page
            }
            return allBoutiques;
        }
        set;
    }
    
    public List<Id> orderedCampaignBoutiqueIds { 
        get{
            if (orderedCampaignBoutiqueIds == null){
                initSelectedBoutiques();
            }
            return orderedCampaignBoutiqueIds;
        } 
        private set;
    }
    
    public Map<Id, Campaign_Boutique__c> selectedBoutiquesMap { 
        get{
            if (selectedBoutiquesMap == null){
                initSelectedBoutiques();
            }
            return selectedBoutiquesMap;
        }
        private set;
    }
    
    private void initSelectedBoutiques(){
        orderedCampaignBoutiqueIds = new List<Id>();
        selectedBoutiquesMap = new Map<Id, Campaign_Boutique__c>();
        SelectedBoutiqueIdsSet = new Set<Id>();
        AreThereProposals = false;
        AreTherePendingApprovals = false;
        AreThereRejecteds = false;
        for(Campaign_Boutique__c cb : [Select cb.Name
                                            , cb.RTCOMBoutique__c
                                            , cb.End_Date__c
                                            , cb.Launch_Date__c
                                            , cb.MPM_End_Date_Proposal__c
                                            , cb.MPM_Launch_Date_Proposal__c
                                            , cb.Scope_Limitation__c
                                            , cb.MPM_Scope_Limitation_Proposal__c
                                            , cb.MPM_Proposal_Comment__c
                                            , cb.MPM_Proposal_Status__c
                                            , cb.HQPM_Rejection_Comment__c
                                            , cb.Campaign_Market__c 
                                        From Campaign_Boutique__c cb 
                                        where cb.Campaign_Market__c = :campaignMarket.Id 
                                        order by RTCOMBoutique__r.Name]){
            selectedBoutiquesMap.put(cb.Id, cb);
            SelectedBoutiqueIdsSet.add(cb.RTCOMBoutique__c);
            orderedCampaignBoutiqueIds.add(cb.Id);
            AreThereProposals |= (STATUS_PROPOSAL.equals(cb.MPM_Proposal_Status__c));
            AreTherePendingApprovals |= (STATUS_PENDINGAPPROVAL.equals(cb.MPM_Proposal_Status__c));
            AreThereRejecteds |= (STATUS_REJECTED.equals(cb.MPM_Proposal_Status__c));
        }
        //throw new CustomException('AreThereProposals:' + AreThereProposals + '-AreTherePendingApprovals:' + AreTherePendingApprovals + '-AreThereRejecteds:' + AreThereRejecteds);
    }
        
    public List<Campaign_Boutique__c> selectedBoutiquesOrdered {
        get{
            List<Campaign_Boutique__c> tmpSelectedBoutiquesOrdered = new List<Campaign_Boutique__c>();
            for(Id cbId : orderedCampaignBoutiqueIds){
                tmpSelectedBoutiquesOrdered.add(selectedBoutiquesMap.get(cbId));
            }
            return tmpSelectedBoutiquesOrdered;
        }
    }
        
    // Add a boutique to the campaign
    public PageReference addBoutiqueToCampaign(){
        String chatterMessage = 'HQPM added Boutique(s) to the campaign\nCampaign : ' + campaignMarket.Name;
        List<Campaign_Boutique__c> cbToAdd = new List<Campaign_Boutique__c>();
        for(SelectableBoutiqueWrapper BoutiqueWrapper : availableBoutiques){
            if (BoutiqueWrapper.Selected){
                chatterMessage += '\nBoutique : ' + BoutiqueWrapper.Boutique.Name;
                /** Below code Added by Promila to set delivery mode from boutique location by default starts here **/
                String deliveryMode=(BoutiqueWrapper.Boutique.Location__r.Delivery_mode__c!=null?BoutiqueWrapper.Boutique.Location__r.Delivery_mode__c:BoutiqueWrapper.Boutique.RTCOMMarket__r.Location__r.Delivery_mode__c); // Added by Promila to set delivery mode from boutique by default
                if(deliveryMode!=null){
                    if(deliveryMode.containsIgnoreCase('Warehouse')){
                       deliveryMode='Warehouse Delivery';
                    }
                    else if(deliveryMode.containsIgnoreCase('Pick')){
                       deliveryMode='Pick up';
                    }
                    else if(deliveryMode.containsIgnoreCase('Boutique')){
                       deliveryMode='Boutique Delivery';
                    }
                }
                /** Below code Added by Promila to set delivery mode from boutique location by default ends here **/
                cbToAdd.add(
                    new Campaign_Boutique__c(
                        Name = BoutiqueWrapper.Boutique.Name
                        , RTCOMBoutique__c = BoutiqueWrapper.Boutique.Id
                         ,Delivery_Mode__c= deliveryMode      // Added by Promila to set delivery mode from boutique by default
                        , Scope_Limitation__c = campaignMarket.Scope_Limitation__c
                        , Launch_Date__c = campaignMarket.Launch_Date__c
                        , End_Date__c = campaignMarket.End_Date__c
                        , MPM_Scope_Limitation_Proposal__c = MPM_PROPOSAL_INHERIT
                        , Campaign_Market__c = campaignMarket.Id));
            }
        }
        if (cbToAdd.size() > 0){
            insert cbToAdd;
            ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);
            
            resetBoutiqueLists();   
        }
        return null;
    }   
    
    // RTCOMBoutiques in the Market but not in the Campaign
    public List<SelectableBoutiqueWrapper> availableBoutiques {
        get{
            if (availableBoutiques == null){
                availableBoutiques = new List<SelectableBoutiqueWrapper>();
                for (RTCOMBoutique__c m : allBoutiques){
                    if (!SelectedBoutiqueIdsSet.contains(m.Id)){
                        availableBoutiques.add(new SelectableBoutiqueWrapper(m));
                    }
                }
            }
            return availableBoutiques;
        }
        private set;
    }
    
    public void approveAllBoutiqueChanges(){
        if(isHQPMorAdmin){
            String chatterMessage = 'HQPM Accepted the folowing changes\nCampaign : ' + campaignMarket.Name;
            List<Campaign_Boutique__c> cbToUpdate = new List<Campaign_Boutique__c>();
            for(Campaign_Boutique__c cb : selectedBoutiquesOrdered){
                if(STATUS_PENDINGAPPROVAL.equals(cb.MPM_Proposal_Status__c)){
                    chatterMessage += '\nBoutique : ' + cb.Name;
                    if (cb.MPM_Launch_Date_Proposal__c != null && cb.Launch_Date__c != cb.MPM_Launch_Date_Proposal__c){
                        chatterMessage += '\nLaunch Date : ' + convertDateToString(cb.Launch_Date__c) + ' => ' + convertDateToString(cb.MPM_Launch_Date_Proposal__c);
                        cb.Launch_Date__c = cb.MPM_Launch_Date_Proposal__c;
                        cb.MPM_Launch_Date_Proposal__c = null;
                    }
                    if (cb.MPM_End_Date_Proposal__c != null && cb.End_Date__c != cb.MPM_End_Date_Proposal__c){
                        chatterMessage += '\nEnd Date : ' + convertDateToString(cb.End_Date__c) + ' => ' + convertDateToString(cb.MPM_End_Date_Proposal__c);
                        cb.End_Date__c = cb.MPM_End_Date_Proposal__c;
                        cb.MPM_End_Date_Proposal__c = null;
                    }
                    if (cb.Scope_Limitation__c != cb.MPM_Scope_Limitation_Proposal__c && cb.MPM_Scope_Limitation_Proposal__c != MPM_PROPOSAL_INHERIT){
                        chatterMessage += '\nScope Limitation : ' + (cb.Scope_Limitation__c == null ? '--None--' : cb.Scope_Limitation__c) + ' => ' + (cb.MPM_Scope_Limitation_Proposal__c == null ? '--None--' : cb.MPM_Scope_Limitation_Proposal__c);
                        cb.Scope_Limitation__c = cb.MPM_Scope_Limitation_Proposal__c;
                        cb.MPM_Scope_Limitation_Proposal__c = MPM_PROPOSAL_INHERIT;                     
                    }
                    cb.MPM_Proposal_Status__c = STATUS_PROPOSAL;
                    cbToUpdate.add(cb);
                }
            }
            if (cbToUpdate.size() > 0){
                try{
                    update cbToUpdate;
                
                    // Post message in Campaign Market Chatter
                    ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);  
                    resetSelectedBoutiques();   
                }
                catch(DMLException e){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
                }       
            }
        }
    }
    
    public void rejectAllBoutiqueChanges(){
        if(isHQPMorAdmin){
            String chatterMessage = 'HQPM Rejected the folowing changes\nCampaign : ' + campaignMarket.Name;
            List<Campaign_Boutique__c> cbToUpdate = new List<Campaign_Boutique__c>();
            for(Campaign_Boutique__c cb : selectedBoutiquesOrdered){
                if(STATUS_PENDINGAPPROVAL.equals(cb.MPM_Proposal_Status__c)){
                    chatterMessage += '\nBoutique : ' + cb.Name;
                    if (cb.MPM_Launch_Date_Proposal__c != null && cb.Launch_Date__c != cb.MPM_Launch_Date_Proposal__c){
                        chatterMessage += '\nLaunch Date : ' + convertDateToString(cb.Launch_Date__c) + ' => ' + convertDateToString(cb.MPM_Launch_Date_Proposal__c);
                        cb.MPM_Launch_Date_Proposal__c = null;
                    }
                    if (cb.MPM_End_Date_Proposal__c != null && cb.End_Date__c != cb.MPM_End_Date_Proposal__c){
                        chatterMessage += '\nEnd Date : ' + convertDateToString(cb.End_Date__c) + ' => ' + convertDateToString(cb.MPM_End_Date_Proposal__c);
                        cb.MPM_End_Date_Proposal__c = null;
                    }
                    if (cb.Scope_Limitation__c != cb.MPM_Scope_Limitation_Proposal__c && cb.MPM_Scope_Limitation_Proposal__c != MPM_PROPOSAL_INHERIT){
                        chatterMessage += '\nScope Limitation : ' + (cb.Scope_Limitation__c == null ? '--None--' : cb.Scope_Limitation__c) + ' => ' + (cb.MPM_Scope_Limitation_Proposal__c == null ? '--None--' : cb.MPM_Scope_Limitation_Proposal__c);
                        cb.MPM_Scope_Limitation_Proposal__c = MPM_PROPOSAL_INHERIT;                     
                    }
                    cb.MPM_Proposal_Status__c = STATUS_PROPOSAL;
                    cbToUpdate.add(cb);
                }
            }
            if (cbToUpdate.size() > 0){
                update cbToUpdate;
                
                // Post message in Campaign Market Chatter
                ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);  
                resetSelectedBoutiques();           
            }
        }
    }
    
    public void updateBoutique(){
        String updatedField = ApexPages.currentPage().getParameters().get('updatedField');
        Id boutiqueId = ApexPages.currentPage().getParameters().get('boutiqueId');      
        
        if (selectedBoutiquesMap.containsKey(boutiqueId)){
            Campaign_Boutique__c cb = selectedBoutiquesMap.get(boutiqueId);
            try{
                
                if (!'Status'.equals(updatedField)){
                    String chatterMessage = 'HQPM Changed Boutique Market details' + '\nCampaign : ' + campaignMarket.Name + ' - Boutique : '+ cb.Name;
    
                    if (FIELD_LAUNCHDATE.equals(updatedField)){
                        chatterMessage += '\nLaunch Date : ' + initialMarketLaunchDate.format('dd MMM yyyy') + ' => ' + convertDateToString(cb.Launch_Date__c);
                    }
                    
                    if (FIELD_ENDDATE.equals(updatedField)){
                        chatterMessage += '\nEnd Date : ' + initialMarketEndDate.format('dd MMM yyyy') + ' => ' + convertDateToString(cb.End_Date__c);
                    }
                    
                    if (FIELD_SCOPE.equals(updatedField)){
                        chatterMessage += '\nScope Limitation : ' + (initialMarketScopeLimitation == null ? '--None--' : initialMarketScopeLimitation) + ' => ' + (campaignMarket.Scope_Limitation__c == null ? '--None--' : campaignMarket.Scope_Limitation__c);
                    }
                     update cb;
               
                    // Post message in Campaign Market Chatter
                    ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);
                }
                resetSelectedBoutiques();
            }
            catch(DMLException e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            }
        }
    }
    
    public void updateBoutiqueProposal(){
        String updatedField = ApexPages.currentPage().getParameters().get('updatedField');
        Id boutiqueId = ApexPages.currentPage().getParameters().get('boutiqueId');      
        if (selectedBoutiquesMap.containsKey(boutiqueId)){
            Campaign_Boutique__c cb = selectedBoutiquesMap.get(boutiqueId);
            try{
                if(cb.MPM_Scope_Limitation_Proposal__c ==ERTAILMasterHelper.SCOPE_INSTOREDISPLAY && CheckDisplay==false){
                    displayError=true; 
                   errormsg ='You are not allowed to select this scope limitation as display topic was not selected for this campaign';
                 }
                else{
                update cb;
                }
                resetSelectedBoutiques(); 
                
            }
            catch(DMLException e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            }
        }
    }
    
        // Remove a boutique from the campaign
    public PageReference removeBoutiqueFromCampaign(){
        if (ApexPages.currentPage().getParameters().containsKey('cmtd')){
            Id campaignBoutiqueToDeleteId = ApexPages.currentPage().getParameters().get('cmtd');
            String chatterMessage = 'HQPM removed a Boutique\nCampaign : ' + campaignMarket.Name;
            if (selectedBoutiquesMap.containsKey(campaignBoutiqueToDeleteId))
                chatterMessage += '\nBoutique : ' + selectedBoutiquesMap.get(campaignBoutiqueToDeleteId).Name;
            else
                chatterMessage += '\nBoutiqueId : ' + campaignBoutiqueToDeleteId;           
            delete new Campaign_Boutique__c(Id = campaignBoutiqueToDeleteId);
            ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);          
            resetBoutiqueLists();
        }
        return null;
    }
    
    public void submitBoutiquesChanges(){
        List<Campaign_Boutique__c> cbToUpdate = new List<Campaign_Boutique__c>();
        String chatterMessage = 'MPM submitted Boutique changes\nCampaign : ' + campaignMarket.Name;
        for (Id cbId : selectedBoutiquesMap.keySet()){
            Campaign_Boutique__c cb = selectedBoutiquesMap.get(cbId);
            if (STATUS_PROPOSAL.equals(cb.MPM_Proposal_Status__c)){
                Boolean cbUpdated = false;
                String partialChatter = '\nBoutique: ' + cb.Name;
                
                if (cb.MPM_Launch_Date_Proposal__c != null && cb.Launch_Date__c != cb.MPM_Launch_Date_Proposal__c){
                    cbUpdated = true;
                    partialChatter += '\nLaunch Date : ' + convertDateToString(cb.Launch_Date__c) + ' => ' + convertDateToString(cb.MPM_Launch_Date_Proposal__c);
                }
                if (cb.MPM_End_Date_Proposal__c != null && cb.End_Date__c != cb.MPM_End_Date_Proposal__c){
                    cbUpdated = true;   
                    partialChatter += '\nEnd Date : ' + convertDateToString(cb.End_Date__c) + ' => ' + convertDateToString(cb.MPM_End_Date_Proposal__c);
                }
                if (cb.Scope_Limitation__c != cb.MPM_Scope_Limitation_Proposal__c && !MPM_PROPOSAL_INHERIT.equals(cb.MPM_Scope_Limitation_Proposal__c)){
                    cbUpdated = true;   
                    partialChatter += '\nScope Limitation : ' + (cb.Scope_Limitation__c == null ? '--None--' : cb.Scope_Limitation__c) + ' => ' + (cb.MPM_Scope_Limitation_Proposal__c == null ? '--None--' : cb.MPM_Scope_Limitation_Proposal__c);
                }
                
                if (cbUpdated){
                    cb.MPM_Proposal_Status__c = STATUS_PENDINGAPPROVAL;     
                    cbToUpdate.add(new Campaign_Boutique__c(Id = cb.Id, MPM_Proposal_Status__c = cb.MPM_Proposal_Status__c));
                    chatterMessage += partialChatter + '\n';
                }
            }
        }
        if (cbToUpdate.size() > 0){
            update cbToUpdate;
            ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage); 
            resetSelectedBoutiques();           
        }
    }
    
    
    public void resetBoutiqueLists(){
        availableBoutiques = null;
        resetSelectedBoutiques();
    }
    
    private void resetSelectedBoutiques(){
        initSelectedBoutiques();
    }
    
    public void saveBoutiqueDecision(){
        String updatedField = ApexPages.currentPage().getParameters().get('updatedField');
        Id boutiqueId = ApexPages.currentPage().getParameters().get('boutiqueId');      
        String decision = ApexPages.currentPage().getParameters().get('decision');
        String chatterMessage = '';
        Boolean isApproved = false;
        if (selectedBoutiquesMap.containsKey(boutiqueId)){
            Campaign_Boutique__c cb = selectedBoutiquesMap.get(boutiqueId);

            if ('Approve'.equals(Decision)){
                chatterMessage += 'HQPM Approved the Boutique Change\nCampaign : ' + campaignMarket.Name + ' - Boutique : '+ cb.Name;
                isApproved = true;
            }else if ('Reject'.equals(Decision)){
                chatterMessage += 'HQPM Rejected the Boutique Change\nCampaign : ' + campaignMarket.Name + ' - Boutique : '+ cb.Name;
            }else
                return;

            if (FIELD_LAUNCHDATE.equals(updatedField)){
                chatterMessage += '\nLaunch Date : ' + convertDateToString(cb.Launch_Date__c) + ' => ' + convertDateToString(cb.MPM_Launch_Date_Proposal__c);
                
                if (isApproved){
                    cb.Launch_Date__c = cb.MPM_Launch_Date_Proposal__c;
                }
                cb.MPM_Launch_Date_Proposal__c = null;
            }
            if (FIELD_ENDDATE.equals(updatedField)){
                chatterMessage += '\nEnd Date : ' + convertDateToString(cb.End_Date__c) + ' => ' + convertDateToString(cb.MPM_End_Date_Proposal__c);
                
                if (isApproved){
                    cb.End_Date__c = cb.MPM_End_Date_Proposal__c;
                }
                cb.MPM_End_Date_Proposal__c = null;
            }
            if (FIELD_SCOPE.equals(updatedField)){
                chatterMessage += '\nScope Limitation : ' + (cb.Scope_Limitation__c == null ? '--None--' : cb.Scope_Limitation__c) + ' => ' + (cb.MPM_Scope_Limitation_Proposal__c == null ? '--None--' : cb.MPM_Scope_Limitation_Proposal__c);
            
                if (isApproved){
                    cb.Scope_Limitation__c = cb.MPM_Scope_Limitation_Proposal__c;
                }
                cb.MPM_Scope_Limitation_Proposal__c = MPM_PROPOSAL_INHERIT;
            }
            
            if (cb.MPM_Launch_Date_Proposal__c == null && cb.MPM_End_Date_Proposal__c == null && cb.MPM_Scope_Limitation_Proposal__c == MPM_PROPOSAL_INHERIT)
                cb.MPM_Proposal_Status__c = STATUS_PROPOSAL;
                
            try{        
                update cb;
                    
                // Post message in Campaign Market Chatter
                ConnectAPI.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, campaignMarket.Id, chatterMessage);
                
                resetSelectedBoutiques();
            }
            catch(DMLException e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            }
        }       
    }
    //Start - req 13
    public PageReference redirectPopup(){
        displayError = false;
        
        return null;
    }
    // End
}
/*
*   @Class      :   ERTAILCampaignBoutiquesListExtTest.cls
*   @Description:   Test methods for class ERTAILCampaignBoutiquesListExt.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
    Ankita Singhal  2 Apr
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest(SeeAllData=true)
private class ERTAILCampaignBoutiquesListExtTest {
//Public Boolean DisplayError;    
    static testMethod void myUnitTest1() {
        Test.startTest();
                
        Profile sysAdmin = [SELECT Id,Name FROM Profile WHERE Name = 'System Admin'];
        
        system.debug('Profile:'+sysAdmin.Name);
        User adminUser = new User(
                            LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
        insert adminUser;
        system.debug('adminUser:'+adminUser);
        
        System.runAs(adminUser){
            Id testCampaignId = ERTAILTestHelper.createCampaignMME();

            Campaign_Market__c cmpMkt = [select Id, Name, Campaign__c,Scope_Limitation__c, Launch_Date__c, End_date__c,
                                         RecordTypeId, MPM_Launch_Date_Proposal__c,MPM_End_Date_Proposal__c,
                                         RTCOMMarket__c, MPM_Proposal_Status__c, MPM_Scope_Limitation_Proposal__c                                          
                                         from Campaign_Market__c where
                                         Campaign__r.Id =: testCampaignId][0];
            system.debug('CampID:'+testCampaignId);
            system.debug('Market:'+cmpMkt);
            ApexPages.StandardController stdController = new ApexPages.StandardController(cmpMkt);
        
            ERTAILCampaignBoutiquesListExt ctrl = new ERTAILCampaignBoutiquesListExt(stdController);
                    
            ctrl.redirectToEditPage();
            ctrl.redirectPopup();
                    
            Boolean b = ctrl.CanEditMarketProposal & ctrl.isMPMProposalRejected; 
            b = ctrl.isMPMProposal & ctrl.isMPMProposalSubmitted;
            b = ctrl.IsScopeProposalDifferentThanExisting & ctrl.IsLaunchDateProposalDifferentThanExisting;
            b = ctrl.IsEndDateProposalDifferentThanExisting & ctrl.isProposalDifferentThanExisting;
            
            ctrl.updateMarketScopeLimitation();
            ctrl.updateMarketLaunchDate();
            ctrl.updateMarketEndDate();
            
            
            ctrl.updateMPMProposal();
            ctrl.updateMPMProposalStatus();

            cmpMkt.MPM_Launch_Date_Proposal__c = System.Today().addDays(1);
            cmpMkt.MPM_End_Date_Proposal__c = System.Today().addDays(1);
            update cmpMkt;

            ctrl.submitMarketChangesProposal();
            ctrl.ResetMarketChangesProposal();

            cmpMkt.MPM_Launch_Date_Proposal__c = System.Today().addDays(1);
            cmpMkt.MPM_End_Date_Proposal__c = System.Today().addDays(1);
            update cmpMkt;

            ctrl.submitMarketChangesProposal();
            ctrl.rejectMarketChangesProposal();

            cmpMkt.MPM_Launch_Date_Proposal__c = System.Today().addDays(1);
            cmpMkt.MPM_End_Date_Proposal__c = System.Today().addDays(1);
            cmpMkt.MPM_Scope_Limitation_Proposal__c = 'pouet';
            update cmpMkt;

            ctrl.submitMarketChangesProposal();
            ctrl.approveMarketChangesProposal();
            
            List<SelectOption> decisionOptions = ctrl.decisionOptions;
        }
            
        Test.stopTest();
            
    }
    
    
    static testMethod void myUnitTest2() {
        Test.startTest();
                
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
        
        User adminUser = new User(
                            LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
        
        System.runAs(adminUser){
            Id testCampaignId = ERTAILTestHelper.createCampaignMME();

            Campaign_Market__c cmpMkt = [select Id, Name, Scope_Limitation__c, Launch_Date__c, End_date__c, RecordTypeId, 
                                         RTCOMMarket__c,Campaign__c,
                                         MPM_Proposal_Status__c, MPM_Scope_Limitation_Proposal__c,
                                         MPM_Launch_Date_Proposal__c,MPM_End_Date_Proposal__c 
                                         from Campaign_Market__c where Campaign__r.Id =: testCampaignId][0];
          // Campaign_Topic__c ctc =[Select Id,name, Campaign_Topic__c] 
                   Campaign_Topic__c ctc = [Select id ,name , campaign__C,topic__r.name from Campaign_Topic__c where Campaign__r.Id =: testCampaignId][0];
            List<Campaign_Topic__c> listCt= new List<Campaign_Topic__c>();
            listCt.add(ctc);
            update listCt;
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(cmpMkt);
        
            ERTAILCampaignBoutiquesListExt ctrl = new ERTAILCampaignBoutiquesListExt(stdController);
                    
            List<Campaign_Boutique__c> cmpBtqList = [select Id, MPM_Launch_Date_Proposal__c, MPM_End_Date_Proposal__c, MPM_Scope_Limitation_Proposal__c, MPM_Proposal_Status__c from Campaign_Boutique__c limit 5];
            
            for(Campaign_Boutique__c cmpBtq : cmpBtqList){
                cmpBtq.MPM_Launch_Date_Proposal__c = System.today().addDays(2);
                cmpBtq.MPM_End_Date_Proposal__c = System.today().addDays(2);
                cmpBtq.MPM_Scope_Limitation_Proposal__c = 'tchouf';
                cmpBtq.MPM_Proposal_Status__c = 'Pending Approval';
            }
            database.update(cmpBtqList,false);
            ctrl.resetBoutiqueLists();
            ctrl.rejectAllBoutiqueChanges();
        }
            
        Test.stopTest();        
    }
    
    static testMethod void myUnitTest3() {
        Test.startTest();
                
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
        
        User adminUser = new User(
                            LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
        
        System.runAs(adminUser){
            Id testCampaignId = ERTAILTestHelper.createCampaignMME();

            Campaign_Market__c cmpMkt = [select Id, Name,Campaign__c, Scope_Limitation__c, Launch_Date__c, End_date__c, RecordTypeId, RTCOMMarket__c
                                        , MPM_Proposal_Status__c, MPM_Scope_Limitation_Proposal__c, MPM_Launch_Date_Proposal__c,MPM_End_Date_Proposal__c from Campaign_Market__c where Campaign__r.Id =: testCampaignId][0];
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(cmpMkt);
        
            ERTAILCampaignBoutiquesListExt ctrl = new ERTAILCampaignBoutiquesListExt(stdController);
                    
            List<Campaign_Boutique__c> cmpBtqList = [select Id, MPM_Launch_Date_Proposal__c, MPM_End_Date_Proposal__c, MPM_Scope_Limitation_Proposal__c, MPM_Proposal_Status__c from Campaign_Boutique__c limit 5];
            
            for(Campaign_Boutique__c cmpBtq : cmpBtqList){
                cmpBtq.MPM_Launch_Date_Proposal__c = System.today().addDays(2);
                cmpBtq.MPM_End_Date_Proposal__c = System.today().addDays(2);
                cmpBtq.MPM_Scope_Limitation_Proposal__c = 'tchouf';
                cmpBtq.MPM_Proposal_Status__c = 'Proposal';
            }
            database.update(cmpBtqList,false);          
            ctrl.resetBoutiqueLists();
            ctrl.submitBoutiquesChanges();
        }
            
        Test.stopTest();        
    }
    
    static testMethod void myUnitTest4() {
        Test.startTest();
                
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
        
        User adminUser = new User(
                            LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
        
        System.runAs(adminUser){
            Id testCampaignId = ERTAILTestHelper.createCampaignMME();

            Campaign_Market__c cmpMkt = [select Id, Name, Scope_Limitation__c,Campaign__c, Launch_Date__c, End_date__c, RecordTypeId, RTCOMMarket__c
                                        , MPM_Proposal_Status__c, MPM_Scope_Limitation_Proposal__c, MPM_Launch_Date_Proposal__c,MPM_End_Date_Proposal__c from Campaign_Market__c where Campaign__r.Id =: testCampaignId][0];
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(cmpMkt);
        
            ERTAILCampaignBoutiquesListExt ctrl = new ERTAILCampaignBoutiquesListExt(stdController);
                    
            List<Campaign_Boutique__c> cmpBtqList = [select Id, MPM_Launch_Date_Proposal__c, MPM_End_Date_Proposal__c, MPM_Scope_Limitation_Proposal__c, MPM_Proposal_Status__c from Campaign_Boutique__c limit 5];
            
            for(Campaign_Boutique__c cmpBtq : cmpBtqList){
                cmpBtq.MPM_Launch_Date_Proposal__c = System.today().addDays(2);
                cmpBtq.MPM_End_Date_Proposal__c = System.today().addDays(2);
                cmpBtq.MPM_Scope_Limitation_Proposal__c = 'tchouf';
                cmpBtq.MPM_Proposal_Status__c = 'Pending Approval';
            }
            database.update(cmpBtqList,false);
            ctrl.resetBoutiqueLists();
            ctrl.approveAllBoutiqueChanges();
        }
        Test.stopTest();
    }
    
    static testMethod void myUnitTest5() {
        Test.startTest();
                
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
        
        User adminUser = new User(
                            LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
        
        System.runAs(adminUser){
            Id testCampaignId = ERTAILTestHelper.createCampaignMME();

            Campaign_Market__c cmpMkt = [select Id, Name, Scope_Limitation__c,Campaign__c, Launch_Date__c, End_date__c, RecordTypeId, RTCOMMarket__c
                                        , MPM_Proposal_Status__c, MPM_Scope_Limitation_Proposal__c, MPM_Launch_Date_Proposal__c,MPM_End_Date_Proposal__c from Campaign_Market__c where Campaign__r.Id =: testCampaignId][0];
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(cmpMkt);
        
            ERTAILCampaignBoutiquesListExt ctrl = new ERTAILCampaignBoutiquesListExt(stdController);
                                
            ctrl.addBoutiqueToCampaign();

            List<Campaign_Boutique__c> cmpBtqList = [select Id, MPM_Launch_Date_Proposal__c, MPM_End_Date_Proposal__c, MPM_Scope_Limitation_Proposal__c, MPM_Proposal_Status__c from Campaign_Boutique__c];

            for(Campaign_Boutique__c cmpBtq : cmpBtqList){
                PageReference pgRef = Page.ERTAILOrdersDetail;
                pgRef.getParameters().put('updatedField', ERTAILCampaignBoutiquesListExt.FIELD_LAUNCHDATE);
                pgRef.getParameters().put('boutiqueId', cmpBtq.Id);
                pgRef.getParameters().put('cmtd', cmpBtq.Id);
                pgRef.getParameters().put('decision', 'Approve');
                Test.setCurrentPage(pgRef);
                ctrl.updateBoutique();
                ctrl.updateBoutiqueProposal();
            }
        }
        Test.stopTest();
    }
    
    static testMethod void myUnitTest6() {
        Test.startTest();
                
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
        
        User adminUser = new User(
                            LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
        
        System.runAs(adminUser){
            Id testCampaignId = ERTAILTestHelper.createCampaignMME();

            Campaign_Market__c cmpMkt = [select Id, Name, Scope_Limitation__c,Campaign__c, Launch_Date__c, End_date__c, RecordTypeId, RTCOMMarket__c
                                        , MPM_Proposal_Status__c, MPM_Scope_Limitation_Proposal__c, MPM_Launch_Date_Proposal__c,MPM_End_Date_Proposal__c from Campaign_Market__c where Campaign__r.Id =: testCampaignId][0];
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(cmpMkt);
        
            ERTAILCampaignBoutiquesListExt ctrl = new ERTAILCampaignBoutiquesListExt(stdController);
                                
            ctrl.addBoutiqueToCampaign();

            List<Campaign_Boutique__c> cmpBtqList = [select Id, MPM_Launch_Date_Proposal__c, MPM_End_Date_Proposal__c, MPM_Scope_Limitation_Proposal__c, MPM_Proposal_Status__c from Campaign_Boutique__c];

            for(Campaign_Boutique__c cmpBtq : cmpBtqList){
                PageReference pgRef = Page.ERTAILOrdersDetail;
                pgRef.getParameters().put('updatedField', ERTAILCampaignBoutiquesListExt.FIELD_LAUNCHDATE);
                pgRef.getParameters().put('boutiqueId', cmpBtq.Id);
                pgRef.getParameters().put('cmtd', cmpBtq.Id);
                pgRef.getParameters().put('decision', 'Approve');
                Test.setCurrentPage(pgRef);
                if(ctrl.selectedBoutiquesMap.containsKey(cmpBtq.Id)){
                    ctrl.saveBoutiqueDecision();
                    pgRef.getParameters().put('updatedField', ERTAILCampaignBoutiquesListExt.FIELD_ENDDATE);
                    ctrl.saveBoutiqueDecision();
                    pgRef.getParameters().put('updatedField', ERTAILCampaignBoutiquesListExt.FIELD_SCOPE);
                    ctrl.saveBoutiqueDecision();
                    pgRef.getParameters().put('decision', 'Reject');
                    ctrl.saveBoutiqueDecision();
                    break;
                }
                
            }


            for(Campaign_Boutique__c cmpBtq : cmpBtqList){
                PageReference pgRef = Page.ERTAILOrdersDetail;
                pgRef.getParameters().put('updatedField', ERTAILCampaignBoutiquesListExt.FIELD_LAUNCHDATE);
                pgRef.getParameters().put('boutiqueId', cmpBtq.Id);
                pgRef.getParameters().put('cmtd', cmpBtq.Id);
                pgRef.getParameters().put('decision', 'Approve');
                Test.setCurrentPage(pgRef);
                ctrl.removeBoutiqueFromCampaign();
                break;
            }

            ctrl.saveMarketDecision();
        }
        Test.stopTest();
    }
}
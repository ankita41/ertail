/*****
*   @Class      :   ERTAILCampaignDashboardController.cls
*   @Description:   Extension developed for ERTAILCampaignDashboard.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignDashboardController {
	public Boolean isHQPMorAdmin { get { return ERTailMasterHelper.isHQPMorAdmin;} }
	public Boolean isMPM { get { return ERTailMasterHelper.isMPM;} }
	public Boolean isAgent { get { return ERTailMasterHelper.isAgent;} }
    public class CustomException extends Exception {/*throw new CustomException()*/}
    public Campaign__c campaign { get; private set;}
    public Date asOfDate {get;set;}
    
    public ERTAILCampaignDashboardController(ApexPages.StandardController stdController){
        campaign = [SELECT Id, Name, Scope__c, Launch_Date__c, End_Date__c, Description__c FROM Campaign__c WHERE Id = :stdController.getRecord().Id];
        
        if(ApexPages.CurrentPage().getParameters().containsKey('showHeader')){
            showHeader = Boolean.valueOf(ApexPages.CurrentPage().getParameters().get('showHeader'));
        }
        
        String temp = ApexPages.currentPage().getParameters().get('asofdate');
        if(temp!=null)
        	asOfDate = Date.valueOf(temp);
        else
        	asOfDate = SYSTEM.Today();
    }
    
    public Boolean showHeader{ 
        get{
            if(showHeader == null)
                showHeader = false;
            return showHeader;
        } 
        set; 
    }
    
    public void selectStatus(){
    	campaignMarketList = null;
    	campaignItemList = null;
		campaignBoutiqueList = null;
    	TimeLineList = null;
		MonthsList = null;
		NBVisualsPerStatusDonutData = null;
		NBProdCostPerStatusDonutData = null;
		NBBoutiquePerCreativityStatusDonutData = null;
		NBBoutiquePerQuantityStatusDonutData = null;
		NBMarketPerCreativityStatusDonutData = null;
		NBMarketPerQuantityStatusDonutData = null;
		NBCampBTQCostPerMarketLineData = null;
    }
    
    private List<Campaign_Item__c> campaignItemList{
        get{
            if(campaignItemList == null)
                campaignItemList = [
                	Select Name, Cost_Step__c, Visual_Step__c, Cost_Step_Value__c, Visual_Step_Value__c
                	FROM Campaign_Item__c 
                	where Campaign__c = :campaign.Id
                ];
            return campaignItemList;
        }
        set;
    }
    
    private List<Campaign_Market__c> campaignMarketList{
        get{
            if(campaignMarketList == null){
		        Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Campaign_Market__c').getDescribe().fields.getMap();
		        String qryStr = 'SELECT ';
		        for(String s : fieldsMap.keySet()){
		            if(fieldsMap.get(s).getDescribe().isAccessible())
		                qryStr = qryStr + s + ', ';
		        }
		        qryStr = qryStr.removeEnd(', ') + ' FROM Campaign_Market__c WHERE Campaign__c = \'' + campaign.Id + '\' ';
		        qryStr = qryStr + 'ORDER BY Name ';
		        campaignMarketList = Database.query(qryStr);								
            }
            return campaignMarketList;
        }
        set;
    }
    
	//MAP TO STORE CAMPAIGN BOUTIQUE SIZE PER MARKET
	private Map<Id,Integer> mapCampBtqOrderPerMktCount{
		get{
			if(mapCampBtqOrderPerMktCount==null){
				mapCampBtqOrderPerMktCount = new Map<Id,Integer>();
				for(Campaign_Market__c cm : campaignMarketList){
					mapCampBtqOrderPerMktCount.put(cm.Id, 0);
				}
				
				for(Boutique_Order_Item__c boi : [SELECT Campaign_Boutique__r.Campaign_Market__c, Quantity_to_Order__c FROM Boutique_Order_Item__c WHERE Campaign_Boutique__r.Campaign_Market__c IN :campaignMarketList]){
					mapCampBtqOrderPerMktCount.put(boi.Campaign_Boutique__r.Campaign_Market__c, mapCampBtqOrderPerMktCount.get(boi.Campaign_Boutique__r.Campaign_Market__c) + (boi.Quantity_to_order__c == null ? 0 : Integer.valueOf(boi.Quantity_to_order__c)));
				}
			}
			return mapCampBtqOrderPerMktCount;
		}
		set;
	}
    
    private List<Campaign_Boutique__c> campaignBoutiqueList{
        get{
            if(campaignBoutiqueList == null){
            	Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Campaign_Boutique__c').getDescribe().fields.getMap();
		        String qryStr = 'SELECT (SELECT Id FROM Boutique_Order_Items__r), Campaign_Market__r.Name, ';
		        for(String s : fieldsMap.keySet()){
		            if(fieldsMap.get(s).getDescribe().isAccessible())
		                qryStr = qryStr + s + ', ';
		        }
		        qryStr = qryStr.removeEnd(', ') + ' FROM Campaign_Boutique__c WHERE Campaign_Market__r.Campaign__c = \'' + campaign.Id + '\' ';
		        qryStr = qryStr + 'ORDER BY Campaign_Market__r.Name ';
		        campaignBoutiqueList = Database.query(qryStr);
            }
            return campaignBoutiqueList;
        }
        set;
    }

    //CHART COLORS/HIGHLIGHTS FOR CAMPAIGN ITEM STATUSES
    private static Map<Integer,String> chartColors1 = new Map<Integer,String>{
    	10 => '#f89d54', 
    	20 => '#d56b15', 
    	30 => '#613c07'
    };
    
    private static Map<Integer,String> chartHighlights1 = new Map<Integer,String>{
    	10 => '#ffaf6f', 
    	20 => '#eb7e25', 
    	30 => '#794d0e'
    };
    
    //CHART COLORS/HIGHLIGHTS FOR CAMPAIGN MARKET AND CAMPAIGN BOUTIQUE STATUSES
    private static Map<Integer,String> chartColors2 = new Map<Integer,String>{
    	10 => '#d56b15', 
    	11 => '#613c07',
    	20 => '#231f20', 
		21 => '#ffffff', 
		30 => '#b80832', 
		40 => '#ede4d8', 
		41 => '#402619', 
		42 => '#d9caaf'
    };
    
    private static Map<Integer,String> chartHighlights2 = new Map<Integer,String>{
    	10 => '#eb7e25', 
    	11 => '#794d0e',
    	20 => '#231f20', 
		21 => '#ffffff', 
		30 => '#b80832', 
		40 => '#ede4d8', 
		41 => '#402619', 
		42 => '#d9caaf'
    };
    
    private static Map<String, String> chartScopeColors = new Map<String, String>{ERTAILMasterHelper.SCOPE_COMPLETE => '#613c07', ERTAILMasterHelper.SCOPE_SHOWWINDOW => '#d56b15', ERTAILMasterHelper.SCOPE_INSTORE => '#f89d54'};
    private static Map<String, String> chartScopeHighlights = new Map<String, String>{ERTAILMasterHelper.SCOPE_COMPLETE => '#794d0e', ERTAILMasterHelper.SCOPE_SHOWWINDOW => '#eb7e25', ERTAILMasterHelper.SCOPE_INSTORE => '#ffaf6f'};
    
	//TIMELINE WRAPPER
	public class TimeLineWrapper{
		public Decimal Width {get; private set;}
		public Decimal Left {get; private set;}
		public String Name {get; private set;}
		public Id ObjId {get; private set;}
		public String Color {get; private set;}
		public String Highlight {get; private set;}
		
		public TimeLineWrapper(Campaign_Market__c cm, Date startDate){
			Name = cm.Name;
			ObjId = cm.Id;
			width = ((cm.Launch_Date__c.daysBetween(cm.End_Date__c) / 365.242) * 100).setScale(2); // duration in days represented as the percentage of a year rounded to two decimals
			left = ((startDate.daysBetween(cm.Launch_Date__c) / 365.242) * 100).setScale(2); // offset is calculated from 1st of Jan of the current year rounded to two decimals
			String scope = cm.Scope_Limitation__c != null ? cm.Scope_Limitation__c : ERTAILMasterHelper.SCOPE_COMPLETE;
			Color = chartScopeColors.containsKey(scope) ? chartScopeColors.get(scope) : 'Black';
			Highlight = chartScopeHighlights.containsKey(scope) ? chartScopeHighlights.get(scope) : 'Black';
		}
	}
	
	public List<TimeLineWrapper> TimeLineList{
		get{
			if(TimeLineList == null){
				TimeLineList = new List<TimeLineWrapper>();
				todayOffset = ((timelineStartDate.daysBetween(Date.today()) / 365.242) * (1110 - 1110/4)).setScale(2);
				todayOffsetMarker = ((timelineStartDate.daysBetween(Date.today()) / 365.242) * 1110).setScale(2);
				for(Campaign_Market__c cm : campaignMarketList){
					timelineDurationInDays = Math.Max(timelineStartDate.daysBetween(cm.End_Date__c), timelineDurationInDays);
					TimeLineList.add(new TimeLineWrapper(cm, timelineStartDate));
				} 
			}
			return TimeLineList;
		}
		private set;
	}
	
	public Decimal todayOffset { get; private set;}
	public Decimal todayOffsetMarker { get; private set;}
	private Integer timelineDurationInDays = 0;
	private Date timelineStartDate{
		get{
			if(campaignMarketList.size() > 0){
				timelineStartDate = campaignMarketList[0].Launch_Date__c;
			} else{
				timelineStartDate = asOfDate;
			}
			return timelineStartDate;
		}
		private set;
	}
	
	public List<String> MonthsList {
		get{
			if(MonthsList == null){
				List<TimeLineWrapper> init = TimeLineList;
				MonthsList = new List<String>();
				Date endDate = timelineStartDate.addDays(timelineDurationInDays);
				// Min displayed is 1 year
				Date oneYear = timelineStartDate.addDays(365);
				endDate = (endDate < oneYear ? oneYear : endDate);
				Integer monthDiff = timelineStartDate.monthsBetween(endDate);
				if(endDate.day() > timelineStartDate.day()) monthDiff++;
				Date cursor = timelineStartDate;
				for(Integer i = 0 ; i < monthDiff ; i++){
					MonthsList.add(Datetime.newInstance(cursor.Year(), cursor.Month(), 1).format('MMM yyyy'));
					cursor = cursor.addMonths(1);
				}
			}
			return MonthsList;
		}
		private set;
	}
    
	//DONUT WRAPPER
    public class DonutChartWrapper{
    	public String label { public get; private set; }
        public Integer value { public get; private set; }
        public String color { public get; private set; }
        public String highlight { public get; private set; }
        
        public DonutChartWrapper(AggregateResult ar, String defaultName, Integer max){
            value = (Integer) ar.get('quantity');
            label = (String) ar.get('name');
            if(label == null){
                label = defaultName;
            }
            label = label.substring(0, Math.MIN(label.length(), max));
        }
        
       public DonutChartWrapper(Integer stepVal, String stepLabel, Integer val, Integer stepType){
            //label = (stepLabel.length()>15) ? stepLabel.substring(0,15) + '...' : stepLabel;
            label = stepLabel;
            value = val;
            
            if(stepType==1){
            	color = chartColors1.containsKey(stepVal) ? chartColors1.get(stepVal) : 'Black';
	            highlight = chartHighlights1.containsKey(stepVal) ? chartHighlights1.get(stepVal) : '#fff';
            }
	        else{
	        	color = chartColors2.containsKey(stepVal) ? chartColors2.get(stepVal) : 'Black';
	            highlight = chartHighlights2.containsKey(stepVal) ? chartHighlights2.get(stepVal) : '#fff';
	        }
       }
    }
    
    private List<DonutChartWrapper> GetDonutChartData (List<SObject> objList, String statField, String statValueField, Integer stepType){
    	Map<Integer,Integer> mapStatusCount = new Map<Integer,Integer>();
		Map<Integer,String> mapStatusLabel = new Map<Integer,String>();
		Integer stepInt;
		
		for(Sobject sobj : objList){
        	stepInt = Integer.valueOf(sobj.get(statValueField));
            if(!mapStatusCount.containsKey(stepInt)){
                mapStatusCount.put(stepInt, 1);
                mapStatusLabel.put(stepInt, String.valueOf(sobj.get(statField)));
            }
            else
                mapStatusCount.put(stepInt, mapStatusCount.get(stepInt) + 1);
		}
		
		List<DonutChartWrapper> varDonutData = new List<DonutChartWrapper>();
		for(Integer stepValue : mapStatusCount.keySet()){
			varDonutData.add(new DonutChartWrapper(stepValue, mapStatusLabel.get(stepValue), mapStatusCount.get(stepValue), stepType));
		}
				
    	return varDonutData;
    }
    
	public String NBVisualsPerStatusDonutDataJSON { get{ return JSON.serialize(NBVisualsPerStatusDonutData); } }
	private List<DonutChartWrapper> NBVisualsPerStatusDonutData { 
		get{
			if(NBVisualsPerStatusDonutData == null){
				NBVisualsPerStatusDonutData = GetDonutChartData(campaignItemList, 'Visual_Step__c', 'Visual_Step_Value__c', 1);
			}
			return NBVisualsPerStatusDonutData;
		} 
		private set;
	}
	
	public String NBProdCostPerStatusDonutDataJSON { get{ return JSON.serialize(NBProdCostPerStatusDonutData); } }
	private List<DonutChartWrapper> NBProdCostPerStatusDonutData { 
		get{
			if(NBProdCostPerStatusDonutData == null){
				NBProdCostPerStatusDonutData = GetDonutChartData(campaignItemList, 'Cost_Step__c', 'Cost_Step_Value__c', 1);
			}
			return NBProdCostPerStatusDonutData;
		} 
		private set;
	}
	
	public String NBBoutiquePerCreativityStatusDonutDataJSON { get{ return JSON.serialize(NBBoutiquePerCreativityStatusDonutData); } }
	private List<DonutChartWrapper> NBBoutiquePerCreativityStatusDonutData { 
		get{
			if(NBBoutiquePerCreativityStatusDonutData == null){
				NBBoutiquePerCreativityStatusDonutData = GetDonutChartData(campaignBoutiqueList, 'Creativity_Step__c', 'Creativity_Step_Value__c', 2);
			}
			return NBBoutiquePerCreativityStatusDonutData;
		} 
		private set;
	}
	
	public String NBBoutiquePerQuantityStatusDonutDataJSON { get{ return JSON.serialize(NBBoutiquePerQuantityStatusDonutData); } }
	private List<DonutChartWrapper> NBBoutiquePerQuantityStatusDonutData { 
		get{
			if(NBBoutiquePerQuantityStatusDonutData == null){
				NBBoutiquePerQuantityStatusDonutData = GetDonutChartData(campaignBoutiqueList, 'Quantity_Step__c', 'Quantity_Step_Value__c', 2);
			}
			return NBBoutiquePerQuantityStatusDonutData;
		} 
		private set;
	}
	
	public String NBMarketPerCreativityStatusDonutDataJSON { get{ return JSON.serialize(NBMarketPerCreativityStatusDonutData); } }
	private List<DonutChartWrapper> NBMarketPerCreativityStatusDonutData { 
		get{
			if(NBMarketPerCreativityStatusDonutData == null){
				NBMarketPerCreativityStatusDonutData = GetDonutChartData(campaignMarketList, 'Creativity_Step__c', 'Creativity_Step_Value__c', 2);
			}
			return NBMarketPerCreativityStatusDonutData;
		} 
		private set;
	}
	
	public String NBMarketPerQuantityStatusDonutDataJSON { get{ return JSON.serialize(NBMarketPerQuantityStatusDonutData); } }
	private List<DonutChartWrapper> NBMarketPerQuantityStatusDonutData { 
		get{
			if(NBMarketPerQuantityStatusDonutData == null){
				NBMarketPerQuantityStatusDonutData = GetDonutChartData(campaignMarketList, 'Quantity_Step__c', 'Quantity_Step_Value__c', 2);
			}
			return NBMarketPerQuantityStatusDonutData;
		} 
		private set;
	}
	
	public String CampaignColorsJSON { get{ return JSON.serialize(CampaignColors);} } 
	private List<String> CampaignColors { 
		get{
			if(CampaignColors == null){
				CampaignColors = new List<String>();
				String scope;
				for(Campaign_Market__c cm : campaignMarketList){
					scope = (cm.Scope_Limitation__c == null ? ERTAILMasterHelper.SCOPE_COMPLETE : cm.Scope_Limitation__c);
					CampaignColors.add(chartScopeColors.containsKey(scope) ? chartScopeColors.get(scope) : 'Black');
				}
			}
			return CampaignColors;
		}
		private set; 
	}

	public String CampaignHighlightsJSON { get{ return JSON.serialize(CampaignHighlights);} } 
	private List<String> CampaignHighlights { 
		get{
			if(CampaignHighlights == null){
				CampaignHighlights = new List<String>();
				String scope;
				for(Campaign_Market__c cm : campaignMarketList){
					scope = (cm.Scope_Limitation__c == null ? ERTAILMasterHelper.SCOPE_COMPLETE : cm.Scope_Limitation__c);
					CampaignHighlights.add(chartScopeHighlights.containsKey(scope) ? chartScopeHighlights.get(scope) : 'Black');
				}
			}
			return CampaignHighlights;
		}
		private set; 
	}
	
	//LINE CHART WRAPPER
	public class LineChartWrapper{
		public List<String> labels { public get; private set; }
		public List<LineChartDataWrapper> datasets { public get; public set; }
		public Double ratio { public get; private set; }
				
		public LineChartWrapper(){
			labels = new List<String>(); 
            //LineChartDataWrapper(String label, String type, String strokeColor, String pointColor, String pointStrokeColor, String pointHighlightFill, String pointHighlightStroke, String fillColor)
			datasets = new List<LineChartDataWrapper> {
				new LineChartDataWrapper('Items Qty', 'bar'),
				new LineChartDataWrapper('Actual (EUR)','line','rgba(184,8,50,1)','rgba(184,8,50,1)','#fff','#fff','rgba(184,8,50,1)','rgba(184,8,50,0.1)'),
				new LineChartDataWrapper('Max (EUR)','line','rgba(38,148,86,1)','rgba(38,148,86,1)','#fff','#fff','rgba(38,148,86,1)','rgba(38,148,86,0.1)')
			};
		}

		public void add(String name, Integer value1, Integer value2, Integer value3){
			labels.add((name.length() > 10) ? name.substring(0, 10) + '...' : name);
			datasets[0].add(value1, value1);
			datasets[1].add(value2, value2 / ratio);
			datasets[2].add(value3, value3 / ratio);
		}
	}
	
	public class LineChartDataWrapper{
		public String label { public get; private set; }
		public String fillColor { public get; public set; }
		public String highlightFill { public get; public set; }
		public String type { public get; private set; }
		public String strokeColor { public get; private set; }
		public String pointColor { public get; private set; }
		public String pointStrokeColor { public get; private set; }
		public String pointHighlightFill { public get; private set; }
		public String pointHighlightStroke { public get; private set; }
		public Boolean scaleShowGridLines { public get; private set; }
		public List<double> data { public get; private set; }
		
		public LineChartDataWrapper(String label, String type, String strokeColor, String pointColor, String pointStrokeColor, String pointHighlightFill, String pointHighlightStroke, String fillColor){
			data = new List<double>();
      		this.label = label;
      		this.type = type;
		    this.fillColor = fillColor;
		    this.strokeColor = strokeColor;
		    this.pointColor = pointColor;
		    this.pointStrokeColor = pointStrokeColor;
		    this.pointHighlightFill = pointHighlightFill;
		    this.pointHighlightStroke = pointHighlightStroke;
  		}
  		
  		public LineChartDataWrapper(String label, String type){
      		data = new List<double>();
      		this.label = label;
      		this.type = type;
		    this.fillColor = 'rgba(213,107,21,0.2)';
		    this.highlightFill = 'rgba(129,129,129,0.2)';	
  		}
		
		public void add(Integer org, Double value){
			data.add(value);
		}
	} 
	
	public String NBCampBTQCostPerMarketLineDataJSON { get{return JSON.serialize(NBCampBTQCostPerMarketLineData); } } 
	private LineChartWrapper NBCampBTQCostPerMarketLineData { 
		get{
			if(NBCampBTQCostPerMarketLineData == null){
				Integer maxActual = 0;
				Integer maxForecast = 0;
				Integer maxOrder = 0;
				NBCampBTQCostPerMarketLineData = new LineChartWrapper();
				
				for(Campaign_Market__c cm : campaignMarketList){
					maxActual = Math.MAX(cm.Total_Actual_Costs_Eur__c != null ? Integer.valueOf(cm.Total_Actual_Costs_Eur__c) : 0, maxActual);
					maxForecast = Math.MAX(cm.Total_Max_Costs_Eur__c != null ? Integer.valueOf(cm.Total_Max_Costs_Eur__c) : 0, maxForecast);
					maxOrder = Math.MAX(mapCampBtqOrderPerMktCount.get(cm.Id), maxOrder);
				}
				
				NBCampBTQCostPerMarketLineData.ratio = Math.MAX(maxActual, maxForecast) / Math.MAX(maxOrder, 1);
				NBCampBTQCostPerMarketLineData.ratio = (NBCampBTQCostPerMarketLineData.ratio!=0) ? NBCampBTQCostPerMarketLineData.ratio : 1;
				
				for(Campaign_Market__c cm : campaignMarketList){
					NBCampBTQCostPerMarketLineData.add(
						cm.Name, 
						mapCampBtqOrderPerMktCount.get(cm.Id), 
						cm.Total_Actual_Costs_Eur__c != null ? Integer.valueOf(cm.Total_Actual_Costs_Eur__c) : 0, 
						cm.Total_Max_Costs_Eur__c != null ? Integer.valueOf(cm.Total_Max_Costs_Eur__c) : 0
					);
				}
			}
			return NBCampBTQCostPerMarketLineData;
		} 
		private set;
	}
	
	//BAR CHART WRAPPER
	public class BarChartWrapper{
		public List<String> labels { public get; private set; }
		public List<BarChartDataWrapper> datasets { public get; private set; }
		
		public BarChartWrapper(){
			labels = new List<String>(); 
			datasets = new List<BarChartDataWrapper>{new BarChartDataWrapper()};
		}

		public void add(String name, Integer value){
			labels.add((name.length() > 10) ? name.substring(0, 10) + '...' : name);
			datasets[0].add(value);
		}
	}
	
	public class BarChartDataWrapper{
		public String fillColor { public get; private set; }
		public String highlightFill { public get; private set; }
		public List<Integer> data { public get; private set; }
		
		public BarChartDataWrapper(){
			data = new List<Integer>();
      		fillColor = 'rgba(213,107,21,0.1)';
      		highlightFill = 'rgba(129,129,129,0.1)';
		}
		
		public void add(Integer value){
			data.add(value);
		}
	}
	
	public String NBCampBtqPerMktBarDataJSON { get{return JSON.serialize(NBCampBtqPerMktBarData); } }
	private BarChartWrapper NBCampBtqPerMktBarData { 
		get{
			if(NBCampBtqPerMktBarData == null){
				NBCampBtqPerMktBarData = new BarChartWrapper();
				for(Campaign_Market__c cm : campaignMarketList){
					NBCampBtqPerMktBarData.add(cm.Name, mapCampBtqOrderPerMktCount.get(cm.Id));
				}
			}
			return NBCampBtqPerMktBarData;
		} 
		private set;
	}
}
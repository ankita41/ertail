/*
*   @Class      :   ERTAILCampaignDashboardControllerTest.cls
*   @Description:   Test methods for class ERTAILCampaignDashboardController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILCampaignDashboardControllerTest {
    
    static testMethod void myUnitTest() {

		Id cmpId = ERTAILTestHelper.createCampaignMME();

	   	Campaign__c cmp = [select Id from Campaign__c where Id =: cmpId];

		ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);

		ERTAILCampaignDashboardController ctrl = new ERTAILCampaignDashboardController(stdController);

		String NBVisualsPerStatusDonutDataJSON = ctrl.NBVisualsPerStatusDonutDataJSON;

		String NBProdCostPerStatusDonutDataJSON = ctrl.NBProdCostPerStatusDonutDataJSON;

		String NBBoutiquePerCreativityStatusDonutDataJSON = ctrl.NBBoutiquePerCreativityStatusDonutDataJSON;

		String NBBoutiquePerQuantityStatusDonutDataJSON = ctrl.NBBoutiquePerQuantityStatusDonutDataJSON;

		String NBMarketPerCreativityStatusDonutDataJSON = ctrl.NBMarketPerCreativityStatusDonutDataJSON;

		String NBMarketPerQuantityStatusDonutDataJSON = ctrl.NBMarketPerQuantityStatusDonutDataJSON;

		String CampaignColorsJSON = ctrl.CampaignColorsJSON;

		String CampaignHighlightsJSON = ctrl.CampaignHighlightsJSON;

		String NBCampBTQCostPerMarketLineDataJSON = ctrl.NBCampBTQCostPerMarketLineDataJSON;

		String NBCampBtqPerMktBarDataJSON = ctrl.NBCampBtqPerMktBarDataJSON;
		
		List<String> monthsList = ctrl.monthsList;
		
		List<ERTAILCampaignDashboardController.TimeLineWrapper> TimeLineList = ctrl.TimeLineList;
    }
}
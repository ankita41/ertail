/*****
*   @Class      :   ERTAILBoutiqueDetailsExt.cls
*   @Description:   Extension developed for ERTAILCampaignGenerateCampaignItems.page, ERTAILCampaignGenerateRecommendations.page and ERTAILCampaignLockOrders.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
    Ankita Singhal  2 Apr 2017       Ertail enhancements 2017
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignExt {
    public class CustomException extends Exception{}
    public static Id CampaignItemShowWindowRTId = Schema.SObjectType.Campaign_Item__c.getRecordTypeInfosByName().get('Show Window').getRecordTypeId();
    public static Id CampaignItemInStoreRTId = Schema.SObjectType.Campaign_Item__c.getRecordTypeInfosByName().get('InStore').getRecordTypeId();
    
    Campaign__c campaign;
    
    public ERTAILCampaignExt(ApexPages.StandardController stdController) {
        this.campaign = (Campaign__c)stdController.getRecord();
    }
    
    public PageReference GenerateCampaignItems(){
        // Request all Campaign boutique for the campaign
        // Extract the Map of Campaign Boutiques ordered by RTCOM Boutique Id
        Map<Id, Campaign_Boutique__c> campaignBtqsOrderedByBtqIdMap = new Map<Id, Campaign_Boutique__c>();
        for(Campaign_Boutique__c cb : [Select c.RTCOMBoutique__c, Scope_limitation__c From Campaign_Boutique__c c where c.Campaign_Market__r.Campaign__c =: campaign.Id]){
            campaignBtqsOrderedByBtqIdMap.put(cb.RTCOMBoutique__c, cb);
        }

        if (campaignBtqsOrderedByBtqIdMap.size() == 0){
            return new PageReference('/' + campaign.Id);
            throw new CustomException('There are no Boutiques selected for the campaign');
        }
        
        // Request the Topics for the campaign
        Set<Id> topicsSet = new Set<Id>();
        for (Campaign_Topic__c ct : [Select c.Topic__c From Campaign_Topic__c c where c.Campaign__c = :campaign.Id]){
            topicsSet.add(ct.Topic__c);
        }
        
        Set<Id> spacesNotLinkedToAnyTopic = new Set<Id>(); 
        Set<Id> spacesLinkedToASelectedTopic = new Set<Id>();  
        Set<Id> spacesLinkedToDisplayTopic = new Set<Id>();  // Added for req 13
        // Get the full list of Spaces 
        // Extract the list of Spaces that are not linked to any topics or that are linked to a topic part of the campaign
        for(Space__c space : [Select s.Name, s.Id, (Select Space__c, Topic__c,Topic__r.Name From Space_Topics__r) From Space__c s]){
            if (space.Space_Topics__r == null || space.Space_Topics__r.size() == 0){
                spacesNotLinkedToAnyTopic.add(space.Id);
            }else{
                for(Space_Topic__c st : space.Space_Topics__r){
                    if (topicsSet.contains(st.Topic__c)){
                        spacesLinkedToASelectedTopic.add(space.Id);
                        if(st.Topic__r.Name=='Display'){                    //added for req 13
                            spacesLinkedToDisplayTopic.add(Space.id);
                        }
                        break;
                    }
                }
            }
        }
        
        Set<Id> spaceCandidates = new Set<Id>();
        spaceCandidates.addAll(spacesNotLinkedToAnyTopic);
        spaceCandidates.addAll(spacesLinkedToASelectedTopic);
        
        if (spaceCandidates.size() == 0){
            return new PageReference('/' + campaign.Id);            
            throw new CustomException('There are no spaces candidates for the Campaign');
        }
        
        // Get the Boutique Spaces for the selected Boutiques with Decoration allowed and with the space part of the Space Candidates
        // Check if the boutique space should be part of the Campaign (depends on the Campaign Boutique scope)
        Map<Id, Space__c> spacesUsedByTheCampaignMap = new Map<Id, Space__c>();
        for (Boutique_Space__c bs : [Select b.Space__c, b.Space__r.Name, b.Space__r.RecordTypeId,b.Space__r.RecordType.developerName, b.Space__r.Decoration_Alias__c, b.Space__r.Default_Campaign_Item_Name__c, b.Space__r.Has_Creativity__c, b.Space__r.OP_max_cost_Eur__c, b.RTCOMBoutique__c, b.Space__r.Creativity_Color__c From Boutique_Space__c b where Decoration_Allowed__c = 'Yes' and Space__c in :spaceCandidates and b.RTCOMBoutique__c in :campaignBtqsOrderedByBtqIdMap.keySet() and b.Excluded_for_future_campaigns__c = false]){
            String btqScopeLimitation = campaignBtqsOrderedByBtqIdMap.get(bs.RTCOMBoutique__c).Scope_Limitation__c;
            if(ERTAILCampaignHandler.IsSpaceAvailableForCampaignBoutique(btqScopeLimitation, bs.Space__r, spacesNotLinkedToAnyTopic, spacesLinkedToASelectedTopic,spacesLinkedToDisplayTopic)){
                spacesUsedByTheCampaignMap.put(bs.Space__c, bs.Space__r);
            }
        }

        if (spacesUsedByTheCampaignMap.size() == 0){
            return new PageReference('/' + campaign.Id);            
            throw new CustomException('There are no Spaces with Decoration Allowed in the boutiques selected for the campaign');
        }
        
        // Request the existing generated campaign items extract the Space Ids
        Set<Id> spacesMappedToAnExistingCampaignItem = new Set<Id>();
        for (Campaign_Item__c ci : [Select Id, Space__c from Campaign_Item__c where Origine__c = 'Generated' and Campaign__c = :campaign.Id]){
            spacesMappedToAnExistingCampaignItem.add(ci.Space__c);
        }
              
        List<Campaign_Item__c> ciToInsertList = new List<Campaign_Item__c>();
        
        for (Space__c s : spacesUsedByTheCampaignMap.values()){
            // If there is already a generated Campaign Item for the space, continue
            if (spacesMappedToAnExistingCampaignItem.contains(s.Id))
                continue;
            
            String n = s.Name;
            if (s.Default_Campaign_Item_Name__c != null){
                n = s.Default_Campaign_Item_Name__c;
            }
            String alias = n;
            if(s.Decoration_Alias__c != null){
                alias = s.Decoration_Alias__c + (s.Has_Creativity__c ? '1' : '');
            }
            String color = (s.Creativity_Color__c != null ? s.Creativity_Color__c.split('\r\n')[0] : 'black');
            ciToInsertList.add(
                    new Campaign_Item__c(
                        Name = (s.Has_Creativity__c ? alias : n)
                        , Alias__c = alias
                        , Space__c = s.Id
                        , Origine__c = 'Generated'
                        , Campaign__c = campaign.Id
                        , Max_Cost_EUR__c = s.OP_max_cost_Eur__c
                        , Actual_Cost_EUR__c = s.OP_max_cost_Eur__c
                        , Proposed_Cost_EUR__c = s.OP_max_cost_Eur__c
                        , Creativity_Color__c = color
                        ,RecordtypeId=(s.Has_Creativity__c ? CampaignItemShowWindowRTId : CampaignItemInstoreRTId )));
        }
        
        if (ciToInsertList.size() > 0)
            insert ciToInsertList;
       /* List<CreativityInstore__c> Cinstore = new List<CreativityInstore__c>();
        for(Campaign_Item__c ci : ciToInsertList){
            if(ci.RecordtypeId==CampaignItemInstoreRTId ){
             Cinstore.add(new CreativityInstore__c(CampaignItem__c=ci.id,Name='Creativity-1')); 
             Cinstore.add(new CreativityInstore__c(CampaignItem__c=ci.id,Name='Creativity-2')); 
             Cinstore.add(new CreativityInstore__c(CampaignItem__c=ci.id,Name='Creativity-3')); 
            }
        }
         if (Cinstore.size() > 0)
            insert Cinstore ;*/
        // Change the campaign record type
        campaign.recordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Creativity').getRecordTypeId();
        update campaign;
        PageReference ERTAILCampaignItemsPage = Page.ERTAILCampaignItemList;
        ERTAILCampaignItemsPage.getParameters().put('Id', campaign.Id);
        return ERTAILCampaignItemsPage;
    }
    
    
     public PageReference GenerateRecommendations(){
        // Get the list of Boutiques for a campaign
        List<Campaign_market__c> cm= new List<Campaign_market__c>([Select id from Campaign_market__c where Campaign__c=:campaign.id]);
        if(cm.size()>0){
            if (ERTAILCampaignHandler.GenerateRecommendations(campaign)){
                PageReference ERTAILMaster = Page.ERTAILMaster;
                ERTAILMaster.getParameters().put('Id', campaign.Id);
                return ERTAILMaster;
            }
        }
        else{
             return new PageReference('/' + campaign.Id);            
          }
        return null;
    }
    
    public PageReference LockOrders(){
        Map<Id, Boutique_Order_Item__c> boiToUpdateMap = new Map<Id, Boutique_Order_Item__c>();
        for (Boutique_Order_Item__c boi : [Select Id, Creativity_Step_Picklist__c, Quantity_Step_Picklist__c from Boutique_Order_Item__c where Campaign__c = :campaign.Id]){
            if (boi.Creativity_Step_Picklist__c != null){
                boi.Creativity_Step_Picklist__c = '70';
                boiToUpdateMap.put(boi.Id, boi);
            }
            if (boi.Quantity_Step_Picklist__c != null){
                boi.Quantity_Step_Picklist__c = '70';
                boiToUpdateMap.put(boi.Id, boi);
            }
        }
        
        if (boiToUpdateMap.size() > 0){
            update boiToUpdateMap.values();
            
            update new Campaign__c(Id=campaign.Id, Notify_Orders_Locked__c = true);
        }
        
        return new PageReference('/' + campaign.Id);
    }
}
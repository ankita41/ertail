/*
*   @Class      :   ERTAILCampaignExtTest.cls
*   @Description:   Test methods for class ERTAILCampaignExt.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILCampaignExtTest {
    
    static testMethod void myUnitTest() {

        Id cmpId = ERTAILTestHelper.createCampaignMME();

        Campaign__c cmp = [select Id ,Scope__c from Campaign__c where Id =: cmpId];
        cmp.Scope__c = 'In-Store Display only';
        update cmp;
        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);

        ERTAILCampaignExt ctrl = new ERTAILCampaignExt(stdController);

        ctrl.GenerateCampaignItems();
        ctrl.GenerateRecommendations();
        ctrl.LockOrders();
        
        

    }
    static testMethod void myUnitTest1() {

        Id cmpId = ERTAILTestHelper.createCampaignWithCampItems() ;
         Campaign__c cmp = [select Id from Campaign__c where Id =: cmpId];

        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);

        ERTAILCampaignExt ctrl = new ERTAILCampaignExt(stdController);

        ctrl.GenerateRecommendations();
      
    }
}
/*****
*   @Class      :   ERTAILCampaignHandler.cls
*   @Description:   Static methods called by triggers and other controllers.
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   TPA             05-Aug-2015     Fix issue with new BOI being created if the Boutique spaces are created after MPM has proposed to order a Campaign Item for a space that initially doesn't exist in the boutique.                        
    Ankita           2 Apr 2017     Ertail enhancements 2017
*****/

public with sharing class ERTAILCampaignHandler {
    public class CustomException extends Exception{}

    public static Id spaceShowWindowRTId = Schema.SObjectType.Space__c.getRecordTypeInfosByName().get('Show Window').getRecordTypeId();
    public static Id spaceInStoreRTId = Schema.SObjectType.Space__c.getRecordTypeInfosByName().get('In Store').getRecordTypeId();
    public static Id boiShowWindowRTId = Schema.SObjectType.Boutique_Order_Item__c.getRecordTypeInfosByName().get('Show Window').getRecordTypeId();
    public static Id boiInStoreRTId = Schema.SObjectType.Boutique_Order_Item__c.getRecordTypeInfosByName().get('In-Store').getRecordTypeId();  
    
    public static Id cmffOrderingRTId = Schema.SObjectType.Campaign_Market__c.getRecordTypeInfosByName().get('Fixture and Furnitures ordering').getRecordTypeId();

    public static void updateCampaignAfterCampaignMarketInsertOrDelete(Map<Id, Campaign_Market__c> campaignMarketMap){
        Set<Id> campaignIds = new Set<Id>();
        for(Campaign_Market__c campaignMarket : campaignMarketMap.Values()){
            if (campaignMarket.Campaign__c != null)
                campaignIds.add(campaignMarket.Campaign__c);
        }
        if (campaignIds.size() > 0)
            ERTAILCampaignRollupfieldscalulation.updateRollUpsInCampaigns(campaignIds);
    }   
    
    public static void DeleteChildrenAfterCampaignDelete(Map<Id, Campaign__c> campaignMap){
        delete [Select Id from Campaign_Market__c where Campaign__c in :campaignMap.keySet()];
        delete [Select Id from Campaign_Topic__c where Campaign__c in :campaignMap.keySet()];
    }
    
    public static void updateCampaignAfterCampaignMarketUpdate(Map<Id, Campaign_Market__c> oldMap, Map<Id, Campaign_Market__c> newMap){
        Set<Id> campaignIds = new Set<Id>();
        for(ID campaignMarketId : oldMap.keySet()){
            if(oldMap.get(campaignMarketId).OP_Max_Agency_Fee__c != newMap.get(campaignMarketId).OP_Max_Agency_Fee__c
                || (oldMap.get(campaignMarketId).Agency_Actual_Fees__c != newMap.get(campaignMarketId).Agency_Actual_Fees__c)
                || (oldMap.get(campaignMarketId).Installation_Fee_Eur__c != newMap.get(campaignMarketId).Installation_Fee_Eur__c)
                || (oldMap.get(campaignMarketId).Delivery_Fee_Eur__c != newMap.get(campaignMarketId).Delivery_Fee_Eur__c)
                || (oldMap.get(campaignMarketId).OP_actual_cost_Eur__c != newMap.get(campaignMarketId).OP_actual_cost_Eur__c)
                || (oldMap.get(campaignMarketId).OP_max_cost_Eur__c != newMap.get(campaignMarketId).OP_max_cost_Eur__c)
                || (oldMap.get(campaignMarketId).Creativity_Step_Value__c != newMap.get(campaignMarketId).Creativity_Step_Value__c)
                || (oldMap.get(campaignMarketId).Quantity_Step_Value__c != newMap.get(campaignMarketId).Quantity_Step_Value__c)
                || (oldMap.get(campaignMarketId).Delivery_Fee_Step_Value__c != newMap.get(campaignMarketId).Delivery_Fee_Step_Value__c)
                || (oldMap.get(campaignMarketId).Installation_Fee_Step_Value__c != newMap.get(campaignMarketId).Installation_Fee_Step_Value__c)
                || (oldMap.get(campaignMarketId).Fixture_Furnitures_Costs_Eur__c != newMap.get(campaignMarketId).Fixture_Furnitures_Costs_Eur__c))
                {
                    if (newMap.get(campaignMarketId).Campaign__c != null)
                        campaignIds.add(oldMap.get(campaignMarketId).Campaign__c);
            }
            
        }

        if(!campaignIds.isEmpty()){
           ERTAILCampaignRollupfieldscalulation.updateRollUpsInCampaigns(campaignIds);
        }
    }

   /* private static void updateRollUpsInCampaigns(Set<Id> campaignIds){
        List<Campaign__c> campaignList = new List<Campaign__c>();
        //throw new CustomException('' + campaignIds);
        for(AggregateResult q : [select Campaign__c
                                , sum(OP_Max_Agency_Fee__c) OP_Max_Agency_Fees
                                , sum(Agency_Actual_Fees__c) Agency_Actual_Fees
                                , sum(Installation_Fee_Eur__c) Installation_Fee_Eur
                                , sum(Delivery_Fee_Eur__c) Delivery_Fee_Eur
                                , sum(OP_actual_cost_Eur__c) OP_actual_cost_Eur
                                , sum(OP_max_cost_Eur__c) OP_max_cost_Eur
                                , min(Creativity_Step_Value__c) Creativity_Step_Value
                                , min(Quantity_Step_Value__c) Quantity_Step_Value
                                , min(Delivery_Fee_Step_Value__c) Delivery_Step_Value
                                , min(Installation_Fee_Step_Value__c) Installation_Step_Value
                                , sum(Fixture_Furnitures_Costs_Eur__c) FF_Costs_Eur
                                from Campaign_Market__c 
                                where Campaign__c IN :campaignIds 
                                group by Campaign__c]){
                Campaign__c campaign = new Campaign__c();
                campaign.Id = (Id)q.get('Campaign__c');
                campaign.Agency_Actual_Fees__c = (Decimal) q.get('Agency_Actual_Fees');
                campaign.OP_Max_Agency_Fees__c = (Decimal) q.get('OP_Max_Agency_Fees');
                campaign.Creativity_Step_Value__c = (Decimal) q.get('Creativity_Step_Value');
                campaign.Quantity_Step_Value__c = (Decimal) q.get('Quantity_Step_Value');
                campaign.Delivery_Cost_Eur__c = (Decimal) q.get('Delivery_Fee_Eur');
                campaign.Installation_Cost_Eur__c = (Decimal) q.get('Installation_Fee_Eur');
                campaign.Actual_Cost_Eur__c = (Decimal) q.get('OP_actual_cost_Eur');
                campaign.Max_Cost_Eur__c = (Decimal) q.get('OP_max_cost_Eur');
                campaign.Delivery_Cost_Step_Value__c = (Decimal) q.get('Delivery_Step_Value');
                campaign.Installation_Cost_Step_Value__c = (Decimal) q.get('Installation_Step_Value');
                campaign.Fixture_Furnitures_Costs_Eur__c = (Decimal) q.get('FF_Costs_Eur');
                campaignList.add(campaign);
        }
        if (campaignList.size() > 0)
            update campaignList;
    }
    */
    
    public static List<Campaign_Item__c> GetCampaignItemsForCampaign(Id campaignId){
        return [Select c.Name
                    , c.Space__c
                    , c.Space__r.Decoration_Alias__c
                    , c.Creativity__c
                    , c.Image_Id__c 
                    , c.Creativity_Color__c
                    , c.Space__r.Has_Creativity__c
                    , c.Space__r.Has_Quantity__c
                   // ,(Select Id, CampaignItem__c,Image_ID__c,Image_medium__c,name From Creativity_for_Instore_items__r)
            From Campaign_Item__c c 
            where c.Campaign__c = : campaignId 
            order by c.Creativity__c, c.Name];
    }
    
    public static Boolean GenerateRecommendations(Campaign__c campaign){
       /* Set<Id> selectedBoutiques = new Set<Id>();
        for (Campaign_Boutique__c c : [Select Id, RTCOMBoutique__c from Campaign_Boutique__c where Campaign_Market__r.Campaign__c = : campaign.Id]){
            selectedBoutiques.add(c.RTCOMBoutique__c);
        }
        return GenerateRecommendations(campaign, new List<Id>(selectedBoutiques));*/
        Id userId = Userinfo.getUserId();
        User u = [Select u.Name, u.Profile.Name, Email,u.FirstName From User u where id=:userId limit 1];
       GenerateRecommendationBatch c = new GenerateRecommendationBatch(campaign, u);
        Database.executeBatch(c,10);
        return true;
        
    }
    
    // Generate Campaign Items recommandations based on the Boutique scope, campaign topics and selected Market/Boutiques and Boutique spaces.
    public static Boolean GenerateRecommendations(Campaign__c campaign, List<Id> selectedBoutiques){
        // Request the selected Boutiques for the campaign
        Map<Id, Campaign_Boutique__c> campaignBtqOrderedByBtqId = new Map<Id,Campaign_Boutique__c>();
        Set<Id> campaignBoutiqueIdsSet = new Set<Id>();
        for (Campaign_Boutique__c cb : [Select c.Id
                                            , c.RTCOMBoutique__c 
                                            , c.Campaign_Market__c
                                            , c.Scope_Limitation__c
                                        From Campaign_Boutique__c c 
                                        where c.RTCOMBoutique__c in :selectedBoutiques 
                                            and Campaign_Market__r.Campaign__c=:campaign.Id]){
            campaignBtqOrderedByBtqId.put(cb.RTCOMBoutique__c, cb);
            campaignBoutiqueIdsSet.add(cb.Id);
        }
        
        // Request all Campaign Items for the Campaign
        Map<Id, List<Campaign_Item__c>> campaignItemsOrderedBySpace = new Map<Id, List<Campaign_Item__c>>();
        // Map<Id, List<CreativityInstore__c>> campaignItemswithCreativity = new Map<Id, List<CreativityInstore__c>>(); -->
        List<Campaign_Item__c> CampaignItems = GetCampaignItemsForCampaign(campaign.Id);
        for(Campaign_Item__c ci : CampaignItems){
          /*  if(ci.Creativity_for_Instore_items__r!=null){
                 campaignItemswithCreativity.put(ci.id,ci.Creativity_for_Instore_items__r );
            }*/
            if (!campaignItemsOrderedBySpace.containsKey(ci.Space__c))
                campaignItemsOrderedBySpace.put(ci.Space__c, new List<Campaign_Item__c>{ci});
            else
                campaignItemsOrderedBySpace.get(ci.Space__c).add(ci);
        }
        
        // Request the Campaign Items Market exclusion
        // Order it by Campaign Market id
        Map<Id, Set<Id>> excludedCampaignItemIdsOrganisedByCampaignMarketId = new Map<Id, Set<Id>>();
        for (Campaign_Item_Exclusion__c cie : [Select c.Name, c.Excluded_Campaign_Market__c, c.Campaign_Item__c From Campaign_Item_Exclusion__c c where c.Excluded_Campaign_Market__r.Campaign__c = :campaign.Id]){
            if (!excludedCampaignItemIdsOrganisedByCampaignMarketId.containsKey(cie.Excluded_Campaign_Market__c))
                excludedCampaignItemIdsOrganisedByCampaignMarketId.put(cie.Excluded_Campaign_Market__c, new Set<Id>{cie.Campaign_Item__c});
            else
                excludedCampaignItemIdsOrganisedByCampaignMarketId.get(cie.Excluded_Campaign_Market__c).add(cie.Campaign_Item__c);
        }
        
        // Request the Topics for the campaign
        Set<Id> topicsSet = new Set<Id>();
        for (Campaign_Topic__c ct : [Select c.Topic__c From Campaign_Topic__c c where c.Campaign__c = :campaign.Id]){
            topicsSet.add(ct.Topic__c);
        }
        
        Set<Id> spacesNotLinkedToAnyTopic = new Set<Id>(); 
        Set<Id> spacesLinkedToASelectedTopic = new Set<Id>();
        Set<Id> spacesLinkedToDisplayTopic = new Set<Id>(); 
          
        // Get the full list of Spaces 
        // Extract the list of Spaces that are not linked to any topics or that are linked to a topic part of the campaign
        for(Space__c space : [Select s.Name, s.Id, (Select Space__c, Topic__c,topic__r.name From Space_Topics__r) From Space__c s]){
             if (space.Space_Topics__r == null || space.Space_Topics__r.size() == 0){
                spacesNotLinkedToAnyTopic.add(space.Id);
            }else{
                for(Space_Topic__c st : space.Space_Topics__r){
                    if (topicsSet.contains(st.Topic__c)){
                        spacesLinkedToASelectedTopic.add(space.Id);
                      //Added for req 13
                       if(st.Topic__r.Name=='Display'){
                            spacesLinkedToDisplayTopic.add(Space.id);
                        }
                        break;
                    }
                }
            }
        }
        
        // Request the existing Boutique Order Items for the campaign, only the creativity and Quantity ones
        Map<Id, Boutique_Order_Item__c> existingBtqOrderItems = new Map<Id, Boutique_Order_Item__c>([Select Id
                                                                                                        , Campaign_Item_to_Order__c
                                                                                                        , Campaign_Item_to_Order__r.Space__c
                                                                                                        , Campaign_Item_to_Order__r.Space__r.Has_Quantity__c
                                                                                                        , Recommended_Campaign_Item__c
                                                                                                        , Recommended_Quantity__c
                                                                                                        , Recommended_Campaign_Item__r.Space__c
                                                                                                        //, Recommended_Campaign_Item__r.Space__r.Has_Quantity__c
                                                                                                        , Campaign_Boutique__r.RTCOMBoutique__c
                                                                                                        , Boutique_Space__c
                                                                                                        , Boutique_Space__r.Space__c
                                                                                                        , Boutique_Space__r.Space__r.Has_Creativity__c
                                                                                                        , RecordTypeId
                                                                                                    from Boutique_Order_Item__c 
                                                                                                    where Campaign_Boutique__r.Campaign_Market__r.Campaign__c = :campaign.Id 
                                                                                                        and (Boutique_Space__r.Space__r.Has_Creativity__c = true // This for creativity BOI
                                                                                                            or Campaign_Item_to_Order__r.Space__r.Has_Quantity__c = true) // This for Quantity BOI
                                                                                                        and Permanent_Fixture_And_Furniture__c = null
                                                                                                        and Campaign_Boutique__r.RTCOMBoutique__c in :selectedBoutiques]);

        
        
        
        // Clone the original boi in working maps and clear all the recommended values
        Map<Id, Boutique_Order_Item__c> existingBoiWithCreativityOrderedByBtqSpaceId = new Map<Id, Boutique_Order_Item__c>();
        Map<Id, Map<Id, Boutique_Order_Item__c>> existingBoiWithQuantityOrderedByBoutiqueThenSpaceId = new Map<Id, Map<Id, Boutique_Order_Item__c>>();
        
        for (Boutique_Order_Item__c originalBoi : existingBtqOrderItems.values()){
            Boutique_Order_Item__c boi = originalBoi.clone();
            boi.Id = originalBoi.Id;
            // set the creativity list
            if (boi.Boutique_Space__c != null && boi.Boutique_Space__r.Space__r.Has_Creativity__c){
                existingBoiWithCreativityOrderedByBtqSpaceId.put(boi.Boutique_Space__c, boi);
            }
            
            // set the Quantity Map, reset the recommended Quantity
            // TPA 05-Aug-2015
            //if (boi.Recommended_Campaign_Item__r.Space__r.Has_Quantity__c){
            if (boi.Campaign_Item_to_Order__r.Space__r.Has_Quantity__c){
                boi.Recommended_Quantity__c = null;
                if (!existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.containsKey(boi.Campaign_Boutique__r.RTCOMBoutique__c)){
                    existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.put(boi.Campaign_Boutique__r.RTCOMBoutique__c, new Map<Id, Boutique_Order_Item__c>());
                }
                existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.get(boi.Campaign_Boutique__r.RTCOMBoutique__c).put(boi.Campaign_Item_to_Order__r.Space__c, boi);
            }
        }
        
        // Request all Boutique Spaces with Decoration allowed
        // If there is no already existing recommandations create a new one, otherwise recalculate the existing one
        Id currentSpaceId;
        Id currentBoutiqueId;
        Integer position;
        Campaign_Item__c campaignItem;
        List<Campaign_Item__c> ciList;
        Boolean btqSpaceToInclude;
        Map<Id, Boutique_Order_Item__c> boiToUpdateMap = new Map<Id, Boutique_Order_Item__c>();
        List<Boutique_Order_Item__c> boiToInsertList = new List<Boutique_Order_Item__c>();
        
        for(Boutique_Space__c bs : [Select b.Id
                                        , b.Quantity_to_Order__c
                                        , b.RTCOMBoutique__c
                                        , b.Space__c
                                        , b.Space__r.Has_Creativity__c
                                        , b.Space__r.Has_Quantity__c 
                                        , b.Space__r.RecordTypeId
                                    From Boutique_Space__c b 
                                    where (b.RTCOMBoutique__c in :selectedBoutiques) 
                                        and b.Decoration_Allowed__c = 'Yes' 
                                        and b.Excluded_for_future_campaigns__c = false
                                    order by b.RTCOMBoutique__r.RTCOMMarket__r.Name
                                        , b.RTCOMBoutique__c
                                        , b.Space__c
                                        , b.Creativity_Order__c]){
            
            // Check if the space should be part of the campaign, based on the Boutique scope and selected topics
                
            if (!IsSpaceAvailableForCampaignBoutique(campaignBtqOrderedByBtqId.get(bs.RTCOMBoutique__c).Scope_Limitation__c, bs.Space__r, spacesNotLinkedToAnyTopic, spacesLinkedToASelectedTopic,spacesLinkedToDisplayTopic))
            {
                if (bs.Space__r.Has_Creativity__c && existingBoiWithCreativityOrderedByBtqSpaceId.containsKey(bs.Id)){
                    Boutique_Order_Item__c boi = existingBoiWithCreativityOrderedByBtqSpaceId.get(bs.Id);
                    if(boi.Recommended_Campaign_Item__c != null){
                        boi.Recommended_Campaign_Item__c = null;
                        boiToUpdateMap.put(boi.Id, boi);   
                    }
                }
                if (bs.Space__r.Has_Quantity__c && existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.containsKey(bs.RTCOMBoutique__c) && existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.get(bs.RTCOMBoutique__c).containsKey(bs.Space__c)){
                    Boutique_Order_Item__c boi = existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.get(bs.RTCOMBoutique__c).get(bs.Space__c);
                    if(boi.Recommended_Campaign_Item__c != null){
                        boi.Recommended_Campaign_Item__c = null;
                        boiToUpdateMap.put(boi.Id, boi);   
                    }
                }
                    
                continue;
            }
            
            // If the Boutique Space has creativity
            if (bs.Space__r.Has_Creativity__c){
                if (bs.Space__c != currentSpaceId || bs.RTCOMBoutique__c != currentBoutiqueId){
                    currentSpaceId = bs.Space__c;
                    currentBoutiqueId = bs.RTCOMBoutique__c;
                    position = 0;
                    ciList = CampaignItemsForCampaignMarketAndSpace(campaignItemsOrderedBySpace.get(currentSpaceId), excludedCampaignItemIdsOrganisedByCampaignMarketId.get(campaignBtqOrderedByBtqId.get(currentBoutiqueId).Campaign_Market__c));
                }           
                
                // If there is no Campaign Item for the Space
                if (ciList.size() == 0)
                    continue;
                
                if (ciList.size() <= position){
                    position = 0;
                }
                
                campaignItem = ciList[position++];
                
                if (existingBoiWithCreativityOrderedByBtqSpaceId.containsKey(bs.Id)){
                    // Check if the recommended values has changed
                    Boutique_Order_Item__c boi = existingBoiWithCreativityOrderedByBtqSpaceId.get(bs.Id);
                    if(boi.Recommended_Campaign_Item__c != campaignItem.Id){
                        boi.Recommended_Campaign_Item__c = campaignItem.Id;
                        boiToUpdateMap.put(boi.Id, boi);   
                    }
                }else{
                    String creativityStep = '10';
                    Id campaignBoutiqueId = campaignBtqOrderedByBtqId.get(bs.RTCOMBoutique__c).Id;
                    
                    boiToInsertList.add(new Boutique_Order_Item__c(
                                                            Recommended_Campaign_Item__c = campaignItem.Id
                                                            , Recommended_Quantity__c = 1
                                                            , Campaign_Boutique__c = campaignBoutiqueId
                                                            , Boutique_Space__c  = bs.Id
                                                            , Campaign__c = campaign.Id
                                                            , Creativity_Step_Picklist__c = creativityStep
                                                            , Quantity_Step_Picklist__c = null
                                                            , RecordTypeId = boiShowWindowRTId));   
                                
                }
            }
            
            // If the Boutique Space has Quantity
            if (bs.Space__r.Has_Quantity__c){
                if (!existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.containsKey(bs.RTCOMBoutique__c)){
                    existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.put(bs.RTCOMBoutique__c, new Map<Id, Boutique_Order_Item__c>());
                }
                
                Map<Id, Boutique_Order_Item__c> existingBoiForBtqOrderedBySpaceId = existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.get(bs.RTCOMBoutique__c);
            
                if (existingBoiForBtqOrderedBySpaceId.containsKey(bs.Space__c)){
                    Boutique_Order_Item__c boi = existingBoiForBtqOrderedBySpaceId.get(bs.Space__c);
                    if (boi.Recommended_Quantity__c == null){
                        boi.Boutique_Space__c  = bs.Id; // first BOI for the space
                        boi.Recommended_Campaign_Item__c = boi.Campaign_Item_to_Order__c;
                    }else{
                        boi.Boutique_Space__c  = null; // Multiple BOI for the space
                    }
                    boi.Recommended_Quantity__c = (boi.Recommended_Quantity__c == null ? 0 : boi.Recommended_Quantity__c) + bs.Quantity_to_Order__c;
                    
                }else if (campaignItemsOrderedBySpace.containsKey(bs.Space__c)){
                    String quantityStep = '10';
                    Id campaignBoutiqueId = campaignBtqOrderedByBtqId.get(bs.RTCOMBoutique__c).Id;
                    Boutique_Order_Item__c boi = new Boutique_Order_Item__c(
                                                            Recommended_Campaign_Item__c = campaignItemsOrderedBySpace.get(bs.Space__c)[0].Id
                                                            , Recommended_Quantity__c = bs.Quantity_to_Order__c
                                                            , Campaign_Boutique__c = campaignBoutiqueId
                                                            // MME boutique space reference added on 23.02.2015
                                                            , Boutique_Space__c  = bs.Id
                                                            , Campaign__c = campaign.Id
                                                            , Quantity_Step_Picklist__c = quantityStep
                                                            , Creativity_Step_Picklist__c = null
                                                            , RecordTypeId = boiInStoreRTId);
                                                            
                    existingBoiForBtqOrderedBySpaceId.put(bs.Space__c, boi);
                    
                }
            } 
        }
                
        // add to boiToInsertList the Boutique Order Items with Quantity that are new or changed    
        for (Id btqId : existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.keySet()){
            for (Boutique_Order_Item__c boi : existingBoiWithQuantityOrderedByBoutiqueThenSpaceId.get(btqId).values()){
                if (boi.Id == null){
                    boiToInsertList.add(boi);
                    continue;
                }
                Boutique_Order_Item__c originalBoi = existingBtqOrderItems.get(boi.Id);
                if (boi.Recommended_Quantity__c != originalBoi.Recommended_Quantity__c){
                    boiToUpdateMap.put(boi.Id, boi);
                }
            }
        }  
        
        if (boiToInsertList.size() > 0)
            upsert boiToInsertList;
        if (boiToUpdateMap.size() > 0)
            update boiToUpdateMap.values();
      /* List<Creativity_items__c> CreativityItems = new List<Creativity_items__c>();
        for(Boutique_Order_Item__c boi : boiToInsertList){
         if(campaignItemswithCreativity.containskey(boi.Recommended_Campaign_Item__c)){
                for(CreativityInstore__c ci:campaignItemswithCreativity.get(boi.Recommended_Campaign_Item__c) ){
                    if(ci.name =='Creativity-1')
                       CreativityItems.add(new Creativity_items__c(Boi__c = boi.id,CreativityInstore__c=ci.id,name=ci.name,HQPM_quantity__c =boi.Recommended_Quantity__c,MPM_Quantity__c=boi.Recommended_Quantity__c,Quantity__c = boi.Recommended_Quantity__c)) ;
                    else
                        CreativityItems.add(new Creativity_items__c(Boi__c = boi.id,CreativityInstore__c=ci.id,name=ci.name)) ;
                       
                 
                }
            }
        }
         if (CreativityItems.size() > 0)
            insert CreativityItems;*/
         System.debug('@@@@insidefor');   
        campaign.recordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Recommendations').getRecordTypeId();
        update campaign;
        return true;
    }
    
    // Remove from the full list of canpaign items, the campaign items to exclude
    private static List<Campaign_Item__c> CampaignItemsForCampaignMarketAndSpace(List<Campaign_Item__c> allCampaignItemsForSpace, Set<Id> campaignItemsToExclude){
        if (allCampaignItemsForSpace == null)
            return new List<Campaign_Item__c>();
        
        if (campaignItemsToExclude == null)
            return allCampaignItemsForSpace.clone();
            
        List<Campaign_Item__c> includedCampaignItems = new List<Campaign_Item__c>();
        for (Campaign_Item__c ci : allCampaignItemsForSpace){
            if (!campaignItemsToExclude.contains(ci.Id)){
                includedCampaignItems.add(ci);
            }
        }
        return includedCampaignItems;
    }
    
    // return true if the space should be included in the campaign for a boutique.  This is based on the Boutique scope limitation
   public static Boolean IsSpaceAvailableForCampaignBoutique(String btqScopeLimitation, Space__c space, Set<Id> spacesNotLinkedToAnyTopic, Set<Id> spacesLinkedToASelectedTopic,Set<Id> spacesLinkedToADisplayTopic){

        if (space == null)
            return false;

        // If the space (In-Store) is limited to a topic selected for the campaign, it is always included
       // if (spacesLinkedToASelectedTopic.contains(space.Id) && space.RecordTypeId == spaceInStoreRTId)
      //     return true;
            
        // If there is no scope limitation for the Campaign Boutique
        if (btqScopeLimitation == null){
            // if the space is a show window, the space is included
            if (space.RecordTypeId == spaceShowWindowRTId){
                return true;
            }
            // if the space is "in Store" not linked to any topic it is included
            else {
                //if (space.RecordTypeId == spaceInStoreRTId && spacesNotLinkedToAnyTopic.contains(space.Id)){
                if (space.RecordTypeId == spaceInStoreRTId) {   
                    return true;
                }
            }
            return false;
        }
        
        // if the scope limitation for the Btq is Show Window
        else if (ERTAILMasterHelper.SCOPE_SHOWWINDOW.equals(btqScopeLimitation) ){
            // if the space is a show window, the space is included
            if (space.RecordTypeId == spaceShowWindowRTId)  
                return true;
            return false;
        }
        
        // if the scope limitation for the Btq is In Store
        else if (ERTAILMasterHelper.SCOPE_INSTORE.equals(btqScopeLimitation) ){
            // if the space is not linked to any topic, the space is included
            if (space.RecordTypeId == spaceInStoreRTId) 
                return true;
            return false;
        }
        /*********Changes for Req - 13.***********/
        else if (ERTAILMasterHelper.SCOPE_INSTOREDISPLAY.equals(btqScopeLimitation) ){
            // if the space is not linked to any topic, the space is included
            if (spacesLinkedToADisplayTopic.contains(space.Id) && space.RecordTypeId == spaceInStoreRTId)
                return true; 
            return false;
        }
        /*********End*****************************/
        return false;
    }
   
}
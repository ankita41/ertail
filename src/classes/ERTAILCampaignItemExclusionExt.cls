/*****
*   @Class      :   ERTAILCampaignItemExclusionExt.cls
*   @Description:   Extension developed for ERTAILCampaignItemExclusion.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignItemExclusionExt {

	public class CustomException extends Exception{}

	public Map<String, Map<Id, CampaignMarketWrapper>> campaignMarketWrapperMaps {get;set;}

    public ERTAILCampaignItemExclusionExt(ApexPages.StandardController stdController) {
        Campaign_Item__c campaignItem = (Campaign_Item__c) stdController.getRecord();

		campaignMarketWrapperMaps = new Map<String,Map<Id, CampaignMarketWrapper>>();

		for(Campaign_Market__c cmpMarket : [Select Id, Name, RTCOMMarket__r.Name, RTCOMMarket__r.Zone__c From Campaign_Market__c Where Campaign__c =: campaignItem.Campaign__c Order By RTCOMMarket__r.Zone__c, Name]){
			String zone = cmpMarket.RTCOMMarket__r.Zone__c == null? '': cmpMarket.RTCOMMarket__r.Zone__c;

			if(campaignMarketWrapperMaps.get(zone) == null){
				campaignMarketWrapperMaps.put(zone, new Map<Id, CampaignMarketWrapper>());
			}
			campaignMarketWrapperMaps.get(zone).put(cmpMarket.Id, new CampaignMarketWrapper(cmpMarket, campaignItem.Id));
		}

		List<Campaign_Item_Exclusion__c> excludedCampaignMarkets = [Select Id, Excluded_Campaign_Market__c, Excluded_Campaign_Market__r.RTCOMMarket__r.Zone__c From Campaign_Item_Exclusion__c Where Campaign_Item__r.Id = : campaignItem.Id];
		
		for(Campaign_Item_Exclusion__c cmpItemExclusion : excludedCampaignMarkets){
			String zone = cmpItemExclusion.Excluded_Campaign_Market__r.RTCOMMarket__r.Zone__c == null? '': cmpItemExclusion.Excluded_Campaign_Market__r.RTCOMMarket__r.Zone__c;
			campaignMarketWrapperMaps.get(zone).get(cmpItemExclusion.Excluded_Campaign_Market__c).campaignItemExclusion = cmpItemExclusion;
			campaignMarketWrapperMaps.get(zone).get(cmpItemExclusion.Excluded_Campaign_Market__c).isExcluded = true;
		}
    }
	
	public Boolean state{get;set;}
	
	
	public void updateDb(){
		String zone = Apexpages.currentPage().getParameters().get('zone');
		Id cmpMarketId = (Id) Apexpages.currentPage().getParameters().get('cmpMarketId');
		CampaignMarketWrapper campaignMarketWrapper = campaignMarketWrapperMaps.get(zone).get(cmpMarketId);
		
		if(campaignMarketWrapper.isExcluded){
			campaignMarketWrapper.isExcluded = false;
			delete campaignMarketWrapper.campaignItemExclusion;
			campaignMarketWrapper.campaignItemExclusion = null;
		}
		else{
			campaignMarketWrapper.isExcluded = true;
			campaignMarketWrapper.campaignItemExclusion = new Campaign_Item_Exclusion__c(Campaign_Item__c = campaignMarketWrapper.campaignItemId, Excluded_Campaign_Market__c = campaignMarketWrapper.campaignMarket.Id);
			insert campaignMarketWrapper.campaignItemExclusion;
		}
	}
		
	public class CampaignMarketWrapper{
		public Id campaignItemId;
		public Campaign_Market__c campaignMarket {get;set;}
		public Boolean isExcluded {get;set;}
				
		public Campaign_Item_Exclusion__c campaignItemExclusion {get;set;}
	
		public CampaignMarketWrapper(Campaign_Market__c campaignMarket, Id campaignItemId){
			this.campaignMarket = campaignMarket;
			this.campaignItemId = campaignItemId;
			isExcluded = false;
		}
	}
}
/*
*   @Class      :   ERTAILCampaignItemExclusionExtTest.cls
*   @Description:   Test methods for class ERTAILCampaignItemExclusionExt.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILCampaignItemExclusionExtTest {
    
    static testMethod void myUnitTest() {
    	
	    Id testCampaignId = ERTAILTestHelper.createCampaignMME();

		Campaign_Boutique__c cmpBtq = [select Id, Campaign_Market__r.Id, RTCOMBoutique__c from Campaign_Boutique__c where Campaign_Market__r.Campaign__r.Id =: testCampaignId][0];

		Campaign_Item__c ci = [select Id, Campaign__c from Campaign_Item__c][0];
        
		ApexPages.StandardController stdController = new ApexPages.StandardController(ci);
		
		Test.startTest();

		PageReference pgRef = Page.ERTAILOrdersDetail;
		pgRef.getParameters().put('zone', 'Zone');
		pgRef.getParameters().put('cmpMarketId', cmpBtq.Campaign_Market__r.Id);
		Test.setCurrentPage(pgRef);

		ERTAILCampaignItemExclusionExt ctrl = new ERTAILCampaignItemExclusionExt(stdController);

		System.assertEquals(ctrl.campaignMarketWrapperMaps.size() > 0, true);

		ctrl.updateDb();		


/*        RTCOMBoutique__c btq = [select Id, RECO_Opening_Project__c, DefaultImageFormula_ID__c, RTCOMMarket__c, RECOBoutique__c, Default_Image__c, Default_Image_2__c, Default_Image_3__c from RTCOMBoutique__C where Id =: cmpBtq.RTCOMBoutique__c];

		ApexPages.StandardController stdController = new ApexPages.StandardController(btq);
		
		
		
		
		insert new RECO_Project__c(Boutique__c = btq.RECOBoutique__c, Project_Type__c = 'New');
		
		FeedItem post = new FeedItem(parentId = btq.Id);
		post.Body = 'Enter post text here';
		post.LinkUrl = 'http://www.someurl.com';
		post.ContentData = Blob.valueOf('bla bla bla');
		post.ContentFileName = 'sample.pdf';
		insert post;
		
		ERTAILBoutiqueDetailsExt ctrl = new ERTAILBoutiqueDetailsExt(stdController);
		
		Boolean b = ctrl.canEditBoutique;
		b = ctrl.isHQPMorAdmin;
		
		
		List<Boutique_Space__c>  btqSpaceList = ctrl.ShowWindows;
		btqSpaceList = cstrl.inStores;
		
		List<RTCOMBoutique__c> btqList = ctrl.BoutiquesInSameMarket;

		List<FeedItem> Images = ctrl.Images;
		
		System.assertEquals(ctrl.Boutique != null, true);
		
		Integer i = ctrl.imagesRowsSize + ctrl.documentsRowsSize;
		
		FeedItem SelectedDocument  = ctrl.SelectedDocument;
	*/	
		Test.stopTest();
			
    }
}
/*****
*   @Class      :   ERTAILCampaignItemListExt.cls
*   @Description:   Extension developed for ERTAILCampaignItemList.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*  Ankita Singhal  2 Apr 2017       Ertail  Enhancements 2017  
----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignItemListExt {
    class CustomException extends Exception {}

    public Campaign__c campaign { get; private set;}
    
    public Campaign_Item__c newCampaignItemWithCreativity { get; set; }
    public Campaign_Item__c newCampaignItemWithoutCreativity { get; set; }
    public boolean showerror{ get; set; }
    public boolean showerrorInstore { get; set; }
    private List<Campaign_Item__c> CampaignItems {
        get{
            
//      throw new customexception(''+[Select c.Space__c, c.Space__r.Name, c.Alias__c, c.Space__r.Decoration_Alias__c, (Select Id, Quantity_to_Order__c From Boutique_Order_Items3__r), (Select Excluded_Campaign_Market__r.RTCOMMarket__r.Name From Campaign_Item_Exclusions__r order by Excluded_Campaign_Market__r.Name), c.Origine__c, c.Name, c.Campaign__c, c.Creativity__c, c.Space__r.Has_Creativity__c, c.Space__r.Category__c, Image_ID__c, Image_Small__c From Campaign_Item__c c where Campaign__c = :campaign.Id order by c.Space__r.Category__c, c.Alias__c, c.Name]);
            
            return [Select c.Space__c, c.Space__r.Name, c.Alias__c, c.Space__r.Decoration_Alias__c, (Select Id, Quantity_to_Order__c From Boutique_Order_Items3__r), (Select Excluded_Campaign_Market__r.RTCOMMarket__r.Name From Campaign_Item_Exclusions__r order by Excluded_Campaign_Market__r.Name), c.Origine__c, c.Name, c.Campaign__c, c.Creativity__c, c.Space__r.Has_Creativity__c, c.Space__r.Category__c, Image_ID__c, Image_Small__c From Campaign_Item__c c where Campaign__c = :campaign.Id order by c.Space__r.Category__c, c.Alias__c, c.Name];
        }
        set;
    }
    
    public Map<Id, List<String>> excludedMarketsPerCampaignId{
        get{
            if (excludedMarketsPerCampaignId == null){
                excludedMarketsPerCampaignId = new Map<Id, List<String>>();
                for (Campaign_Item__c ci : CampaignItems){
                    excludedMarketsPerCampaignId.put(ci.Id, new List<String>());
                    for(Campaign_Item_Exclusion__c cie : ci.Campaign_Item_Exclusions__r){
                        excludedMarketsPerCampaignId.get(ci.Id).add(cie.Excluded_Campaign_Market__r.RTCOMMarket__r.Name);
                    }
                }
            }
            return excludedMarketsPerCampaignId;
        }
        private set;
    }
    
    private void initCampaignLists(){
        campaignItemsWithCreativityResults = new List<Campaign_Item__c>();
        campaignItemsWithoutCreativityResults = new List<Campaign_Item__c>();
        // T1, T2, ... first
        // Then Key1, Key2, ...
        // Then the other ones
        Integer tIndex = 0;
        Integer kIndex = 0;

        for (Campaign_Item__c ci : CampaignItems){
            if (ci.Space__r.Has_Creativity__c){
               if(ERTAILMasterHelper.DECORATION_ALIAS_STANDARDSHOWWINDOW.equals(ci.Space__r.Decoration_Alias__c)){
                    if(tIndex < campaignItemsWithCreativityResults.size()){
                        campaignItemsWithCreativityResults.add(tIndex, ci);
                    }
                    else{
                        campaignItemsWithCreativityResults.add(ci);
                    }
                    tIndex++;
                }
                else if(ERTAILMasterHelper.DECORATION_ALIAS_KEY.equals(ci.Space__r.Decoration_Alias__c)){
                    if(tIndex + kIndex < campaignItemsWithCreativityResults.size()){
                        campaignItemsWithCreativityResults.add(tIndex + kIndex, ci);
                    }
                    else{
                        campaignItemsWithCreativityResults.add(ci);
                    }
                    kIndex++;
                }
                else{
                    campaignItemsWithCreativityResults.add(ci);
                }
            }else{
                campaignItemsWithoutCreativityResults.add(ci);
            }
        }
    }
    
    public void refreshLists(){
        campaignItemsWithCreativityResults = null;
        excludedMarketsPerCampaignId = null;
    }
    
    public List<Campaign_Item__c> campaignItemsWithCreativityResults{
        get{
            if (campaignItemsWithCreativityResults == null){
                initCampaignLists();
            }
            return campaignItemsWithCreativityResults;
        }
        private set;
    }
        
    public List<Campaign_Item__c> campaignItemsWithoutCreativityResults{
        get{
            if (campaignItemsWithoutCreativityResults == null){
                initCampaignLists();
            }
            return campaignItemsWithoutCreativityResults;
        }
        private set;
    }

    public class InStoreWrapper{
        public String cat {
            get;
            private set;
        }
        
        public List<Campaign_Item__c> campaignItems {
            get;
            private set;
        }
        
        public InStoreWrapper(String cat){
            this.cat = cat;
            campaignItems = new List<Campaign_Item__c>();
        }
    }

    public Map<Id, Integer> quantitiesToOrder{
        get{
            if (quantitiesToOrder == null){
                quantitiesToOrder = new Map<Id, Integer>();
                for (Campaign_Item__c ci : campaignItems){
                    if (ci.Boutique_Order_Items3__r.isEmpty()){
                        quantitiesToOrder.put(ci.Id, 0);    
                    }else{
                        Integer qty = 0;
                        for (Boutique_Order_Item__c boi : ci.Boutique_Order_Items3__r){
                            qty += boi.Quantity_to_Order__c == null ? 0 : Integer.valueOf(boi.Quantity_to_Order__c);
                        }
                        quantitiesToOrder.put(ci.Id, qty);
                    }
                    
                }
            }
            return quantitiesToOrder;
        }
        private set;
    }

    public List<InStoreWrapper> inStoreWrappers {
        get{
            List<InStoreWrapper> inStoreWrappers = new List<InStoreWrapper>();
            InStoreWrapper inStoreWrapper = null;
            String currentCategory = null;
            
            InStoreWrapper undefinedCategory = new InStoreWrapper('Undefined');
            
            for(Campaign_Item__c campaignItem : campaignItemsWithoutCreativityResults){
                if(campaignItem.Space__r.Category__c == null){
                    undefinedCategory.campaignItems.add(campaignItem);
                }
                else{
                    String campaignItemCategory = campaignItem.Space__r.Category__c;
                    if(campaignItemCategory != currentCategory){
                        currentCategory = campaignItemCategory;
                        inStoreWrapper = new InStoreWrapper(currentCategory);
                        inStoreWrapper.campaignItems.add(campaignItem);
                        inStoreWrappers.add(inStoreWrapper);
                    }
                    else{
                        inStoreWrapper.campaignItems.add(campaignItem);
                    }
                }
            }
            
            if(undefinedCategory.campaignItems.size() > 0){
                inStoreWrappers.add(undefinedCategory);
            }
            return inStoreWrappers;
        }
    }

    public List<Campaign_Item__c> campaignItemsResults{
        get{
            List<Campaign_Item__c> cmpItems = new List<Campaign_Item__c>(campaignItemsWithCreativityResults);
            cmpItems.addAll(campaignItemsWithoutCreativityResults);
            return cmpItems;
        }
    }
    
    public Map<Id,CollaborationGroupFeed > mapFeed {get;set;}
    
    public Map<Id, String> excludedMarketsPerCampaignItem{
        get{
            Map<Id, String> excludedMarketsPerCampaignItem = new Map<Id, String>();
                
            for(Campaign_Item__c cmpItem : campaignItemsResults){
                String excludedMarkets = '';
                for(Campaign_Item_Exclusion__c cmpItemExclusion : cmpItem.Campaign_Item_Exclusions__r){
                    excludedMarkets += cmpItemExclusion.Excluded_Campaign_Market__r.Name + '\n';
                }
                excludedMarketsPerCampaignItem.put(cmpItem.Id, excludedMarkets);
            }
            return excludedMarketsPerCampaignItem;
        }
        private set;
    }
    
    public String itemsToCreate{get;set;}
    
    public String campaignItemName {get;set;}
    public Set<id> DisplaySpaceId;
    public ERTAILCampaignItemListExt(ApexPages.StandardController stdController){
        this.campaign = (Campaign__c)stdController.getRecord();
        newCampaignItemWithCreativity = new Campaign_Item__c();
        newCampaignItemWithoutCreativity = new Campaign_Item__c();
        DisplaySpaceId = new Set<Id>();
        Showerror=False;
        ShowerrorInstore=False;
        
        //Start --Added for req 13
        if(campaign.Scope__c ==ERTAILMasterHelper.SCOPE_INSTOREDISPLAY){
            for(space_topic__c st: [select id,topic__c,space__c from space_topic__c where topic__r.Name='Display']){
                DisplaySpaceId.add(st.space__c);
            }
        }
        //End
        queryFeeds();
    }
    
    private void queryFeeds(){
        mapFeed = new Map<Id,CollaborationGroupFeed >();
        for(Campaign_Item__c campaignItem : campaignItemsResults){
            mapFeed.put(campaignItem.Id, new CollaborationGroupFeed ());
        }
        
        for(CollaborationGroupFeed  f : [
                SELECT ContentFileName, ParentId, RelatedRecordId 
                FROM CollaborationGroupFeed  
                WHERE ParentId IN :mapFeed.keySet() AND Type = 'ContentPost' 
                ORDER BY CreatedDate
            ]){
            mapFeed.put(f.ParentId, f);
        }
    }        
    
    public PageReference saveImages(){
        //CREATES NEW FEED CONTENTS
        List<FeedItem> newImages = new List<FeedItem>();
        for(Id objId : mapFeed.keySet()){
            if(mapFeed.get(objId).ContentFileName!=null && mapFeed.get(objId).RelatedRecordId==null){
                CollaborationGroupFeed cgf = mapFeed.get(objId);
                FeedItem fi = new FeedItem(ContentFileName = cgf.ContentFileName, ParentId = objId, RelatedRecordId=cgf.RelatedRecordId);
                newImages.add(fi);
            }
        }

        try{
            if(!newImages.isEmpty()){
                insert newImages;

                // Get the new feedItem Ids
                List<Id> feedItemIds = new List<Id>();
                for (FeedItem fi : newImages)
                    feedItemIds.add(fi.Id);
                
                // Get the related Records Ids
                Map<Id, Id> RelatedRecordIdsOrganisedByFeedItemIds = new Map<Id, Id>(); 
                for (FeedItem fi : [Select Id, RelatedRecordId from FeedItem where Id in :feedItemIds])
                    RelatedRecordIdsOrganisedByFeedItemIds.put(fi.Id, fi.RelatedRecordId);
    
                //STORES CONTENT IDs TO THE IMAGE FIELD OF THE OBJECT
                List<Campaign_Item__c> campaignItemsToUpdate = new List<Campaign_Item__c>();

                for (FeedItem fi : newImages){
                    campaignItemsToUpdate.add(new Campaign_Item__c(Id=fi.ParentId, Image_Id__c = RelatedRecordIdsOrganisedByFeedItemIds.get(fi.Id)));
                }
                //throw new CustomException('campaignItemsToUpdate:' + campaignItemsToUpdate);
                if (campaignItemsToUpdate.size() > 0)
                    update campaignItemsToUpdate;
                    
                //RESETS VIEW STATE
                newImages = new List<FeedItem>();   

                queryFeeds();
            }
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
        
        return null;
    }
    
    public void delImgCommit(){
        Id imgParentId = ApexPages.currentPage().getParameters().get('imgParentId');
        
        //DELETE IMAGE
        delete mapFeed.get(imgParentId);
        delete [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :mapFeed.get(imgParentId).RelatedRecordId];

        //UPDATE PARENT RECORD
        for(Campaign_Item__c campaignItem : campaignItemsResults){
            if(campaignItem.id == imgParentId){
                campaignItem.Image_ID__c = null;
                update campaignItem;
            }
        }

        queryFeeds();
    }
        
    public PageReference manageOrders(){
        PageReference ref = Page.ERTAILMaster;
        ref.getParameters().put('Id', campaign.Id);
        return ref;
    }    
       
    private void reload(){
        quantitiesToOrder = null;
        campaignItemsWithoutCreativityResults = null;   
        campaignItemsWithCreativityResults = null;
    }
        
    public PageReference saveNewCampaignItemWithoutCreativity(){
        Decimal creativity = maxCreativity(false);
        newcampaignItemWithoutCreativity.Campaign__c = campaign.Id;
        Space__c space = [Select id, Decoration_Alias__c, Default_Campaign_Item_Name__c, OP_max_cost_Eur__c, Name FROM Space__c WHERE id =: newCampaignItemWithoutCreativity.Space__c];
        //Start  -- req 13
         showerror = false;
        if(campaign.Scope__c ==ERTAILMasterHelper.SCOPE_INSTOREDISPLAY){
            if(!DisplaySpaceId.contains(space.id)){
                showerror = true;
                return null;
            }
        } 
       /* if(showerror){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You cannot select this Campaign Item as Scope Limitation is "In-Store Display only" '));
                return null;
        } */
        //End  
        if(campaignItemName == null || campaignItemName == ''){
            newcampaignItemWithoutCreativity.Name = space.Default_Campaign_Item_Name__c;
        }
        else{
            newcampaignItemWithoutCreativity.Name = campaignItemName;
        }       
        newcampaignItemWithoutCreativity.Creativity__c = creativity + 1;
        newcampaignItemWithoutCreativity.Alias__c = space.Decoration_Alias__c + (creativity + 1);
        newcampaignItemWithoutCreativity.Max_Cost_EUR__c = space.OP_max_cost_Eur__c;
        newcampaignItemWithoutCreativity.Actual_Cost_EUR__c = space.OP_max_cost_Eur__c;
        newcampaignItemWithoutCreativity.Proposed_Cost_EUR__c = space.OP_max_cost_Eur__c;
        insert newCampaignItemWithoutCreativity;
        newCampaignItemWithoutCreativity = new Campaign_Item__c();
        campaignItemName = null;
        
        queryFeeds();
        reload();
        return null;
    }
    
    public PageReference saveNewCampaignItemWithCreativity(){
        List<Campaign_Item__c> newCampaignItemList = new List<Campaign_Item__c>();
        Space__c space = [Select id, Name, Decoration_Alias__c, Default_Campaign_Item_Name__c, Creativity_Color__c, OP_max_cost_Eur__c FROM Space__c WHERE id =: newCampaignItemWithCreativity.Space__c];
        //Start req 13
        showerrorInstore = false;
        if(campaign.Scope__c ==ERTAILMasterHelper.SCOPE_INSTOREDISPLAY){
            if(!DisplaySpaceId.contains(space.id)){
                showerrorInstore = true;
            }
        } 
       /* if(showerror){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You cannot select this Campaign Item as Scope Limitation is "In-Store Display only" '));
                return null;
        }*/
        //End   
        Decimal maxcreativity = maxCreativity(true);
        
        List<String> colorList = space.Creativity_Color__c != null ? space.Creativity_Color__c.split('\r\n') : new List<String>();
        
        List<Integer> spaceColor = null;
        
        for(Integer index = 1; index <= Integer.valueOf(itemsToCreate); index++){
            Integer creativity = Integer.valueOf(maxcreativity) + index;
            String n = space.Name;
            if (space.Default_Campaign_Item_Name__c != null){
                n = space.Default_Campaign_Item_Name__c;
            }
            String alias = n;
            if(space.Decoration_Alias__c != null){
                alias = space.Decoration_Alias__c + (creativity);
            }
            
            Campaign_Item__c campaignItem = new Campaign_Item__c();
            campaignItem.Campaign__c = campaign.Id;
            campaignItem.Space__c = space.id;
            campaignItem.Name = alias;
            campaignItem.Creativity__c = creativity;
            campaignItem.Alias__c = alias;
            campaignItem.Max_Cost_EUR__c = space.OP_max_cost_Eur__c;
            campaignItem.Actual_Cost_EUR__c = space.OP_max_cost_Eur__c;
            campaignItem.Proposed_Cost_EUR__c = space.OP_max_cost_Eur__c;
            campaignItem.Creativity_Color__c = colorList.size() > (creativity -1) ? colorList[creativity - 1] : '#000000';
            newCampaignItemList.add(campaignItem);
        }
        
        insert newCampaignItemList;
        newCampaignItemWithCreativity = new Campaign_Item__c();
        queryFeeds();
        reload();
        return null;
    }

    public void updateInStoreCampaignItemName(){
        Id ciId= (Id) ApexPages.currentPage().getParameters().get('campaignItemId');
        String ciName= ApexPages.currentPage().getParameters().get('campaignItemName');

        if(ciName != null && ciName != ''){     
            Campaign_Item__c campaignItem = new Campaign_Item__c(Id = ciId, Name = ciName);
            
            update campaignItem;
        }
    }
    
    public PageReference DeleteCampaignItem(){
        if (ApexPages.currentPage().getParameters().containsKey('campaignItemId')){
            Id campaignItemToDeleteId = ApexPages.currentPage().getParameters().get('campaignItemId');
            delete new Campaign_Item__c(Id = campaignItemToDeleteId);
            queryFeeds();
            reload();
        }
        
        return null;
    }
    
    private Decimal MaxCreativity(Boolean withCreativity){
        Decimal maxCreativity = 0;
        if (withCreativity){
            for (Campaign_Item__c ci : campaignItemsWithCreativityResults){
                if (ci.Creativity__c > maxCreativity && ci.Space__c == newCampaignItemWithCreativity.Space__c){
                    maxCreativity = ci.Creativity__c;
                }
            }
            return maxCreativity;
        } else {
            for (Campaign_Item__c ci : campaignItemsWithoutCreativityResults){
                if (ci.Creativity__c > maxCreativity && ci.Space__c == newCampaignItemWithoutCreativity.Space__c){
                    maxCreativity = ci.Creativity__c;
                }
            }
            return maxCreativity;
        }
    }
}
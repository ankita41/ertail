/*
*   @Class      :   ERTAILCampaignItemListExtTest.cls
*   @Description:   Test methods for class ERTAILCampaignItemListExt.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILCampaignItemListExtTest {
    
    static testMethod void myUnitTest() {
	    Id cmpId = ERTAILTestHelper.createCampaignMME();

		Campaign__c cmp = [select Id,Scope__c from Campaign__c where Id =:cmpId];
        
		ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);

		ERTAILCampaignItemListExt ctrl = new ERTAILCampaignItemListExt(stdController);

		String s = ctrl.campaignItemName;

		Map<Id,List<String>> excludedMarketsPerCampaignId = ctrl.excludedMarketsPerCampaignId;

		ctrl.refreshLists();

		Map<Id,Integer> quantitiesToOrder = ctrl.quantitiesToOrder;

		List<ERTAILCampaignItemListExt.InStoreWrapper> inStoreWrappers = ctrl.inStoreWrappers;

		Map<Id,String> excludedMarketsPerCampaignItem = ctrl.excludedMarketsPerCampaignItem;

		Space__c space = [select Id from Space__c where name = 'Test Space'][0];
		System.assertEquals(space != null, true);
		ctrl.newCampaignItemWithoutCreativity.Space__c = space.Id;
		ctrl.saveNewCampaignItemWithoutCreativity();
		
		space = [select Id from Space__c where name = 'Test Space with creativity'][0];
		ctrl.newCampaignItemWithCreativity.Space__c = space.Id;
		ctrl.itemsToCreate = '3';
		ctrl.saveNewCampaignItemWithCreativity();


        Boutique_Order_Item__c boi = [select Id, Campaign_item_To_ORder__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and boutique_Space__r.Space__r.has_Creativity__c = true][0];
		
		PageReference pgRef = Page.ERTAILOrdersDetail;
		pgRef.getParameters().put('campaignItemId', boi.Campaign_item_To_ORder__r.Id);
		pgRef.getParameters().put('campaignItemName', 'new name');
		pgRef.getParameters().put('imgParentId', boi.Campaign_item_To_ORder__r.Id);
		Test.setCurrentPage(pgRef);

//		ctrl.delImgCommit();
		
		ctrl.updateInStoreCampaignItemName();
		
		ctrl.deleteCampaignItem();
			
		ctrl.manageOrders();
		
		
		ctrl.saveImages();

	}
}
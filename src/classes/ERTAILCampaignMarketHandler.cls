/*****
*   @Class      :   ERTAILCampaignMarketHandler.cls
*   @Description:   Static methods called by triggers.
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignMarketHandler {
    public class CustomException extends Exception {}
    
    public static Id cmffOrderingRTId = Schema.SObjectType.Campaign_Market__c.getRecordTypeInfosByName().get('Fixture and Furnitures ordering').getRecordTypeId();
    public static Id btqffOrderingRTId = Schema.SObjectType.Campaign_Boutique__c.getRecordTypeInfosByName().get('Fixture and Furnitures ordering').getRecordTypeId();     
    
	public static void CreateCampaignBoutiqueFFRecordsAfterCampaignMarketsFFInsert(Map<Id, Campaign_Market__c> newMap){
		List<Campaign_Market__c> cmsToCreateCbFFFor = new List<Campaign_Market__c>();
		for (Campaign_Market__c cm : newMap.values()){
			if (cm.RecordTypeId == cmffOrderingRTId){
				cmsToCreateCbFFFor.add(cm);
			}
		}
		//throw new CustomException('' + cmsToCreateCbFFFor);
		if (cmsToCreateCbFFFor.size() > 0){
			CreateCampaignBoutiqueFFRecordsAfterCampaignMarketsFFInsert(cmsToCreateCbFFFor);
		}
	} 
	
	public static void CreateCampaignBoutiqueFFRecordsAfterCampaignMarketsFFInsert(List<Campaign_Market__c> cmToAdd){
		// create all the CampaignBoutiques for the newly selected markets
		Map<Id, Id> selectedMarketIdsMap = new Map<Id, Id>();
		for(Campaign_Market__c cm : cmToAdd){
			selectedMarketIdsMap.put(cm.RTCOMMarket__c, cm.Id);
		}
		List<Campaign_Boutique__c> cbToInsert = new List<Campaign_Boutique__c>();
		// request all the boutiques for the selected markets
		for (RTCOMBoutique__c b : [Select Id, Name, RTCOMMarket__c from RTCOMBoutique__c where RTCOMMarket__c in : selectedMarketIdsMap.keySet() and Excluded_for_future_campaigns__c = false]){
			cbToInsert.add(
				new Campaign_Boutique__c(
					Name = b.Name
					, RTCOMBoutique__c = b.Id
					, Campaign_Market__c = selectedMarketIdsMap.get(b.RTCOMMarket__c)
					, RecordTypeId = btqffOrderingRTId
				)
			);
		}
		//throw new CustomException('' + cbToInsert);
		if (cbToInsert.size() > 0)
			insert cbToInsert;
	}

}
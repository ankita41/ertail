/*****
*   @Class      :   ERTAILCampaignMarketListExt.cls
*   @Description:   Extension developed for ERTAILCampaignMarketSelection.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*  Ankita Singhal  2 Apr 2017       Ertail  Enhancements 2017  
   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignMarketListExt {
    public static Id cmCampaignRTId = Schema.SObjectType.Campaign_Market__c.getRecordTypeInfosByName().get('Campaign').getRecordTypeId();
    public static Id cbCampaignRTId = Schema.SObjectType.Campaign_Boutique__c.getRecordTypeInfosByName().get('Campaign').getRecordTypeId();
    public String Scopevalue { get; set; }
    public class CustomException extends Exception {}
    public Boolean checkDisplay;
    public Boolean DisplayError{ get; set; }
    public Boolean MktDel{ get; set; }
    public id campaignMarketToDeleteId ;
    public String errormsg{ get; set; }
    public Campaign__c campaign { get; private set;}
    private Set<Id> SelectedMarketIdsSet;
    
    public class SelectableMarketWrapper{
        public SelectableMarketWrapper(RTCOMMarket__c market){
            this.market = market;
            this.Selected = false;
        }
        public RTCOMMarket__c market { get; set; }
        public Boolean Selected { get; set; }
    }
    
    public List<Campaign_Market__c> selectedMarkets { 
        get{
            if (selectedMarkets == null){
                selectedMarkets = new List<Campaign_Market__c>();
                SelectedMarketIdsSet.clear();
                for(Campaign_Market__c cm : [Select c.Name, c.RTCOMMarket__c, c.Number_of_Selected_Boutiques__c, c.Scope_Limitation__c, c.RTCOMMarket__r.Name, c.Launch_Date__c, c.End_Date__c, c.Campaign__c From Campaign_Market__c c where Campaign__c = :campaign.Id order by RTCOMMarket__r.Name]){
                    selectedMarkets.add(cm);
                    SelectedMarketIdsSet.add(cm.RTCOMMarket__c);
                }
            }
            return selectedMarkets;
        }
        private set;
    }
    
    public PageReference redirectToERTAILCampaignBoutiqueSelection(){
        // If the logged in user has access to only one RTCOM Market then the user is redirected to ERTAILCampaignBoutiqueSelection
        if (selectedMarkets.size() == 1 && AllMarkets.size() ==1){
            PageReference pageRef = Page.ERTAILCampaignBoutiqueSelection;
            pageRef.getParameters().put('Id', selectedMarkets[0].Id);
            return pageRef;
        }
        return null;
    }
    
    private List<RTCOMMarket__c> AllMarkets {
        get{
            if(AllMarkets == null){
                AllMarkets = [Select Id, Name, OwnerId from RTCOMMarket__c WHERE Hide_from_Future_Campaigns__c = FALSE order by Name];
            }
            return AllMarkets;
        }
        set;
    }
    
    public List<SelectableMarketWrapper> availableMarkets {
        get{
            if (availableMarkets == null){
                availableMarkets = new List<SelectableMarketWrapper>();
                for (RTCOMMarket__c m : AllMarkets){
                    if (!SelectedMarketIdsSet.contains(m.Id)){
                        availableMarkets.add(new SelectableMarketWrapper(m));
                    }
                }
            }
            return availableMarkets;
        }
        private set;
    }

    public ERTAILCampaignMarketListExt(ApexPages.StandardController stdController) {
        this.campaign = (Campaign__c)stdController.getRecord();
        SelectedMarketIdsSet = new Set<Id>();
        DisplayError=false;
        MktDel=false;
        errormsg='';
        List<Campaign_Topic__c> TopicsSelected = new List<Campaign_Topic__c>([Select Id , Name , campaign__c,topic__r.Name from campaign_Topic__c where Campaign__c =: campaign.id]);
            
            //Start --- req 13
            checkDisplay=false;
            for(Campaign_Topic__c ct: TopicsSelected ){
                if(ct.topic__r.Name=='Display'){
                  checkDisplay= true;
                  break;
                  }
            }
            //End
            system.debug('@@@TopicSelected'+checkDisplay);
    }

    public PageReference DeleteCampaignMarket(){
            MktDel=false;
           delete new Campaign_Market__c(Id = campaignMarketToDeleteId);
            resetMarketsLists();
       
        return null;
    }
    
    public PageReference addMarkets(){
        AddMarketsToSelection(false);
        return  null;
    }
    
    public PageReference addMarketsWithBoutiques(){
        AddMarketsToSelection(true);
        return  null;
    }
    // Start  -- req 13
     public PageReference CheckMktDelete(){
            MktDel=true;
            String campaignMarket='';
             if (ApexPages.currentPage().getParameters().containsKey('cmName')){
                 campaignMarket = ApexPages.currentPage().getParameters().get('cmName');
              }
               if (ApexPages.currentPage().getParameters().containsKey('cmtd')){
                campaignMarketToDeleteId = ApexPages.currentPage().getParameters().get('cmtd');
            }   
            errormsg ='Are you sure you want to delete market: '+ campaignMarket +'?';
          //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not allowed to select this scope limitation as display topic was not selected for this campaign'));
          return  null;
    }
    //End
    // Start  -- req 64
     public PageReference CheckTopic(){
         displayError=false;
         if(Scopevalue==ERTAILMasterHelper.SCOPE_INSTOREDISPLAY && CheckDisplay==false){
            displayError=true;
            errormsg ='You are not allowed to select this scope limitation as display topic was not selected for this campaign';
          //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not allowed to select this scope limitation as display topic was not selected for this campaign'));
         }
        return  null;
    }
    //End
    private void AddMarketsToSelection(Boolean includeBoutiques){
        List<Campaign_Market__c> cmToAdd = new List<Campaign_Market__c>();
        for(SelectableMarketWrapper marketWrapper : availableMarkets){
            if (marketWrapper.Selected){
                cmToAdd.add(
                    new Campaign_Market__c(
                        Name = campaign.Name + ' [' + marketWrapper.market.Name + ']'
                        , RTCOMMarket__c = marketWrapper.market.Id
                        , MPM_Scope_Limitation_Proposal__c = '--Inherit--'                      
                        , OwnerId = marketWrapper.market.OwnerId
                        , Scope_Limitation__c = campaign.Scope__c
                        , Launch_Date__c = campaign.Launch_Date__c
                        , End_Date__c = campaign.End_Date__c
                        , Campaign__c = campaign.Id
                        , RecordTypeId = cmCampaignRTId
                        , MPM_Proposal_Status__c = 'Proposal'
                    ));
            }
        }
        try{
            if (cmToAdd.size() > 0){
                insert cmToAdd;
         
            if (includeBoutiques){
                // create all the CampaignBoutiques for the newly selected markets
                Map<Id, Id> selectedMarketIdsMap = new Map<Id, Id>();
                for(Campaign_Market__c cm : cmToAdd){
                    selectedMarketIdsMap.put(cm.RTCOMMarket__c, cm.Id);
                }
                List<Campaign_Boutique__c> cbToInsert = new List<Campaign_Boutique__c>();
                
                // Default delivery mode value
                String defaultDeliveryMode = '';
                /** Below code commented by Promila **/
                /*Schema.DescribeFieldResult F = Campaign_Boutique__c.Delivery_Mode__c.getDescribe();
                List <Schema.PicklistEntry> pickVals = F.getPicklistValues();
                for (Schema.PicklistEntry av: pickVals) {
                    if (av.isDefaultValue()) {
                        defaultDeliveryMode = av.getValue();
                        break;
                    }
                }*/
                
                // request all the boutiques for the selected markets
                for (RTCOMBoutique__c b : [Select Id, Name, RTCOMMarket__c,Location__r.Delivery_mode__c,RTCOMMarket__r.Location__r.Delivery_mode__c from RTCOMBoutique__c where RTCOMMarket__c in : selectedMarketIdsMap.keySet() and Excluded_for_future_campaigns__c = false]){
                      /** Below code Added by Promila to set delivery mode from boutique location by default starts here **/
                    defaultDeliveryMode=(b.Location__r.Delivery_mode__c!=null?b.Location__r.Delivery_mode__c:b.RTCOMMarket__r.Location__r.Delivery_mode__c); // Added by Promila to set delivery mode from boutique by default
                    if(defaultDeliveryMode!=null){
                        if(defaultDeliveryMode.containsIgnoreCase('Warehouse')){
                           defaultDeliveryMode='Warehouse Delivery';
                        }
                        else if(defaultDeliveryMode.containsIgnoreCase('Pick')){
                           defaultDeliveryMode='Pick up';
                        }
                        else if(defaultDeliveryMode.containsIgnoreCase('Boutique')){
                           defaultDeliveryMode='Boutique Delivery';
                        }
                    }
                    cbToInsert.add(
                        new Campaign_Boutique__c(
                            Name = b.Name
                            , RTCOMBoutique__c = b.Id
                            , Scope_Limitation__c = campaign.Scope__c
                            , MPM_Scope_Limitation_Proposal__c = '--Inherit--'
                            , Launch_Date__c = campaign.Launch_Date__c
                            , End_Date__c = campaign.End_Date__c
                            , Campaign_Market__c = selectedMarketIdsMap.get(b.RTCOMMarket__c)
                            , RecordTypeId = cbCampaignRTId
                            , Delivery_Mode__c = defaultDeliveryMode
                        )
                    );
                }
                if (cbToInsert.size() > 0)
                    insert cbToInsert;
            }
            resetMarketsLists();        
        }
        }
         catch(exception e){
         //Start -- req 13
             if(e.getmessage().contains('STRING_TOO_LONG, Name'))
                 errormsg ='Your Campaign Title is too long. Max. length of title is 80 characters, please rename title of campaign';
            else
                 errormsg =e.getmessage();    
            displayError=true; 
         
         } 
         //End  
    }
    public void SaveSelection(){
        try{
           //STart --- req 13
            displayError =false;
            if(CheckDisplay==false){
                for(Campaign_Market__c cm:selectedMarkets ){
                 
                    if(cm.Scope_Limitation__c==ERTAILMasterHelper.SCOPE_INSTOREDISPLAY ){
                        displayError = true;
                       break;
                    }
                    
                }
             }   
            if(DisplayError){
                 errormsg ='You are not allowed to select this scope limitation as display topic was not selected for this campaign';
            }
            else{
            //End
                update selectedMarkets;
            }    
            selectedMarkets = null;
        }
        catch(DMLException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
        }
    }
    
    private void resetMarketsLists(){
        availableMarkets = null;
        resetSelectedMarkets();
    }
    
    private void resetSelectedMarkets(){
        selectedMarkets = null;
    }
    public PageReference redirectPopup(){
        displayError = false;
        mktdel=false;
        return null;
    }

}
/*
*   @Class      :   ERTAILCampaignMarketListExtTest.cls
*   @Description:   Test methods for class ERTAILCampaignMarketListExt.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
public class ERTAILCampaignMarketListExtTest {

    static testMethod void myUnitTest() {
    	Test.startTest();
				
		Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
		
		User adminUser = new User(
							LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
		
		System.runAs(adminUser){
			Id testCampaignId = ERTAILTestHelper.createCampaignMME();

			Campaign__c cmp = [select Id, Name, Scope__c, Launch_Date__c, End_Date__c from Campaign__c where Id =: testCampaignId][0];
		
			Campaign_Market__c cmpMkt = [select Id from Campaign_Market__c where Campaign__r.Id =: cmp.Id][0];
	
			PageReference pgRef = Page.ERTAILOrdersDetail;
			Test.setCurrentPage(pgRef);

			ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);
				
			ERTAILCampaignMarketListExt ctrl = new ERTAILCampaignMarketListExt(stdController);
			
			pgRef.getParameters().put('cmtd', cmpMkt.Id);
            ctrl.CheckMktDelete();
			ctrl.DeleteCampaignMarket();

			for(ERTAILCampaignMarketListExt.SelectableMarketWrapper marketWrapper : ctrl.availableMarkets){
				marketWrapper.Selected = true;
			}

			ctrl.addMarketsWithBoutiques();

			ctrl.saveSelection();
			
			List<Campaign_Market__c> selectedMarkets = ctrl.selectedMarkets;


			ctrl.redirectToERTAILCampaignBoutiqueSelection();
			
			ctrl.addMarkets();
		}
			
		Test.stopTest();
			
    }
}
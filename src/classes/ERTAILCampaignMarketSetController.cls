/*****
*   @Class      :   ERTAILCampaignMarketSetController.cls
*   @Description:   Extension developed for ERTAILCampaignExportCostsPartial.page, ERTAILCampaignExportOrdersPartial.page, ERTAILCampaignRemindMPMForFeedbackButton.page, ERTAILCampaignRequestMPMProposalButton.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer           Date            Description
    Ankita Singhal    2 Apr 2017        Ertail Enhancements 2017
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignMarketSetController {
    public class CustomException extends Exception {}
    public Id campaignId{get;set;}
    private List<Campaign_Market__c> campaignMarkets;
    
    private static Set<String> HQPMProposalSteps = new Set<String>{'10', '11'};
    private static Set<Integer> MPMFeedbackSteps = new Set<Integer>{20, 21};
    private static String MPMProposalStep = '20';
    public List<Id> campaignMarketList;
    private Set<Id> CampaignMarketIds {
        get{
            Set<Id> cmIds = new Set<Id>();
            for (Campaign_Market__c cm : campaignMarkets){
                cmIds.add(cm.Id);
            }
            return cmIds;
        }
    }
    
    public ERTAILCampaignMarketSetController(ApexPages.StandardSetController setCon){
        campaignMarkets = (List<Campaign_Market__c>) setCon.getSelected();
        campaignId = ApexPages.CurrentPage().getParameters().get('id');
       //Start-- Added by Ankita Singhal - Req 28
        if(ApexPages.currentPage().getParameters().containsKey('CampaignmktIds')){
                            campaignMarketList = (List<Id>) ApexPages.currentPage().getParameters().get('CampaignmktIds').split(',');
                     }  
                    if( campaignMarketList != null){
                        campaignMarkets = new  List<Campaign_Market__c>([Select id ,Name from Campaign_Market__c where id in :campaignMarketList]);
                     }
        //End - Req 28                
        }           
         //Start-- Added by Ankita Singhal - Req 28       
                Public PageReference LockOrders(){
                      Map<Id, Boutique_Order_Item__c> boiToUpdateMap = new Map<Id, Boutique_Order_Item__c>();
                      for (Boutique_Order_Item__c boi : [Select Id, Creativity_Step_Picklist__c, Quantity_Step_Picklist__c from Boutique_Order_Item__c where Campaign_Boutique__r.Campaign_Market__c in : CampaignMarketIds]){
                         if (boi.Creativity_Step_Picklist__c != null){
                          boi.Creativity_Step_Picklist__c = '70';
                          boiToUpdateMap.put(boi.Id, boi);
                        }
                         if (boi.Quantity_Step_Picklist__c != null){
                          boi.Quantity_Step_Picklist__c = '70';
                          boiToUpdateMap.put(boi.Id, boi);
                        }
                      }
                      
                      if (boiToUpdateMap.size() > 0){
                        update boiToUpdateMap.values();
                      }  
                    PageReference pageRef = new PageReference('/' + campaignId);
                    return pageRef;
                }
          //End      
     public PageReference RedirecttoPage(){
          PageReference pageRef = new PageReference('/apex/Ertail_selectedmarketList?id=' + campaignId);
          return pageRef;
     }
    public PageReference SendHQPMPropositionToMPM(){
        // Request all Boutique Order Items for the Campaign Markets where Creativity_Step__c in (10, 11) or Quantity_Step__c in (10, 11)
        List<FeedItem> fiList = new List<FeedItem>();
        List<Boutique_Order_Item__c> boiList = [Select Id
                                                , Quantity_Step_Picklist__c
                                                , Creativity_Step_Picklist__c 
                                                , Campaign_Boutique__r.Campaign_Market__c
                                                , Campaign_Boutique__r.Campaign_Market__r.Campaign__c
                                            from Boutique_Order_Item__c 
                                            where Campaign_Boutique__r.Campaign_Market__c in :CampaignMarketIds
                                                and (
                                                    Quantity_Step_Picklist__c in :HQPMProposalSteps
                                                    or 
                                                    Creativity_Step_Picklist__c in :HQPMProposalSteps
                                                )
                                            ]; 
        if (boiList.size() !=0){
        
            for(Boutique_Order_Item__c boi : boiList){  
                if (HQPMProposalSteps.contains(boi.Quantity_Step_Picklist__c)){
                    boi.Quantity_Step_Picklist__c = MPMProposalStep;
                }
                
                if (HQPMProposalSteps.contains(boi.Creativity_Step_Picklist__c)){
                    boi.Creativity_Step_Picklist__c = MPMProposalStep;
                }
                
                campaignMarketIds.add(boi.Campaign_Boutique__r.Campaign_Market__c);
            }
            
            // create the Chatter Messages
            for(Id cmId : campaignMarketIds){
                fiList.add(new FeedItem(
                                ParentId = cmId,
                                Body = 'Request for MPM Creativity & Quantity feedback'
                            ));
            }
            
            // set the "Send HQPM recommendation to MPM checkbox"
            List<Campaign_Market__c> cmList = new List<Campaign_Market__c>();
            for (Id cmId : campaignMarketIds)
                cmList.add(new Campaign_Market__c(Id = cmId, Send_HQPM_Recommendation_to_MPM__c = true));
            
            update boiList;
            insert fiList;
            update cmList;
        }
        PageReference pageRef = new PageReference('/' + campaignId);
        return pageRef;
    }
    
    public PageReference SendReminderToMPM(){
        List<Campaign_Market__c> cmList = [Select Id
                                            from Campaign_Market__c 
                                            where Id in :CampaignMarketIds
                                            and (
                                                    Quantity_Step_Value__c in :MPMFeedbackSteps
                                                    or 
                                                    Creativity_Step_Value__c in :MPMFeedbackSteps
                                                )
                                            ]; 
        
        for(Campaign_Market__c cm : cmList){
            cm.Reminder_for_MPM_Feedback__c = true;
        }
        
        if (cmList.size() > 0){
            List<FeedItem> fiList = new List<FeedItem>();
            
            // create the Chatter Messages
            for(Campaign_Market__c cm : cmList){
                fiList.add(new FeedItem(
                                ParentId = cm.Id,
                                Body = 'Reminder for MPM Creativity & Quantity feedback'
                            ));
            }
            update cmList;
            
            insert fiList;
        }
        
        PageReference pageRef = new PageReference('/' + campaignId);
        return pageRef;
    }
    
    
    public String mktIds {
        get{
            String mktIds = '';
            
            for(Id mktId : campaignMarketIds){
                mktIds +=  (mktIds==''?'':',') + mktId;
            }
            return mktIds;  
        }
    }
}
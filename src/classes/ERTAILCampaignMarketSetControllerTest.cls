/*
*   @Class      :   ERTAILCampaignMarketSetControllerTest.cls
*   @Description:   Test methods for class ERTAILCampaignMarketSetController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILCampaignMarketSetControllerTest {
    
    static testMethod void myUnitTest() {
    	Test.startTest();
				
		Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
		
		User adminUser = new User(
							LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
		
		System.runAs(adminUser){
			
			Id testCampaignId = ERTAILTestHelper.createCampaignMME();

			Campaign_Boutique__c cmpBtq = [select Id, Campaign_Market__r.Id from Campaign_Boutique__c where Campaign_Market__r.Campaign__r.Id =: testCampaignId][0];
			List<Campaign_Market__c> cmpMkt = [select Id from Campaign_Market__c where Id =: cmpBtq.Campaign_Market__r.Id];
	        
	        Set<String> HQPMProposalSteps = new Set<String>{'10', '11'};
	        
	        List<Boutique_Order_Item__c> bois = [select Id from Boutique_Order_Item__c where Campaign_Boutique__r.Id =: cmpBtq.Id and (quantity_step_Picklist__c in : HQPMProposalSteps)];
	        
	        Boutique_Order_Item__c boi = [select Id, Campaign_item_To_ORder__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:testCampaignId and boutique_Space__r.Space__r.has_Creativity__c = true][0];
	        Boutique_Order_Item__c boi2 = [select Id, Campaign_item_To_ORder__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:testCampaignId and boutique_Space__r.Space__r.has_Creativity__c = false][0];
	        Boutique_Order_Item__c boi3 = [select Id, Permanent_Fixture_And_Furniture__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:testCampaignId and Campaign_item_To_Order__c = null][0];

			boi.Quantity_Step_Picklist__c = '10';
			boi2.Creativity_Step_Picklist__c = '10';
			boi3.Quantity_Step_Picklist__c = '10';

			update boi;
			update boi2;
			update boi3;

			PageReference pgRef = Page.ERTAILOrdersDetail;
			pgRef.getParameters().put('Id', testCampaignId);
			Test.setCurrentPage(pgRef);

			ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(cmpMkt);
				
			stdController.setSelected(cmpMkt);
				
			ERTAILCampaignMarketSetController ctrl = new ERTAILCampaignMarketSetController(stdController);
			
			ctrl.SendHQPMPropositionToMPM();
			ctrl.SendReminderToMPM();
			
			String s = ctrl.mktIds;
		}
			
		Test.stopTest();
    }
}
/*
*   @Class      :   ERTAILCampaignOrderExportTest.cls
*   @Description:   Test methods for classes ERTAILExportOrdersExt.cls, ERTAILExportCostsExt.cls and ERTAILExportBoutiquesExt.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILCampaignOrderExportTest {
    
    static testMethod void myUnitTest() {
		
        RTCOMMarket__c testMarket = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket;
        
        RTCOMMarket__c testMarket2 = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket2;
        
        RTCOMBoutique__c testBoutique = new RTCOMBoutique__c(
        	Name = 'Test Boutique',
        	RTCOMMarket__c = testMarket.Id
        );
        insert testBoutique;
        
        RTCOMBoutique__c testBoutique2 = new RTCOMBoutique__c(
        	Name = 'Test Boutique 2',
        	RTCOMMarket__c = testMarket.Id
        );
        insert testBoutique2;
        
        Space__c testSpace = new Space__c(Name = 'Test Space', Category__c = 'In Store');
        insert testSpace;
        
        Boutique_Space__c  btqSpace = new Boutique_Space__c(
        	RTCOMBoutique__c = testBoutique.Id,
        	Space__c = testSpace.Id,
        	Decoration_Allowed__c = 'Yes'
        );
        
        insert btqSpace;
        
        Boutique_Space__c  btqSpace2 = new Boutique_Space__c(
        	RTCOMBoutique__c = testBoutique.Id,
        	Space__c = testSpace.Id,
        	Decoration_Allowed__c = 'Yes'
        );
        
        insert btqSpace2;
        
        Space__c testSpace2 = new Space__c(Name = 'Test Space', Category__c = 'Show Window');
        insert testSpace2;
        
        Boutique_Space__c  btqSpace3 = new Boutique_Space__c(
        	RTCOMBoutique__c = testBoutique.Id,
        	Space__c = testSpace2.Id,
        	Decoration_Allowed__c = 'Yes'
        );
        
        insert btqSpace3;
        
        Boutique_Space__c  btqSpace4 = new Boutique_Space__c(
        	RTCOMBoutique__c = testBoutique.Id,
        	Space__c = testSpace2.Id,
        	Decoration_Allowed__c = 'Yes'
        );
        
        insert btqSpace4;
        
    	Topic__c testTopic = new Topic__c(Name = 'Test Topic');
        insert testTopic;
        
        insert new Space_Topic__c(
        	Space__c = testSpace.Id,
        	Topic__c = testTopic.Id
        );
        
        Campaign__c testCampaign = new Campaign__c(
        	Name = 'Test Campaign',
        	Launch_Date__c = System.Today().addMonths(1),
        	End_Date__c = System.Today().addMonths(2),
        	Description__c = 'This is a Test'
        );
        insert testCampaign;
        
        insert new Campaign_Topic__c(
        	Campaign__c = testCampaign.Id,
        	Topic__c = testTopic.Id
        );
        
        Campaign_Market__c testCampMkt = new Campaign_Market__c(
        	Campaign__c = testCampaign.Id,
        	RTCOMMarket__c = testMarket.Id
        );
        insert testCampMkt;
        
        Campaign_Market__c testEmptyCampMkt = new Campaign_Market__c(
        	Campaign__c = testCampaign.Id,
        	RTCOMMarket__c = testMarket2.Id
        );
        insert testEmptyCampMkt;
        
        Campaign_Boutique__c testCmpBtq = new Campaign_Boutique__c(
        	Campaign_Market__c = testCampMkt.Id,
        	RTCOMBoutique__c = testBoutique.Id
        );
        
        insert testCmpBtq;

		Campaign_Item__c testCmpItem = new Campaign_Item__c(Campaign__c = testCampaign.Id, Space__c = testSpace.Id, Actual_Cost_Eur__c = 5);

		insert testCmpItem;

		Boutique_Order_Item__c testBoi = new Boutique_Order_Item__c(Campaign__c = testCampaign.Id, Campaign_Boutique__c = testCmpBtq.Id, Boutique_Space__c = btqSpace.Id, Campaign_Item_to_order__c = testCmpItem.Id, Quantity_to_order__c = 2, Unit_Cost__c = 10);

		insert testBoi;
        
        Permanent_Fixture_And_Furniture__c testFF = new Permanent_Fixture_And_Furniture__c(Name = 'test ff');
        
 		insert testFF;
 
		Boutique_Order_Item__c testBoiFF = new Boutique_Order_Item__c(Campaign__c = testCampaign.Id, Campaign_Boutique__c = testCmpBtq.Id, Permanent_Fixture_And_Furniture__c = testFF.Id,  Quantity_to_order__c = 2, Order_comment__c = 'comment');

		insert testBoiFF;
 
		ApexPages.StandardController stdController = new ApexPages.StandardController(testCampaign);
		
		ApexPages.StandardController stdController2 = new ApexPages.StandardController(testCampMkt);
		
		
		Test.startTest();
    	
    	ERTAILExportOrdersExt exp = new ERTAILExportOrdersExt(stdController);

		String s = exp.xlsHeader;
    	s = exp.spaceWorksheetAutofilter;
    	s = exp.ffWorksheetAutofilter;
	    s = exp.spaceWorksheetHeader;
    	s = exp.ffWorksheetHeader;
    	s = exp.workbookEnd;

    	ERTAILExportCostsExt exp2 = new ERTAILExportCostsExt(stdController);

		s = exp2.xlsHeader;
		s = exp2.summaryAutofilter;
		s = exp2.summaryWorksheetHeader;
		s = exp2.workbookEnd;
		s = exp2.spaceSummaryColumns;
		s = exp2.ffSummaryColumns;
		s = exp2.campaignLaunchDate;
		s = exp2.campaignEndDate;
		s = exp2.campaignMarketLaunchDate;
		s = exp2.campaignMarketEndDate;
		s = exp2.campaignMarketScopeLimitation;
		    	
		    	
    	ERTAILExportOrdersExt exp3 = new ERTAILExportOrdersExt(stdController2);

		s = exp3.xlsHeader;
    	s = exp3.spaceWorksheetAutofilter;
    	s = exp3.ffWorksheetAutofilter;
	    s = exp3.spaceWorksheetHeader;
    	s = exp3.ffWorksheetHeader;
    	s = exp3.workbookEnd;

    	ERTAILExportCostsExt exp4 = new ERTAILExportCostsExt(stdController2);

		s = exp4.xlsHeader;
		s = exp4.summaryAutofilter;
		s = exp4.summaryWorksheetHeader;
		s = exp4.workbookEnd;
		s = exp4.spaceSummaryColumns;
		s = exp4.ffSummaryColumns;
		s = exp4.campaignLaunchDate;
		s = exp4.campaignEndDate;
		s = exp4.campaignMarketLaunchDate;
		s = exp4.campaignMarketEndDate;
		s = exp4.campaignMarketScopeLimitation;
		
		Boolean b = exp4.hasCampaignItems;
		b = exp4.hasFF;

		Account account = new Account(Name = 'account name', ShippingCity = 'city', BillingCity = 'city', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERTAIL Location').getRecordTypeId());
		insert account;
		
		Contact contact = new Contact(FirstName = 'tutu', LastName = 'toto', AccountId = account.Id);
		insert contact;
		
		account.Main_Contact__c = contact.Id;
		account.Delivery_Contact__c = contact.Id;
		account.Invoice_Contact__c = contact.Id;
		
		update account;
		
		testBoutique.Location__c = account.Id;
		
		update testBoutique;		
		
		testMarket.Location__c = account.Id;
		update testMarket;
		
		ERTAILExportBoutiquesExt exp5 = new ERTAILExportBoutiquesExt();
		
		String st = exp5.xlsHeader;
    	st = exp5.worksheetAutofilter;
	    st = exp5.worksheetHeader;
    	st = exp5.workbookEnd;
		//Mansimar : added to cover ERTAILExportBoutiquesExt parametrised constructor 
		ERTAILExportBoutiquesExt expParam = new ERTAILExportBoutiquesExt(stdController);
        //Mansimar : added to cover ERTAILExportBoutiquesExt parametrised constructor 
		ERTAILExportBoutiquesExt expCampaignMarket = new ERTAILExportBoutiquesExt(stdController2);
		
		Test.stopTest();
        
    }
    
}
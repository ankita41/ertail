/*****
*   @Class      :   ERTAILCampaignRollupfieldscalulation.cls
*   @Description:   Static methods called by triggers and other controllers.
*   @Author     :   Ankita Singhal
*   @Created    :   1 May 2017
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   *****/
public without sharing class ERTAILCampaignRollupfieldscalulation {
    public static void updateRollUpsInCampaigns(Set<Id> campaignIds){
        List<Campaign__c> campaignList = new List<Campaign__c>();
        //throw new CustomException('' + campaignIds);
        for(AggregateResult q : [select Campaign__c
                                , sum(OP_Max_Agency_Fee__c) OP_Max_Agency_Fees
                                , sum(Agency_Actual_Fees__c) Agency_Actual_Fees
                                , sum(Installation_Fee_Eur__c) Installation_Fee_Eur
                                , sum(Delivery_Fee_Eur__c) Delivery_Fee_Eur
                                , sum(OP_actual_cost_Eur__c) OP_actual_cost_Eur
                                , sum(OP_max_cost_Eur__c) OP_max_cost_Eur
                                , min(Creativity_Step_Value__c) Creativity_Step_Value
                                , min(Quantity_Step_Value__c) Quantity_Step_Value
                                , min(Delivery_Fee_Step_Value__c) Delivery_Step_Value
                                , min(Installation_Fee_Step_Value__c) Installation_Step_Value
                                , sum(Fixture_Furnitures_Costs_Eur__c) FF_Costs_Eur
                                from Campaign_Market__c 
                                where Campaign__c IN :campaignIds 
                                group by Campaign__c]){
                Campaign__c campaign = new Campaign__c();
                campaign.Id = (Id)q.get('Campaign__c');
                campaign.Agency_Actual_Fees__c = (Decimal) q.get('Agency_Actual_Fees');
                campaign.OP_Max_Agency_Fees__c = (Decimal) q.get('OP_Max_Agency_Fees');
                campaign.Creativity_Step_Value__c = (Decimal) q.get('Creativity_Step_Value');
                campaign.Quantity_Step_Value__c = (Decimal) q.get('Quantity_Step_Value');
                campaign.Delivery_Cost_Eur__c = (Decimal) q.get('Delivery_Fee_Eur');
                campaign.Installation_Cost_Eur__c = (Decimal) q.get('Installation_Fee_Eur');
                campaign.Actual_Cost_Eur__c = (Decimal) q.get('OP_actual_cost_Eur');
                campaign.Max_Cost_Eur__c = (Decimal) q.get('OP_max_cost_Eur');
                campaign.Delivery_Cost_Step_Value__c = (Decimal) q.get('Delivery_Step_Value');
                campaign.Installation_Cost_Step_Value__c = (Decimal) q.get('Installation_Step_Value');
                campaign.Fixture_Furnitures_Costs_Eur__c = (Decimal) q.get('FF_Costs_Eur');
                campaignList.add(campaign);
        }
        if (campaignList.size() > 0)
            update campaignList;
    }
}
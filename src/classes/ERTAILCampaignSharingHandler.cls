/*****
*   @Class      :   ERTAILCampaignSharingHandler.cls
*   @Description:   Handles all ERTAIL Campaign sharing actions/events.
*   @Author     :   Thibauld
*   @Created    :   08 FEB 2015
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------  
*  
**/
public without sharing class ERTAILCampaignSharingHandler {
	public static void ShareCampaignWithParticipatingMarketsRecords(Map<Id, Campaign_Market__c> marketMap){
    	List<Campaign__Share> cSharesToCreate = CreateSharingRecords(marketMap);		
    	System.debug('insert cSharesToCreate:' + cSharesToCreate);			    										
		if (cSharesToCreate.size() > 0)
			insert cSharesToCreate;
    }
    
    public static Map<Id, Set<Id>> marketOwners(Map<Id, Campaign_Market__c> marketMap){
    	Map<Id, Set<Id>> usersOwnersOfTheMarket = new Map<Id, Set<Id>>();
    	
    	// if the market owner is a queue, request the members of the queue
    	Set<Id> queuesToRequest = new Set<Id>();
    	for (Campaign_Market__c cm : marketMap.values()){
    		String ownerId = cm.OwnerId;
			usersOwnersOfTheMarket.put(ownerId, new Set<Id>{});
    		if (ownerId.substring(0, 3) == '00G'){
    			queuesToRequest.add(ownerId);
    		}
    		else{
    			usersOwnersOfTheMarket.get(ownerId).add(ownerId);
    		}
    	}

		for(GroupMember gm : [Select UserOrGroupId, GroupId From GroupMember where GroupId =:queuesToRequest]){
			usersOwnersOfTheMarket.get(gm.GroupId).add(gm.UserOrGroupId);
		}
    	return usersOwnersOfTheMarket;
    }
    
    public static void RecalculateSharingforCampaignsAfterCampaignMarketDeletion(Map<Id, Campaign_Market__c> marketMap){
		Set<Id> campaignIds = new Set<Id>();
		for (Campaign_Market__c cm : marketMap.values()){
			if (cm.Campaign__c != null)
				campaignIds.add(cm.Campaign__c);
		}
		RecalculateSharingforCampaigns(campaignIds);
    }
    
    public static void RecalculateSharingforCampaigns(Set<Id> campaignIds){
		// request all the Campaign Markets linked to the campaign
		Map<Id, Campaign_Market__c> cmMap = new Map<Id, Campaign_Market__c>([Select Id, ownerId, Campaign__c from Campaign_Market__c where Campaign__c in :campaignIds]);
		
		// delete existing sharing for the campaigns
		delete [SELECT Id FROM Campaign__Share WHERE ParentId IN :campaignIds AND 
             RowCause in (:Schema.Campaign__Share.RowCause.ParticipatingMarket__c)];
		
		// recreate all the sharing records
		ShareCampaignWithParticipatingMarketsRecords(cmMap);
	}
    
    public static List<Campaign__Share> CreateSharingRecords(Map<Id, Campaign_Market__c> marketMap){
    	Map<Id, Set<Id>> usersToShareWith = marketOwners(marketMap);
    	
		// share with the users & roles
		List<Campaign__Share> newcampaignShrs = new List<Campaign__Share>();
		// Construct new sharing records  
        for(Campaign_Market__c market : marketMap.values()){
        	for (Id userOrGroupId : usersToShareWith.get(market.ownerId))
        		if (market.Campaign__c != null)
        			newcampaignShrs.add(CreateMarketSharing(userOrGroupId, market.Campaign__c));
        }
        return newcampaignShrs;
	}
	
	private static Campaign__Share CreateMarketSharing(Id userOrGroupId, Id campaignId){
		return new Campaign__Share(UserOrGroupId = userOrGroupId
										, AccessLevel = 'Edit'
										, ParentId = campaignId
										, RowCause = Schema.Campaign__Share.RowCause.ParticipatingMarket__c);
	}
}
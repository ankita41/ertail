/*****
*   @Class      :   ERTAILCampaignTopicListExt.cls
*   @Description:   Extension developed for ERTAILCampaignTopicSelection.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*  Ankita Singhal  2 Apr 2017       Ertail  Enhancements 2017     
----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignTopicListExt {

    public class CustomException extends Exception {}

    public Campaign__c campaign { get; private set;}
    public Campaign_Topic__c campaignTopic { get; private set;}
    private Set<Id> SelectedTopicIdsSet;
    
    public class SelectableTopicWrapper{
        public SelectableTopicWrapper(Topic__c topic){
            this.topic = topic;
            this.Selected = false;
        }
        public Topic__c topic { get; set; }
        public Boolean Selected { get; set; }
    }
    
    public List<Campaign_Topic__c> selectedTopics { 
        get{
            selectedTopics = new List<Campaign_Topic__c>();
            SelectedTopicIdsSet.clear();
            for(Campaign_Topic__c ct : [Select ct.Id, ct.Topic__c, ct.Topic__r.Name,ct.Campaign__r.Scope__c From Campaign_Topic__c ct where ct.Campaign__c = :campaign.Id order by ct.Topic__r.Name]){
                selectedTopics.add(ct);
                SelectedTopicIdsSet.add(ct.Topic__c);
            }
            return selectedTopics;
        }
        private set;
    }
    
    public  List<Topic__c> AllTopics {
        get{
            if (AllTopics == null){
                //Start--- Req 13
                if(Campaign.Scope__c==ERTAILMasterHelper.SCOPE_INSTOREDISPLAY){
                    AllTopics = [Select Id, Name from Topic__c where Name='Display'];
                }    
                else{ 
                //End  
                    AllTopics = [Select Id, Name from Topic__c order by Name];
                }    
            }
            return AllTopics;
        }
        set;
    }
    
    public List<SelectableTopicWrapper> availableTopics {
        get{
            if (availableTopics == null){
                availableTopics = new List<SelectableTopicWrapper>();
                for (Topic__c m : AllTopics){
                    if (!SelectedTopicIdsSet.contains(m.Id)){
                        availableTopics.add(new SelectableTopicWrapper(m));
                    }
                }
            }
            return availableTopics;
        }
        private set;
    }

    public ERTAILCampaignTopicListExt(ApexPages.StandardController stdController) {
        this.campaign = (Campaign__c)stdController.getRecord();
        SelectedTopicIdsSet = new Set<Id>();
    }

    public PageReference DeleteCampaignTopic(){
        if (ApexPages.currentPage().getParameters().containsKey('cttd')){
            Id campaignTopicToDeleteId = ApexPages.currentPage().getParameters().get('cttd');
            delete new Campaign_Topic__c(Id = campaignTopicToDeleteId);
            availableTopics = null;
        }
        return null;
    }

    public void AddTopics(){
        List<Campaign_Topic__c> ctToAdd = new List<Campaign_Topic__c>();
        for(SelectableTopicWrapper TopicWrapper : availableTopics){
            if (TopicWrapper.Selected){
                ctToAdd.add(
                    new Campaign_Topic__c(
                        Campaign__c = campaign.Id
                        , Topic__c = TopicWrapper.Topic.Id));
            }
        }
        if (ctToAdd.size() > 0){
            insert ctToAdd;
            availableTopics = null; 
        }
    }

}
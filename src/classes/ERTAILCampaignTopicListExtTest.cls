/*
*   @Class      :   ERTAILCampaignTopicListExtTest.cls
*   @Description:   Test methods for class ERTAILCampaignTopicListExt.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
public class ERTAILCampaignTopicListExtTest {

    static testMethod void myUnitTest() {
        Test.startTest();
                
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
        
        User adminUser = new User(
                            LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
        
        System.runAs(adminUser){
            Id testCampaignId = ERTAILTestHelper.createCampaignMME();

            Campaign__c cmp = [select Id,Scope__c from Campaign__c where Id =: testCampaignId][0];
    
            PageReference pgRef = Page.ERTAILOrdersDetail;
            Test.setCurrentPage(pgRef);

            ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);
                
            ERTAILCampaignTopicListExt ctrl = new ERTAILCampaignTopicListExt(stdController);
            
            List<Campaign_Topic__c> cmpTopics = ctrl.selectedTopics;
            
            System.assertEquals(cmpTopics.size() > 0, true);
    
            for(Campaign_Topic__c topic : cmpTopics){
                pgRef.getParameters().put('cttd', topic.Id);
                ctrl.DeleteCampaignTopic();
            }
    
            List<Campaign_Topic__c> selectedTopics = ctrl.selectedTopics;

            for(ERTAILCampaignTopicListExt.SelectableTopicWrapper selectableTopicWrapper : ctrl.availableTopics){
                selectableTopicWrapper.Selected = true;
            }
            
            ctrl.AddTopics();
            
            Campaign_Topic__c cmpTop = ctrl.campaignTopic;
        }
            
        Test.stopTest();
            
    }
}
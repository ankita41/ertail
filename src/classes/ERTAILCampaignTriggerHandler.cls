/*
*   @Class      :   ERTAILCampaignTriggerHandler.cls
*   @Description:   Handler of the ERTAILCampaignTrigger.trigger
*   @Author     :   Jacky Uy
*   @Created    :   15 DEC 2014
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*                         
*/

public with sharing class ERTAILCampaignTriggerHandler {
	
	public static void CreateCampaignAgencies(List<Campaign__c> newList){
		List<Campaign_Agency__c> newAgencies = new List<Campaign_Agency__c>();
		for(Campaign__c c : newList){
			newAgencies.add(new Campaign_Agency__c(
				Campaign__c = c.Id,
				RecordTypeId = Schema.sObjectType.Campaign_Agency__c.getRecordTypeInfosByName().get('Creative Agency').getRecordTypeId()
			));
		
			newAgencies.add(new Campaign_Agency__c(
				Campaign__c = c.Id,
				RecordTypeId = Schema.sObjectType.Campaign_Agency__c.getRecordTypeInfosByName().get('Production Agency').getRecordTypeId()
			));
		}
		insert newAgencies;
	}
	
	
	// Copy Agency fees in Campaign Agency fees for all new campaigns
	public static void CreateCampaignAgencyFees(List<Campaign__c> newList){
		List<ERTAILAgency_Fee__c> agencyFeeList = [Select Id, Name, OP_max_fee__c, Apply_to_Cluster__c from ERTAILAgency_Fee__c];
		List<ERTAILCampaign_Agency_Fee__c> newCampaignAgencyFees = new List<ERTAILCampaign_Agency_Fee__c>();
		for(Campaign__c c : newList){
			for(ERTAILAgency_Fee__c af : agencyFeeList){
				newCampaignAgencyFees.add(new ERTAILCampaign_Agency_Fee__c(
					Name = af.Name,
					Agency_Fee__c = af.Id,
					Campaign__c = c.Id,
					OP_max_fee__c = af.OP_max_fee__c,
					Actual_fee_Eur__c = af.OP_max_fee__c,
					Fee_Status_picklist__c = (af.Apply_to_Cluster__c == c.Is_Cluster__c? '10': null)
				));
			}
		}		
		if (newCampaignAgencyFees.size() > 0)
			insert newCampaignAgencyFees;
	}
}
/*****
*   @Class      :   ERTAILCampaignsDashboardController.cls
*   @Description:   Extension developed for ERTAILCampaignsDashboard.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILCampaignsDashboardController {
    public Boolean isHQPMorAdmin { get { return ERTailMasterHelper.isHQPMorAdmin;} }
    public Boolean isMPM { get { return ERTailMasterHelper.isMPM;} }
    public Boolean isAgent { get { return ERTailMasterHelper.isAgent;} }
    
    public class CustomException extends Exception {}
    public String selectedMarket {get;set;}
    public String selectedMarketName {get;set;}

    public Integer campStatus {get;set;}
    public Date asOfDate {get;set;}
    
    public static Id cmffOrderingRTId = Schema.SObjectType.Campaign_Market__c.getRecordTypeInfosByName().get('Fixture and Furnitures ordering').getRecordTypeId();
    
    public ERTAILCampaignsDashboardController(){
        if(ApexPages.CurrentPage().getParameters().containsKey('showHeader')){
            showHeader = Boolean.valueOf(ApexPages.CurrentPage().getParameters().get('showHeader'));
        }
        
        campStatus = 1;
        
        String temp = ApexPages.currentPage().getParameters().get('asofdate');
        if(temp!=null)
            asOfDate = Date.valueOf(temp);
        else
            asOfDate = SYSTEM.Today();
        /*
        if(isMPM && UserInfo.getUserRoleId()!=null){
            Group g = [SELECT DeveloperName FROM Group WHERE RelatedId = :UserInfo.getUserRoleId() AND Type = 'Role'];
            GroupMember gm = [SELECT GroupId FROM GroupMember WHERE UserOrGroupId = :g.Id AND Group.DeveloperName = :g.DeveloperName];
		*/
            List<RTCOMMarket__c> cmpMkt = [SELECT Name FROM RTCOMMarket__c /*WHERE OwnerId = :gm.GroupId*/];
        //    if(!cmpMkt.isEmpty()){
        	if(cmpMkt.size() == 1){
                selectedMarket = cmpMkt[0].Id;
                selectedMarketName = cmpMkt[0].Name;
            }
            /*
            else{
                throw new CustomException('Cannot find the Market that is assigned to you. Please contact your administrator for assistance.');
            }
        }
        */
        initCampaignList();
        initCampaignBoutiques();
    }
    
    public List<SelectOption> marketOpts {
        get{
            if(marketOpts == null){
                Set<String> mktNames = new Set<String>();
                marketOpts = new List<SelectOption>();
                for(RTCOMMarket__c m : [SELECT Id, Name FROM RTCOMMarket__c order by Name]){
                    marketOpts.add(new SelectOption(m.Id, m.Name));
                }
            }
            return marketOpts;
        }
        set;
    }
    
    public void selectMarket(){
        marketList = null;
        campaignList = null;
        initCampaignList();
        mapCampBtqOrderItemCount = null;
        campaignBtqList = null;
        initCampaignBoutiques();
        TimeLineList = null;
        MonthsList = null;
        NBCampaignPerScopeDonutData = null;
        NBMarketPerScopeDonutData = null;
        NBBoutiquePerScopeDonutData = null;
    }
    
    private static Map<String, String> chartColors = new Map<String, String>{
        ERTAILMasterHelper.SCOPE_COMPLETE => '#613c07', 
        ERTAILMasterHelper.SCOPE_SHOWWINDOW => '#d56b15', 
        ERTAILMasterHelper.SCOPE_INSTORE => '#f89d54'
    };
    
    private static Map<String, String> chartHighlights = new Map<String, String>{
        ERTAILMasterHelper.SCOPE_COMPLETE => '#794d0e', 
        ERTAILMasterHelper.SCOPE_SHOWWINDOW => '#eb7e25', 
        ERTAILMasterHelper.SCOPE_INSTORE=> '#ffaf6f'
    };
    
    //TIMELINE WRAPPER
    public class TimeLineWrapper{
        public Decimal Width {get; private set;}
        public Decimal Left {get; private set;}
        public String Name {get; private set;}
        public Id ObjId {get; private set;}
        public String Color {get; private set;}
        public String Highlight {get; private set;}
        
        public TimeLineWrapper(Campaign__c c, Date startDate){
            Name = c.Name;
            ObjId = c.Id;
            width = ((c.Launch_Date__c.daysBetween(c.End_Date__c) / 365.242) * 100).setScale(2); // duration in days represented as the percentage of a year rounded to two decimals
            left = ((startDate.daysBetween(c.Launch_Date__c) / 365.242) * 100).setScale(2); // offset is calculated from 1st of Jan of the current year rounded to two decimals
            String scope = c.Scope__c != null ? c.Scope__c : ERTAILMasterHelper.SCOPE_COMPLETE;
            Color = chartColors.containsKey(scope) ? chartColors.get(scope) : 'Black';
            Highlight = chartHighlights.containsKey(scope) ? chartHighlights.get(scope) : 'Black';
        }
    }
    
    public List<TimeLineWrapper> TimeLineList{
        get{
            if(TimeLineList == null){
                TimeLineList = new List<TimeLineWrapper>();
                todayOffset = ((timelineStartDate.daysBetween(Date.today()) / 365.242) * (1110 - 1110/4)).setScale(2);
                todayOffsetMarker = ((timelineStartDate.daysBetween(Date.today()) / 365.242) * 1110).setScale(2);
                for(Campaign__c c : campaignList){
                    timelineDurationInDays = Math.Max(timelineStartDate.daysBetween(c.End_Date__c), timelineDurationInDays);
                    TimeLineList.add(new TimeLineWrapper(c, timelineStartDate));
                } 
            }
            return TimeLineList;
        }
        private set;
    }
    
    public Decimal todayOffset { get; private set;}
    public Decimal todayOffsetMarker { get; private set;}
    private Integer timelineDurationInDays = 0;
    private Date timelineStartDate{
        get{
            if(campaignList.size() > 0){
                timelineStartDate = campaignList[0].Launch_Date__c;
            } else{
                timelineStartDate = asOfDate;
            }
            return timelineStartDate;
        }
        private set;
    }
    
    public List<String> MonthsList {
        get{
            if(MonthsList == null){
                List<TimeLineWrapper> init = TimeLineList;
                MonthsList = new List<String>();
                Date endDate = timelineStartDate.addDays(timelineDurationInDays);
                // Min displayed is 1 year
                Date oneYear = timelineStartDate.addDays(365);
                endDate = (endDate < oneYear ? oneYear : endDate);
                Integer monthDiff = timelineStartDate.monthsBetween(endDate);
                if(endDate.day() > timelineStartDate.day()) monthDiff++;
                Date cursor = timelineStartDate;
                for(Integer i = 0 ; i < monthDiff ; i++){
                    MonthsList.add(Datetime.newInstance(cursor.Year(), cursor.Month(), 1).format('MMM yyyy'));
                    cursor = cursor.addMonths(1);
                }
            }
            return MonthsList;
        }
        private set;
    }
    
    //DONUT WRAPPER
    public class DonutChartWrapper{
        public String label { public get; private set; }
        public Integer value { public get; private set; }
        public String color { public get; private set; }
        public String highlight { public get; private set; }
        
        public DonutChartWrapper(AggregateResult ar, String defaultName, Integer max){
            value = (Integer) ar.get('quantity');
            label = (String) ar.get('name');
            if(label == null){
                label = defaultName;
            }
            label = label.substring(0, Math.MIN(label.length(), max));
        }
        
        public DonutChartWrapper(String name, Integer val){
            label = name;
            value = val;
            color = chartColors.containsKey(name) ? chartColors.get(name) : 'Black';
            highlight = chartHighlights.containsKey(name) ? chartHighlights.get(name) : 'yellow';
       }
    }
    
    public String NBCampaignPerScopeDonutDataJSON { get{ return JSON.serialize(NBCampaignPerScopeDonutData); } }
    private List<DonutChartWrapper> NBCampaignPerScopeDonutData { 
        get{
            if(NBCampaignPerScopeDonutData == null){
                NBCampaignPerScopeDonutData = new List<DonutChartWrapper>();
                Map<String, Integer> scopeCount = new Map<String, Integer>();
                String name;
                for(Campaign__c c : campaignList){
                    name = (c.Scope__c == null ? ERTAILMasterHelper.SCOPE_COMPLETE : c.Scope__c);
                    if(!scopeCount.containsKey(name))
                        scopeCount.put(name, 1);
                    else
                        scopeCount.put(name, scopeCount.get(name) + 1);
                }
                for(String scope : scopeCount.keySet()){
                    NBCampaignPerScopeDonutData.add(new DonutChartWrapper(scope, scopeCount.get(scope)));
                }
            }
            return NBCampaignPerScopeDonutData;
        } 
        private set;
    }
    
    public String NBMarketPerScopeDonutDataJSON { get{ return JSON.serialize(NBMarketPerScopeDonutData); } }
    private List<DonutChartWrapper> NBMarketPerScopeDonutData { 
        get{
            if(NBMarketPerScopeDonutData == null){
                NBMarketPerScopeDonutData = new List<DonutChartWrapper>();
                Map<String, Integer> scopeCount = new Map<String, Integer>();
                String name;
                for(Campaign_Market__c cm : marketList){
                    name = (cm.Scope_Limitation__c == null ? ERTAILMasterHelper.SCOPE_COMPLETE : cm.Scope_Limitation__c);
                    if(!scopeCount.containsKey(name))
                        scopeCount.put(name, 1);
                    else
                        scopeCount.put(name, scopeCount.get(name) + 1);
                }
                for(String scope : scopeCount.keySet()){
                    NBMarketPerScopeDonutData.add(new DonutChartWrapper(scope, scopeCount.get(scope)));
                }
            }
            return NBMarketPerScopeDonutData;
        } 
        private set;
    }
    
    public String NBBoutiquePerScopeDonutDataJSON { get{ return JSON.serialize(NBBoutiquePerScopeDonutData); } }
    private List<DonutChartWrapper> NBBoutiquePerScopeDonutData { 
        get{
            if(NBBoutiquePerScopeDonutData == null){
                NBBoutiquePerScopeDonutData = new List<DonutChartWrapper>();
                Map<String, Integer> scopeCount = new Map<String, Integer>();
                String name;
                for(Campaign_Boutique__c cb : campaignBtqList){
                    name = (cb.Scope_Limitation__c == null ? ERTAILMasterHelper.SCOPE_COMPLETE : cb.Scope_Limitation__c);
                    if(!scopeCount.containsKey(name))
                        scopeCount.put(name, 1);
                    else
                        scopeCount.put(name, scopeCount.get(name) + 1);
                }
                for(String scope : scopeCount.keySet()){
                    NBBoutiquePerScopeDonutData.add(new DonutChartWrapper(scope, scopeCount.get(scope)));
                }
            }
            return NBBoutiquePerScopeDonutData;
        } 
        private set;
    }
    
    //BAR CHART WRAPPER
    public class BarChartWrapper{
        public List<String> labels { public get; private set; }
        public List<BarChartDataWrapper> datasets { public get; private set; }
        
        public BarChartWrapper(){
            labels = new List<String>(); 
            datasets = new List<BarChartDataWrapper>{new BarChartDataWrapper()};
        }

        public void add(String name, Integer value){
            labels.add((name.length() > 10) ? name.substring(0, 10) + '...' : name);
            datasets[0].add(value);
        }
    }
    
    public class BarChartDataWrapper{
        public String fillColor { public get; private set; }
        public String highlightFill { public get; private set; }
        public List<Integer> data { public get; private set; }
        
        public BarChartDataWrapper(){
            data = new List<Integer>();
            fillColor = 'rgba(213,107,21,0.1)';
            highlightFill = 'rgba(129,129,129,0.1)';
        }
        
        public void add(Integer value){
            data.add(value);
        }
    }
    
    public String NBCampaignItemsPerCampaignBarDataJSON { get{return JSON.serialize(NBCampaignItemsPerCampaignBarData); } }
    private BarChartWrapper NBCampaignItemsPerCampaignBarData { 
        get{
            BarChartWrapper tmpNBCampaignItemsPerCampaignBarData = new BarChartWrapper();
            if(selectedMarket!=null){
                Map<Id, Set<Id>> ciDistintIdsGroupedPerCampaign = new Map<Id, Set<Id>>();
                for(Boutique_Order_Item__c boi : [Select Campaign_Item_to_Order__c, Campaign_Boutique__r.Campaign_Market__r.Campaign__c from Boutique_Order_Item__c where Campaign_Boutique__r.RTCOMBoutique__r.RTCOMMarket__c = :selectedMarket and Campaign_Item_to_Order__c != null]){
                    Id campaignId = boi.Campaign_Boutique__r.Campaign_Market__r.Campaign__c;
                    if (!ciDistintIdsGroupedPerCampaign.containsKey(campaignId))
                        ciDistintIdsGroupedPerCampaign.put(campaignId, new Set<Id>());
                    ciDistintIdsGroupedPerCampaign.get(campaignId).add(boi.Campaign_Item_to_Order__c);
                }
                for(Campaign__c c : campaignList){
                    tmpNBCampaignItemsPerCampaignBarData.add(c.Name, (ciDistintIdsGroupedPerCampaign.containsKey(c.Id) ? ciDistintIdsGroupedPerCampaign.get(c.Id).size() : 0));
                }
            }
            else{
                tmpNBCampaignItemsPerCampaignBarData = new BarChartWrapper();
                for(Campaign__c c : campaignList){
                    tmpNBCampaignItemsPerCampaignBarData.add(c.Name, c.Campaign_Items__r.size());
                }
            }
            return tmpNBCampaignItemsPerCampaignBarData;
        } 
    }
    
    public String NBOrderPerCampaignBarDataJSON { get{return JSON.serialize(NBOrderPerCampaignBarData); } }
    private BarChartWrapper NBOrderPerCampaignBarData { 
        get{
            BarChartWrapper tmpNBOrderPerCampaignBarData = new BarChartWrapper();
            for(Campaign__c c : campaignList){
                tmpNBOrderPerCampaignBarData.add(c.Name, mapCampBtqOrderItemCount.get(c.Id));
            }
            return tmpNBOrderPerCampaignBarData;
        } 
    }
    
    //LINE CHART WRAPPER
    public class LineChartWrapper{
        public List<String> labels { public get; private set; }
        public List<LineChartDataWrapper> datasets { public get; public set; }
        public Double ratio { public get; private set; }
                
        public LineChartWrapper(){
            labels = new List<String>(); 
                
            // LineChartDataWrapper(String label,   String type, String strokeColor,    String pointColor,      String pointStrokeColor, String pointHighlightFill, String pointHighlightStroke, String fillColor){
            datasets = new List<LineChartDataWrapper>{new LineChartDataWrapper('Items Qty',       'bar'),
                                                      new LineChartDataWrapper('Actual (EUR)',      'line',       'rgba(184,  8, 50,1)', 'rgba(184,  8, 50,1)',  '#fff',                  '#fff',                'rgba(184,  8, 50,1)',       'rgba(184,  8, 50,0.1)'),
                                                      new LineChartDataWrapper('Forecasted (EUR)',  'line',       'rgba( 38,148, 86,1)', 'rgba( 38,148, 86,1)',  '#fff',                  '#fff',                'rgba( 38,148, 86,1)',       'rgba( 38,148, 86,0.1)')
                                                    };
        }

        public void add(String name, Integer value1, Integer value2, Integer value3){
            labels.add((name.length() > 10) ? name.substring(0, 10) + '...' : name);
            datasets[0].add(value1, value1);
            datasets[1].add(value2, value2 / ratio);
            datasets[2].add(value3, value3 / ratio);
        }
    }
    
    public class LineChartDataWrapper{
        public String label { public get; private set; }
        public String fillColor { public get; public set; }
        public String highlightFill { public get; public set; }
        public String type { public get; private set; }
        public String strokeColor { public get; private set; }
        public String pointColor { public get; private set; }
        public String pointStrokeColor { public get; private set; }
        public String pointHighlightFill { public get; private set; }
        public String pointHighlightStroke { public get; private set; }
        
        public Boolean scaleShowGridLines { public get; private set; }
        
        public List<double> data { public get; private set; }
        
        public LineChartDataWrapper(String label, String type, String strokeColor, String pointColor, String pointStrokeColor, String pointHighlightFill, String pointHighlightStroke, String fillColor){
            data = new List<double>();
            this.label = label;
            this.type = type;
            this.fillColor = fillColor;
            this.strokeColor = strokeColor;
            this.pointColor = pointColor;
            this.pointStrokeColor = pointStrokeColor;
            this.pointHighlightFill = pointHighlightFill;
            this.pointHighlightStroke = pointHighlightStroke;
        }
        
        public LineChartDataWrapper(String label, String type){
            data = new List<double>();
            this.label = label;
            this.type = type;
            this.fillColor = 'rgba(213,107,21,0.2)';
            this.highlightFill = 'rgba(129,129,129,0.2)';   
            
        }
        
        public void add(Integer org, Double value){
            data.add(value);
        }
    }
    
    public String NBBOIAndCostPerCampaignBarDataJSON { get{return JSON.serialize(NBBOIAndCostPerCampaignBarData); } }
    private LineChartWrapper NBBOIAndCostPerCampaignBarData { 
        get{
            Integer maxActual = 0;
            Integer maxForecast = 0;
            Integer maxOrder = 0;
            LineChartWrapper tmpNBBOIAndCostPerCampaignBarData = new LineChartWrapper();
            if (selectedMarket== null){
                for(Campaign__c c : campaignList){
                    maxActual = Math.MAX(c.Total_Actual_Costs_Eur__c != null ? Integer.valueOf(c.Total_Actual_Costs_Eur__c) : 0, maxActual);
                    maxForecast = Math.MAX(c.Total_Max_Costs_Eur__c != null ? Integer.valueOf(c.Total_Max_Costs_Eur__c) : 0, maxForecast);
                    maxOrder = Math.MAX(mapCampBtqOrderItemCount.get(c.Id) != null ? mapCampBtqOrderItemCount.get(c.Id) : 0, maxOrder);
                }
                
                tmpNBBOIAndCostPerCampaignBarData.ratio = Math.MAX(maxActual, maxForecast) / Math.MAX(maxOrder, 1);
                tmpNBBOIAndCostPerCampaignBarData.ratio = (tmpNBBOIAndCostPerCampaignBarData.ratio!=0) ? tmpNBBOIAndCostPerCampaignBarData.ratio : 1;
                
                for(Campaign__c c : campaignList){
                    tmpNBBOIAndCostPerCampaignBarData.add(
                        c.Name, 
                        mapCampBtqOrderItemCount.get(c.Id) != null ? mapCampBtqOrderItemCount.get(c.Id) : 0, 
                        c.Total_Actual_Costs_Eur__c != null ? Integer.valueOf(c.Total_Actual_Costs_Eur__c) : 0, 
                        c.Total_Max_Costs_Eur__c != null ? Integer.valueOf(c.Total_Max_Costs_Eur__c) : 0
                    );
                }
            }else{
                for(Campaign__c c : campaignList){
                    maxActual = Math.MAX(c.Campaign_Markets__r[0].Total_Actual_Costs_Eur__c != null ? Integer.valueOf(c.Campaign_Markets__r[0].Total_Actual_Costs_Eur__c) : 0, maxActual);
                    maxForecast = Math.MAX(c.Campaign_Markets__r[0].Total_Max_Costs_Eur__c != null ? Integer.valueOf(c.Campaign_Markets__r[0].Total_Max_Costs_Eur__c) : 0, maxForecast);
                    maxOrder = Math.MAX(mapCampBtqOrderItemCount.get(c.Id) != null ? mapCampBtqOrderItemCount.get(c.Id) : 0, maxOrder);
                }
                
                tmpNBBOIAndCostPerCampaignBarData.ratio = Math.MAX(maxActual, maxForecast) / Math.MAX(maxOrder, 1);
                tmpNBBOIAndCostPerCampaignBarData.ratio = (tmpNBBOIAndCostPerCampaignBarData.ratio!=0) ? tmpNBBOIAndCostPerCampaignBarData.ratio : 1;
                
                for(Campaign__c c : campaignList){
                    tmpNBBOIAndCostPerCampaignBarData.add(
                        c.Name, 
                        mapCampBtqOrderItemCount.get(c.Id) != null ? mapCampBtqOrderItemCount.get(c.Id) : 0, 
                        c.Campaign_Markets__r[0].Total_Actual_Costs_Eur__c != null ? Integer.valueOf(c.Campaign_Markets__r[0].Total_Actual_Costs_Eur__c) : 0, 
                        c.Campaign_Markets__r[0].Total_Max_Costs_Eur__c != null ? Integer.valueOf(c.Campaign_Markets__r[0].Total_Max_Costs_Eur__c) : 0
                    );
                }
            }
            
            return tmpNBBOIAndCostPerCampaignBarData;
        } 
    }
    
    public String CampaignColorsJSON { get{ return JSON.serialize(CampaignColors);} } 
    public List<String> CampaignColors { 
        get{
            List<String> tmpCampaignColors = new List<String>();
            String scope;
            for(Campaign__c c : campaignList){
                scope = (c.Scope__c == null ? ERTAILMasterHelper.SCOPE_COMPLETE : c.Scope__c);
                tmpCampaignColors.add(chartColors.containsKey(scope) ? chartColors.get(scope) : 'Black');
            }
            return tmpCampaignColors;
        }
    }

    public String CampaignHighlightsJSON { get{ return JSON.serialize(CampaignHighlights);} } 
    public List<String> CampaignHighlights { 
        get{
            List<String> tmpCampaignHighlights = new List<String>();
            String scope;
            for(Campaign__c c : campaignList){
                scope = (c.Scope__c == null ? ERTAILMasterHelper.SCOPE_COMPLETE : c.Scope__c);
                tmpCampaignHighlights.add(chartHighlights.containsKey(scope) ? chartHighlights.get(scope) : 'Black');
            }
            return tmpCampaignHighlights;
        }
    }
    
    private List<Campaign_Market__c> marketList{
        get{
            if(marketList==null){
                String qryStr = 'SELECT Launch_Date__c, End_Date__c, Name, Scope_Limitation__c FROM Campaign_Market__c WHERE ';
                
                if(selectedMarket!=null)
                    qryStr += 'RTCOMMarket__c = :selectedMarket AND ';
                
                qryStr += 'RecordTypeId != \'' + cmffOrderingRTId + '\' AND ';
                
                if(campStatus==1)
                    qryStr += 'End_Date__c > :asOfDate ';
                else if(campStatus==2)
                    qryStr += 'End_Date__c <= :asOfDate ';
                else if(campStatus==3){
                    Integer tempYear = asOfDate.year();
                    qryStr += '(CALENDAR_YEAR(Launch_Date__c) = :tempYear OR CALENDAR_YEAR(End_Date__c) = :tempYear) ';
                }
                
                qryStr += 'ORDER BY Launch_Date__c, Name ';
                marketList = Database.query(qryStr);
            }
            return marketList;
        }
        set;
    }
    
    transient List<Campaign__c> campaignList;
    private void initCampaignList(){
        Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Campaign__c').getDescribe().fields.getMap();
        String qryStr = 'SELECT (Select Id From Campaign_Items__r), ';
        for(String s : fieldsMap.keySet()){
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        //throw new CustomException('' + selectedMarket);
        if(selectedMarket!=null)
            qryStr += '(SELECT Total_Actual_Costs_Eur__c, Total_Max_Costs_Eur__c FROM Campaign_Markets__r where RTCOMMarket__c = :selectedMarket), ';
        
        qryStr = qryStr.removeEnd(', ') + ' FROM Campaign__c WHERE ';
                
        if(selectedMarket!=null)
            qryStr += 'Id IN (SELECT Campaign__c FROM Campaign_Market__c WHERE RTCOMMarket__c = :selectedMarket) AND ';
        
        if(campStatus==1)
            qryStr += 'End_Date__c > :asOfDate ';
        else if(campStatus==2)
            qryStr += 'End_Date__c <= :asOfDate ';
        else if(campStatus==3){
            Integer tempYear = asOfDate.year();
            qryStr += '(CALENDAR_YEAR(Launch_Date__c) = :tempYear OR CALENDAR_YEAR(End_Date__c) = :tempYear) ';
        }
        
        qryStr += 'ORDER BY Launch_Date__c, Name ';
        campaignList = Database.query(qryStr);
        //throw new CustomException('' + campaignList);
    }
    
    //MAP TO STORE BOUTIQUE ORDER ITEM orders PER CAMPAIGN
    private Map<Id,Integer> mapCampBtqOrderItemCount{
        get{
            if(mapCampBtqOrderItemCount==null){
                mapCampBtqOrderItemCount = new Map<Id,Integer>();
                for(Campaign__c c : campaignList){
                    mapCampBtqOrderItemCount.put(c.Id, 0);
                }
                if (selectedMarket == null){
                    for(Boutique_Order_Item__c boi : [SELECT Campaign__c, Quantity_to_order__c FROM Boutique_Order_Item__c WHERE Campaign__c IN :campaignList AND Permanent_Fixture_And_Furniture__c = NULL]){
                        mapCampBtqOrderItemCount.put(boi.Campaign__c, mapCampBtqOrderItemCount.get(boi.Campaign__c) + (boi.Quantity_to_order__c == null ? 0 : Integer.valueOf(boi.Quantity_to_order__c)));
                    }
                }else{
                    for(Boutique_Order_Item__c boi : [SELECT Campaign__c, Quantity_to_order__c FROM Boutique_Order_Item__c WHERE Campaign_Boutique__r.Campaign_Market__r.RTCOMMarket__c = :selectedMarket AND Campaign__c IN :campaignList AND Permanent_Fixture_And_Furniture__c = NULL]){
                        mapCampBtqOrderItemCount.put(boi.Campaign__c, mapCampBtqOrderItemCount.get(boi.Campaign__c) + (boi.Quantity_to_order__c == null ? 0 : Integer.valueOf(boi.Quantity_to_order__c)));
                    }
                }
            }
            return mapCampBtqOrderItemCount;
        }
        set;
    }
    
    transient List<Campaign_Boutique__c> campaignBtqList;
    private void initCampaignBoutiques(){
        String qryStr = 'SELECT Id, Scope_Limitation__c  FROM Campaign_Boutique__c WHERE Campaign_Market__c IN :marketList ';
        
        if(campStatus==1)
            qryStr = qryStr + 'AND End_Date__c > :asOfDate ';
        else if(campStatus==2)
            qryStr = qryStr + 'AND End_Date__c <= :asOfDate ';
        else if(campStatus==3){
            Integer tempYear = asOfDate.year();
            qryStr = qryStr + 'AND (CALENDAR_YEAR(Launch_Date__c) = :tempYear OR CALENDAR_YEAR(End_Date__c) = :tempYear) ';
        }
        
        qryStr = qryStr + 'ORDER BY Launch_Date__c, Name ';
        campaignBtqList = Database.query(qryStr);
    }
    
    public Boolean showHeader{ 
        get{
            if(showHeader == null)
                showHeader = false;
            return showHeader;
        } 
        set; 
    }
}
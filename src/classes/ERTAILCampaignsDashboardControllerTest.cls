/*
*   @Class      :   ERTAILCampaignsDashboardControllerTest.cls
*   @Description:   Test methods for class ERTAILCampaignsDashboardController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILCampaignsDashboardControllerTest {
    
    static testMethod void myUnitTest() {

		Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
		
		User adminUser = new User(
							LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
		
		System.runAs(adminUser){

			Id cmpId = ERTAILTestHelper.createCampaignMME();
	
		   	Campaign__c cmp = [select Id from Campaign__c where Id =: cmpId];
	
			PageReference pgRef = Page.ERTAILOrdersDetail;
			pgRef.getParameters().put('showHeader', 'true');
	//		pgRef.getParameters().put('selectedView', 'HQPM_Prop');
			Test.setCurrentPage(pgRef);
	
			ERTAILCampaignsDashboardController ctrl = new ERTAILCampaignsDashboardController();
	
			Boolean b = ctrl.isHQPMorAdmin & ctrl.isMPM;
			b = ctrl.showHeader;
	
			List<SelectOption> marketOpts = ctrl.marketOpts;
			ctrl.selectMarket();
			
			List<ERTAILCampaignsDashboardController.TimeLineWrapper> TimeLineList = ctrl.TimeLineList;
	
			List<String> monthsList = ctrl.monthsList;
	
			String s = ctrl.NBCampaignPerScopeDonutDataJSON;
			s = ctrl.NBMarketPerScopeDonutDataJSON;
			s = ctrl.NBBoutiquePerScopeDonutDataJSON;
			s = ctrl.NBCampaignItemsPerCampaignBarDataJSON;
			s = ctrl.NBOrderPerCampaignBarDataJSON;
			s = ctrl.NBBOIAndCostPerCampaignBarDataJSON;
			s = ctrl.CampaignColorsJSON;
			s = ctrl.CampaignHighlightsJSON;
			
			ctrl.campStatus = 2;
			ctrl.selectMarket();
			
			ctrl.campStatus = 3;
			ctrl.selectMarket();
	
			ctrl.selectedMarket = null;
			
			s = ctrl.NBCampaignPerScopeDonutDataJSON;
			s = ctrl.NBMarketPerScopeDonutDataJSON;
			s = ctrl.NBBoutiquePerScopeDonutDataJSON;
			s = ctrl.NBCampaignItemsPerCampaignBarDataJSON;
			s = ctrl.NBOrderPerCampaignBarDataJSON;
			s = ctrl.NBBOIAndCostPerCampaignBarDataJSON;
			s = ctrl.CampaignColorsJSON;
			s = ctrl.CampaignHighlightsJSON;
	
	    }
    }
}
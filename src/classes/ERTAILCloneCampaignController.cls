/*
*   @Class      :   ERTAILCloneCampaignController.cls
*   @Description:   Controller of the ERTAILCloneCampaign
*   @Author     :   Ankita Singhal
*   @Created    :   2 Apr 2017
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*                         
*/
public with sharing class ERTAILCloneCampaignController {
    Campaign__c campaign;
    public ERTAILCloneCampaignController(ApexPages.StandardController controller) {
        this.campaign = (Campaign__c)Controller.getRecord();
    }
     public PageReference CloneCampaign(){
         Campaign__c CloneCampaign;
         Map<String, Schema.SobjectField> fieldsMap = Campaign__c.SobjectType.getDescribe().fields.getMap();
         String qryStr = 'SELECT ';
        for(String s : fieldsMap.keySet()) {
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM Campaign__c where id' +' = \'' + Campaign.Id + '\' ';
        campaign= database.query(qryStr);
        CloneCampaign=campaign.clone(false);
        String Campaignname =campaign.name;
        if(Campaignname.length()>64){
            Campaignname=Campaignname.substring(0,63);
        }
        CloneCampaign.Name= Campaignname +'-Post Production';
        CloneCampaign.recordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Creativity').getRecordTypeId();
        
        CloneCampaign.Quantity_Step_Value__c= 10;
        CloneCampaign.Creativity_Step_Value__c= 10;
        insert CloneCampaign;
        List<Campaign_Item__c> CloneCampaignItem = new List<Campaign_Item__c>();
        List<Campaign_Item__c> CampaignItem = new List<Campaign_Item__c>([Select c.Visual_Step__c, c.Visual_Step_Value__c, c.Visual_Status_picklist__c, c.Visual_Reject_Reason__c, c.Visual_Change_Reason__c, c.SystemModstamp, c.Space__c, c.Proposed_Cost_EUR__c, c.Proposed_Cost_CHF__c, c.Origine__c, c.Name, c.Max_Cost_EUR__c, c.Max_Cost_CHF__c, c.Image_small__c, c.Image_medium__c, c.Image_ID__c, c.Id, c.Creativity__c, c.Creativity_Color__c, c.Cost_Step__c, c.Cost_Step_Value__c, c.Cost_Status_picklist__c, c.Cost_Reject_Reason__c, c.Cost_Change_Reason__c, c.Campaign__r.Id, c.Campaign__c, c.Alias__c, c.Actual_Cost_EUR__c, c.Actual_Cost_CHF__c From Campaign_Item__c c where c.Campaign__r.Id=:campaign.Id]);
        for(Campaign_Item__c ci : CampaignItem ){
            Campaign_Item__c newCI ;
            newCI = ci.clone(false);
            newCI.Campaign__c = CloneCampaign.id;
            CloneCampaignItem.add(newCI );
        }
        if(CloneCampaignItem.size()>0){
            insert CloneCampaignItem;
        }
        List<Campaign_topic__c> CloneTopic = new List<Campaign_topic__c>();
        List<Campaign_topic__c> Topic = new List<Campaign_topic__c>([Select Campaign__c,Id,Name,Topic__c From Campaign_topic__c where Campaign__r.Id=:campaign.Id]);
        for(Campaign_topic__c ci : Topic ){
            Campaign_topic__c newCI ;
            newCI = ci.clone(false);
            newCI.Campaign__c = CloneCampaign.id;
            CloneTopic.add(newCI );
        }
        if(CloneTopic.size()>0){
            insert CloneTopic;
        }
         return new PageReference('/'+CloneCampaign.id);
     }
}
/*
 * @Class  		:   ERTAILCloneCampaignControllerTest.cls
 * @author		: 	Mansimar Singh
 * @Created 	: 	15 march 2017
 * @Description	: 	Test class for ERTAILCloneCampaignController class 
 * 					Original class created by Ankita Singhal for ERTAIL 2017 enhancements
 * */
@isTest
public class ERTAILCloneCampaignControllerTest {
    
    static testMethod void myUnitTest() {
        Id testCampaignId = ERTAILTestHelper.createCampaignWithCampItems();
        Campaign__c cmp = [select Id, Name, Scope__c, Launch_Date__c, End_Date__c from Campaign__c where Id =: testCampaignId][0];
        //for campaign name greater than 64 char 
        cmp.name='CampaignNameTestdummyCampaignNameTestdummyCampaignNameTestdummytest';
        update cmp;
        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);
        ERTAILCloneCampaignController ctrl = new ERTAILCloneCampaignController(stdController);  
        PageReference pref=ctrl.CloneCampaign();
    }
  
}
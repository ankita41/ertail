/*
*   @Class      :   ERTAILCostsController.cls
*   @Description:   Controller of the ERTAILCosts.component
*   @Author     :   Jacky Uy
*   @Created    :   16 DEC 2014
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*                         
*/

public with sharing class ERTAILCostsController extends ERTailMasterHandler {
	public static Boolean isHQPM { get{ return ERTailMasterHelper.isHQPM; } }
	public static Boolean isProductionAgency { get{ return ERTailMasterHelper.isProductionAgency; } }
	
	public Id campaignId {
		get;
		set{
			campaign = [SELECT Id, Name, Scope__c, Launch_Date__c, End_Date__c, Description__c FROM Campaign__c WHERE Id = :value];
		}
	}
	
	public ERTAILCostsController(){}
	
	private void initCampItems(){
		CampaignItems = null;
		CampaignItemsWithCreativity = null;
		CampaignItemsWithOutCreativity = null;
		mapInStorePerCategory = null;
		//mapSelectedItems = null;
	}
	
	public Map<Id,Integer> mapItemIndex {		
		get{
			if(mapItemIndex==null){
				mapItemIndex = new Map<Id,Integer>();
				for(Integer i=0; i<CampaignItemsWithCreativity.size(); i++){
					mapItemIndex.put(CampaignItemsWithCreativity[i].Id, i);
				}
				for(Integer i=0; i<CampaignItemsWithoutCreativity.size(); i++){
					mapItemIndex.put(CampaignItemsWithoutCreativity[i].Id, i);
				}
			}
			return mapItemIndex;
		} set;
	}
	
	/*public Map<Id,Boolean> mapSelectedItems {		
		get{
			if(mapSelectedItems==null){
				mapSelectedItems = new Map<Id,Boolean>();
				for(Campaign_Item__c ci : CampaignItems){
					mapSelectedItems.put(ci.Id, false);
				}
			}
			return mapSelectedItems;
		} set;
	}*/
	
	public void updateCostItem(){
		Integer ciIndex = Integer.valueOf(Apexpages.currentPage().getParameters().get('ciIndex'));
		Boolean withCreativity = Boolean.valueOf(Apexpages.currentPage().getParameters().get('withCreativity'));
		
		if(withCreativity){
			//CampaignItemsWithCreativity[ciIndex].Cost_Status_picklist__c = '10';
			if(CampaignItemsWithCreativity[ciIndex].Cost_Step_Value__c==30)
				CampaignItemsWithCreativity[ciIndex].Actual_Cost_EUR__c = CampaignItemsWithCreativity[ciIndex].Proposed_Cost_EUR__c;
			
			update CampaignItemsWithCreativity[ciIndex];
		}
		else{
			//CampaignItemsWithOutCreativity[ciIndex].Cost_Status_picklist__c = '10';
			if(CampaignItemsWithOutCreativity[ciIndex].Cost_Step_Value__c==30)
				CampaignItemsWithOutCreativity[ciIndex].Actual_Cost_EUR__c = CampaignItemsWithOutCreativity[ciIndex].Proposed_Cost_EUR__c;
				
			update CampaignItemsWithOutCreativity[ciIndex];
		}
		
		initCampItems();
	}

	public void submitForApproval(){
		//List<Campaign_Item__c> updCampItems = new List<Campaign_Item__c>();
		for(Campaign_Item__c ci : CampaignItems){
			if(!ci.Boutique_Order_Items3__r.isEmpty() && (ci.Proposed_Cost_EUR__c==null || ci.Proposed_Cost_EUR__c==0)){
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Costs of Used Campaign Items must be populated before submitting for Confirmation.')); 
				initCampItems();		
			}
			
			//if(mapSelectedItems.get(ci.Id)){
			if(ci.Cost_Status_picklist__c == '10')
				ci.Cost_Status_picklist__c = '20';
				//updCampItems.add(ci);
			//}
		}
		update CampaignItems;
		initCampItems();
		
		//CHATTER POST
		if(!prodAgency.isEmpty()){
			insert new FeedItem(
	        	ParentId = prodAgency[0].Id,
	        	Body = 'Production Costs submitted for Confirmation \nBy: ' + UserInfo.getName() + '\nCampaign: ' + campaign.Name
	        );
		}
	}
	
	public void approveSelected(){
		//List<Campaign_Item__c> updCampItems = new List<Campaign_Item__c>();
		for(Campaign_Item__c ci : CampaignItems){
			//if(mapSelectedItems.get(ci.Id)){
			if(ci.Cost_Status_picklist__c == '20'){
				ci.Actual_Cost_EUR__c = ci.Proposed_Cost_EUR__c;
				ci.Cost_Status_picklist__c = '30';
			}
				//updCampItems.add(ci);
			//}
		}
		update CampaignItems;
		initCampItems();
		
		//CHATTER POST
		if(!prodAgency.isEmpty()){
			insert new FeedItem(
	        	ParentId = prodAgency[0].Id,
	        	Body = 'Production Costs Confirmed \nBy: ' + UserInfo.getName() + '\nCampaign: ' + campaign.Name
	        );
		}
	}
	
	public void rejectSelected(){
		//List<Campaign_Item__c> updCampItems = new List<Campaign_Item__c>();
		for(Campaign_Item__c ci : CampaignItems){
			//if(mapSelectedItems.get(ci.Id)){
				ci.Cost_Status_picklist__c = '10';
				//updCampItems.add(ci);
			//}
		}
		update CampaignItems;
		initCampItems();
	}
	
	public class CustomException extends Exception {/*throw new CustomException();*/}
}
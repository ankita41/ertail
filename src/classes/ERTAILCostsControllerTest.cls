/*
*   @Class      :   ERTAILCostsControllerTest.cls
*   @Description:   Test Coverage for ERTAILCostsController.cls
*   @Author     :   Jacky Uy
*   @Created    :   11 FEB 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        	Description
*   ------------------------------------------------------------------------------------------------------------------------    
*	Jacky Uy		11 FEB 2015		Test Coverage at 
*
*/

@isTest
private class ERTAILCostsControllerTest {
    static testMethod void myUnitTest() {
		Id testCampaignId = ERTAILTestHelper.createCampaignWithCampItems();
        
        PageReference pRef = Page.ERTAILMaster; 
        pRef.getParameters().put('id', testCampaignId);
        Test.setCurrentPage(pRef);
        
        Test.startTest();
        ERTAILCostsController testCon = new ERTAILCostsController();
        testCon.campaignId = testCampaignId;
        SYSTEM.Assert(!testCon.CampaignItems.isEmpty());
        
        pRef.getParameters().put('ciIndex', String.valueOf(testCon.mapItemIndex.get(testCon.CampaignItemsWithCreativity[0].Id)));
        pRef.getParameters().put('withCreativity', 'true');
        Test.setCurrentPage(pRef);
        testCon.updateCostItem();
        
        pRef.getParameters().put('ciIndex', String.valueOf(testCon.mapItemIndex.get(testCon.CampaignItemsWithOutCreativity[0].Id)));
        pRef.getParameters().put('withCreativity', 'false');
        Test.setCurrentPage(pRef);
        testCon.updateCostItem();
        
        Boolean b = ERTAILCostsController.isHQPM & ERTAILCostsController.isProductionAgency;
        
        testCon.submitForApproval();
        testCon.approveSelected();
        testCon.rejectSelected();
        
        Test.stopTest();
    }
}
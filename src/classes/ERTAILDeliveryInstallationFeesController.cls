/*
*   @Class      :   ERTAILDeliveryInstallationFeesController.cls
*   @Description:   Controller of the ERTAILDeliveryInstallationFees.component
*   @Author     :   Jacky Uy
*   @Created    :   08 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*                         
*/

public with sharing class ERTAILDeliveryInstallationFeesController extends ERTailMasterHandler {
    public static Boolean isHQPM { get{ return ERTailMasterHelper.isHQPM; } }
    public static Boolean isProductionAgency { get{ return ERTailMasterHelper.isProductionAgency; } }
    
    public class CustomException extends Exception {/*throw new CustomException();*/}
    
    public String feeType {get;set;}
    public Decimal mktFee {get;set;}
    
    public Id selectedMarketId {
    	get;
    	set{
    		selectedMarketId = value;
    		if(selectedMarket==null || selectedMarket.Id!=selectedMarketId){
    			selectedMarket = [SELECT Id, Name FROM RTCOMMarket__c WHERE Id = :selectedMarketId];
	    		campBtqs = null;
    		}
    	}
    }
    
    public RTCOMMarket__c selectedMarket {get;set;}
    
    public ERTAILDeliveryInstallationFeesController(){}
    
    public Id campaignId {
        get;
        set{
            campaign = [SELECT Id, Name, Scope__c, Launch_Date__c, End_Date__c, Description__c FROM Campaign__c WHERE Id = :value];
        }
    }
    
    public Map<Id,MarketFee> mapMarketFees{
        get{
            if(mapMarketFees==null){
                mapMarketFees = new Map<Id,MarketFee>();
                
                for(Campaign_Market__c cm : [SELECT RTCOMMarket__c, Name, Delivery_Fee_MIN__c, Delivery_Fee_MAX__c, Installation_Fee_MIN__c, Installation_Fee_MAX__c,
                                                Delivery_Fee_Step_Value__c, Installation_Fee_Step_Value__c
                                                FROM Campaign_Market__c WHERE Campaign__c  = :campaign.Id]){
                    mapMarketFees.put(cm.RTCOMMarket__c, new MarketFee());
                    mapMarketFees.get(cm.RTCOMMarket__c).mktName = cm.Name;
                    mapMarketFees.get(cm.RTCOMMarket__c).mktDFStatus = Integer.valueOf(cm.Delivery_Fee_Step_Value__c);
                    mapMarketFees.get(cm.RTCOMMarket__c).mktIFStatus = Integer.valueOf(cm.Installation_Fee_Step_Value__c);
                }
                
            }
            return mapMarketFees;
        }
        set;
    }
    
    public List<Campaign_Boutique__c> campBtqs {
    	get{
    		if(campBtqs==null){
		        campBtqs = [
		            SELECT Name, Delivery_Fee__c, Installation_Fee__c, Delivery_Fee_Step_Value__c, Installation_Fee_Step_Value__c, 
		            	Delivery_Fee_Step__c, Installation_Fee_Step__c, Delivery_Fee_Status_picklist__c, Installation_Fee_Status_picklist__c, Campaign_Market__r.Name 
		            	, Delivery_Date__c, Delivery_Mode__c, Warehouse__c
		            FROM Campaign_Boutique__c 
		            WHERE Campaign_Market__r.Campaign__c = :campaign.Id 
		            AND Campaign_Market__r.RTCOMMarket__c = :selectedMarket.Id
		            ORDER BY Name
		        ];
		        
		        mapItemIndex = null;
    		}
    		return campBtqs;
    	}
    	set;
    }
    
	public Map<Id,Integer> mapItemIndex {		
		get{
			if(mapItemIndex==null){
				mapItemIndex = new Map<Id,Integer>();
				for(Integer i=0; i<campBtqs.size(); i++){
					mapItemIndex.put(campBtqs[i].Id, i);
				}
			}
			return mapItemIndex;
		} set;
	}
    
    public void updateFee(){
    	Integer index = Integer.valueOf(Apexpages.currentPage().getParameters().get('index'));
    	
    	/*if(feeType=='df')
            campBtqs[index].Delivery_Fee_Status_picklist__c = '10';
        else if(feeType=='if')
            campBtqs[index].Installation_Fee_Status_picklist__c = '10';*/
            
        update campBtqs[index];
        campBtqs = null;
    }
    
    public String retMessageJSON{
    	get{
    		return JSON.serialize(retMessage);
    	}
    } 
    
    private ReturnedMessage retMessage { 
    	get{
    		if (retMessage == null)
    			retMessage = new ReturnedMessage(-1);
    		return retMessage;
		} 
    	set;
    }
    public class ReturnedMessage{
    	public Boolean IsSuccess { get; private set; }
    	public String Message { get; private set; }
    	public Integer Index { get; private set; }
    	
    	public ReturnedMessage(Integer inte, Boolean success, String mess){
    		Index = inte;
    		IsSuccess = success;
    		Message = mess;
    	}
    	
    	public ReturnedMessage(Integer inte){
    		Index = inte;
    		IsSuccess = true;
    		Message = '';
    	}
    }
    
    public void updateDeliveryMode(){
    	Integer index = -1;
    	try{
    		index = Integer.valueOf(Apexpages.currentPage().getParameters().get('index'));
        	if (!'Warehouse Delivery'.equals(campBtqs[index].Delivery_Mode__c)){
        		campBtqs[index].Warehouse__c=null;
        	}
        	update campBtqs[index];
        	campBtqs = null;
        	retMessage = new ReturnedMessage(index);
    	}
		catch(DMLException ex){
			retMessage = new ReturnedMessage(index, false, ex.getDmlMessage(0).unescapeHtml4());
		}
    }
    
    public void applyMktFee(){
        if(mktFee!=null){
            for(Campaign_Boutique__c cb : campBtqs){
                if(feeType=='df')
                    cb.Delivery_Fee__c = mktFee;
                else if(feeType=='if')
                    cb.Installation_Fee__c = mktFee;
            }
            
            update campBtqs;
            campBtqs = null;
	        mktFee = null;
        }
    }
    
    public void submitForApproval(){
        String statVal = ApexPages.currentPage().getParameters().get('statVal');
        String chatterMessage;
        
        if(feeType=='df')
            chatterMessage = 'Delivery/Pick-up Fees ';
        else if(feeType=='if')
            chatterMessage = 'Installation Fees ';
        
        if(statVal=='20')
            chatterMessage = chatterMessage + ' submitted for Confirmation \nBy: ' + UserInfo.getName() + '\nCampaign: ' + campaign.Name;
        else if(statVal=='30')
            chatterMessage = chatterMessage + ' Confirmed \nBy: ' + UserInfo.getName() + '\nCampaign: ' + campaign.Name;
        
        if(feeType=='df'){
        	for(Campaign_Boutique__c cb : campBtqs){
            	if(cb.Delivery_Fee__c==null || cb.Delivery_Fee__c==0){
					ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'All Fees must be populated before submitting for Confirmation.')); 
			        campBtqs = null;
			        mapMarketFees = null;		
				}
            	
            	cb.Delivery_Fee_Status_picklist__c = statVal;
        	}
        }
                
        else if(feeType=='if'){
        	for(Campaign_Boutique__c cb : campBtqs){
            	if(cb.Installation_Fee__c==null || cb.Installation_Fee__c==0){
					ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'All Fees must be populated before submitting for Confirmation.')); 
			        campBtqs = null;
			        mapMarketFees = null;		
				}
            	
            	cb.Installation_Fee_Status_picklist__c = statVal;
        	}
        }
        
        update campBtqs;
        campBtqs = null;
        mapMarketFees = null;
        
        //CHATTER POST
        if(!prodAgency.isEmpty() && !isAdmin){
            insert new FeedItem(
                ParentId = prodAgency[0].Id,
                Body = chatterMessage
            );
        }
    }
    
    public class MarketFee{
        public String mktName {get;set;}
        public Decimal mktFee {get;set;}
        public Integer mktDFStatus {get;set;}
        public Integer mktIFStatus {get;set;}
        public MarketFee(){}
    }
}
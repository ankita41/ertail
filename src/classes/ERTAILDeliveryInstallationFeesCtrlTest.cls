/*
*   @Class      :   ERTAILDeliveryInstallationFeesCtrlTest.cls
*   @Description:   Test methods for class ERTAILDeliveryInstallationFeesController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
public class ERTAILDeliveryInstallationFeesCtrlTest {

    static testMethod void myUnitTest() {
    	Test.startTest();
				
		Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Admin'][0];
		
		User adminUser = new User(
							LastName = 'test user 1', 
                             Username = 'b2bcsysadmin@example.com', 
                             Email = 'test.1@example.com', 
                             Alias = 'Germany', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
		
		System.runAs(adminUser){
			Id testCampaignId = ERTAILTestHelper.createCampaignMME();

			Campaign__c cmp = [select Id from Campaign__c where Id =: testCampaignId][0];
	
			PageReference pgRef = Page.ERTAILOrdersDetail;
			Test.setCurrentPage(pgRef);

			ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);
				
			ERTAILDeliveryInstallationFeesController ctrl = new ERTAILDeliveryInstallationFeesController();
			
			ctrl.campaignId = cmp.Id;

			Campaign_Market__c cmpMkt = [select Id, RTCOMMarket__r.Id from Campaign_Market__c where Campaign__r.Id =: cmp.Id];

			ctrl.selectedMarketId = cmpMkt.RTCOMMarket__r.Id;

			ctrl.MktFee = 10;

			Map<Id,ERTAILDeliveryInstallationFeesController.MarketFee> mapMarketFees = ctrl.mapMarketFees;

			Boolean b = ERTAILDeliveryInstallationFeesController.isHQPM & ERTAILDeliveryInstallationFeesController.isProductionAgency;
			
			pgRef.getParameters().put('index', '0');
			ctrl.updateFee();
			ctrl.updateDeliveryMode();
			
			ctrl.feeType = 'df';
			ctrl.submitForApproval();
			
			ctrl.feeType = 'if';
			ctrl.submitForApproval();
			
			ctrl.applyMktFee();
		}
			
		Test.stopTest();
			
    }
}
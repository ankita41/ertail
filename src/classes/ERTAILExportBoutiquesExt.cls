/*****
*   @Class      :   ERTAILExportBoutiquesExt.cls
*   @Description:   Extension developed for ERTAILCampaignExportBoutiques.page, ERTAILCampaignMarketExportBoutiques.page, ERTAILExportBoutiques.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*  Ankita Singhal  2 Apr 2017       Ertail  Enhancements 2017     
----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILExportBoutiquesExt {

    public class CustomException extends Exception{}

    public String xlsHeader {get {return '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';}}

    public String worksheetAutofilter {get { return '<x:AutoFilter x:Range="R1C1:R7C44"></x:AutoFilter>';}}

    public String worksheetHeader {get{return '<Worksheet ss:Name="Boutique details">';}}
    
    public String workbookEnd {get{return '</Worksheet></Workbook>';}}

    public List<List<Row>> btqRowList {get; set;}

    public ERTAILExportBoutiquesExt() {
        fillBoutiqueRows(null);
    }
    Map<String,String>campBtqDelivMode=new Map<String,String>(); //Added by Promila to display delivery mode of campaign boutique
    public ERTAILExportBoutiquesExt(ApexPages.StandardController stdController) {
        SObjectType sObjectType = stdController.getRecord().getSObjectType();

        SObject record = stdController.getRecord();
        
        List<Id> boutiqueIds = new List<Id>();
        
        Set<Id> marketIds = new Set<Id>();
        
        if(record instanceof Campaign__c){
            for(Campaign_Boutique__c cmpBtq : [select Delivery_Mode__c   //Added field by Promila on 28/3/16 **/
                                                    , RTCOMBoutique__r.Id
                                                    , Campaign_Market__r.RTCOMMarket__r.Id
                                                from
                                                    Campaign_Boutique__c
                                                where
                                                    Campaign_Market__r.Campaign__r.Id = : record.Id]){
                boutiqueIds.add(cmpBtq.RTCOMBoutique__r.Id);
                marketIds.add(cmpBtq.Campaign_Market__r.RTCOMMarket__r.Id);
                campBtqDelivMode.put(cmpBtq.RTCOMBoutique__r.Id,cmpBtq.Delivery_Mode__c); //Added by Promila to display delivery mode of campaign boutique
            }
        }
        else if(record instanceof Campaign_Market__c){
            for(Campaign_Boutique__c cmpBtq : [select Delivery_Mode__c   //Added field by Promila on 28/3/16 **/
                                                    , RTCOMBoutique__r.Id
                                                    , Campaign_Market__r.RTCOMMarket__r.Id
                                                from
                                                    Campaign_Boutique__c
                                                where
                                                    Campaign_Market__r.Id = : record.Id]){
                boutiqueIds.add(cmpBtq.RTCOMBoutique__r.Id);
                marketIds.add(cmpBtq.Campaign_Market__r.RTCOMMarket__r.Id);
                campBtqDelivMode.put(cmpBtq.RTCOMBoutique__r.Id,cmpBtq.Delivery_Mode__c); //Added by Promila to display delivery mode of campaign boutique
            }
        }
        
        for(RTCOMBoutique__c btq : [select
                                        Id
                                    from
                                        RTCOMBoutique__c
                                    where
                                        RTCOMMarket__r.Id in : marketIds
                                        and RecordTypeId =: Schema.SObjectType.RTCOMBoutique__c.getRecordTypeInfosByName().get('Warehouse').getRecordTypeId()]){
            boutiqueIds.add(btq.Id);
        }
        
        fillBoutiqueRows(boutiqueIds);
    }

    public void fillBoutiqueRows(List<Id> boutiqueIds){
        btqRowList = new List<List<Row>>();     
        List<Row> currentList = new List<Row>();
        btqRowList.add(currentList);
        
        Id warehouseRecordTypeID = Schema.SObjectType.RTCOMBoutique__c.getRecordTypeInfosByName().get('Warehouse').getRecordTypeId();
        
        List<RTCOMBoutique__c> btqList;
        if(boutiqueIds != null){
            btqList = [select 
                            Id
                            , RecordTypeId
                            , RECOBoutique__r.Id
                            , Name
                            , Location__r.Name
                            , RTCOMMarket__r.Name
                
                            , Considered_Type__c
                            , Typology__c
                            , Languages__c
                            , RECOConcept__c
                            , Opening_Date__c
    
                            , RECOBoutique__r.boutique_location_account__r.Street_English__c
                            , RECOBoutique__r.boutique_location_account__r.Zip_Postal_Code_English__c
                            , RECOBoutique__r.boutique_location_account__r.City_English__c
                            , RECOBoutique__r.boutique_location_account__r.State_Province_English__c
                            , RECOBoutique__r.boutique_location_account__r.Country_English__c

                            , RECOBoutique__r.boutique_location_account__r.BillingStreet
                            , RECOBoutique__r.boutique_location_account__r.BillingPostalCode
                            , RECOBoutique__r.boutique_location_account__r.BillingCity
                            , RECOBoutique__r.boutique_location_account__r.BillingState
                            , RECOBoutique__r.boutique_location_account__r.BillingCountry

                            , Location__r.Id
                            , RTCOMMarket__r.Location__r.Id
                        from
                            RTCOMBoutique__c
                        where
                            Id in : boutiqueIds
                        order by 
                            RTCOMMarket__r.Name
                            , Name];
        }
        else{
            btqList = [select 
                            Id
                            , RecordTypeId
                            , RECOBoutique__r.Id
                            , Name
                            , Location__r.Name
                            , RTCOMMarket__r.Name
                
                            , Considered_Type__c
                            , Typology__c
                            , Languages__c
                            , RECOConcept__c
                            , Opening_Date__c
    
                            , RECOBoutique__r.boutique_location_account__r.Street_English__c
                            , RECOBoutique__r.boutique_location_account__r.Zip_Postal_Code_English__c
                            , RECOBoutique__r.boutique_location_account__r.City_English__c
                            , RECOBoutique__r.boutique_location_account__r.State_Province_English__c
                            , RECOBoutique__r.boutique_location_account__r.Country_English__c

                            , RECOBoutique__r.boutique_location_account__r.BillingStreet
                            , RECOBoutique__r.boutique_location_account__r.BillingPostalCode
                            , RECOBoutique__r.boutique_location_account__r.BillingCity
                            , RECOBoutique__r.boutique_location_account__r.BillingState
                            , RECOBoutique__r.boutique_location_account__r.BillingCountry

                            , Location__r.Id
                            , RTCOMMarket__r.Location__r.Id
                        from
                            RTCOMBoutique__c
                        order by 
                            RTCOMMarket__r.Name
                            , Name];
        }
        
        Set<Id> accountIdSet = new Set<Id>();
        
        for(RTCOMBoutique__c btq : btqList){
            accountIdSet.add(btq.Location__r.Id);
            accountIdSet.add(btq.RTCOMMarket__r.Location__r.Id);
        }
        
        Map<Id, Account> locationMap = new Map<Id, Account>([select
                                                                Name
                                                                , Main_Contact__r.Id
                                                                , Main_Contact__r.Name
                                                                , Main_Contact__r.Email
                                                                , Main_Contact__r.Phone
                                                                , Main_Contact__r.OtherPhone

                                                                , Invoice_Contact__r.Name
                                                                , Invoice_Contact__r.Email
                                                                , Invoice_Contact__r.Phone
                                                                , Invoice_Contact__r.OtherPhone
                                                                , Invoice_Name__c
                                                                , BillingStreet
                                                                , BillingPostalCode
                                                                , BillingCity
                                                                , BillingState
                                                                , BillingCountry
                        
                                                                , Delivery_Contact__r.Name
                                                                , Delivery_Contact__r.Email
                                                                , Delivery_Contact__r.Phone
                                                                , Delivery_Contact__r.OtherPhone
                                                                , Delivery_Name__c
                                                                , ShippingStreet
                                                                , ShippingPostalCode
                                                                , ShippingCity
                                                                , ShippingState
                                                                , ShippingCountry
                                                                , Delivery_Mode__c
                                                                , Delivery_Schedule__c
                                                                , Delivery_Waste_Removal__c
                                                                , Delivery_Lifting_Truck__c
                                                                , Delivery_Lead_Time__c
                                                                , Delivery_Comments__c
                                                                , Delivery_Additional_Information__c
                                                                , Delivery_Incoterms__c
                                                                ,Decotobedeliveredinsideboutique__c
                                                            from
                                                                Account
                                                            where
                                                                Id in : accountIdSet]);
        
        for(RTCOMBoutique__c btq : btqList){

            if(currentList.size() == 1000){
                currentList = new List<Row>();
                btqRowList.add(currentList);
            }

            Integer index = 1;

            Account btqLocation = btq.Location__r.Id != null ? locationMap.get(btq.Location__r.Id) : null;
            Account mktLocation = btq.RTCOMMarket__r.Location__r.Id != null ? locationMap.get(btq.RTCOMMarket__r.Location__r.Id) : null;
            
            Row excelRow = new Row();

            excelRow.addStringCell(btq.RTCOMMarket__r.Name, 't2', index++);
            excelRow.addStringCell(btq.RECOBoutique__r.Id != null ? btq.RECOBoutique__r.Id : btq.Id, 't2', index++);
            excelRow.addStringCell(btq.Name, 't2', index++);
            excelRow.addStringCell(btqLocation != null? btqLocation.Name : null, 't2', index++);


            if(btq.RecordTypeId == warehouseRecordTypeID){
                if(btqLocation != null && btqLocation.ShippingCity != null && btqLocation.ShippingCity != ''){
                    excelRow.addStringCell(btqLocation.ShippingStreet, 't2', index++);
                    excelRow.addStringCell(btqLocation.ShippingPostalCode, 't2', index++);
                    excelRow.addStringCell(btqLocation.ShippingCity, 't2', index++);
                    excelRow.addStringCell(btqLocation.ShippingState, 't2', index++);
                    excelRow.addStringCell(btqLocation.ShippingCountry, 't2', index++);
                }
                else if (mktLocation != null){
                    excelRow.addStringCell(mktLocation.ShippingStreet, 't2', index++);
                    excelRow.addStringCell(mktLocation.ShippingPostalCode, 't2', index++);
                    excelRow.addStringCell(mktLocation.ShippingCity, 't2', index++);
                    excelRow.addStringCell(mktLocation.ShippingState, 't2', index++);
                    excelRow.addStringCell(mktLocation.ShippingCountry, 't2', index++);
                }
                else{
                    index += 5;
                }
            }
            else{
                excelRow.addStringCell(btq.RECOBoutique__r.boutique_location_account__r.Street_English__c != null ? btq.RECOBoutique__r.boutique_location_account__r.Street_English__c : btq.RECOBoutique__r.boutique_location_account__r.BillingStreet, 't2', index++);
                excelRow.addStringCell(btq.RECOBoutique__r.boutique_location_account__r.Zip_Postal_Code_English__c != null ? btq.RECOBoutique__r.boutique_location_account__r.Zip_Postal_Code_English__c : btq.RECOBoutique__r.boutique_location_account__r.BillingPostalCode, 't2', index++);
                excelRow.addStringCell(btq.RECOBoutique__r.boutique_location_account__r.City_English__c != null ? btq.RECOBoutique__r.boutique_location_account__r.City_English__c : btq.RECOBoutique__r.boutique_location_account__r.BillingCity, 't2', index++);
                excelRow.addStringCell(btq.RECOBoutique__r.boutique_location_account__r.State_Province_English__c != null ? btq.RECOBoutique__r.boutique_location_account__r.State_Province_English__c : btq.RECOBoutique__r.boutique_location_account__r.BillingState, 't2', index++);
                excelRow.addStringCell(btq.RECOBoutique__r.boutique_location_account__r.Country_English__c != null ? btq.RECOBoutique__r.boutique_location_account__r.Country_English__c : btq.RECOBoutique__r.boutique_location_account__r.BillingCountry, 't2', index++);
            }

    
            // Main Contact Data
            if(btqLocation != null && btqLocation.Main_Contact__r.Id != null){
                excelRow.addStringCell(btqLocation.Main_Contact__r.Name, 't2', index++);
                excelRow.addStringCell(btqLocation.Main_Contact__r.Email, 't2', index++);
                excelRow.addStringCell(btqLocation.Main_Contact__r.Phone, 't2', index++);
                excelRow.addStringCell(btqLocation.Main_Contact__r.OtherPhone, 't2', index++);
            }
            else if(mktLocation != null && mktLocation.Main_Contact__r.Id != null){
                excelRow.addStringCell(mktLocation.Main_Contact__r.Name, 't2', index++);
                excelRow.addStringCell(mktLocation.Main_Contact__r.Email, 't2', index++);
                excelRow.addStringCell(mktLocation.Main_Contact__r.Phone, 't2', index++);
                excelRow.addStringCell(mktLocation.Main_Contact__r.OtherPhone, 't2', index++);
            }
            else{
                index += 4;
            }
            
            
            excelRow.addStringCell(btq.Considered_Type__c != null ? btq.Considered_Type__c : btq.Typology__c, 't2', index++);
            excelRow.addStringCell(btq.Languages__c, 't2', index++);
            excelRow.addStringCell(btq.RECOConcept__c, 't2', index++);
            excelRow.addDateCell(btq.Opening_Date__c, 't2', index++);
            
            
            if(btqLocation != null && btqLocation.ShippingCity != null && btqLocation.ShippingCity != ''){
                excelRow.addStringCell(btqLocation.Delivery_Name__c, 't2', index++);
                excelRow.addStringCell(btqLocation.ShippingStreet, 't2', index++);
                excelRow.addStringCell(btqLocation.ShippingPostalCode, 't2', index++);
                excelRow.addStringCell(btqLocation.ShippingCity, 't2', index++);
                excelRow.addStringCell(btqLocation.ShippingState, 't2', index++);
                excelRow.addStringCell(btqLocation.ShippingCountry, 't2', index++);

                excelRow.addStringCell(btqLocation.Delivery_Contact__r.Name, 't2', index++);
                excelRow.addStringCell(btqLocation.Delivery_Contact__r.Email, 't2', index++);
                excelRow.addStringCell(btqLocation.Delivery_Contact__r.Phone, 't2', index++);
                excelRow.addStringCell(btqLocation.Delivery_Contact__r.OtherPhone, 't2', index++);

                //excelRow.addStringCell(btqLocation.Delivery_Mode__c, 't2', index++); //Commented by Promila on 28/3/16
                excelRow.addStringCell(campBtqDelivMode.get(btq.Id), 't2', index++);  //Added by Promila to display delivery mode of campaign boutique
                excelRow.addBooleanCell(btqLocation.Decotobedeliveredinsideboutique__c, 't2',false, index++);
                excelRow.addStringCell(btqLocation.Delivery_Schedule__c, 't2', index++);
                excelRow.addBooleanCell(btqLocation.Delivery_Waste_Removal__c, 't2', false, index++);
                excelRow.addBooleanCell(btqLocation.Delivery_Lifting_Truck__c, 't2', false, index++);
                excelRow.addStringCell(btqLocation.Delivery_Lead_Time__c, 't2', index++);
                excelRow.addStringCell(btqLocation.Delivery_Comments__c, 't2', index++);
                excelRow.addStringCell(btqLocation.Delivery_Additional_Information__c, 't2', index++);
                excelRow.addStringCell(btqLocation.Delivery_Incoterms__c, 't2', index++);
            }
            else if (mktLocation != null){
                excelRow.addStringCell(mktLocation.Delivery_Name__c, 't2', index++);
                excelRow.addStringCell(mktLocation.ShippingStreet, 't2', index++);
                excelRow.addStringCell(mktLocation.ShippingPostalCode, 't2', index++);
                excelRow.addStringCell(mktLocation.ShippingCity, 't2', index++);
                excelRow.addStringCell(mktLocation.ShippingState, 't2', index++);
                excelRow.addStringCell(mktLocation.ShippingCountry, 't2', index++);

                excelRow.addStringCell(mktLocation.Delivery_Contact__r.Name, 't2', index++);
                excelRow.addStringCell(mktLocation.Delivery_Contact__r.Email, 't2', index++);
                excelRow.addStringCell(mktLocation.Delivery_Contact__r.Phone, 't2', index++);
                excelRow.addStringCell(mktLocation.Delivery_Contact__r.OtherPhone, 't2', index++);

               // excelRow.addStringCell(mktLocation.Delivery_Mode__c, 't2', index++); // Commented by Promila
                excelRow.addStringCell(campBtqDelivMode.get(btq.Id), 't2', index++);  // Added by Promila to display delivery mode of campaign boutique
                excelRow.addBooleanCell(mktLocation.Decotobedeliveredinsideboutique__c, 't2',false, index++); // Added for Req 65
                
                excelRow.addStringCell(mktLocation.Delivery_Schedule__c, 't2', index++);
                excelRow.addBooleanCell(mktLocation.Delivery_Waste_Removal__c, 't2', false, index++);
                excelRow.addBooleanCell(mktLocation.Delivery_Lifting_Truck__c, 't2', false, index++);
                excelRow.addStringCell(mktLocation.Delivery_Lead_Time__c, 't2', index++);
                excelRow.addStringCell(mktLocation.Delivery_Comments__c, 't2', index++);
                excelRow.addStringCell(mktLocation.Delivery_Additional_Information__c, 't2', index++);
                excelRow.addStringCell(mktLocation.Delivery_Incoterms__c, 't2', index++);
            }
            else{
                index += 18;
            }
            
            if(btqLocation != null && btqLocation.BillingCity != null && btqLocation.BillingCity != ''){
                excelRow.addStringCell(btqLocation.Invoice_Name__c, 't2', index++);
                excelRow.addStringCell(btqLocation.BillingStreet, 't2', index++);
                excelRow.addStringCell(btqLocation.BillingPostalCode, 't2', index++);
                excelRow.addStringCell(btqLocation.BillingCity, 't2', index++);
                excelRow.addStringCell(btqLocation.BillingState, 't2', index++);
                excelRow.addStringCell(btqLocation.BillingCountry, 't2', index++);

                excelRow.addStringCell(btqLocation.Invoice_Contact__r.Name, 't2', index++);
                excelRow.addStringCell(btqLocation.Invoice_Contact__r.Email, 't2', index++);
                excelRow.addStringCell(btqLocation.Invoice_Contact__r.Phone, 't2', index++);
                excelRow.addStringCell(btqLocation.Invoice_Contact__r.OtherPhone, 't2', index++);
            }
            else if(mktLocation != null){
                excelRow.addStringCell(mktLocation.Invoice_Name__c, 't2', index++);
                excelRow.addStringCell(mktLocation.BillingStreet, 't2', index++);
                excelRow.addStringCell(mktLocation.BillingPostalCode, 't2', index++);
                excelRow.addStringCell(mktLocation.BillingCity, 't2', index++);
                excelRow.addStringCell(mktLocation.BillingState, 't2', index++);
                excelRow.addStringCell(mktLocation.BillingCountry, 't2', index++);

                excelRow.addStringCell(mktLocation.Invoice_Contact__r.Name, 't2', index++);
                excelRow.addStringCell(mktLocation.Invoice_Contact__r.Email, 't2', index++);
                excelRow.addStringCell(mktLocation.Invoice_Contact__r.Phone, 't2', index++);
                excelRow.addStringCell(mktLocation.Invoice_Contact__r.OtherPhone, 't2', index++);
            }
            currentList.add(excelRow);
        }
    }
    
    public class Cell {
        public String cStyle {get; set;}
        public Integer cIndex {get; set;}
        public String cType {get; set;}
        public String cValue {get; set;}
        
        public Cell(String cStyle, Integer cIndex, String cType, String cValue){
            this.cStyle = cStyle;
            this.cIndex = cIndex;
            this.cType = cType;
            this.cValue = cValue;
        }
    }
    
    public class Row {
        public List<Cell> cells {get; set;} 
    
        public Row(){
            cells = new List<Cell>();
        }
        
        public void addStringCell(String s, String style, Integer index){
            if(s != null && s != ''){
                cells.add(new Cell(style, index, 'String', s));
            }
        }

        public void addBooleanCell(Boolean b, String style, Boolean emptyCell, Integer index){
            if(!emptyCell){
                cells.add(new Cell(style, index, 'String', '' + b));
            }
        }
    
        public void addDateCell(Date d, String style, Integer index){
            if(d != null){
                cells.add(new Cell(style, index, 'String', DateTime.newInstance(d, Time.newInstance(0,0,0,0)).format('dd MMM yyyy')));
            }
        }
    }   
}
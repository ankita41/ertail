/*****
*   @Class      :   ERTAILExportOrdersExt.cls
*   @Description:   Extension developed for ERTAILCampaignExportOrders.page and ERTAILCampaignMarketExportOrders.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Priya        25 Jan'17         Added "Sticker's color" field of Boutique Space object in export order xls for Req 37new  
    Ankita Singhal  2 Apr 2017       Ertail  Enhancements 2017             
*****/

public with sharing class ERTAILExportOrdersExt {

    public class CustomException extends Exception{}

    public String xlsHeader {get {return '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';}}

    public String spaceWorksheetAutofilter {get { return '<x:AutoFilter x:Range="R2C1:R7C45"></x:AutoFilter>';}}
    public String ffWorksheetAutofilter {get { return '<x:AutoFilter x:Range="R2C1:R7C13"></x:AutoFilter>';}}

    public String spaceWorksheetHeader {get{return '<Worksheet ss:Name="Order details">';}}
    public String ffWorksheetHeader {get{return '</Worksheet><Worksheet ss:Name="Fixture and Furnitures details">';}}
    
    public String workbookEnd {get{return '</Worksheet>';}} /***Ticket #RM0022405236: removed workbook tag to resolve issue of corrupted file, Modified By: Promila **/

    public List<Id> campaignMarketList {get; set;}

    public List<List<Row>> spaceRowList {get; set;}
    public List<List<Row>> ffRowList {get; set;}

    public Boolean isPartial {get; set;}

    public ERTAILExportOrdersExt(ApexPages.StandardController stdController) {
        SObject record = stdController.getRecord();
        
        isPartial = false;

        if(record instanceof Campaign__c){          
            if(ApexPages.currentPage().getParameters().containsKey('mktIds')){
                campaignMarketList = (List<Id>) ApexPages.currentPage().getParameters().get('mktIds').split(',');
                isPartial = true;
            }
            else{
                campaignMarketList = new List<Id>();
    
                List<Campaign_Market__c> cmpMkts = [select Id from Campaign_Market__c where Campaign__r.Id =: record.Id];
                for(Campaign_Market__c cmpMkt : cmpMkts){
                    campaignMarketList.add(cmpMkt.Id);
                }
            }
        }
        else if(record instanceof Campaign_Market__c){
            campaignMarketList = new List<Id>{record.Id};
        }
        
        fillSpaceRows();        
        fillFFRows();        
    }
    
    public void fillSpaceRows(){
        spaceRowList = new List<List<Row>>();       
        List<Row> currentList = new List<Row>();
        spaceRowList.add(currentList);
        
        for(Boutique_Order_Item__c boi : [select 
                                            // boutique order item data
                                            b.Name
                                            , b.Unit_Cost__c
                                            , b.Quantity_to_Order__c
                                            , b.Actual_Cost_Eur__c
                                            , b.OP_unit_max_cost_Eur__c
                                            , b.OP_total_max_cost_Eur__c
                                            , b.Order_Comment__c
                                            // campaign item data
                                            , b.Campaign_Item_to_order__r.Name
                                          //  , b.Campaign_Item_to_order__r.Actual_Cost_EUR__c
                                          //  , b.Campaign_Item_to_order__r.Max_Cost_EUR__c
                                            // space data
                                            , b.Campaign_Item_to_order__r.Space__c
                                            , b.Campaign_Item_to_order__r.Space__r.Name
                                            , b.Campaign_Item_to_order__r.Space__r.Category__c
                                            , b.Campaign_Item_to_order__r.Space__r.Has_Creativity__c
                                            , b.Campaign_Item_to_order__r.Space__r.Has_Dimensions__c
                                            , b.Campaign_Item_to_order__r.Space__r.Has_Quantity__c
                                          //  , b.Campaign_Item_to_order__r.Space__r.OP_max_cost_Eur__c
                                            // boutique space data
                                            , b.Boutique_Space__c
                                            , b.Boutique_Space__r.Name
                                            , b.Boutique_Space__r.Space__c
                                            , b.Boutique_Space__r.Creativity_Order__c
                                            , b.Boutique_Space__r.Comment__c
                                            , b.Boutique_Space__r.Decoration_Allowed__c
                                            , b.Boutique_Space__r.Hanging_System_Available__c
                                            , b.Boutique_Space__r.plug_available__c
                                            , b.Boutique_Space__r.Closed_facade__c
                                            , b.Boutique_Space__r.Depth__c
                                            , b.Boutique_Space__r.Height__c
                                            , b.Boutique_Space__r.Width__c
                                            , b.Boutique_Space__r.Window_Depth__c
                                            , b.Boutique_Space__r.Window_Height__c
                                            , b.Boutique_Space__r.Window_Width__c
                                            , b.Boutique_Space__r.Quantity_to_Order__c
                                            , b.Boutique_Space__r.Stickers__c
                                            , b.Boutique_Space__r.Sticker_color__c
                                            , b.Boutique_Space__r.Excluded_for_future_campaigns__c
                                            // FF data
                                            , b.Permanent_Fixture_And_Furniture__r.Name
                                            , b.Permanent_Fixture_And_Furniture__r.Unit_Cost__c
                                            // boutique data
                                            , b.Campaign_Boutique__r.Name
                                            , b.Campaign_Boutique__r.Delivery_Mode__c
                                            , b.Campaign_Boutique__r.Delivery_Date__c
                                            , b.Campaign_Boutique__r.Delivery_Fee__c
                                            , b.Campaign_Boutique__r.Warehouse__r.Id
                                            , b.Campaign_Boutique__r.Warehouse__r.Name
                                            , b.Campaign_Boutique__r.End_Date__c
                                            , b.Campaign_Boutique__r.Fixture_Furnitures_Costs_Eur__c
                                            , b.Campaign_Boutique__r.Installation_Fee__c
                                            , b.Campaign_Boutique__r.Launch_Date__c
                                            , b.Campaign_Boutique__r.OP_actual_cost__c
                                            , b.Campaign_Boutique__r.OP_Max_Agency_Fee__c
                                            , b.Campaign_Boutique__r.OP_max_cost_Eur__c
                                            , b.Campaign_Boutique__r.Scope_Limitation__c
                                            , b.Campaign_Boutique__r.Typology__c
                                            , b.Campaign_Boutique__r.Campaign_Market__r.Id
                                            , b.Campaign_Boutique__r.CampaignId__c
                                            , b.Campaign_Boutique__r.RTCOMBoutique__c
                                            , b.Campaign_Boutique__r.RTCOMBoutique__r.RECOBoutique__c
                                            , b.Campaign_Boutique__r.RTCOMBoutique__r.Languages__c /** added as per ticket reference no. # RM0021257847 **/
                                            ,b.Campaign_Boutique__r.RTCOMBoutique__r.Location__r.Showbook_format__c
                                           , b.Campaign_Boutique__r.RTCOMBoutique__r.RTCOMMarket__r.Name
                                         //   ,(Select id,Name,Quantity__c,HQPM_Quantity__c,MPM_Quantity__c from Creativity_Items__r order by Name) 
                                             from Boutique_Order_Item__c b
                                            where b.Campaign_Boutique__r.Campaign_Market__r.Id in: campaignMarketList
                                                    and b.Permanent_Fixture_And_Furniture__c = null and 
                                                    b.Quantity_to_Order__c!=0 and  // added for req 22 & 60
                                                    b.Quantity_to_Order__c!=null and                 // added for req 22 & 60
                                                    b.Campaign_Item_to_order__c!=null and            // added for req 22 & 60
                                                    (b.Boutique_Space__c=null or 
                                                    (b.Boutique_Space__r.Decoration_Allowed__c ='Yes' and        // added for req 22 & 60
                                                    b.Boutique_Space__r.Excluded_for_future_campaigns__c=false))    // added for req 22 & 60
                                            
                                                    
                                                    
                                            order by b.Campaign_Boutique__r.RTCOMBoutique__r.RTCOMMarket__r.Name
                                                    , b.Campaign_Boutique__r.RTCOMBoutique__r.RTCOMMarket__c
                                                    , b.Campaign_Boutique__r.RTCOMBoutique__r.Name
                                                    , b.Campaign_Boutique__r.RTCOMBoutique__c
                                                    , b.Campaign_Item_to_order__r.Space__c]){               

            if(currentList.size() == 1000){
                currentList = new List<Row>();
                spaceRowList.add(currentList);
            }

            Row excelRow = new Row();
    
            Integer index = 1;
            
            excelRow.addStringCell(boi.Campaign_Boutique__r.RTCOMBoutique__r.RTCOMMarket__r.Name, 't2', index++);
            excelRow.addStringCell(boi.Campaign_Boutique__r.Name, 't2', index++);
            excelRow.addStringCell(boi.Campaign_Boutique__r.RTCOMBoutique__r.RECOBoutique__c != null ? boi.Campaign_Boutique__r.RTCOMBoutique__r.RECOBoutique__c : boi.Campaign_Boutique__r.RTCOMBoutique__c, 't2', index++);
            excelRow.addDateCell(boi.Campaign_Boutique__r.Launch_Date__c, 't3', index++);
            excelRow.addDateCell(boi.Campaign_Boutique__r.End_Date__c, 't3', index++);
            excelRow.addStringCell(boi.Campaign_Boutique__r.RTCOMBoutique__r.Languages__c, 't3', index++); /** Added as per ticket reference no. # RM0021257847 **/
            excelRow.addStringCell(boi.Campaign_Boutique__r.RTCOMBoutique__r.Location__r.Showbook_format__c, 't3', index++); //// added for req 65
            excelRow.addStringCell(boi.Campaign_Boutique__r.Delivery_Mode__c, 't3', index++); /** Added Delivery Mode 10-mar-2016 **/
            if(boi.Campaign_Boutique__r.Delivery_Mode__c == 'Pickup'){
                excelRow.addDateCell(boi.Campaign_Boutique__r.Delivery_Date__c, 't3', index++);
                excelRow.addStringCell('', 't2', index++);
                excelRow.addStringCell('', 't2', index++);
                
            }
            else if(boi.Campaign_Boutique__r.Delivery_Mode__c == 'Boutique Delivery'){
                excelRow.addStringCell('', 't2', index++);
                excelRow.addDateCell(boi.Campaign_Boutique__r.Delivery_Date__c, 't3', index++);
                excelRow.addStringCell('', 't2', index++);
            }
            else if(boi.Campaign_Boutique__r.Delivery_Mode__c == 'Warehouse Delivery'){
                excelRow.addStringCell('', 't2', index++);
                excelRow.addStringCell('', 't2', index++);
                excelRow.addDateCell(boi.Campaign_Boutique__r.Delivery_Date__c, 't3', index++);
            }
            else{
                excelRow.addStringCell('', 't2', index++);
                excelRow.addStringCell('', 't2', index++);
                excelRow.addStringCell('', 't2', index++);
                
            }
            excelRow.addStringCell(boi.Campaign_Boutique__r.Warehouse__r.Name, 't2', index++);
            excelRow.addStringCell(boi.Campaign_Boutique__r.Scope_Limitation__c, 't2', index++);
           // excelRow.addStringCell(boi.Campaign_Boutique__r.Typology__c, 't2', index++);
            excelRow.addNumberCell(boi.Campaign_Boutique__r.OP_actual_cost__c, 't90', index++);
            excelRow.addNumberCell(boi.Campaign_Boutique__r.OP_max_cost_Eur__c, 't90', index++);
            excelRow.addNumberCell(boi.Campaign_Boutique__r.OP_Max_Agency_Fee__c, 't90', index++);
            excelRow.addNumberCell(boi.Campaign_Boutique__r.Delivery_Fee__c, 't90', index++);
            excelRow.addNumberCell(boi.Campaign_Boutique__r.Installation_Fee__c, 't90', index++);
           // excelRow.addStringCell(boi.Name, 't2', index++);
            excelRow.addNumberCell(boi.Unit_Cost__c, 't90', index++);
            excelRow.addNumberCell(boi.Quantity_to_Order__c, 't9', index++);
            excelRow.addNumberCell(boi.Actual_Cost_Eur__c, 't90', index++);
            excelRow.addNumberCell(boi.OP_unit_max_cost_Eur__c, 't90', index++);
            excelRow.addNumberCell(boi.OP_total_max_cost_Eur__c, 't90', index++);
            /*String Creativityitems ='';
            if(boi.Creativity_Items__r!=null){
                for(Creativity_Items__c ci :boi.Creativity_Items__r){
                    if(ci.Quantity__c!=0 && ci.Quantity__c!=null){
                    Creativityitems = Creativityitems + ci.Name+' : ' +ci.Quantity__c + ' , ';
                    }
                }
            }*/
          //   excelRow.addStringCell(Creativityitems , 't2', index++);
           excelRow.addStringCell(boi.Order_Comment__c, 't2', index++);
            excelRow.addStringCell(boi.Campaign_Item_to_order__r.Name, 't2', index++);
           // excelRow.addNumberCell(boi.Campaign_Item_to_order__r.Actual_Cost_EUR__c, 't90', index++);
           // excelRow.addNumberCell(boi.Campaign_Item_to_order__r.Max_Cost_EUR__c, 't90', index++);
            excelRow.addStringCell((boi.Boutique_Space__c == null ? '&lt;multiple&gt;' : boi.Boutique_Space__r.Name), 't2', index++);
            excelRow.addNumberCell(boi.Boutique_Space__r.Creativity_Order__c, 't9', index++);
            excelRow.addStringCell(boi.Boutique_Space__r.Comment__c, 't2', index++);
            excelRow.addStringCell(boi.Boutique_Space__r.Decoration_Allowed__c, 't2', index++);
            excelRow.addBooleanCell(boi.Boutique_Space__r.Hanging_System_Available__c, 't2', boi.Boutique_Space__c == null, index++);
            excelRow.addBooleanCell(boi.Boutique_Space__r.plug_available__c, 't2', boi.Boutique_Space__c == null, index++);
            excelRow.addBooleanCell(boi.Boutique_Space__r.Closed_facade__c, 't2', boi.Boutique_Space__c == null, index++);
            excelRow.addNumberCell(boi.Boutique_Space__r.Depth__c, 't9', index++);
            excelRow.addNumberCell(boi.Boutique_Space__r.Height__c, 't9', index++);
            excelRow.addNumberCell(boi.Boutique_Space__r.Width__c, 't9', index++);
            excelRow.addNumberCell(boi.Boutique_Space__r.Window_Depth__c, 't9', index++);
            excelRow.addNumberCell(boi.Boutique_Space__r.Window_Height__c, 't9', index++);
            excelRow.addNumberCell(boi.Boutique_Space__r.Window_Width__c, 't9', index++);
            excelRow.addNumberCell(boi.Boutique_Space__r.Quantity_to_Order__c, 't9', index++);
            excelRow.addStringCell(boi.Boutique_Space__r.Stickers__c, 't2', index++);
            excelRow.addStringCell(boi.Boutique_Space__r.Sticker_color__c, 't2', index++);
            excelRow.addStringCell(boi.Campaign_Item_to_order__r.Space__r.Name, 't2', index++);
           // excelRow.addStringCell(boi.Campaign_Item_to_order__r.Space__r.Category__c, 't2', index++);
            excelRow.addBooleanCell(boi.Campaign_Item_to_order__r.Space__r.Has_Creativity__c, 't2', boi.Campaign_Item_to_order__r.Space__r.Name == null, index++);
            excelRow.addBooleanCell(boi.Campaign_Item_to_order__r.Space__r.Has_Dimensions__c, 't2', boi.Campaign_Item_to_order__r.Space__r.Name == null, index++);
            excelRow.addBooleanCell(boi.Campaign_Item_to_order__r.Space__r.Has_Quantity__c, 't2', boi.Campaign_Item_to_order__r.Space__r.Name == null, index++);
           // excelRow.addNumberCell(boi.Campaign_Item_to_order__r.Space__r.OP_max_cost_Eur__c, 't90', index);
            
            currentList.add(excelRow);
        }
    }
    
    public void fillFFRows(){
        ffRowList = new List<List<Row>>();      
        List<Row> currentList = new List<Row>();
        ffRowList.add(currentList);
        
        for(Boutique_Order_Item__c boi : [  select 
                                                // boutique data
                                                b.Campaign_Boutique__r.RTCOMBoutique__r.RTCOMMarket__r.Name
                                                , b.Campaign_Boutique__r.RTCOMBoutique__c
                                                , b.Campaign_Boutique__r.RTCOMBoutique__r.RECOBoutique__c
                                                , b.Campaign_Boutique__r.RTCOMBoutique__r.Languages__c /** added as per ticket reference no. # RM0021257847 **/
                                                , b.Campaign_Boutique__r.Name
                                                //, b.Campaign_Boutique__r.Typology__c
                                                , b.Campaign_Boutique__r.Delivery_Mode__c
                                                , b.Campaign_Boutique__r.Delivery_Date__c
                                                , b.Campaign_Boutique__r.Warehouse__r.Name
                                                , b.Campaign_Boutique__r.Fixture_Furnitures_Costs_Eur__c
                                                // boutique order item data
                                               // , b.Name
                                                , b.Permanent_Fixture_And_Furniture__r.Name
                                                , b.Unit_Cost__c
                                                , b.Quantity_to_Order__c
                                                , b.Actual_Cost_Eur__c
                                                , b.Order_Comment__c
                                                // FF data
                                            from 
                                                Boutique_Order_Item__c b
                                            where 
                                                b.Campaign_Boutique__r.Campaign_Market__r.Id in: campaignMarketList
                                                and b.Permanent_Fixture_And_Furniture__c != null
                                            order by 
                                                b.Campaign_Boutique__r.RTCOMBoutique__r.RTCOMMarket__r.Name
                                                , b.Campaign_Boutique__r.Name
                                                , b.Name]){

            if(currentList.size() == 1000){
                currentList = new List<Row>();
                ffRowList.add(currentList);
            }

            Row excelRow = new Row();
    
            Integer index = 1;
            
            excelRow.addStringCell(boi.Campaign_Boutique__r.RTCOMBoutique__r.RTCOMMarket__r.Name, 't2', index++);
            excelRow.addStringCell(boi.Campaign_Boutique__r.Name, 't2', index++);
            excelRow.addStringCell(boi.Campaign_Boutique__r.RTCOMBoutique__r.RECOBoutique__c != null ? boi.Campaign_Boutique__r.RTCOMBoutique__r.RECOBoutique__c : boi.Campaign_Boutique__r.RTCOMBoutique__c, 't2', index++);
            excelRow.addStringCell(boi.Campaign_Boutique__r.RTCOMBoutique__r.Languages__c, 't2', index++); /** added as per ticket reference no. # RM0021257847 **/
            excelRow.addStringCell(boi.Campaign_Boutique__r.Delivery_Mode__c, 't2', index++); /** Added Delivery Mode 10-mar-2016 **/
            if(boi.Campaign_Boutique__r.Delivery_Mode__c == 'Pickup'){
                excelRow.addDateCell(boi.Campaign_Boutique__r.Delivery_Date__c, 't3', index++);
                excelRow.addStringCell('', 't2', index++);
                excelRow.addStringCell('', 't2', index++);
                
            }
            else if(boi.Campaign_Boutique__r.Delivery_Mode__c == 'Boutique Delivery'){
                excelRow.addStringCell('', 't2', index++);
                excelRow.addDateCell(boi.Campaign_Boutique__r.Delivery_Date__c, 't3', index++);
                excelRow.addStringCell('', 't2', index++);
            }
            else if(boi.Campaign_Boutique__r.Delivery_Mode__c == 'Warehouse Delivery'){
                excelRow.addStringCell('', 't2', index++);
                excelRow.addStringCell('', 't2', index++);
                excelRow.addDateCell(boi.Campaign_Boutique__r.Delivery_Date__c, 't3', index++);
            }
            else{
                excelRow.addStringCell('', 't2', index++);
                excelRow.addStringCell('', 't2', index++);
                excelRow.addStringCell('', 't2', index++);
            }
            excelRow.addStringCell(boi.Campaign_Boutique__r.Warehouse__r.name, 't2', index++);
           // excelRow.addStringCell(boi.Campaign_Boutique__r.Typology__c, 't2', index++);
            excelRow.addNumberCell(boi.Campaign_Boutique__r.Fixture_Furnitures_Costs_Eur__c, 't90', index++);
           // excelRow.addStringCell(boi.Name, 't2', index++);
            excelRow.addStringCell(boi.Permanent_Fixture_And_Furniture__r.Name, 't2', index++);
            excelRow.addNumberCell(boi.Unit_Cost__c, 't90', index++);
            excelRow.addNumberCell(boi.Quantity_to_Order__c, 't9', index++);
            excelRow.addNumberCell(boi.Actual_Cost_Eur__c, 't90', index++);
            excelRow.addStringCell(boi.Order_Comment__c, 't2', index++);
                                            
            currentList.add(excelRow);
        }
    }
    
    public class Cell {
        public String cStyle {get; set;}
        public Integer cIndex {get; set;}
        public String cType {get; set;}
        public String cValue {get; set;}
        
        public Cell(String cStyle, Integer cIndex, String cType, String cValue){
            this.cStyle = cStyle;
            this.cIndex = cIndex;
            this.cType = cType;
            this.cValue = cValue;
        }
    }
    
    public class Row {
        public List<Cell> cells {get; set;} 
    
        public Row(){
            cells = new List<Cell>();
        }
        
        public void addStringCell(String s, String style, Integer index){
            if(s != null && s != ''){
                cells.add(new Cell(style, index, 'String', s));
            }
           /*  else{
                cells.add(new Cell(style, index, 'String', ''));
            } */
        }

        public void addBooleanCell(Boolean b, String style, Boolean emptyCell, Integer index){
            if(!emptyCell){
                cells.add(new Cell(style, index, 'String', '' + b));
            }
          /*  else{
                cells.add(new Cell(style, index, 'String', ''));
            }*/
        }
    
        public void addNumberCell(Decimal d, String style, Integer index){
            if(d != null){
                cells.add(new Cell(style, index, 'Number', '' + d));
            }
            /* else{
                cells.add(new Cell(style, index, 'String', ''));
            }*/
        }

        public void addDateCell(Date d, String style, Integer index){
            if(d != null){
                cells.add(new Cell(style, index, 'String', DateTime.newInstance(d, Time.newInstance(0,0,0,0)).format('dd MMM yyyy')));
            }
            /* else{
                cells.add(new Cell(style, index, 'String', ''));
            }*/
        }
    }   
}
/*
*   @Class      :   ERTAILFeedItemHandler.cls
*   @Description:   Handler of the ERTAILFeedItem.trigger 
*   @Author     :   Jacky Uy
*   @Created    :   05 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*                         
*/

public without sharing class ERTAILFeedItemHandler {
    public static Map<Id,FeedItem> queryCampaignItemImgs(List<Campaign_Item__c> campItems){
        Map<Id,FeedItem> mapCampaignItemImg = new Map<Id,FeedItem>();
        for(Campaign_Item__c ci : campItems){
            mapCampaignItemImg.put(ci.Id, new FeedItem());
        }
        
        for(FeedItem f : [
                SELECT ContentFileName, ParentId, RelatedRecordId 
                FROM FeedItem 
                WHERE ParentId IN :mapCampaignItemImg.keySet() AND Type = 'ContentPost' 
                ORDER BY CreatedDate
            ]){
            mapCampaignItemImg.put(f.ParentId, f);
        }
        return mapCampaignItemImg;
    }
}
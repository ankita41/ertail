/*
*   @Class      :   ERTAILFileUploadController.cls
*   @Description:   Controller of the ERTAILFileUpload.page
*   @Author     :   Jacky Uy
*   @Created    :   06 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*                         
*/

public with sharing class ERTAILFileUploadController {
	public String parentName {
		get{
			if(parentName==null){
				parentName = ApexPages.currentPage().getParameters().get('parent');
			}
			return parentName;
		}
		set;
	}
	
	public String ciName {
		get{
			if(ciName==null){
				ciName = ApexPages.currentPage().getParameters().get('name');
			}
			return ciName;
		}
		set;
	}
	
	public Id imageId{
		get{
			return imageId;
		}
		private set;
	}
	
	public Id objId {
		get{
			if(objId==null){
				objId = ApexPages.currentPage().getParameters().get('id');
				if(objId==null)
					throw new CustomException('The required URL parameter \'id\' is null. Please contact your administrator for assistance.');
			}
			return objId;
		} 
		private set;
	}
	
	private String objName {
		get{
			if(objName==null){
				objName = objId.getSObjectType().getDescribe().getName();
			}
			return objName;
		} set;
	}
	
	private Schema.SObjectType objType {
		get{
			if(objType==null){
				objType = Schema.getGlobalDescribe().get(objName);
			}
			return objType;
		} set;
	}
	
	private Map<String, Schema.SobjectField> fieldsMap {
		get{
			if(fieldsMap==null){
				fieldsMap = objType.getDescribe().fields.getMap();
			}
			return fieldsMap;
		} set;
	}
	
	private sObject varSObject {get;set;}
	
	public FeedItem varFeed {get;set;}
	public Boolean isDone {get;set;}
	
	public ERTAILFileUploadController(){
		if(!fieldsMap.containsKey('image_id__c'))
			throw new CustomException('A text field called Image_ID__c cannot be found associated to this record. Please contact your administrator for assistance.');
			
		if(!fieldsMap.get('image_id__c').getDescribe().isAccessible())
			throw new CustomException('Your profile has no access to the Image ID field associated to this record. Please contact your administrator for assistance.');
			
		varFeed = new FeedItem(ParentId = objId);
		varSObject = objType.newSObject();
		varSObject.put('Id', objId);
		
		isDone = false;
	}
	
	public void uploadFile(){
		insert varFeed;
		varFeed = [SELECT RelatedRecordId FROM FeedItem WHERE Id = :varFeed.Id];
		imageId = varFeed.RelatedRecordId;		
		
		varSObject.put('Image_ID__c', imageId);
		update varSObject;
		
		varFeed = new FeedItem(ParentId = objId);
		isDone = true;
	}
	
	public class CustomException extends Exception {
		//throw new CustomException();
	}
}
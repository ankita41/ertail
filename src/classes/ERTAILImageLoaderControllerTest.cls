/*
*   @Class      :   ERTAILImageLoaderControllerTest.cls
*   @Description:   Test methods for class ImageLoaderController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILImageLoaderControllerTest {
    
    static testMethod void myUnitTest() {

        RTCOMMarket__c mkt = new RTCOMMarket__c(Name='mkt');

        insert mkt;

        RTCOMBoutique__c btq = new RTCOMBoutique__c(Name='test btq', RTCOMMarket__c = mkt.Id);

        insert btq;
        
        ImageLoaderController ctrl = new ImageLoaderController();
        
        ctrl.parent = btq;
        Feeditem f =ctrl.Feeditem;
//      Boolean refreshPage = ctrl.refreshPage;
        String s = ctrl.Imageid;
        String s1 = ctrl.Imageid1;
        String s2 = ctrl.Imageid2;
        ctrl.feedItem = new FeedItem(ParentId = btq.Id, Type='ContentPost',contentFileName='abc');
        ctrl.saveImage();
        ctrl.deleteImage();

        
            
    }
}
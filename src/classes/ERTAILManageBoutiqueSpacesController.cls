/*****
*   @Class      :   ERTAILManageBoutiqueSpacesController.cls
*   @Description:   Extension developed for ERTAILManageBoutiqueSpaces.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Priya           20 Jan'17       Updated for Req 45 For ERTAIL 2017 release                    
*****/

public with sharing class ERTAILManageBoutiqueSpacesController {

    public class CustomException extends Exception {}

    public Boolean showHeader {get; private set;}

    public Boolean showMarkets {get{return items.size() >1; }}

    public Id showWindowRecordType {get; private set;}
    public Id inStoreRecordType {get; private set;}

    public String selectedMarketId {get;set;}
    public RTCOMMarket__c market {get;private set;}

    public List<Id> boutiqueIds {get; private set;}
    public Map<Id, RTCOMBoutique__c> boutiques {get; private set;}
    
    public Map<Id, List<Id>> showWindowsPerBoutique {get; private set;}
    public Map<Id, Boutique_Space__c> showWindowMap {get; private set;}

    Transient public Map<Id, Map<Id,SpaceWrapper>> inStoreSpacePerBoutique {get; private set;}
    public Map<Id, Map<Id,SpaceWrapper>> inStoreSpacePerBoutique_Pagination {get; private set;}
    
    public static Boolean isProductionAgency{ get{ return ERTailMasterHelper.isProductionAgency; } }
   
    Transient List<Boutique_Space__c> marketBoutiqueSpaces {get; private set;}
    
    public List<Id> inStoreSpacesList {get;private set; }
    
    public Map<Id, Space__c> inStoreSpacesMap {get;private set; }
    
    public class SpaceWrapper{
        public Space__c space {get; private set;}

        public List<Boutique_Space__c> btqSpaces {get; set;}
    
        public SpaceWrapper(Space__c space){
            this.space = space;
            btqSpaces = new List<Boutique_Space__c>();
        }
    }

    public Integer showWindowMaxLength {get; private set;}
    
    public List<Integer> showWindowSuffixes {get; private set;}

    public Map<Id, List<String>> test {get; private set;}

    public String boutiqueSpaceKeyPrefix {
        get{
            return Schema.sObjectType.Boutique_Space__c.getKeyPrefix();
        }
    }

    public void init(){
        showHeader = true;

        List<RTCOMMarket__c> allMarkets = [Select c.Id, c.Name, c.Zone__c From RTCOMMarket__c c order by c.Name];

        // market options
        marketOptions = new List<SelectOption>();
        for(RTCOMMarket__c m : allMarkets){
            marketOptions.add(new SelectOption(m.Id, m.Name));
        }       


        if(items.size() == 1){
                market = allMarkets.get(0);
            selectedMarketId = allMarkets[0].Id;       
        }
        else{
            if(ApexPages.CurrentPage().getParameters().get('Id') == null){
                market = allMarkets.get(0);
                selectedMarketId = market.Id;
            }
            else{
                selectedMarketId = ApexPages.CurrentPage().getParameters().get('Id');
                market = [Select c.Name, c.Zone__c From RTCOMMarket__c c Where Id =: selectedMarketId];
            }
        }

        showWindowRecordType = Schema.SObjectType.Boutique_Space__c.getRecordTypeInfosByName().get('Show Window').getRecordTypeId();
        inStoreRecordType = Schema.SObjectType.Boutique_Space__c.getRecordTypeInfosByName().get('In Store').getRecordTypeId();

        showWindowsPerBoutique = new Map<Id, List<Id>>();
        showWindowMap = new Map<Id,Boutique_Space__c>();
        inStoreSpacePerBoutique = new Map<Id, Map<Id,SpaceWrapper>>();

        boutiqueIds = new List<Id>();
        boutiques = new Map<Id, RTCOMBoutique__c>();
        //Priya : Added where clause to exclude closed Boutiques for Req#45
        for(RTCOMBoutique__c btq : [Select c.Id, c.Name From RTCOMBoutique__c c Where c.RTCOMMarket__r.Id =: selectedMarketId and c.Excluded_for_future_campaigns__c=False and (RecordTypeId = null or RecordType.Name != 'Warehouse') order by c.Name]){
            boutiqueIds.add(btq.Id);
            boutiques.put(btq.Id, btq);
        
            showWindowsPerBoutique.put(btq.Id, new List<Id>());
            inStoreSpacePerBoutique.put(btq.Id, new Map<Id, SpaceWrapper>());           
        }
        
        
        for(Boutique_Space__c bs : [select b.Space__r.Id, b.Space__r.Name, b.Space__r.Decoration_Alias__c from Boutique_Space__c b where b.RTCOMBoutique__r.RTCOMMarket__r.Id =: selectedMarketId and b.Space__r.Category__c != 'Show Window' and b.Excluded_for_future_Campaigns__c = false]){
            for(Id boutiqueId : boutiqueIds){
                if(!inStoreSpacePerBoutique.get(boutiqueId).containsKey(bs.Space__r.Id)){
                    inStoreSpacePerBoutique.get(boutiqueId).put(bs.Space__r.Id, new SpaceWrapper(new Space__c(Id = bs.Space__r.Id, Name = bs.Space__r.Name, Decoration_Alias__C = bs.Space__r.Decoration_Alias__c)));
                }
            }
        }
    
        // all boutique spaces linked to market
        marketBoutiqueSpaces = [Select 
                                    b.Space__r.Decoration_Alias__c
                                    , b.Space__r.Category__c
                                    , b.Space__r.Has_Creativity__c
                                    , b.Space__c
                                    , b.Space__r.Id
                                    , b.Space__r.Name
                                    , (Select Id From Boutique_Order_Items__r)
                                    , b.Comment__c
                                    , b.Space_Quantity__c
                                    , b.Decoration_Allowed__c
                                    , b.Creativity_Order__c
                                    , b.RTCOMBoutique__r.Id
                                From 
                                    Boutique_Space__c b 
                                where 
                                    b.RTCOMBoutique__r.RTCOMMarket__r.Id =: selectedMarketId 
                                    and Excluded_for_future_Campaigns__c = false
                                    and b.RTCOMBoutique__r.Excluded_for_future_campaigns__c=False
                                order by
                                    b.Creativity_Order__c desc
                                    //, b.Space__r.Decoration_Alias__c
                                    , b.Space__r.Name];


        // set of all In Store spaces related to the market
        Set<Id> inStoreSpacesSet = new Set<Id>();
        inStoreSpacesList = new List<Id>();
        inStoreSpacesMap = new Map<Id, Space__c>();
        
        for(Boutique_Space__c btqSpace : marketBoutiqueSpaces){
            if(btqSpace.Space__r.Category__c != 'Show Window'){
                
                if(!inStoreSpacesSet.contains(btqSpace.Space__r.Id)){
                    Space__c space = new Space__c(Id = btqSpace.Space__r.Id, Name = btqSpace.Space__r.Name, Decoration_Alias__c = btqSpace.Space__r.Decoration_Alias__c);
                    inStoreSpacesList.add(btqSpace.Space__r.Id);
                    inStoreSpacesSet.add(btqSpace.Space__r.Id);
                    inStoreSpacesMap.put(btqSpace.Space__r.Id, space);          
                }
            if(btqSpace.Space__r.Id != null)
                inStoreSpacePerBoutique.get(btqSpace.RTCOMBoutique__r.Id).get(btqSpace.Space__r.Id).btqSpaces.add(btqSpace);                
            }
            else{
                // if the list of spaces in the boutique is empty or if the creativity order is null, the space is added at the end of the list...
                if(btqSpace.Creativity_Order__c == null || showWindowsPerBoutique.get(btqSpace.RTCOMBoutique__r.Id).size() == 0){
                    showWindowsPerBoutique.get(btqSpace.RTCOMBoutique__r.Id).add(btqSpace.Id);
                }
                // ...otherwise it is added in the first position of the list.
                // As the Boutique_space__c List is sorted by creativity_order DESC, the last element that will be added at the position 0 will be 
                // the one with the lowest, non-null, creativity order. The the elements with a null creativity order will be added at the end of the list
                else{
                    showWindowsPerBoutique.get(btqSpace.RTCOMBoutique__r.Id).add(0, btqSpace.Id);
                }
                showWindowMap.put(btqSpace.Id, btqSpace);
            }
        }
        

        // determinate max number of show windows
        showWindowMaxLength = 0;
        for(List<Id> btqSpaceList : showWindowsPerBoutique.values()){
            showWindowMaxLength = Math.max(showWindowMaxLength, btqSpaceList.size());
        }

        // generate suffixes list
        showWindowSuffixes = new List<Integer>();
        for(Integer i = 1; i <= showWindowMaxLength; i++){
            showWindowSuffixes.add(i);
        }

        test = new Map<Id, List<String>>();
        
        for(Id btqId :boutiqueIds){
            List<String> ls = new List<String>();
            for(Integer i = showWindowsPerBoutique.get(btqId).size(); i <= showWindowMaxLength; i++){
                ls.add('');
            }
            test.put(btqId, ls);
        }
    }

    public ERTAILManageBoutiqueSpacesController(){
        init();
    }


    public String boutiqueSpaceBoutiqueFieldId {
        get{
            return BoutiqueSpaceFieldIdWebService.getFieldId('Boutique');
        }
    }
    
    public String boutiqueSpaceSpaceFieldId {
        get{
            return BoutiqueSpaceFieldIdWebService.getFieldId('Space');
        }
    }

    // Contains all the distinct markets. Displayed only if user is HQPM
    public List<SelectOption> marketOptions {get; private set;}

    public List<SelectOption> Items {get{return marketOptions;}}


    public String btqSpaceId {get;set;}

    public PageReference deleteBoutiqueSpace(){

        if(btqSpaceId != null){

            try{
                delete new Boutique_Space__c(Id = btqSpaceId);
            }
            catch(Exception e){
                update new Boutique_Space__c(Id = btqSpaceId, Excluded_for_future_campaigns__c = true);
            }
        }
        btqSpaceId = null;

        init();
        
        return null;
    }
    
    //pagination
    List<RTCOMBoutiqueWrapper> RTCOMBoutique_Wrp_List {get;set;}        
        
    // instantiate the StandardSetController from a query locator       
/*    public ApexPages.StandardSetController con {        
        get {       
            if(con == null) {       
                con = new ApexPages.StandardSetController(Database.getQueryLocator([Select c.Id, c.Name From RTCOMBoutique__c c Where c.RTCOMMarket__r.Id =: selectedMarketId and c.Excluded_for_future_campaigns__c=False and (RecordTypeId = null or RecordType.Name != 'Warehouse') order by c.Name]));     
                // sets the number of records in each page set      
                con.setPageSize(15);        
            }       
            return con;     
        }       
        set;        
    }       
        
*/        
    public string pNumber{
    get{
        pnumber = string.valueOf(counter);
        return pNumber ;
    }
    set;
    }
    
    private integer counter = ApexPages.currentPage().getParameters().containsKey('vCounter') ? (Apexpages.currentpage().getparameters().get('vCounter') == '') ? 0:(integer.valueof(Apexpages.currentpage().getparameters().get('vCounter')) )  : 0 ; //0;  //keeps track of the offset
    private integer list_size = 30; //sets the page size or number of rows
    public integer total_size //used to show user the total size of the list
    {
        get{
            if(total_size == null)
            {
                total_size = [Select count() From RTCOMBoutique__c c Where c.RTCOMMarket__r.Id =: selectedMarketId and c.Excluded_for_future_campaigns__c=False and (RecordTypeId = null or RecordType.Name != 'Warehouse')] ;
            }
            return total_size ;
        }
        
        private set ;
    }
    
    // returns a list of wrapper objects for the sObjects in the current page set       
    public List<RTCOMBoutiqueWrapper> getRTCOMBoutique_Wrp_List() {     
        RTCOMBoutique_Wrp_List = new List<RTCOMBoutiqueWrapper>();      
        inStoreSpacePerBoutique_Pagination = new Map<Id, Map<Id,SpaceWrapper>>();       
                
        Map<id,RTCOMBoutique__c> mapRTCOMBoutique = new Map<id,RTCOMBoutique__c>() ;        
                
        List<RTCOMBoutique__c> RTCOMBoutiqueList  = [Select c.Id, c.Name From RTCOMBoutique__c c Where c.RTCOMMarket__r.Id =: selectedMarketId and c.Excluded_for_future_campaigns__c=False and (RecordTypeId = null or RecordType.Name != 'Warehouse') order by c.Name limit :list_size offset:counter] ;
        if( RTCOMBoutiqueList <> null )
        {
            for (RTCOMBoutique__c RTCOMBoutiqueObj: RTCOMBoutiqueList)       
            {       
                RTCOMBoutique_Wrp_List.add(new RTCOMBoutiqueWrapper(RTCOMBoutiqueObj));     
                inStoreSpacePerBoutique_Pagination.put(RTCOMBoutiqueObj.Id, new Map<Id, SpaceWrapper>());       
                mapRTCOMBoutique.put(RTCOMBoutiqueObj.id,RTCOMBoutiqueObj) ;                
            }       
                    
            for(Boutique_Space__c bs : [select b.Space__r.Id, b.Space__r.Name, (Select Id From Boutique_Order_Items__r), b.Space__r.Category__c, b.Space__r.Has_Creativity__c       
                                        , b.Space__c, b.Comment__c, b.Space_Quantity__c, b.Decoration_Allowed__c, b.Creativity_Order__c     
                                        , b.RTCOMBoutique__r.Id, b.Space__r.Decoration_Alias__c from Boutique_Space__c b where b.RTCOMBoutique__r.RTCOMMarket__r.Id =: selectedMarketId and b.Space__r.Category__c != 'Show Window' and b.Excluded_for_future_Campaigns__c = false and b.RTCOMBoutique__r.Excluded_for_future_campaigns__c=False]){     
                        
                for( RTCOMBoutique__c RTCOMBoutiqueObj: RTCOMBoutiqueList ) {       
                    if(!inStoreSpacePerBoutique_Pagination.get(RTCOMBoutiqueObj.Id).containsKey(bs.Space__r.Id)){       
                        inStoreSpacePerBoutique_Pagination.get(RTCOMBoutiqueObj.id).put(bs.Space__r.Id, new SpaceWrapper(new Space__c(Id = bs.Space__r.Id, Name = bs.Space__r.Name, Decoration_Alias__C = bs.Space__r.Decoration_Alias__c)));       
                    }       
                }       
                        
                if(bs.Space__r.Id != null && bs.Space__r.Category__c != 'Show Window' && mapRTCOMBoutique.containsKey(bs.RTCOMBoutique__r.Id) ){        
                    inStoreSpacePerBoutique_Pagination.get(bs.RTCOMBoutique__r.Id).get(bs.Space__r.Id).btqSpaces.add(bs);       
                }       
                        
            }       
        }
        return RTCOMBoutique_Wrp_List;      
    }       
    public list<id> getBoutiqueId_pagination(){
        List<id> btqId = new List<Id>();
        List<RTCOMBoutique__c> RTCOMBoutiqueList  = [Select c.Id, c.Name From RTCOMBoutique__c c Where c.RTCOMMarket__r.Id =: selectedMarketId and c.Excluded_for_future_campaigns__c=False and (RecordTypeId = null or RecordType.Name != 'Warehouse') order by c.Name limit :list_size offset:counter] ;
        if( RTCOMBoutiqueList <> null )
        {
                for (RTCOMBoutique__c RTCOMBoutiqueObj: RTCOMBoutiqueList)       
                { 
                    btqid.add(RTCOMBoutiqueObj.id);
                }
        }
        return btqid;
        
    }    
   
            
     public class RTCOMBoutiqueWrapper {        
        
        public Boolean checked{ get; set; }     
        public RTCOMBoutique__c RTCOMBoutique { get; set;}      
            
        public RTCOMBoutiqueWrapper(){      
            RTCOMBoutique = new RTCOMBoutique__c();     
            //checked = false;      
        }       
            
        public RTCOMBoutiqueWrapper(RTCOMBoutique__c RTCOMBoutiqueObj){     
            RTCOMBoutique = RTCOMBoutiqueObj;       
            //checked = false;      
        }       
            
    }
    
    
    
     public PageReference Beginning() { //user clicked beginning
      counter = 0;
      return null;
   }

   public PageReference Previous() { //user clicked previous button
      counter -= list_size;
      return null;
   }

   public PageReference Next() { //user clicked next button
      counter += list_size;
      return null;
   }

   public PageReference End() { //user clicked end
      //counter = total_size - math.mod(total_size, list_size);
      if(math.mod(total_size, list_size) == 0){
          counter = total_size - list_size;
      }
      else{
          counter = total_size - math.mod(total_size, list_size);
      }
      return null;
   }
    
   public Boolean getDisablePrevious() { 
      //this will disable the previous and beginning buttons
      if (counter > 0) 
          return false; 
      else 
          return true;
   }

   public Boolean getDisableNext() { //this will disable the next and end buttons
      if (counter + list_size < total_size) 
          return false; 
      else 
          return true;
   }
    
    

}
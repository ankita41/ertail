/*
*   @Class      :   ERTAILManageBoutiqueSpacesControllerTest.cls
*   @Description:   Test methods for class ERTAILManageBoutiqueSpacesController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILManageBoutiqueSpacesControllerTest {
    
    static testMethod void myUnitTest() {
        ERTAILTestHelper.createCampaignMME();
    
        ERTAILManageBoutiqueSpacesController ctrl = new ERTAILManageBoutiqueSpacesController();

        ctrl.deleteBoutiqueSpace();
       /* String o=ctrl.boutiqueSpaceBoutiqueFieldId;
        String p=ctrl.boutiqueSpacespaceFieldId;
        String r = ctrl.pnumber;
        Decimal t = ctrl.total_size;*/
        ctrl.getRTCOMBoutique_Wrp_List();
        ctrl.getBoutiqueId_pagination();
        ctrl.Beginning();
        ctrl.Previous();
        ctrl.Next();
        ctrl.End();
        ctrl.getDisablePrevious();
        ctrl.getDisableNext();
        ERTAILManageBoutiqueSpacesController.SpaceWrapper wrap = new ERTAILManageBoutiqueSpacesController.SpaceWrapper(new Space__c());

        String s = ctrl.boutiqueSpaceKeyPrefix;
   }
}
/*****
*   @Class      :   ERTAILMarketExt.cls
*   @Description:   Extension developed for ERTAILMarketDeleteButton.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILMarketExt {
	public class CustomException extends Exception{}
	
	RTCOMMarket__c market;
    public ERTAILMarketExt(ApexPages.StandardController stdController) {
        market = (RTCOMMarket__c) stdController.getRecord();
    }
    
    public Boolean isReferencedInCampaigns{
    	get{
    		if (isReferencedInCampaigns == null){
    			isReferencedInCampaigns = [Select Id from Campaign_Market__c where RTCOMMarket__c = :market.Id].size() > 0;
    		}
    		return isReferencedInCampaigns;
    	}
    	private set;
    }
    
    public void hideFromFutureCampaigns(){
    	//throw new CustomException('In hide');
    	market.Hide_from_Future_Campaigns__c = true;
    	update market;
    }
    
    public void deleteMarket(){
    	//throw new CustomException('In delete');
    	delete market;
    	//Schema.DescribeSObjectResult result = RTCOMMarket__c.SObjectType.getDescribe();
    }
}
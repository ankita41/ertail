/*
*   @Class      :   ERTAILMarketHandler.cls
*   @Description:   Handles trigger events for the RTCOMMarket__c object. Please note that no sharing is specified for the class to inherit from the caller.
*   @Author     :   TPA
*   @Created    :   08 APR 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*   TPA				13 Aug 2015		Set the Market account ownership to the first available admin                       
*/
public class ERTAILMarketHandler {
	private static Id firstAdminId = [Select Id, Name From User u where IsActive=true and Profile.Name='System Admin' order by lastlogindate desc limit 1].Id;
		
	public static void UpdateCampaignMarketOwnerAfterMarketOwnerUpdate(Map<Id, RTCOMMarket__c> oldMap, Map<Id, RTCOMMarket__c> newMap){
		Map<Id,Id> mapMktOwners = new Map<Id,Id>();
        for(Id mktId : newMap.keySet()){
        	if (oldMap.get(mktId).OwnerId != newMap.get(mktId).OwnerId)
				mapMktOwners.put(mktId, newMap.get(mktId).OwnerId);
        }
        
        if(mapMktOwners.isEmpty()){
        	return;
        }
        
        List<Campaign_Market__c> updCampMkts = new List<Campaign_Market__c>();
        for(Campaign_Market__c cm : [SELECT OwnerId, RTCOMMarket__c FROM Campaign_Market__c WHERE RTCOMMarket__c IN :mapMktOwners.keySet()]){
            cm.OwnerId =  mapMktOwners.get(cm.RTCOMMarket__c);
            updCampMkts.add(cm);
        }
        update updCampMkts;
    }
	
	public static void CreateERTAILLocationAndOwnerBeforeMarketInsert(List<RTCOMMarket__c> newMarkets){
		List<Id> mktIds = new List<Id>();
		List<Id> gpIds = new List<Id>();
		List<Id> ownerIds = new List<Id>();
				
		Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERTAIL Location').getRecordTypeId();
		List<Account> accToInsert = new List<Account>();
		List<Group> queuesToInsert = new List<Group>();
		
		for(RTCOMMarket__c newMkt : newMarkets){
			accToInsert.add(new Account(Name=newMkt.Name, RecordTypeId=rtId, OwnerId=firstAdminId));
			queuesToInsert.add(new Group(Name='ERTAIL ' + newMkt.Name, Type='Queue',DoesSendEmailToMembers=true));
			mktIds.add(newMkt.Id);
		}
		
		insert accToInsert;
		insert queuesToInsert;

		for (Group gp : queuesToInsert){
			gpIds.add(gp.Id);
		}
		if (!Test.isRunningTest())
			InsertQueuesObject(gpIds);

		for(Integer i=0 ; i<newMarkets.size() ; i++){
			newMarkets[i].Location__c = accToInsert[i].Id;
		}
	}
	
	public static void UpdateERTAILMarketOwnerAfterInsert(List<RTCOMMarket__c> newMarkets){

		List<String> mktNames = new List<String>();
		List<Id> mktIds = new List<Id>();
		for	(RTCOMMarket__c mkt : newMarkets){
			mktNames.add('ERTAIL ' + mkt.Name);
			mktIds.add(mkt.Id);
		}

		SetMarketOwner(mktIds, mktNames);
	}

	// If the Market has never been used, the market and associated boutiques and Boutique spaces are deleted
	// If the Market is referenced by a Campaign Market, the Market is not deleted but marked as "Excluded from future campaigns"
	public static void ControlBeforeERTAILMarketDelete(Map<Id, RTCOMMarket__c> oldMarketsMap){
		Set<Id> marketsToHide = new Set<Id>();
		
		// Request the Campaign Markets linked to one of the deleted Markets
		for (Campaign_Market__c cm : [Select Id, RTCOMMarket__c from Campaign_Market__c where RTCOMMarket__c in :oldMarketsMap.keySet()]){
			marketsToHide.add(cm.RTCOMMarket__c);
		}

		if (marketsToHide.size() == 0){
			return;
		}
		
		for(Id mktId : marketsToHide){
			oldMarketsMap.get(mktId).addError('Market is referenced in a Campaign and cannot be deleted.  Please mark it as excluded from future Campaigns instead.');
		}
	}

	// Create an ERTAIL Market whenever a new RECO Market is created
	public static List<RTCOMMarket__c> InsertERTAILMarketAfterRECOMarketInsert(Map<Id, Market__c> newRECOMap){
		// for each newly created RECO market, create a new ERTAIL Market
		List<RTCOMMarket__c> mktsToInsertList = new List<RTCOMMarket__c>();
		List<String> listOfMarkets = new List<String>();
		for(Market__c mkt : newRECOMap.values()){
			mktsToInsertList.add(new RTCOMMarket__c (Name = mkt.Name, RECOMarket__c = mkt.Id));
			listOfMarkets.add(mkt.Name);
		}
		
		if (mktsToInsertList.size() > 0){
			insert mktsToInsertList;
			if (!Test.isRunningTest())
				sendEmailFollowingMarketCreation(listOfMarkets);
		}
		
		return mktsToInsertList;
	}
	
	private static void sendEmailFollowingMarketCreation(List<String> marketNames){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage() ;
		List<String> toAddresses = new List<String>();
		for (User adminUser : [Select email from User where Profile.Name = :ERTAILMasterHelper.ERT_HQPM and IsActive=true]){
			toAddresses.add(adminUser.email);
		}
		
		if(toAddresses.size() == 0){
			return;
		}
		mail.setToAddresses(toAddresses) ;
		mail.setSubject('New ERTAIL Market(s) Created');
		
		String body = '<html><body>'+
		                 '<br><br>The following ERTAIL Markets have just been created<br/>For each Market please:<ul><li>Change the owner to the appropriate Queue</li><li>Create a new ERTAIL Role and set it as a member of the queue</li></ul><table><tr><th>Market</th><th>Queue</th></tr>';
		for(String mktName : marketNames){
			body += '<tr><td>' + mktName + '</td><td>ERTAIL ' + mktName + '</td></tr>';
		}
		
		body += '</table>Regards,</body></html>';
				
		mail.setHtmlBody(body);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
	}

	@future
	private static void InsertQueuesObject(List<Id> gpIds){
		List<queuesobject> qus = new List<queuesobject >();
		for(Id gpId : gpIds){
			qus.add(new queuesobject (queueid=gpId, sobjecttype='RTCOMMarket__c'));
			qus.add(new queuesobject (queueid=gpId, sobjecttype='Campaign_Market__c'));
		}
		insert qus;
	}

	@future
	private static void SetMarketOwner(List<Id> mktIds, List<String> mktName){
		Map<String, QueueSobject> qsoGroupedByQueueName = new Map<String, QueueSobject>();
		for(QueueSobject qso : [Select q.Queue.Name, q.QueueId From QueueSobject q where SobjectType = 'RTCOMMarket__c' and q.Queue.Name in :mktName]){
			qsoGroupedByQueueName.put(qso.Queue.Name, qso);
		}
		
		List<RTCOMMarket__c> marketsToUpdate = new List<RTCOMMarket__c>();
		for(Integer i=0 ; i < mktIds.size() && i < mktName.size() ; i++){
			if(qsoGroupedByQueueName.containsKey('ERTAIL ' + mktName[i])){
				marketsToUpdate.add(new RTCOMMarket__c(Id = mktIds[i], OwnerId = qsoGroupedByQueueName.get('ERTAIL ' + mktName[i]).QueueId));
			}
		}
		
		update marketsToUpdate;
	}
	
}
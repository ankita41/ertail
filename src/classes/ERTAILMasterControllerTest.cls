/*
*   @Class      :   ERTAILMasterControllerTest.cls
*   @Description:   Test methods for class ERTAILMasterController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILMasterControllerTest {
    
    static testMethod void myUnitTest1() {      
        Id cmpId = ERTAILTestHelper.createCampaignMME();
        Campaign__c cmp = [select Id, Name from Campaign__c where Id =: cmpId];

        Boutique_Order_Item__c boi = [select Id, Campaign_item_To_Order__r.Id,Campaign_Boutique__c from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and boutique_Space__r.Space__r.has_Creativity__c = true][0];
        Boutique_Order_Item__c boi2 = [select Id, Campaign_item_To_Order__r.Id ,Campaign_Boutique__c from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and boutique_Space__r.Space__r.has_Creativity__c = false][0];
        Boutique_Order_Item__c boi3 = [select Id, Permanent_Fixture_And_Furniture__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and Campaign_item_To_Order__c = null][0];
        Campaign_boutique__c cb = [Select id,Campaign_Boutique__c.RTCOMBoutique__c from Campaign_boutique__c where id=:boi.Campaign_Boutique__c][0];
        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);

        ERTAILMasterController ctrl = new ERTAILMasterController(stdController);

        PageReference pgRef = Page.ERTAILOrdersDetail;
        pgRef.getParameters().put('btqOrderItemId', boi.Id);
        pgRef.getParameters().put('value', 'Approve');
        pgRef.getParameters().put('selectedScreen', 'bof');
        Test.setCurrentPage(pgRef);

        
        String s = ctrl.selectedScreen;
        s = ctrl.BtqCreativityStatusString;
        s = ctrl.BtqQuantityStatusString;
        s = ctrl.selectedView;
        s = ctrl.selectedBoutiqueId;
        s= ctrl.btqSpacesConflictsMapJSON;
        s= ctrl.spacesConflictsMapJSON;
        ctrl.selectedScreen = 'Visuals';
        ctrl.selectedMarketId = null;
        ctrl.Docancel();
        ctrl.redirectPopup();
        ctrl.redirectPopup1();
        ctrl.redirectBOIPopup();
        Double d = ctrl.MaxCostEur;
        d = ctrl.ActualCostEur;
        
        Boolean b;
        
        b = ERTAILMasterController.isAdmin;
        b = ERTAILMasterController.isMPM;
        b = ERTAILMasterController.isAgent;
        b = ERTAILMasterController.isHQPM;
        b = ERTAILMasterController.isCreativeAgency;
        b = ERTAILMasterController.isProductionAgency;
        b = ERTAILMasterController.isMPMorAdmin;
        b = ERTAILMasterController.isHQPMorAdmin;
        
        List<SelectOption> options = ctrl.viewOptions;
        options = ctrl.Items;
        options = ctrl.screenOptions;
        
        ctrl.getController();
        
        Id i = ctrl.feedId;
        
        Map<Id, Boutique_Order_Item__c> BtqOrderItemsOrderedByBtqSpacesId = ctrl.BtqOrderItemsOrderedByBtqSpacesId;
        
        
        ctrl.selectedView= ctrl.VIEW_MPM;
        ctrl.saveCreativityProposal();
        
        ctrl.ApproveMPMCreativityProposal();        

        pgRef.getParameters().put('fromStep', 'step1');
        pgRef.getParameters().put('toStep', 'step2');
        pgRef.getParameters().put('value', 'Reject');
        pgRef.getParameters().put('reason', 'Reject');
        pgRef.getParameters().put('quantity', '5');
        ctrl.ApproveMPMCreativityProposal();        

        
        Map<Id, Map<Id, Boutique_Order_Item__c>> BoiWithQuantityOrderedByBtqIdThenCampaignItemId = ctrl.BoiWithQuantityOrderedByBtqIdThenCampaignItemId;
        
        Map<Id, List<Campaign_Item__c>> CampaignItemsWithQuantityOrderedBySpaceId = ctrl.CampaignItemsWithQuantityOrderedBySpaceId;
        pgRef.getParameters().put('value', 'Approve');
        ctrl.ApproveMPMQuantityProposal();
        
        pgRef.getParameters().put('value', '');
        ctrl.ApproveMPMQuantityProposal();
      
        Map<Id, Map<Id, Boutique_Order_Item__c>> BtqOrderItemsOrderedByBtqIdThenFfId = ctrl.BtqOrderItemsOrderedByBtqIdThenFfId; 
        
        Map<Id, Boutique_Space__c> btqSpacesForMarketMap = ctrl.btqSpacesForMarketMap;
        
        ctrl.selectedView= ctrl.VIEW_HQPM;
        
        ctrl.saveCreativityProposal();
        
        System.assertEquals(ctrl.BoiWithQuantityOrderedByBtqIdThenCampaignItemId.size() > 0, true);

        s = ctrl.completedBoutiques;

        ctrl.saveQuantitycIProposal();
        
        Map<Id, Boolean> CampaignItemsInUse = ctrl.CampaignItemsInUse;
        
        Map<Id, Boolean> SpacesInUse = ctrl.SpacesInUse;
        
        ctrl.saveQuantityFFProposal();
        
        ctrl.generateRecommandations();
        
        ctrl.SendHQPMCreativityProposalToMPM();
        
        ctrl.SendHQPMQuantityProposalToMPM();
        
        /*ctrl.SendAgentCreativityProposalToMPM(); 
        ctrl.MarkMPMZoneCreativityProposalAsCompleted();
        ctrl.SendMPMQuantityProposalToHQPM();*/
        ctrl.refresh();
        decimal de= 1;
        Date ddate = System.today();
        ERTAILMasterController.ConflictWrapper cw = new ERTAILMasterController.ConflictWrapper(boi.id,de,de,s,ddate,ddate);
        cw.add(de,s,ddate,ddate);
        ctrl.exportorder();
       
        
      }
      
    static testMethod void myUnitTest2() {
        Id cmpId = ERTAILTestHelper.createCampaignMME();
        Campaign__c cmp = [select Id, Name from Campaign__c where Id =: cmpId];

        Boutique_Order_Item__c boi = [select Id, Campaign_item_To_Order__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and boutique_Space__r.Space__r.has_Creativity__c = true][0];
        Boutique_Order_Item__c boi2 = [select Id, Campaign_item_To_Order__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and boutique_Space__r.Space__r.has_Creativity__c = false][0];
        Boutique_Order_Item__c boi3 = [select Id, Permanent_Fixture_And_Furniture__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and Campaign_item_To_Order__c = null][0];

        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);

        ERTAILMasterController ctrl = new ERTAILMasterController(stdController);
        
        String s = ctrl.selectedScreen;
        s = ctrl.BtqCreativityStatusString;
        s = ctrl.BtqQuantityStatusString;
        s = ctrl.selectedView;
        
        Boolean b;
        
        b = ERTAILMasterController.isAdmin;
        b = ERTAILMasterController.isMPM;
        b = ERTAILMasterController.isAgent;
        b = ERTAILMasterController.isHQPM;
        b = ERTAILMasterController.isCreativeAgency;
        b = ERTAILMasterController.isProductionAgency;
        b = ERTAILMasterController.isMPMorAdmin;
        b = ERTAILMasterController.isHQPMorAdmin;
        
        List<SelectOption> options = ctrl.viewOptions;
        options = ctrl.Items;
        options = ctrl.screenOptions;
        
        ctrl.getController();
        
        Id i = ctrl.feedId;
        
        Map<Id, Boutique_Order_Item__c> BtqOrderItemsOrderedByBtqSpacesId = ctrl.BtqOrderItemsOrderedByBtqSpacesId;
        
        
        ctrl.selectedView= ctrl.VIEW_MPM;
        ctrl.saveCreativityProposal();
        
        PageReference pgRef = Page.ERTAILOrdersDetail;
        pgRef.getParameters().put('btqOrderItemId', boi.Id);
        pgRef.getParameters().put('value', 'Approve');

        pgRef.getParameters().put('fromStep', 'step1');
        pgRef.getParameters().put('toStep', 'step2');
        pgRef.getParameters().put('value', 'Reject');
        pgRef.getParameters().put('reason', 'Reject');
        pgRef.getParameters().put('quantity', '5');
        Test.setCurrentPage(pgRef);

        
        
        ctrl.selectedView= ctrl.VIEW_HQPM;
        
        ctrl.saveCreativityProposal();
        
        System.assertEquals(ctrl.BoiWithQuantityOrderedByBtqIdThenCampaignItemId.size() > 0, true);

        s = ctrl.completedBoutiques;

        ctrl.saveQuantitycIProposal();
        
        Map<Id, Boolean> CampaignItemsInUse = ctrl.CampaignItemsInUse;
        
        Map<Id, Boolean> SpacesInUse = ctrl.SpacesInUse;
        
        ctrl.saveQuantityFFProposal();
        
        ctrl.generateRecommandations();
        
        ctrl.SendMPMCreativityProposalToHQPM();
        
        ctrl.SendMPMCreativityProposalToHQPM();
        
        ctrl.SendAgentQuantityProposalToMPM();
        
        ctrl.SendAgentQuantityProposalToMPM();      
      }      
    
    
    static testMethod void myUnitTest3() {
        
        Id cmpId = ERTAILTestHelper.createCampaignMME();

        Campaign__c cmp = [select Id, Name from Campaign__c where Id =: cmpId];

        Boutique_Order_Item__c boi = [select Id, Campaign_item_To_ORder__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and boutique_Space__r.Space__r.has_Creativity__c = true][0];
        Boutique_Order_Item__c boi2 = [select Id, Campaign_item_To_ORder__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and boutique_Space__r.Space__r.has_Creativity__c = false][0];
        Boutique_Order_Item__c boi3 = [select Id, Permanent_Fixture_And_Furniture__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and Campaign_item_To_Order__c = null][0];

        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);

        ERTAILMasterController ctrl = new ERTAILMasterController(stdController);
        
        String s = ctrl.selectedScreen;
        s = ctrl.BtqCreativityStatusString;
        s = ctrl.BtqQuantityStatusString;
        s = ctrl.selectedView;
        
        List<SelectOption> options = ctrl.viewOptions;
        options = ctrl.Items;
        options = ctrl.screenOptions;
        
        ctrl.getController();
        
        Id i = ctrl.feedId;
        
        Map<Id, Boutique_Order_Item__c> BtqOrderItemsOrderedByBtqSpacesId = ctrl.BtqOrderItemsOrderedByBtqSpacesId;
        
        
        ctrl.selectedView= ctrl.VIEW_MPM;
        ctrl.saveCreativityProposal();
        
        PageReference pgRef = Page.ERTAILOrdersDetail;
        pgRef.getParameters().put('btqOrderItemId', boi.Id);
        pgRef.getParameters().put('value', 'Approve');
        Test.setCurrentPage(pgRef);
        ctrl.ApproveMPMCreativityProposal();        

        pgRef.getParameters().put('fromStep', 'step1');
        pgRef.getParameters().put('toStep', 'step2');
        pgRef.getParameters().put('value', 'Reject');
        pgRef.getParameters().put('reason', 'Reject');
        pgRef.getParameters().put('quantity', '5');
        ctrl.ApproveMPMCreativityProposal();        
        
        Map<Id, Map<Id, Boutique_Order_Item__c>> BoiWithQuantityOrderedByBtqIdThenCampaignItemId = ctrl.BoiWithQuantityOrderedByBtqIdThenCampaignItemId;
        
        Map<Id, List<Campaign_Item__c>> CampaignItemsWithQuantityOrderedBySpaceId = ctrl.CampaignItemsWithQuantityOrderedBySpaceId;
        
        ctrl.ApproveMPMQuantityProposal();
        
        Map<Id, Map<Id, Boutique_Order_Item__c>> BtqOrderItemsOrderedByBtqIdThenFfId = ctrl.BtqOrderItemsOrderedByBtqIdThenFfId; 
        
        Map<Id, Boutique_Space__c> btqSpacesForMarketMap = ctrl.btqSpacesForMarketMap;
        
        ctrl.selectedView= ctrl.VIEW_HQPM;
        
        ctrl.saveCreativityProposal();
        
        System.assertEquals(ctrl.BoiWithQuantityOrderedByBtqIdThenCampaignItemId.size() > 0, true);

        s = ctrl.completedBoutiques;

        ctrl.saveQuantitycIProposal();
        
        Map<Id, Boolean> CampaignItemsInUse = ctrl.CampaignItemsInUse;
        
        Map<Id, Boolean> SpacesInUse = ctrl.SpacesInUse;
        
        ctrl.saveQuantityFFProposal();
        
        ctrl.generateRecommandations();
        
        ctrl.LockOrderItems();
        
        ctrl.LockOrderQuantity();  
    }
    
    static testMethod void myUnitTest4() {
        Id cmpId = ERTAILTestHelper.createCampaignMME();
        Campaign__c cmp = [select Id, Name from Campaign__c where Id =: cmpId];

        Boutique_Order_Item__c boi = [select Id, Campaign_item_To_ORder__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and boutique_Space__r.Space__r.has_Creativity__c = true][0];
        Boutique_Order_Item__c boi2 = [select Id, Campaign_item_To_ORder__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and boutique_Space__r.Space__r.has_Creativity__c = false][0];
        Boutique_Order_Item__c boi3 = [select Id, Permanent_Fixture_And_Furniture__r.Id from Boutique_Order_Item__c where Campaign__r.Id =:cmpId and Campaign_item_To_Order__c = null][0];

        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);

        ERTAILMasterController ctrl = new ERTAILMasterController(stdController);
        
        Map<Id, Boutique_Order_Item__c> BtqOrderItemsOrderedByBtqSpacesId = ctrl.BtqOrderItemsOrderedByBtqSpacesId;
        
        ctrl.selectedView= ctrl.VIEW_MPM;
        ctrl.saveCreativityProposal();
        
        PageReference pgRef = Page.ERTAILOrdersDetail;
        pgRef.getParameters().put('btqOrderItemId', boi.Id);
        pgRef.getParameters().put('value', 'Approve');
        pgRef.getParameters().put('fromStep', 'step1');
        pgRef.getParameters().put('toStep', 'step2');
        pgRef.getParameters().put('value', 'Reject');
        pgRef.getParameters().put('reason', 'Reject');
        pgRef.getParameters().put('quantity', '5');
        Test.setCurrentPage(pgRef);
        
        ctrl.selectedView= ctrl.VIEW_HQPM;
        
        ctrl.saveCreativityProposal();
        
        System.assertEquals(ctrl.BoiWithQuantityOrderedByBtqIdThenCampaignItemId.size() > 0, true);

        ctrl.saveQuantitycIProposal();
        
        ctrl.MarkHQPMCreativityProposalAsCompleted();
        
        ctrl.MarkHQPMQuantityProposalAsCompleted();
        
        ctrl.MarkMPMCreativityProposalAsCompleted();
        
        ctrl.MarkMPMQuantityProposalAsCompleted();
        
        ctrl.ApproveMPMCreativityProposals();
        
    }
    
    static testMethod void myUnitTest5() {
        Id cmpId = ERTAILTestHelper.createCampaignMME();

        Campaign__c cmp = [select Id, Name from Campaign__c where Id =: cmpId];

        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);
        ERTAILMasterController ctrl = new ERTAILMasterController(stdController);
        
        Campaign_Item__c ci = [select Id from Campaign_Item__c][0];

        PageReference pgRef = Page.ERTAILOrdersDetail;
        pgRef.getParameters().put('value', 'Approve');
        pgRef.getParameters().put('fromStep', 'step1');
        pgRef.getParameters().put('toStep', 'step2');
        pgRef.getParameters().put('value', 'Reject');
        pgRef.getParameters().put('reason', 'Reject');
        pgRef.getParameters().put('quantity', '5');
        pgRef.getParameters().put('replaceFrom', ci.Id);
        pgRef.getParameters().put('replaceTo', ci.Id);
        Test.setCurrentPage(pgRef);
                
        
        ctrl.selectedView= ctrl.VIEW_HQPM;
    
        ctrl.RejectMPMCreativityProposals();
                
        ctrl.ApproveMPMQuantityProposals();
        
        ctrl.RejectMPMQuantityProposals();
        
        ctrl.MarkMPMQuantityProposalAsCompleted();
        
        ctrl.MarkMPMZoneQuantityProposalAsCompleted();
        
        ctrl.MarkMPMCreativityProposalAsCompleted();
        
        ctrl.replaceCreativities();
                    
        Map<Id, ERTAILMasterController.ConflictWrapper> btqSpacesConflictsMap = ctrl.btqSpacesConflictsMap;     
    }    
        
    static testMethod void myUnitTest6() {
        Id cmpId = ERTAILTestHelper.createCampaignMME();

        Campaign__c cmp = [select Id, Name from Campaign__c where Id =: cmpId];

        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);
        ERTAILMasterController ctrl = new ERTAILMasterController(stdController);
        ctrl.Beginning();
        ctrl.Previous();
        ctrl.Next();
        ctrl.End();
        ctrl.getDisablePrevious();
        ctrl.getDisableNext();
        ctrl.voidmethod();
        ctrl.createBoutiqueOrderItem();
        Campaign_Item__c ci = [select Id from Campaign_Item__c][0];

        PageReference pgRef = Page.ERTAILOrdersDetail;
        pgRef.getParameters().put('value', 'Approve');
        pgRef.getParameters().put('fromStep', 'step1');
        pgRef.getParameters().put('toStep', 'step2');
        pgRef.getParameters().put('value', 'Reject');
        pgRef.getParameters().put('reason', 'Reject');
        pgRef.getParameters().put('quantity', '5');
        pgRef.getParameters().put('replaceFrom', ci.Id);
        pgRef.getParameters().put('replaceTo', ci.Id);
        Test.setCurrentPage(pgRef);
                
        String s = ctrl.VIEW_FINAL + ctrl.SCREEN_COSTS + ctrl.SCREEN_DELIVERY + ctrl.SCREEN_INSTALLATION + ctrl.SCREEN_AGENCYFEES + ctrl.SCREEN_VISUALS;

        Boolean b = ctrl.isProductionAgency;
        b = ctrl.isCreativeAgency;

        List<Id> CampaignItemIdsWithCreativity = ctrl.CampaignItemIdsWithCreativity;

        Map<String,List<Campaign_Item__c>> mapInStorePerCategory = ctrl.mapInStorePerCategory;

        s = ctrl.CampaignItemsPerSpaceJSON + ctrl.CampaignItemsJSON;

        Map<Id, Integer> quantitiesToOrder = ctrl.quantitiesToOrder;

        Map<Id, Permanent_Fixture_And_Furniture__c> PermanentFixturesAndFurnituresMap = ctrl.PermanentFixturesAndFurnituresMap;
      
    } 
    /*
     * @Author:Mansimar
     * @Description: Added test method for Boutique Wrapper and 
     * */
     static testMethod void myUnitTest7() {
        Id cmpId = ERTAILTestHelper.createCampaignMME();

        Campaign__c cmp = [select Id, Name from Campaign__c where Id =: cmpId]; 
         
        ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);
        ERTAILMasterController ctrl = new ERTAILMasterController(stdController);
        
        Campaign_Item__c ci = [select Id from Campaign_Item__c][0];
        Campaign_Boutique__c cmpBout= [select Id, RTCOMBoutique__c,Campaign_Market__c,Creativity_Step__c,Quantity_Step__c,Creativity_Step_Value__c,Quantity_Step_Value__c  from Campaign_Boutique__c][0];
         RTCOMBoutique__c RBot=[select Id from  RTCOMBoutique__c][0];
         Id bid = Rbot.id;
        cmpBout.RTCOMBoutique__r=RBot;
         PageReference pgRef = Page.ERTAILOrdersDetail;
        pgRef.getParameters().put('value', 'Approve');
        pgRef.getParameters().put('fromStep', 'step1');
         //NOT REQUIRED BELOW PARAMS
      /*  pgRef.getParameters().put('toStep', 'step2');
        pgRef.getParameters().put('value', 'Reject');
        pgRef.getParameters().put('reason', 'Reject');
        pgRef.getParameters().put('quantity', '5');
        pgRef.getParameters().put('replaceFrom', ci.Id);
        pgRef.getParameters().put('replaceTo', ci.Id);
       */ 
        Test.setCurrentPage(pgRef);
  
        //Mansimar - added to cover changeBoutiqueCreativityStep() method 
        pgRef.getParameters().put('toStep', '');
         pgRef.getParameters().put('boutiqueId', bId);
        pgRef.getParameters().put('selectedView', ctrl.VIEW_HQPM); 
        ctrl.changeBoutiqueCreativityStep();
        ctrl.changeBoutiqueQuantityStep();
        ctrl.RejectboutiqueId=bID;
        ctrl.RejectMPMCreativityProposals1();
        ctrl.RejectMPMQuantityProposals1();
        //ctrl.LockOrderQuantity(); 
        ERTAILMasterController dummy = new ERTAILMasterController();
         
        ctrl.selectedView= ctrl.VIEW_HQPM; 
        //For HQPM 
        //cmpBout.Creativity_Step__c='HQPM Recommendation';
        //cmpBout.Quantity_Step_Value__c=10;
        //update cmpBout; 
        
        //BOUTIQUE WRAPPER  
        ERTAILMasterController.BoutiqueWrapper bwrap= new ERTAILMasterController.BoutiqueWrapper(cmpBout,ctrl.VIEW_HQPM); 
        
        Boolean b =bwrap.isMPMCreativityProposal;
         b =bwrap.isMPMZoneCreativityProposal;
         b =bwrap.isHQPMCreativityProposal;
         b =bwrap.isMPMCreativityApproval;
         b =bwrap.isMPMQuantityProposal;
         b =bwrap.isMPMZoneQuantityProposal;
         b =bwrap.isHQPMQuantityProposal;
         b =bwrap.isMPMQuantityApproval;
         String s =bwrap.ScopeLimitationShort; 
         s=bwrap.creativityStepLabel;
         s=bwrap.quantityStepLabel;
         
        //QuantityChangeResult class
        ERTAILMasterController.QuantityChangeResult qr = new ERTAILMasterController.QuantityChangeResult();
        //qr.QuantityChangeResult(); 
         
        List<SelectOption> selectOptions =bwrap.selectCreativityOptions;
        List<SelectOption> selectQuanOptions =bwrap.selectQuantityOptions;
        List<Id> idd=bwrap.getBtqSpacesWithCreativity();
        idd=bwrap.getCampaignItemsWithQuantity(); 
        Map<Id, List<Id>> mapId =bwrap.getBsPerSpaceMap(); 
        
         //NEW CONTROLLER TYPE --->Campaign market in controller record 
        Campaign_Market__c cmarket =[select Id,RTCOMMarket__C,Campaign__c from Campaign_Market__c ][0];
         ApexPages.StandardController stdControllerMarket = new ApexPages.StandardController(cmarket); 
        ERTAILMasterController ctrlMarket = new ERTAILMasterController(stdControllerMarket); 
         
         String sctrl = ctrlMarket.quantityResultJSON;
         sctrl=ctrlMarket.quantityApprovalResultJSON;
         sctrl= ctrlMarket.BF;
         sctrl= ctrlMarket.creativityResultJSON;
         sctrl= ctrlMarket.creativityApprovalResultJSON;
         
         List<Id> ob =ctrlMarket.OrderedBoutiques;
     }
    
    
}
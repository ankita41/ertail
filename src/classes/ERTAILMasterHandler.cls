/*****
*   @Class      :   ERTAILMasterHandler.cls
*   @Description:   Static methods used accross ERTAIL
*   @Author     :   Thibauld
*   @Created    :   XXX
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/
/*************
* PLEASE ONLY ADD Methods or Lazy loading properties in this class
************************************/
public virtual with sharing class ERTAILMasterHandler {
    public Campaign__c campaign { get; set;}
    
    public Boolean IsHQPMorAdmin { get { return IsHQPM || isAdmin;} }
    public Boolean IsMPMorAdmin { get { return IsMPM || isAdmin;} }
    public Boolean IsHQPM { get { return ERTailMasterHelper.isHQPM;} }
    public Boolean IsMPM { get { return ERTailMasterHelper.isMPM;} }
    public Boolean IsAgent { get { return ERTailMasterHelper.isAgent;} }
    
    public Boolean isProductionAgency { get{ return ERTailMasterHelper.isProductionAgency; } }
    public Boolean isCreativeAgency { get{ return ERTailMasterHelper.isCreativeAgency; } }
    public Boolean isAdmin { get{ return ERTailMasterHelper.isAdmin; } }

    public String VIEW_MPM { get { return ERTailMasterHelper.VIEW_MPM;} }
    public String VIEW_HQPM { get { return ERTailMasterHelper.VIEW_HQPM;} }
    public String VIEW_STRUCTURE { get { return ERTailMasterHelper.VIEW_STRUCTURE;} }
    public String VIEW_FINAL { get { return ERTailMasterHelper.VIEW_FINAL;} }
    
    public String SCREEN_INSTORE { get { return ERTailMasterHelper.SCREEN_INSTORE;} }
    public String SCREEN_SHOWWINDOW { get { return ERTailMasterHelper.SCREEN_SHOWWINDOW;} }
    public String SCREEN_FIXTURE { get { return ERTailMasterHelper.SCREEN_FIXTURE;} }
    public String SCREEN_VISUALS { get { return ERTailMasterHelper.SCREEN_VISUALS;} }
    public String SCREEN_COSTS { get { return ERTailMasterHelper.SCREEN_COSTS;} }
    public String SCREEN_DELIVERY { get { return ERTailMasterHelper.SCREEN_DELIVERY;} }
    public String SCREEN_INSTALLATION { get { return ERTailMasterHelper.SCREEN_INSTALLATION;} }
    public String SCREEN_AGENCYFEES { get { return ERTailMasterHelper.SCREEN_AGENCYFEES;} }


/* Spaces */
    public List<Space__c> SpacesWithQuantityOrderedByName(){
        return [Select Category__c
                        , Name
                        , Id
                        , Decoration_Alias__c 
                From Space__c c
                where Has_Quantity__c = true
                order by Name];
    }
    
    public static Map<Id, Space__c> AllSpacesMap{
        get{
            if (AllSpacesMap == null){
                AllSpacesMap = new Map<Id, Space__c>([Select Category__c
                                                            , Name
                                                            , Id
                                                            , Decoration_Alias__c
                                                            , Has_Creativity__c
                                                            , Has_Quantity__c
                                                            , Image_Id__c
                                                    From Space__c c
                                                    order by Name]);
            }
            return AllSpacesMap;
        }
        private set; 
    }


/* Boutiques Spaces */
    public static List<Boutique_Space__c> BoutiqueSpacesForRTCOMBtqIdOrderedByRTCOMBtqNameThenCreativityOrder(Id RTCOMbtqId){
        return [Select b.Space__r.Decoration_Alias__c
                        , b.Decoration_Allowed__c
                        , b.Space__c
                        , b.Space_Quantity__c
                        , b.Comment__c
                        , b.Creativity_Order__c
                        , b.RTCOMBoutique__r.RTCOMMarket__c
                        , b.RTCOMBoutique__r.Name
                        , b.RTCOMBoutique__r.Id
                        , b.RTCOMBoutique__c 
                From Boutique_Space__c b 
                where b.RTCOMBoutique__c = :RTCOMbtqId 
                    and b.Space__r.RecordType.Name = 'Show_Window' 
                order by b.RTCOMBoutique__r.Name
                    , b.Creativity_Order__c];
    }
    
    public List<Boutique_Space__c> BoutiqueSpacesForRTCOMBtqIdsOrderedByRTCOMBtqNameThenSpaceThenCreativity(List<Id> btqIds){
        return [Select b.Space__r.Decoration_Alias__c
                        , b.Quantity_to_Order__c
                        , b.Decoration_Allowed__c
                        , b.Width__c
                        , b.Height__c
                        , b.Depth__c
                        , b.Space__r.Image_Id__c
                        , b.Space__r.Name
                        , b.Space__r.Id
                        , b.Space_Quantity__c
                        , b.Image_Id__c
                        , b.Space__c
                        , b.Comment__c
                        , b.Name
                        , b.Creativity_Order__c
                        , b.RTCOMBoutique__r.RTCOMMarket__c
                        , b.RTCOMBoutique__r.Name
                        , b.RTCOMBoutique__r.Id
                        , b.RTCOMBoutique__c 
                From Boutique_Space__c b 
                where b.RTCOMBoutique__c in :btqIds 
                order by b.RTCOMBoutique__r.Name, b.Creativity_Order__c, b.Space__r.Name];
    }
    
/* Campaign Items */
    public Map<Id, Campaign_Item__c> CampaignItemsGroupedById { get { return new Map<Id, Campaign_Item__c>(CampaignItems); } }

    public List<Campaign_Item__c> CampaignItems{
        get{
            if (CampaignItems == null){
                Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Campaign_Item__c').getDescribe().fields.getMap();
                //String qryStr = 'SELECT Space__r.Name, Space__r.Has_Creativity__c, Space__r.Has_Quantity__c, Space__r.Decoration_Alias__c, Space__r.Category__c, (Select Id, Quantity_to_Order__c From Boutique_Order_Items3__r), ';
              //  String qryStr = 'SELECT (Select Id, Quantity_to_Order__c From Boutique_Order_Items3__r),(Select Id, CampaignItem__c,Image_ID__c,Image_medium__c,name,Excluded_for_campaign__c From Creativity_for_Instore_items__r order by name), ';                
               String qryStr = 'SELECT (Select Id, Quantity_to_Order__c From Boutique_Order_Items3__r), ';                
               
                for(String s : fieldsMap.keySet()){
                    if(fieldsMap.get(s).getDescribe().isAccessible())
                        qryStr = qryStr + s + ', ';
                }
                qryStr = qryStr.removeEnd(', ') + ' FROM Campaign_Item__c WHERE Campaign__c = \'' + campaign.Id + '\' ';
                qryStr = qryStr + 'ORDER BY Space__r.Category__c, Creativity__c, Alias__c, Name ';
                
                CampaignItems = Database.query(qryStr);
            }
            return CampaignItems;
        }
        set;
    }

    public List<Campaign_Item__c> CampaignItemsWithCreativity{
        get{
            if (campaignItemsWithCreativity == null){
                campaignItemsWithCreativity = new List<Campaign_Item__c>();
                // T1, T2, ... first
                // Then Key1, Key2, ...
                // Then the other ones
                Integer tIndex = 0;
                Integer kIndex = 0;
                for (Campaign_Item__c ci : CampaignItems){
                    Space__c space = AllSpacesMap.get(ci.Space__c);
                    if (space!= null && space.Has_Creativity__c){
                        if(campaignItemsWithCreativity.size() == 0){
                            campaignItemsWithCreativity.add(ci);
                        }else{
                            if(ERTAILMasterHelper.DECORATION_ALIAS_STANDARDSHOWWINDOW.equals(space.Decoration_Alias__c)){
                                if (tIndex < campaignItemsWithCreativity.size())
                                    campaignItemsWithCreativity.add(tIndex, ci);
                                else
                                    campaignItemsWithCreativity.add(ci);
                            }
                            else if(ERTAILMasterHelper.DECORATION_ALIAS_KEY.equals(space.Decoration_Alias__c)){
                                if (tIndex + kIndex < campaignItemsWithCreativity.size())
                                    campaignItemsWithCreativity.add(tIndex + kIndex, ci);
                                else
                                    campaignItemsWithCreativity.add(ci);
                            }
                            else{
                                campaignItemsWithCreativity.add(ci);
                            }
                        }
                        
                        if(ERTAILMasterHelper.DECORATION_ALIAS_STANDARDSHOWWINDOW.equals(space.Decoration_Alias__c)){
                            tIndex++;
                        } else if(ERTAILMasterHelper.DECORATION_ALIAS_KEY.equals(space.Decoration_Alias__c)){
                            kIndex++;
                        } 
                        
                    }
                }
            }
            return campaignItemsWithCreativity;
        }
        set;
    }
    
    public List<Id> CampaignItemIdsWithCreativity{
        get{
            if (CampaignItemIdsWithCreativity == null){
                List<Id> ciList = new List<Id>();
                // T1, T2, ... first
                // Then Key1, Key2, ...
                // Then the other ones
                Integer tIndex = 0;
                Integer kIndex = 0;
                for (Campaign_Item__c ci : CampaignItems){
                    Space__c space = AllSpacesMap.get(ci.Space__c);
                    if (space.Has_Creativity__c){
                        if(ciList.size() == 0){
                            ciList.add(ci.Id);
                        }else{
                            if(ERTAILMasterHelper.DECORATION_ALIAS_STANDARDSHOWWINDOW.equals(space.Decoration_Alias__c)){
                                if (tIndex < ciList.size())
                                    ciList.add(tIndex, ci.Id);
                                else
                                    ciList.add(ci.Id);
                            }
                            else if(ERTAILMasterHelper.DECORATION_ALIAS_KEY.equals(space.Decoration_Alias__c)){
                                if (tIndex + kIndex < ciList.size())
                                    ciList.add(tIndex + kIndex, ci.Id);
                                else
                                    ciList.add(ci.Id);
                            }
                            else{
                                ciList.add(ci.Id);
                            }
                        }
                        
                        if(ERTAILMasterHelper.DECORATION_ALIAS_STANDARDSHOWWINDOW.equals(space.Decoration_Alias__c)){
                            tIndex++;
                        } else if(ERTAILMasterHelper.DECORATION_ALIAS_KEY.equals(space.Decoration_Alias__c)){
                            kIndex++;
                        } 
                        
                    }
                }
                campaignItemIdsWithCreativity = ciList;
            }
            return campaignItemIdsWithCreativity;
        }
        set;
    }
        
    public List<Campaign_Item__c> CampaignItemsWithOutCreativity{
        get{
            if (campaignItemsWithOutCreativity == null){
                campaignItemsWithOutCreativity = new List<Campaign_Item__c>();
                for (Campaign_Item__c ci : CampaignItems)
                    if (AllSpacesMap.containsKey(ci.Space__c)){   
                        Space__c space = AllSpacesMap.get(ci.Space__c);
                        if (!space.Has_Creativity__c)
                            campaignItemsWithOutCreativity.add(ci);
                    }
            }
            return campaignItemsWithOutCreativity;
        }
        set;
    } 
    
    public Map<String,List<Campaign_Item__c>> mapInStorePerCategory{
        get{
            if(mapInStorePerCategory == null){
                mapInStorePerCategory = new Map<String,List<Campaign_Item__c>>();
                for(Campaign_Item__c ci : CampaignItemsWithOutCreativity){
                    if (AllSpacesMap.containsKey(ci.Space__c)){   
                        Space__c space = AllSpacesMap.get(ci.Space__c);
                        space.Category__c = (space.Category__c!=null) ? space.Category__c : 'Uncategorized';
                        if(mapInStorePerCategory.containsKey(space.Category__c)){
                            mapInStorePerCategory.get(space.Category__c).add(ci);
                        }
                        else{
                            mapInStorePerCategory.put(space.Category__c, new List<Campaign_Item__c>());
                            mapInStorePerCategory.get(space.Category__c).add(ci);
                        }
                    }
                }
            }
            return mapInStorePerCategory;
        }
        set;
    }
    
    private List<Campaign_Item__c> campaignItemsReduced{
        get{
            if (campaignItemsReduced == null){
                campaignItemsReduced = new List<Campaign_Item__c>();
                for (Campaign_Item__c ci : campaignItemsWithCreativity)
                    campaignItemsReduced.add(new Campaign_Item__c(Id=ci.Id, Name=ci.Name, Alias__c = ci.Alias__c, Space__c = ci.Space__c, Creativity_Color__c = ci.Creativity_Color__c));
            }
            return campaignItemsReduced;
        }
        set;
    }

    public String CampaignItemsPerSpaceJSON{
        get{
            Map<Id, List<Campaign_Item__c>> ciOrderedBySpace = new Map<Id, List<Campaign_Item__c>>();
            for (Campaign_Item__c ci : campaignItemsReduced){
                if (ciOrderedBySpace.containsKey(ci.Space__c))
                    ciOrderedBySpace.get(ci.Space__c).add(ci);
                else
                    ciOrderedBySpace.put(ci.Space__c, new List<Campaign_Item__c>{ci});
            }
            return JSON.serializePretty(ciOrderedBySpace);
        }
    }
    
    public String CampaignItemsJSON{
        get{
            return JSON.serializePretty(campaignItemsReduced);
        }
    }
    
    public Map<Id, Integer> quantitiesToOrder{
        get{
            if (quantitiesToOrder == null){
                quantitiesToOrder = new Map<Id, Integer>();
                for (Campaign_Item__c ci : CampaignItems){
                    if (ci.Boutique_Order_Items3__r.isEmpty()){
                        quantitiesToOrder.put(ci.Id, 0);    
                    }else{
                        Integer qty = 0;
                        for (Boutique_Order_Item__c boi : ci.Boutique_Order_Items3__r){
                            qty += boi.Quantity_to_Order__c == null ? 0 : Integer.valueOf(boi.Quantity_to_Order__c);
                        }
                        quantitiesToOrder.put(ci.Id, qty);
                    }
                    
                }
            }
            return quantitiesToOrder;
        }
        private set;
    }

/* Boutique Order Items */
    public static List<Boutique_Order_Item__c> BoisWithCreativityForBoutiqueIds(Set<Id> btqIdSet){
        return [Select b.Name
                    , b.MPM_Comment__c
                    , b.Campaign_Boutique__c
                    , b.Campaign_Boutique__r.RTCOMBoutique__c
                    , b.Campaign_Boutique__r.Creativity_Step__c
                    , b.RecordType.Name
                    , b.RecordType.DeveloperName
                    , b.HQPM_Decision__c
                    , b.HQPM_Decision_Comment__c
                    , b.Creativity_Step_Picklist__c
                    , b.Creativity_Step__c
                    , b.Recommended_Campaign_Item__c
                    , b.Recommended_Campaign_Item__r.Name
                    , b.Recommended_Campaign_Item__r.Alias__c
                    , b.MPM_Campaign_Item__c
                    , b.MPM_Campaign_Item__r.Name
                    , b.MPM_Campaign_Item__r.Alias__c
                    , b.MPM_Campaign_Item__r.Image_Id__c
                    , b.MPM_Campaign_Item__r.Creativity__c
                    , b.MPM_Campaign_Item__r.Space__r.Decoration_Alias__c
                    , b.MPM_Campaign_Item__r.Creativity_Color__c
                    , b.MPM_Campaign_Item__r.Visual_Step_Value__c
                    , b.MPM_Campaign_Item__r.Visual_Step__c
                    , b.HQPM_Campaign_Item__c
                    , b.HQPM_Campaign_Item__r.Name
                    , b.HQPM_Campaign_Item__r.Alias__c
                    , b.HQPM_Campaign_Item__r.Image_Id__c
                    , b.HQPM_Campaign_Item__r.Creativity__c
                    , b.HQPM_Campaign_Item__r.Space__r.Decoration_Alias__c
                    , b.HQPM_Campaign_Item__r.Creativity_Color__c
                    , b.HQPM_Campaign_Item__r.Visual_Step_Value__c
                    , b.HQPM_Campaign_Item__r.Visual_Step__c
                    , b.Campaign_Item_to_order__c
                    , b.Campaign_Item_to_order__r.Name
                    , b.Campaign_Item_to_order__r.Alias__c
                    , b.Campaign_Item_to_order__r.Image_Id__c
                    , b.Campaign_Item_to_order__r.Creativity__c
                    , b.Campaign_Item_to_order__r.Space__r.Decoration_Alias__c
                    , b.Campaign_Item_to_order__r.Creativity_Color__c
                    , b.Campaign_Item_to_order__r.Visual_Step_Value__c
                    , b.Campaign_Item_to_order__r.Visual_Step__c
                    , b.Boutique_Space__c 
                    , b.Boutique_Space__r.Space__c 
                    , b.Has_Creativity__c
                From Boutique_Order_Item__c b 
                where b.Campaign_Boutique__c in :btqIdSet
                    and b.Has_Creativity__c = 'Yes'
                order by b.Name];
    }
    
    public static Map<Id, Boutique_Order_Item__c> BoisWithQuantityForCampaignBtqIdGroupedByCampaignItemId(Id cbId){
        Map<Id, Boutique_Order_Item__c> boiMap = new Map<Id, Boutique_Order_Item__c>();
        for (Boutique_Order_Item__c boi : BoisWithQuantityForCampaignBtqIds(new Set<Id>{cbId})){
            boiMap.put(boi.Campaign_Item_to_order__c, boi);
        }
        return boiMap;
    }
    
    public static List<Boutique_Order_Item__c> BoisWithQuantityForCampaignBtqIds(Set<Id> btqIdSet){
        return [Select b.Name
                    , b.MPM_Comment__c
                    , b.Campaign_Boutique__c
                    , b.Campaign_Boutique__r.RTCOMBoutique__c
                    , b.Campaign_Boutique__r.Quantity_Step__c
                    , b.RecordType.Name
                    , b.RecordType.DeveloperName
                    , b.HQPM_Decision__c
                    , b.HQPM_Decision_Comment__c
                    , b.Quantity_Step_Picklist__c
                    , b.Quantity_Step__c
                    /*, b.Max_Cost_Eur__c*/
                    /*, b.Actual_Cost_Eur__c*/
                    , b.Recommended_Quantity__c
                    , b.MPM_Quantity__c
                    , b.HQPM_Quantity__c
                    , b.Quantity_to_order__c
                    , b.Campaign_Item_to_order__c
                    , b.Campaign_Item_to_order__r.Name
                    , b.Campaign_Item_to_order__r.Alias__c
                    , b.Campaign_Item_to_order__r.Image_Id__c
                    , b.Campaign_Item_to_order__r.Visual_Step_Value__c
                    , b.Campaign_Item_to_order__r.Visual_Step__c

                    , b.Has_Quantity__c
                From Boutique_Order_Item__c b 
                where b.Campaign_Boutique__c in :btqIdSet
                    and b.Has_Quantity__c = 'Yes'
                order by b.Name];
    }
    
    public static Map<Id, Boutique_Order_Item__c> BoisWithFixtureForCampaignBtqIdGroupedByFixtureId(Id cbId){
        Map<Id, Boutique_Order_Item__c> boiMap = new Map<Id, Boutique_Order_Item__c>();
        for (Boutique_Order_Item__c boi : BoisWithFixtureForBoutiqueIds(new Set<Id>{cbId})){
            boiMap.put(boi.Permanent_Fixture_And_Furniture__c, boi);
        }
        return boiMap;
    }
    
    public static List<Boutique_Order_Item__c> BoisWithFixtureForBoutiqueIds(Set<Id> btqIdSet){
        return [Select b.Name
                    , b.Campaign_Boutique__c
                    , b.Campaign_Boutique__r.RTCOMBoutique__c
                    , b.RecordType.Name
                    , b.RecordType.DeveloperName
                    , b.Quantity_to_order__c
                    , b.Permanent_Fixture_And_Furniture__c
                    , b.Permanent_Fixture_And_Furniture__r.Image_Id__c
                    , b.Permanent_Fixture_And_Furniture__r.Name 
                From Boutique_Order_Item__c b 
                where b.Campaign_Boutique__c in :btqIdSet
                    and b.Permanent_Fixture_And_Furniture__c != null
                order by b.Name];
    }
    
/* Permanent Fixture & Furnitures */    
    public List<Permanent_Fixture_And_Furniture__c> PermanentFixturesAndFurnitures{
        get{
            if (PermanentFixturesAndFurnitures == null)
                PermanentFixturesAndFurnitures = [Select p.Name, p.Image_Id__c, p.Description__c From Permanent_Fixture_And_Furniture__c p order by Name];
            return PermanentFixturesAndFurnitures;
        }
        private set;
    }   
    
    public Map<Id, Permanent_Fixture_And_Furniture__c> PermanentFixturesAndFurnituresMap{
        get{
            if (PermanentFixturesAndFurnituresMap == null){
                PermanentFixturesAndFurnituresMap = new Map<Id, Permanent_Fixture_And_Furniture__c>(PermanentFixturesAndFurnitures);
            }
            return PermanentFixturesAndFurnituresMap;
        }
        private set;
    } 
    
    public List<Campaign_Agency__c> creaAgency {        
        get{
            if(creaAgency==null){
                creaAgency = [SELECT Id FROM Campaign_Agency__c WHERE Campaign__c = :campaign.Id AND RecordType.Name = 'Creative Agency'];
            }
            return creaAgency;
        } set;
    }
    
    public List<Campaign_Agency__c> prodAgency {        
        get{
            if(prodAgency==null){
                prodAgency = [SELECT Id FROM Campaign_Agency__c WHERE Campaign__c = :campaign.Id AND RecordType.Name = 'Production Agency'];
            }
            return prodAgency;
        } set;
    }
    
    public List<ERTAILCampaign_Agency_Fee__c> campAgencyFees {
        get{
            if(campAgencyFees==null){
                Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('ERTAILCampaign_Agency_Fee__c').getDescribe().fields.getMap();
                String qryStr = 'SELECT Campaign__r.Agency_Fee_Step_Value__c, ';
                for(String s : fieldsMap.keySet()){
                    if(fieldsMap.get(s).getDescribe().isAccessible())
                        qryStr = qryStr + s + ', ';
                }
                qryStr = qryStr.removeEnd(', ') + ' FROM ERTAILCampaign_Agency_Fee__c WHERE Campaign__c = \'' + campaign.Id + '\' ';
                qryStr = qryStr + 'AND Status__c = \'Active\' ORDER BY Name ';
                
                campAgencyFees = Database.query(qryStr);
            }
            return campAgencyFees;
        }
        set;
    }
}
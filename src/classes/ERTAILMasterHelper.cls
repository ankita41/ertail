/*****
*   @Class      :   ERTAILMasterHelper.cls
*   @Description:   Static methods used accross ERTAIL
*   @Author     :   Thibauld
*   @Created    :   XXX
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/
public with sharing class ERTAILMasterHelper {
    public static final String SYS_ADM = 'System Admin';
    public static final String ERT_MPM = 'ERTAIL MPM';
    public static final String ERT_HQPM = 'ERTAIL HQPM';
    public static final String ERT_AGENT = 'ERTAIL Agent';
    public static final String ERT_CREA = 'ERTAIL Creative Agency';
    public static final String ERT_PROD = 'ERTAIL Production Agency';
    
    public static final String VIEW_MPM = 'MPM_Prop';
    public static final String VIEW_HQPM = 'HQPM_Prop';
    public static final String VIEW_STRUCTURE = 'Spaces';
    public static final String VIEW_FINAL = 'Final';
    
    public static final String SCREEN_INSTORE = 'Quantity';
    public static final String SCREEN_SHOWWINDOW = 'Creativity';
    public static final String SCREEN_FIXTURE = 'Fixture';
    public static final String SCREEN_VISUALS = 'Visuals';
    public static final String SCREEN_COSTS = 'Costs';
    public static final String SCREEN_DELIVERY = 'Delivery';
    public static final String SCREEN_INSTALLATION = 'Installation';
    public static final String SCREEN_AGENCYFEES = 'Agency Fees';
    
    public static String SCOPE_COMPLETE = 'Complete';
    public static String SCOPE_SHOWWINDOW = 'Show Window only';
    public static String SCOPE_INSTORE = 'In-Store only';
     public static String SCOPE_INSTOREDISPLAY = 'In-Store Display only';
    
    public static Set<String> TYPOLOGY_KEY = new Set<String>{'Key'};
    public static Set<String> TYPOLOGY_FLAGSHIP = new Set<String>{'Flagship'};
    
    public static String DECORATION_ALIAS_KEY = 'Key'; 
    public static String DECORATION_ALIAS_STANDARDSHOWWINDOW = 'T'; 
        
    private static Profile userProfile = [Select Id, Name from Profile where Id=:UserInfo.getProfileId()];
    
    public static Boolean isAdmin{
        get{ return (userProfile.Name == SYS_ADM); }
    }
    
    public static Boolean isMPM{
        get{ return (userProfile.Name == ERT_MPM); }
    }
    
    public static Boolean isAgent{
        get{ return (userProfile.Name == ERT_AGENT); }
    }
    
    public static Boolean isHQPM{
        get { return (userProfile.Name == ERT_HQPM); }
    }
    
    public static Boolean isCreativeAgency{
        get { return (userProfile.Name == ERT_CREA); }
    }
    
    public static Boolean isProductionAgency{
        get { return (userProfile.Name == ERT_PROD); }
    }
    
    public static Boolean isMPMorAdmin{
        get{ return (isMPM || isAdmin); }
    }
    
    public static Boolean isHQPMorAdmin{
        get { return (isHQPM || isAdmin); }
    }
    
    public static Boolean isProdorAdmin{
        get{ return (isProductionAgency || isAdmin); }
    }
    
    public static Boolean isCreaorAdmin{
        get { return (isCreativeAgency || isAdmin); }
    }
    
    public static Map<String, String> creativityStepFlow = new Map<String, String>{ '10' => '11' //HQPM Proposal --> HQPM Proposal Completed
                                                                    , '11' => '10' //HQPM Proposal Completed --> HQPM Proposal
                                                                    , '20' => '21' //MPM Proposal --> MPM Proposal Completed
                                                                    , '21' => '20' //MPM Proposal Completed --> MPM Proposal
                                                                    };
   
    public static Map<String, String> quantityStepFlow = new Map<String, String>{ '10' => '11' //HQPM Proposal --> HQPM Proposal Completed
                                                                    , '11' => '10' //HQPM Proposal Completed --> HQPM Proposal
                                                                    , '20' => '21' //MPM Proposal --> MPM Proposal Completed
                                                                    , '21' => '20' //MPM Proposal Completed --> MPM Proposal
                                                                    };
    
    public static List<SelectOption> viewOptions{
        get{
            viewOptions = new List<SelectOption>();
            if (isHQPMorAdmin || isMPM || isAgent || isCreativeAgency || isProductionAgency){
              //  viewOptions.add(new SelectOption(VIEW_STRUCTURE, 'Structure'));
                viewOptions.add(new SelectOption(VIEW_HQPM, 'HQPM Recommendations'));
                viewOptions.add(new SelectOption(VIEW_MPM, 'MPM Feedbacks'));
                viewOptions.add(new SelectOption(VIEW_FINAL, 'Final Order'));               
            }
            return viewOptions;
        }
    }
    
    public static List<SelectOption> screenOptions{
        get{
            screenOptions = new List<SelectOption>();
            if (isHQPMorAdmin || isMPM || isAgent || isCreativeAgency || isProductionAgency){
                screenOptions.add(new SelectOption(SCREEN_SHOWWINDOW, 'Show Windows'));
                screenOptions.add(new SelectOption(SCREEN_INSTORE, 'In-Stores'));               
            }           
            if (isHQPMorAdmin || isMPM || isAgent){
                screenOptions.add(new SelectOption(SCREEN_FIXTURE, 'Fixture & Furniture'));
            }
           /* if (isHQPMorAdmin || isCreativeAgency){
                screenOptions.add(new SelectOption(SCREEN_VISUALS, 'Visuals'));
            }*/
            if(isHQPMorAdmin || isProductionAgency || isMPM || isAgent){
                screenOptions.add(new SelectOption(SCREEN_COSTS, 'Production Costs'));
            }
            if(isHQPMorAdmin || isProductionAgency|| isMPM || isAgent){
                screenOptions.add(new SelectOption(SCREEN_DELIVERY, 'Delivery/Pick-up Fees'));
                screenOptions.add(new SelectOption(SCREEN_INSTALLATION, 'Installation Fees'));
            }
            if(isHQPMorAdmin || isProductionAgency){
                screenOptions.add(new SelectOption(SCREEN_AGENCYFEES, 'Agency Fees'));
            }
            return screenOptions;
        }
    }       
    public static Map<Integer,String> quantityBoutiqueStepMap  = new Map<Integer,String>{ 10 => 'HQPM Recommendation'
                                                                            , 11 => 'HQPM Reco. Completed'
                                                                            , 20 => 'MPM Feedback'
                                                                            , 21 => 'MPM Feedback Completed'
                                                                            , 25 => 'MPM Zone Feedback'
                                                                            , 26 => 'MPM Zone Feed. Completed'
                                                                            , 30 => 'MPM Feedback Approval'
                                                                            , 40 => 'HQPM Review Completed'
                                                                            , 41 => 'HQPM Review Completed'
                                                                            , 42 => 'HQPM Review Completed'
                                                                            , 70 => 'Locked'};
    
    public static Map<Integer,String> quantityStepMap  = new Map<Integer,String>{ 10 => 'HQPM Recommendation'
                                                                            , 11 => 'HQPM Reco. Completed'
                                                                            , 20 => 'MPM Feedback'
                                                                            , 21 => 'MPM Feedback Completed'
                                                                            , 25 => 'MPM Zone Feedback'
                                                                            , 26 => 'MPM Zone Feed. Completed'
                                                                            , 30 => 'MPM Feedback Approval'
                                                                            , 40 => 'HQPM Review Completed'
                                                                            , 41 => 'MPM Feedback Approved'
                                                                            , 42 => 'MPM Feedback Rejected'
                                                                            , 70 => 'Locked'};
    
    public static Map<Integer,String> creativityBoutiqueStepMap = new Map<Integer,String>{ 10 => 'HQPM Recommendation'
                                                                            , 11 => 'HQPM Reco. Completed'
                                                                            , 20 => 'MPM Feedback'
                                                                            , 21 => 'MPM Feedback Completed'
                                                                            , 25 => 'MPM Zone Feedback'
                                                                            , 26 => 'MPM Zone Feed. Completed'
                                                                            , 30 => 'MPM Feedback Approval'
                                                                            , 40 => 'HQPM Review Completed'
                                                                            , 41 => 'HQPM Review Completed'
                                                                            , 42 => 'HQPM Review Completed'
                                                                            , 70 => 'Locked'
                                                                            };
                                                                            
    public static Map<Integer,String> creativityStepMap = new Map<Integer,String>{ 10 => 'HQPM Recommendation'
                                                                            , 11 => 'HQPM Reco. Completed'
                                                                            , 20 => 'MPM Feedback'
                                                                            , 21 => 'MPM Feedback Completed'
                                                                            , 25 => 'MPM Zone Feedback'
                                                                            , 26 => 'MPM Zone Feed. Completed'
                                                                            , 30 => 'MPM Feedback Approval'
                                                                            , 40 => 'HQPM Review Completed'
                                                                            , 41 => 'MPM Feedback Approved'
                                                                            , 42 => 'MPM Feedback Rejected'
                                                                            , 70 => 'Locked'
                                                                            };      
                                                                                                                                                
    public static List<Double> HQPMCreativityStatuses = new List<Double>{10, 11, 30, 31};
    public static List<Double> HQPMQuantityStatuses = new List<Double>{10, 11, 30, 31};
    public static List<Double> HQPMVisualStatuses = new List<Double>{20};
    public static List<Double> HQPMInstallationFeeStatuses = new List<Double>{20};
    public static List<Double> HQPMDeliveryFeeStatuses = new List<Double>{20};
    public static List<Double> HQPMAgencyFeeStatuses = new List<Double>{20};
    public static List<Double> HQPMCostStatuses = new List<Double>{20};
    public static String HQPMMarketChangesPendingApproval = 'Pending Approval';
    public static String HQPMBoutiqueChangesPendingApproval = 'Pending Approval';
        
    public static List<Double> AgentCreativityStatuses = new List<Double>{20, 21}; 
    public static List<Double> AgentQuantityStatuses = new List<Double>{20, 21}; 
    public static List<Double> MPMCreativityStatuses = new List<Double>{20, 21, 25, 26}; 
    public static List<Double> MPMQuantityStatuses = new List<Double>{20, 21, 25, 26}; 

    public static List<Double> ProdCostStatuses = new List<Double>{10};
    public static List<Double> ProdInstallationFeeStatuses = new List<Double>{10};
    public static List<Double> ProdDeliveryFeeStatuses = new List<Double>{10};
    public static List<Double> ProdAgencyFeeStatuses = new List<Double>{10};

    public static List<Double> CreaVisualStatuses = new List<Double>{10};


    public static String MPM_PROPOSAL_INHERIT = '--inherit--';
    
    
}
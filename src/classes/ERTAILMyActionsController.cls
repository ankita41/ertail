/*****
*   @Class      :   ERTAILMyActionsController.cls
*   @Description:   Extension developed for ERTAILMyActions.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILMyActionsController{
	public class CustomException extends Exception{}

	public static String MPM_PROPOSAL_INHERIT { get { return ERTAILMasterHelper.MPM_PROPOSAL_INHERIT; } }	

    public static Boolean isAdmin{ get{ return ERTailMasterHelper.isAdmin; } }
    public static Boolean isMPM{ get{ return ERTailMasterHelper.isMPM; } }
    public static Boolean isAgent{ get{ return ERTailMasterHelper.isAgent; } }
    public static Boolean isHQPM{ get{ return ERTailMasterHelper.isHQPM; } }
    public static Boolean isCreativeAgency{ get{ return ERTailMasterHelper.isCreativeAgency; } }
    public static Boolean isProductionAgency{ get{ return ERTailMasterHelper.isProductionAgency; } }
    
    public static String ShowWindowScopeLimitation = ERTAILMasterHelper.SCOPE_SHOWWINDOW;
    public static String InStoreScopeLimitation = ERTAILMasterHelper.SCOPE_INSTORE;

	public static String SCOPE_COMPLETE { get{ return ERTAILMasterHelper.SCOPE_COMPLETE;} }

    public ERTAILMyActionsController() {
    }
    
    public List<Campaign__c> CampaignsWithHQPMRequestedActions_CreativityQuantity{
        get{
            if (CampaignsWithHQPMRequestedActions_CreativityQuantity == null){
                CampaignsWithHQPMRequestedActions_CreativityQuantity = [Select c.Show_Window_Status_Formula__c
                                                        , c.In_Store_Status_Formula__c
                                                        , c.Name
                                                        , c.Launch_Date__c
                                                        , c.Id
                                                        , c.End_Date__c
                                                        , c.Scope__c 
                                                    From Campaign__c c 
                                                    where (
                                                            c.Creativity_Step_Value__c in :ERTAILMasterHelper.HQPMCreativityStatuses
                                                            and 
                                                            (c.Scope__c = null or c.Scope__c = :ShowWindowScopeLimitation)
                                                        )or(
                                                            c.Quantity_Step_Value__c in :ERTAILMasterHelper.HQPMQuantityStatuses
                                                            and 
                                                            (c.Scope__c = null or c.Scope__c = :InStoreScopeLimitation)
                                                        )
                                                    order by Launch_Date__c];
            }
            return CampaignsWithHQPMRequestedActions_CreativityQuantity;
        }
        private set;
    }

    public List<Campaign__c> CampaignsWithHQPMRequestedActions_Costs{
        get{
            if (CampaignsWithHQPMRequestedActions_Costs == null){
                CampaignsWithHQPMRequestedActions_Costs = [Select c.Scope__c
                                                            , c.Production_Costs_Formula__c
                                                            , c.Installation_Costs_Formula__c
                                                            , c.Delivery_Costs_Formula__c
                                                            , c.Agency_Fees_Formula__c
                                                            , c.Name
                                                            , c.Launch_Date__c
                                                            , c.End_Date__c
                                                            , c.Id 
                                                        From Campaign__c c 
                                                        where 
                                                            c.Installation_Cost_Step_Value__c in :ERTAILMasterHelper.HQPMInstallationFeeStatuses
                                                            or 
                                                            c.Delivery_Cost_Step_Value__c in :ERTAILMasterHelper.HQPMDeliveryFeeStatuses
                                                            or
                                                            c.Prod_Cost__c = 'Pending Confirmation'
                                                            or 
                                                            c.Agency_Fee_Step_Value__c in :ERTAILMasterHelper.HQPMAgencyFeeStatuses
                                                        order by Launch_Date__c, c.Name];
            }
            return CampaignsWithHQPMRequestedActions_Costs;
        }
        private set;
    }
    
    public List<Campaign__c> CampaignsWithHQPMRequestedActions_Visuals{
        get{
            if (CampaignsWithHQPMRequestedActions_Visuals == null){
            	CampaignsWithHQPMRequestedActions_Visuals = new List<Campaign__c>();
                Set<Id> campaignIds = new Set<Id>();
                for (Campaign_Item__c ci : [SELECT ci.Campaign__c
                							, ci.Campaign__r.Id 
											, ci.Campaign__r.Name
					                        , ci.Campaign__r.Launch_Date__c
					                        , ci.Campaign__r.End_Date__c
					                        , ci.Campaign__r.Scope__c
					                        , ci.Campaign__r.Visuals_Formula__c
									      FROM Campaign_Item__c ci
										  WHERE ci.Visual_Step_Value__c in :ERTAILMasterHelper.HQPMVisualStatuses
									      ORDER BY ci.Campaign__r.Launch_Date__c])  {
					if (!campaignIds.contains(ci.Campaign__c)){
						CampaignsWithHQPMRequestedActions_Visuals.add(ci.Campaign__r);					      	
						campaignIds.add(ci.Campaign__c);
					}
		      }
            }
            return CampaignsWithHQPMRequestedActions_Visuals;
        }
        private set;
    }
    
        
    public List<Campaign_Market__c> CampaignMarketsWithScopeOrDatesPendingHQPMApproval{
        get{
            if (CampaignMarketsWithScopeOrDatesPendingHQPMApproval == null){
                CampaignMarketsWithScopeOrDatesPendingHQPMApproval = [Select c.Name
                											, c.Scope_Limitation__c
                                                            , c.MPM_Scope_Limitation_Proposal__c
                                                            , c.Launch_Date__c
                                                            , c.MPM_Launch_Date_Proposal__c
                                                            , c.End_Date__c
                                                            , c.MPM_End_Date_Proposal__c
                                                            , c.Id 
                                                        From Campaign_Market__c c 
                                                        where 
                                                            c.MPM_Proposal_Status__c = :ERTAILMasterHelper.HQPMMarketChangesPendingApproval
                                                        order by Launch_Date__c, c.Name];
            }
            return CampaignMarketsWithScopeOrDatesPendingHQPMApproval;
        }
        private set;
    }
    
    public List<Campaign_Boutique__c> CampaignBoutiquesWithScopeOrDatesPendingHQPMApproval{
        get{
            if (CampaignBoutiquesWithScopeOrDatesPendingHQPMApproval == null){
                CampaignBoutiquesWithScopeOrDatesPendingHQPMApproval = [Select c.Name
                											, c.Campaign_Market__r.Name
                											, c.Campaign_Market__c
                											, c.Scope_Limitation__c
                                                            , c.MPM_Scope_Limitation_Proposal__c
                                                            , c.Launch_Date__c
                                                            , c.MPM_Launch_Date_Proposal__c
                                                            , c.End_Date__c
                                                            , c.MPM_End_Date_Proposal__c
                                                            , c.Id 
                                                        From Campaign_Boutique__c c 
                                                        where 
                                                            c.MPM_Proposal_Status__c = :ERTAILMasterHelper.HQPMBoutiqueChangesPendingApproval
                                                        order by Launch_Date__c, c.Name];
            }
            return CampaignBoutiquesWithScopeOrDatesPendingHQPMApproval;
        }
        private set;
    }
    
    public List<Campaign__c> CampaignsWithCreaRequestedActions_Visuals{
        get{
            if (CampaignsWithCreaRequestedActions_Visuals == null){
                CampaignsWithCreaRequestedActions_Visuals = [Select c.Visuals_Formula__c
                                                        , c.Name
                                                        , c.Launch_Date__c
                                                        , c.Id
                                                        , c.End_Date__c
                                                        , c.Scope__c 
                                                    From Campaign__c c 
                                                    where c.Visual_Step_Value__c in :ERTAILMasterHelper.CreaVisualStatuses
                                                    order by Launch_Date__c];
            }
            return CampaignsWithCreaRequestedActions_Visuals;
        }
        private set;
    }
    
    public List<Campaign__c> CampaignsWithMPMRequestedActions{
        get{
            if (CampaignsWithMPMRequestedActions == null){
                CampaignsWithMPMRequestedActions = [Select c.Show_Window_Status_Formula__c
                                                        , c.In_Store_Status_Formula__c
                                                        , c.Name
                                                        , c.Launch_Date__c
                                                        , c.Id
                                                        , c.End_Date__c
                                                        , c.Scope__c 
                                                    From Campaign__c c 
                                                    where (
                                                            (
                                                            	c.Creativity_Step_Value__c in :ERTAILMasterHelper.AgentCreativityStatuses
                                                            	or
                                                            	c.Creativity_Step_Value__c in :ERTAILMasterHelper.MPMCreativityStatuses
                                                        	)
                                                            and 
                                                            (c.Scope__c = null or c.Scope__c = :ShowWindowScopeLimitation)
                                                        )or(
                                                            (
                                                            	c.Quantity_Step_Value__c in :ERTAILMasterHelper.MPMQuantityStatuses
                                                            	or
                                                            	c.Quantity_Step_Value__c in :ERTAILMasterHelper.AgentQuantityStatuses
                                                            )
                                                            and 
                                                            (c.Scope__c = null or c.Scope__c = :InStoreScopeLimitation)
                                                        )
                                                    order by Launch_Date__c];
            }
            return CampaignsWithMPMRequestedActions;
        }
        private set;
    }
        
    public List<Campaign_Market__c> CampaignMarketsWithMPMRequestedActions{
        get{
            if (CampaignMarketsWithMPMRequestedActions == null){
                CampaignMarketsWithMPMRequestedActions = [Select c.Scope_Limitation__c
                                                            , c.In_Store_Status_Formula__c
                                                            , c.Name
                                                            , c.Launch_Date__c
                                                            , c.End_Date__c
                                                            , c.Show_Window_Status_Formula__c
                                                            , c.Campaign__c 
                                                            , c.Campaign__r.Name
                                                        From Campaign_Market__c c 
                                                        where 
                                                            c.Creativity_Step_Value__c in :ERTAILMasterHelper.MPMCreativityStatuses
                                                            or 
                                                            c.Quantity_Step_Value__c in :ERTAILMasterHelper.MPMQuantityStatuses
                                                        order by Launch_Date__c, Campaign__r.Name, c.Name];
            }
            return CampaignMarketsWithMPMRequestedActions;
        }
        private set;
    }

	public List<Campaign__c> CampaignsWithProdRequestedActions_Costs{
        get{
            if (CampaignsWithProdRequestedActions_Costs == null){
                CampaignsWithProdRequestedActions_Costs = [Select c.Scope__c
                                                            , c.Production_Costs_Formula__c
                                                            , c.Installation_Costs_Formula__c
                                                            , c.Delivery_Costs_Formula__c
                                                            , c.Agency_Fees_Formula__c
                                                            , c.Name
                                                            , c.Launch_Date__c
                                                            , c.End_Date__c
                                                            , c.Id 
                                                        From Campaign__c c 
                                                        where 
                                                            c.Installation_Cost_Step_Value__c in :ERTAILMasterHelper.ProdInstallationFeeStatuses
                                                            or 
                                                            c.Delivery_Cost_Step_Value__c in :ERTAILMasterHelper.ProdDeliveryFeeStatuses
                                                            or
                                                            c.Prod_Cost__c = 'Proposal'
                                                            or 
                                                            c.Agency_Fee_Step_Value__c in :ERTAILMasterHelper.ProdAgencyFeeStatuses
                                                        order by Launch_Date__c, c.Name];
            }
            return CampaignsWithProdRequestedActions_Costs;
        }
        private set;
    }


}
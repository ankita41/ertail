/*
*   @Class      :   ERTAILMyActionsControllerTest.cls
*   @Description:   Test methods for class ERTAILMyActionsController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILMyActionsControllerTest {
    static testMethod void myUnitTest() {
		String MPM_PROPOSAL_INHERIT = ERTAILMyActionsController.MPM_PROPOSAL_INHERIT;
		String ShowWindowScopeLimitation = ERTAILMyActionsController.ShowWindowScopeLimitation;
		String InStoreScopeLimitation = ERTAILMyActionsController.InStoreScopeLimitation;

		Boolean isAdmin = ERTAILMyActionsController.isAdmin;
		Boolean isMPM = ERTAILMyActionsController.isMPM;
		Boolean isHQPM = ERTAILMyActionsController.isHQPM;
		Boolean isCreativeAgency = ERTAILMyActionsController.isCreativeAgency;
		Boolean isProductionAgency = ERTAILMyActionsController.isProductionAgency;
		
    	ERTAILMyActionsController ctrl = new ERTAILMyActionsController();

		List<Campaign__c> l1 = ctrl.CampaignsWithHQPMRequestedActions_CreativityQuantity
		;
		List<Campaign__c> l2 = ctrl.CampaignsWithHQPMRequestedActions_Costs
		;
		List<Campaign__c> l3 = ctrl.CampaignsWithHQPMRequestedActions_Visuals
		;
		List<Campaign_Market__c> l4 = ctrl.CampaignMarketsWithScopeOrDatesPendingHQPMApproval
		;
		List<Campaign_Boutique__c> l5 = ctrl.CampaignBoutiquesWithScopeOrDatesPendingHQPMApproval
		;
		List<Campaign__c> l6 = ctrl.CampaignsWithCreaRequestedActions_Visuals
		;
		List<Campaign__c> l7 = ctrl.CampaignsWithMPMRequestedActions
		;
		List<Campaign_Market__c> l8 = ctrl.CampaignMarketsWithMPMRequestedActions
		;
		List<Campaign__c> l9 = ctrl.CampaignsWithProdRequestedActions_Costs
		;
		

    }
}
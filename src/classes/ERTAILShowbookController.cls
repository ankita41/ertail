/*****
*   @Class      :   ERTAILShowbookController.cls
*   @Description:   Extension developed for ERTAILShowbook.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILShowbookController extends ERTailMasterHandler {

	public class CustomException extends Exception {}

    public Boolean showHeader {get ; private set;}
    
	public static Boolean isHQPM { get{ return ERTailMasterHelper.isHQPM; } }
	public static Boolean isCreativeAgency { get{ return ERTailMasterHelper.isCreativeAgency; } }
	
   
    public List<Id> selectedMarkets { get; private set;}
    
    private List<Id> allMarketsIdsOrderedByName;
    
    public String[] markets { get; set; }
    
    public List<SelectOption> Items { get { return marketOptions; } }
    
    List<SelectOption> marketOptions = new List<SelectOption>{new SelectOption('ALL', 'All')};
    
  // Used by UI
    private void initSelectedMarketUI(){
        if (marketOptions.size() == 2){
            markets = new List<String>{marketOptions[1].getValue()}; 
        }
        else
            markets = new List<String>{'ALL'};
    }
    
     private void initFromCampaignMarkets(){
     	Id marketId;
        for (Campaign_Market__c cm : [
        Select c.Name, c.Id, c.Campaign__c From Campaign_Market__c c
                                        where c.Campaign__c =: campaign.Id 
                                        order by c.Name])
        {
            
            allMarketsIdsOrderedByName.add(cm.Id);
                        
            marketOptions.add(new SelectOption(cm.Id, cm.Name));
            
          
        }  
        selectedMarkets = allMarketsIdsOrderedByName.clone();
        
     }

	public class InStoreWrapper{
		public String cat {
			get;
			private set;
		}
		
		public List<Campaign_Item__c> campaignItems {
			get;
			private set;
		}
		
		public InStoreWrapper(String cat){
			this.cat = cat;
			campaignItems = new List<Campaign_Item__c>();
		}
	}    
	
	public List<InStoreWrapper> inStoreWrappers {
		get{
			List<InStoreWrapper> inStoreWrappers = new List<InStoreWrapper>();
			InStoreWrapper inStoreWrapper = null;
			String currentCategory = null;
			
			InStoreWrapper undefinedCategory = new InStoreWrapper('Undefined');
			
			for(Campaign_Item__c campaignItem : campaignItemsWithoutCreativity){
				Space__c space = AllSpacesMap.get(campaignItem.Space__c);
				if(space.Category__c == null){
					undefinedCategory.campaignItems.add(campaignItem);
				}
				else{
					String campaignItemCategory = space.Category__c;
					if(campaignItemCategory != currentCategory){
						currentCategory = campaignItemCategory;
						inStoreWrapper = new InStoreWrapper(currentCategory);
						inStoreWrapper.campaignItems.add(campaignItem);
						inStoreWrappers.add(inStoreWrapper);
					}
					else{
						inStoreWrapper.campaignItems.add(campaignItem);
					}
				}
			}
			
			if(undefinedCategory.campaignItems.size() > 0){
				inStoreWrappers.add(undefinedCategory);
			}
			return inStoreWrappers;
		}
	}
	
	//***CONSTRUCTOR***//
	public ERTAILShowbookController(ApexPages.StandardController stdController) {
        this.campaign = (Campaign__c)stdController.getRecord();
        
        selectedMarkets = new List<Id>();
        
        allMarketsIdsOrderedByName = new List<Id>();
        
        showHeader = true;
         if (ApexPages.CurrentPage().getParameters().containsKey('showHeader')){
            showHeader = Boolean.valueOf(ApexPages.CurrentPage().getParameters().get('showHeader'));
        }
        
        initSelectedMarketUI();
        initFromCampaignMarkets();
	}

        
	public PageReference filter(){
        
        selectedMarkets.clear();
        
        for (String MarketId : markets) {
            if (MarketId == 'ALL') {
                selectedMarkets = allMarketsIdsOrderedByName.clone();
                break;
            } else {
                selectedMarkets.add(MarketId);
            }     
        }
        if (selectedMarkets.IsEmpty()) selectedMarkets = allMarketsIdsOrderedByName.clone();
        
        return null;
    }
    
    
    
}
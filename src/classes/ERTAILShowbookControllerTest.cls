/*
*   @Class      :   ERTAILShowbookControllerTest.cls
*   @Description:   Test methods for class ERTAILShowbookController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILShowbookControllerTest {
    static testMethod void myUnitTest() {
    	Id testCampaignId = ERTAILTestHelper.createCampaignMME();

		Campaign__c cmp = [select Id from Campaign__c where Id =: testCampaignId];

		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(cmp);
    
    	ERTAILShowbookController ctrl = new ERTAILShowbookController(stdCtrl);
    
       	Boolean bobo = ERTAILShowbookController.isHQPM & ERTAILShowbookController.isCreativeAgency;
 
		if(ctrl.inStoreWrappers== null){}
		
		ctrl.filter();
    }
}
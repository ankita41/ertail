/*****
*   @Class      :   ERTAILSpaceCatalogController.cls
*   @Description:   Extension developed for ERTAILSpaceCatalog.page
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ERTAILSpaceCatalogController extends ERTailMasterHandler {

	public class CustomException extends Exception {}

    public Boolean showHeader {get ; private set;}
    
	public static Boolean isHQPM { get{ return ERTailMasterHelper.isHQPM; } }
	public static Boolean isCreativeAgency { get{ return ERTailMasterHelper.isCreativeAgency; } }
	
	public List<Space__c> spaces {
		get{
			if(spaces == null){
				spaces = [Select Has_Creativity__c, Category__c, Decoration_Alias__c, Name, Image_Id__c, Image_Medium__c from Space__c where Include_in_Space_Catalog__c = true order by Has_Creativity__c, Category__c, Decoration_Alias__c, Name];
			}
			return spaces;
		}
		private set;
	}

	public List<Space__c> showWindowSpaces {
		get{
			if(showWindowSpaces == null){
				showWindowSpaces = new List<Space__c>();
				Integer tIndex = 0;
				Integer kIndex = 0;
				
				for(Space__c space : spaces){
					if(space.Has_Creativity__c == true){
						if(ERTAILMasterHelper.DECORATION_ALIAS_STANDARDSHOWWINDOW.equals(space.Decoration_Alias__c)){
					   		if(tIndex < showWindowSpaces.size()){
					   			showWindowSpaces.add(tIndex, space);
					   		}
					   		else{
					   			showWindowSpaces.add(space);
					   		}
					   		tIndex++;
		                }
		                else if(ERTAILMasterHelper.DECORATION_ALIAS_KEY.equals(space.Decoration_Alias__c)){
		                	if(tIndex + kIndex < showWindowSpaces.size()){
					   			showWindowSpaces.add(tIndex + kIndex, space);
					   		}
					   		else{
					   			showWindowSpaces.add(space);
					   		}
					   		kIndex++;
		                }
		                else{
		                    showWindowSpaces.add(space);
		                }
					}
				}
			}
			return showWindowSpaces;
		}
		private set;
	}

	public List<InStoreSpaceWrapper> inStoreSpaces {
		get{
			List<InStoreSpaceWrapper> inStoreSpaces = new List<InStoreSpaceWrapper>();
			InStoreSpaceWrapper inStoreWrapper = null;
			String currentCategory = null;
			
			InStoreSpaceWrapper undefinedCategory = new InStoreSpaceWrapper('Undefined');
			
			for(Space__c space : spaces){
				if(space.Has_Creativity__c == false){
					if(space.Category__c == null){
						undefinedCategory.spaces.add(space);
					}
					else{
						String spaceCategory = space.Category__c;
						if(spaceCategory != currentCategory){
							currentCategory = spaceCategory;
							inStoreWrapper = new InStoreSpaceWrapper(currentCategory);
							inStoreWrapper.spaces.add(space);
							inStoreSpaces.add(inStoreWrapper);
						}
						else{
							inStoreWrapper.spaces.add(space);
						}
					}
				}
				
			}
			
			if(undefinedCategory.spaces.size() > 0){
				inStoreSpaces.add(undefinedCategory);
			}
			return inStoreSpaces;
		}
	}


	public class InStoreSpaceWrapper{
		public String cat {
			get;
			private set;
		}
		
		public List<Space__c> spaces {
			get;
			private set;
		}
		
		public InStoreSpaceWrapper(String cat){
			this.cat = cat;
			spaces = new List<Space__c>();
		}
	}    
	
	
	//***CONSTRUCTOR***//
	public ERTAILSpaceCatalogController() {
        
        showHeader = true;
         if (ApexPages.CurrentPage().getParameters().containsKey('showHeader')){
            showHeader = Boolean.valueOf(ApexPages.CurrentPage().getParameters().get('showHeader'));
        }
	}        
}
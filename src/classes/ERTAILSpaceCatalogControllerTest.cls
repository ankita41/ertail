/*
*   @Class      :   ERTAILSpaceCatalogControllerTest.cls
*   @Description:   Test methods for class ERTAILSpaceCatalogController.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILSpaceCatalogControllerTest {
    static testMethod void myUnitTest() {
		List<Space__c> spaceList = new List<Space__C>();

        spaceList.add(new Space__c(Name = 'Test Space', Decoration_Alias__c ='Hop', Has_Creativity__c = true));
        spaceList.add(new Space__c(Name = 'Test Space', Decoration_Alias__c = ERTAILMasterHelper.DECORATION_ALIAS_STANDARDSHOWWINDOW, Has_Creativity__c = true));
        spaceList.add(new Space__c(Name = 'Test Space', Decoration_Alias__c = ERTAILMasterHelper.DECORATION_ALIAS_STANDARDSHOWWINDOW, Has_Creativity__c = true));
        spaceList.add(new Space__c(Name = 'Test Space', Decoration_Alias__c = ERTAILMasterHelper.DECORATION_ALIAS_KEY, Has_Creativity__c = true));
        spaceList.add(new Space__c(Name = 'Test Space', Decoration_Alias__c = ERTAILMasterHelper.DECORATION_ALIAS_KEY, Has_Creativity__c = true));
        
        spaceList.add(new Space__c(Name = 'Test Space', Decoration_Alias__c ='Hop', Has_Creativity__c = false, category__c = null));
        spaceList.add(new Space__c(Name = 'Test Space', Decoration_Alias__c ='Hop', Has_Creativity__c = false, category__c = 'In-Store'));
        
        insert spaceList;

    	ERTAILSpaceCatalogController ctrl = new ERTAILSpaceCatalogController();
	
		Boolean showHeader = ctrl.showHeader;
		Boolean isHQPM = ERTAILSpaceCatalogController.isHQPM;
		Boolean isCreativeAgency = ERTAILSpaceCatalogController.isCreativeAgency;
		
		List<Space__c> spaces = ctrl.spaces;
		
		List<Space__c> showWindowSpaces = ctrl.showWindowSpaces;
		
		List<ERTAILSpaceCatalogController.InStoreSpaceWrapper> inStoreSpaces = ctrl.inStoreSpaces;

    }
}
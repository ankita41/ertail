/*
*   @Class      :   ERTAILTestHelper.cls
*   @Description:   static Test methods used in several Test classes
*   @Author     :   Jacky Uy
*   @Created    :   11 FEB 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy        11 FEB 2015     Test Coverage at 

*
*/

@isTest
public class ERTAILTestHelper {
    public static testMethod Id createCampaignWithCampItems() {
        RTCOMMarket__c testMarket = new RTCOMMarket__c(Name = 'Test Market');
        insert testMarket;
        
        Boutique__c recoBoutique = new Boutique__c(Name='reco btq', Default_Image__c = '', Default_Image_2__c = '', Default_Image_3__c = '');
        
        insert recoBoutique;
        
        RTCOMBoutique__c testBoutique = new RTCOMBoutique__c(
            Name = 'Test Boutique',
            RTCOMMarket__c = testMarket.Id
            , RECOBoutique__c = recoBoutique.Id
        );
        insert testBoutique;
        
        Space__c testSpace = new Space__c(Name = 'Test Space', Decoration_Alias__c ='Hop');
        insert testSpace;
        
        insert new Boutique_Space__c(
            RTCOMBoutique__c = testBoutique.Id,
            Space__c = testSpace.Id,
            Decoration_Allowed__c = 'Yes',
            Stickers__c = 'From outside',
            Sticker_color__c = 'Black'
        );

        Boutique_Space__c bs = new Boutique_Space__c(
            RTCOMBoutique__c = testBoutique.Id,
            Space__c = testSpace.Id,
            Decoration_Allowed__c = 'Yes',
            Stickers__c = 'From outside',
            Sticker_color__c = 'Black'
        );

        insert bs;
        update bs;
        delete bs;
        undelete bs;

        Space__c testSpaceWithCreativity = new Space__c(Name = 'Test Space with creativity', Has_Creativity__c = true, Category__c = 'Show Window', Decoration_Alias__c ='Hop');
        insert testSpaceWithCreativity;
        
        insert new Boutique_Space__c(
            RTCOMBoutique__c = testBoutique.Id,
            Space__c = testSpaceWithCreativity.Id,
            Decoration_Allowed__c = 'Yes',
            Creativity_Order__c = 2,
            Stickers__c = 'From outside',
            Sticker_color__c = 'Black'
        );
        
        Topic__c testTopic = new Topic__c(Name = 'Test Topic');
        insert testTopic;
        
        insert new Space_Topic__c(
            Space__c = testSpace.Id,
            Topic__c = testTopic.Id
        );
        
        insert new Space_Topic__c(
            Space__c = testSpaceWithCreativity.Id,
            Topic__c = testTopic.Id
        );
        
        Campaign__c testCampaign = new Campaign__c(
            Name = 'Test Campaign',
            Launch_Date__c = System.Today().addMonths(1),
            End_Date__c = System.Today().addMonths(2),
            Description__c = 'This is a Test'
        );
        insert testCampaign;
        
        insert new Campaign_Topic__c(
            Campaign__c = testCampaign.Id,
            Topic__c = testTopic.Id
        );
        
        Campaign_Market__c testCampMkt = new Campaign_Market__c(
            Campaign__c = testCampaign.Id,
            RTCOMMarket__c = testMarket.Id
        );
        insert testCampMkt;
        
        insert new Campaign_Boutique__c(
            Campaign_Market__c = testCampMkt.Id,
            RTCOMBoutique__c = testBoutique.Id
        );
        
        ApexPages.StandardController stdCon = new ApexPages.StandardController(testCampaign);
        ERTAILCampaignExt testExt = new ERTAILCampaignExt(stdCon);
        testExt.GenerateCampaignItems();
        
        return testCampaign.Id;
    }
    
    public static testMethod Id createCampaignMME() {
        RTCOMMarket__c testMarket = new RTCOMMarket__c(Name = 'Test Market', Zone__c = 'Zone');
        insert testMarket;
        
        Boutique__c recoBoutique = new Boutique__c(Name='reco btq', Default_Image__c = '', Default_Image_2__c = '', Default_Image_3__c = '');
        
        insert recoBoutique;
        
        RTCOMBoutique__c testBoutique = new RTCOMBoutique__c(
            Name = 'Test Boutique'
            , RTCOMMarket__c = testMarket.Id
            , RECOBoutique__c = recoBoutique.Id
        );
        insert testBoutique;
        RTCOMBoutique__c testBoutique1 = new RTCOMBoutique__c(
            Name = 'Test Boutique1'
            , RTCOMMarket__c = testMarket.Id
            , RECOBoutique__c = recoBoutique.Id
        );
        insert testBoutique1;
        
        Space__c testSpaceWithoutCreativity = new Space__c(Name = 'Test Space', Has_quantity__c = true, Decoration_Alias__c ='Hop', Category__c ='In-Store');
        insert testSpaceWithoutCreativity;
        
        Boutique_Space__c btqSpaceWithoutCreativity = new Boutique_Space__c(
            RTCOMBoutique__c = testBoutique.Id,
            Space__c = testSpaceWithoutCreativity.Id,
            Decoration_Allowed__c = 'Yes',
            Stickers__c = 'From outside',
            Sticker_color__c = 'Black'
        );
        
        insert btqSpaceWithoutCreativity;
        
        Space__c testSpaceWithoutCreativity1 = new Space__c(Name = 'Test Space1', Has_quantity__c = true, Decoration_Alias__c ='Hop', Category__c ='In-Store');
        insert testSpaceWithoutCreativity1;
        
        Boutique_Space__c btqSpaceWithoutCreativity1 = new Boutique_Space__c(
            RTCOMBoutique__c = testBoutique1.Id,
            Space__c = testSpaceWithoutCreativity1.Id,
            Decoration_Allowed__c = 'Yes',
            Stickers__c = 'From outside',
            Sticker_color__c = 'Black'
        );
        
        insert btqSpaceWithoutCreativity1;
        Space__c testSpaceWithCreativity = new Space__c(Name = 'Test Space with creativity', Has_Creativity__c = true, Category__c = 'Show Window', Decoration_alias__c = ERTAILMasterHelper.DECORATION_ALIAS_STANDARDSHOWWINDOW);
        insert testSpaceWithCreativity;
        
        Boutique_Space__c btqSpaceWithCreativity = new Boutique_Space__c(
            Name = 'name',
            RTCOMBoutique__c = testBoutique.Id,
            Space__c = testSpaceWithCreativity.Id,
            Decoration_Allowed__c = 'Yes',
            Stickers__c = 'From outside',
            Sticker_color__c = 'Black'
        );
        
        insert btqSpaceWithCreativity;
        
        Space__c testSpaceWithCreativity2 = new Space__c(Name = 'Test Space with creativity', Has_Creativity__c = true, Category__c = 'Show Window', Decoration_alias__c = ERTAILMasterHelper.DECORATION_ALIAS_KEY);
        insert testSpaceWithCreativity2;
        
        Boutique_Space__c btqSpaceWithCreativity2 = new Boutique_Space__c(
            Name = 'name',
            RTCOMBoutique__c = testBoutique.Id,
            Space__c = testSpaceWithCreativity2.Id,
            Decoration_Allowed__c = 'Yes',
            Stickers__c = 'From outside',
            Sticker_color__c = 'Black'
        );
        
        insert btqSpaceWithCreativity2;
        
        Topic__c testTopic = new Topic__c(Name = 'Test Topic');
        insert testTopic;
        
        insert new Space_Topic__c(
            Space__c = testSpaceWithoutCreativity.Id,
            Topic__c = testTopic.Id
        );
        insert new Space_Topic__c(
            Space__c = testSpaceWithoutCreativity1.Id,
            Topic__c = testTopic.Id
        );
        insert new Space_Topic__c(
            Space__c = testSpaceWithCreativity.Id,
            Topic__c = testTopic.Id
        );
        
        Campaign__c testCampaign = new Campaign__c(
            Name = 'Test Campaign',
            Launch_Date__c = System.Today().addMonths(1),
            End_Date__c = System.Today().addMonths(2),
            Description__c = 'This is a Test'
            
        );
        insert testCampaign;
        
        insert new Campaign_Topic__c(
            Campaign__c = testCampaign.Id,
            Topic__c = testTopic.Id
        );
        
        Campaign_Market__c testCampMkt = new Campaign_Market__c(
            Campaign__c = testCampaign.Id,
            RTCOMMarket__c = testMarket.Id
        );
        insert testCampMkt;
        
        
        Campaign_Boutique__c testCmpBtq = new Campaign_Boutique__c(
            Campaign_Market__c = testCampMkt.Id,
            RTCOMBoutique__c = testBoutique.Id,
            scope_limitation__c ='Show Window only'
        );

        insert testCmpBtq;
        Campaign_Boutique__c testCmpBtq1 = new Campaign_Boutique__c(
            Campaign_Market__c = testCampMkt.Id,
            RTCOMBoutique__c = testBoutique1.Id
            
        );

        insert testCmpBtq1;
        
        testCampMkt.End_Date__c = System.Today();
        testCampMkt.Launch_Date__c = System.Today();
        
        update testCampMkt;

        List<Campaign_Item__c> ciList = new List<Campaign_Item__c>();
        
        Campaign_Item__c testCmpItemWithCreativity = new Campaign_Item__c(Campaign__c = testCampaign.Id, Space__c = testSpaceWithCreativity.Id, Alias__c = 'toto');
        ciList.add(testCmpItemWithCreativity);

        Campaign_Item__c testCmpItemWithCreativity2 = new Campaign_Item__c(Campaign__c = testCampaign.Id, Space__c = testSpaceWithCreativity2.Id, Alias__c = 'toto');
        ciList.add(testCmpItemWithCreativity2);

        Campaign_Item__c testCmpItemWithCreativity3 = new Campaign_Item__c(Campaign__c = testCampaign.Id, Space__c = testSpaceWithCreativity2.Id, Alias__c = 'toto');
        ciList.add(testCmpItemWithCreativity3);

        Campaign_Item__c testCmpItemWithoutCreativity = new Campaign_Item__c(Campaign__c = testCampaign.Id, Space__c = testSpaceWithoutCreativity.Id, Alias__c = 'toto');
        ciList.add(testCmpItemWithoutCreativity);
        
        Campaign_Item__c testCmpItemWithoutCreativity1 = new Campaign_Item__c(Campaign__c = testCampaign.Id, Space__c = testSpaceWithoutCreativity1.Id, Alias__c = 'toto');
        ciList.add(testCmpItemWithoutCreativity1);
        
        insert ciList;

        
        Permanent_Fixture_And_Furniture__c testFF = new Permanent_Fixture_And_Furniture__c();
        
        insert testFF;
        
        
        List<Boutique_Order_Item__c> boiList = new List<Boutique_Order_Item__c>();

        Boutique_Order_Item__c testBoiWithCreativity = new Boutique_Order_Item__c(Campaign__c = testCampaign.Id, Campaign_Boutique__c = testCmpBtq.Id, Boutique_Space__c = btqSpaceWithCreativity.Id, Quantity_Step_Picklist__c = '10', Campaign_Item_to_order__c = testCmpItemWithCreativity.Id, Recommended_Campaign_Item__c = testCmpItemWithCreativity.Id);
        boiList.add(testBoiWithCreativity);

        Boutique_Order_Item__c testBoiWithCreativity2 = new Boutique_Order_Item__c(Campaign__c = testCampaign.Id, Campaign_Boutique__c = testCmpBtq.Id, Boutique_Space__c = btqSpaceWithCreativity.Id, Quantity_Step_Picklist__c = '10', Campaign_Item_to_order__c = testCmpItemWithCreativity2.Id, Recommended_Campaign_Item__c = testCmpItemWithCreativity2.Id);
        boiList.add(testBoiWithCreativity2);

        Boutique_Order_Item__c testBoiWithCreativity3 = new Boutique_Order_Item__c(Campaign__c = testCampaign.Id, Campaign_Boutique__c = testCmpBtq.Id, Boutique_Space__c = btqSpaceWithCreativity.Id, Quantity_Step_Picklist__c = '10', Campaign_Item_to_order__c = testCmpItemWithCreativity3.Id, Recommended_Campaign_Item__c = testCmpItemWithCreativity3.Id);
        boiList.add(testBoiWithCreativity3);

        Boutique_Order_Item__c testBoiWithoutCreativity = new Boutique_Order_Item__c(Campaign__c = testCampaign.Id, Campaign_Boutique__c = testCmpBtq.Id, Boutique_Space__c = btqSpaceWithoutCreativity.Id, Campaign_Item_to_order__c = testCmpItemWithoutCreativity.Id, Recommended_Campaign_Item__c = testCmpItemWithoutCreativity.Id);
        boiList.add(testBoiWithoutCreativity);
        
        Boutique_Order_Item__c testBoiFF = new Boutique_Order_Item__c(Campaign__c = testCampaign.Id, Campaign_Boutique__c = testCmpBtq.Id, Permanent_Fixture_And_Furniture__c = testFF.Id);
        boiList.add(testBoiFF);

        insert boiList;
        
        return testCampaign.Id;
    }
}
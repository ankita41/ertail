/*
*   @Class      :   ERTAILVisualsController.cls
*   @Description:   Controller of the ERTAILVisuals.component
*   @Author     :   Jacky Uy
*   @Created    :   10 DEC 2014
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*                         
*/

public with sharing class ERTAILVisualsController extends ERTailMasterHandler {
	public static Boolean isHQPMorAdmin { get{ return isHQPM || isAdmin; } }
	public static Boolean isCreaorAdmin { get{ return isCreativeAgency || isAdmin; } }
	
	public static Boolean isAdmin { get{ return ERTailMasterHelper.isAdmin; } }
	public static Boolean isHQPM { get{ return ERTailMasterHelper.isHQPM; } }
	public static Boolean isCreativeAgency { get{ return ERTailMasterHelper.isCreativeAgency; } }
	
	public ERTAILVisualsController(){}
	
	public Map<Id, Space__c> AllSpacesMap{ get{ return ERTailMasterHandler.AllSpacesMap; }}
	
	public Id campaignId {
		get;
		set{
			campaign = [SELECT Id, Name, Scope__c, Launch_Date__c, End_Date__c, Description__c FROM Campaign__c WHERE Id = :value];
		}
	}
	
	public Map<Id,FeedItem> mapCampaignItemImg {
		get{
			if(mapCampaignItemImg==null){
				mapCampaignItemImg = ERTAILFeedItemHandler.queryCampaignItemImgs(CampaignItems);
			}
			return mapCampaignItemImg;
		} private set;
	}
	
	public Map<Id,Boolean> mapSelectedItems {		
		get{
			if(mapSelectedItems==null){
				mapSelectedItems = new Map<Id,Boolean>();
				for(Campaign_Item__c ci : CampaignItems){
					mapSelectedItems.put(ci.Id, false);
				}
			}
			return mapSelectedItems;
		} set;
	}
	
	public class CustomException extends Exception {/*throw new CustomException();*/}
	
	public String ReasonForRejection {get; set;}
	
	public void rerenderFunc(){
		CampaignItems = null;
		CampaignItemsWithCreativity = null;
		CampaignItemsWithoutCreativity = null;
		mapInStorePerCategory = null;
		mapCampaignItemImg = null;
		ReasonForRejection=null;
	}
	
	public void delImgCommit(){
		Id imgParentId = ApexPages.currentPage().getParameters().get('imgParentId');
		
        //DELETE IMAGE
        delete mapCampaignItemImg.get(imgParentId);
        delete [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :mapCampaignItemImg.get(imgParentId).RelatedRecordId];

        //UPDATE PARENT RECORD
        for(Campaign_Item__c ci : CampaignItems){
        	if(ci.id == imgParentId){
        		ci.Image_ID__c = null;
        		ci.Visual_Reject_Reason__c = null;
        		update ci;
        	}
        }
        rerenderFunc();
    }
    
    public PageReference submitItems(){
    	String chatterMessage = UserInfo.getName() + ' submitted Visual Changes for Approval' + '\nCampaign: ' + campaign.Name;
    	
    	List<Campaign_Item__c> updItems = new List<Campaign_Item__c>();
    	for(Campaign_Item__c ci : CampaignItems){
        	if(mapSelectedItems.get(ci.Id)){
        		ci.Visual_Status_picklist__c = '20';
        		updItems.add(ci);
        		
        		chatterMessage = chatterMessage + '\nCampaign Item: ' + ci.Alias__c;
        	}
        }
        update updItems;
		
		//CHATTER POST
		if(!updItems.isEmpty() && !creaAgency.isEmpty()){
			insert new FeedItem(
	        	ParentId = creaAgency[0].Id,
	        	Body = chatterMessage
	        );
		}
        return ApexPages.currentPage();
    }
    
    public PageReference approveItems(){
    	String chatterMessage = UserInfo.getName() + ' approved the Visual Changes' + '\nCampaign: ' + campaign.Name;
    	
    	List<Campaign_Item__c> updItems = new List<Campaign_Item__c>();
    	for(Campaign_Item__c ci : CampaignItems){
        	if(mapSelectedItems.get(ci.Id)){
        		ci.Visual_Status_picklist__c = '30';
        		ci.Visual_Reject_Reason__c = null;
        		updItems.add(ci);
        		
        		chatterMessage = chatterMessage + '\nCampaign Item: ' + ci.Alias__c;
        	}
        }
        update updItems;
        
        //CHATTER POST
		if(!updItems.isEmpty() && !creaAgency.isEmpty()){
			insert new FeedItem(
	        	ParentId = creaAgency[0].Id,
	        	Body = chatterMessage
	        );
		}
        return ApexPages.currentPage();
    }
    
    public PageReference rejectItems(){
    	ReasonForRejection = (ReasonForRejection == null || ''.equals(ReasonForRejection) ? 'Rejected' : ReasonForRejection);
    	String chatterMessage = UserInfo.getName() + ' rejected the Visual Changes' + '\nCampaign: ' + campaign.Name;
    	
    	List<Campaign_Item__c> updItems = new List<Campaign_Item__c>();
    	for(Campaign_Item__c ci : CampaignItems){
        	if(mapSelectedItems.get(ci.Id)){
        		ci.Visual_Status_picklist__c = '10';
        		ci.Visual_Reject_Reason__c = ReasonForRejection;
        		updItems.add(ci);
        		
        		chatterMessage = chatterMessage + '\nCampaign Item: ' + ci.Alias__c;
        	}
        }
        update updItems;
        
        //CHATTER POST
		if(!updItems.isEmpty() && !creaAgency.isEmpty()){
			insert new FeedItem(
	        	ParentId = creaAgency[0].Id,
	        	Body = chatterMessage + ('Rejected'.equals(ReasonForRejection) ? '' : '\nReason for Rejection: ' + ReasonForRejection)
	        );
		}
        return ApexPages.currentPage();
    }
}
/*
*   @Class      :   ERTAILVisualsControllerTest.cls
*   @Description:   Test Coverage for ERTAILVisualsController.cls
*   @Author     :   Jacky Uy
*   @Created    :   02 FEB 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        	Description
*   ------------------------------------------------------------------------------------------------------------------------    
*	Jacky Uy		11 FEB 2015		Test Coverage at 100%
*
*/

@isTest
private class ERTAILVisualsControllerTest {
    static testMethod void myUnitTest() {
		Id testCampaignId = ERTAILTestHelper.createCampaignWithCampItems();
        
        PageReference pRef = Page.ERTAILMaster; 
        pRef.getParameters().put('id', testCampaignId);
        Test.setCurrentPage(pRef);
        
        Test.startTest();
        ERTAILVisualsController testCon = new ERTAILVisualsController();
        testCon.campaignId = testCampaignId;
        SYSTEM.Assert(!testCon.CampaignItems.isEmpty());
        
        pRef = Page.ERTAILFileUpload; 
        pRef.getParameters().put('id', testCon.CampaignItems[0].Id);
        Test.setCurrentPage(pRef);
        
        ERTAILFileUploadController fileUploadCon = new ERTAILFileUploadController();
        fileUploadCon.varFeed.ContentFileName = 'Test File Upload';
        fileUploadCon.varFeed.ContentData = Blob.valueOf('Test File Upload');
        fileUploadCon.uploadFile();
        testCon.rerenderFunc();
        SYSTEM.Assert([SELECT Image_ID__c FROM Campaign_Item__c WHERE Id = :testCon.CampaignItems[0].Id].Image_ID__c!=NULL);
        
        testCon.mapSelectedItems.put(testCon.CampaignItems[0].Id, true);
        testCon.submitItems();
        testCon.approveItems();
        testCon.rejectItems();
        testCon.rerenderFunc();
        
        pRef = Page.ERTAILMaster; 
        pRef.getParameters().put('imgParentId', testCon.CampaignItems[0].Id);
        Test.setCurrentPage(pRef);
        testCon.delImgCommit();
        SYSTEM.Assert([SELECT Image_ID__c FROM Campaign_Item__c WHERE Id = :testCon.CampaignItems[0].Id].Image_ID__c==NULL);
        
        SYSTEM.Assert(ERTAILVisualsController.isHQPMorAdmin==true);
        SYSTEM.Assert(ERTAILVisualsController.isCreaorAdmin==true);
        SYSTEM.Assert(ERTAILVisualsController.isAdmin==true);
        SYSTEM.Assert(ERTAILVisualsController.isHQPM==false);
        SYSTEM.Assert(ERTAILVisualsController.isCreativeAgency==false);
        Test.stopTest();
    }
}
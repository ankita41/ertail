/*
*   @Class      :   ERTAILWarehouseDetailsExtTest.cls
*   @Description:   Test methods for class ERTAILWarehouseDetailsExt.cls
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class ERTAILWarehouseDetailsExtTest {
    
    static testMethod void myUnitTest() {
        
        
        Id testCampaignId = ERTAILTestHelper.createCampaignWithCampItems();

        Campaign_Boutique__c cmpBtq = [select Id, RTCOMBoutique__c from Campaign_Boutique__c where Campaign_Market__r.Campaign__r.Id =: testCampaignId][0];
        
        RTCOMBoutique__c btq = [select Id, RECO_Opening_Project__c, RecordtypeID, DefaultImageFormula_ID__c, RTCOMMarket__c, RECOBoutique__c, Default_Image__c, Default_Image_2__c, Default_Image_3__c from RTCOMBoutique__C where Id =: cmpBtq.RTCOMBoutique__c];
        
        System.assertEquals(btq != null, true);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(btq);
        
        
        Test.startTest();
        
        
        RECO_Project__c bProj =new RECO_Project__c(Boutique__c = btq.RECOBoutique__c, Project_Type__c = 'New');
        insert bProj;
        
        btq.RECO_Opening_Project__c=bProj.Id;
        update btq;
        
        
        FeedItem post = new FeedItem(parentId = btq.Id);
        post.Body = 'Enter post text here';
        post.LinkUrl = 'http://www.someurl.com';
        //post.ContentData = Blob.valueOf('bla bla bla');
        //post.ContentFileName = 'sample.pdf';
        insert post;
        
             
        Attachment_Folder__c mainFolder = new Attachment_Folder__c(name = 'Full Design Package',RECO_Project__c=btq.RECO_Opening_Project__c);
        insert mainFolder;
        FeedItem feedsMain = new FeedItem (ParentId = mainFolder.id,Body = 'Test Subfolders');
        insert feedsMain;
        Attachment_Subfolder__c attsub = new Attachment_Subfolder__c(Attachment_Folder__c = mainFolder.id,name = 'test record');
        insert attsub;
        FeedItem feeds = new FeedItem (ParentId = attsub.id,Body = 'Test Subfolders');
        insert feeds;
        update mainFolder;
        
        ERTAILWarehouseDetailsExt ctrl = new ERTAILWarehouseDetailsExt(stdController);
        
        Boolean b = ctrl.canEditBoutique;
        b = ctrl.isHQPMorAdmin;
        
        Boolean c = ctrl.IsWarehouse;
        
        List<Boutique_Space__c>  btqSpaceList = ctrl.ShowWindows;
        btqSpaceList = ctrl.inStores;
        
        List<RTCOMBoutique__c> btqList = ctrl.BoutiquesInSameMarket;

        List<FeedItem> Images = ctrl.Images;
        
        System.assertEquals(ctrl.Boutique != null, true);
        
        Integer i = ctrl.imagesRowsSize + ctrl.documentsRowsSize;
        
        FeedItem SelectedDocument  = ctrl.SelectedDocument;
        
        Test.stopTest();
            
    }
    static testMethod void myUnitTest1() {
        
        
        Id testCampaignId = ERTAILTestHelper.createCampaignWithCampItems();

        Campaign_Boutique__c cmpBtq = [select Id, RTCOMBoutique__c from Campaign_Boutique__c where Campaign_Market__r.Campaign__r.Id =: testCampaignId][0];
        
        RTCOMBoutique__c btq = [select Id, RECO_Opening_Project__c, RecordtypeID, DefaultImageFormula_ID__c, RTCOMMarket__c, RECOBoutique__c, Default_Image__c, Default_Image_2__c, Default_Image_3__c from RTCOMBoutique__C where Id =: cmpBtq.RTCOMBoutique__c];

        System.assertEquals(btq != null, true);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(btq);
        
        
        Test.startTest();
        
        
        RECO_Project__c bProj =new RECO_Project__c(Boutique__c = btq.RECOBoutique__c, Project_Type__c = 'New');
        insert bProj;
        
        FeedItem post = new FeedItem(parentId = btq.Id);
        post.Body = 'Enter post text here';
        post.LinkUrl = 'http://www.someurl.com';
        //post.ContentData = Blob.valueOf('bla bla bla');
        //post.ContentFileName = 'sample.pdf';
        insert post;
        
        FeedItem post2 = new FeedItem(parentId = btq.Id);
        post2.Body = 'Enter post text here2';
        post2.LinkUrl = 'http://www.someurl.com';
        //post2.ContentData = Blob.valueOf('bla bla bla11');
        //post2.ContentFileName = 'sample2.pdf';
        insert post2;
    
        FeedItem post3 = new FeedItem(parentId = btq.Id);
        post3.Body = 'Enter post text here3';
        post3.LinkUrl = 'http://www.someurl.com';
        //post3.ContentData = Blob.valueOf('bla bla bla1');
        //post3.ContentFileName = 'sample3.pdf';
        insert post3;
    
        FeedItem post4 = new FeedItem(parentId = btq.Id);
        post4.Body = 'Enter post text here4';
        post4.LinkUrl = 'http://www.someurl.com';
        //post4.ContentData = Blob.valueOf('bla bla');
        //post4.ContentFileName = 'sample4.pdf';
        insert post4;
    
        FeedItem post5 = new FeedItem(parentId = btq.Id);
        post5.Body = 'Enter post text here5';
        post5.LinkUrl = 'http://www.someurl.com';
        //post5.ContentData = Blob.valueOf('bla bla bl');
        //post5.ContentFileName = 'sample5.pdf';
        insert post5;
        
        Attachment_Folder__c mainFolder = new Attachment_Folder__c(name = 'Test Pictures',RECO_Project__c=bProj.id);
        insert mainFolder;
        FeedItem feedsMain = new FeedItem (ParentId = mainFolder.id,Body = 'Test Subfolders');
        insert feedsMain;
        Attachment_Subfolder__c attsub = new Attachment_Subfolder__c(Attachment_Folder__c = mainFolder.id,name = 'test record');
        insert attsub;
        FeedItem feeds = new FeedItem (ParentId = attsub.id,Body = 'Test Subfolders');
        insert feeds;
        update mainFolder;
        
        ERTAILWarehouseDetailsExt ctrl = new ERTAILWarehouseDetailsExt(stdController);
        
        Boolean b = ctrl.canEditBoutique;
        b = ctrl.isHQPMorAdmin;
                
        Boolean c = ctrl.IsWarehouse;
        
        List<Boutique_Space__c>  btqSpaceList = ctrl.ShowWindows;
        btqSpaceList = ctrl.inStores;
        
        List<RTCOMBoutique__c> btqList = ctrl.BoutiquesInSameMarket;

        List<FeedItem> Images = ctrl.Images;
        
        System.assertEquals(ctrl.Boutique != null, true);
        
        Integer i = ctrl.imagesRowsSize + ctrl.documentsRowsSize;
        
        FeedItem SelectedDocument  = ctrl.SelectedDocument;
        ERTAILWarehouseDetailsExt.FeedItemRow fItemRow= new ERTAILWarehouseDetailsExt.FeedItemRow(10); 
        Test.stopTest();
            
    }
}
/*****
*   @Class      :   ERTAILWarehousePerMarketController.cls
*   @Description:   Extension developed for ERTAILWarehousePerMarket.page
*   @Author     :   Priya Srivastava
*   @Created    :   25 OCT 2016
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/
public with sharing class ERTAILWarehousePerMarketController {
    public class CustomException extends Exception {}

    public RTCOMMarket__c selectedMarket {get; set;} 
    public Boolean showHeader {get ; private set;}
    
    public static Boolean isMPMorAgent { get{ return ERTailMasterHelper.isMPM || ERTailMasterHelper.isAgent; } }
    public static Boolean isHQPMorAdmin { get{ return ERTailMasterHelper.isHQPM || ERTailMasterHelper.isAdmin; } }
    
    public static Boolean isHQPM { get{ return ERTailMasterHelper.isHQPM; } }
    public static Boolean isCreativeAgency { get{ return ERTailMasterHelper.isCreativeAgency; } }
    
    public static Id warehouseRTId = Schema.SObjectType.RTCOMBoutique__c.getRecordTypeInfosByName().get('Warehouse').getRecordTypeId();
        
    private List<Id> allMarketsIdsOrderedByName;
    public RTCOMBoutique__c repeatBoutique { get; private set;}
    
    public List<SelectOption> Items { get { return marketOptions; } }
    List<SelectOption> marketOptions = new List<SelectOption>{};
    
    public String boutiqueSearch { 
        get{
            if (boutiqueSearch == null){
                boutiqueSearch = '';
            }
            return boutiqueSearch;
        } 
        set; 
    }
    
    public List<SelectOption> typologyOptions {
        get{
            if(typologyOptions == null){
                typologyOptions = new List<SelectOption>();
                
                Schema.sObjectType objType = RTCOMBoutique__c.getSObjectType();

                Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
//              throw new CustomException('b');
                
                Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
                
                List<Schema.PicklistEntry> values = fieldMap.get('Typology__c').getDescribe().getPicklistValues();
            
                typologyOptions.add(new SelectOption( 'ALL', 'All Typologies'));
            
                for (Schema.PicklistEntry a : values){
                    typologyOptions.add(new SelectOption(a.getLabel(), a.getValue())); 
                }
                
                
            }
            return typologyOptions;
        }
        set;
    }
    
    public String typologySearch {
        get;
        set;
    }
    
    public String typologyFilter {
        get{
            if(typologySearch == null || typologySearch == 'ALL'){
                return '';
            }
            else{
                return typologySearch;
            }
        }
    }
    
    
    public List<RTCOMBoutique__c> BoutiqueList{
        get{
            if (BoutiqueList == null){
                List<RTCOMBoutique__c> intermediateList = [Select r.RTCOMMarket__r.Id
                , r.RTCOMMarket__c
                , r.RECOBoutique__c
                , r.RECOBoutique__r.Name
                , r.RECOBoutique__r.Status__c
                , r.StatusFormula__c
                , r.DefaultImageFormula_ID__c 
                , r.Delivery_Address_Formula__c
                , r.Name
                //, r.Location__c
                , r.City_English__c
                , r.Country_English__c
                , r.Image_ID__c
                , r.Id
                , r.Typology__c
                , r.Address__c From RTCOMBoutique__c r
                                where r.RTCOMMarket__r.Id =: selectedMarket.Id
                                //and r.Excluded_for_future_campaigns__c = false
                                and recordTypeId = :warehouseRTId
                                and (r.Name like :('%' + boutiqueSearch + '%') or RECOBoutique__r.Name like :('%' + boutiqueSearch + '%'))
                                order by City_English__c, r.Name]; 
            
                if(typologyFilter == ''){
                    BoutiqueList = intermediateList;
                }
                else{
                    BoutiqueList = new List<RTCOMBoutique__c>();
                    for(RTCOMBoutique__c boutique : intermediateList){
                        if(boutique.Typology__c == typologySearch){
                            BoutiqueList.add(boutique);
                        }
                    }
                }
            }
            return BoutiqueList;
        }
        set;
    }
  
    public void filterBoutiques(){
        BoutiqueList = null;
    }
    
     private void initFromCampaignMarkets(){
        List<RTCOMMarket__c> fullMarketList = [Select r.Name, r.Zone__c, r.Delivery_Address_Formula__c , r.RECOMarket__c, r.Id From RTCOMMarket__c r where ID in (Select RTCOMMarket__c from RTCOMBoutique__c where RecordtypeID =:warehouseRTId) order by r.Name]; 
        for (RTCOMMarket__c cm : fullMarketList)
        {
            marketOptions.add(new SelectOption(cm.Id, cm.Name));
        }
        if (selectedMarket.Id ==null && fullMarketList.size() > 0){
            selectedMarket = fullMarketList[0];
        }
     }
    
    
    //***CONSTRUCTOR***//
    public ERTAILWarehousePerMarketController (ApexPages.StandardController stdController) {
        selectedMarket = (RTCOMMarket__c) stdController.getRecord();
        repeatBoutique = new RTCOMBoutique__c();
        
        showHeader = true;
         if (ApexPages.CurrentPage().getParameters().containsKey('showHeader')){
            showHeader = Boolean.valueOf(ApexPages.CurrentPage().getParameters().get('showHeader'));
        }
        
        initFromCampaignMarkets();
    }

        
    public PageReference filter(){
        PageReference ref = ApexPages.CurrentPage();
        ref.getParameters().put('Id', selectedMarket.Id);
        ref.setRedirect(true);
        //throw new CustomException (''+selectedMarketId);
        return ref;
    }
    
    public PageReference RedirectWarehouses(){
      Schema.DescribeSObjectResult R = RTCOMBoutique__c.SObjectType.getDescribe();
      PageReference ref;
       ref = new PageReference('/' + R.getKeyPrefix() +'/e'+ '?RecordType=' + warehouseRTId);
       //ref.getParameters().put('retURL', '/apex/ERTAILBoutiquePerMarket?Id=' + selectedMarket.Id);
       ref.setRedirect(true);
      return ref;
    }
    
    
}
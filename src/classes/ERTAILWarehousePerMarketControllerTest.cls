/*
*   @Class      :   ERTAILWarehousePerMarketControllerTest.cls
*   @Description:   Test methods for class ERTAILWarehousePerMarketController.cls
*   @Author     :   Mansimar Singh
*   @Created    :   28 Mar 2017
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
public class ERTAILWarehousePerMarketControllerTest {

    static testMethod void myUnitTest() {
        
        
        Id testCampaignId = ERTAILTestHelper.createCampaignWithCampItems();
        RTCOMMarket__c testMarket = [Select id,Name from RTCOMMarket__c where Name='Test Market'][0];
        ApexPages.StandardController stdController = new ApexPages.StandardController(testMarket);
        
        Test.startTest();
        ERTAILWarehousePerMarketController ctrl = new ERTAILWarehousePerMarketController(stdController);
        
        String boutiqueSearch= ctrl.boutiqueSearch;
        PageReference pref= Page.ERTAILBoutiquePerMarket;
        ctrl.RedirectWarehouses();
        pref.getParameters().put('Id',testMarket.id);
        //Setting Boutique list --not required as with boutique list no code to be covered
       // ctrl.BoutiqueList=[Select id ,name from RTCOMBoutique__c where RTCOMMarket__c =:testMarket.Id];
       ctrl.typologySearch='NOTALL';
       List<SelectOption> listSO= ctrl.typologyOptions; 
        List<RTCOMBoutique__c> l= ctrl.BoutiqueList;
        ctrl.filter();
        ctrl.filterBoutiques();
        
        
        
        Test.stopTest();
    }
    
    //Boutique list null as market is dummy 
    static testMethod void dummyMarket() {
        
        insert new RTCOMMarket__c(name='dummy') ;
        RTCOMMarket__c dummyMarket = [Select id,Name from RTCOMMarket__c where Name='dummy'];
        
        //Id testCampaignId = ERTAILTestHelper.createCampaignWithCampItems();
        ApexPages.StandardController stdController = new ApexPages.StandardController(dummyMarket);
        
        Test.startTest();
        ERTAILWarehousePerMarketController ctrl = new ERTAILWarehousePerMarketController(stdController);
        
        String boutiqueSearch= ctrl.boutiqueSearch;
        PageReference pref= Page.ERTAILBoutiquePerMarket;
        ctrl.RedirectWarehouses();
        pref.getParameters().put('Id',dummyMarket.id);
        ctrl.filter();
        ctrl.filterBoutiques();
        ctrl.typologySearch='NOTALL';
        List<SelectOption> listSO= ctrl.typologyOptions;
        List<RTCOMBoutique__c> l= ctrl.BoutiqueList;
        
        Test.stopTest();
    }
}
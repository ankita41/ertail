public with sharing class EmbeddedReportController{

    public class CustomException extends Exception{}
    private Integer NbColumns = 1;
    private List<Object> detailColumns;
    Map<String, Object> detailColumnInfo;
    //Map<String, Object> groupingsDown;
    
    public Id report {get; set;}
    public String baseURL {get; set;}

    public class Row{
        public List<Cell> cells { get; set;}
        
        public Row(){
            cells  = new List<Cell>();
        }
    }

    public class Cell{
        public String label {get; set;}
        public String value {get; set;}
        
        public Cell(Map<String, Object> c){
            label = (String)c.get('label');
            value = String.valueOf(c.get('value'));
        }
    }

    public class Grouping{
        public List<Grouping> groupings { get; set;}
        public List<Row> rows {get; set;}
        public String key{ get; set;}
        public String label{ get; set;}
        public String value{ get; set;}
        
        public Grouping(Map<String, Object> grp, Map<String, Object> factMap){
            key = (String)grp.get('key');
            label = (String)grp.get('label');
            value = (String)grp.get('value');
            groupings  = new List<Grouping>();
            rows = new List<Row>();
            List<Object> items = (List<Object>)grp.get('groupings');
            for (Object item : items ){
               groupings.add(new Grouping((Map<String, Object>)item, factMap)); 
            }
            String mapKey = key + '!T';
            if (factMap.containsKey(mapKey)){
                Map<String, Object> aggregates = (Map<String, Object>)factMap.get(mapKey);
                List<Object> rs = (List<Object>)aggregates.get('rows');
                for (Object r: rs){
                  Row newRow = new Row();
                  List<Object> dataCells= (List<Object>)((Map<String, Object>)r).get('dataCells');  
                  for (Object cell : dataCells){
                     newRow .Cells.add(new Cell((Map<String, Object>)cell)); 
                  }
                  rows.add(newRow );
                }
            }
        }
        
        public Grouping(Map<String, Object> factMap){
			groupings  = new List<Grouping>();
			rows = new List<Row>();  
            String mapKey = 'T!T';
            if (factMap.containsKey(mapKey)){
                Map<String, Object> aggregates = (Map<String, Object>)factMap.get(mapKey);
                List<Object> rs = (List<Object>)aggregates.get('rows');
                for (Object r: rs){
                  Row newRow = new Row();
                  List<Object> dataCells= (List<Object>)((Map<String, Object>)r).get('dataCells');  
                  for (Object cell : dataCells){
                     newRow .Cells.add(new Cell((Map<String, Object>)cell)); 
                  }
                  rows.add(newRow );
                }
            }
        }
    }
    
    public HttpRequest BuildHTTPRequest(){
    	HttpRequest req = new HttpRequest();
        req.setEndpoint(GetBaseUrlForInstance() + '/services/data/v29.0/analytics/reports/' + report + '?includeDetails=true');
        req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
        req.setMethod('GET');
        return req;
    }
    
    private HTTPResponse getData(){
        Http http = new Http();
        HttpRequest req = BuildHTTPRequest();
        return http.send(req);

    }

    public EmbeddedReportController(){
    	detailColumns = new List<Object>();
        //report = '00O17000000FF36';
    }
    
    public void loadReport(){
    }
    
    public static String Instance {
        public get {
            if (Instance == null) {
                //
                // Possible Scenarios:
                //
                // (1) ion--test1--nexus.cs0.visual.force.com  --- 5 parts, Instance is 2nd part
                // (2) na12.salesforce.com      --- 3 parts, Instance is 1st part
                // (3) ion.my.salesforce.com    --- 4 parts, Instance is not determinable
    
                // Split up the hostname using the period as a delimiter
                List<String> parts = System.URL.getSalesforceBaseUrl().getHost().replace('-api','').split('\\.');
                if (parts.size() == 3) Instance = parts[0];
                else if (parts.size() == 5) Instance = parts[1];
                else Instance = null;
            } return Instance;
        } private set;
    }
    
    // And you can then get the Salesforce base URL like this:
    public static String GetBaseUrlForInstance() {
    
         return 'https://' + Instance + '.salesforce.com';
    
    }
        
   private List<Grouping> getReportData(){
        HTTPResponse res = getData();
        Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        Map<String, Object> groupingsDown= (Map<String, Object>)root.get('groupingsDown');
        Map<String, Object> factMap = (Map<String, Object>)root.get('factMap');
        Map<String, Object> reportMetadata = (Map<String, Object>)root.get('reportMetadata');
        detailColumns= (List<Object>)reportMetadata.get('detailColumns');
        Map<String, Object> reportExtendedMetadata = (Map<String, Object>)root.get('reportExtendedMetadata');
        detailColumnInfo = (Map<String, Object>)reportExtendedMetadata.get('detailColumnInfo');
                
        NbColumns = detailColumnInfo.size();
        List<Object> items = (List<Object>)groupingsDown.get('groupings');
        List<Grouping> tableData = new List<Grouping>();
        if (items.size() > 0){
	        for (Object item : items){
	            tableData.add(new Grouping((Map<String, Object>)item, factMap )); 
	        }
        }else{
        	tableData.add(new Grouping(factMap)); 
        }
        return tableData;
    }
    
    public Component.Apex.OutputPanel getThePanel(){
        Component.Apex.OutputPanel outPanel = new Component.Apex.OutputPanel();
        
        if (report != null){
            List<Grouping> reportData = getReportData();
            Component.Apex.OutputText htmlTxt = constructHtmlText(reportData);
            outPanel.childComponents.add(htmlTxt);
        }
        return outPanel;
    }
    
    public Component.Apex.OutputText constructHtmlText(List<Grouping> reportData){
    	Component.Apex.OutputText htmlTxt = new Component.Apex.OutputText();
        htmlTxt.value = '<table cellpadding="0" cellspacing="0"><tr>';
        for (Object ob : detailColumns){
            htmlTxt.value += '<th>' + (String)((Map<String, Object>)detailColumnInfo.get((String)ob)).get('label') + '</th>';
        }
        htmlTxt.value += '</tr>';
        htmlTxt.escape = false;
        for(Grouping grp : reportData ) {
            htmlTxt.value += addChildComponents(grp, 1);
        }
        htmlTxt.value += '</table>';
        return htmlTxt;
    }
    
    public Boolean isValidId(String s) {
        Id validId;
        try {
            validId = s;
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
        
    public String addChildComponents(Grouping grp, Integer level) {
        String rowClass = 'even';
        String htmlTxt = '';
        {
        	if (grp.Label != null)
            	htmlTxt += '<tr><td colspan="' + NbColumns + '" class="group"><h' + level + '>' + grp.Label + '</h' + level + '></td></tr>';
            if (grp.Rows.size() > 0){
                for (Row r : grp.Rows){
                    htmlTxt += '<tr class="' + rowClass + '">';
                    for (Cell c : r.Cells){
                        if (c.Label != c.Value && isValidId(c.Value)){
                            htmlTxt += '<td><a href="' + c.Value + '">' + c.Label + '</a></td>';
                        }else{
                            htmlTxt += '<td>' + c.Label + '</td>';
                        }
                    }
                    htmlTxt += '</tr>';
                    rowClass = ('even'.equals(rowClass) ? 'odd' : 'even');
                }
            }
        }
        if (grp.groupings.size() >0){
            for (Grouping childGrp : grp.groupings){
                htmlTxt  += addChildComponents( childGrp , level+1);
            }
        }
        return htmlTxt;
    }

}
/*
*   @Class      :   Ertail_SelectedMarketListContoller.cls
*   @Description:   Controller for Page Ertail_SelectedMarketList
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/
public with Sharing class Ertail_SelectedMarketListContoller {
    public List<marketWrapper> CampaignMarket {get; set;}
    public Campaign__c Campaign{get;set;}
    public string selectedItemId{get;set;}
    public Integer sizeList{get;set;}
    public string val{get;set;}
    public String mktIds;
    public Id CampaignId{get;set;}
    private List<Campaign_Market__c> campaignMarkets;
    public id CampaignRecId{get; set;}
    public list<String> AlphaList {get; set;}
    public String AlphaFilter {get; set;}
    public String SortFieldSave;
    
    private String QueryAccount;
    
    public String RecPerPage {get; set;}
    
    public Ertail_SelectedMarketListContoller(ApexPages.StandardController controller) {
        Campaign= (Campaign__c)controller.getRecord();
         CampaignId=Campaign.id;
         CampaignRecId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Recommendations').getRecordTypeId();
         RecPerPage = '100'; 
         // initialization alpha list
            AlphaList = new list<String> {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Other', 'All'};
            SortFieldSave = SortField;
            if (apexpages.currentpage().getparameters().get('alpha') == null) {
                AlphaFilter = 'All';
            } else {
                AlphaFilter = apexpages.currentpage().getparameters().get('alpha');
            }
            BuildQuery();
   }
   
   public ApexPages.StandardSetController StdSetControllerList {
        get {
            if(StdSetControllerList == null) {
                StdSetControllerList = new ApexPages.StandardSetController(Database.getQueryLocator(QueryAccount));
                // sets the number of records in each page set
                StdSetControllerList.setPageSize(Integer.valueOf(RecPerPage));
            }
            return StdSetControllerList;
        }
        set;
    }
    
   public String SortDirection {
        get { if (SortDirection == null) {  SortDirection = 'asc'; } return SortDirection;  }
        set;
    }
    public String SortField {
        get { if (SortField == null) {SortField = 'Name'; } return SortField;  }
        set; 
    } 
    public void SortToggle() {
        SortDirection = SortDirection.equals('asc') ? 'desc' : 'asc';
        // reset alpha filter and sort sequence when sorted field is changed
        if (SortFieldSave != SortField) {
            SortDirection = 'asc';
            AlphaFilter = 'All';
            SortFieldSave = SortField;
        }
        // run the query again
        BuildQuery();
    }
    public List<marketWrapper> getCampaignMarket1(){
     //if(CampaignMarket==null){
            CampaignMarket= new List<marketWrapper>();
            system.debug('****CampaignMarket='+CampaignMarket);
            for(Campaign_Market__c cm : (list<Campaign_Market__c>)StdSetControllerList.getRecords()){
                CampaignMarket.add(new marketWrapper(cm));
            }  
      //  }
        return CampaignMarket;
        
    }
    public pageReference SelectAll()
    {
        for(marketWrapper m : CampaignMarket){
          m.selectedMarket = true;
         }
         return null;
    }
   
    public String getmktId() {
       mktIds = '';
      for(marketWrapper m : CampaignMarket){
       system.debug('*****selectedMarket '+m.selectedMarket );
          if(m.selectedMarket == true){
               mktIds +=  (mktIds==''?'':',') + m.Cmarket.id;

          }
      }
        return mktIds;  
    }
  
    public pageReference deleteItem()
    {
        try{
                delete [select id from Campaign_Market__c where id=:selectedItemId];
                return new pageReference('/'+Campaign.id);
        }
                catch(dmlexception e) { }
                return null;
    }
    public pageReference ERTAILCampaignRequestMPMProposalButton()
    {
        try{
               mktIds=getmktId();
               return new pageReference('/apex/ERTAILCampaignRequestMPMProposalButton?id='+Campaign.id +'&CampaignmktIds='+mktIds);
        }
                catch(dmlexception e) { }
                return null;
    }
     public pageReference ERTAILCampaignRemindMPMForFeedbackButton()
    {
        try{
               mktIds='';
               mktIds=getmktId();
               return new pageReference('/apex/ERTAILCampaignRemindMPMForFeedbackButton?id='+Campaign.id +'&CampaignmktIds='+mktIds);
        }
                catch(dmlexception e) { }
                return null;
    }

     public pageReference ERTAILCampaignExportOrdersPartial()
    {
        try{
               mktIds='';
               mktIds=getmktId();
               return new pageReference('/apex/ERTAILCampaignExportOrdersPartial?id='+Campaign.id +'&CampaignmktIds='+mktIds);
        }
                catch(dmlexception e) { }
                return null;
    }

     public pageReference ERTAILCampaignExportCostsPartial()
    {
        try{
                mktIds='';
                mktIds=getmktId();
                return new pageReference('/apex/ERTAILCampaignExportCostsPartial?id='+Campaign.id +'&CampaignmktIds='+mktIds);
        }
                catch(dmlexception e) { }
                return null;
    }
    public pageReference ERTAILCampaignLockOrderButton()
    {
        try{
               mktIds=getmktId();
               return new pageReference('/apex/ERTAILCampaignMarketLockOrders?id='+Campaign.id +'&CampaignmktIds='+mktIds);
        }
                catch(dmlexception e) { }
                return null;
    }

   public class marketWrapper{
       public Boolean selectedMarket {get; set;}
       public Campaign_Market__c Cmarket {get; set;}
       public marketWrapper(Campaign_Market__c Con){
           selectedMarket = false;
           Cmarket = Con;
       }
       
   } 
   public void BuildQuery() {
        StdSetControllerList = null;
        String QueryWhere = '';
        
        if (AlphaFilter == null || AlphaFilter.trim().length() == 0) {
            AlphaFilter = 'All';
        }
        
        QueryAccount = 'Select c.Total_Max_Costs_Eur__c, c.RTCOMMarket__r.Id,c.RTCOMMarket__r.Name,c.Total_Actual_Costs_Eur__c, c.SystemModstamp, c.Show_Window_Status_Formula__c, c.Send_HQPM_Recommendation_to_MPM__c, c.Scope_Limitation__c, c.Reminder_for_MPM_Feedback__c, c.Reminder_Last_Sent_Date__c, c.RecordTypeId, c.RTCOMMarket__c, c.Quantity_Step__c, c.Quantity_Step_Value__c, c.Production_Contact__c, c.OwnerId, c.OP_max_cost_Eur__c, c.OP_actual_cost_Eur__c, c.OP_Max_Agency_Fee__c, c.Number_of_Selected_Boutiques__c, c.Notify_Orders_Locked__c, c.Name, c.ManageOrders_Link__c, c.MPM_Scope_Limitation_Proposal__c, c.MPM_Proposal_Status__c, c.MPM_Proposal_Comment__c, c.MPM_Launch_Date_Proposal__c, c.MPM_End_Date_Proposal__c, c.MPM_Boutiques_Proposal_Comment__c, c.Launch_Date__c, c.LastModifiedDate, c.LastModifiedById, c.Installation_Fee_Step__c, c.Installation_Fee_Step_Value__c, c.Installation_Fee_Step_Formula__c, c.Installation_Fee_MIN__c, c.Installation_Fee_MAX__c, c.Installation_Fee_Eur__c, c.In_Store_Status_Formula__c, c.Id, c.HQPM_Rejection_Comment__c, c.HQPM_Boutiques_Rejection_Comment__c, c.Fixture_Furnitures_Costs_Eur__c, c.End_Date__c, c.Delivery_Pick_Up_Fee_Step_Formula__c, c.Delivery_Fee_Step__c, c.Delivery_Fee_Step_Value__c, c.Delivery_Fee_MIN__c, c.Delivery_Fee_MAX__c, c.Delivery_Fee_Eur__c, c.Creativity_Step__c, c.Creativity_Step_Value__c, c.CreatedDate, c.CreatedById, c.Campaign__r.Id, c.Campaign__c, c.Campaign_Market_Status__c, c.Campaign_Market_Status_Value__c, c.CampaignMarketLink__c, c.Agency_Actual_Fees__c From Campaign_Market__c c where Campaign__r.Id=:CampaignId'; 
        
                
        
        if (AlphaFilter == 'Other') {
            QueryWhere = BuildWhere(QueryWhere, '(' + String.escapeSingleQuotes(SortField) + ' < \'A\' OR ' + 
                                    String.escapeSingleQuotes(SortField) + ' > \'Z\') AND (NOT ' + 
                                    String.escapeSingleQuotes(SortField) + ' LIKE \'Z%\') ');
        } else if (AlphaFilter != 'All') {
            QueryWhere = BuildWhere(QueryWhere, '(' + String.escapeSingleQuotes(SortField) + ' LIKE \'' + String.escapeSingleQuotes(AlphaFilter) + '%\')' );
        }
        
        
        
        QueryAccount += QueryWhere;
        QueryAccount += ' ORDER BY ' + String.escapeSingleQuotes(SortField) + ' ' + String.escapeSingleQuotes(SortDirection) + ' LIMIT 10000';
        
        system.debug('QueryAccount:' + QueryAccount);
    }
    
    public String BuildWhere(String QW, String Cond) {
        if (QW == '') {
            return  ' AND '+Cond;
        } else {
            return QW + ' AND ' + Cond;
        }
    }
}
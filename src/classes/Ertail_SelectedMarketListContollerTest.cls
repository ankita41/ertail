@isTest
Public class Ertail_SelectedMarketListContollerTest {

    static testMethod void TestMethod1() {
          Id testCampaignId = ERTAILTestHelper.createCampaignMME();
          Campaign__c cmp = [select Id, Name, Scope__c, Launch_Date__c, End_Date__c from Campaign__c where Id =: testCampaignId][0];  
          Campaign_Market__c cmpMkt = [select Id from Campaign_Market__c where Campaign__r.Id =: cmp.Id][0];
          ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);
          Ertail_SelectedMarketListContoller ctrl = new Ertail_SelectedMarketListContoller(stdController);
          ctrl.SelectedItemId='';
          ctrl.val='1';
          ctrl.sizelist=10;
          ApexPages.StandardSetController std =ctrl.StdSetControllerList ;
          ctrl.CampaignMarket = ctrl.getCampaignMarket1();
          ctrl.CampaignMarket[0].selectedMarket = true;
          ctrl.ERTAILCampaignRemindMPMForFeedbackButton();
          ctrl.ERTAILCampaignRequestMPMProposalButton();
          ctrl.ERTAILCampaignRemindMPMForFeedbackButton();
          ctrl.ERTAILCampaignExportCostsPartial();
          ctrl.ERTAILCampaignExportOrdersPartial();
          ctrl.ERTAILCampaignLockOrderButton();
          ctrl.deleteItem();
          ctrl.SortFieldSave ='id';
          ctrl.sorttoggle();
          
      
    }
    static testMethod void TestMethod2() {
          Id testCampaignId = ERTAILTestHelper.createCampaignMME();
          Campaign__c cmp = [select Id, Name, Scope__c, Launch_Date__c, End_Date__c from Campaign__c where Id =: testCampaignId][0];  
          Campaign_Market__c cmpMkt = [select Id from Campaign_Market__c where Campaign__r.Id =: cmp.Id][0];
          Apexpages.currentPage().getParameters().put('alpha','other');
          ApexPages.StandardController stdController = new ApexPages.StandardController(cmp);
          Ertail_SelectedMarketListContoller ctrl = new Ertail_SelectedMarketListContoller(stdController);
          ApexPages.StandardSetController std =ctrl.StdSetControllerList ;
          
          
      
    }
 }
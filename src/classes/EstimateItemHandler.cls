/*****
*   @Class      :   EstimateItemHandler.cls
*   @Description:   Handles all Estimate_Item__c recurring actions/events.
*   @Author     :   Jacky Uy
*   @Created    :   15 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer        Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy         17 APR 2014     Fix: Changed the CurrencyConverter constructor to pass a Set of Local Currencies
*   Jacky Uy         22 APR 2014     Fix: Passed the record's creation date as a parameter when converting currencies
*   Jacky Uy         02 MAY 2014     Fix: Roll-up Calculation
*   Jacky Uy         09 MAY 2014     Change: Referenced a static method for Currency Conversion
*   Jacky Uy         03 JUN 2014     Add: Amount__c field on Estimate Item to be used in calculating the Currency Conversion
*   Jacky Uy         30 JUN 2014     Improved Currency Conversion method
*   Jacky Uy         02 JUL 2014     Created: ValidateCurrencyConversion() method
*  Koteswararao      19 March 2015    Modified code convertEstimateItemCurrency method ,for the curecny upload 
*                         
***/

public with sharing class EstimateItemHandler{
    public static void StopUpsertOfEstimatesAfterValidation(List<Estimate_Item__c> newList){
        Set<Id> projIds = new Set<Id>();
        for(Estimate_Item__c ei : newList){
            projIds.add(ei.RECO_Project__c);
        }
        
        Map<Id, RECO_Project__c> mapProjects = new Map<Id, RECO_Project__c>([
            SELECT Id, Notify_national_btq_manager_for_estimate__c, Notify_for_estimate_cost_approval__c, Cost_estimate_approval_status__c 
            FROM RECO_Project__c WHERE Id IN :projIds
        ]);
        
        String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        for(Estimate_Item__c ei : newList) {
            if (((currentUserProfile == 'NES RECO Retail Project Manager' || currentUserProfile == 'NES NCAFE Retail Project Manager') && mapProjects.get(ei.RECO_Project__c).Cost_estimate_approval_status__c == 'Approved') ||
                ((currentUserProfile == 'NES RECO Manager Architect' || currentUserProfile == 'NES NCAFE Manager Architect') && mapProjects.get(ei.RECO_Project__c).Notify_national_btq_manager_for_estimate__c == true) || 
                ((currentUserProfile == 'NES RECO National Boutique Manager' || currentUserProfile == 'NES NCAFE National Boutique Manager' ) && mapProjects.get(ei.RECO_Project__c).Notify_for_estimate_cost_approval__c == true)){
                
                ei.RECO_Project__c.addError('The estimated cost form for this project is frozen. Modifying or adding new items is not authorized.');
            }
        }
    }
    
    public static void convertEstimateItemCurrency(List<Estimate_Item__c> newList, Map<Id,Estimate_Item__c> oldMap){
        Set<String> currencyYears = new Set<String>();
        Set<String> itemCurrencies = new Set<String>();
        Datetime currencyAppliedDate;
         Set<String> projectList=new Set<String>();  
        for(Estimate_Item__c obj: newList) {
            projectList.add(obj.RECO_Project__c);        
         }      
         map<id,RECO_Project__c> offersmap= New map<id,RECO_Project__c>([Select ID,OpeningFormula__c,Cost_estimate_approval_status__c,Cost_estimate_approval_date__c from RECO_Project__c Where ID IN :projectList]);   
            
        
        for(Estimate_Item__c obj: newList) {
            /* code modifcation done for currency upload --start--Koteswarar*/
           RECO_Project__c  recoproject=offersmap.get(obj.RECO_Project__c);
            currencyAppliedDate= (recoproject.OpeningFormula__c!=null) ? recoproject.OpeningFormula__c : SYSTEM.Today();
            if(recoproject.Cost_estimate_approval_status__c!= null && recoproject.Cost_estimate_approval_status__c !='' &&recoproject.Cost_estimate_approval_status__c=='Approved'){
                currencyAppliedDate= recoproject.Cost_estimate_approval_date__c;
            } 
            /* code modifcation done for currency upload --End--Koteswarar*/ 
             if(currencyAppliedDate != null)          
            currencyYears .add(String.valueOf(currencyAppliedDate.year()));
            system.debug('currencyAppliedDate@@'+currencyAppliedDate);            
            itemCurrencies.add(obj.Currency__c);
        }
        //before calling populate currency,add current year if creationDate currency is not available in currency object,consider the current year  
        currencyYears .add(String.valueOf(SYSTEM.today().year()));
        CurrencyConverter.PopulateCurrencyConversion(currencyYears , itemCurrencies);
        
        for(Estimate_Item__c obj : newList){
            /* code modifcation done for currency upload --start--Koteswarar*/
            RECO_Project__c  recoproject=offersmap.get(obj.RECO_Project__c);
            
            currencyAppliedDate= recoproject.OpeningFormula__c != null ? recoproject.OpeningFormula__c : SYSTEM.Today();
            system.debug('currencyAppliedDate@@'+currencyAppliedDate+'RECO_Project__r.Cost_estimate_approval_status__c@@'+recoproject.Cost_estimate_approval_status__c);
            if(recoproject.Cost_estimate_approval_status__c!= null && recoproject.Cost_estimate_approval_status__c !='' &&recoproject.Cost_estimate_approval_status__c=='Approved'){
                currencyAppliedDate= recoproject.Cost_estimate_approval_date__c != null ?recoproject.Cost_estimate_approval_date__c :SYSTEM.Today();
            system.debug('currencyAppliedDate@@'+currencyAppliedDate);
            }        
         /* code modifcation done for currency upload --end--Koteswarar*/    
            if(!obj.Do_Not_Convert__c && obj.Amount__c!=null){
                //INITIALIZED TO 0 TO MAKE SURE THAT CURRENCY CONVERSION IS GOING TO WORK
                obj.Amount_CHF__c = 0;
                obj.Amount_EUR__c = 0;
                obj.Amount_local__c = 0;
                
                obj.Amount_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Amount__c), obj.Currency__c, 'CHF', currencyAppliedDate);
                obj.Amount_EUR__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Amount__c), obj.Currency__c, 'EUR', currencyAppliedDate);
                obj.Amount_local__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Amount__c), obj.Currency__c, obj.Project_Currency__c, currencyAppliedDate);
                
             }
        }
    }
    
    
  

    //PERFORMS ROLL UPS ON ALL ESTIMATE ITEMS ASSOCIATED TO THE PARENT BOUTIQUE PROJECT
    //AND STORES THE SUMMARY TO THE BUDGET OVERVIEW OBJECT OF THE SAME PARENT
   public static void RollUpEstimateItems(List<Estimate_Item__c> newList){
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        Set<Id> parentIds = new Set<Id>();
        for(Estimate_Item__c ei : newList){
            parentIds.add(ei.RECO_Project__c);
        }
        //Added by Himanshu Palerwal 
        Map<Id,Map<String,Boolean>> eItemMap=new Map<Id,Map<String,Boolean>>();
        Map<Id,Map<String,Double>> mapLocalEstimates = new Map<Id,Map<String,Double>>();
        Map<Id,Map<String,Double>> mapCHFEstimates = new Map<Id,Map<String,Double>>();
        
        Map<Id,Map<String,Boolean>> eItemMapPrivate=new Map<Id,Map<String,Boolean>>();
        Map<Id,Map<String,Double>> mapLocalEstimatesPrivate = new Map<Id,Map<String,Double>>();
        Map<Id,Map<String,Double>> mapCHFEstimatesPrivate = new Map<Id,Map<String,Double>>();
        
        for(Estimate_Item__c ei : [SELECT RECO_Project__c,private__c, Amount_local__c, Amount_CHF__c, Level_1__c FROM Estimate_Item__c WHERE RECO_Project__c IN :parentIds AND Amount_local__c!=NULL]){
            
              if((ei.private__c==true || ei.private__c==false)){
	              //CALCULATES THE SUM OF ALL ESTIMATED COSTS IN THE LOCAL CURRENCY
	              if(mapLocalEstimates.containsKey(ei.RECO_Project__c) && mapLocalEstimates.get(ei.RECO_Project__c).containsKey(ei.Level_1__c)){
	                  mapLocalEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, mapLocalEstimates.get(ei.RECO_Project__c).get(ei.Level_1__c) + ei.Amount_local__c);
	                  mapLocalEstimates.get(ei.RECO_Project__c).put('Total', mapLocalEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_local__c);
	                  eItemMap.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
	              }
	              else if(mapLocalEstimates.containsKey(ei.RECO_Project__c)){
	                  mapLocalEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_local__c);
	                  mapLocalEstimates.get(ei.RECO_Project__c).put('Total', mapLocalEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_local__c);
	                  eItemMap.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
	              }
	              else{
	                  mapLocalEstimates.put(ei.RECO_Project__c, new Map<String,Double>());
	                  mapLocalEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_local__c);
	                  mapLocalEstimates.get(ei.RECO_Project__c).put('Total', ei.Amount_local__c);
	                  eItemMap.put(ei.RECO_Project__c, new Map<String,Boolean>());
	                  eItemMap.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
	              }
	              
	              //CALCULATES THE SUM OF ALL ESTIMATED COSTS IN CHF
	              if(mapCHFEstimates.containsKey(ei.RECO_Project__c) && mapCHFEstimates.get(ei.RECO_Project__c).containsKey(ei.Level_1__c)){
	                  mapCHFEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, mapCHFEstimates.get(ei.RECO_Project__c).get(ei.Level_1__c) + ei.Amount_CHF__c);
	                  mapCHFEstimates.get(ei.RECO_Project__c).put('Total', mapCHFEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_CHF__c);
	              }
	              else if(mapCHFEstimates.containsKey(ei.RECO_Project__c)){
	                  mapCHFEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_CHF__c);
	                  mapCHFEstimates.get(ei.RECO_Project__c).put('Total', mapCHFEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_CHF__c);
	              }
	              else{
	                  mapCHFEstimates.put(ei.RECO_Project__c, new Map<String,Double>());
	                  mapCHFEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_CHF__c);
	                  mapCHFEstimates.get(ei.RECO_Project__c).put('Total', ei.Amount_CHF__c);
	              }
              }
            
              	if(ei.private__c!=true){
	              	  if(mapLocalEstimatesPrivate.containsKey(ei.RECO_Project__c) && mapLocalEstimatesPrivate.get(ei.RECO_Project__c).containsKey(ei.Level_1__c)){
		                  mapLocalEstimatesPrivate.get(ei.RECO_Project__c).put(ei.Level_1__c, mapLocalEstimatesPrivate.get(ei.RECO_Project__c).get(ei.Level_1__c) + ei.Amount_local__c);
		                  mapLocalEstimatesPrivate.get(ei.RECO_Project__c).put('Total', mapLocalEstimatesPrivate.get(ei.RECO_Project__c).get('Total') + ei.Amount_local__c);
		                  eItemMapPrivate.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
		              }
		              else if(mapLocalEstimatesPrivate.containsKey(ei.RECO_Project__c)){
		                  mapLocalEstimatesPrivate.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_local__c);
		                  mapLocalEstimatesPrivate.get(ei.RECO_Project__c).put('Total', mapLocalEstimatesPrivate.get(ei.RECO_Project__c).get('Total') + ei.Amount_local__c);
		                  eItemMapPrivate.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
		              }
		              else{
		                  mapLocalEstimatesPrivate.put(ei.RECO_Project__c, new Map<String,Double>());
		                  mapLocalEstimatesPrivate.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_local__c);
		                  mapLocalEstimatesPrivate.get(ei.RECO_Project__c).put('Total', ei.Amount_local__c);
		                  eItemMapPrivate.put(ei.RECO_Project__c, new Map<String,Boolean>());
		                  eItemMapPrivate.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
		              }
		              
		              //CALCULATES THE SUM OF ALL ESTIMATED COSTS IN CHF
		              if(mapCHFEstimatesPrivate.containsKey(ei.RECO_Project__c) && mapCHFEstimatesPrivate.get(ei.RECO_Project__c).containsKey(ei.Level_1__c)){
		                  mapCHFEstimatesPrivate.get(ei.RECO_Project__c).put(ei.Level_1__c, mapCHFEstimatesPrivate.get(ei.RECO_Project__c).get(ei.Level_1__c) + ei.Amount_CHF__c);
		                  mapCHFEstimatesPrivate.get(ei.RECO_Project__c).put('Total', mapCHFEstimatesPrivate.get(ei.RECO_Project__c).get('Total') + ei.Amount_CHF__c);
		              }
		              else if(mapCHFEstimatesPrivate.containsKey(ei.RECO_Project__c)){
		                  mapCHFEstimatesPrivate.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_CHF__c);
		                  mapCHFEstimatesPrivate.get(ei.RECO_Project__c).put('Total', mapCHFEstimatesPrivate.get(ei.RECO_Project__c).get('Total') + ei.Amount_CHF__c);
		              }
		              else{
		                  mapCHFEstimatesPrivate.put(ei.RECO_Project__c, new Map<String,Double>());
		                  mapCHFEstimatesPrivate.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_CHF__c);
		                  mapCHFEstimatesPrivate.get(ei.RECO_Project__c).put('Total', ei.Amount_CHF__c);
		              }
	              }
              
           
        }
                
        List<Budget_Overview__c> upsertOverviews = new List<Budget_Overview__c>();
        for(Budget_Overview__c bo : [SELECT Boutique_Project__c,private__c, Budget_Item__c, Estimate_local__c, Estimate_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c IN :parentIds]){
            bo.Estimate_local__c = 0;
            if(mapLocalEstimates.containsKey(bo.Boutique_Project__c) && mapLocalEstimates.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c)){
                bo.Estimate_local__c = mapLocalEstimates.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
                system.debug('=====boovaloo===='+eItemMap.get(bo.Boutique_Project__c).get(bo.Budget_Item__c));
                if(eItemMap.get(bo.Boutique_Project__c).get(bo.Budget_Item__c)==true){
                  bo.private__c=true;
                }
                else{
                  bo.private__c=false;
                }
                system.debug('=====boooo===='+bo.id);
            }
                
            bo.Estimate_CHF__c = 0;
            if(mapCHFEstimates.containsKey(bo.Boutique_Project__c) && mapCHFEstimates.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c)){
                bo.Estimate_CHF__c = mapCHFEstimates.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
                if(eItemMap.get(bo.Boutique_Project__c).get(bo.Budget_Item__c)==true){
                  bo.private__c=true;
                }
                else{
                  bo.private__c=false;
                }
                 system.debug('=====boooo=1==='+bo.id);
            }
            
            upsertOverviews.add(bo);
        }
        system.debug('=====eItemMap===='+eItemMap);
        update upsertOverviews;
        
        
        
        
        
        
        
        
        
        
        List<Budget_Overview__c> upsertOverviewsPrivate = new List<Budget_Overview__c>();
        for(Budget_Overview__c bo : [SELECT Boutique_Project__c,private__c, Budget_Item__c,Estimate_CHF_Private__c,Estimate_local_Private__c, Estimate_local__c, Estimate_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c IN :parentIds]){
            bo.Estimate_local_Private__c = 0;
            if(mapLocalEstimatesPrivate.containsKey(bo.Boutique_Project__c) && mapLocalEstimatesPrivate.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c)){
                bo.Estimate_local_Private__c = mapLocalEstimatesPrivate.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
                system.debug('=====boovaloo===='+eItemMapPrivate.get(bo.Boutique_Project__c).get(bo.Budget_Item__c));
                if(eItemMapPrivate.get(bo.Boutique_Project__c).get(bo.Budget_Item__c)==true){
                  bo.private__c=true;
                }
                else{
                  bo.private__c=false;
                }
                system.debug('=====boooo===='+bo.id);
            }
                
            bo.Estimate_CHF_Private__c = 0;
            if(mapCHFEstimatesPrivate.containsKey(bo.Boutique_Project__c) && mapCHFEstimatesPrivate.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c)){
                bo.Estimate_CHF_Private__c = mapCHFEstimatesPrivate.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
                if(eItemMapPrivate.get(bo.Boutique_Project__c).get(bo.Budget_Item__c)==true){
                  bo.private__c=true;
                }
                else{
                  bo.private__c=false;
                }
                 system.debug('=====boooo=1==='+bo.id);
            }
            
            upsertOverviewsPrivate.add(bo);
        }
        system.debug('=====eItemMapPrivate===='+eItemMap);
        update upsertOverviewsPrivate;
        
        
        
    }
    
    
    
    
     public static void PrivateRollUpEstimateItems(List<Estimate_Item__c> newList){ 
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        Set<Id> parentIds = new Set<Id>();
        for(Estimate_Item__c ei : newList){
            parentIds.add(ei.RECO_Project__c);
        }
        //Added by Himanshu Palerwal 
       Map<Id,Map<String,Boolean>> eItemMap=new Map<Id,Map<String,Boolean>>();
       
        Map<Id,Map<String,Double>> mapLocalEstimates = new Map<Id,Map<String,Double>>();
        Map<Id,Map<String,Double>> mapCHFEstimates = new Map<Id,Map<String,Double>>();
        
        for(Estimate_Item__c ei : [SELECT RECO_Project__c,private__c, Amount_local__c, Amount_CHF__c, Level_1__c FROM Estimate_Item__c WHERE RECO_Project__c IN :parentIds AND Amount_local__c!=NULL]){
            
           // if((profileName!=Label.Design_Architect_Profile && profileName!=Label.Furniture_Supplier_Profile && profileName!=Label.Manager_Architect_Profile )){
              //CALCULATES THE SUM OF ALL ESTIMATED COSTS IN THE LOCAL CURRENCY
              if(mapLocalEstimates.containsKey(ei.RECO_Project__c) && mapLocalEstimates.get(ei.RECO_Project__c).containsKey(ei.Level_1__c)){
                  mapLocalEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, mapLocalEstimates.get(ei.RECO_Project__c).get(ei.Level_1__c) + ei.Amount_local__c);
                  mapLocalEstimates.get(ei.RECO_Project__c).put('Total', mapLocalEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_local__c);
                  eItemMap.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
              }
              else if(mapLocalEstimates.containsKey(ei.RECO_Project__c)){
                  mapLocalEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_local__c);
                  mapLocalEstimates.get(ei.RECO_Project__c).put('Total', mapLocalEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_local__c);
                  eItemMap.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
              }
              else{
                  mapLocalEstimates.put(ei.RECO_Project__c, new Map<String,Double>());
                  mapLocalEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_local__c);
                  mapLocalEstimates.get(ei.RECO_Project__c).put('Total', ei.Amount_local__c);
                  eItemMap.put(ei.RECO_Project__c, new Map<String,Boolean>());
                  eItemMap.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
              }
              
              //CALCULATES THE SUM OF ALL ESTIMATED COSTS IN CHF
              if(mapCHFEstimates.containsKey(ei.RECO_Project__c) && mapCHFEstimates.get(ei.RECO_Project__c).containsKey(ei.Level_1__c)){
                  mapCHFEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, mapCHFEstimates.get(ei.RECO_Project__c).get(ei.Level_1__c) + ei.Amount_CHF__c);
                  mapCHFEstimates.get(ei.RECO_Project__c).put('Total', mapCHFEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_CHF__c);
              }
              else if(mapCHFEstimates.containsKey(ei.RECO_Project__c)){
                  mapCHFEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_CHF__c);
                  mapCHFEstimates.get(ei.RECO_Project__c).put('Total', mapCHFEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_CHF__c);
              }
              else{
                  mapCHFEstimates.put(ei.RECO_Project__c, new Map<String,Double>());
                  mapCHFEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_CHF__c);
                  mapCHFEstimates.get(ei.RECO_Project__c).put('Total', ei.Amount_CHF__c);
              }
           // }
            /*else{ 
                     if(ei.private__c!=true && (profileName==Label.Design_Architect_Profile || profileName==Label.Furniture_Supplier_Profile || profileName==Label.Manager_Architect_Profile )){
                  //CALCULATES THE SUM OF ALL ESTIMATED COSTS IN THE LOCAL CURRENCY
                  if(mapLocalEstimates.containsKey(ei.RECO_Project__c) && mapLocalEstimates.get(ei.RECO_Project__c).containsKey(ei.Level_1__c)){
                      mapLocalEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, mapLocalEstimates.get(ei.RECO_Project__c).get(ei.Level_1__c) + ei.Amount_local__c);
                      mapLocalEstimates.get(ei.RECO_Project__c).put('Total', mapLocalEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_local__c);
                      eItemMap.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
                  }
                  else if(mapLocalEstimates.containsKey(ei.RECO_Project__c)){
                      mapLocalEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_local__c);
                      mapLocalEstimates.get(ei.RECO_Project__c).put('Total', mapLocalEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_local__c);
                      eItemMap.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
                  }
                  else{
                      mapLocalEstimates.put(ei.RECO_Project__c, new Map<String,Double>());
                      mapLocalEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_local__c);
                      mapLocalEstimates.get(ei.RECO_Project__c).put('Total', ei.Amount_local__c);
                      eItemMap.put(ei.RECO_Project__c, new Map<String,Boolean>());
                      eItemMap.get(ei.RECO_Project__c).put(ei.Level_1__c,ei.private__c);
                  }
                  
                  //CALCULATES THE SUM OF ALL ESTIMATED COSTS IN CHF
                  if(mapCHFEstimates.containsKey(ei.RECO_Project__c) && mapCHFEstimates.get(ei.RECO_Project__c).containsKey(ei.Level_1__c)){
                      mapCHFEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, mapCHFEstimates.get(ei.RECO_Project__c).get(ei.Level_1__c) + ei.Amount_CHF__c);
                      mapCHFEstimates.get(ei.RECO_Project__c).put('Total', mapCHFEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_CHF__c);
                  }
                  else if(mapCHFEstimates.containsKey(ei.RECO_Project__c)){
                      mapCHFEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_CHF__c);
                      mapCHFEstimates.get(ei.RECO_Project__c).put('Total', mapCHFEstimates.get(ei.RECO_Project__c).get('Total') + ei.Amount_CHF__c);
                  }
                  else{
                      mapCHFEstimates.put(ei.RECO_Project__c, new Map<String,Double>());
                      mapCHFEstimates.get(ei.RECO_Project__c).put(ei.Level_1__c, ei.Amount_CHF__c);
                      mapCHFEstimates.get(ei.RECO_Project__c).put('Total', ei.Amount_CHF__c);
                  }
                }
                    
            }*/
        }
                
        List<Budget_Overview__c> upsertOverviews = new List<Budget_Overview__c>();
        for(Budget_Overview__c bo : [SELECT Boutique_Project__c,private__c, Budget_Item__c, Estimate_local__c, Estimate_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c IN :parentIds]){
            bo.Estimate_local__c = 0;
            if(mapLocalEstimates.containsKey(bo.Boutique_Project__c) && mapLocalEstimates.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c)){
                bo.Estimate_local__c = mapLocalEstimates.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
                system.debug('=====boovaloo===='+eItemMap.get(bo.Boutique_Project__c).get(bo.Budget_Item__c));
                if(eItemMap.get(bo.Boutique_Project__c).get(bo.Budget_Item__c)==true){
                  bo.private__c=true;
                }
                else{
                  bo.private__c=false;
                }
                system.debug('=====boooo===='+bo.id);
            }
                
            bo.Estimate_CHF__c = 0;
            if(mapCHFEstimates.containsKey(bo.Boutique_Project__c) && mapCHFEstimates.get(bo.Boutique_Project__c).containsKey(bo.Budget_Item__c)){
                bo.Estimate_CHF__c = mapCHFEstimates.get(bo.Boutique_Project__c).get(bo.Budget_Item__c);
                if(eItemMap.get(bo.Boutique_Project__c).get(bo.Budget_Item__c)==true){
                  bo.private__c=true;
                }
                else{
                  bo.private__c=false;
                }
                 system.debug('=====boooo=1==='+bo.id);
            }
            
            upsertOverviews.add(bo);
        }
        system.debug('=====eItemMap===='+eItemMap);
        update upsertOverviews;
    }
    
    
    
    public static void ValidateCurrencyConversion(List<Estimate_Item__c> newList){
        String objIds = '';
        for(Estimate_Item__c obj : newList){
            if(obj.Amount__c!=null && (obj.Amount_local__c==null || obj.Amount_EUR__c==null || obj.Amount_CHF__c==null || obj.Amount_local__c==0 || obj.Amount_EUR__c==0 || obj.Amount_CHF__c==0)){
                objIds = objIds + '\n' + obj.Id + ': ';
                if(obj.Amount_local__c==null || obj.Amount_local__c==0)
                    objIds = objIds + obj.Currency__c + '>' + obj.Project_Currency__c + ', ';
                if(obj.Amount_EUR__c==null || obj.Amount_EUR__c==0)
                    objIds = objIds + obj.Currency__c + '>EUR' + ', ';
                if(obj.Amount_CHF__c==null || obj.Amount_CHF__c==0)
                    objIds = objIds + obj.Currency__c + '>CHF' + ', ';
            }
        }
        
        if(objIds!=''){
            Task t = new Task();
            t.OwnerId = '005b0000001IuX8';
            t.Subject = 'Check Currency Conversion for Estimate Items';
            t.Status = 'Not Started';
            t.Priority = 'High';
            t.Description = objIds;
            t.ActivityDate = SYSTEM.today();
            
            Database.DMLOptions dmlOpt = new Database.DMLOptions(); 
            dmlOpt.EmailHeader.TriggerUserEmail = true; 
            Database.Insert(t,dmlOpt);
            
            /*Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'jacqueline.uy@sword-group.com'};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('jacqueline.uy@sword-group.com');
            mail.setSubject('Salesforce: Currency Conversion Failed');
            mail.setPlainTextBody('Estimate Item records that did not convert properly: ' + objIds);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});*/
        }
    }
}
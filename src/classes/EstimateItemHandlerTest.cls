/*****
*   @Class      :   EstimateItemHandlerTest.cls
*   @Description:   Test coverage for the EstimateItemTriggers.trigger/EstimateItemHandler.cls
*   @Author     :   Jacky Uy
*   @Created    :   28 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        28 APR 2014     EstimateItemTriggers.trigger Test Coverage at 100%
*   Jacky Uy        28 APR 2014     EstimateItemHandler.cls Test Coverage at 92%
*   Jacky Uy        28 APR 2014     CurrencyConverter.cls Test Coverage at 93%
*   Jacky Uy        02 MAY 2014     EstimateItemTriggers.trigger Test Coverage at 85%
*   Jacky Uy        13 MAY 2014     Change: BTQ Project Record Type from 'New' to 'Boutique Project'
*   Jacky Uy        06 JUN 2014     EstimateItemHandler.cls Test Coverage at 93%
*   Jacky Uy        06 JUN 2014     EstimateItemTriggers.trigger Test Coverage at 100%
*   Jacky Uy        02 JUL 2014     EstimateItemHandler.cls Test Coverage at 95%
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*****/

@isTest
private with sharing class EstimateItemHandlerTest{
    static testMethod void unitTest1(){
        Id rTypeId;
        RECO_Project__c bProj;
        List<Estimate_Item__c> newEstItems;
        List<Estimate_Item__c> updEstItems;
        
        SYSTEM.runAs(TestHelper.CurrentUser){
            TestHelper.createCurrencies();
            Market__c market = TestHelper.createMarket();
            insert market;
            
            rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
            Account acc = TestHelper.createAccount(rTypeId, market.Id);
            insert acc;
        
            rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
            Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
            insert btq;
            
            rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
            bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
            bProj.Currency__c = 'EUR';
            insert bProj;
            
            rTypeId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Pending approval').getRecordTypeId();
            newEstItems = TestHelper.createEstimateItems(rTypeId, bProj.Id);
            rTypeId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
            newEstItems.addAll(TestHelper.createEstimateItems(rTypeId, bProj.Id));
            insert newEstItems;
            
            Double totalEstAmtLocal = 0;
            Double totalEstAmtCHF = 0;
            updEstItems = new List<Estimate_Item__c>();
            
            for(Estimate_Item__c ei : [SELECT Amount__c, Amount_local__c, Amount_CHF__c FROM Estimate_Item__c WHERE Id IN :newEstItems]){
                //TEST convertEstimateItemCurrency() ON INSERT
               // SYSTEM.Assert(ei.Amount_CHF__c == (ei.Amount__c * 1.50));
    
                totalEstAmtLocal = totalEstAmtLocal + ei.Amount_local__c;
                totalEstAmtCHF = totalEstAmtCHF + ei.Amount_CHF__c;
                
                //ei.Amount_local__c = 2000;
                updEstItems.add(ei);
            }
            
            Budget_Overview__c bo = [
                SELECT Estimate_local__c, Estimate_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c = :bProj.Id AND Budget_Item__c = 'Fees'
            ];
            
            //TEST RollUpEstimateItems ON INSERT
           // SYSTEM.AssertEquals(totalEstAmtLocal, bo.Estimate_local__c);
            //SYSTEM.AssertEquals(totalEstAmtCHF, bo.Estimate_CHF__c);
    
            update updEstItems;
            totalEstAmtLocal = 0;
            totalEstAmtCHF = 0;
            
            for(Estimate_Item__c ei : [SELECT Amount__c, Amount_local__c, Amount_CHF__c FROM Estimate_Item__c WHERE Id IN :newEstItems]){
                //TEST convertEstimateItemCurrency() ON UPDATE
               // SYSTEM.Assert(ei.Amount_CHF__c == (ei.Amount__c * 1.50));
    
                totalEstAmtLocal = totalEstAmtLocal + ei.Amount_local__c;
                totalEstAmtCHF = totalEstAmtCHF + ei.Amount_CHF__c;
            }
            
            bo = [SELECT Estimate_local__c, Estimate_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c = :bProj.Id AND Budget_Item__c = 'Fees'];
            
            //TEST RollUpEstimateItems ON UPDATE
            SYSTEM.AssertEquals(totalEstAmtLocal, bo.Estimate_local__c);
            SYSTEM.AssertEquals(totalEstAmtCHF, bo.Estimate_CHF__c);
            
            delete updEstItems[0];
            totalEstAmtLocal = 0;
            totalEstAmtCHF = 0;
            
            for(Estimate_Item__c ei : [SELECT Amount__c, Amount_local__c, Amount_CHF__c FROM Estimate_Item__c WHERE Id IN :newEstItems]){
                totalEstAmtLocal = totalEstAmtLocal + ei.Amount_local__c;
                totalEstAmtCHF = totalEstAmtCHF + ei.Amount_CHF__c;
            }
            
            bo = [SELECT Estimate_local__c, Estimate_CHF__c FROM Budget_Overview__c WHERE Boutique_Project__c = :bProj.Id AND Budget_Item__c = 'Fees'];
            
            //TEST RollUpEstimateItems ON DELETE
            SYSTEM.AssertEquals(totalEstAmtLocal, bo.Estimate_local__c);
            SYSTEM.AssertEquals(totalEstAmtCHF, bo.Estimate_CHF__c);
            
            bProj = [SELECT Cost_estimate_approval_status__c FROM RECO_Project__c WHERE Id = :bProj.Id];
            bProj.Cost_estimate_approval_status__c = 'Approved';
            update bProj;
        }
        
        SYSTEM.runAs(TestHelper.RetailProjectManager){
            try{
                newEstItems = TestHelper.createEstimateItems(rTypeId, bProj.Id);
                insert newEstItems;
            }
            catch(Exception e){
                //TEST StopUpsertOfEstimatesAfterValidation() ON INSERT
                //SYSTEM.Assert(e.getMessage().contains('The estimated cost form for this project is frozen. Modifying or adding new items is not authorized.'));
            }
            
            try{
                List<Estimate_Item__c> estItems = [SELECT Id FROM Estimate_Item__c WHERE RECO_Project__c = :bProj.Id];
                update estItems;
            }
            catch(Exception e){
                //TEST StopUpsertOfEstimatesAfterValidation() ON UPDATE
                //SYSTEM.Assert(e.getMessage().contains('The estimated cost form for this project is frozen. Modifying or adding new items is not authorized.'));
            }
        }
    }
    
    //TESTING ValidateCurrencyConversion()
    static testMethod void unitTest2(){
        Id rTypeId;
        
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'EUR';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Pending approval').getRecordTypeId();
        List<Estimate_Item__c> newEstItems = TestHelper.createEstimateItems(rTypeId, bProj.Id);
        newEstItems[0].Do_Not_Convert__c = true;
        insert newEstItems;
        
        SYSTEM.AssertEquals(1, [SELECT Id FROM Task WHERE Subject = 'Check Currency Conversion for Estimate Items'].size());
    }
}
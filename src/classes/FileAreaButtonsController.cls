/*****
*   @Class      :   FileAreaButtonsController.cls
*   @Description:   Controller for the File Area buttons.
*   @Author     :   XXXXXXXXX
*   @Created    :   
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Thibauld        28 APR 2014     Add: installationInformationProvided call for Workflow 3.0
*   Thibauld        30 APR 2014     Add: new buttons for the Agent workflow 
*   Thibauld        05 MAY 2014     Add: Back to Handover
*   Thibauld        26 MAY 2014     Add: On Hold behavior
*   Jacky Uy        29 JUL 2014     Transformed WF Field Updates to Apex Code
*
***/

public with sharing class FileAreaButtonsController {
    public class CustomException extends Exception{}
    
    private String LAYOUTPROPOSALSUBMITTEDSUCCESS_SALESPROMOTER = 'Thank you, your layout proposal has been transferred to your NES Project Manager for selection and approval.\\nIn the case of non-standard corner, it will also be sent to Nespresso Trade (HQ).';
    private String LAYOUTPROPOSALSUBMITTEDSUCCESS_AGENT = 'Thank you, your layout proposal has been transferred to your Business Development Manager for selection and approval.\\nIn the case of non-standard corner, it will also be sent to Nespresso Trade (HQ).';
    
    private String LAYOUTPROPOSALHQAPPROVESSUCCESS_SALESPROMOTER = 'Thank you! The decision will sent to the NES Project Manager.'; 
    private String LAYOUTPROPOSALHQAPPROVESSUCCESS_AGENT = 'Thank you! The decision will sent to the Business Development Manager.';
    
    public Project_File_Area__c file {get; set;}
    public String currentUserProfile;
    public String layoutProposalSubmittedSuccess {get; set;}
    public String layoutProposalHQApprovedSuccess {get; set;}
    
    private Boolean IsProjectOnHold{
        get{
            if (IsProjectOnHold == null){
                IsProjectOnHold = (file.Project__r.Status__c == CornerProjectButtonsController.ONHOLDSTATUS);
            }
            return IsProjectOnHold;
        }
        set;
    }
  
    private void InitMessages(){
        if (file.Project__r.Workflow_Type__c == 'Agent'){
            layoutProposalSubmittedSuccess = LAYOUTPROPOSALSUBMITTEDSUCCESS_AGENT;
            layoutProposalHQApprovedSuccess = LAYOUTPROPOSALHQAPPROVESSUCCESS_AGENT;
        } else if (file.Project__r.Workflow_Type__c == 'Sales Promoter'){
            layoutProposalSubmittedSuccess = LAYOUTPROPOSALSUBMITTEDSUCCESS_SALESPROMOTER ;
            layoutProposalHQApprovedSuccess = LAYOUTPROPOSALHQAPPROVESSUCCESS_SALESPROMOTER ;
        }
    }
  
    private List<RecordType> fileAreaRTList{
        get{
            if (fileAreaRTList == null)
                fileAreaRTList = [SELECT Id, DeveloperName, Name FROM RecordType WHERE SobjectType = 'Project_File_Area__c'];
            return fileAreaRTList;
        }
        set;
    }
       
    private RecordType getRecordType(String developerName){
        for(RecordType rt : fileAreaRTList){
            if (rt.DeveloperName == developerName)
                return rt;
        }
        return null;
    }
    
    public Boolean showSendExecutiveDrawingsToSupplier {
        get {
            String hasAccessRights = 'NES Corner Architect,System Administrator,System Admin';
            RecordType execDrawingsRT = getRecordType('Executive_Drawings');
            
            return (!IsProjectOnHold && file.File_sent_date__c == null && file.RecordTypeId == execDrawingsRT.Id && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    
    public Boolean allowSendExecutiveDrawingsToSupplier { 
        get{
            List<Attachment> tmpListAtt = [SELECT Id FROM Attachment WHERE ParentID = :file.Id];
            List<Furnitures_Inventory__c> tmpListInv = [SELECT Id FROM Furnitures_Inventory__c WHERE Project__c = :file.Project__c];
            Project__c tmpProj = [SELECT Notify_Installation_Info__c FROM Project__c WHERE Id = :file.Project__c];
            
            return !(tmpProj.Notify_Installation_Info__c == false || tmpListAtt.isEmpty() || tmpListInv.isEmpty());
        } set;
    }

    public Boolean showSendDrawingsForApproval {
        get {
            String hasAccessRights = 'NES Corner Supplier,System Administrator,System Admin';
            RecordType supDrawingsRT = getRecordType('Supplier_Drawings');
            
            return (!IsProjectOnHold && file.File_sent_date__c == null && file.RecordTypeId == supDrawingsRT.Id && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    public Boolean allowSendDrawingsForApproval { 
        get{
            List<Attachment> tmpListAtt = [SELECT Id FROM Attachment WHERE ParentID = :file.Id];
            
            return !(tmpListAtt.isEmpty());
        } set;
    }
    
    public Boolean showSendDecisionToSupplier {
        get {
            String hasAccessRights = 'NES Corner Architect,System Administrator,System Admin';
            
            return (!IsProjectOnHold && file.Approval_Status__c == 'Waiting for architect approval' && file.Architect_Decision_Sent_Date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    
    
    public Boolean allowSendDecisionToSupplier { 
        get{
            return ((file.Approval_Status__c == 'Waiting for architect approval' && (file.Architect_Decision__c == 'Approved' || (file.Architect_Decision__c == 'Disapproved' && file.Architect_Comments__c != null)))
                    || (file.Approval_Status__c == 'Waiting for approval NES' && file.NES_Decision__c != null));
        } set;
    }
    
    public Boolean showSendLayoutForApproval {
        get {
            String hasAccessRights = 'NES Corner Architect,System Administrator,System Admin';
            RecordType layoutProposalRT = getRecordType('Layout_Proposal');
            
            return (!IsProjectOnHold && file.File_sent_date__c == null && file.RecordTypeID == layoutProposalRT.Id && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    
    public Boolean showBackToHandoverReport{
        get { 
            System.debug('************ file.RecordType.DeveloperName' + file.RecordType.DeveloperName);
            return (file.RecordType.DeveloperName == 'Handover_Photo' || file.RecordType.DeveloperName == 'Handover_Report_signed');
        }
    }
    
    public Boolean isDataValidated{
        get{
            if (isDataValidated == null)
                //Data must have been validated at the project level
                isDataValidated = [Select Notify_Data_Validated__c from Project__c where Id=:file.Project__c].Notify_Data_Validated__c;
            return isDataValidated;
        }
        set;
    }
    
    public Boolean allowSendLayoutForApproval { 
        get{
            // Min two layout proposals
            
            List<Attachment> tmpListAtt = [SELECT Id FROM Attachment WHERE ParentID = :file.Id];
            
            return (isDataValidated && !tmpListAtt.isEmpty());
        } set;
    }
    
    public Boolean showSendDecisionToMarket {
        get {
            String hasAccessRights = 'NES Trade/Proc Manager,System Administrator,System Admin';
            
            return (!IsProjectOnHold && file.Approval_Status__c == 'Waiting for approval HQ' && file.HQ_Dis_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    public Boolean allowSendDecisionToMarket { 
        get{
            return !(file.HQ_Decision__c == null || (file.HQ_Decision__c == 'Disapproved' && file.HQ_Reason__c == null));
        } set;
    }
    
    public Boolean showSendDecisionToArchitect {
        get {
            String hasAccessRights = 'NES Market Local Manager,System Administrator,System Admin';
            Boolean isNewCorner = ([Select count() from Project__c where Id=:file.Project__c and (RecordType.DeveloperName = 'New_Corner' or RecordType.DeveloperName = 'New_Mass_Corner')] ==1);
            return (!IsProjectOnHold && isNewCorner && file.Approval_Status__c == 'Waiting for approval NES' && file.NES_Dis_or_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    
    public Boolean allowSendDecisionToArchitect { 
        get{
            return !(file.NES_Decision__c == null || (file.NES_Decision__c == 'Disapproved' && file.NES_Reason__c == null));
        } set;
    }
    
    public Boolean showSendDecisionFromAgentToArchitect {
        get {
            String hasAccessRights = 'Agent Local Manager,System Administrator,System Admin';
            Boolean isNewCornerFromAgent = ([Select count() from Project__c where Id=:file.Project__c and (RecordType.DeveloperName = 'New_Corner_Agent' or RecordType.DeveloperName = 'New_Mass_Corner_Agent')] ==1);
            return (!IsProjectOnHold && isNewCornerFromAgent && file.Approval_Status__c == 'Waiting for approval Agent' && file.Agent_Dis_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    
    public Boolean allowSendDecisionFromAgentToArchitect { 
        get{
            return !(file.Agent_Decision__c == null || (file.Agent_Decision__c == 'Disapproved' && file.Agent_Reason__c == null));
        } set;
    }
    
    public Boolean showSendDecisionFromBDM {
        get {
            String hasAccessRights = 'NES Market Business Development Manager,System Administrator,System Admin';
            Boolean isNewCorner = ([Select count() from Project__c where Id=:file.Project__c and (RecordType.DeveloperName = 'New_Corner_Agent' or RecordType.DeveloperName = 'New_Mass_Corner_Agent')] ==1);
            return (!IsProjectOnHold && isNewCorner && file.Approval_Status__c == 'Waiting for approval NES' && file.NES_Dis_or_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    public Boolean allowSendDecisionFromBDM { 
        get{
            return (file.NES_Decision__c != null && (file.NES_Decision__c == 'Approved' || (file.NES_Decision__c == 'Disapproved' && file.NES_Reason__c != null)));
        } set;
    }

    public Boolean installationInformationProvided{
        get{
            return CornerProjectHandler.InstallationInformationPopulated(file.Project__c);
        } set;
    }
    
    public Boolean isMasscorner{
         get{
            return file.Project__r.isMassCorner__c;
        } set;
      }  
    public FileAreaButtonsController(ApexPages.StandardController controller) {
        currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        
        file = (Project_File_Area__c)controller.getRecord();
        file = [SELECT Id, RecordTypeId, Project__c,Project__r.isMassCorner__C, Send_file_for_approval__c, File_sent_date__c, Approval_Status__c, Architect_Decision_Sent_Date__c, 
                        Architect_Decision__c, Architect_Dis_Approval__c, Architect_Comments__c, Standardization__c, NES_Dis_or_Approval_date__c, NES_Decision__c, NES_Dis_Approved__c, 
                        HQ_Dis_Approval_date__c, HQ_Dis_Approved__c, HQ_Decision__c, HQ_Reason__c, NES_Reason__c, RecordType.DeveloperName,
                        Agent_Dis_Approval_date__c, Agent_Decision__c, Agent_Reason__c, Handover_Report__c, Project__r.Workflow_Type__c, 
                        Project__r.Status__c, Project__r.RecordTypeId, Project__r.Approval_Status__c, Project__r.Notify_Layout_Proposal__c, Project__r.Notify_Date_Layout_Proposal__c
                        FROM Project_File_Area__c WHERE Id = :file.Id];
        InitMessages();
    }
    
    private Id layoutProposalRTypeId = Schema.sObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Layout Proposal').getRecordTypeId();
    private Id execDrawingsRTypeId = Schema.sObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Executive Drawings').getRecordTypeId();
    private Id suppDrawingsRTypeId = Schema.sObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Supplier Drawings').getRecordTypeId();
    private Id newCornerInstallDateRTypeId = Schema.sObjectType.Project__c.getRecordTypeInfosByName().get('New Corner Installation Date').getRecordTypeId();
    
    //EXECUTIVE DRAWINGS
    public PageReference SendExecutiveDrawingsToSupplier () {
        if(allowSendExecutiveDrawingsToSupplier){
            file.Send_file_for_approval__c = true;
            update file;
            
            WF_DrawingsApprovedSendQuotationSupplierArchitect();
        }
            
        return redirectToId(file.Id);
    }
    
    //SUPPLIER DRAWINGS & Layout proposals
    public PageReference SendFileForApproval () {
        if(allowSendDrawingsForApproval){
            file.Send_file_for_approval__c = true;
            update file;
            
            WF_SupplierDrawingsForArchitectApproval();
        }
        if (IsDataValidated && allowSendLayoutForApproval){
            file.Send_file_for_approval__c = true;
            update file;
            
            WF_LayoutForApproval();
        }
        
        return redirectToId(file.Id);
    }
    
    public PageReference SendDecisionToSupplier () {
        if(allowSendDecisionToSupplier){
            if(file.Approval_Status__c == 'Waiting for architect approval')
                file.Architect_Dis_Approval__c = true;
            if(file.Approval_Status__c == 'Waiting for approval NES')
                file.NES_Dis_Approved__c = true;
            update file;
            
            WF_DrawingsApprovedByArchitect();
        }
        
        return redirectToId(file.Id);
    }
    
    public PageReference SendDecisionToArchitect () {
        if(installationInformationProvided && allowSendDecisionToArchitect){
            file.NES_Dis_Approved__c  = true;
            update file;
            
            WF_LayoutApprovedByMarket();
        }
        return redirectToId(file.Id);
    }
    
    public PageReference SendDecisionFromAgentToArchitect () {
       
        if (installationInformationProvided && allowSendDecisionFromAgentToArchitect){
            file.Agent_Dis_Approved__c  = true;
            update file;
            
            WF_LayoutApprovedByMarket_Agent();
        }
        return redirectToId(file.Id);
    }
    
    public PageReference SendDecisionFromBDM () {
        if(allowSendDecisionFromBDM){
            file.NES_Dis_Approved__c  = true;
            update file;
            
            WF_LayoutApprovedByMarket();
        }
        return redirectToId(file.Id);
    }
    
    public PageReference SendDecisionToMarket () {
        if (allowSendDecisionToMarket){
            file.HQ_Dis_Approved__c  = true;
            update file;
        }
        return redirectToId(file.Id);
    }
    
    public PageReference BackToProject () {     
        return redirectToId(file.Project__c);
    }
    
    public PageReference BackToHandoverReport () {     
        return redirectToId(file.Handover_Report__c);
    }
    
    private PageReference redirectToId(Id idToRedirectTo){
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + idToRedirectTo);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
    
    private void WF_DrawingsApprovedSendQuotationSupplierArchitect(){
        //"DrawingsApprovedSendQuotationSupplier&Architect"
        //(File Area: Send file for approvalEQUALSTrue) AND 
        //(File Area: Record TypeEQUALSExecutive Drawings) AND 
        //(File Area: File sent dateEQUALSnull)
        //(File Area: Approval StatusEQUALSnull) AND
        
        if(file.Send_file_for_approval__c && file.RecordTypeId==execDrawingsRTypeId && file.File_sent_date__c==null && file.Approval_Status__c==null){
            update new Project__c(
                Id = file.Project__c,
                Approval_Status__c = 'Supplier Quotation',
                Notify_Architect_for_Quotation_Date__c = SYSTEM.Now(),
                Notify_Architect_for_Quotation__c = true,
                Notify_Supplier_for_Quotation_Date__c = SYSTEM.Now(),
                Notify_Supplier_for_Quotation__c = true
            );
        }
    }
    
    private void WF_SupplierDrawingsForArchitectApproval(){
        //"SupplierDrawingsForArchitectApproval"
        //(File Area: Send file for approvalEQUALSTrue) AND 
        //(File Area: Record TypeEQUALSSupplier Drawings) AND 
        //(File Area: File sent dateEQUALSnull)
        
        if(file.Send_file_for_approval__c && file.RecordTypeId==suppDrawingsRTypeId && file.File_sent_date__c==null){
            update new Project__c(
                Id = file.Project__c,
                Approval_Status__c = 'Supplier Drawings Approval'
            );
        }
    }
    
    private void WF_DrawingsApprovedByArchitect(){
        //"DrawingsApprovedByArchitect"
        //(File Area: Architect DecisionEQUALSApproved) AND 
        //(File Area: Architect Decision Sent DateEQUALSnull) AND 
        //(File Area: Record TypeEQUALSSupplier Drawings) AND 
        //(File Area: Architect (Dis)ApprovalEQUALSTrue)
        
        if(file.Architect_Decision__c=='Approved' && file.Architect_Decision_Sent_Date__c==null && file.RecordTypeId==suppDrawingsRTypeId && file.Architect_Dis_Approval__c){
         /*****changes done by promila starts here***********/
          string recTypeID;
          Boolean isMassCorner=[select isMassCorner__c from Project__c where Id=:file.Project__c limit 1].isMassCorner__c ;
          if(isMassCorner ==true)
          {
             recTypeID=CornerProjectHelper.MassNewCornerInstallDatetRT.Id;
          }
          else
          {
            recTypeID=newCornerInstallDateRTypeId;
          }
           /*****changes done by promila endshere***********/
            update new Project__c(
                Id = file.Project__c,
                Approval_Status__c = 'Supplier Propose Installation Date',
                Notify_Supplier_for_Installation_Date__c = SYSTEM.Now(),
                Notify_Supplier_for_Installation__c = true,
                RecordTypeId = recTypeID
            );
        }
    }
    
    private void WF_LayoutForApproval(){
        //"WF Agent: LayoutForApprovalStandard"
        //(File Area: Send file for approvalEQUALSTrue) AND 
        //(File Area: Record TypeEQUALSLayout Proposal) AND 
        //(File Area: File sent dateEQUALSnull) AND 
        //(File Area: StandardizationEQUALSStandard) AND 
        //(File Area: Approval StatusEQUALSnull) AND 
        //(Corner Project: Workflow TypeEQUALSAgent)
        
        //"WF Sales Promoter: LayoutForApprovalStandard"
        //(File Area: Send file for approvalEQUALSTrue) AND 
        //(File Area: Record TypeEQUALSLayout Proposal) AND 
        //(File Area: File sent dateEQUALSnull) AND 
        //(File Area: StandardizationEQUALSStandard) AND 
        //(File Area: Approval StatusEQUALSnull) AND 
        //(Corner Project: Workflow TypeEQUALSSales Promoter)
        //OR
        //"WF Common: LayoutForApprovalNonStandard"
        //(File Area: Send file for approvalEQUALSTrue) AND 
        //(File Area: Record TypeEQUALSLayout Proposal) AND 
        //(File Area: File sent dateEQUALSnull) AND 
        //(File Area: StandardizationEQUALSNon Standard)
        
        if(file.Send_file_for_approval__c && file.RecordTypeId==layoutProposalRTypeId && file.File_sent_date__c==null && 
            file.Standardization__c=='Standard' && file.Approval_Status__c==null && 
            (file.Project__r.Workflow_Type__c=='Sales Promoter' || file.Project__r.Workflow_Type__c=='Agent')){
            update new Project__c(
                Id = file.Project__c,
                Approval_Status__c = 'Layout Approval',
                Notify_Layout_Proposal__c = true,
                Notify_Date_Layout_Proposal__c = SYSTEM.Now(),
                Status__c = 'Execution'
            );
        }else if(file.Send_file_for_approval__c && file.RecordTypeId==layoutProposalRTypeId && file.File_sent_date__c==null && 
            file.Standardization__c=='Non Standard'){
            update new Project__c(
                Id = file.Project__c,
                Approval_Status__c = 'Layout Approval',
                Notify_Layout_Proposal__c = true,
                Notify_Date_Layout_Proposal__c = SYSTEM.Now(),
                Status__c = 'Execution'
            );
        }
    }
    
    /*private void WF_Layout_Proposal_submitted(){
        //"WF Common: Layout Proposal submitted"
        //(File Area: Send file for approvalequalsTrue) and 
        //(Corner Project: Process StepequalsLayout Approval) and 
        //(Corner Project: Notify Layout ProposalequalsFalse) and 
        //(Corner Project: Notify Date Layout Proposalequalsnull) 
        
        if(file.Send_file_for_approval__c && file.Project__r.Approval_Status__c=='Layout Approval' && !file.Project__r.Notify_Layout_Proposal__c && 
            file.Project__r.Notify_Date_Layout_Proposal__c==null){
            update new Project__c(
                Id = file.Project__c,
                Approval_Status__c = 'Layout Approval',
                Notify_Layout_Proposal__c = true,
                Notify_Date_Layout_Proposal__c = SYSTEM.Now()
            );
        }
    }*/
    
    private void WF_LayoutApprovedByMarket(){
        //"WF Sales Promoter: LayoutApprovedByMarket"
        //***(File Area: NES DecisionequalsApproved) and 
        //(File Area: NES (Dis)Approval dateequalsnull) and 
        //(File Area: Record TypeequalsLayout Proposal) and 
        //(File Area: NES (Dis)ApprovedequalsTrue) and 
        //(File Area: Standardization equals Standard,Non Standard) and 
        //***(Corner Project: Record TypeequalsNew Corner) 

        if(file.NES_Decision__c=='Approved' && file.NES_Dis_or_Approval_date__c==null && file.NES_Dis_Approved__c && 'Sales Promoter'.equals(file.Project__r.Workflow_Type__c)
            && (file.Standardization__c=='Standard' || file.Standardization__c=='Non Standard')
            ){
            update new Project__c(
                Id = file.Project__c,
                Approval_Status__c = 'Draw Technical Plan',
                Notify_Installation_Info_Date__c = SYSTEM.Now(),
                Notify_Installation_Info__c = true,
                Status__c = 'Execution'
            );
        }
    }
    
    private void WF_LayoutApprovedByMarket_Agent(){
        
        //"WF Agent: LayoutApprovedByAgent"
        //***(File Area: Agent DecisionequalsApproved) and 
        //(File Area: Agent (Dis)Approval dateequalsnull) and 
        //(File Area: Record TypeequalsLayout Proposal) and 
        //(File Area: Agent (Dis)ApprovedequalsTrue) and 
        //(File Area: Standardization equals Standard,Non Standard) and 
        //***(Corner Project: Record TypeequalsNew Corner (Agent))
        if('Approved'.equals(file.Agent_Decision__c) && file.Agent_Dis_Approval_date__c==null && file.Agent_Dis_Approved__c && 'Agent'.equals(file.Project__r.Workflow_Type__c)
            && (file.Standardization__c=='Standard' || file.Standardization__c=='Non Standard')
            ){
                
            update new Project__c(
                Id = file.Project__c,
                Approval_Status__c = 'Draw Technical Plan',
                Notify_Installation_Info_Date__c = SYSTEM.Now(),
                Notify_Installation_Info__c = true,
                Status__c = 'Execution'
            );
        }
    }
}
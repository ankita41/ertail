public with sharing class FileAreaController {
	public class CustomException extends Exception{}
	private String currentUserProfile;
	public Project_File_Area__c file {get; set;}
	public String strUserRole {get; set;} 
    public Boolean hasAccessToFile {
    	get {
    		RecordType execDrawingsRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Executive_Drawings' AND SobjectType = 'Project_File_Area__c'];
    		
    		Boolean hasAccess = true;
    		
    		if ((currentUserProfile == 'NES Market Local Manager' || currentUserProfile == 'NES Market Local Manager (read only)') && file.RecordTypeId == execDrawingsRT.Id) {
    			hasAccess = false;
			}
			
			if (  (currentUserProfile == 'NES Corner Architect' && UserInfo.getUserId() != file.Architect__c) || 
				  (currentUserProfile == 'NES Corner Supplier'  && UserInfo.getUserId() != file.Supplier__c) ||
				  (currentUserProfile == 'NES Market Sales Promoter'  && strUserRole != file.Market__c) || 
				  (currentUserProfile == 'NES Market Local Manager'  && strUserRole != file.Market__c)  || 
				  (currentUserProfile == 'NES Market Local Manager (read only)'  && strUserRole != file.Market__c) ) {
				
				hasAccess = false;
		  	}
    		
    		return hasAccess;
    	} set;
    }
	
	public FileAreaController(ApexPages.StandardController controller) {
		currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
		strUserRole = [SELECT Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name; 
        
        file = (Project_File_Area__c)controller.getRecord();
        file = [SELECT Id, RecordTypeId, Architect__c, Nespresso_Contact__c, Supplier__c, Market__c FROM Project_File_Area__c WHERE Id = :file.Id];
	}
}
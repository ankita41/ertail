public with sharing class FileAreaEditDispatchController {
	private ApexPages.StandardController controller;
	
	public FileAreaEditDispatchController(ApexPages.StandardController controller) {
        this.controller = controller;
    }    
	
	public PageReference getRedir() {
		PageReference newPage = null;
		
		Map<String, String> mapUrlParam = new Map<String, String>();
		mapUrlParam = ApexPages.currentPage().getParameters();
		
		String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
		String strUserRole = [SELECT Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name;
		RecordType execDrawingsRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Executive_Drawings' AND SobjectType = 'Project_File_Area__c'];
		
		Project_File_Area__c file = (Project_File_Area__c)controller.getRecord();
        file = [SELECT Id, RecordTypeId, Architect__c, Nespresso_Contact__c, Supplier__c, Market__c FROM Project_File_Area__c WHERE Id = :file.Id];
		
		if (  (currentUserProfile == 'NES Corner Architect' && UserInfo.getUserId() != file.Architect__c) || 
			  (currentUserProfile == 'NES Corner Supplier'  && UserInfo.getUserId() != file.Supplier__c) ||
			  (currentUserProfile == 'NES Market Sales Promoter'  && strUserRole != file.Market__c) || 
			  (currentUserProfile == 'NES Market Local Manager'  && strUserRole != file.Market__c)  || 
			  (currentUserProfile == 'NES Market Local Manager' || currentUserProfile == 'NES Market Local Manager (read only)') && file.RecordTypeId == execDrawingsRT.Id ) {
			
			newPage = new PageReference('/apex/InsufficientPrivileges');
			
		} else {
		    newPage = new PageReference('/' + file.Id + '/e');
		    newPage.getParameters().put('nooverride', '1');
		    for (String key : mapUrlParam.keySet()) {
				newPage.getParameters().put(key, mapUrlParam.get(key)); 
			}
		}
		
		return newPage.setRedirect(true);
	}
}
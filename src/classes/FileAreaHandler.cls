/*****
*   @Class      :   FileAreaHandler.cls
*   @Description:   Support Project_File_Area__c
*					PLEASE LET WITHOUT SHARING
*   @Author     :   XXXXXXXX
*   @Created    :   XXXXXXXX
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*	Thibauld		24 JUL 2014		Add Update Market Name     
*	Jacky			29 JUL 2014		Add WF Field Update in code
*	Thibauld		29 SEP 2014		Move CheckThereIsOnlyOneMainProjectImageBeforeUpsert in this handler
***/
public class FileAreaHandler{
	
	public class CustomException extends Exception{}
    
    public static void CopyFromProjectToFileAreaBeforeInsertOrUpdate(List<Project_File_Area__c> newList){
         //instantiate set to hold unique record ids
        Set<Id> projectIds = new Set<Id>();
        for(Project_File_Area__c p : newList)
        {
            projectIds.add(p.Project__c);
            System.debug(projectIds);
        }
    
        //instantiate map to hold record ids to their corresponding ownerids
        Map<Id, Project__c> projectMap = new Map<Id, Project__c>([SELECT Id, Workflow_Type__c, Nespresso_Contact__c, NES_Business_Development_Manager__c, Agent_Local_Manager__c, Supplier__c, Architect__c, Point_of_Sale__c, Market__c FROM Project__c WHERE Id IN: projectIds]);
    
        for (Project_File_Area__c p : newList) 
        {
        	if (projectMap.containsKey(p.Project__c)){
	            if (p.Nespresso_Contact__c == null) 
	            {
	                p.Nespresso_Contact__c = projectMap.get(p.Project__c).Nespresso_Contact__c;
	            }
	    
	            if (p.Supplier__c == null) 
	            {
	                p.Supplier__c = projectMap.get(p.Project__c).Supplier__c;
	            }
	        
	            if (p.Architect__c == null) 
	            {
	                p.Architect__c = projectMap.get(p.Project__c).Architect__c;
	            }
	            if (p.Market__c == null) 
	            {
	            	p.Market__c = projectMap.get(p.Project__c).Market__c;
	            }
	            if (p.Point_of_Sale__c == null) 
	            {
	                p.Point_of_Sale__c = projectMap.get(p.Project__c).Point_of_Sale__c;
	            }
	            if (p.NES_Business_Development_Manager__c == null) 
	            {
	                p.NES_Business_Development_Manager__c = projectMap.get(p.Project__c).NES_Business_Development_Manager__c;
	            }
	            if (p.Agent_Local_Manager__c == null) 
	            {
	                p.Agent_Local_Manager__c = projectMap.get(p.Project__c).Agent_Local_Manager__c;
	            }
	            if (p.Workflow_Type__c == null) 
	            {
	                p.Workflow_Type__c = projectMap.get(p.Project__c).Workflow_Type__c;
	            }   
        	}
        }
    }
    
    //This is called from CornerProjectHandler if the Market has changed
    public static void UpdateMarketName(Map<Id, Project__c> projectsMap){
    	List<Project_File_Area__c> faList = [Select Id, Market__c, Project__c from Project_File_Area__c where Project__c in :projectsMap.keySet()];
    	for(Project_File_Area__c fa : faList){
    		fa.Market__c = projectsMap.get(fa.Project__c).MarketName__c;
    	}
    	update faList;
    }
    
    public static void RecalculateFormulasOnProjectAfterFileAreaACD(List<Project_File_Area__c> pfaList){
		Set<Id> projIdSet = new Set<Id>();
		for(Project_File_Area__c pfa : pfaList){
			projIdSet.add(pfa.Project__c);
		}
		CornerProjectHandler.RecalculateFileAreaSummaryFields(projIdSet);
	}
	
	public static void UpdateProjectStandardizationAfterFileAreaApproval(Map<Id, Project_File_Area__c> pfaOldMap, Map<Id, Project_File_Area__c> pfaNewMap){
		List<Project__c> projToUpdateList = new List<Project__c>();
		for (Id pfaId : pfaNewMap.keySet()){
			if (pfaNewMap.get(pfaId).RecordTypeId == Schema.sObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Layout Proposal').getRecordTypeId()
				&& 'Approved by NES'.equals(pfaNewMap.get(pfaId).Approval_Status__c)
				&& pfaNewMap.get(pfaId).Approval_Status__c != pfaOldMap.get(pfaId).Approval_Status__c){
				projToUpdateList.add(new Project__c(
						        			Id = pfaNewMap.get(pfaId).Project__c,
						            		Standardization__c = pfaNewMap.get(pfaId).Standardization__c));
			}
		}
		if (projToUpdateList.size() > 0)
			update projToUpdateList;
	}
	
	public static void CheckThereIsOnlyOneMainProjectImageBeforeUpsert(List<Project_File_Area__c> newList) {
		List<Project_File_Area__c> pfaToUpdate = new List<Project_File_Area__c>();
		for(Project_File_Area__c pfa : newList) {
		    if (pfa.Main_Project_Image__c) {
		        List<Project_File_Area__c> currentMainImage = [SELECT Id, Main_Project_Image__c FROM Project_File_Area__c WHERE Project__c = :pfa.Project__c AND Main_Project_Image__c = true AND Id != :pfa.Id];
		        
		        for (Project_File_Area__c p : currentMainImage) {
		            p.Main_Project_Image__c = false;
		            pfaToUpdate.add(p);
		        }       
		    }
		}
		if (pfaToUpdate.size() >0)
			Database.update(pfaToUpdate, false);
	}
}
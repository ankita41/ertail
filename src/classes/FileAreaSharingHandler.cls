/*****
*   @Class      :   FileAreaSharingHandler.cls
*   @Description:   Support Project_File_Area__c sharing
*					PLEASE LET WITHOUT SHARING
*   @Author     :   XXXXXXXX
*   @Created    :   XXXXXXXX
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
***/
public without sharing class FileAreaSharingHandler {
	public static void ShareFileAreas(Map<Id, Project_File_Area__c> newMap){
		List<Project_File_Area__Share> newSharing = CreateSharingRecords(newMap);
		if (newSharing.size() > 0){
			insert newSharing;
		}
	}
	
	public static void RecalculateSharingAfterUpdate(Map<Id, Project_File_Area__c> oldMap, Map<Id, Project_File_Area__c> newMap){
		Map<Id, Project_File_Area__c> pfaToRecalculate = new Map<Id, Project_File_Area__c>();
		for (Id pfaId : newMap.keySet()){
			if ((oldMap.get(pfaId).Architect__c != newMap.get(pfaId).Architect__c)
				|| (oldMap.get(pfaId).Supplier__c != newMap.get(pfaId).Supplier__c)
				|| (oldMap.get(pfaId).Nespresso_Contact__c != newMap.get(pfaId).Nespresso_Contact__c)
				|| (oldMap.get(pfaId).Agent_Local_Manager__c != newMap.get(pfaId).Agent_Local_Manager__c)
				|| (oldMap.get(pfaId).NES_Business_Development_Manager__c != newMap.get(pfaId).NES_Business_Development_Manager__c))
				pfaToRecalculate.put(pfaId, newMap.get(pfaId));
		}
		
		if (pfaToRecalculate.size() > 0) {
			DeleteExistingSharing(pfaToRecalculate.keySet());
			ShareFileAreas(pfaToRecalculate);
		}
	}
	
	public static void DeleteExistingSharing(Set<Id> pfaIdSet){
		// Locate all existing sharing records for the project records in the batch.
        // Only records using an Apex sharing reason for this app should be returned. 
        List<Project_File_Area__Share> oldpfaShrs = [SELECT Id FROM Project_File_Area__Share WHERE ParentId IN 
             :pfaIdSet AND 
            (RowCause = :Schema.Project_File_Area__Share.rowCause.IsArchitect__c 
            OR RowCause = :Schema.Project_File_Area__Share.rowCause.IsSupplier__c
            OR RowCause = :Schema.Project_File_Area__Share.rowCause.IsProjectManager__c
            OR RowCause = :Schema.Project_File_Area__Share.rowCause.IsAgentLocalManager__c
            OR RowCause = :Schema.Project_File_Area__Share.rowCause.IsBusinessDevelopmentManager__c)]; 
        
        // Delete the existing sharing records.
       // This allows new sharing records to be written from scratch.
        Delete oldpfaShrs;
	}
	
	public static List<Project_File_Area__Share> CreateSharingRecords(Map<Id, Project_File_Area__c> pfaMap){
		List<Project_File_Area__Share> newprojectShrs = new List<Project_File_Area__Share>();
		// Construct new sharing records  
        for(Project_File_Area__c pfa : pfaMap.values()){
        	if (pfa.Architect__c != null){
	           newprojectShrs.add(CreateArchitectSharing(pfa.Architect__c, pfa.Id));
        	}
        	if (pfa.Supplier__c != null){
	           newprojectShrs.add(CreateSupplierSharing(pfa.Supplier__c, pfa.Id));
        	}
        	if (pfa.Nespresso_Contact__c != null){
	           newprojectShrs.add(CreateProjectManagerSharing(pfa.Nespresso_Contact__c, pfa.Id));
        	}
        	if (pfa.Agent_Local_Manager__c != null){
	           newprojectShrs.add(CreateAgentLocalManagerSharing(pfa.Agent_Local_Manager__c, pfa.Id));
        	}
        	if (pfa.NES_Business_Development_Manager__c != null){
	           newprojectShrs.add(CreateBusinessDevelopmentManagerSharing(pfa.NES_Business_Development_Manager__c, pfa.Id));
        	}
        }
        return newprojectShrs;
	}

    private static Project_File_Area__Share CreateProjectManagerSharing(Id userId, Id pfaId){
		return new Project_File_Area__Share(UserOrGroupId = userId
										, AccessLevel = 'Edit'
										, ParentId = pfaId
										, RowCause = Schema.Project_File_Area__Share.RowCause.IsProjectManager__c);
	}

	private static Project_File_Area__Share CreateBusinessDevelopmentManagerSharing(Id userId, Id pfaId){
		return new Project_File_Area__Share(UserOrGroupId = userId
										, AccessLevel = 'Edit'
										, ParentId = pfaId
										, RowCause = Schema.Project_File_Area__Share.RowCause.IsBusinessDevelopmentManager__c);
	}
	
	private static Project_File_Area__Share CreateAgentLocalManagerSharing(Id userId, Id pfaId){
		return new Project_File_Area__Share(UserOrGroupId = userId
										, AccessLevel = 'Edit'
										, ParentId = pfaId
										, RowCause = Schema.Project_File_Area__Share.RowCause.IsAgentLocalManager__c);
	}

	private static Project_File_Area__Share CreateArchitectSharing(Id userId, Id pfaId){
		return new Project_File_Area__Share(UserOrGroupId = userId
										, AccessLevel = 'Edit'
										, ParentId = pfaId
										, RowCause = Schema.Project_File_Area__Share.RowCause.IsArchitect__c);
	}
	
	private static Project_File_Area__Share CreateSupplierSharing(Id userId, Id pfaId){
		return new Project_File_Area__Share(UserOrGroupId = userId
										, AccessLevel = 'Edit'
										, ParentId = pfaId
										, RowCause = Schema.Project_File_Area__Share.RowCause.IsSupplier__c);
	}
}
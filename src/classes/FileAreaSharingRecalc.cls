/*****
*   @Class      :   FileAreaSharingRecalc.cls
*   @Description:   Handles sharing recalculation for File Area
*   @Author     :   Thibauld
*   @Created    :   30 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer        Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------   
*/
global class FileAreaSharingRecalc implements Database.Batchable<sObject> {
    
	// Get email address logged in user
	static String emailAddress = [Select Email From User where Id = : UserInfo.getUserId() limit 1].Email;
    
    // The start method is called at the beginning of a sharing recalculation.
    // This method returns a SOQL query locator containing the records 
    // to be recalculated. 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([Select p.Supplier__c, p.Nespresso_Contact__c, p.NES_Business_Development_Manager__c, p.Architect__c, p.Agent_Local_Manager__c From Project_File_Area__c p]);  
    }
    
    // The executeBatch method is called for each chunk of records returned from start.  
    global void execute(Database.BatchableContext BC, List<sObject> scope){
       // Create a map for the chunk of records passed into method.
        Map<ID, Project_File_Area__c> pfaMap = new Map<ID, Project_File_Area__c>((List<Project_File_Area__c>)scope);  
        
        // Create a list of Project_File_Area__Share objects to be inserted.
        List<Project_File_Area__Share> newprojectShrs = FileAreaSharingHandler.CreateSharingRecords(pfaMap);
        
        try {
           FileAreaSharingHandler.DeleteExistingSharing(pfaMap.keySet());
            
           // Insert the new sharing records and capture the save result. 
           // The false parameter allows for partial processing if multiple records are 
           // passed into operation. 
           Database.SaveResult[] lsr = Database.insert(newprojectShrs,false);
           
           String errorMessage = '';
           // Process the save results for insert.
           for(Database.SaveResult sr : lsr){
               if(!sr.isSuccess()){
                   // Get the first save result error.
                   Database.Error err = sr.getErrors()[0];
                   
                   // Check if the error is related to trivial access level.
                   // Access levels equal or more permissive than the object's default 
                   // access level are not allowed. 
                   // These sharing records are not required and thus an insert exception 
                   // is acceptable. 
                   if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                     &&  err.getMessage().contains('AccessLevel'))){
                       // Error is not related to trivial access level.
                       // Send an email to the Apex project's submitter.
                     errorMessage += 'The File Area sharing recalculation threw the following exception: ' + 
                             err.getMessage() + '\n';
                   }
               }
           }
           SendApexSharingRecalculationErrors(errorMessage);
           
        } catch(DmlException e) {
           // Send an email to the Apex project's submitter on failure.
            SendApexSharingRecalculationException(e);
        }
    }
    
        
    public static void SendApexSharingRecalculationErrors(String errorMessage){
    	if (!''.equals(errorMessage)){
           	 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             String[] toAddresses = new String[] {emailAddress}; 
             mail.setToAddresses(toAddresses); 
             mail.setSubject('File Area Apex Sharing Recalculation Errors');
             mail.setPlainTextBody(errorMessage);
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
           }     
    }
    
    public static void SendApexSharingRecalculationException(Exception e){
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {emailAddress}; 
        mail.setToAddresses(toAddresses); 
        mail.setSubject('File Area Apex Sharing Recalculation Exception');
        mail.setPlainTextBody(
          'The File Area Apex sharing recalculation threw the following exception: ' + 
                    e.getMessage());
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    // The finish method is called at the end of a sharing recalculation.
    global void finish(Database.BatchableContext BC){  
        // Send an email to the Apex project's submitter notifying of project completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {emailAddress}; 
        mail.setToAddresses(toAddresses); 
        mail.setSubject('Apex Sharing Recalculation Completed.');
        mail.setPlainTextBody
                      ('The Apex sharing recalculation finished processing');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}
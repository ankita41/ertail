/*****
*   @Class      :   ForecastHandler.cls
*   @Description:   Handles all Objective__c recurring actions/events.
*   @Author     :   Thibauld
*   @Created    :   24 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                      
***/
public with sharing class ForecastHandler {
    
    public class CustomException extends Exception {}
    
    private static void ResetFutureForecast(Objective__c obj){
        Integer year = Integer.valueOf(obj.year__c);
        Datetime now = Datetime.now();
        Date startQ1 = Date.newInstance(year, 1, 1);
        Date startQ2 = Date.newInstance(year, 4, 1);
        Date startQ3 = Date.newInstance(year, 7, 1);
        Date startQ4 = Date.newInstance(year, 10, 1);
        
        if(now > startQ4) return;
        obj.Q4_Forecast__c = 0;
        
        if (now > startQ3) return;
        obj.Q3_Forecast__c = 0;
        obj.Q4_Forecast_as_of_Q3__c = 0;
        
        if (now > startQ2) return;
        obj.Q2_Forecast__c = 0;
        obj.Q4_Forecast_as_of_Q2__c = 0;
        obj.Q3_Forecast_as_of_Q2__c = 0;
        
        if (now > startQ1) return;
        obj.Q1_Forecast__c = 0;
        obj.Q4_Forecast_as_of_Q1__c = 0;
        obj.Q3_Forecast_as_of_Q1__c = 0;
        obj.Q2_Forecast_as_of_Q1__c = 0;
    }
    
    private static void ResetActuals(Objective__c obj)
    {
        obj.Q1_Actual__c = 0;
        obj.Q2_Actual__c = 0;
        obj.Q3_Actual__c = 0;
        obj.Q4_Actual__c = 0;
    }
    
    // Return the appropriate objective from the Map, null if it doesn't exist
    private static Objective__c GetObjectiveForMarketAndYear(Id marketId, String year, Map<Id, Map<String, Objective__c>> objectivesOrganisedByYearThenMarketIdMap){
        
        if (!objectivesOrganisedByYearThenMarketIdMap.containsKey(marketId))
            return null;
        if (!objectivesOrganisedByYearThenMarketIdMap.get(marketId).containsKey(year))
            return null;
        return objectivesOrganisedByYearThenMarketIdMap.get(marketId).get(year);
    }
            
    private static void AddProjectToActual(Date ActualInstallationDate, Objective__c obj){
        Date startQ2 = Date.newInstance(Integer.valueOf(obj.Year__c), 4, 1);
        Date startQ3 = Date.newInstance(Integer.valueOf(obj.Year__c), 7, 1);
        Date startQ4 = Date.newInstance(Integer.valueOf(obj.Year__c), 10, 1);
        
        if (ActualInstallationDate < startQ2)
            obj.Q1_Actual__c += 1;
        else if (ActualInstallationDate < startQ3)
            obj.Q2_Actual__c += 1;
        else if (ActualInstallationDate < startQ4)
            obj.Q3_Actual__c += 1;
        else
            obj.Q4_Actual__c += 1;
        
        return;
    }
    
    private static void AddProjectToForecast(Date targetedInstallationDate, Objective__c obj){
        Datetime now = Datetime.now();
        Date startQ1 = Date.newInstance(Integer.valueOf(obj.Year__c), 1, 1);
        Date startQ2 = Date.newInstance(Integer.valueOf(obj.Year__c), 4, 1);
        Date startQ3 = Date.newInstance(Integer.valueOf(obj.Year__c), 7, 1);
        Date startQ4 = Date.newInstance(Integer.valueOf(obj.Year__c), 10, 1);
                                
        // ignore projects with a targeted installation date in the past
        if (targetedInstallationDate < now) return;
                
        if (targetedInstallationDate >= startQ4){
            if (startQ4 < now) return;
        
            obj.Q4_Forecast__c = (obj.Q4_Forecast__c==null)?1:obj.Q4_Forecast__c+1;
            
            if (now < startQ3){
                obj.Q4_Forecast_as_of_Q3__c += 1;
            
                if (now < startQ2){
                    obj.Q4_Forecast_as_of_Q2__c += 1;
                
                    if (now < startQ1)
                        obj.Q4_Forecast_as_of_Q1__c += 1;
                }
            }
            return;
        }
        
        if (targetedInstallationDate >= startQ3){
            if (startQ3 < now) return;
            
            obj.Q3_Forecast__c += 1;
            
            if (now < startQ2){
                obj.Q3_Forecast_as_of_Q2__c += 1;
            
                if (now < startQ1)
                    obj.Q3_Forecast_as_of_Q1__c += 1;
            }
            return;
        }
        
        if (targetedInstallationDate >= startQ2){
            if (startQ2 < now) return;
            
            obj.Q2_Forecast__c += 1;
            
            if (now < startQ1)
                obj.Q2_Forecast_as_of_Q1__c += 1;
            
            return;
        }
        
        if (startQ1 < now) return;
            
        obj.Q1_Forecast__c += 1;
        return;
    }
    
    public static void RecalculateFutureForecastsForAllMarkets(){
        List<Project__c> projToUpdateList = new list<Project__c>();
        List<Linked_POS__c> posUpdateList = new List<Linked_POS__c>();
            
        // Get all the objectives for the markets organised in a map
        Map<Id, Map<String, Objective__c>> objectivesOrganisedByYearThenMarketIdMap = new Map<Id, Map<String, Objective__c>>();
        for (Objective__c obj : [Select o.Year__c, o.Q4_Forecast_as_of_Q3__c, o.Q4_Forecast_as_of_Q2__c, o.Q4_Forecast_as_of_Q1__c, o.Q4_Forecast__c, o.Q3_Forecast_as_of_Q2__c, o.Q3_Forecast_as_of_Q1__c, o.Q3_Forecast__c, o.Q2_Forecast_as_of_Q1__c, o.Q2_Forecast__c, o.Q1_Forecast__c, o.Market__c, o.Q1_Actual__c, o.Q2_Actual__c, o.Q3_Actual__c, o.Q4_Actual__c
                                From Objective__c o 
                                where o.Date_From__c >= :Date.newInstance(Date.Today().year()-1, 1, 1)])
        {
            // reset future Forecasts
            ResetFutureForecast(obj);
                        
            // reset actuals
            ResetActuals(obj);
                        
            if (!objectivesOrganisedByYearThenMarketIdMap.containsKey(obj.Market__c)){
                objectivesOrganisedByYearThenMarketIdMap.put(obj.Market__c, new Map<String, Objective__c>());
            }
            if (!objectivesOrganisedByYearThenMarketIdMap.get(obj.Market__c).containsKey(obj.Year__c)){
                objectivesOrganisedByYearThenMarketIdMap.get(obj.Market__c).put(obj.Year__c, obj);
            }
        }
        
        Date today = Date.Today();
        // Get all the non cancelled New projects with a futured Targeted Installation for the Markets or an Actual Installation Date > 1st of jan last year
        for (Project__c proj : [Select p.Key_Account__c, p.Key_Account__r.Market__c, p.Targeted_Installation_Date__c, p.Actual_Installation_Date__c, p.RecordTypeId, p.Corner__c, p.Corner_Point_of_Sale_r__r.Market__c, p.Corner__r.Market__c, p.Corner_Point_of_Sale_r__c , p.Forecast__c
                                From Project__c p 
                                where p.RecordType.DeveloperName not in ('Corner_Cancelled', 'Corner_Migrated')
                                and Workflow_Type__c in ('Agent', 'Sales Promoter')
                                and ((Targeted_Installation_Date__c!= null and Targeted_Installation_Date__c > :today)
                                    or (p.Actual_Installation_Date__c != null 
                                        and 
                                        p.Actual_Installation_Date__c >= :Date.newInstance(today.year()-1, 1, 1)))])
        {
            // Get the relevant objectives
            Id marketId = (proj.Key_Account__c != null) ? proj.Key_Account__r.Market__c : (proj.Corner_Point_of_Sale_r__c != null) ? proj.Corner_Point_of_Sale_r__r.Market__c : null;
            
            if (marketId != null){          
                // Update the forecast for the objective
                if (proj.Targeted_Installation_Date__c > today){
                    String year = String.valueOf(proj.Targeted_Installation_Date__c.year());
                    Objective__c obj = GetObjectiveForMarketAndYear(marketId, year, objectivesOrganisedByYearThenMarketIdMap);
                    if (obj != null) {
                        if (proj.Forecast__c == null){
                            projToUpdateList.add(proj);
                        }
                        AddProjectToForecast(proj.Targeted_Installation_Date__c, obj);
                    }
                }
                
                // Update the actual for the objective
                if (proj.Actual_Installation_Date__c != null){
                    String year = String.valueOf(proj.Actual_Installation_Date__c.year());
                    Objective__c obj = GetObjectiveForMarketAndYear(marketId, year, objectivesOrganisedByYearThenMarketIdMap);
                    if (obj != null)
                        AddProjectToActual(proj.Actual_Installation_Date__c, obj);
                }
            }
        }
        /** Below block is added for mass corner change ***/
        for(Linked_POS__c link:[SELECT Id, Corner_Point_of_Sale__c, Key_Account__c, Targeted_Installation_Date__c, Actual_Installation_Date__c, 
                               Corner_Project__c,Corner_Project__r.recordtype.DeveloperName,Key_Account__r.Market__c,Corner_Point_of_Sale__r.Market__c,Forecast__c FROM Linked_POS__c 
                               where Corner_Project__r.recordtype.DeveloperName not in ('Corner_Cancelled', 'Corner_Migrated') 
                                AND ((Targeted_Installation_Date__c > :today) or (Actual_Installation_Date__c != null and  
                                        Actual_Installation_Date__c >= :Date.newInstance(today.year()-1, 1, 1)))]){
                
                Id marketId = (link.Key_Account__c != null) ? link.Key_Account__r.Market__c : (link.Corner_Point_of_Sale__c != null) ? link.Corner_Point_of_Sale__r.Market__c : null;
            
                if (marketId != null){
                      // Update the forecast for the objective
                    if (link.Targeted_Installation_Date__c > today){
                        String year = String.valueOf(link.Targeted_Installation_Date__c.year());
                        Objective__c obj = GetObjectiveForMarketAndYear(marketId, year, objectivesOrganisedByYearThenMarketIdMap);
                        if (obj != null) {
                            if (link.Forecast__c == null){
                                posUpdateList.add(link);
                            }
                            AddProjectToForecast(link.Targeted_Installation_Date__c, obj);
                        }
    
                     }   
                 } 
                 
                 // Update the actual for the objective
                if (link.Actual_Installation_Date__c != null){
                    String year = String.valueOf(link.Actual_Installation_Date__c.year());
                    Objective__c obj = GetObjectiveForMarketAndYear(marketId, year, objectivesOrganisedByYearThenMarketIdMap);
                    if (obj != null)
                        AddProjectToActual(link.Actual_Installation_Date__c, obj);
                }
             
        }       
       
        /** Below block is added for mass corner change ***/
        // Upsert the objectives
        List<Objective__c> objectivesToUpsert = new List<Objective__c>();
        for (Map<String, Objective__c> objOrganisedByYear : objectivesOrganisedByYearThenMarketIdMap.values()){
            objectivesToUpsert.addAll(objOrganisedByYear.values()); 
        }
        upsert objectivesToUpsert;
        
        // Update the projects to link them to the appropriate Forecast
        if (!projToUpdateList.isEmpty()){
            for (Project__c proj : projToUpdateList){
                Id marketId = (proj.Key_Account__c != null) ? proj.Key_Account__r.Market__c : (proj.Corner_Point_of_Sale_r__c != null) ? proj.Corner_Point_of_Sale_r__r.Market__c : null;
                String year = String.valueOf(proj.Targeted_Installation_Date__c.year());
                Objective__c obj = GetObjectiveForMarketAndYear(marketId, year, objectivesOrganisedByYearThenMarketIdMap);
                if (obj != null)
                    proj.Forecast__c = obj.Id;
            }
            update(projToUpdateList);
        }
        /** Below block is added for mass corner change ***/
         // Update the projects to link them to teh appropriate Forecast
        if (!posUpdateList.isEmpty()){
            for (Linked_POS__c proj : posUpdateList){
                Id marketId = (proj.Key_Account__c != null) ? proj.Key_Account__r.Market__c : (proj.Corner_Point_of_Sale__c != null) ? proj.Corner_Point_of_Sale__r.Market__c : null;
                String year = String.valueOf(proj.Targeted_Installation_Date__c.year());
                Objective__c obj = GetObjectiveForMarketAndYear(marketId, year, objectivesOrganisedByYearThenMarketIdMap);
                if (obj != null)
                    proj.Forecast__c = obj.Id;
            }
            update(posUpdateList);
        }
        /** Below block is added for mass corner change ***/      

    }
    
    public static void PreventDuplicates(List<Objective__c> oldList, List<Objective__c> newList){
        // Request the list of existing Objectives
        Map<Id, Map<String, Id>> objectivesOrderedByMarketThenYear = new Map<Id, Map<String, Id>>();
        for (Objective__c obj :[Select Id, Market__c, year__c from Objective__c where Market__c!=null and year__c!=null])  
        {
            AddElementToObjectivesOrderedByMarketThenYear(objectivesOrderedByMarketThenYear, obj, true, false);
        }
        
        // Remove the old values from the Map
        if (oldList != null)
            for(Objective__c obj : oldList){
                objectivesOrderedByMarketThenYear.get(obj.Market__c).remove(obj.year__c);
            }
            
        // Try to add the new values
        for(Objective__c obj : newList){
            AddElementToObjectivesOrderedByMarketThenYear(objectivesOrderedByMarketThenYear, obj, false, true);
        }
    }
    
    // After a new Objective Creation, add the new links to the relevant projects   
    public static void UpdateExistingProjectsAfterObjectiveInsert(List<Objective__c> newList){
        Map<Id, Map<String, Id>> newObjectivesOrderedByMarketThenYear = new Map<Id, Map<String, Id>>();
        Set<Id> marketIdsSet = new Set<Id>();
        
        for(Objective__c obj : newList){
            AddElementToObjectivesOrderedByMarketThenYear(newObjectivesOrderedByMarketThenYear, obj, true, false);
            marketIdsSet.add(obj.Market__c);
        }
        
        // request all the projects for the selected Market & year
        List<Project__c> projectsToUpdateList = new List<Project__c>();
        
        for(Project__c proj : [Select Id, Targeted_Installation_Date__c, Forecast__c, Corner_Point_of_Sale_r__r.Market__c from Project__c where Corner_Point_of_Sale_r__r.Market__c in:marketIdsSet and Workflow_Type__c in ('Agent', 'Sales Promoter')]){
            if (proj.Targeted_Installation_Date__c != null && proj.Corner_Point_of_Sale_r__r.Market__c  != null){
                String yearStr = String.valueOf(proj.Targeted_Installation_Date__c.year());
                if (newObjectivesOrderedByMarketThenYear.containsKey(proj.Corner_Point_of_Sale_r__r.Market__c) && newObjectivesOrderedByMarketThenYear.get(proj.Corner_Point_of_Sale_r__r.Market__c).containsKey(yearStr)){
                    proj.Forecast__c = newObjectivesOrderedByMarketThenYear.get(proj.Corner_Point_of_Sale_r__r.Market__c).get(yearStr);
                    projectsToUpdateList.add(proj);
                }
                    
            }
        }   
        
        if (!projectsToUpdateList.isEmpty())
            update (projectsToUpdateList);
    }
    
    private static void AddElementToObjectivesOrderedByMarketThenYear(Map<Id, Map<String, Id>> objectivesOrderedByMarketThenYear, Objective__c obj, Boolean throwException, Boolean addError){
        if(!objectivesOrderedByMarketThenYear.containsKey(obj.Market__c))
            objectivesOrderedByMarketThenYear.put(obj.Market__c, new Map<String, Id>());
        if(!objectivesOrderedByMarketThenYear.get(obj.Market__c).containsKey(obj.year__c)){
            objectivesOrderedByMarketThenYear.get(obj.Market__c).put(obj.year__c, obj.Id);
        }else{
            if (addError)
                obj.year__c.addError('Error: There is already a Forecast for this Market and year.');
            if (throwException)
                throw new CustomException('Error: Multiple Forecasts for Market:' + obj.Market__c + ' and year:' + obj.year__c + '. Please contact your administrator.');
        }
            
    }
    
    public static void PreventYearChange(Map<Id, Objective__c> oldMap, Map<Id, Objective__c> newMap){
        for (Id objId : newMap.keySet()){
            if (oldMap.get(objId).year__c != null && oldMap.get(objId).year__c != newMap.get(objId).year__c){
                newMap.get(objId).year__c.addError('Once set, the year cannot be changed. This has been rolled back.');
                newMap.get(objId).year__c = oldMap.get(objId).year__c;
            }
                
        }
    }
    
    public static void ConvertCurrenciesBeforeUpsert(List<Objective__c> newList){
        Set<String> createdYears = new Set<String>();
        Set<String> localCurrencies = new Set<String>();
        for (Objective__c obj: newList) {
            if(obj.CreatedDate == null)
                createdYears.add(String.valueOf(SYSTEM.Today().year()));
            else
                createdYears.add(String.valueOf(obj.CreatedDate.year()));
            
            localCurrencies.add(obj.Currency__c);
        }
    
        CurrencyConverter cc = new CurrencyConverter(createdYears, localCurrencies);
        ConvertCostsToCHF(newList, cc);
    }
    
    private static void ConvertCostsToCHF(List<Objective__c> newList, CurrencyConverter cc){
        for(Objective__c obj : newList){
            obj.Avg_Capsule_Sales_Price_CHF__c = cc.convertValueFromTo(double.valueOf(obj.Avg_Capsule_sales_price_in_LC_ex_VAT__c), obj.Currency__c, 'CHF', obj.CreatedDate);
            obj.Avg_Machine_Sales_Price_CHF__c = cc.convertValueFromTo(double.valueOf(obj.Avg_machine_sales_price_in_LC_ex_VAT__c), obj.Currency__c, 'CHF', obj.CreatedDate);
            obj.Capsules_Cost_CHF__c = cc.convertValueFromTo(double.valueOf(obj.Caps_Cost_in_LC__c), obj.Currency__c, 'CHF', obj.CreatedDate);
        }
    }

}
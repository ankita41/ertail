global class GenerateRecommendationBatch implements Database.Batchable<sObject> {
 global final String query;
 Campaign__c NCampaign ; 
 User CurrentUser;
   global GenerateRecommendationBatch(Campaign__c Campaign, user loggedinUser) {
       NCampaign = Campaign;
       CurrentUser = loggedinUser;
       query = 'Select Campaign__r.Id, Campaign__c From Campaign_Market__c  where Campaign__c =\''+NCampaign.Id+'\'  ';
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
       system.debug('query'+query);
       String email_body='Hello '+CurrentUser.Firstname+'<br /><br />';
       email_body+=' Your “Generate Recommendations” request has started. It might take a few minutes to complete. You will receive a notification once action will be finished.'+'<br /><br/>';
       email_body+='<br/><br/> Thanks. <br/>';
       List <string> toAddresses = new List<String>();
       toAddresses.add(CurrentUser.Email);
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       mail.setToAddresses(toAddresses);
       mail.setHtmlBody(email_body);
       mail.setSaveAsActivity(false);
       mail.setSubject('Generate Recommendation Process Start');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                           
      return Database.getQueryLocator(query);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> scope){
    List<id> SelectedBoutiques = new List<id>();
    List<id> SelectedMarket = new List<id>();
    
      for(Sobject S : Scope){
          SelectedMarket.add(s.id);
      }
      for (Campaign_Boutique__c c : [Select Id, RTCOMBoutique__c from Campaign_Boutique__c where Campaign_Market__c IN :SelectedMarket]){
            selectedBoutiques.add(c.RTCOMBoutique__c);
       }
      system.debug('@@@campaign '+NCampaign );
      system.debug('@@@SelectedBoutiques'+SelectedBoutiques);
      ERTAILCampaignHandler.GenerateRecommendations(NCampaign ,SelectedBoutiques);
        
   }

   global void finish(Database.BatchableContext BC){
       String email_body='Hello '+CurrentUser.Firstname+'<br /><br />';
       email_body+=' Your “Generate Recommendations” request has been completed. You can now check your campaign and finalize it.'+'<br /><br/>';
       email_body+='<br/><br/> Thanks.<br/>';
       List <string> toAddresses = new List<String>();
       toAddresses.add(CurrentUser.Email);
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       mail.setToAddresses(toAddresses);
       mail.setHtmlBody(email_body);
       mail.setSaveAsActivity(false);
       mail.setSubject('Generate Recommendation Process Finish');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
   }
  }
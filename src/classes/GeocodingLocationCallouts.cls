public class GeocodingLocationCallouts {

    @future (callout=true)  // future method needed to run callouts from Triggers
    static public void getLocation(Set<Id> cornerIds){
      	List<Corner__c> corners = [SELECT Id, Point_of_Sale__c, Location__Latitude__s, Location__Longitude__s, Geocoding_Zero_Results__c FROM Corner__c WHERE Id IN :cornerIds];
      	Set<Id> setPOSIds = new Set<Id>();
      	for (Corner__c c : corners) {
      		setPOSIds.add(c.Point_of_Sale__c);
      	}
      	
      	RecordType posRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Point_of_Sales'];
      	Map<Id, Account> mapPOSAccountsPerAccountId = new Map<Id, Account>([SELECT Id,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet FROM Account WHERE RecordTypeId = :posRT.Id AND Id IN :setPOSIds]);
        
        String clientID = GoogleMapsSettings__c.getValues('Client ID').Value__c;
        String privateKey = GoogleMapsSettings__c.getValues('Crypto Key').Value__c;
        
        for (Corner__c c : corners) {
	        Account a = mapPOSAccountsPerAccountId.get(c.Point_of_Sale__c);
	 
	        // create an address string
	        String address = '';
	        if (a.BillingStreet != null)
	            address += a.BillingStreet +', ';
	        if (a.BillingCity != null)
	            address += a.BillingCity +', ';
	        if (a.BillingState != null)
	            address += a.BillingState +' ';
	        if (a.BillingPostalCode != null)
	            address += a.BillingPostalCode +', ';
	        if (a.BillingCountry != null)
	            address += a.BillingCountry;
	 
	        address = EncodingUtil.urlEncode(address, 'UTF-8');
	        
	        // get digital signature
	        String url = '/maps/api/geocode/json?address='+address+'&sensor=false&client=' + clientID;
			
			privateKey = privateKey.replace('-', '+');
			privateKey = privateKey.replace('_', '/');
			
			Blob privateKeyBlob = EncodingUtil.base64Decode(privateKey);
			//Blob privateKeyBlob = Blob.valueOf(privateKey);
			Blob urlBlob = Blob.valueOf(url);
			Blob signatureBlob = Crypto.generateMac('hmacSHA1', urlBlob, privateKeyBlob);
			
			//String signature = EncodingUtil.urlEncode(EncodingUtil.base64Encode(signatureBlob), 'UTF-8');
			String signature = EncodingUtil.base64Encode(signatureBlob);
			signature = signature.replace('+', '-');
			signature = signature.replace('/', '_');
			
			system.debug('signature is ' + signature);
	 
	        // build callout
	        Http h = new Http();
	        HttpRequest req = new HttpRequest();
	        req.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false&client=' + clientID + '&signature=' + signature);
	        req.setMethod('GET');
	        req.setTimeout(60000);
	 		
	        try{
	            // callout
	            HttpResponse res = h.send(req);
	            system.debug(res.getBody());
	 
	            // parse coordinates from response
	            JSONParser parser = JSON.createParser(res.getBody());
	            double lat = null;
	            double lon = null;
	            while (parser.nextToken() != null) {
	                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'location')) {
                       parser.nextToken(); // object start
                       while (parser.nextToken() != JSONToken.END_OBJECT) {
                            String txt = parser.getText();
                            parser.nextToken();
                            if (txt == 'lat') {
                                lat = parser.getDoubleValue();
                            } else if (txt == 'lng') {
                                lon = parser.getDoubleValue();
                            }
                        }
	                } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'status')) {
	                	parser.nextToken();
	                	String txt = parser.getText();
	                	if (txt == 'ZERO_RESULTS') {
	                		c.Geocoding_Zero_Results__c = true;
	                	}
	                }
	            }
	 
	            // update coordinates if we get back
	            if (lat != null){
	                c.Location__Latitude__s = lat;
	                c.Location__Longitude__s = lon;
	                
	            }
	 
	        } catch (Exception e) {
	        }
        }
        
        update corners;
    }
}
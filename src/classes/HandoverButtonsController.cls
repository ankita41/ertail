/*****
*   @Class      :   HandoverButtonsController.cls
*   @Description:   Controller for the Handover Report buttons.
*   @Author     :   XXXXXXXXX
*   @Created    :   
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        29 JUL 2014     Transformed WF Field Updates to Apex Code
*
***/

public with sharing class HandoverButtonsController {
    public Handover_Report__c handover {get; set;}
    public String currentUserProfile;
   // public Boolean sendNotficationFlag{get;set;}
    public String validateHandoverReportSuccessMessage {get; set;}
   // public Boolean allowSendNotification{get;set;}
    
    private  Map<String, RecordType> fileAreaRecordTypeMapOrderedByDeveloperName{
        get{
            if (fileAreaRecordTypeMapOrderedByDeveloperName == null){
                fileAreaRecordTypeMapOrderedByDeveloperName = new Map<String, RecordType>();
                for (RecordType rt : [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType = 'Project_File_Area__c']){
                    fileAreaRecordTypeMapOrderedByDeveloperName.put(rt.DeveloperName, rt);
                }
            }
            return fileAreaRecordTypeMapOrderedByDeveloperName;
        }
        set;
    }
    
    private Id getFileAreaRecordTypeId(String developerName){
        if (fileAreaRecordTypeMapOrderedByDeveloperName.containsKey(developerName))
            return fileAreaRecordTypeMapOrderedByDeveloperName.get(developerName).Id;
        return null;
    }
    
    public Boolean allowDisplayMassPDFGeneratorButton { 
        get{
            if(handover.Project__r.isMassCorner__c==true)
              return true;
            else
              return false;
        } set;
    }
    
    private Boolean IsProjectOnHold{
        get{
            if (IsProjectOnHold == null){
                IsProjectOnHold = (handover.Project__r.Status__c == CornerProjectButtonsController.ONHOLDSTATUS);
            }
            return IsProjectOnHold;
        }
        set;
    }
    
    private String WorkflowType{
        get{
            if (WorkflowType == null){
                WorkflowType = [Select Workflow_Type__c from Project__c where Id=:handover.Project__c].Workflow_Type__c;
            }
            return WorkflowType;
        }
        set;
    }
    
    public Boolean showSendForValidation {
        get {
            String hasAccessRights = 'NES Market Local Manager, NES Market Sales Promoter,System Administrator,System Admin';
            return (!IsProjectOnHold && WorkflowType== 'Sales Promoter' && handover.Report_sent_date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    public Boolean showSendForValidation_Agent {
        get {
            String hasAccessRights = 'Agent Local Manager, NES Market Business Development Manager,System Administrator,System Admin';
            return (!IsProjectOnHold && WorkflowType== 'Agent' && handover.Report_sent_date__c == null && hasAccessRights.contains(currentUserProfile));
        } set;
    }

    public Boolean allowSendForValidation { 
        get{
            return true;
        } set;
    }
    
    public Boolean showSendDecision {
        get {
            String hasAccessRights = 'NES Market Local Manager,System Administrator,System Admin';
            return (!IsProjectOnHold && WorkflowType== 'Sales Promoter' && handover.NES_Approval_date__c == null && handover.Report_sent_date__c != null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    
    public Boolean showSendDecision_Agent {
        get {
            String hasAccessRights = 'NES Market Business Development Manager,System Administrator,System Admin';
            return (!IsProjectOnHold && WorkflowType== 'Agent' && handover.NES_Approval_date__c == null && handover.Report_sent_date__c != null && hasAccessRights.contains(currentUserProfile));
        } set;
    }
    
    public Boolean allowSendDecision { 
        get{
            Boolean hasPhoto = false;
            Boolean isSigned = false;
            for (Project_File_Area__c fa: [SELECT Id, RecordType.DeveloperName FROM Project_File_Area__c WHERE Handover_Report__c = :handover.Id AND RecordType.DeveloperName in ('Handover_Photo', 'Handover_Report_signed')]){
                hasPhoto |= (fa.RecordType.DeveloperName == 'Handover_Photo');
                isSigned |= (fa.RecordType.DeveloperName == 'Handover_Report_signed');
            }
            return (handover.NES_Decision__c != null && ((handover.NES_Decision__c == 'Approved' && hasPhoto && isSigned) || (handover.NES_Decision__c == 'Disapproved' && handover.NES_Reason__c != null)));
        } set;
    }
    
    
    public HandoverButtonsController(ApexPages.StandardController controller) {
        currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        
        handover = (Handover_Report__c)controller.getRecord();
        handover = [SELECT Id, Send_report_for_approval__c,Linked_POS__C, Report_sent_date__c, Send_NES_Decision__c, NES_Dis_Approval_date__c, NES_Decision__c, Approval_Status__c, 
                        NES_Reason__c, Project__c, IsSendNotification__c,NES_Approval_date__c, Project__r.Status__c, Project__r.isMassCorner__c,RecordTypeId, Project__r.Workflow_Type__c, Installer_Departure_Date__c
                        FROM Handover_Report__c WHERE Id = :handover.Id];
                        
        if (WorkflowType== 'Agent' || WorkflowType== 'Sales Promoter')
            validateHandoverReportSuccessMessage = 'Thank you, your decision will be sent.';
           // allowSendNotification=false;
    }
    
    private Id tradeCornerRTypeId = Schema.sObjectType.Handover_Report__c.getRecordTypeInfosByName().get('Trade Corner').getRecordTypeId();
    
    public PageReference SendForValidation () {
        handover.Send_report_for_approval__c = true;
        update handover;
        
        //"WF Sales Promoter: HandoverReportForApproval"
        //(Handover Report: Send report for approvalequalsTrue) and 
        //(Handover Report: Report sent dateequalsnull) and 
        //(Handover Report: Record TypeequalsTrade Corner) and 
        //(Corner Project: Workflow TypeequalsSales Promoter)
        
        //"WF Agent: HandoverReportForApproval"
        //(Handover Report: Send report for approvalequalsTrue) and 
        //(Handover Report: Report sent dateequalsnull) and 
        //(Handover Report: Record TypeequalsTrade Corner) and 
        //(Corner Project: Workflow TypeequalsAgent)
        
        if(handover.Send_report_for_approval__c && handover.Report_sent_date__c==null && handover.RecordTypeId==tradeCornerRTypeId &&
            (handover.Project__r.Workflow_Type__c=='Sales Promoter' || handover.Project__r.Workflow_Type__c=='Agent')){
            update new Project__c(
                Id = handover.Project__c,
                Status__c = 'Handover',
                Approval_Status__c = 'Review Handover Report',
                Actual_Installation_Date__c = handover.Installer_Departure_Date__c
            );
        }
        
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + handover.Id);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
    
    public PageReference SendDecision () {
        handover.Send_NES_Decision__c = true;
        update handover;
        
        //"WF Sales Promoter: HandoverReportApprovedByMarket"
        //(Handover Report: NES DecisionequalsApproved) and 
        //(Handover Report: NES Approval dateequalsnull) and 
        //(Handover Report: Send NES DecisionequalsTrue) and 
        //(Corner Project: Workflow TypeequalsSales Promoter) 
        
        //"WF Agent: HandoverReportApprovedByBDM"
        //(Handover Report: NES DecisionequalsApproved) and 
        //(Handover Report: NES Approval dateequalsnull) and 
        //(Handover Report: Send NES DecisionequalsTrue) and 
        //(Corner Project: Workflow TypeequalsAgent) 
        
        if(handover.NES_Decision__c=='Approved' && handover.NES_Approval_date__c==null && handover.Send_NES_Decision__c &&
            (handover.Project__r.Workflow_Type__c=='Sales Promoter' || handover.Project__r.Workflow_Type__c=='Agent')){
            update new Project__c(
                Id = handover.Project__c,
                Approval_Status__c = 'Close Project'
            );
        }
        
        //"WF Common: HandoverReportDisapprovedByMarket"
        //(Handover Report: NES DecisionequalsDisapproved) and 
        //(Handover Report: Record TypeequalsTrade Corner) and 
        //(Handover Report: NES Disapproval dateequalsnull) and 
        //(Handover Report: Send NES DecisionequalsTrue) and 
        //(Corner Project: Workflow TypeequalsSales Promoter,Agent) 
        
        else if(handover.NES_Decision__c=='Disapproved' && handover.RecordTypeId==tradeCornerRTypeId && handover.NES_Dis_Approval_date__c==null &&
            handover.Send_NES_Decision__c && (handover.Project__r.Workflow_Type__c=='Sales Promoter' || handover.Project__r.Workflow_Type__c=='Agent')){
            update new Project__c(
                Id = handover.Project__c,
                Approval_Status__c = 'Nonconformity Action Plan'
            );
        }
        
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + handover.Id);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
    
    public PageReference BackToProject () {     
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + handover.Project__c);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
    Public String strHandoverMsg{get;set;}
    public Boolean allowNotification{get{if(handover.IsSendNotification__c==false && handover.Linked_POS__c != null){return true;}else {return false;}}set;}        
    public Boolean allowSendNotification{
    get{
    List<Project_File_Area__c > fa= [SELECT Id, RecordType.DeveloperName FROM Project_File_Area__c WHERE Handover_Report__c = :handover.Id AND RecordType.DeveloperName in ('Handover_Photo', 'Handover_Report_signed')];
      List<Project_File_Area__c> signedList=new list<Project_File_Area__c>();
      List<Project_File_Area__c> photoList=new list<Project_File_Area__c>();
      if(fa.size()>0 && handover.IsSendNotification__c==false){
          // allowNotification= true;      
          for(Project_File_Area__c obj:fa){
              if(obj.RecordType.DeveloperName=='Handover_Photo'){photoList.add(obj);}
              if(obj.RecordType.DeveloperName=='Handover_Report_signed'){signedList.add(obj);}
          }
          if(signedList.size()>0 && photoList.size()==0){
              strHandoverMsg='Sorry, the report must have at least one Handover Photo of the Corner in the File area section.';
              return true;
          } else if (signedList.size()==0 && photoList.size()>0)
          { 
              strHandoverMsg='Sorry, the report must have at least one Signed Handover Report in the File area section.';
              return true;
          }else if(signedList.size()>0 && photoList.size()>0){return false;}
       return true;
        
       }
       else
       {
        allowNotification=false; 
         strHandoverMsg='Sorry, the report must have at least one Handover Photo of the Corner and also the Signed Handover Report in the File area section.';     
       return true;
       }
    } set;
   }    public void sendNotification()
    {
       
     // List<Project_File_Area__c >fa= [SELECT Id, RecordType.DeveloperName FROM Project_File_Area__c WHERE Handover_Report__c = :handover.Id AND RecordType.DeveloperName in ('Handover_Photo', 'Handover_Report_signed')];
      if(allowNotification){
        handover.IsSendNotification__c=true;
        map<id,Linked_POS__c> linkPOSMap=new Map<id,Linked_POS__c>([Select id,Actual_Installation_Date__c from Linked_POS__c where Corner_Project__c=:handover.Project__c]);
         integer lnkSize=0;
         if(linkPOSMap!= null && !linkPOSMap.isempty()){
             lnkSize=linkPOSMap.keyset().size();
            /* Linked_POS__c obj=linkPOSMap.get(handover.Linked_POS__c);
             if(obj!= null && handover!=null && handover.Installer_Departure_Date__c != null){
                 obj.Actual_Installation_Date__c=handover.Installer_Departure_Date__c;
                update obj;                
             } */
         }
       // allowSendNotification=true;
        update handover;
        Boolean isCloseproject=true;
        //integer lnkSize=[Select count() from Linked_POS__c where Corner_Project__c=:handover.Project__c];
        
        Map<Id,Boolean>posIDHandover=new Map<Id,Boolean>();
        for(Handover_Report__c hndOver: [Select Id, Project__c,Linked_POS__c , IsSendNotification__c from Handover_Report__c where Project__c=:handover.Project__c ])
        {
        If(!posIDHandover.containsKey(hndOver.Linked_POS__c ))
        {
        posIDHandover.put(hndOver.Linked_POS__c , hndOver.IsSendNotification__c);
        }
        If(hndOver.IsSendNotification__c==false){ isCloseproject=false;break;}
        
        }
        System.debug('--isclose'+isCloseproject);
            if(isCloseproject ==true  && posIDHandover.keySet().Size()==lnkSize ) {
                Project__c prj=new Project__c(Id=handover.Project__c,Approval_Status__c='Close Project',Status__c='Handover');
            Update prj;
        }


       }
    }
}
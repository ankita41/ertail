public class HandoverChecklistExtension {
    private Handover_Report__c hr;
    public List<Handover_Checklist__c> checklist {get; set;}
    public String currentUserProfile{get;set;}
    public string strUserRole{get;set;}
    
    public Boolean hasAccessToHandover {
        get {
            strUserRole = [SELECT Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name; 
            String tempMarket='';
            if(strUserRole.contains('UK')){
                tempMarket='UK/Ireland';
            }
            else if(strUserRole.contains('Belgium')){
                tempMarket='Belgium Luxembourg';
            }
            else{
                tempMarket=hr.Market__c;
            }
            
            return !(  
                      (currentUserProfile == 'NES Corner Architect' && UserInfo.getUserId() != hr.Architect__c) || 
                      (currentUserProfile == 'NES Corner Supplier'  && UserInfo.getUserId() != hr.Supplier__c) ||
                      (currentUserProfile == 'NES Market Sales Promoter'  && UserInfo.getUserId() != hr.NES_Sales_Promoter__c) || 
                      (currentUserProfile == 'NES Market Local Manager'  && strUserRole != tempMarket) 
                      );
        } set;
    }
    
    public HandoverChecklistExtension(ApexPages.StandardController controller) {
        currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        
        this.hr = (Handover_Report__c)controller.getRecord();
        hr = [SELECT Id, Architect__c, Supplier__c, NES_Sales_Promoter__c, Market__c FROM Handover_Report__c WHERE Id = :hr.Id];
        
        checklist = [SELECT Id, Furniture__c, Quantity__c, Note__c, Comments_ci__c, Status_ci__c, Comments_ed__c,  Status_ed__c, Comments_L__c,  Status_L__c
                        FROM Handover_Checklist__c WHERE Handover_Report__c = :hr.Id AND Completed_items__c = true];
    }
    
    public PageReference saveHandoverChecklist () {
        try {
            update checklist;
        } catch (DMLException e) {
            system.debug(e.getMessage());
        }   
        
        PageReference retPage = new PageReference('/' + hr.Id);
        retPage.setRedirect(true);
        return retPage;
    }
}
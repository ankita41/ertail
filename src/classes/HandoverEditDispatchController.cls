public with sharing class HandoverEditDispatchController {
	private ApexPages.StandardController controller;
	
	public HandoverEditDispatchController(ApexPages.StandardController controller) {
        this.controller = controller;
    }    
	
	public PageReference getRedir() {
		PageReference newPage = null;
		
		Map<String, String> mapUrlParam = new Map<String, String>();
		mapUrlParam = ApexPages.currentPage().getParameters();
		
		String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
		
		Handover_Report__c hr = (Handover_Report__c)controller.getRecord();
        hr = [SELECT Id, Market__c, Architect__c, Nespresso_Contact__c, Supplier__c, NES_Sales_Promoter__c, RecordTypeId FROM Handover_Report__c WHERE Id = :hr.Id];
		String strUserRole = [SELECT Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name; 
		
		if (  (currentUserProfile == 'NES Corner Architect' && UserInfo.getUserId() != hr.Architect__c) || 
			  (currentUserProfile == 'NES Corner Supplier'  && UserInfo.getUserId() != hr.Supplier__c) ||
			  (currentUserProfile == 'NES Market Sales Promoter'  && strUserRole != hr.Market__c) ||
  			  (currentUserProfile == 'NES Market Local Manager'  && strUserRole != hr.Market__c) ) {
			
			newPage = new PageReference('/apex/InsufficientPrivileges');
			
		} else {
		    newPage = new PageReference('/' + hr.Id + '/e');
		    newPage.getParameters().put('nooverride', '1');
		    for (String key : mapUrlParam.keySet()) {
				newPage.getParameters().put(key, mapUrlParam.get(key));
			}
		}
		
		return newPage.setRedirect(true);
	}
}
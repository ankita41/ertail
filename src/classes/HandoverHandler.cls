/*****
*   @Class      :   HandoverHandler.cls
*   @Description:   Handles all Handover__c recurring actions/events.
*   @Author     :   Jacky Uy
*   @Created    :   04 SEP 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class HandoverHandler {
	public static void CopyDefaultValuesFromRECOProjectToHandover(List<Handover__c> newList){
	    Set<Id> projIds = new Set<Id>();
	    for(Handover__c h : newList){
	        projIds.add(h.RECO_Project__c);
	    }
	    
	    Map<Id, RECO_Project__c> projectMap = new Map<Id, RECO_Project__c>([
	        SELECT National_boutique_manager__c, Support_National_Boutique_Manager__c, Retail_project_manager__c, Other_Retail_Project_Manager__c, Manager_architect__c, Other_Manager_Architect__c
	        FROM RECO_Project__c WHERE Id IN :projIds
	    ]);
	    
	    for(Handover__c h : newList){
	        h.Retail_project_manager__c = projectMap.get(h.RECO_Project__c).Retail_project_manager__c;
	        h.Support_Retail_Project_Manager__c = projectMap.get(h.RECO_Project__c).Other_Retail_Project_Manager__c;
	        h.National_boutique_manager__c = projectMap.get(h.RECO_Project__c).National_boutique_manager__c;
	        h.Support_National_Boutique_Manager__c = projectMap.get(h.RECO_Project__c).Support_National_Boutique_Manager__c;
	        h.Manager_architect__c = projectMap.get(h.RECO_Project__c).Manager_architect__c;
	        h.Other_Manager_Architect__c = projectMap.get(h.RECO_Project__c).Other_Manager_Architect__c;
	    }
	}
	
	//Prevent deletion of handovers from closed projects
	public static void StopDeletionOfHandovers(List<Handover__c> oldList){
		String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
		String hasDeleteRights = 'NES RECO Retail Project Manager, System Admin';
		Map<Id, RECO_Project__c> mapProjectsPerId = new Map<Id, RECO_Project__c>([SELECT Id, Status__c FROM RECO_Project__c]);
		  
		for(Handover__c hr : oldList) {
			if(mapProjectsPerId.get(hr.RECO_Project__c).Status__c == 'Completed' && !hasDeleteRights.contains(currentUserProfile))
		    	hr.RECO_Project__c.addError('You are not authorized to delete handovers related to completed projects.');
		}
	}
	
	//CREATE ATTACHMENT FOLDERS ON HANDOVERS
    public static void AutoCreateAttachmentFolders(List<Handover__c> newList){
        List<Attachment_Folder__c> newFolders = new List<Attachment_Folder__c>();
        Map<String, Custom_Folders__c> customFolders = Custom_Folders__c.getAll();
        
        for(Handover__c h : newList) {
            for(String folderName : customFolders.keySet()){
            	if(customFolders.get(folderName).Parent_Object__c=='Handover__c'){
                	newFolders.add(new Attachment_Folder__c(
                		Handover__c = h.Id,
	                    Name = folderName,
	                    Folder_Description_long__c = 'Currently Accessible By: Retail Project Manager, '
                	));
            	}
            }
        }
        insert newFolders;
        AttachmentFolderHandler.AddHandoverFolderSharing(newFolders);
    }
}
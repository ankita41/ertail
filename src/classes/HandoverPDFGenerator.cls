/*****
*   @Class      :   HandoverPDFGenerator.cls
*   @Description:   XXXXX
*   @Author     :   XXXXX
*   @Created    :   XXXXX
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date           Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy        20 MAY 2014    Change: Render Snag Images from Chatter File rendition instead of the stored Documents
*   Jacky Uy        27 MAY 2014    Updated code to display after removing the Actions object to display Snag list
*   Jacky Uy        06 JUN 2014    Update: added ORDER BY Area__c on Snaglist
*   Priya           22 FEB 2016    Update: added ORDER BY Area_in_asc_order__c ASC on Snaglist                         
*****/

public without sharing class HandoverPDFGenerator {
    public Handover__c handover {get; set;}
    public List<Snag__c> snagList {get;set;}
    String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
    public String nesLogoURL { 
        get {
            Document logoFile = [SELECT Id FROM Document WHERE DeveloperName = 'NES_Boutique_Platform_Logo'];
            return '/servlet/servlet.ImageServer?id=' + logoFile.Id + '&oid=' + UserInfo.getOrganizationId();
        } set;
    }

    public String responsibleName {
        get {
            List<User> tmpUser = [SELECT Name FROM User WHERE Id = :handover.Responsible__c];
            return tmpUser.isEmpty() ? '' : tmpUser[0].Name;
        } set; 
    }
    
      public HandoverPDFGenerator (ApexPages.StandardController stdController) {
        handover = (Handover__c)stdController.getRecord();
        handover = [SELECT Id, Name, Visit_date__c,Notify_approval_decision_date__c, Responsible__c, Retail_project_manager__c, RECO_Project__c, Supplier_contact__c, RECO_Project__r.Boutique__r.Name,
                        RECO_Project__r.Name,Manager_architect__r.name, RECO_Project__r.Project_Name__c,RECO_Project__r.Retail_project_manager__r.Name, Supplier_contact__r.Name
                        FROM Handover__c WHERE Id = :handover.Id];
        String isSendPDF=ApexPages.currentPage().getParameters().get('isSendPDF');  
        if(!String.isBlank(isSendPDF) && handover.Notify_approval_decision_date__c==null)
        {
         handover.Notify_approval_decision_date__c=SYSTEM.Now();
         system.debug('==='+handover.Notify_validation_decision_date__c); 
        
        }
        
        querySnags();
    }
   
    public void sendPdf(String templateName, string targetId, Boolean SendAttachment,Boolean isEmail,Boolean isApprove) {
        EMailTemplate em = [SELECT id, FolderId, ApiVersion,  BrandTemplateId, Description, DeveloperName, Encoding, TemplateStyle, TemplateType, HTMLValue, Name, Body, Markup, Subject 
                    FROM EMailTemplate WHERE DeveloperName =:templateName
                ];
        String insUrl=System.Url.getSalesforceBaseURL().toExternalForm();
        
        /** Fetching Header of template starts here****/   
                
        BrandTemplate brand_value=[Select value from BrandTemplate  where id=:em.BrandTemplateId];
        String bHeader,bFooter,brandvalue=brand_value.value;
        integer i=brandvalue.indexOf('<img');
        integer j=brandvalue.indexOf('</img>');
        bHeader=brandvalue.subString(i,j+6);
        bHeader= bHeader.replace('">','" src=');
        if(bHeader.contains('https'))
            bHeader=bHeader.replace('<![CDATA[','\"');
        else
           bHeader=bHeader.replace('<![CDATA[','\"'+insUrl);    
        bHeader=bHeader.replace(']]','\" ');
        /** Fetching Header of template ends here****/ 
        PageReference pdf = Page.HandoverPdfGenerator;
            // add parent id to the parameters for standardcontroller
            pdf.getParameters().put('id',handover.Id);
            if(isApprove==true)
                pdf.getParameters().put('isSendPDF','1');
            // the contents of the attachment from the pdf
            Blob body;
            
            try {
                // returns the output of the page as a PDF
                body = pdf.getContent();
                
                // need to pass unit test -- current bug  
            } catch (VisualforceException e) {
                body = Blob.valueOf('Some Text');
            }
        
        
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType('application/pdf');
        attach.setFileName('Handover_Checklist.pdf');
        attach.setInline(false);
        attach.Body = body;
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.saveAsActivity = false; 
        //mail.setTemplateId(em.id);
        if(isEmail==false)
            mail.setTargetObjectId(targetId);
        else{
           List<String> toAddresses = new List<String>();
           toAddresses.add(targetId);
           mail.setToAddresses(toAddresses);}
        //mail.setwhatid();
        if(SendAttachment==true)
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
            
        String destName = '';
        if(isEmail==true){
         destname='';
        }else if (targetId.substring(0,3) == '003') {
            // If the target is a Contact, get the Contact's name
            try {
                Contact dest = [SELECT Id, Name FROM Contact WHERE Id = :targetId];
                destName = dest.Name;
            } catch(Exception e) {}
        } else if (targetId.substring(0,3) == '005') {
            // If the target is a User, get the User's name
            try {
                User dest = [SELECT Id, Name FROM User WHERE Id = :targetId];
                destName = dest.Name;
            } catch (Exception e) {} // for testing purposes
        }
            
        RECO_Project__c proj = [SELECT Id, Name, Project_Name__c FROM RECO_Project__c WHERE Id = :handover.RECO_Project__c];
        String htmlBody = em.HTMLvalue;
        if (htmlBody != null) {
            htmlBody = htmlBody.replace('{!Recipient_name}', destName);
            htmlBody = htmlBody.replace('{!RECO_Project__c.Project_Name__c}', proj.Project_Name__c);
            htmlBody = htmlBody.replace('{!RECO_Project__c.Name}', proj.Name);
            
            htmlBody = htmlBody.replace('{!Handover__c.Supplier_contact__c}', handover.Supplier_contact__r.name);
            htmlBody = htmlBody.replace('{!Handover__c.Visit_date__c}', string.valueof(handover.Visit_date__c));
            htmlBody = htmlBody.replace('{!RECO_Project__c.Retail_project_manager__c}', string.valueof(handover.RECO_Project__r.Retail_project_manager__r.Name));
            if(handover.Manager_architect__c!=null){
            htmlBody = htmlBody.replace('{!Handover__c.Manager_architect__c}', handover.Manager_architect__r.name);
            }
            else
            {
            htmlBody = htmlBody.replace('{!Handover__c.Manager_architect__c}', '');
            }
            htmlBody = htmlBody.replace('{!Handover__c.Link}', insUrl+'/'+handover.Id);
            htmlBody = htmlBody.replace('{!RECO_Project__c.Link}', insUrl+'/'+handover.reco_project__c);
         }
        String subj = em.Subject ;
        if (subj != null) {
                subj = subj.replace('{!RECO_Project__c.Project_Name__c}', proj.Project_Name__c);
        }
        htmlBody=htmlBody.replace('<![CDATA[','');
        htmlBody=htmlBody.replace(']]>','');
        bHeader='<style>p{margin-top:0px; margin-bottom:0px;}</style><body class="setupTab"  style=" background-color:#CCCCCC; bEditID:bHeaderst1; bLabel:body;"><center ><table cellpadding="0" width="500" cellspacing="0" id="topTable" height="450" ><tr valign="top" ><td  style=" background-color:#FFFFFF; bEditID:r1st1; bLabel:header; vertical-align:top; height:100; text-align:left;">'+bHeader+'</td></tr><tr valign="top" ><td  style=" background-color:#330000; bEditID:r2st1; bLabel:accent1; height:1;"></td></tr>';
        bHeader+='<tr valign="top" ><td styleInsert="1" height="300"  style=" background-color:#FFFFFF; bEditID:r3st1; color:#000000; bLabel:main; font-size:12pt; font-family:arial;">';
        bFooter='</td></tr><tr valign="top" ><td  style=" background-color:#330000; bEditID:r4st1; bLabel:accent2; height:1;"></td></tr><tr valign="top" ><td  style=" background-color:#FFFFFF; bEditID:r5st1; bLabel:footer; vertical-align:top; height:100; text-align:left;"></td></tr><tr valign="top" ><td  style=" background-color:#330000; bEditID:r6st1; bLabel:accent3; height:1;"></td></tr></table></center><br><br>';
        htmlBody=bHeader+htmlBody+bFooter;
        system.debug('==htmlBody=='+htmlBody);
        mail.setHtmlBody(htmlBody);
        mail.setSubject(subj);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    private void querySnags(){
        Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Snag__c').getDescribe().fields.getMap();
        String qryStr = 'SELECT ';
        for(String s : fieldsMap.keySet()) {
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM Snag__c WHERE Handover__c = \'' + handover.Id + '\' ORDER BY Area_in_asc_order__c ASC';
        snagList = Database.query(qryStr);
    }
}
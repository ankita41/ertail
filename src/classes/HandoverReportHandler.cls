/*****
*   @Class      :   HandoverReportHandler.cls
*   @Description:   Support Handover_Report__c
*                   PLEASE LET WITHOUT SHARING
*   @Author     :   XXXXXXXX
*   @Created    :   XXXXXXXX
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Thibauld        29 SEP 2014     MOVE CreateHandoverChecklistAfterInsert in handler
    Promila         25 May 2015     Updated code of CopyFromProjectToHandoverReport for POS,City,Market for Linked POS
***/
public without sharing class HandoverReportHandler{
    public static void CopyFromProjectToHandoverReport(List<Handover_Report__c> newList){
         //instantiate set to hold unique project record ids
        Set<Id> projectIds = new Set<Id>();
        Set<Id> lnkPOSIds = new Set<Id>();
        for(Handover_Report__c p : newList)
        {
            projectIds.add(p.Project__c);
            lnkPOSIds.add(p.Linked_POS__c); //newly added code for linked POS
            System.debug('--LNKPOS IDs--'+lnkPOSIds);
        }
    
        //instantiate map to hold project record ids to their corresponding ownerids
        Map<Id, Project__c> projectMap = new Map<Id, Project__c>([SELECT Id, MarketName__c, Supplier__c, Project_Manager_HQ__c, Architect__c, Workflow_Type__c, Nespresso_Contact__c, NES_Business_Development_Manager__c, Agent_Local_Manager__c FROM Project__c WHERE Id IN: projectIds]);
        Map<Id,Linked_POS__c>lnkMap=new Map<Id,Linked_POS__c>([SELECT Id,Market__c,Actual_Installation_Date__c,Corner_Point_of_Sale__r.Name,Corner__c from Linked_POS__c where Id IN :lnkPOSIds]);
         List<Linked_POS__c> listPOS= new list<Linked_POS__c>();
        for (Handover_Report__c p : newList) 
        {
            if (projectMap.containsKey(p.Project__c)){
                if (p.Supplier__c == null) 
                {
                    p.Supplier__c = projectMap.get(p.Project__c).Supplier__c;
                }
                if (p.Architect__c == null) 
                {
                    p.Architect__c = projectMap.get(p.Project__c).Architect__c;
                }
                if (p.Nespresso_Contact__c == null) 
                {
                    p.Nespresso_Contact__c = projectMap.get(p.Project__c).Nespresso_Contact__c;
                }
                if (p.NES_Business_Development_Manager__c == null) 
                {
                    p.NES_Business_Development_Manager__c = projectMap.get(p.Project__c).NES_Business_Development_Manager__c;
                }
                if (p.Agent_Local_Manager__c == null) 
                {
                    p.Agent_Local_Manager__c = projectMap.get(p.Project__c).Agent_Local_Manager__c;
                }
                if (p.Project_Manager_HQ__c == null) 
                {
                    p.Project_Manager_HQ__c = projectMap.get(p.Project__c).Project_Manager_HQ__c;
                }
                if (p.Workflow_Type__c == null) 
                {
                    p.Workflow_Type__c = projectMap.get(p.Project__c).Workflow_Type__c;
                }
                if (p.MarketName__c == null) 
                {
                  /*****New code for Linked POS added starts here *****/
                    if(projectMap.get(p.Project__c).MarketName__c!=null){
                    p.MarketName__c = projectMap.get(p.Project__c).MarketName__c;}
                    else{
                      if(lnkMap.containsKey(p.Linked_POS__c))
                      {
                        p.MarketName__c =lnkMap.get(p.Linked_POS__c).Market__c;}
                     }
                  
                }
                if (p.Point_of_Sales_r__c == null && p.Point_of_Sales__c==null) 
                { 
                    if(lnkMap.containsKey(p.Linked_POS__c)){
                       p.Point_of_Sales_r__c =lnkMap.get(p.Linked_POS__c).Corner_Point_of_Sale__r.Name;}
                }
                if (p.Corner__c == null ) 
                { 
                    if(lnkMap.containsKey(p.Linked_POS__c)){
                       p.Corner__c =lnkMap.get(p.Linked_POS__c).Corner__c;}
                }
                if(lnkMap != null && !lnkMap.isEmpty()){
                   Linked_POS__c obj= lnkMap.get(p.Linked_POS__c);
                   if(obj!= null && p.Installer_Departure_Date__c!= null){
                   obj.Actual_Installation_Date__c=p.Installer_Departure_Date__c;
                   obj.status__C ='Handover Report';
                       listPOS.add(obj);
                   }
                //  Installer_Departure_Date__c
                }
                /*****New code for Linked POS added ends here *****/
            }
        }
        
        if( listPOS != null && listPOS.size()>0 ){
            update listPOS;
        }
    }
    
    public static void RecalculateFormulasOnProjectAfterHandoverReportACD(List<Handover_Report__c> hrList){
        Set<Id> projIdSet = new Set<Id>();
        for(Handover_Report__c hr : hrList){
            projIdSet.add(hr.Project__c);
        }
        CornerProjectHandler.RecalculateHandoverReportSummaryFields(projIdSet);
    }

    //This is called from CornerProjectHandler if the Market has changed
    public static void UpdateMarketName(Map<Id, Project__c> projectsMap){
        List<Handover_Report__c> hrList = [Select Id, MarketName__c, Project__c from Handover_Report__c where Project__c in :projectsMap.keySet()];
        List<Handover_Report__c> hrToUpdateList = new List<Handover_Report__c>();
        for(Handover_Report__c hr : hrList){
            hr.MarketName__c = projectsMap.get(hr.Project__c).MarketName__c;
        }
        update hrList;
    }

    public static void CreateHandoverChecklistAfterInsert(List<Handover_Report__c> hrList) {
        Set<Id> projectIds = new Set<Id>();
        for (Handover_Report__c hr : hrList) {        
            // Extract the list of project Ids
            projectIds.add(hr.Project__c);
        }
        
        // get the list of projects where WorkflowType is Agent or Sales Promoter
        List<Project__c> projList = [Select Id from Project__c where Id in :projectIds and Workflow_Type__c in ('Agent', 'Sales Promoter')];
        
        if (!projList.isEmpty()){
            Set<Id> newCornerProjectIds = new Set<Id>();
            for(Project__c p : projList)
                newCornerProjectIds.add(p.Id);
                
            // Get project's Furniture Inventory data        
            List<Furnitures_Inventory__c> FurnitureInventory = [SELECT Id, Furniture__c, Note__c, Quantity__c, Project__c FROM Furnitures_Inventory__c 
                        WHERE Project__c in :newCornerProjectIds];    
                
            // Create a Map with the furnitures
            Map<Id, List<Furnitures_Inventory__c>> furnituresMap = new Map<Id, List<Furnitures_Inventory__c>>();
            for(Furnitures_Inventory__c fi: FurnitureInventory){
                if (!furnituresMap.containsKey(fi.Project__c))
                    furnituresMap.put(fi.Project__c, new List<Furnitures_Inventory__c>());
                furnituresMap.get(fi.Project__c).add(fi);
            }
                            
            List<Handover_Checklist__c> hcToInsert = new List<Handover_Checklist__c>();
            for (Handover_Report__c hr : hrList) {
                if (newCornerProjectIds.contains(hr.Project__c)){
                    // If there is no Furniture Inventory related to the Project, raise an error and stop insertion
                    if (!furnituresMap.containsKey(hr.Project__c) || furnituresMap.get(hr.Project__c).size() == 0) {
                        hr.Project__c.addError('This Project has no Furniture Inventory. A Handover Report can only be created for Projects with non-empty Furniture Inventories.');
                    } else {
                        // 29 SEP 2014 THIS CODE SHOULD BE REFACTORED TO REMOVE THE QUERY FROM THE LOOP
                        // For each inventory item, create a new Handover Checklist
                        for (Furnitures_Inventory__c item : furnituresMap.get(hr.Project__c)) {
                            List<Furniture__c> f = [SELECT Id, Completed_items__c, Electrical_devices__c, Lighting__c FROM Furniture__c WHERE Id = :item.Furniture__c LIMIT 1];
                            
                            Handover_Checklist__c hc = new Handover_Checklist__c();
                            hc.Handover_Report__c = hr.Id;
                            hc.Furnitures_Inventory__c = item.Id;
                            hc.Furniture__c = item.Furniture__c;
                            hc.Note__c = item.Note__c;
                            hc.Quantity__c = item.Quantity__c;
                            
                            if (f.size()>0) {
                                hc.Completed_items__c = f[0].Completed_items__c;
                                hc.Electrical_devices__c = f[0].Electrical_devices__c;
                                hc.Lighting__c = f[0].Lighting__c;
                            }
                            
                            hcToInsert.add(hc);
                        }
                    }
                }
            }
            if (hcToInsert.size() >0)
                insert(hcToInsert);
        }
    }

}
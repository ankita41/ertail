/*****
*   @Class      :   HandoverReportSharingHandler.cls
*   @Description:   Support HandoverReport sharing
*					PLEASE LET WITHOUT SHARING
*   @Author     :   Thibauld
*   @Created    :   31 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
***/
public without sharing class HandoverReportSharingHandler {
	public static void ShareHandoverReports(Map<Id, Handover_Report__c> newMap){
		List<Handover_Report__Share> newSharing = CreateSharingRecords(newMap);
		if (newSharing.size() > 0){
			insert newSharing;
		}
	}
	
	public static void RecalculateSharingAfterUpdate(Map<Id, Handover_Report__c> oldMap, Map<Id, Handover_Report__c> newMap){
		Map<Id, Handover_Report__c> hrToRecalculate = new Map<Id, Handover_Report__c>();
		for (Id hrId : newMap.keySet()){
			if ((oldMap.get(hrId).Architect__c != newMap.get(hrId).Architect__c)
				|| (oldMap.get(hrId).Supplier__c != newMap.get(hrId).Supplier__c))
				hrToRecalculate.put(hrId, newMap.get(hrId));
		}
		
		if (hrToRecalculate.size() > 0) {
			DeleteExistingSharing(hrToRecalculate.keySet());
			ShareHandoverReports(hrToRecalculate);
		}
	}
	
	public static void DeleteExistingSharing(Set<Id> hrIdSet){
		// Locate all existing sharing records for the project records in the batch.
        // Only records using an Apex sharing reason for this app should be returned. 
        List<Handover_Report__Share> oldHtShrs = [SELECT Id FROM Handover_Report__Share WHERE ParentId IN 
             :hrIdSet AND 
            (RowCause = :Schema.Handover_Report__Share.rowCause.IsArchitect__c 
            OR RowCause = :Schema.Handover_Report__Share.rowCause.IsSupplier__c)]; 
        
        // Delete the existing sharing records.
       // This allows new sharing records to be written from scratch.
        Delete oldHtShrs;
	}
	
	public static List<Handover_Report__Share> CreateSharingRecords(Map<Id, Handover_Report__c> hrMap){
		List<Handover_Report__Share> newprojectShrs = new List<Handover_Report__Share>();
		// Construct new sharing records  
        for(Handover_Report__c hr : hrMap.values()){
        	if (hr.Architect__c != null){
	           newprojectShrs.add(CreateArchitectSharing(hr.Architect__c, hr.Id));
        	}
        	if (hr.Supplier__c != null){
	           newprojectShrs.add(CreateSupplierSharing(hr.Supplier__c, hr.Id));
        	}
        }
        return newprojectShrs;
	}

	private static Handover_Report__Share CreateArchitectSharing(Id userId, Id hrId){
		return new Handover_Report__Share(UserOrGroupId = userId
										, AccessLevel = 'Edit'
										, ParentId = hrId
										, RowCause = Schema.Handover_Report__Share.RowCause.IsArchitect__c);
	}
	
	private static Handover_Report__Share CreateSupplierSharing(Id userId, Id hrId){
		return new Handover_Report__Share(UserOrGroupId = userId
										, AccessLevel = 'Edit'
										, ParentId = hrId
										, RowCause = Schema.Handover_Report__Share.RowCause.IsSupplier__c);
	}
}
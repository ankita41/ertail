/*****
*   @Class      :   ImageLoaderController.cls
*   @Description:   Extension developed for ImageLoader.component
*   @Author     :   Thibauld
*   @Created    :   01 JAN 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public virtual class ImageLoaderController {

    public class CustomException extends Exception{}

    public ApexPages.StandardController stdCtrl {get{
        if(stdCtrl == null){
            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(parent);
            return stdCtrl;
        }
        return stdCtrl;
    } set;}
    
    public Boolean refreshPage {get; set;}
    
    public sObject parent {get;set;}

    public String imageId {get;set;}
    public String imageId1 {get;set;}
     public String imageId2 {get;set;}
    public FeedItem feedItem {get{
        
        if(feedItem == null){
            for(FeedItem f : [
                    SELECT ContentFileName, ParentId, RelatedRecordId 
                    FROM FeedItem 
                    WHERE ParentId =: parent.ID AND Type = 'ContentPost' 
                    ORDER BY CreatedDate
                ]){
                feedItem = f;
                break;
            }
            if(feedItem == null){
                feedItem = new FeedItem();
            }
        }
                
        return feedItem;
    }set;}

/*
    public String imageURL{get{
        return '/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB120BY90&versionId=' + feedItem.RelatedRecordId;
    }set;}
*/
    public ImageLoaderController(){
    }
    

    public void saveImage(){
        //CREATES NEW FEED CONTENTS
        FeedItem newImage = null;
        if(feedItem.contentFileName!=null && feedItem.RelatedRecordId==null){           
            feedItem.ParentId = parent.ID;
            newImage = feedItem;
        }
        
        if(newImage != null){
            try{
                insert newImage;
                feedItem = null;
                //STORES CONTENT ID TO THE IMAGE FIELD OF THE OBJECT
                if(newImage != null){
                    parent.put('Image_ID__c',feedItem.RelatedRecordId);
                    //RESETS VIEW STATE
                    imageId = feedItem.RelatedRecordId;

                    newImage = new FeedItem();
                }
                update parent;
                
            }
            catch(DmlException ex){
                
                ApexPages.addMessages(ex);
            }
            finally{
                feedItem = null;
            }

            refreshPage=true;
            stdCtrl.save();
        }
    }

    public void deleteImage(){
        //DELETE IMAGE
        system.debug('@@feeditem'+feedItem);
        if(feedItem.id!=null){
            delete feedItem;
            delete [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :feedItem.RelatedRecordId];
         }
        parent.put('Image_ID__c',null);
        update parent;

        refreshPage = true;
        stdCtrl.save();
    }

}
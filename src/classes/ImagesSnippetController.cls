public virtual class ImagesSnippetController {

	public class CustomException extends Exception {}
	
	private static String DEFAULTIMAGEFIELD = 'Default_Image__c';
	private static String DEFAULTIMAGE2FIELD = 'Default_Image_2__c';
	private static String DEFAULTIMAGE3FIELD = 'Default_Image_3__c';
	
    public sObject obj  { 
    	get; 
    	set { 
        	setObjectType(value); 
    		obj = reLoadObjectWithFieldData();
    		newImage.ParentId = obj.Id;
    		DefaultImageId = (Id)obj.get(DEFAULTIMAGEFIELD);
    		DefaultImage2Id = (Id)obj.get(DEFAULTIMAGE2FIELD);
    		DefaultImage3Id = (Id)obj.get(DEFAULTIMAGE3FIELD);
    		ObjId = (Id)obj.get('Id');
    		queryImages();
    	} 
	}
    private String objType;
        
    public List<FeedItem> images {get;set;}
    
    public FeedItem newImage {get; set;}
    
    public Id DefaultImageId { get; private set;  }
    public Id DefaultImage2Id { get; private set;  }
    public Id DefaultImage3Id { get; private set;  }
    
    public Id ObjId { get; private set; }
    public Boolean refreshParent { get; set; }
    public ImagesSnippetController() {
   		newImage = new FeedItem();
    }
    
	// The sObject type as a string
    public String getObjectType() {
    	  return(this.objType);
    }
    
    public String setObjectType(sObject newObj) {
        this.objType = newObj.getSObjectType().getDescribe().getName();
        return(this.objType);
    }
    
    private sObject reLoadObjectWithFieldData() {
    	String qid = ApexPages.currentPage().getParameters().get('id');
        String theQuery = 'SELECT Id, ' + DEFAULTIMAGEFIELD + ', ' + DEFAULTIMAGE2FIELD + ', ' + DEFAULTIMAGE3FIELD + 
                          ' FROM ' + getObjectType() + 
                          ' WHERE Id = :qid';
        return(Database.query(theQuery));
    }
    
    public PageReference addImage(){
        if(newImage.ContentData!=null){
            insert newImage;
            newImage = new FeedItem(ParentId = obj.Id);
        }
        return reLoad();
    }
    
    public PageReference delImage(){
        Id feedId = ApexPages.currentPage().getParameters().get('feedId');
        delete [SELECT Id FROM FeedItem WHERE Id = :feedId];
        
        Id docId = ApexPages.currentPage().getParameters().get('docId');
        delete [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :docId];
        
        if (docId == DefaultImage2Id){
        	obj.put(DEFAULTIMAGE2FIELD, null);
        	update obj;
        }
        
        if (docId == DefaultImage3Id){
        	obj.put(DEFAULTIMAGE3FIELD, null);
        	update obj;
        }
        
        return reLoad();
    }
    
    public PageReference setDefaultImage(){
        Id docId = ApexPages.currentPage().getParameters().get('docId');
        obj.put(DEFAULTIMAGEFIELD, docId);
        if (docId == DefaultImage2Id){
        	obj.put(DEFAULTIMAGE2FIELD, DefaultImageId);
        }else if (docId == DefaultImage3Id){
        	obj.put(DEFAULTIMAGE3FIELD, DefaultImage2Id);
        	obj.put(DEFAULTIMAGE2FIELD, DefaultImageId);
        }
        update obj;
        return reLoad();
    }
    
    public PageReference setDefaultImage2(){
        Id docId = ApexPages.currentPage().getParameters().get('docId');
        if (docId == DefaultImage3Id)
        	obj.put(DEFAULTIMAGE3FIELD, DefaultImage2Id);

        // Unset if already selected
        if (docId == DefaultImage2Id)
        	obj.put(DEFAULTIMAGE2FIELD, null);
        else
        	obj.put(DEFAULTIMAGE2FIELD, docId);
                 
        update obj;
        return reLoad();
    }
    
    public PageReference setDefaultImage3(){
        Id docId = ApexPages.currentPage().getParameters().get('docId');
        // Unset if already selected
        if (docId == DefaultImage3Id)
        	obj.put(DEFAULTIMAGE3FIELD, null);
        else
        	obj.put(DEFAULTIMAGE3FIELD, docId);
        update obj;
        return reLoad();
    }

    private void queryImages(){
    	Boolean firstFound = false;
    	Boolean secondFound = false;
        images = [SELECT Id, ContentFileName, RelatedRecordId FROM FeedItem WHERE ParentId = :obj.Id AND Type = 'ContentPost'ORDER BY CreatedDate DESC];
        //images = ImageSnippetHelper.GetImages(obj.Id);
        Integer listSize = images.size();
        // Sort the list to have Default Images first
        for(Integer i=0; i<listSize; i++){
        	FeedItem fi = images[i];
        	if (fi.RelatedRecordId == DefaultImageId){
        		images.remove(i);
        		if (images.size() > 0)
        			images.add(0, fi);
        		else
        			images.add(fi);
        		firstFound = true;
        	}else if(fi.RelatedRecordId == DefaultImage2Id){
        		images.remove(i);
        		if (firstFound){
        			if (images.size() > 1)
        				images.add(1, fi);
        			else 
        				images.add(fi);
        		}else{
	        		if (images.size() > 0)
	        			images.add(0, fi);
	        		else
	        			images.add(fi);
        		}
        		secondFound = true;
        	}else if(fi.RelatedRecordId == DefaultImage3Id){
        		images.remove(i);        		
        		if (firstFound && secondFound){
        			if (images.size() > 2)
        				images.add(2, fi);
        			else 
        				images.add(fi);
        		}else if (firstFound || secondFound)
        			if (images.size() > 1)
        				images.add(1, fi);
        			else 
        				images.add(fi);
        		else{
	        		if (images.size() > 0)
	        			images.add(0, fi);
	        		else
	        			images.add(fi);
        		}
        	}
        }
    }
    
    private PageReference reLoad(){
    	ApexPages.currentPage().getParameters().put('id', obj.Id);
    	ApexPages.currentPage().getParameters().put('feedId', null);
    	ApexPages.currentPage().getParameters().put('docId', null);
        return ApexPages.currentPage();
    }
}
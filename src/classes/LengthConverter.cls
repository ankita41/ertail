/**
 *      @description    Class for length conversion
 *      @param          Input: Unit from (cm or inch), Unit to (cm or inch), Length, Output: Converted Length
 *      @return         -1 if any error arises, converted value otherwise 
 */

public with sharing class LengthConverter {
    
    public Double convertLengthFromTo (Double dblValue, String strFromUnitCode, String strToUnitCode) {
        if (strFromUnitCode == '' || strToUnitCode == null || strToUnitCode == '' || dblValue == null || dblValue < 0) {
            return -1;
        }
        
        if (strFromUnitCode == strToUnitCode) {
            return dblValue;
        } else {
                        
            if (strFromUnitCode == 'cm') {
                return dblValue*0.393700787;
            } else {
                return dblValue*2.54;
            }
        }
    }
}
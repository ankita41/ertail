/*****
*   @Class      :   ManageChildObjectsController.cls
*   @Description:   Controller for all the child pages that manages the creation, update and deletion of objects.
*                   Do not use with attachments.
*   @Author     :   JACKY UY
*   @Created    :   06 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*       JACKY UY                06 AUG 2014             Add: setManualSharing() method to set Manual Sharing
*   JACKY UY        15 SEP 2014     Change: Delete functionality to checkboxes
*                         
*****/

public with sharing class ManageChildObjectsController{
    //ADD DEFAULT RECORD TYPES FOR CHILD OBJECTS HERE
    Static String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
    private Map<String,String> mapDefaultObjRTypes = new Map<String,String>();
    // ticket#RM0026301599 : Commented below code
   /*  static {
       
       if(currentUserProfile.contains('NCAFE')){
         mapDefaultObjRTypes.put('Cost_Item__c','NCAFE Expected');
       }
       else
         mapDefaultObjRTypes.put('Cost_Item__c','Expected');
    }*/

    private Id parentId {get;set;}
    private String parentObjName {get;set;}
    public sObject parentObj {get;set;}
    
    private String childObjName {get;set;}
    private Schema.SObjectType childObjType {get;set;}
    private Map<String, Schema.SobjectField> fieldsMap {get;set;}
    
    public Boolean isAsc {get;set;}
    public String orderBy {get;set;}
    public List<SObjectWrapper> objList {get;set;}
    public Map<Id,SObjectWrapper> mapObjWrap {get;set;} //MAP TO EASILY ACCESS A SINGLE RECORD ON objList
    
    //private List<sObject> delObjs {get;set;}
    public Map<Integer,SObjectWrapper> mapExCostCheckbox {get;set;} //MAP TO DISPLAY THE EXTRA COST PICKLIST AS A CHECKBOX
    public Boolean showIsPrivateColumn {get;set;}
    
    public ManageChildObjectsController(){
        parentId = ApexPages.currentPage().getParameters().get('id');
        childObjName = ApexPages.currentPage().getParameters().get('obj');
        orderBy = ApexPages.currentPage().getParameters().get('order');
        
        if(parentId!=NULL && childObjName!=NULL){
            parentObjName = parentId.getSObjectType().getDescribe().getName();
            parentObj = Database.query('SELECT Id, Name,Recordtypeid  FROM ' + parentObjName + ' WHERE Id = \'' + parentId + '\'');
            Id RId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
            Id TypeId = (Id) ParentObj.get('RecordtypeId');
            childObjType = Schema.getGlobalDescribe().get(childObjName);
            system.debug('=====childObjName============='+childObjName);
            system.debug('=====childobjtype============='+childObjType);
            system.debug('=====RId============='+RId);
            system.debug('=====TypeId ============='+TypeId );
            fieldsMap = childObjType.getDescribe().fields.getMap();
            
          //  String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
            // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (Changed below if condition)
            if(RId == typeId){
                 mapDefaultObjRTypes.put('Cost_Item__c','NCAFE Expected');
                 mapDefaultObjRTypes.put('Estimate_Item__c','NCAFE Pending approval');
            }
            else{
                 mapDefaultObjRTypes.put('Cost_Item__c','Expected');
                 mapDefaultObjRTypes.put('Estimate_Item__c','Pending approval');
             }
                isAsc = true;
                queryObjs();
            }
    }
    
    public void queryObjs(){
        String qryStr = 'SELECT ';
        for(String s : fieldsMap.keySet()) {
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        if(UserConstants.CURRENT_USER.Profile.Name!=Label.Design_Architect_Profile && UserConstants.CURRENT_USER.Profile.Name!=Label.Furniture_Supplier_Profile && UserConstants.CURRENT_USER.Profile.Name!=Label.Manager_Architect_Profile && UserConstants.CURRENT_USER.Profile.Name!=UserConstants.NCAFE_DA && UserConstants.CURRENT_USER.Profile.Name!=UserConstants.NCAFE_FSupp && UserConstants.CURRENT_USER.Profile.Name!=UserConstants.NCAFE_MA ){
           qryStr = qryStr.removeEnd(', ') + ' FROM ' + childObjName + ' WHERE ' + parentObjName + ' = \'' + parentObj.Id + '\' ';
       }
       else{
           qryStr = qryStr.removeEnd(', ') + ' FROM ' + childObjName + ' WHERE ' + parentObjName + ' = \'' + parentObj.Id + '\' and Private__c=false '; 
       }
        
        showIsPrivateColumn = true;
        if(qryStr.contains('isprivate__c') && UserConstants.CURRENT_USER.Profile.Name!=UserConstants.NCAFE_RPM && UserConstants.CURRENT_USER.Profile.Name!=UserConstants.BTQ_RPM && UserConstants.CURRENT_USER.Profile.Name!=UserConstants.SYS_ADM){
            //qryStr = qryStr + 'AND IsPrivate__c = false ';
            showIsPrivateColumn = false;
        }
       
        
        if(orderBy!=null)    
            qryStr = qryStr + 'ORDER BY ' + orderBy + ' ' + ((isAsc) ? 'ASC' : 'DESC');

        SObjectWrapper sObjWrap;
        objList = new List<SObjectWrapper>();
        mapObjWrap = new Map<Id,SObjectWrapper>();
        
        for(sObject o : Database.query(qryStr)){
            sObjWrap = new SObjectWrapper();
            sObjWrap.objId = o.Id;
            sObjWrap.obj = o;
            objList.add(sObjWrap);
            
            mapObjWrap.put(o.Id, sObjWrap);
        }
        
        //IF SOBJECT HAS AN ACCESSIBLE PICKLIST FIELD CALLED EXTRA COST
        if(qryStr.contains('extra_cost__c') && fieldsMap.get('extra_cost__c').getDescribe().getType() == Schema.DisplayType.PICKLIST){
            for(SObjectWrapper o : objList){
                o.isExtraCost = (o.obj.get('Extra_Cost__c') == 'Yes') ? true : false;
            }
        }
            
        //delObjs = new List<sObject>();
    }
    
    public void addRow(){
        SObject newSObj = childObjType.newSObject();
        newSObj.put(parentObjName, parentObj.Id);
        
        if(mapDefaultObjRTypes.containsKey(childObjName)){
            Id rTypeId = childObjType.getDescribe().getRecordTypeInfosByName().get(mapDefaultObjRTypes.get(childObjName)).getRecordTypeId();
            newSObj.put('RecordTypeId', rTypeId);
        }
        
        SObjectWrapper sObjWrap = new SObjectWrapper();
        sObjWrap.obj = newSObj;
        sObjWrap.isExtraCost = false;
        sObjWrap.isSelected = false;
        objList.add(sObjWrap);
    }
    
    public void delSelectedRows(){
        Set<Id> selectedObjs = new Set<Id>();
        for(Id objId : mapObjWrap.keySet()){
            if(mapObjWrap.get(objId).isSelected)
                selectedObjs.add(objId);
        }
        
        try{
                List<sObject> delObjs = Database.query('SELECT Id FROM ' + childObjName + ' WHERE Id IN :selectedObjs');
                delete delObjs;
                queryObjs();
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
    }
    
    /*public void delRow(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        delRow(index);
    }
    
    private void delRow(Integer index){
        if(objList[index].objId!=null)
            delObjs.add(objList[index].obj);

        objList.remove(index);
    }*/
    
    //SAVES CHANGES TO RECORDS
    public void save(){
        List<sObject> newObjs = new List<sObject>();
        List<sObject> updObjs = new List<sObject>();
        
        for(SObjectWrapper o : objList){
            if(fieldsMap.containsKey('extra_cost__c') && fieldsMap.get('extra_cost__c').getDescribe().isAccessible())
                o.obj.put('Extra_Cost__c', (o.isExtraCost) ? 'Yes' : 'No');
            
            if(o.objId==null)  
                newObjs.add(o.obj);
            else
                updObjs.add(o.obj);
        }
        
        try{
            insert newObjs;
            update updObjs;
            //delete delObjs;
            
            queryObjs();
            
            //SET MANUAL SHARING SETTINGS
            //setManualSharing();
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
    }

    public PageReference saveClose(){
        save();
        
        if(ApexPages.hasMessages())
            return null;
        else
            return close();
    }
    
    public PageReference close(){
        return new PageReference('/' + parentObj.Id);
    }
    
    //WRAPPER CLASS TO CUSTOMIZE THE SOBJECT
    //USE WHEN NECESSARY LIKE ADDING PROPERTIES
    public class SObjectWrapper{
        public Id objId {get;set;}
        public SObject obj {get;set;}
        public Boolean isExtraCost {get;set;} //USED ONLY FOR THE 'EXTRA_COST__C' FIELD
        public Boolean isSelected {get;set;}
        public SObjectWrapper(){}
    }
    
    //MANUAL SHARING SETTINGS FOR BOUTIQUE PROJECT CHILD RECORDS ONLY
    /*private void setManualSharing(){
        SYSTEM.DEBUG('setManualSharing');
        if(parentObjName=='RECO_Project__c' && fieldsMap.containsKey('isprivate__c') && fieldsMap.get('isprivate__c').getDescribe().isAccessible()){
                String sObjectShareName = childObjName.replace('__c','__Share');
                Set<Id> existingShares = new Set<Id>();
                for(sObject o : Database.query('SELECT ParentId FROM ' + sObjectShareName + ' WHERE ParentId IN :mapObjWrap.keySet() AND RowCause!=\'Owner\'')){
                        existingShares.add((Id)o.get('ParentId'));
                }
                
                Set<Id> stakeholdersIds = new Set<Id>();
                for(Stakeholder__c s : [SELECT Platform_user__c FROM Stakeholder__c WHERE RECO_Project__c = :parentId]){
                        stakeholdersIds.add(s.Platform_user__c);
                }
                
                SObject newSObjShare;
                List<sObject> newShareObjs = new List<sObject>();
                Set<Id> delShareObjsParents = new Set<Id>();
                for(SObjectWrapper o : objList){
                        if(!existingShares.contains(o.objId) && o.obj.get('IsPrivate__c') == false){
                                for(Id sid : stakeholdersIds){
                                        if(sid!=o.obj.get('OwnerId')){
                                                newSObjShare = Schema.getGlobalDescribe().get(sObjectShareName).newSObject();
                                                newSObjShare.put('ParentId', o.objId);
                                                newSObjShare.put('AccessLevel', 'Edit');
                                                newSObjShare.put('UserOrGroupId', sid);
                                                newShareObjs.add(newSObjShare);
                                        }
                                }
                        }
                        else if(existingShares.contains(o.objId) && o.obj.get('IsPrivate__c') == true){
                                if(o.obj.get('OwnerId')==UserInfo.getUserId())
                                        delShareObjsParents.add(o.objId);
                        }
                }
                insert newShareObjs;
                
                List<sObject> delShareObjs = Database.query('SELECT ParentId FROM ' + sObjectShareName + ' WHERE ParentId IN :delShareObjsParents AND RowCause!=\'Owner\'');
                delete delShareObjs;
                queryObjs();
        }
    }*/
}
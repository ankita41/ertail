/*****
*   @Class      :   ManageChildObjectsControllerTest.cls
*   @Description:   Test coverage for the ManageChildObjectsController.cls
*   @Author     :   JACKY UY
*   @Created    :   05 JUN 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   JACKY UY        06 JUN 2014     ManageChildObjectsController.cls Test Coverage at 94%
*   JACKY UY        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   JACKY UY        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*   JACKY UY        15 SEP 2014     Change: Delete functionality to checkboxes
*   JACKY UY        15 SEP 2014     ManageChildObjectsController.cls Test Coverage at 94%
*
*****/

@isTest
private with sharing class ManageChildObjectsControllerTest{
    //TEST ManageCostItems PAGE
    static testMethod void unitTest(){
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        PageReference pageRef = Page.ManageEstimateItems;
        pageRef.getParameters().put('id', bProj.Id);
        pageRef.getParameters().put('obj', 'Cost_Item__c');
        pageRef.getParameters().put('order', 'CreatedDate');
        Test.setCurrentPage(pageRef);
        
        ManageChildObjectsController con = new ManageChildObjectsController();
        
        con.addRow();
        con.objList[0].obj.put('Level_1__c', 'Fees');
        con.objList[0].obj.put('Expected_amount__c', 1000);
        con.objList[0].obj.put('Currency__c', 'CHF');
        con.save();
        
        SYSTEM.Assert(([SELECT Id FROM Cost_Item__c WHERE RECO_Project__c = :bProj.Id]).size() == 1);
        
        con.objList[0].obj.put('Expected_amount__c', 2000);
        con.save();
        
        SYSTEM.Assert([SELECT Expected_amount__c FROM Cost_Item__c WHERE Id = :con.objList[0].objId].Expected_amount__c == 2000);
        
        con.addRow();
        con.objList[1].obj.put('Level_1__c', 'Nespresso supplied items');
        con.objList[1].obj.put('Expected_amount__c', 1000);
        con.objList[1].obj.put('Currency__c', 'CHF');
        con.save();
        
        //pageRef.getParameters().put('index', '0');
        //Test.setCurrentPage(pageRef);
        //con.delRow(); 
        //con.delRow();
        con.mapObjWrap.get(con.objList[0].objId).isSelected = true;
        con.mapObjWrap.get(con.objList[1].objId).isSelected = true;
        con.delSelectedRows();
        con.saveClose();
        
        SYSTEM.Assert(([SELECT Id FROM Cost_Item__c WHERE RECO_Project__c = :bProj.Id]).size() == 0);
    }
}
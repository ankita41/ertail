/*****TO BE DELETED (JUY 08/09) REPLACED BY ManageUntrackedCostsController.cls
*   @Class      :   ManageObjectsController.cls
*   @Description:   Controller for all the pages that manages the creation, update and deletion of objects.
*   @Author     :   Jacky Uy
*   @Created    :   06 AUG 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*	JACKY UY		17 AUG 2014		Change Market lookup inputField to a selectList due to bug when rerendering the page.
*	JACKY UY		18 AUG 2014		Added filterBy and filterStrVal parameters.
*                         
*****/

public with sharing class ManageObjectsController {
    /*private String objName {get;set;}
    private Schema.SObjectType objType {get;set;}
    private Map<String, Schema.SobjectField> fieldsMap {get;set;}
    
    private String filterBy {get;set;}
    private String filterStrVal {get;set;}
    private String orderBy {get;set;}
    private Boolean isAsc {get;set;}
    
    public List<SelectOption> marketOpts {get;set;}
    public sObject obj {get;set;}
    public List<SObjectWrapper> objList {get;set;}
    
    private List<sObject> delObjs {get;set;}
    
    public ManageObjectsController(){
    	objName = ApexPages.currentPage().getParameters().get('obj');
    	orderBy = ApexPages.currentPage().getParameters().get('order');
    	filterBy = ApexPages.currentPage().getParameters().get('filterBy');
    	filterStrVal = ApexPages.currentPage().getParameters().get('filterStrVal');
    	
    	objType = Schema.getGlobalDescribe().get(objName);
    	fieldsMap = objType.getDescribe().fields.getMap();
        isAsc = true;
        queryMarkets();
        queryObjs();
    }
    
    public void queryMarkets(){
        marketOpts = new List<SelectOption>();
        for(Market__c m : Database.query('SELECT Name FROM Market__c ORDER BY Name')){
            marketOpts.add(new SelectOption(m.Id,m.Name));
        }
    }
    
    public void queryObjs(){
        String qryStr = 'SELECT ';
        for(String s : fieldsMap.keySet()) {
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM ' + objName + ' ';
        
        if(filterStrVal!=null && filterBy!=null)
        	qryStr = qryStr + 'WHERE ' + filterBy + ' = :filterStrVal ';
        
        if(orderBy!=null)    
            qryStr = qryStr + 'ORDER BY ' + orderBy + ' ' + ((isAsc) ? 'ASC' : 'DESC');

        SObjectWrapper sObjWrap;
        objList = new List<SObjectWrapper>();
        
        for(sObject o : Database.query(qryStr)){
            sObjWrap = new SObjectWrapper();
            sObjWrap.objId = o.Id;
            sObjWrap.obj = o;
            objList.add(sObjWrap);
        }
        
        delObjs = new List<sObject>();
    }
    
    public void addRow(){
        SObject newSObj = objType.newSObject();
        
        if(filterStrVal!=null && filterBy!=null)
        	newSObj.put(filterBy, filterStrVal);
        
        SObjectWrapper sObjWrap = new SObjectWrapper();
        sObjWrap.obj = newSObj;
        objList.add(sObjWrap);
    }
    
    public void delRow(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        delRow(index);
    }
    
    private void delRow(Integer index){
        if(objList[index].objId!=null)
            delObjs.add(objList[index].obj);

        objList.remove(index);
    }
    
    //SAVES CHANGES TO RECORDS
    public void save(){
        List<sObject> newObjs = new List<sObject>();
        List<sObject> updObjs = new List<sObject>();
        
        for(SObjectWrapper o : objList){
            if(o.objId==null)  
                newObjs.add(o.obj);
            else
                updObjs.add(o.obj);
        }
        
        try{
            insert newObjs;
            update updObjs;
            delete delObjs;
            queryObjs();
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
    }

    public PageReference saveClose(){
        save();
        
        if(ApexPages.hasMessages())
            return null;
        else
            return close();
    }
    
    public PageReference close(){
    	Schema.DescribeSObjectResult objDesc = objType.getDescribe();
    	PageReference pgRef = new PageReference('/' + objDesc.getKeyPrefix());
		pgRef.setRedirect(true);
		return pgRef;
    }
    
    //WRAPPER CLASS TO CUSTOMIZE THE SOBJECT
    //USE WHEN NECESSARY LIKE ADDING PROPERTIES
    public class SObjectWrapper{
        public Id objId {get;set;}
        public SObject obj {get;set;}
        public SObjectWrapper(){}
    }*/
}
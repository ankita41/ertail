/*****TO BE DELETED (JUY 08/09) REPLACED BY ManageUntrackedCostsControllerTest.cls
*   @Class      :   ManageObjectsControllerTest.cls
*   @Description:   Test coverage for the ManageObjectsController.cls
*   @Author     :   Jacky Uy
*   @Created    :   07 AUG 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*
*****/

@isTest
private class ManageObjectsControllerTest {
	//Manage Untracked Costs Page
    /*static testMethod void unitTest(){
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        PageReference pageRef = Page.ManageUntrackedCosts;
        pageRef.getParameters().put('obj', 'Untracked_Cost__c');
        pageRef.getParameters().put('order', 'Market__c');
        Test.setCurrentPage(pageRef);
        
        ManageObjectsController con = new ManageObjectsController();
        
        con.addRow();
        con.objList[0].obj.put('Year__c', '2014');
        con.objList[0].obj.put('Market__c', market.Id);
        con.objList[0].obj.put('Description__c', 'This is a Test.');
        con.objList[0].obj.put('Planned_OP__c', 1000);
        con.objList[0].obj.put('Latest_DF__c', 1000);
        con.save();
        
        SYSTEM.Assert(([SELECT Id FROM Untracked_Cost__c]).size() == 1);
        
        con.objList[0].obj.put('Description__c', 'This is a New Test.');
        con.save();
        
        SYSTEM.Assert([SELECT Description__c FROM Untracked_Cost__c WHERE Id = :con.objList[0].objId].Description__c == 'This is a New Test.');
        
        con.addRow();
        con.objList[0].obj.put('Year__c', '2014');
        con.objList[0].obj.put('Market__c', market.Id);
        con.objList[0].obj.put('Description__c', 'This is a Test.');
        con.objList[0].obj.put('Planned_OP__c', 1000);
        con.objList[0].obj.put('Latest_DF__c', 1000);
        
        pageRef.getParameters().put('index', '0');
        Test.setCurrentPage(pageRef);
        con.delRow();
        con.delRow();
        con.saveClose();
        
        SYSTEM.Assert(([SELECT Id FROM Untracked_Cost__c]).size() == 0);
    }*/
}
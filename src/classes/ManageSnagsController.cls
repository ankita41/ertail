/*****
*   @Class      :   ManageSnagsController.cls
*   @Description:   Controller for the creation, update and deletion of Snags.
*   @Author     :   Jacky Uy
*   @Created    :   28 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        03 JUN 2014     Updated code to ORDER BY Area__c when redirecting the ManageSnagImages page.
*
*                         
*****/

public class ManageSnagsController{
    public String userAgent{get;set;}
    public Id parentId {get;set;}
    public Handover__c handover {get;set;}
    
    public Boolean isAsc {get;set;}
    public String orderBy {get;set;}
    private Map<Id,Snag__c> mapSnagList {get;set;} //MAP TO EASILY ACCESS A SINGLE RECORD ON snagList
    
    public List<Snag__c> snagList {get;set;}
    public Map<Id,FeedItem> mapFeed {get;set;}
    private List<Snag__c> delObjs {get;set;}
    
    public boolean IEtrue{get;set;}
    public boolean IEfalse{get;set;}
    public ManageSnagsController(){
        IEtrue=false;
        IEfalse=false;
        parentId = ApexPages.currentPage().getParameters().get('id');
        orderBy = ApexPages.currentPage().getParameters().get('order');
	//ApexPages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=10');
	//Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
	Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
	String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
	if(String.isNotBlank(userAgent)){
		if(userAgent.contains('Trident')){
		    IEtrue=true;
		    IEfalse=false;
		}
		else{
		     IEtrue=false;
		    IEfalse=true;
		}
	}
	system.debug('===========userAgent====');
        
         Snag__c snagRec=new Snag__c();        
          if(String.isBlank(orderBy)){        
           orderBy='Area__c';       
           snagRec=[Select id,handover__c from snag__c where id=:parentId];     
           parentId=snagRec.handover__c;        
        }
        
         if(String.isNotEmpty(parentId)){
            handover = Database.query('SELECT Id, Name FROM Handover__c WHERE Id = \'' + parentId + '\'');
            isAsc = true;
            queryObjs();
        }
        List<Attachment_folder__c> attRec=[Select id,handover__c from Attachment_folder__c where handover__c =:parentId and name=:'Snags'];
        if(!attRec.isEmpty()){
            snagId=attRec[0].id;
        }
    }
    
    public void queryObjs(){
        Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Snag__c').getDescribe().fields.getMap();
        String qryStr = 'SELECT ';
        for(String s : fieldsMap.keySet()) {
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM Snag__c WHERE Handover__c = \'' + handover.Id + '\' ';
        
        if(orderBy!=null)    
            qryStr = qryStr + 'ORDER BY ' + orderBy + ' ' + ((isAsc) ? 'ASC' : 'DESC');

        snagList = Database.query(qryStr);
        
        mapSnagList = new Map<Id,Snag__c>();
        for(Snag__c s : snagList){
            mapSnagList.put(s.Id, s);
        }
        
        queryFeeds();
        delObjs = new List<Snag__c>();
    }
    
    private void queryFeeds(){
        mapFeed = new Map<Id,FeedItem>();
        for(Snag__c s : snagList){
            mapFeed.put(s.Id, new FeedItem());
        }
        
        for(FeedItem f : [
                SELECT ContentFileName, ParentId, RelatedRecordId 
                FROM FeedItem 
                WHERE ParentId IN :mapFeed.keySet() AND Type = 'ContentPost' 
                ORDER BY CreatedDate
            ]){
            mapFeed.put(f.ParentId, f);
        }
    }
    
    public void addRow(){
        snagList.add(new Snag__c(Handover__c = handover.Id));
    }
    
    public void delRow(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        
        if(snagList[index].Id!=null)
            delObjs.add(snagList.remove(index));
        else
            snagList.remove(index);
    }

    public void save(){
        try{
            upsert snagList;
            delete delObjs;
            saveImages();
            queryObjs();
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
    }
    
    public PageReference next(){
        orderBy = 'Area__c';
        isAsc = true;
        save();
        return Page.ManageSnagImages;
    }
    
    public PageReference back(){
        queryFeeds();
        return Page.ManageSnags;
    }
    
    public void saveImages(){
        //CREATES NEW FEED CONTENTS
        List<FeedItem> newImages = new List<FeedItem>();
        for(Id objId : mapFeed.keySet()){
            if(mapFeed.get(objId).ContentFileName!=null && mapFeed.get(objId).RelatedRecordId==null){
                mapFeed.get(objId).ParentId = objId;
                newImages.add(mapFeed.get(objId));
            }
        }

        try{
            insert newImages;
            queryFeeds();
            
            //STORES CONTENT IDs TO THE IMAGE FIELD OF THE OBJECT
            if(!newImages.isEmpty()){
                for(Snag__c s : snagList){
                    s.Image_ID__c = mapFeed.get(s.Id).RelatedRecordId;
                }
                //RESETS VIEW STATE
                newImages = new List<FeedItem>();
            }
            upsert snagList;
            queryObjs();
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
    }
    public string snagId{get;set;}
    public void insertSnags()
    {
               Snag__c sngRec=new Snag__c();
               //sngRec.Image_ID__c=newFeedContent.Id;
               sngRec.Handover__c=Handover.Id;
               insert sngRec;
               snagId=sngRec.id;
    }
    public void delImgCommit(){
        Id imgParentId = ApexPages.currentPage().getParameters().get('imgParentId');
        //DELETE IMAGE
        delete mapFeed.get(imgParentId);
        delete [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :mapFeed.get(imgParentId).RelatedRecordId];

        //UPDATE PARENT RECORD
        mapSnagList.get(imgParentId).Image_ID__c = null;
        update mapSnagList.get(imgParentId);
        queryObjs();
    }

    public PageReference saveClose(){
        save();
        return close();
    }
    
    public PageReference close(){
        return new PageReference('/' + handover.Id);
    }
}
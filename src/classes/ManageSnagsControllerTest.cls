/*****
*   @Class      :   ManageSnagsControllerTest.cls
*   @Description:   Test coverage for the ManageSnagsController.cls
*   @Author     :   Jacky Uy
*   @Created    :   29 JUN 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        29 JUN 2014     ManageSnagsController.cls Test Coverage at 95%
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*
*****/

@isTest
private class ManageSnagsControllerTest {
    static testMethod void myUnitTest() {
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Handover__c.getRecordTypeInfosByName().get('Construction').getRecordTypeId();
        Handover__c ho = TestHelper.createHandover(rTypeId, bProj.Id);
        insert ho;
        
        PageReference pageRef = Page.ManageSnags;
        pageRef.getParameters().put('id', ho.Id);
        pageRef.getParameters().put('order', 'Area__c');
        Test.setCurrentPage(pageRef);
        
        ManageSnagsController con = new ManageSnagsController();
        con.addRow();
        con.addRow();
        con.addRow();
        
        pageRef.getParameters().put('index', '0');
        con.delRow();
        con.next();
        con.back();
        con.delRow();
        con.next();
        
        con.mapFeed.get(con.snagList[0].Id).ContentFileName = 'Test_File1.jpg';
        con.mapFeed.get(con.snagList[0].Id).ContentData = Blob.valueOf('Unit Testing');
        con.saveImages();
        
        pageRef.getParameters().put('imgParentId', con.snagList[0].Id);
        con.delImgCommit();
        con.saveClose();
    }
}
/*****
*   @Class      :   ManageStakeholdersController.cls
*   @Description:   Controller for the Manage Stakeholders VF Page.
*   @Author     :   Jacky Uy
*   @Created    :   14 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        23 JUL 2014     Added More Support Stakeholders and Grouped Stakeholders by Function
*   Jacky Uy        06 AUG 2014     Transferred the copyStakeholderToBTQProject method from the StakeholderHandler
*   Jacky Uy        03 SEP 2014     Added Attachment Folder sharing when saving
*                         
*****/

public without sharing class ManageStakeholdersController {
    private final Set<String> EXTERNAL_CONTACTS = new Set<String>{'Furniture Supplier','Lighting Designer','Lighting Supplier','General Contractor'};
    public RECO_Project__c parent {get;set;}
    public String notifiedUsers {get;set;}
    
    public Map<String,StakeholderWrapper> mapStakeholders {get;set;}
    public List<String> mainStakeholders {get;set;}
    public List<String> supportStakeholders {get;set;}
    public List<String> supplierStakeholders {get;set;}
    
    private Id userRTypeId = Schema.sObjectType.Stakeholder__c.getRecordTypeInfosByName().get('Platform user').getRecordTypeId();
    private Id conRTypeId = Schema.sObjectType.Stakeholder__c.getRecordTypeInfosByName().get('External contact').getRecordTypeId();

    public ManageStakeholdersController(){
        parent = [SELECT Name, RecordTypeId,Recordtype.Name FROM RECO_Project__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        populateMap();
        
        String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        
        notifiedUsers = '';
        
        // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (Changed below if condition)
        //if(currentUserProfile.contains('NCAFE')){
        if(Parent.Recordtype.Name.contains('NCafe')){
            for(User u : [SELECT Name FROM User WHERE IsActive=TRUE AND (Profile.Name='NES NCAFE Property Manager' OR Profile.Name='NES NCAFE Marketing') ORDER BY Name]){
                notifiedUsers += u.Name + ', ' ;
            }
        }
        else{
            for(User u : [SELECT Name FROM User WHERE IsActive=TRUE AND (Profile.Name='NES RECO Property Manager' OR Profile.Name='NES RECO Marketing') ORDER BY Name]){
                notifiedUsers += u.Name + ', ' ;
            }
        }
        
        
        notifiedUsers = notifiedUsers.removeEnd(', ');
    }
    
    private void populateMap(){
        mapStakeholders = new Map<String,StakeholderWrapper>();
        mainStakeholders = new List<String>();
        supportStakeholders = new List<String>();
        supplierStakeholders = new List<String>();
        
        Schema.DescribeFieldResult functionDescResult = Stakeholder__c.Function__c.getDescribe();
        
        for(Schema.PicklistEntry plEntry : functionDescResult.getPicklistValues()){
            if(plEntry.isActive()){
                mapStakeholders.put(plEntry.getLabel(), new StakeholderWrapper());
                mapStakeholders.get(plEntry.getLabel()).stakeholder.RECO_Project__c = parent.Id;
                mapStakeholders.get(plEntry.getLabel()).stakeholder.Function__c = plEntry.getLabel();
                
                //SUPPLIERS
                if(EXTERNAL_CONTACTS.contains(plEntry.getLabel())){
                    mapStakeholders.get(plEntry.getLabel()).stakeholder.RecordTypeId = conRTypeId;
                    mapStakeholders.get(plEntry.getLabel()).isContact = true;
                    supplierStakeholders.add(plEntry.getLabel());
                }
                
                //SUPPORT STAKEHOLDERS
                else if(plEntry.getLabel().startswith('Support')){
                    mapStakeholders.get(plEntry.getLabel()).stakeholder.RecordTypeId = userRTypeId;
                    mapStakeholders.get(plEntry.getLabel()).isContact = false;
                    supportStakeholders.add(plEntry.getLabel());
                }
                
                //MAIN STAKEHOLDERS
                else{
                    mapStakeholders.get(plEntry.getLabel()).stakeholder.RecordTypeId = userRTypeId;
                    mapStakeholders.get(plEntry.getLabel()).isContact = false;
                    
                    if(!plEntry.getLabel().endswith('Architect') && !plEntry.getLabel().contains('Supplier')){
                        mapStakeholders.get(plEntry.getLabel()).stakeholder.Grant_Access__c = true;
                        mapStakeholders.get(plEntry.getLabel()).stakeholder.Notify__c = true;
                    }
                    
                    mainStakeholders.add(plEntry.getLabel());
                }
            }
        }
        
        //EXISTING STAKEHOLDERS
        for(Stakeholder__c s : [
                SELECT Name, RecordTypeId, Function__c, Platform_user__c, Contact__c, RECO_Project__c, Grant_Access__c, Notify__c, Notified_Date__c 
                FROM Stakeholder__c 
                WHERE RECO_Project__c = :parent.Id
            ]){
                
            mapStakeholders.get(s.Function__c).stakeholder = s;
            if(!EXTERNAL_CONTACTS.contains(s.Function__c) && !s.Function__c.startswith('Support') && !s.Function__c.endswith('Architect') && !s.Function__c.contains('Supplier')){
                mapStakeholders.get(s.Function__c).stakeholder.Grant_Access__c = true;
                mapStakeholders.get(s.Function__c).stakeholder.Notify__c = true;
            }
        }
    }
    
    public void save(){
        List<Stakeholder__c> upsStakeholders = new List<Stakeholder__c>();
        List<Stakeholder__c> delStakeholders = new List<Stakeholder__c>();
        for(StakeholderWrapper sw : mapStakeholders.values()){
            if(sw.stakeholder.RecordTypeId==userRTypeId)
                sw.stakeholder.Notify__c = sw.stakeholder.Grant_Access__c;
            
            if(!sw.stakeholder.Notify__c)
                sw.stakeholder.Notified_Date__c = NULL;
                
            if(sw.stakeholder.RecordTypeId==userRTypeId && sw.stakeholder.Platform_user__c!=NULL){
                //NOT BEST PRACTICE FOR SOQL STATEMENT IN FOR LOOP BUT NECESSARY
                sw.stakeholder.Name = [SELECT Name FROM User WHERE Id = :sw.stakeholder.Platform_user__c].Name;
                upsStakeholders.add(sw.stakeholder);
            }
            else if(sw.stakeholder.RecordTypeId==conRTypeId && sw.stakeholder.Contact__c!=NULL){
                //NOT BEST PRACTICE FOR SOQL STATEMENT IN FOR LOOP BUT NECESSARY
                sw.stakeholder.Name = [SELECT Name FROM Contact WHERE Id = :sw.stakeholder.Contact__c].Name;
                upsStakeholders.add(sw.stakeholder);
            }
            else if(sw.stakeholder.Id!=NULL)
                delStakeholders.add(sw.stakeholder);
        }
        upsert upsStakeholders;
        delete delStakeholders;
        
        copyStakeholderToBTQProject(parent.Id);
        AttachmentFolderHandler.AddBTQProjFolderSharing(parent.Id);
        /*** Added below code for req #99 starts here *****/
        try{
            DeleteSharingforDeletedStakeholders();
            CreateSharingForLinkedPurchaseOrders();
            ShareFileToStakeholders();
        } 
        Catch(exception e){
            system.debug('===EXCEPTION ==='+e);}
        /*** Added below code for req #99 ends here *****/
        populateMap();
    }
    /*** Added below code for req #99 starts here *****/
    /*** Below code share the existing files to newly updated stakeholders *****/
    public void ShareFileToStakeholders(){
      Map<Id,Offer__c>allOffers=new Map<Id,Offer__c>([Select Id from Offer__c where Reco_Project__c=:parent.Id]);
        if(!allOffers.isEmpty())
        { 
          Map<Id,Attachment_Folder__c>allFolders=new Map<Id,Attachment_Folder__c>([SELECT Name, Offer__c,Offer__r.RECO_Project__c,Offer__r.RECO_Project__r.Other_Retail_Project_Manager__c,Offer__r.RECO_Project__r.Retail_Project_Manager__c, Offer__r.RECO_Project__r.Support_National_boutique_manager__c,Offer__r.RECO_Project__r.National_boutique_manager__c,Offer__r.RECO_Project__r.PO_operator__c, Offer__r.RECO_Project__r.PO_operator__r.Name, Offer__r.RECO_Project__r.Support_PO_Operator__c, Offer__r.RECO_Project__r.Support_PO_Operator__r.Name FROM Attachment_Folder__c WHERE Name='Budget & Costs' and Offer__c IN :allOffers.keySet()]);
          if(!allFolders.isEmpty())
          {
            List<Attachment_Folder__c>allAttchFold=allFolders.values();
            Attachment_Folder__c mainFolder=allAttchFold[0];
            Set<Id>relIDS=new Set<Id>();
             for(Attachment_Folder__feed attchFeeds:[Select Id,ParentId,RelatedRecordId from Attachment_Folder__feed where Type='ContentPost' and ParentId IN :allFolders.keySet()]){
                 relIDS.add(attchFeeds.RelatedRecordId);
             }
             Map<Id,Attachment_SubFolder__c> attachFolderRecs=new Map<Id,Attachment_SubFolder__c>([Select id,Name,Attachment_Folder__c from Attachment_SubFolder__c where Attachment_Folder__r.Name='Budget & Costs' and Attachment_Folder__c IN :allFolders.keySet()]);
             if(!attachFolderRecs.isEmpty()){
                 for(Attachment_SubFolder__feed attchSubFeeds:[Select Id,ParentId,RelatedRecordId from Attachment_SubFolder__feed where Type='ContentPost' and createdDate=today and ParentId IN :allFolders.keySet()]){
                   relIDS.add(attchSubFeeds.RelatedRecordId);
                 }
             }
             if(!relIDS.isEmpty() || Test.IsrunningTest()){
                List<ContentDocumentLink> newCDLs = new List<ContentDocumentLink>();
                for(ContentDocument conDoc :[SELECT Id, ParentId, PublishStatus FROM ContentDocument WHERE LatestPublishedVersionId IN :relIDS]){
                     if(mainFolder.Offer__r.RECO_Project__r.PO_operator__c!=NULL){
                        newCDLs.add(new ContentDocumentLink(
                            LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.PO_operator__c,
                            ContentDocumentId = conDoc .Id,
                            ShareType = 'C'
                        ));
                    }
            
                    if(mainFolder.Offer__r.RECO_Project__r.Support_PO_Operator__c!=NULL  ){
                        newCDLs.add(new ContentDocumentLink(
                            LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.Support_PO_Operator__c,
                            ContentDocumentId = conDoc.Id,
                            ShareType = 'C'
                        ));
                    }
                   
                    if(mainFolder.Offer__r.RECO_Project__r.National_boutique_manager__c!=NULL  ){
                        newCDLs.add(new ContentDocumentLink(
                            LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.National_boutique_manager__c,
                            ContentDocumentId = conDoc.Id,
                            ShareType = 'V'
                        ));
                    }
                    if(mainFolder.Offer__r.RECO_Project__r.Support_National_boutique_manager__c!=NULL ){
                          newCDLs.add(new ContentDocumentLink(
                            LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.Support_National_Boutique_Manager__c,
                            ContentDocumentId = conDoc.Id,
                            ShareType = 'V'
                        ));
                    }
                    if(mainFolder.Offer__r.RECO_Project__r.Retail_project_manager__c!=NULL ){
                        newCDLs.add(new ContentDocumentLink(
                            LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.Retail_project_manager__c,
                            ContentDocumentId = conDoc.Id,
                            ShareType = 'C'
                        ));
                    }
                    if(mainFolder.Offer__r.RECO_Project__r.Other_Retail_project_manager__c!=NULL ){
                        newCDLs.add(new ContentDocumentLink(
                            LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.Other_Retail_project_manager__c,
                            ContentDocumentId = conDoc.Id,
                            ShareType = 'C'
                        ));
                    }
            
                    String AdminGroupId=System.Label.System_Admin_Group;
                    if(AdminGroupId!=NULL){
                        newCDLs.add(new ContentDocumentLink(
                            LinkedEntityId = AdminGroupId,
                            ContentDocumentId = conDoc.Id,
                            ShareType = 'C'
                        ));
                    }
                }
                if(!newCDLs.isEmpty()){
                  Database.SaveResult[] lsr = Database.insert(newCDLs,false);
                }
             }
             
          }
        }
    
    }
    /*** Below code shares the existing POs to the updated stakeholders *****/
    public void CreateSharingForLinkedPurchaseOrders(){
        Map<ID, Purchase_Order__c> purchaseOrderMap=new Map<ID, Purchase_Order__c>([Select p.RECO_Project__r.Support_PO_Operator__c
                            , p.RECO_Project__r.Support_National_Boutique_Manager__c
                            , p.RECO_Project__r.Support_International_Retail_Operations__c
                            , p.RECO_Project__r.Other_Retail_Project_Manager__c
                            , p.RECO_Project__r.Other_Manager_Architect__c
                            , p.RECO_Project__r.Other_Design_Architect__c
                            , p.RECO_Project__r.Retail_project_manager__c
                            , p.RECO_Project__r.PO_operator__c
                            , p.RECO_Project__r.National_boutique_manager__c
                            , p.RECO_Project__r.International_Retail_Operator__c
                            , p.RECO_Project__r.Manager_architect__c
                            , p.RECO_Project__r.Design_architect__c
                            , p.RECO_Project__c 
                            , p.Id
                        From Purchase_Order__c p where p.RECO_Project__c = :parent.Id]);
                       // system.debug('---SIZE--'+purchaseOrderMap.size());
        try{
          if(!purchaseOrderMap.isEmpty()){
               List<Purchase_Order__Share> newPurchaseOrderShrs = PurchaseOrderSharingHandler.CreateSharingRecordsNewforPO(purchaseOrderMap);
               Database.SaveResult[] lsr = Database.insert(newPurchaseOrderShrs,false);
            }
        }
        catch(Exception e) {
          system.debug('===EXCEPTION DURING INSERTION OF SHARING RECORDS==='+e);
        }
      
    }
    /*** Below code delete the old sharings of POs *****/
    public void DeleteSharingforDeletedStakeholders()
    {
        Map<ID, Purchase_Order__c> purchaseOrderMap=new Map<ID, Purchase_Order__c>([Select p.RECO_Project__r.Support_PO_Operator__c
                            , p.RECO_Project__r.Support_National_Boutique_Manager__c
                            , p.RECO_Project__r.Support_International_Retail_Operations__c
                            , p.RECO_Project__r.Other_Retail_Project_Manager__c
                            , p.RECO_Project__r.Other_Manager_Architect__c
                            , p.RECO_Project__r.Other_Design_Architect__c
                            , p.RECO_Project__r.Retail_project_manager__c
                            , p.RECO_Project__r.PO_operator__c
                            , p.RECO_Project__r.National_boutique_manager__c
                            , p.RECO_Project__r.International_Retail_Operator__c
                            , p.RECO_Project__r.Manager_architect__c
                            , p.RECO_Project__r.Design_architect__c
                            , p.RECO_Project__c 
                            , p.Id
                        From Purchase_Order__c p where p.RECO_Project__c = :parent.Id]);
        try{
          if(!purchaseOrderMap.isEmpty()){
              PurchaseOrderSharingHandler.DeleteExistingSharing(purchaseOrderMap.keySet());
          }
        }
        catch(Exception e) {
         system.debug('===EXCEPTION DURING DELETION OF SHARING RECORDS==='+e);
        }
                        
    }
    /*** Added below code for req #99 ends here *****/
    public PageReference saveClose(){
        save();
        return close();
    }
    
    public PageReference close(){
        return new PageReference('/' + parent.Id);
    }
    
    public String getParentName() {
        return parent.Name;
    }

    public class StakeholderWrapper{
        public Stakeholder__c stakeholder {get;set;}
        public Boolean isContact {get;set;}
        public StakeholderWrapper(){
            stakeholder = new Stakeholder__c();
        }
    }
    
    //COPIES A LIST OF STAKEHOLDERS TO THE BTQ PROJECT RECORD
    private void copyStakeholderToBTQProject(Id projId){
        RECO_Project__c btqProj = [
            SELECT Retail_project_manager__c, 
                National_boutique_manager__c,
                International_Retail_Operator__c,
                PO_operator__c, 
                Manager_architect__c, 
                Design_architect__c, 
                Other_Retail_Project_Manager__c,
                Support_National_Boutique_Manager__c,
                Support_International_Retail_Operations__c,
                Support_PO_Operator__c,
                Other_Manager_Architect__c,
                Other_Design_Architect__c, 
                Furniture_Supplier_User__c, 
                Furniture_Supplier__c
            FROM RECO_Project__c WHERE Id = :projId
        ];
        
        btqProj.Retail_project_manager__c = null;
        btqProj.National_boutique_manager__c = null; 
        btqProj.International_Retail_Operator__c = null;
        btqProj.PO_operator__c = null;
        btqProj.Manager_architect__c = null; 
        btqProj.Design_architect__c = null; 
        btqProj.Other_Retail_Project_Manager__c = null;
        btqProj.Support_National_Boutique_Manager__c = null;
        btqProj.Support_International_Retail_Operations__c = null;
        btqProj.Support_PO_Operator__c = null;
        btqProj.Other_Manager_Architect__c = null; 
        btqProj.Other_Design_Architect__c = null;
        btqProj.Furniture_Supplier_User__c = null;
        btqProj.Furniture_Supplier__c= null;
        
        for(Stakeholder__c s : [SELECT Function__c, Platform_user__c, Contact__c, Grant_Access__c, Notify__c FROM Stakeholder__c WHERE RECO_Project__c = :projId]){
            if(s.Function__c.equals('Retail Project Manager'))
                btqProj.Retail_project_manager__c = s.Platform_user__c;
            else if(s.Function__c.equals('National Boutique Manager'))
                btqProj.National_boutique_manager__c = s.Platform_user__c;
            else if(s.Function__c.equals('International Retail Operations'))
                btqProj.International_Retail_Operator__c = s.Platform_user__c;
            else if(s.Function__c.equals('PO Operator'))
                btqProj.PO_operator__c = s.Platform_user__c; 
            else if(s.Function__c.equals('Manager Architect') && s.Grant_Access__c)
                btqProj.Manager_architect__c = s.Platform_user__c;
            else if(s.Function__c.equals('Design Architect') && s.Grant_Access__c)
                btqProj.Design_architect__c = s.Platform_user__c;
            else if(s.Function__c.equals('Furniture Supplier (User)') && s.Grant_Access__c)
                btqProj.Furniture_Supplier_User__c = s.Platform_user__c;
            
            else if(s.Function__c.equals('Support Retail Project Manager') && s.Grant_Access__c)
                btqProj.Other_Retail_Project_Manager__c = s.Platform_user__c;
            else if(s.Function__c.equals('Support National Boutique Manager') && s.Grant_Access__c)
                btqProj.Support_National_Boutique_Manager__c = s.Platform_user__c;
            else if(s.Function__c.equals('Support International Retail Operations') && s.Grant_Access__c)
                btqProj.Support_International_Retail_Operations__c = s.Platform_user__c;
            else if(s.Function__c.equals('Support PO Operator') && s.Grant_Access__c)
                btqProj.Support_PO_Operator__c = s.Platform_user__c; 
            else if(s.Function__c.equals('Support Manager Architect') && s.Grant_Access__c)
                btqProj.Other_Manager_Architect__c = s.Platform_user__c;
            else if(s.Function__c.equals('Support Design Architect') && s.Grant_Access__c)
                btqProj.Other_Design_Architect__c = s.Platform_user__c;   
                
            else if(s.Function__c.equals('Furniture Supplier') && s.Notify__c)
                btqProj.Furniture_Supplier__c = s.Contact__c;
        }

        update btqProj;
    }
}
/*****
*   @Class      :   ManageStakeholdersControllerTest.cls
*   @Description:   Test coverage for the ManageStakeholdersController.cls
*   @Author     :   Jacky Uy
*   @Created    :   13 JUN 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   JACKY UY        13 JUN 2014     ManageStakeholdersController.cls Test Coverage at 90%
*   JACKY UY        13 JUN 2014     StakeholderHandler.cls Test Coverage at 96%
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*****/

@isTest
private with sharing class ManageStakeholdersControllerTest{
    static testMethod void unitTest(){
        SYSTEM.runAs(TestHelper.CurrentUser){
            TestHelper.createCurrencies();
           
            Market__c market = TestHelper.createMarket();
            insert market;
            
            Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
            Account acc = TestHelper.createAccount(rTypeId, market.Id);
            insert acc;
            
            rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account acc1 = TestHelper.createAccount(rTypeId, market.Id);
            insert acc1;
        
            Contact contct = TestHelper.createContact(); 
            contct.email='test@gmail.com';
            contct.AccountId=acc1.Id;
            insert contct;
            
            
            rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
            Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
            btq.Name = 'Test BTQ';
            insert btq;
            
            rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
            RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
            insert bProj;
            
            rTypeId = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Draft').getRecordTypeId();
            Offer__c offer = TestHelper.createOffer(rTypeId, bProj.Id);
            offer.Currency__c = 'CHF';
            offer.Supplier_Contact__c = contct.Id;
            offer.RECO_Project__c = bProj.Id;
            offer.Supplier__c = acc1.Id;
            insert offer;
            
           Custom_Folders__c cf = new Custom_Folders__c(Name='Budget & Costs',Parent_Object__c='Offer__c ',   Folder_Number__c ='6.a.' ,   Has_Read_Write_Access__c=' BTQ_RPM; BTQ_NBM; BTQ_PO; BTQ_MA; BTQ_Proc; NCAFE_RPM;NCAFE_NBM; NCAFE_PO; NCAFE_MA; NCAFE_Proc;');
            insert cf;
            /* Stakeholder__c s = new Stakeholder__c (RECO_Project__c=bProj.id,Grant_Access__c = true, Platform_user__c=TestHelper.DesignArchitect.id, Function__c='Design Architect');
            insert s;
            Stakeholder__c s1 = new Stakeholder__c (RECO_Project__c=bProj.id,Grant_Access__c = true, Platform_user__c=TestHelper.ManagerArchitect.id, Function__c='Manager Architect');
            insert s1;
             */
            Attachment_Folder__c a = new Attachment_Folder__c(Name = 'Budget & Costs',Offer__c=offer.Id);
            insert a;
            
            ContentVersion cont = new ContentVersion();
            
            cont.Title = 'Title for this contentVersion';
            cont.PathOnClient = 'file_' + Datetime.now().getTime() + '.txt';
            cont.VersionData = Blob.valueOf('My Content in file_' + Datetime.now().getTime() + '.txt');
            cont.Origin = 'H';
            insert cont;
    
            FeedItem elm = new FeedItem(Body = 'Post with related document body', ParentId = a.id, RelatedRecordId = cont.Id, Type = 'ContentPost');
            insert elm;
            
            
            rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
            List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
             for(Cost_Item__c c : newCostItems){
              c.Currency__c = 'CHF';
              c.Offer__c = offer.Id;
            }

            PageReference pageRef = Page.ManageStakeholders;
            pageRef.getParameters().put('id', bProj.Id);
            Test.setCurrentPage(pageRef);
            
            ManageStakeholdersController con = new ManageStakeholdersController();
            SYSTEM.Assert(con.getParentName().contains('Test BTQ'));
            con.mapStakeholders.get('National Boutique Manager').stakeholder.Platform_user__c = UserInfo.getUserId();
            con.mapStakeholders.get('International Retail Operations').stakeholder.Platform_user__c = UserInfo.getUserId();
            con.mapStakeholders.get('PO Operator').stakeholder.Platform_user__c = UserInfo.getUserId();
            con.mapStakeholders.get('Manager Architect').stakeholder.Platform_user__c = UserInfo.getUserId();
            con.mapStakeholders.get('Support Manager Architect').stakeholder.Platform_user__c = UserInfo.getUserId();
            con.mapStakeholders.get('Design Architect').stakeholder.Platform_user__c = UserInfo.getUserId();
            con.mapStakeholders.get('Support Design Architect').stakeholder.Platform_user__c = UserInfo.getUserId();
            con.mapStakeholders.get('Furniture Supplier').stakeholder.Contact__c= contct.Id;
            con.saveClose();
            
        }
    }
}
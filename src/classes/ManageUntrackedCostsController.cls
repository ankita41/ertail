/*****
*   @Class      :   ManageUntrackedCostsController.cls
*   @Description:   Controller for the ManageUntrackedCosts page.
*   @Author     :   Jacky Uy
*   @Created    :   08 SEP 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public with sharing class ManageUntrackedCostsController {
    private Schema.SObjectType objType {get;set;}
    private Map<String, Schema.SobjectField> fieldsMap {get;set;}
    
    private String orderBy {get;set;}
    private Boolean isAsc {get;set;}
    
    public sObject obj {get;set;}
    public List<SObjectWrapper> objList {get;set;}
    
    public String filterYear {get;set;}
    public List<SelectOption> yearOpts {get;set;}
    public List<SelectOption> marketOpts {get;set;}
    
    private List<sObject> delObjs {get;set;}
    
    public ManageUntrackedCostsController(){
    	objType = Schema.getGlobalDescribe().get('Untracked_Cost__c');
    	fieldsMap = objType.getDescribe().fields.getMap();
    	
        isAsc = true;
        queryYears();
        queryMarkets();
        
        filterYear = String.valueOf(SYSTEM.Now().year());
        queryObjs();
    }
    
    private void queryYears(){
    	Set<String> setYears = new Set<String>();
    	for(Untracked_Cost__c c : [SELECT Year__c FROM Untracked_Cost__c ORDER BY CreatedDate DESC LIMIT 10000]){
    		setYears.add(c.Year__c);
    	}
    	
        yearOpts = new List<SelectOption>();
        for(String y : setYears){
        	yearOpts.add(new SelectOption(y,y));
        }
        yearOpts.sort();
    }
    
    private void queryMarkets(){
        marketOpts = new List<SelectOption>();
        for(Market__c m : Database.query('SELECT Name FROM Market__c ORDER BY Name')){
            marketOpts.add(new SelectOption(m.Id,m.Name));
        }
    }
    
    public void queryObjs(){
        String qryStr = 'SELECT ';
        for(String s : fieldsMap.keySet()) {
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM Untracked_Cost__c WHERE Year__c = :filterYear ';
        
        if(orderBy!=null)    
            qryStr = qryStr + 'ORDER BY ' + orderBy + ' ' + ((isAsc) ? 'ASC' : 'DESC');

        SObjectWrapper sObjWrap;
        objList = new List<SObjectWrapper>();
        
        for(sObject o : Database.query(qryStr)){
            sObjWrap = new SObjectWrapper();
            sObjWrap.objId = o.Id;
            sObjWrap.obj = o;
            objList.add(sObjWrap);
        }
        
        delObjs = new List<sObject>();
    }
    
    public void addRow(){
        SObject newSObj = objType.newSObject();
        newSObj.put('Year__c', filterYear);
        
        SObjectWrapper sObjWrap = new SObjectWrapper();
        sObjWrap.obj = newSObj;
        objList.add(sObjWrap);
    }
    
    public void delRow(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        delRow(index);
    }
    
    private void delRow(Integer index){
        if(objList[index].objId!=null)
            delObjs.add(objList[index].obj);

        objList.remove(index);
    }
    
    //SAVES CHANGES TO RECORDS
    public void save(){
        List<sObject> newObjs = new List<sObject>();
        List<sObject> updObjs = new List<sObject>();
        
        for(SObjectWrapper o : objList){
            if(o.objId==null)  
                newObjs.add(o.obj);
            else
                updObjs.add(o.obj);
        }
        
        try{
            insert newObjs;
            update updObjs;
            delete delObjs;
            queryObjs();
            queryYears();
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
    }

    public PageReference saveClose(){
        save();
        
        if(ApexPages.hasMessages())
            return null;
        else
            return close();
    }
    
    public PageReference close(){
    	Schema.DescribeSObjectResult objDesc = objType.getDescribe();
    	PageReference pgRef = new PageReference('/' + objDesc.getKeyPrefix());
		pgRef.setRedirect(true);
		return pgRef;
    }
    
    //WRAPPER CLASS TO CUSTOMIZE THE SOBJECT
    //USE WHEN NECESSARY LIKE ADDING PROPERTIES
    public class SObjectWrapper{
        public Id objId {get;set;}
        public SObject obj {get;set;}
        public SObjectWrapper(){}
    }
}
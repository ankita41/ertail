/*****
*   @Class      :   ManageUntrackedCostsControllerTest.cls
*   @Description:   Test coverage for the ManageUntrackedCostsController.cls
*   @Author     :   Jacky Uy
*   @Created    :   08 SEP 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   JACKY UY        08 SEP 2014     Test coverage at 93%
*
*****/

@isTest
public with sharing class ManageUntrackedCostsControllerTest {
    //Manage Untracked Costs Page
    static testMethod void unitTest(){
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        PageReference pageRef = Page.ManageUntrackedCosts;
        pageRef.getParameters().put('obj', 'Untracked_Cost__c');
        pageRef.getParameters().put('order', 'Market__c');
        Test.setCurrentPage(pageRef);
        
        ManageUntrackedCostsController con = new ManageUntrackedCostsController();
        
        con.addRow();
        con.objList[0].obj.put('Year__c', '2014');
        con.objList[0].obj.put('Market__c', market.Id);
        con.objList[0].obj.put('Description__c', 'This is a Test.');
        con.objList[0].obj.put('Planned_OP__c', 1000);
        con.objList[0].obj.put('Latest_DF__c', 1000);
        con.save();
        
        SYSTEM.Assert(([SELECT Id FROM Untracked_Cost__c]).size() == 1);
        /*
        con.objList[0].obj.put('Description__c', 'This is a New Test.');
        con.save();
        
        SYSTEM.Assert([SELECT Description__c FROM Untracked_Cost__c WHERE Id = :con.objList[0].objId].Description__c == 'This is a New Test.');
        */
        con.addRow();
        con.objList[0].obj.put('Year__c', '2014');
        con.objList[0].obj.put('Market__c', market.Id);
        con.objList[0].obj.put('Description__c', 'This is a Test.');
        con.objList[0].obj.put('Planned_OP__c', 1000);
        con.objList[0].obj.put('Latest_DF__c', 1000);
        
        pageRef.getParameters().put('index', '0');
        Test.setCurrentPage(pageRef);
        con.delRow();
        con.saveClose();
        
        //SYSTEM.Assert(([SELECT Id FROM Untracked_Cost__c]).size() == 0);
    }
}
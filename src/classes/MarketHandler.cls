/*****

*   @Class      :   MarketHandler.cls
*   @Description:   Static methods related to Market.
*					It is important to keep this class WITHOUT sharing as the sharing will be inherited from the caller (Trigger, controller...)
					NOT DEPLOYED AS MIGHT NOT BE REQUIRED
*   @Author     :   Thibauld
*   @Created    :   24 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
************/
public without sharing class MarketHandler {
    public static String ISARCHITECTPERMISSION = 'Edit';
    public static String ISSUPPLIERPERMISSION = 'Edit';    
    
    public static void ShareWithArchitectAndSupplierAfterInsert(List<Market__c> newList){
        List<Market__Share> shareList = new List<Market__Share>();
        String isArchitectReason = Schema.Market__Share.RowCause.IsArchitect__c;
        String isSupplierReason = Schema.Market__Share.RowCause.IsSupplier__c;
        
        for (Market__c m : newList){
            if (m.Default_Architect__c != null){
                shareList.add(CreateShareRecord(m.Id, m.Default_Architect__c, ISARCHITECTPERMISSION, isArchitectReason));
            }
            if (m.Default_Supplier__c != null){
                shareList.add(CreateShareRecord(m.Id, m.Default_Supplier__c, ISSUPPLIERPERMISSION, isSupplierReason));
            }
        }
        
        if (shareList.size() > 0)
            insert shareList;
    }   

    public static void ShareWithArchitectAndSupplierAfterUpdate(Map<Id, Market__c> oldMap, Map<Id, Market__c> newMap){
        String isArchitectReason = Schema.Market__Share.RowCause.IsArchitect__c;
        String isSupplierReason = Schema.Market__Share.RowCause.IsSupplier__c;
        
        List<Market__Share> sharingToCreate = new List<Market__Share>();
        String deleteSharingStr = '';
        
        for(Id mId : newMap.keySet()){
            if(oldMap.get(mId).Default_Architect__c != newMap.get(mId).Default_Architect__c){
                if (oldMap.get(mId).Default_Architect__c != null)
                    deleteSharingStr += DeleteSharingRecordStr(mId, oldMap.get(mId).Default_Architect__c, isArchitectReason);
                if (newMap.get(mId).Default_Architect__c != null)
                    sharingToCreate.add(CreateShareRecord(mId, newMap.get(mId).Default_Architect__c, ISARCHITECTPERMISSION, isArchitectReason));
            }
            if(oldMap.get(mId).Default_Supplier__c != newMap.get(mId).Default_Supplier__c){
                if (oldMap.get(mId).Default_Supplier__c != null)
                    deleteSharingStr += DeleteSharingRecordStr(mId, oldMap.get(mId).Default_Supplier__c, isSupplierReason);
                if (newMap.get(mId).Default_Supplier__c != null)
                    sharingToCreate.add(CreateShareRecord(mId, newMap.get(mId).Default_Supplier__c, ISSUPPLIERPERMISSION, isSupplierReason));               
            }       
        }
        
        // Delete the sharing reasons that require deletion
        if (!''.equals(deleteSharingStr)){
            deleteSharingStr = deleteSharingStr.removeEnd(' OR ');
            delete Database.query('Select Id from Corner__Share WHERE ' + deleteSharingStr);
        }
        
        // Create the sharing rules that require creation
        if (sharingToCreate.size() > 0)
            insert sharingToCreate;
    }   
    
    
    private static String DeleteSharingRecordStr(Id parentId, Id userId, String reason){
        return '(ParentId = \'' + parentId + '\''
                +' AND UserOrGroupId = \'' + userId + '\''
                +' AND RowCause = \'' + reason + '\') OR ';
    }

    private static Market__Share CreateShareRecord(Id ParentId, Id userId, String access, String reason){
        Market__Share marketShr  = new Market__Share();
        // Set the ID of record being shared.
        marketShr.ParentId = ParentId;
        // Set the ID of user or group being granted access.
        marketShr.UserOrGroupId = userId;
        // Set the access level.
        marketShr.AccessLevel = 'Read';
        marketShr.RowCause = reason;
        return marketShr;
    }
}
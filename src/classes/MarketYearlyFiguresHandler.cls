public with sharing class MarketYearlyFiguresHandler {
    public class CustomException extends Exception{}
    /*
    public static void PreventDuplicates(List<Market_Yearly_Figures__c> oldList, List<Market_Yearly_Figures__c> newList){
        // Request the list of existing Market Yearly Figures
        Map<Id, Map<Double, Id>> marketYearlyFiguresOrderedByMarketThenYear = new Map<Id, Map<Double, Id>>();
        
        for (Market_Yearly_Figures__c myf : [Select Id, Market__c, year__c from Market_Yearly_Figures__c])  
        {
            AddElementToMarketYearlyFiguresOrderedByMarketThenYear(marketYearlyFiguresOrderedByMarketThenYear, myf, true, false);
        }
        
        // Remove the old values from the Map
        if (oldList != null)
            for(Market_Yearly_Figures__c myf : oldList){
                marketYearlyFiguresOrderedByMarketThenYear.get(myf.Market__c).remove(myf.year__c);
            }
            
        // Try to add the new values
        for(Market_Yearly_Figures__c myf : newList){
            AddElementToMarketYearlyFiguresOrderedByMarketThenYear(marketYearlyFiguresOrderedByMarketThenYear, myf, false, true);
        }
    }
    
    // After a new Market Yearly Figure Creation, add the new links to the relevant projects    
    public static void UpdateExistingProjectsAfterMarketYearlyFigureInsert(List<Market_Yearly_Figures__c> newList){
        Map<Id, Map<Double, Id>> newMarketYearlyFiguresOrderedByMarketThenYear = new Map<Id, Map<Double, Id>>();
        Set<Id> marketIdsSet = new Set<Id>();
        
        for(Market_Yearly_Figures__c myf : newList){
            AddElementToMarketYearlyFiguresOrderedByMarketThenYear(newMarketYearlyFiguresOrderedByMarketThenYear, myf, true, false);
            marketIdsSet.add(myf.Market__c);
        }
        
        // request all the projects for the selected Market & year
        List<Project__c> projectsToUpdateList = new List<Project__c>();
        
        for(Project__c proj : [Select Id, Targeted_Installation_Date__c, Corner_Point_of_Sale_r__r.Market__c from Project__c where Corner_Point_of_Sale_r__r.Market__c in:marketIdsSet]){
            if (proj.Targeted_Installation_Date__c != null && proj.Corner_Point_of_Sale_r__r.Market__c  != null){
                Double year = proj.Targeted_Installation_Date__c.year();
                if (newMarketYearlyFiguresOrderedByMarketThenYear.containsKey(proj.Corner_Point_of_Sale_r__r.Market__c) && newMarketYearlyFiguresOrderedByMarketThenYear.get(proj.Corner_Point_of_Sale_r__r.Market__c).containsKey(year)){
                    proj.Market_Yearly_Figures__c = newMarketYearlyFiguresOrderedByMarketThenYear.get(proj.Corner_Point_of_Sale_r__r.Market__c).get(year);
                    projectsToUpdateList.add(proj);
                }
                    
            }
        }   
        
        if (!projectsToUpdateList.isEmpty())
            update (projectsToUpdateList);
    }
    
    private static void AddElementToMarketYearlyFiguresOrderedByMarketThenYear(Map<Id, Map<Double, Id>> marketYearlyFiguresOrderedByMarketThenYear, Market_Yearly_Figures__c myf, Boolean throwException, Boolean addError){
        if(!marketYearlyFiguresOrderedByMarketThenYear.containsKey(myf.Market__c))
            marketYearlyFiguresOrderedByMarketThenYear.put(myf.Market__c, new Map<Double, Id>());
        if(!marketYearlyFiguresOrderedByMarketThenYear.get(myf.Market__c).containsKey(myf.year__c)){
            marketYearlyFiguresOrderedByMarketThenYear.get(myf.Market__c).put(myf.year__c, myf.Id);
        }else{
            if (addError)
                myf.year__c.addError('Error: There is already a Market Yearly Figures for this Market and year.');
            if (throwException)
                throw new CustomException('Error: Multiple Market Yearly Figures for Market:' + myf.Market__c + ' and year:' + myf.year__c + '. Please contact your administrator.');
        }
            
    }
    */
}
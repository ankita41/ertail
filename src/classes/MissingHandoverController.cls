/*****
*   @Class      :   MissingHandoverController.cls
*   @Description:   Controller for MissingHandoverReport.page 
*   @Author     :   Koteswararao
*   @Created    :   20-Nov-2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/
public with sharing class MissingHandoverController {

    public List<ReportWapper> getData() {
            return MissingHandoverController.getProjectData();
     }
    // public static List<ReportWapper> data1{get;set;}
     public static List<ReportWapper> getProjectData() {
            List<ReportWapper> data = new List<ReportWapper>(); 
            //Map<String ,List<ReportWapper>> cornermap= new Map<String, List<ReportWapper>>();
            List<Project__c> projectList=[Select id,p.Supplier__c, p.Status__c,p.Nespresso_Contact__c,p.Nespresso_Contact__r.name, p.Project_Type__c, p.Project_Manager_HQ__c,p.Project_Manager_HQ__r.name, p.Product_Range__c, 
                                p.Name, p.NES_Sales_Promoter__c, p.NES_Business_Development_Manager__c, p.MarketName__c, p.IsMine__c, p.RecordType.name,
                                p.Handover_Submission_Date2__c, p.Handover_Approved_Date2__c, p.Corner__r.Name, p.Corner__c, p.Architect__c, 
                                p.Agreed_Installation_Date__c,p.Approval_Status__c 
                                From Project__c p 
                                where p.status__C!='Closed' and isMassCorner__c=false and p.Agreed_Installation_Date__c!= null  
                                and  p.Agreed_Installation_Date__c <=today
                                and ( p.Handover_Submission_Date2__c = null or Handover_Approved_Date2__c =null or Handover_NES_Dis_Approval_date2__c!= null or Handover_Disapproved_Date2__c != null)
                                and p.Approval_Status__c not IN ( 'closed','Close Project','Cancelled','on hold','draft','Waiting for repair validation','Review Handover Report')  
                                and p.RecordType.name not in ('Maintenance Outside Warranty','Corner Cancelled','Corner Closed') 
                                and p.IsMine__c ='yes'  order by p.Agreed_Installation_Date__c DESC];
                                
             if(projectList != null && projectList.size()>0){
                 for(Project__c obj: projectList ){
                 data.add(new ReportWapper(obj.name,obj.Agreed_Installation_Date__c,obj.Project_Manager_HQ__r.name,obj.status__C,
                                           obj.Approval_Status__c,obj.MarketName__c,obj.id,obj.Nespresso_Contact__r.name,obj.Project_Type__c));
                 }
             }
            
            List<Linked_POS__c> linkedPosList=[Select l.Workflow_Type__c, l.Targeted_Installation_Date__c, l.Status__c, l.Project_Name__c, l.Name, 
                                    l.NES_Sales_Promoter__c, l.NES_Architect__c, l.Corner_Project__r.MarketName__c, l.Corner_Project__r.Approval_Status__c,
                                    l.Corner_Project__r.Handover_Submission_Date2__c, l.Corner_Project__r.Project_Manager_HQ__c, 
                                    l.Corner_Project__r.Supplier__c, l.Corner_Project__r.Status__c, l.Corner_Project__r.Nespresso_Contact__c, 
                                    l.Corner_Project__r.NES_Sales_Promoter__c, l.Corner_Project__r.IsMine__c,  
                                    l.Corner_Project__r.Project_Type__c, l.Corner_Project__r.RecordTypeId, 
                                    l.Corner_Project__r.Name, l.Corner_Project__c, l.Agreed_Installation_Date__c, l.Corner_Project__r.Nespresso_Contact__r.name,
                                    l.Actual_Installation_Date__c From Linked_POS__c l 
                                    where l.Agreed_Installation_Date__c!=null and l.Corner_Project__c != null and l.Corner_Project__r.status__C!='Closed' 
                                    and l.Corner_Project__r.Approval_Status__c in ('Handover Report','Waiting for Installation')  and Corner_Project__r.IsMine__c='yes'
                                    and l.Agreed_Installation_Date__c <= today order by l.Agreed_Installation_Date__c DESC ];
             
             if(linkedPosList != null && linkedPosList.size()>0){
             Set<String> cornerProjectSet= new set<String>();
                 for(Linked_POS__c obj:linkedPosList ){
                     if(cornerProjectSet!= null && !cornerProjectSet.contains(obj.Corner_Project__r.Name)){
                         data.add(new ReportWapper(obj.Corner_Project__r.Name,obj.Agreed_Installation_Date__c,obj.Corner_Project__r.Project_Manager_HQ__c,obj.Corner_Project__r.Status__c,
                                           obj.Corner_Project__r.Approval_Status__c,obj.Corner_Project__r.MarketName__c,obj.Corner_Project__c,obj.Corner_Project__r.Nespresso_Contact__r.name,obj.Corner_Project__r.Project_Type__c));
                        cornerProjectSet.add(obj.Corner_Project__r.Name);                    
                     }
                 }
             
             }
            
            return data ;
     }

    public class ReportWapper{
        public String projectname { get; set; }
        public Date agreedInstallationDate{ get; set; }
        public String projectid{ get; set; }
        public String projectManager{ get; set; }
        public String projectPype{ get; set; }
        public String nespressoContact{ get; set; }
        public String projectStatus{ get; set; }
        public String supplier{get;set;}
        public String architect{ get; set; }
        public String cornerName{get;set;}
        public String approvalStatus{ get; set; }
        public String productRange{get;set;}
        public String marketName{get;set;}
        public ReportWapper(String name, Date installationDate,String projectManager,String projectStatus,
                            String approvalStatus,String marketName,String projectid,String nespressoContact,String projectPype) {
             this.projectname = name;
             this.agreedInstallationDate= installationDate;
             this.projectManager= projectManager;
            this.projectStatus= projectStatus;
            this.approvalStatus=approvalStatus;
            this.marketName=marketName;
            this.projectid=projectid;
            this.nespressoContact=nespressoContact;
            this.projectPype=projectPype;
        }
    }

}
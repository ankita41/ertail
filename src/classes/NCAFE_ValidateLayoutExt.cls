/*****
*   @Class      :   NCAFE_ValidateLayoutExt.cls
*   @Description:   Controller class of the NCAFE_ValidateLayoutExt VF page
*   @Author     :   Priya
*   @Created    :   21 APR 2016
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
                     
*****/

public class NCAFE_ValidateLayoutExt{
    public final List<Attachment_Folder__c> mainFolder {get;set;}
    public Reco_Project__c recoProj{get;set;}
    public String maxFileSize {get;set;}
    private final Integer MAX_FILE_SIZE {get;set;}
    public Id selectedFolder {get;set;}
    public Id recoId {get;set;}
    public FeedItem newFeedContent {get;set;}
    public List<FeedItem> files {get;set;}
    public Map<Id,Boolean> fileCheckBoxMap {get;set;}

public NCAFE_ValidateLayoutExt(ApexPages.standardController con){
    maxFileSize = '10MB';
        MAX_FILE_SIZE = 10485760;
    recoId = ApexPages.currentPage().getParameters().get('Id');
    newFeedContent =new FeedItem();
    recoProj = [SELECT id,Layout_approved__c,Layout_approved_by__c,Layout_approved_date__c, LastModifiedDate,LastModifiedby.Name FROM Reco_Project__c WHERE id=:recoId];

    mainFolder = [SELECT id,Name,RECO_Project__c FROM Attachment_Folder__c WHERE Name='Validated Layout' and RECO_Project__c = :recoId limit 1];
    if(!mainFolder.isEmpty()){
        if(mainFolder[0].Name=='Validated Layout')
        selectedFolder = mainFolder[0].Id;
    }
}
public PageReference addFile(){
    FeedItem newFeedcontentforPrj= new FeedItem();
        if(newFeedContent.ContentFileName!=null && newFeedContent.ContentSize<=MAX_FILE_SIZE){
            newFeedContent.ParentId = selectedFolder;
            insert newFeedContent;
        //recoProj.Layout_approved__c = True;
        //update recoProj;
        queryFiles(selectedFolder);
        }
        else if(newFeedContent.ContentFileName!=null && newFeedContent.ContentSize>MAX_FILE_SIZE){
            newFeedContent.ContentData = null;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Maximum File Size is ' + maxFileSize));
            return null;
        }
    //return null;
    return new PageReference('/apex/NCAFE_ValidateLayout?id=' + recoId);
    }
public List<FeedItem> showFiles{
    get{
    fileCheckBoxMap = new Map<Id,Boolean>();
        files = [SELECT Body,ContentFileName, ContentDescription, ContentSize, RelatedRecordId, CreatedBy.Name, CreatedDate 
                    FROM FeedItem WHERE ParentId = :selectedFolder];
        List<FeedItem> tempFeedItemList=new List<FeedItem>();
        for(FeedItem feed : files)
        {
            if(feed.ContentSize>0){
                tempFeedItemList.add(feed);
            }  
        }
        if(!files.isEmpty()){
            files.clear();
            files.addall(tempFeedItemList);
        }
        
        
        for(FeedItem f : files){
            fileCheckBoxMap.put(f.Id,false);
        }
        
        return tempFeedItemList;
}
set;
}

private void queryFiles(Id folderId){
        newFeedContent = new FeedItem();
        fileCheckBoxMap = new Map<Id,Boolean>();
        files = [
            SELECT Body,ContentFileName, ContentDescription, ContentSize, RelatedRecordId, CreatedBy.Name, CreatedDate 
            FROM FeedItem WHERE ParentId = :folderId ORDER BY CreatedDate DESC
        ]; 
        
        List<FeedItem> tempFeedItemList=new List<FeedItem>();
        for(FeedItem feed : files)
        {
            if(feed.ContentSize>0){
                tempFeedItemList.add(feed);
            }  
        }
        if(!files.isEmpty()){
            files.clear();
            files.addall(tempFeedItemList);
        }
        
        
        for(FeedItem f : files){
            fileCheckBoxMap.put(f.Id,false);
        }
    }
public void delFile(){
        delete [SELECT Id FROM FeedItem WHERE Id = :ApexPages.currentPage().getParameters().get('feedId')];
        delete [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :ApexPages.currentPage().getParameters().get('docId')];
        queryFiles(selectedFolder);
    }
}
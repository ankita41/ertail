/*****
*   @Class      :    NCAFE_ValidateLayoutExtTest.cls
*   @Description:   Test coverage for the  NCAFE_ValidateLayoutExt.cls

*

*****/

@isTest
private with sharing class NCAFE_ValidateLayoutExtTest{
    static testMethod void unitTest(){
       TestHelper.createCurrencies();
            Market__c market = TestHelper.createMarket();
            insert market;
            
            Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
            Account acc = TestHelper.createAccount(rTypeId, market.Id);
            insert acc;
        
            rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('NCafe').getRecordTypeId();
            Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
            insert btq;

            rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
            RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
            bProj.Currency__c = 'CHF';
            bProj.Project_Type__c='Minor refurbishment';
            insert bProj;
           
            
            Attachment_Folder__c mainFolder = new Attachment_Folder__c(name = 'Validated Layout',RECO_Project__c=bProj.id);
            insert mainFolder;

	        FeedItem feeds = new FeedItem (ParentId = mainFolder.id,Body = 'Test Subfolders');
            insert feeds;
            update mainFolder;
            
            
            Test.startTest();
            ApexPages.currentPage().getParameters().put('Id',bProj.Id); 
            NCAFE_ValidateLayoutExt val=new NCAFE_ValidateLayoutExt(new ApexPages.StandardController(mainFolder));
            val.newFeedContent.ContentFileName='Test.txt';
            val.newFeedContent.ContentData=blob.valueof('Test');
            val.newFeedContent.ParentId=mainFolder.Id;
	        val.addFile();
	        insert val.newFeedContent;
     
            Map<Id,Boolean> fileCheckBoxMap = new Map<Id,Boolean>();
            List<FeedItem >files = [SELECT Body,ContentFileName, ContentDescription, ContentSize, RelatedRecordId, CreatedBy.Name, CreatedDate 
            FROM FeedItem WHERE ParentId = :mainFolder.Id ORDER BY CreatedDate DESC];        
            List<FeedItem> tempFeedItemList=new List<FeedItem>();
            for(FeedItem feed : files)
            {
                if(feed.ContentSize>0){
                    tempFeedItemList.add(feed);
                }  
            }
            if(!files.isEmpty()){
                files.clear();
                files.addall(tempFeedItemList);
            }
            
            
            for(FeedItem f : files){
                fileCheckBoxMap.put(f.Id,false);
            }
            
            fileCheckBoxMap.putAll(fileCheckBoxMap);
            
            val.addFile();
            List<FeedItem> showFiles = val.showFiles;
            val.delFile();
            delete feeds;
            delete mainFolder;
            Test.stopTest();
 
    }
}
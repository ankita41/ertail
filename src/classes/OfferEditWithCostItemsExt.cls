/*****
*   @Class      :   OfferEditWithCostItemsExt.cls
*   @Description:   Controller for the EditOfferWithCostItems VF Page
*   @Author     :   Jacky Uy
*   @Created    :   15 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   JACKY UY        10 SEP 2014     Fixed issue on Too many DML rows: 10001
*   Chirag Gulati   22 Apr 2015     Change Record Type of Cost Items to 'Confirmed' when added to approved offer. Line 82. 
*   Koteswararao    2 NOV 2015     Fixed  issue user clicks on freeze cost items , after created offers are bydefault extracost true.  
*   Priya         19 FEB 2016    Code added for REQ # 119 (R2)      
    Himanshu         26 FEB 2016    Code added for REQ # 26 (R2)                                      
*****/

public with sharing class OfferEditWithCostItemsExt{
    public Offer__c varOffer {get;set;}
    public List<Cost_Item__c> costItems {get;set;}
    public Map<Id,Boolean> mapCostItemOfferLink {get;set;}
    public Offer__c recName {get;set;}
    public boolean showError;
    private String qryStr {get;set;}
    
    public Boolean isAdmin{
        get{
            return (UserConstants.CURRENT_USER.Profile.Name == UserConstants.SYS_ADM);
        } set;
    }
    
    public OfferEditWithCostItemsExt(ApexPages.StandardController con) {
        showError=false;
        if(con.getRecord().Id!=null){
            varOffer = (Offer__c)con.getRecord();
            queryOffer();
            queryCostItems();
        }
        else{
            varOffer = (Offer__c)con.getRecord();
            //added follwoing code if the freezecost items are checked in project level ,new offer should be checked by default
            if(varoffer != null && varOffer.RECO_Project__c != null){
             List<RECO_Project__c> objList=[SELECT Id,Freeze_Cost_Items__c  from  RECO_Project__c where id= :varOffer.RECO_Project__c];
             if(objList!= null && objList.size()>0){
                 RECO_Project__c obj=objList[0];
                 if(obj != null)
                     varoffer.Extra_cost__c=obj.Freeze_Cost_Items__c;
             }
            
            }
            //system.debug('varOffer @@'+varOffer.RECO_Project__c+''+varOffer.RECO_Project__r.Freeze_Cost_Items__c );
            
        }
        contactListRerender();
    }
    /**********Req #26 Added by Himanshu*******************************************/
    public list<selectoption> contactSelectOption{get;set;}
    public void contactListRerender()
    {
        contactSelectOption=new List<selectoption>();
        contactSelectOption.add(new SelectOption('','--Choose--'));
        //system.debug('=====test==========='+varOffer.Supplier__c);
        if(String.isNotBlank(varOffer.Supplier__c)){
        
            List<Contact> conList=[Select id,name,Do_Not_Choose__c from contact where accountid=:varOffer.Supplier__c];
            if(!conList.isEmpty()){
                
                for(contact con : conList)
                {
                     if(con.Do_Not_Choose__c!=true && !con.name.contains('DO NOT CHOOSE')){
                         contactSelectOption.add(new SelectOption(con.id,con.name));
                     }
                }
            }
        }
    }
     /**********Req #26************************************************************/ 
    
    private void queryOffer(){
        Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Offer__c').getDescribe().fields.getMap();
        String qryStr = 'SELECT RECO_Project__r.Name, ';
        for(String s : fieldsMap.keySet()) {
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM Offer__c WHERE Id = \'' + varOffer.Id + '\' ';
        varOffer = Database.query(qryStr);
    }
    
    public void queryCostItems(){
        if(varOffer.Id!=null){
            Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Cost_Item__c').getDescribe().fields.getMap();
            qryStr = 'SELECT ';
            for(String s : fieldsMap.keySet()) {
                if(fieldsMap.get(s).getDescribe().isAccessible())
                    qryStr = qryStr + s + ', ';
            }
            qryStr = qryStr.removeEnd(', ') + ' FROM Cost_Item__c WHERE RECO_Project__c = \'' + varOffer.RECO_Project__c + '\' AND Currency__c = \'' + varOffer.Currency__c + '\' ';
            costItems = Database.query(qryStr + 'AND Offer__c = \'' + varOffer.Id + '\' ORDER BY Level_1__c, Level_2__c, Level_3__c');
            
            List<Cost_Item__c> tempList = Database.query(qryStr + 'AND Offer__c = NULL ORDER BY Level_1__c, Level_2__c, Level_3__c');
            costItems.addAll(tempList);
            
            mapCostItemOfferLink = new Map<Id,Boolean>();
            for(Cost_Item__c c : costItems){
                if(c.Offer__c == varOffer.Id)
                    mapCostItemOfferLink.put(c.Id, true);
                else
                    mapCostItemOfferLink.put(c.Id, false);
            }
        }
    }

    public PageReference save(){
    
        if(String.isEmpty(varOffer.Supplier_Contact__c)){
            showError=true;
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please choose any contact'));
           return null;
        }
        Double totalSelectedCost = 0;
        List<Cost_Item__c> updCostItems = new List<Cost_Item__c>();

    /***** PRIYA – 19 Feb 2016 -  Code added for REQ#119 (R2 ) *********/
    
     if(varOffer.Id!=null){
        recName = [SELECT RecordTypeId FROM Offer__c where id = :varOffer.Id];
        if( varOffer.Approval_status__c == 'Approved' && recName.RecordTypeId != varOffer.RecordTypeId )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record type can not be changed for Approved offer'));
            return null;
        }
    }
    if(varOffer.Id!=null){
    
    Offer__c approvedOffer = [SELECT Proforma_No__c,Receive_date__c,Document_date__c,Currency__c,Total_Amount_excl_VAT__c,Supplier__c,Supplier_Contact__c from Offer__c where id = :varOffer.Id];
    if(varOffer.Approval_status__c == 'Approved' && ((approvedOffer.Proforma_No__c != varOffer.Proforma_No__c) 
                        || (approvedOffer.Receive_date__c != varOffer.Receive_date__c) 
                        || (approvedOffer.Document_date__c != varOffer.Document_date__c) 
                        || (approvedOffer.Currency__c != varOffer.Currency__c) 
                        || (approvedOffer.Total_Amount_excl_VAT__c != varOffer.Total_Amount_excl_VAT__c) 
                        || (approvedOffer.Supplier__c != varOffer.Supplier__c) 
                        || (approvedOffer.Supplier_Contact__c != varOffer.Supplier_Contact__c)))
    {
        varOffer.EmailsenttoRPMforapprovedofferchkbox__c = True;
    }
    else{
        varOffer.EmailsenttoRPMforapprovedofferchkbox__c = false;
    }
    }
        /****** Code ends for REQ#119 (R2) *********/
    Id ConfirmedId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId();       
    Id NcafeConfirmedId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId();       
    Id ExpectedId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();       
    Id NcafeExpectedId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Expected').getRecordTypeId(); 
    //Priya: Added below record types for RM0024302819 on 19 Dec'16
    Id PendingApprovalId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Pending approval').getRecordTypeId(); 
    Id NcafePendingApprovalId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Pending approval').getRecordTypeId();     
        if(varOffer.Id!=null){
            
            for(Cost_Item__c c : costItems){
                if(mapCostItemOfferLink.get(c.Id)){
                    c.Offer__c = varOffer.Id;
                    totalSelectedCost = totalSelectedCost + c.Validated_amount__c;
                    if(varOffer.Approval_status__c == 'Approved'){
                        if( c.RecordTypeId == ExpectedId || c.RecordTypeId == PendingApprovalId)
                             c.RecordTypeId = ConfirmedId ; 
                        else if( c.RecordTypeId == NcafeExpectedId || c.RecordTypeId == NcafePendingApprovalId)
                            c.RecordTypeId = NcafeConfirmedId ;
                     }
                     else if(varOffer.Approval_status__c == 'Waiting for Approval'){ 
                         if( c.RecordTypeId == ExpectedId )
                             c.RecordTypeId = PendingApprovalId; 
                        else if( c.RecordTypeId == NcafeExpectedId)
                            c.RecordTypeId = NcafePendingApprovalId;
                     }       
                                   
                }
                else{
                  c.Offer__c = NULL;
                  if( c.RecordTypeId == ConfirmedId )
                        c.RecordTypeId = ExpectedId ; 
                   else if( c.RecordTypeId == NcafeConfirmedId  )
                        c.RecordTypeId = NcafeExpectedId ;
              
              }      
                updCostItems.add(c);
            }
            
            /*for(Cost_Item__c c : [SELECT Offer__c FROM Cost_Item__c WHERE Offer__c = :varOffer.Id AND Currency__c != :varOffer.Currency__c]){
                c.Offer__c = NULL;
                updCostItems.add(c);
            }*/
            //--start--Modifed the exsting code to fix the save issue --            
            showError=false;
            varOffer.Total_Selected_Items__c = totalSelectedCost;
            if( varoffer.Total_Amount_excl_VAT__c == totalSelectedCost){
                 showError = false;
            }else if(costItems != null && costItems .size()>0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, the offer could not be saved because the total amount does not match the total of the selected items.'));
                showError = true;
                return null;
            }
             //--end--Modifed the exsting code to fix the save issue --   
        update updCostItems;
        }

        Contact con;
        if(varOffer.Supplier_Contact__c!=null){
            con = [SELECT AccountId FROM Contact WHERE Id = :varOffer.Supplier_Contact__c];
            varOffer.Supplier__c = con.AccountId;
        }
        
        upsert varOffer;  
        
        ApexPages.currentPage().getParameters().put('Id',varOffer.id);
        String hostVal  = ApexPages.currentPage().getHeaders().get('Host');
        String urlVal = Apexpages.currentPage().getUrl();
        String URLL = 'https://' + hostVal+ urlVal;
        urll=urll.replaceall('AJAXREQUEST=_viewRoot','id='+varOffer.id);
                          
        queryOffer();
        queryCostItems();
        //String url='https://' + ApexPages.currentPage().getHeaders().get('Host') + ApexPages.currentPage().getUrl()+'?id='+varOffer.id;
        
        
        system.debug('====urlll============='+urll);
        
         PageReference redirectPage = new PageReference(urll);
        //Decide if you want to keep the viewState
        redirectPage.setRedirect(true);//true = discard the viewState
        //return the PageReference
        return redirectPage;
        
    }
    
    public PageReference close(){
        if(varOffer.Id!=NULL)
            return new PageReference('/' + varOffer.Id);
        else
            return new PageReference('/' + varOffer.RECO_Project__c);
    }
    
    public PageReference saveClose(){
        save();
        if (!showError){
        return close();
        }else return null;
    }
}
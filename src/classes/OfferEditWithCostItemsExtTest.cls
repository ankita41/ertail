/*****
*   @Class      :   OfferEditWithCostItemsExtTest.cls
*   @Description:   Test coverage for the OfferEditWithCostItemsExt.cls
*   @Author     :   Jacky Uy
*   @Created    :   06 JUN 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        06 JUN 2014     OfferEditWithCostItemsExt.cls Test Coverage at 96%
*   Jacky Uy        21 AUG 2014     Change: BTQ Record Type from 'Boutique - Concept 2002' to 'Boutique'
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*
*****/

@isTest
private with sharing class OfferEditWithCostItemsExtTest{
    static testMethod void unitTest(){
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id);
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        for(Cost_Item__c c : newCostItems){
            c.Currency__c = 'CHF';
        }
        insert newCostItems;
        
        Test.startTest();
        PageReference pageRef = Page.OfferEditWithCostItems;
        Test.setCurrentPage(pageRef);
        
        Offer__c offer = new Offer__c(RECO_Project__c = bProj.Id,Approval_status__c ='Approved');
        insert offer;
        ApexPages.StandardController con = new ApexPages.StandardController(offer);
        OfferEditWithCostItemsExt ext = new OfferEditWithCostItemsExt(con);
        
        ext.varOffer.Currency__c = 'CHF';
        ext.varOffer.Supplier_Contact__c = suppCon.Id; 
        ext.IsAdmin=true;    
        ext.Close(); 
        ext.save();
        
        try
        {
        ext.mapCostItemOfferLink.put(ext.costItems[0].Id, true);
        ext.Close();
        ext.saveClose();
        //SYSTEM.Assert([SELECT Offer__c FROM Cost_Item__c WHERE Id = :ext.costItems[0].Id].Offer__c == offer.Id);
        }
        catch(Exception e){}
        Test.stopTest();
    }
}
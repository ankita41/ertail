/*****
*   @Class      :   OfferHandler.cls
*   @Description:   Handles all Offer__c recurring actions/events.
*   @Author     :   Jacky Uy
*   @Created    :   22 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        23 JUL 2014     Added More Support Stakeholders
        
*****/

public class OfferHandler{
    public static void CopyDefaultValuesFromRECOProjectToOffer(List<Offer__c> newList){
        Set<Id> projIds = new Set<Id>();
        for(Offer__c o : newList){
            projIds.add(o.RECO_Project__c);
        }
        
        Map<Id, RECO_Project__c> projectMap = new Map<Id, RECO_Project__c>([
            SELECT National_boutique_manager__c, Retail_project_manager__c, PO_operator__c,
                 Manager_architect__c, Other_Manager_Architect__c, Design_Architect__c, Other_Design_Architect__c,
                 Other_Retail_Project_Manager__c, Support_National_Boutique_Manager__c, Support_PO_Operator__c
            FROM RECO_Project__c WHERE Id IN :projIds
        ]);
        
        for(Offer__c o : newList){
            if(projectMap.containsKey(o.RECO_Project__c)){
                o.Retail_project_manager__c = projectMap.get(o.RECO_Project__c).Retail_project_manager__c;
                o.Support_Retail_Project_Manager__c = projectMap.get(o.RECO_Project__c).Other_Retail_Project_Manager__c;
                o.National_boutique_manager__c = projectMap.get(o.RECO_Project__c).National_boutique_manager__c;
                o.Support_National_Boutique_Manager__c = projectMap.get(o.RECO_Project__c).Support_National_Boutique_Manager__c;
                o.PO_operator__c = projectMap.get(o.RECO_Project__c).PO_operator__c;
                o.Support_PO_Operator__c = projectMap.get(o.RECO_Project__c).Support_PO_Operator__c;
                o.Manager_architect__c = projectMap.get(o.RECO_Project__c).Manager_architect__c;
                o.Other_Manager_Architect__c = projectMap.get(o.RECO_Project__c).Other_Manager_Architect__c;
                o.Design_Architect__c = projectMap.get(o.RECO_Project__c).Design_Architect__c;
                o.Other_Design_Architect__c = projectMap.get(o.RECO_Project__c).Other_Design_Architect__c;
            }
        }
    }
    
    public static void CopyDefaultValuesFromRECOProjectToInvoice(List<Invoice__c> newList){
        
        Set<Id> offerIds=new Set<Id>();
        Set<Id> projIds = new Set<Id>();
        for(Invoice__c o : newList){
            projIds.add(o.RECO_Project__c);
            if(o.offer__c!=null){
                offerIds.add(o.offer__c);
            }
        }
        if(!offerIds.isEmpty()){
            List<Offer__c> offerList=[Select id,createdbyId,supplier__c from Offer__c where id in:offerIds];
            Map<Id,Id> offerIdCreaIdMap=new Map<Id,Id>();
             Map<Id,Id> offerIdSuppMap=new Map<Id,Id>();
            for(Offer__c off : offerList)
            {
                offerIdCreaIdMap.put(off.id,off.createdbyId);
                offerIdSuppMap.put(off.id,off.supplier__c);
            } 
            for(Invoice__c o : newList){
                if(o.offer__c!=null){
                    o.Offer_Owner__c=offerIdCreaIdMap.get(o.offer__c);
                    o.supplier__c=offerIdSuppMap.get(o.offer__c);
                }
            }
        }
        
        Map<Id, RECO_Project__c> projectMap = new Map<Id, RECO_Project__c>([
            SELECT National_boutique_manager__c,ownerId, Retail_project_manager__c, PO_operator__c,
                 Manager_architect__c, Other_Manager_Architect__c, Design_Architect__c, Other_Design_Architect__c,
                 Other_Retail_Project_Manager__c, Support_National_Boutique_Manager__c, Support_PO_Operator__c
            FROM RECO_Project__c WHERE Id IN :projIds
        ]);
        
        for(Invoice__c o : newList){
            if(projectMap.containsKey(o.RECO_Project__c)){
                o.Retail_project_manager__c = projectMap.get(o.RECO_Project__c).Retail_project_manager__c;
                o.Support_Retail_Project_Manager__c = projectMap.get(o.RECO_Project__c).Other_Retail_Project_Manager__c;
                o.National_boutique_manager__c = projectMap.get(o.RECO_Project__c).National_boutique_manager__c;
                o.Support_National_Boutique_Manager__c = projectMap.get(o.RECO_Project__c).Support_National_Boutique_Manager__c;
                o.PO_operator__c = projectMap.get(o.RECO_Project__c).PO_operator__c;
                o.Support_PO_Operator__c = projectMap.get(o.RECO_Project__c).Support_PO_Operator__c;
                o.Manager_architect__c = projectMap.get(o.RECO_Project__c).Manager_architect__c;
                o.Other_Manager_Architect__c = projectMap.get(o.RECO_Project__c).Other_Manager_Architect__c;
                o.Design_Architect__c = projectMap.get(o.RECO_Project__c).Design_Architect__c;
                o.Other_Design_Architect__c = projectMap.get(o.RECO_Project__c).Other_Design_Architect__c;
                //o.Offer_Owner__c=projectMap.get(o.RECO_Project__c).ownerId;
            }
        }
    }
    
    public static void ConvertOfferTotalAmount(List<Offer__c> newList){
        Set<Id> projIds = new Set<Id>();
        Set<String> createdYears = new Set<String>();
        Set<String> localCurrencies = new Set<String>();
        for(Offer__c obj : newList) {
            if(obj.CreatedDate == null)
                createdYears.add(String.valueOf(SYSTEM.Today().year()));
            else
                createdYears.add(String.valueOf(obj.CreatedDate.year()));
            
            localCurrencies.add(obj.Currency__c);
            projIds.add(obj.RECO_Project__c);
        }
    
        CurrencyConverter.PopulateCurrencyConversion(createdYears, localCurrencies);
        Map<Id,RECO_Project__c> mapProjectsPerId = new Map<Id,RECO_Project__c>([SELECT Id, Currency__c FROM RECO_Project__c WHERE Id IN :projIds]);
        
        for(Offer__c obj : newList) {
            if(obj.Total_Amount_excl_VAT__c!=null){
                obj.Total_Amount_local__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Total_Amount_excl_VAT__c), obj.Currency__c, mapProjectsPerId.get(obj.RECO_Project__c).Currency__c, obj.CreatedDate);
                obj.Total_Amount_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Total_Amount_excl_VAT__c), obj.Currency__c, 'CHF', obj.CreatedDate);
                obj.Total_Amount_EUR__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Total_Amount_excl_VAT__c), obj.Currency__c, 'EUR', obj.CreatedDate);
            }
        }
    }
    
    public static void CopyExtraCostFromOfferToCostItems(Map<Id, Offer__c> newMap){
        List<Cost_Item__c> costItems = [SELECT Id, Offer__c, Extra_cost__c, RecordTypeId FROM Cost_Item__c WHERE Offer__c IN :newMap.keySet()];
        for(Cost_Item__c costItem : costItems) {
            if(newMap.containsKey(costItem.Offer__c))
                costItem.Extra_cost__c = newMap.get(costItem.Offer__c).Extra_cost__c ? 'Yes' : 'No';
        }
        update costItems;
    }
    
    public static void StopInsertOfOffers(List<Offer__c> newList){
        Map<Id, RECO_Project__c> mapProjectsPerId = new map<Id, RECO_Project__c>([SELECT Id, Status__c FROM RECO_Project__c]);
        for (Offer__c o : newList) {
            if(mapProjectsPerId.containsKey(o.RECO_Project__c) && mapProjectsPerId.get(o.RECO_Project__c).Status__c == 'Completed') {
                o.RECO_Project__c.addError('Offers cannot be created for completed projects.');
            }
        }
    }
    
    //Prevent deletion of offers by users other than RPM / Admin, after they have been sent for approval
    public static void StopDeletionOfOffers(List<Offer__c> oldList){
        String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
        String hasDeleteRights = UserConstants.SYS_ADM + ',' + UserConstants.BTQ_RPM+ ',' + UserConstants.NCAFE_RPM;
        
        for (Offer__c o : oldList) {
            if (o.Notify_for_approval__c && !hasDeleteRights.contains(currentUserProfile)) {
              o.Name.addError('You are not authorized to delete offers after they have been sent for validation.');
            }
        }
    }
    
    public static void unlinkCostItemsOnQuotationDeletion(List<Offer__c> oldList){
        String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
        //Id expectedRTId = (currentUserProfile.contains('NCAFE')?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Expected').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId()));
         Id expectedRTId;
        List<Cost_Item__c> costItems = [SELECT Id, Offer__c, RecordTypeId,Reco_Project__R.Recordtype.name FROM Cost_Item__c WHERE Offer__c IN :Trigger.old];
        // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType - Start
        if(!costItems.isEmpty()){
             expectedRTId = (costItems.get(0).Reco_Project__R.Recordtype.name.contains('NCafe')?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Expected').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId()));
        }
        // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType - End
        for (Cost_Item__c costItem : costItems) {
            costItem.Offer__c = null;
            costItem.RecordTypeId = expectedRTId;
        }
        update costItems;
    }
    
    //CREATE ATTACHMENT FOLDERS ON OFFERS
    public static void AutoCreateAttachmentFolders(List<Offer__c> newList){
        List<Attachment_Folder__c> newFolders = new List<Attachment_Folder__c>();
        Map<String, Custom_Folders__c> customFolders = Custom_Folders__c.getAll();
        
        for(Offer__c o : newList) {
            for(String folderName : customFolders.keySet()){
                if(customFolders.get(folderName).Parent_Object__c=='Offer__c'){
                    newFolders.add(new Attachment_Folder__c(
                        Offer__c = o.Id,
                        Name = folderName,
                        Folder_Description_long__c = 'Currently Accessible By: Retail Project Manager, '
                    ));
                }
            }
        }
        insert newFolders;
        AttachmentFolderHandler.AddOfferFolderSharing(newFolders);
    }
    
    //CREATE ATTACHMENT FOLDERS ON Invoices
    public static void AutoCreateAttachmentFoldersInvoices(List<Invoice__c> newList){
        List<Attachment_Folder__c> newFolders = new List<Attachment_Folder__c>();
        Map<String, Custom_Folders__c> customFolders = Custom_Folders__c.getAll();
        
        for(Invoice__c o : newList) {
            for(String folderName : customFolders.keySet()){
                if(customFolders.get(folderName).Parent_Object__c=='Offer__c'){
                    newFolders.add(new Attachment_Folder__c(
                        Invoice__c = o.Id,
                        Name = folderName,
                        Folder_Description_long__c = 'Currently Accessible By: Retail Project Manager, '
                    ));
                }
            }
        }
        insert newFolders;
        //AttachmentFolderHandler.AddOfferFolderSharing(newFolders);
    }
    
    
    
}
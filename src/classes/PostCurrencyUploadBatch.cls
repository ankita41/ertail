/****
Description:This class is for updating the Estimated & Cost item records after new currency rates are uploaded.
Admin would need to run the batch manually to reflect the new values based on new conversion rates.
Created Date: 3/3/15
Created by: Chirag Gulati
Updated by: Priya on 15Dec'16 (As per discussion over RM0023477244 : Approved offers should not be impacted for currency upload task
*****/
    
global class PostCurrencyUploadBatch implements Database.Batchable<sObject>{
String query;
//Priya : Added below record types and removed "Confirmed" as per new process: currency task should not be done on confirmed cost items
String str1 = Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
String str2 = Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Expected').getRecordTypeId();
String str3 = Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('Pending approval').getRecordTypeId();
String str4 = Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Pending approval').getRecordTypeId();
Set<String> recordtypeName = new Set<String>{str1,str2,str3,str4};

global Database.querylocator start(Database.BatchableContext BC){
    query = 'SELECT Id, Cost_estimate_approval_date__c, OpeningFormula__c, (SELECT Id FROM Estimate_Items__r), (SELECT Id, RecordTypeId, Offer__r.Dis_Approval_date__c, Offer__r.Approval_status__c FROM Cost_Items__r) FROM RECO_Project__c WHERE Cost_estimate_approval_date__c = THIS_YEAR OR OpeningFormula__c= THIS_YEAR';
    return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<RECO_Project__c> scope){
    list <Estimate_Item__c> eitems = new list <Estimate_Item__c>();
    list <Cost_Item__c> citems = new list <Cost_Item__c>();
    
    //String str = Schema.Sobjecttype.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId();  
   
    for(RECO_Project__c prj : scope){ 
        eitems.addAll(prj.Estimate_Items__r); // adding Estimated Item Records
        
        for(Cost_Item__c citem : prj.Cost_Items__r){
        //Priya: Updated If condition as per new currency update task i.e. Approved offer should not be impacted
            if(((recordtypeName.contains(citem.RecordTypeId)) && citem.Offer__r != null && citem.Offer__r.Approval_status__c != null && citem.Offer__r.Approval_status__c != 'Approved')|| ((citem.RecordTypeId == str1 || citem.RecordTypeId == str2)&& citem.Offer__r == null)){
                citems.add(citem);
            }else if(prj.OpeningFormula__c != null && prj.OpeningFormula__c.year() == Date.today().year()){
              citems.add(citem);
            }
        }
    }
    if(eitems.size() > 0) update eitems;
    if(citems.size() > 0) update citems;
}
global void finish(Database.BatchableContext BC){


}
}
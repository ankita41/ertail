/***** TO BE DELETED - JUY(12/08/2014) *****/
public with sharing class PurchaseOrderController {
	public Purchase_Order__c po {get; set;}
    public String currentUserProfile;
    
    public Boolean hasAccessToPO {
    	get {
    		return !( (currentUserProfile == 'NES RECO Manager Architect' && UserInfo.getUserId() != po.RECO_Project__r.Manager_architect__c) || 
    				  (currentUserProfile == 'NES RECO Design Architect'  && UserInfo.getUserId() != po.RECO_Project__r.Design_architect__c) ||
    				  (currentUserProfile == 'NES RECO Retail Project Manager'  && UserInfo.getUserId() != po.RECO_Project__r.Retail_project_manager__c) ||
    				  (currentUserProfile == 'NES RECO National Boutique Manager'  && UserInfo.getUserId() != po.RECO_Project__r.National_boutique_manager__c) ||
    				  (currentUserProfile == 'NES RECO PO Operator'  && UserInfo.getUserId() != po.RECO_Project__r.PO_operator__c) ||
    				  (currentUserProfile == 'NES RECO Market Director'  && UserInfo.getUserId() != po.RECO_Project__r.Market_director__c) );
    	} set;
    }
    
    public PurchaseOrderController (ApexPages.StandardController stdController) {
        currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        
        po = (Purchase_Order__c)stdController.getRecord();
        po = [SELECT Id, RECO_Project__r.Retail_project_manager__c, RECO_Project__r.Manager_architect__c, RECO_Project__r.National_boutique_manager__c, 
        			RECO_Project__r.Design_architect__c, RECO_Project__r.PO_operator__c, RECO_Project__r.Market_director__c
                    FROM Purchase_Order__c WHERE Id = :po.Id];
        
    }    
}
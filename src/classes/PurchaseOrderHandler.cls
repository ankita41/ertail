/*****
*   @Class      :   PurchaseOrderHandler.cls
*   @Description:   Handles all Purchase_Order__c recurring actions/events.
*   @Author     :   Jacky Uy
*   @Created    :   08 SEP 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public class PurchaseOrderHandler {
    public static void RecalculateOfferPOListOnPOUpsert(List<Purchase_Order__c> newList, List<Purchase_Order__c> oldList){
        Set<Id> setRelatedOfferIds = new Set<Id>();

        if(newList != null){
            for(Purchase_Order__c po : newList){
                if(po.Offer__c != null)
                    setRelatedOfferIds.add(po.Offer__c);
            }
        } 
        else if(oldList != null){
            for(Purchase_Order__c po : oldList){
                if(po.Offer__c != null)
                    setRelatedOfferIds.add(po.Offer__c);
            }
        }
        
        List<Purchase_Order__c> lstPOs = [SELECT Id, Name, Offer__c FROM Purchase_Order__c WHERE Offer__c IN :setRelatedOfferIds];
        List<Offer__c> lstRelatedOffers = [SELECT Id, PO_List__c,EmailsenttoRPMforapprovedofferchkbox__c FROM Offer__c WHERE Id IN :setRelatedOfferIds];
        
        for(Offer__c offer : lstRelatedOffers){
            String tmpPOList = '';
            for(Purchase_Order__c po : lstPOs){
                if(po.Offer__c == offer.Id)
                    tmpPOList +=(tmpPOList == '' ? '' : ', ') + po.Name;
            }
            offer.PO_List__c = tmpPOList;
            offer.EmailsenttoRPMforapprovedofferchkbox__c = false;
        }
        
        update lstRelatedOffers;
    }
    
    public static void ConvertPOAmount(List<Purchase_Order__c> newList){
        Set<Id> setRelatedProjectIds = new Set<Id>();
        Set<Id> setRelatedOfferIds = new Set<Id>();
        Set<String> createdYears = new Set<String>();
        Set<String> localCurrencies = new Set<String>();
        
        for(Purchase_Order__c obj: newList){
            if(obj.CreatedDate == null)
                createdYears.add(String.valueOf(SYSTEM.Today().year()));
            else
                createdYears.add(String.valueOf(obj.CreatedDate.year()));
            
            localCurrencies.add(obj.Currency__c);
            setRelatedProjectIds.add(obj.RECO_Project__c);
            setRelatedOfferIds.add(obj.Offer__c);
        }
    
        CurrencyConverter cc = new CurrencyConverter(createdYears, localCurrencies);
        Map<Id,RECO_Project__c> mapProjectsPerId = new Map<Id,RECO_Project__c>([SELECT Id, Currency__c FROM RECO_Project__c WHERE Id IN :setRelatedProjectIds]);
        Map<Id,Offer__c> mapOffersPerId = new Map<Id,Offer__c>([SELECT Id, Currency__c FROM Offer__c WHERE Id IN :setRelatedOfferIds]);
        
        for(Purchase_Order__c po : newList){
            po.Amount_local__c = cc.convertValueFromTo(double.valueOf(po.Amount__c), po.Currency__c, mapProjectsPerId.get(po.RECO_Project__c).Currency__c, po.CreatedDate);
            po.Amount_Offer_Currency__c = cc.convertValueFromTo(double.valueOf(po.Amount__c), po.Currency__c, mapOffersPerId.get(po.Offer__c).Currency__c, po.CreatedDate);
            po.Amount_CHF__c = cc.convertValueFromTo(double.valueOf(po.Amount__c), po.Currency__c, 'CHF', po.CreatedDate);
            po.Amount_EUR__c = cc.convertValueFromTo(double.valueOf(po.Amount__c), po.Currency__c, 'EUR', po.CreatedDate);
        }
    }
    
    //ROLLS UP THE TOTAL AMOUNT OF ALL POs ASSOCIATED TO THE OFFER
    public static void RollupPOAmount(List<Purchase_Order__c> newList){
        Set<Id> offerIds = new Set<Id>();
        for(Purchase_Order__c po : newList){
            offerIds.add(po.Offer__c);
        }
        
        Map<Id,Double> mapOfferAmounts = new Map<Id,Double>();
        for(Purchase_Order__c po : [SELECT Amount_Offer_Currency__c, Offer__c FROM Purchase_Order__c WHERE Offer__c IN :offerIds]){
            if(po.Amount_Offer_Currency__c!=NULL){
                if(mapOfferAmounts.containsKey(po.Offer__c))
                    mapOfferAmounts.put(po.Offer__c, mapOfferAmounts.get(po.Offer__c) + po.Amount_Offer_Currency__c);
                else
                    mapOfferAmounts.put(po.Offer__c, po.Amount_Offer_Currency__c);
            }
        }
        
        List<Offer__c> updOffers = new List<Offer__c>();
        for(Offer__c o : [SELECT Total_Amount_of_Related_POs__c FROM Offer__c WHERE Id IN :mapOfferAmounts.keySet()]){
            o.Total_Amount_of_Related_POs__c = mapOfferAmounts.get(o.Id);
            updOffers.add(o);
        }
        update updOffers;
    }
    /*** Below Method added for req. #99 in R2 **/
    public static void ReCalculateSharing(Set<Id> poIds){
        Map<ID, Purchase_Order__c> purchaseOrderMap=new Map<ID, Purchase_Order__c>([Select p.RECO_Project__r.Support_PO_Operator__c
                            , p.RECO_Project__r.Support_National_Boutique_Manager__c
                            , p.RECO_Project__r.Support_International_Retail_Operations__c
                            , p.RECO_Project__r.Other_Retail_Project_Manager__c
                            , p.RECO_Project__r.Other_Manager_Architect__c
                            , p.RECO_Project__r.Other_Design_Architect__c
                            , p.RECO_Project__r.Retail_project_manager__c
                            , p.RECO_Project__r.PO_operator__c
                            , p.RECO_Project__r.National_boutique_manager__c
                            , p.RECO_Project__r.International_Retail_Operator__c
                            , p.RECO_Project__r.Manager_architect__c
                            , p.RECO_Project__r.Design_architect__c
                            , p.RECO_Project__c 
                            , p.Id
                        From Purchase_Order__c p where Id IN :poIds]);
        //system.debug('---SIZE--'+purchaseOrderMap.size());
        try{
            PurchaseOrderSharingHandler.DeleteExistingSharing(purchaseOrderMap.keySet());
            List<Purchase_Order__Share> newPurchaseOrderShrs = PurchaseOrderSharingHandler.CreateSharingRecordsNewforPO(purchaseOrderMap);
            Database.SaveResult[] lsr = Database.insert(newPurchaseOrderShrs,false);
        }
        catch(Exception e) {
            system.debug('===EXCEPTION DURING INSERTION OF SHARING RECORDS==='+e);
        }
    }
}
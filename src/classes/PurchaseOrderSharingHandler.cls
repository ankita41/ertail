/*****
*   @Class      :   PurchaseOrderSharingHandler.cls
*   @Description:   Handles all Purchase_Order__c sharing actions/events.
*                   THIS CLASS MUST STAY WITHOUT SHARING
*   @Author     :   Thibauld
*   @Created    :   29 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
public without sharing class PurchaseOrderSharingHandler {
    
    public static void RecalculateSharingAfterRECOProjectUpdate(Map<Id, RECO_Project__c> oldMap, Map<Id, RECO_Project__c> newMap){
        Set<Id> poIds = new Set<Id>();
        
        for(Id poId : newMap.keySet()){
            if ((oldMap.get(poId).Support_PO_Operator__c != newMap.get(poId).Support_PO_Operator__c)
                || (oldMap.get(poId).Support_National_Boutique_Manager__c != newMap.get(poId).Support_National_Boutique_Manager__c)
                || (oldMap.get(poId).Support_International_Retail_Operations__c != newMap.get(poId).Support_International_Retail_Operations__c)
                || (oldMap.get(poId).Other_Retail_Project_Manager__c != newMap.get(poId).Other_Retail_Project_Manager__c)
                || (oldMap.get(poId).Other_Manager_Architect__c != newMap.get(poId).Other_Manager_Architect__c)
                || (oldMap.get(poId).Other_Design_Architect__c != newMap.get(poId).Other_Design_Architect__c)
                || (oldMap.get(poId).Retail_project_manager__c != newMap.get(poId).Retail_project_manager__c)
                || (oldMap.get(poId).PO_operator__c != newMap.get(poId).PO_operator__c)
                || (oldMap.get(poId).National_boutique_manager__c != newMap.get(poId).National_boutique_manager__c)
                || (oldMap.get(poId).International_Retail_Operator__c != newMap.get(poId).International_Retail_Operator__c)
                || (oldMap.get(poId).Manager_architect__c != newMap.get(poId).Manager_architect__c)
                || (oldMap.get(poId).Design_architect__c != newMap.get(poId).Design_architect__c))
                poIds.add(poId);
        }
        
        if (poIds.size() > 0){
            Map<Id, Purchase_Order__c> posToRecalculate = new Map<Id, Purchase_Order__c>([Select p.RECO_Project__r.Support_PO_Operator__c
                                                                                            , p.RECO_Project__r.Support_National_Boutique_Manager__c
                                                                                            , p.RECO_Project__r.Support_International_Retail_Operations__c
                                                                                            , p.RECO_Project__r.Other_Retail_Project_Manager__c
                                                                                            , p.RECO_Project__r.Other_Manager_Architect__c
                                                                                            , p.RECO_Project__r.Other_Design_Architect__c
                                                                                            , p.RECO_Project__r.Retail_project_manager__c
                                                                                            , p.RECO_Project__r.PO_operator__c
                                                                                            , p.RECO_Project__r.National_boutique_manager__c
                                                                                            , p.RECO_Project__r.International_Retail_Operator__c
                                                                                            , p.RECO_Project__r.Manager_architect__c
                                                                                            , p.RECO_Project__r.Design_architect__c
                                                                                            , p.RECO_Project__c 
                                                                                        From Purchase_Order__c p where p.RECO_Project__c in :poIds]);
            DeleteExistingSharing(posToRecalculate.keySet());
            List<Purchase_Order__Share> poSharesToCreate = CreateSharingRecords(posToRecalculate);                                                              
            if (poSharesToCreate.size() > 0)
                insert poSharesToCreate;
        }
    }
    
    public static void DeleteExistingSharing(Set<Id> poIdSet){
        // Locate all existing sharing records for the project records in the batch.
        // Only records using an Apex sharing reason for this app should be returned. 
        List<Purchase_Order__Share> oldPurchaseOrderShrs = [SELECT Id FROM Purchase_Order__Share WHERE ParentId IN 
             :poIdSet AND 
             (RowCause = :Schema.Purchase_Order__Share.rowCause.IsRetailProjectManager__c 
             OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsNationalBoutiqueManager__c
             OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsInternationalRetailOperations__c
             OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsManagerArchitect__c
             OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsDesignArchitect__c
             OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsPOOperator__c)]; 
        
        // Delete the existing sharing records.
       // This allows new sharing records to be written from scratch.
        Delete oldPurchaseOrderShrs;
    }
    
    public static List<Purchase_Order__Share> CreateSharingRecords(Map<Id, Purchase_Order__c> poMap){
        List<Purchase_Order__Share> newPurchaseOrderShrs = new List<Purchase_Order__Share>();
        // Construct new sharing records for the Retail Project Manager and Supplier 
        // on each project record.
        for(Purchase_Order__c po : poMap.values()){
            if (po.Id != null){
                if (po.RECO_Project__r.Retail_project_manager__c != null){
                    newPurchaseOrderShrs.add(CreateRetailProjectManagerSharing(po.RECO_Project__r.Retail_project_manager__c, po.Id));
                }
                if (po.RECO_Project__r.Other_Retail_Project_Manager__c != null){
                    newPurchaseOrderShrs.add(CreateRetailProjectManagerSharing(po.RECO_Project__r.Other_Retail_Project_Manager__c, po.Id));
                }
                if (po.RECO_Project__r.National_boutique_manager__c != null){
                    newPurchaseOrderShrs.add(CreateNationalBoutiqueManagerSharing(po.RECO_Project__r.National_boutique_manager__c, po.Id));
                }
                if (po.RECO_Project__r.Support_National_Boutique_Manager__c != null){
                    newPurchaseOrderShrs.add(CreateNationalBoutiqueManagerSharing(po.RECO_Project__r.Support_National_Boutique_Manager__c, po.Id));
                }
                if (po.RECO_Project__r.PO_operator__c != null){
                    newPurchaseOrderShrs.add(CreatePOOperatorSharing(po.RECO_Project__r.PO_operator__c, po.Id));
                }
                if (po.RECO_Project__r.Support_PO_Operator__c != null){
                    newPurchaseOrderShrs.add(CreatePOOperatorSharing(po.RECO_Project__r.Support_PO_Operator__c, po.Id));
                }
                if (po.RECO_Project__r.International_Retail_Operator__c != null){
                    newPurchaseOrderShrs.add(CreateInternationalRetailOperationsSharing(po.RECO_Project__r.International_Retail_Operator__c, po.Id));
                }
                if (po.RECO_Project__r.Support_International_Retail_Operations__c != null){
                    newPurchaseOrderShrs.add(CreateInternationalRetailOperationsSharing(po.RECO_Project__r.Support_International_Retail_Operations__c, po.Id));
                }
                if (po.RECO_Project__r.Manager_architect__c != null){
                    newPurchaseOrderShrs.add(CreateManagerArchitectSharing(po.RECO_Project__r.Manager_architect__c, po.Id));
                }
                if (po.RECO_Project__r.Other_Manager_Architect__c != null){
                    newPurchaseOrderShrs.add(CreateManagerArchitectSharing(po.RECO_Project__r.Other_Manager_Architect__c, po.Id));
                }
                if (po.RECO_Project__r.Design_architect__c != null){
                    newPurchaseOrderShrs.add(CreateDesignArchitectSharing(po.RECO_Project__r.Design_architect__c, po.Id));
                }
                if (po.RECO_Project__r.Other_Design_Architect__c != null){
                    newPurchaseOrderShrs.add(CreateDesignArchitectSharing(po.RECO_Project__r.Other_Design_Architect__c, po.Id));
                }
            }
        }
        return newPurchaseOrderShrs;
    }
    
     public static List<Purchase_Order__Share> CreateSharingRecordsNewforPO(Map<Id, Purchase_Order__c> poMap){
        List<Purchase_Order__Share> newPurchaseOrderShrs = new List<Purchase_Order__Share>();
        // Construct new sharing records for the Retail Project Manager and Supplier 
        // on each project record.
        for(Purchase_Order__c po : poMap.values()){
            if (po.Id != null){
                if (po.RECO_Project__r.Retail_project_manager__c != null){
                    newPurchaseOrderShrs.add(CreateRetailProjectManagerSharing(po.RECO_Project__r.Retail_project_manager__c, po.Id));
                }
                if (po.RECO_Project__r.Other_Retail_Project_Manager__c != null){
                    newPurchaseOrderShrs.add(CreateRetailProjectManagerSharing(po.RECO_Project__r.Other_Retail_Project_Manager__c, po.Id));
                }
                if (po.RECO_Project__r.National_boutique_manager__c != null){
                    newPurchaseOrderShrs.add(CreateNationalBoutiqueManagerSharing(po.RECO_Project__r.National_boutique_manager__c, po.Id));
                }
                if (po.RECO_Project__r.Support_National_Boutique_Manager__c != null){
                    newPurchaseOrderShrs.add(CreateNationalBoutiqueManagerSharing(po.RECO_Project__r.Support_National_Boutique_Manager__c, po.Id));
                }
                if (po.RECO_Project__r.PO_operator__c != null){
                    newPurchaseOrderShrs.add(CreatePOOperatorSharing(po.RECO_Project__r.PO_operator__c, po.Id));
                }
                if (po.RECO_Project__r.Support_PO_Operator__c != null){
                    newPurchaseOrderShrs.add(CreatePOOperatorSharing(po.RECO_Project__r.Support_PO_Operator__c, po.Id));
                }
                
            }
        }
        return newPurchaseOrderShrs;
    }
    private static Purchase_Order__Share CreateRetailProjectManagerSharing(Id userId, Id poId){
        return new Purchase_Order__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = poId
                                        , RowCause = Schema.Purchase_Order__Share.RowCause.IsRetailProjectManager__c);
    }

    private static Purchase_Order__Share CreateNationalBoutiqueManagerSharing(Id userId, Id poId){
        return new Purchase_Order__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = poId
                                        , RowCause = Schema.Purchase_Order__Share.RowCause.IsNationalBoutiqueManager__c);
    }
    
    private static Purchase_Order__Share CreateInternationalRetailOperationsSharing(Id userId, Id poId){
        return new Purchase_Order__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = poId
                                        , RowCause = Schema.Purchase_Order__Share.RowCause.IsInternationalRetailOperations__c);
    }

    private static Purchase_Order__Share CreateManagerArchitectSharing(Id userId, Id poId){
        return new Purchase_Order__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = poId
                                        , RowCause = Schema.Purchase_Order__Share.RowCause.IsManagerArchitect__c);
    }
    
    private static Purchase_Order__Share CreateDesignArchitectSharing(Id userId, Id poId){
        return new Purchase_Order__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = poId
                                        , RowCause = Schema.Purchase_Order__Share.RowCause.IsDesignArchitect__c);
    }
    
    private static Purchase_Order__Share CreatePOOperatorSharing(Id userId, Id poId){
        return new Purchase_Order__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = poId
                                        , RowCause = Schema.Purchase_Order__Share.RowCause.IsPOOperator__c);
    }
}
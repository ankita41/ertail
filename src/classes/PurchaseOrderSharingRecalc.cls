/*****
*   @Class      :   PurchaseOrderSharingRecalc.cls
*   @Description:   Support Purchase order batch sharing
*   @Author     :   Thibauld
*   @Created    :   30 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
     
***/
global class PurchaseOrderSharingRecalc implements Database.Batchable<sObject> {
    
	// Get email address logged in user
	static String emailAddress = [Select Email From User where Id = : UserInfo.getUserId() limit 1].Email;
    
    // The start method is called at the beginning of a sharing recalculation.
    // This method returns a SOQL query locator containing the records 
    // to be recalculated. 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([Select p.RECO_Project__r.Support_PO_Operator__c
        										, p.RECO_Project__r.Support_National_Boutique_Manager__c
        										, p.RECO_Project__r.Support_International_Retail_Operations__c
        										, p.RECO_Project__r.Other_Retail_Project_Manager__c
        										, p.RECO_Project__r.Other_Manager_Architect__c
        										, p.RECO_Project__r.Other_Design_Architect__c
        										, p.RECO_Project__r.Retail_project_manager__c
        										, p.RECO_Project__r.PO_operator__c
        										, p.RECO_Project__r.National_boutique_manager__c
        										, p.RECO_Project__r.International_Retail_Operator__c
        										, p.RECO_Project__r.Manager_architect__c
        										, p.RECO_Project__r.Design_architect__c
        										, p.RECO_Project__c 
        										, p.Id
    										From Purchase_Order__c p]);  
    }
        
    // The executeBatch method is called for each chunk of records returned from start.  
    global void execute(Database.BatchableContext BC, List<sObject> scope){
       // Create a map for the chunk of records passed into method.
        Map<ID, Purchase_Order__c> purchaseOrderMap = new Map<ID, Purchase_Order__c>((List<Purchase_Order__c>)scope);  
        
        // Create a list of Purchase_Order__Share objects to be inserted.
        List<Purchase_Order__Share> newPurchaseOrderShrs = PurchaseOrderSharingHandler.CreateSharingRecords(purchaseOrderMap);
        
        try {
           // Delete the existing sharing records.
           // This allows new sharing records to be written from scratch.
           PurchaseOrderSharingHandler.DeleteExistingSharing(purchaseOrderMap.keySet());
            
           // Insert the new sharing records and capture the save result. 
           // The false parameter allows for partial processing if multiple records are 
           // passed into operation. 
           Database.SaveResult[] lsr = Database.insert(newPurchaseOrderShrs,false);
           
           String errorMessage = '';
           // Process the save results for insert.
           for(Database.SaveResult sr : lsr){
           		
               if(!sr.isSuccess()){
                   // Get the first save result error.
                   Database.Error err = sr.getErrors()[0];
                   
                   // Check if the error is related to trivial access level.
                   // Access levels equal or more permissive than the object's default 
                   // access level are not allowed. 
                   // These sharing records are not required and thus an insert exception 
                   // is acceptable. 
                   if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                     &&  err.getMessage().contains('AccessLevel'))){
                                     	
                       // Error is not related to trivial access level.
                       // Send an email to the Apex project's submitter.
                       errorMessage += 'The Purchase Order sharing recalculation threw the following exception: ' + 
                             err.getMessage() + '\n';
                 }
               }
           }
           
           if (!''.equals(errorMessage)){
           		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                 String[] toAddresses = new String[] {emailAddress}; 
                 mail.setToAddresses(toAddresses); 
                 mail.setSubject('Apex Sharing Recalculation Exception');
                 mail.setPlainTextBody(errorMessage);
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
           }
              
        } catch(DmlException e) {
           // Send an email to the Apex project's submitter on failure.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {emailAddress}; 
            mail.setToAddresses(toAddresses); 
            mail.setSubject('Apex Sharing Recalculation Exception');
            mail.setPlainTextBody(
              'The Purchase Order sharing recalculation threw the following exception: ' + 
                        e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    // The finish method is called at the end of a sharing recalculation.
    global void finish(Database.BatchableContext BC){  
        // Send an email to the Apex project's submitter notifying of project completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {emailAddress}; 
        mail.setToAddresses(toAddresses); 
        mail.setSubject('Apex Sharing Recalculation Completed.');
        mail.setPlainTextBody
                      ('The Purchase Order sharing recalculation finished processing');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}
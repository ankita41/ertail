/*****
*   @Class      :   QuotationButtonsController.cls
*   @Description:   Controller for the Quotation buttons.
*   @Author     :   XXXXXXXXX
*   @Created    :   
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        29 JUL 2014     Transformed WF Field Updates to Apex Code
*
***/

public with sharing class QuotationButtonsController {
    public class CustomException extends Exception{}
    
    public Quotation__c quotation {get; set;}
    public String currentUserProfile;
    
    public Boolean IsAgentWF { get{return quotation.Project__r.Workflow_Type__c == 'Agent';}}
    public Boolean IsSalesPromoterWF { get{return quotation.Project__r.Workflow_Type__c == 'Sales Promoter';}}
    public Boolean IsMaintenanceUnderWarrantyWF { get{return quotation.Project__r.Workflow_Type__c == 'Maintenance Under Warranty';}}
    public Boolean IsMaintenanceOutsideWarrantyWF { get{return quotation.Project__r.Workflow_Type__c == 'Maintenance Outside Warranty';}}
    
    public String architectDecisionSuccessMessage {get; set;}
    
    private String ARCHITECTDECISIONSENTSUCCESS_DEFAULT= 'Thank you, your decision will be sent to both the Supplier and the NES Project Manager.';
    private String ARCHITECTDECISIONSENTSUCCESS_AGENT = 'Thank you, your decision will be sent to both the Supplier and the Agent.';
    
    public String sendNESDecisionButton {get; set;}
    public String sendNESDecisionSuccessStr {get; set;}
    
        private  Map<String, RecordType> quotationRecordTypeMapOrderedByDeveloperName{
        get{
            if (quotationRecordTypeMapOrderedByDeveloperName == null){
                quotationRecordTypeMapOrderedByDeveloperName = new Map<String, RecordType>();
                for (RecordType rt : [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType = 'Quotation__c']){
                    quotationRecordTypeMapOrderedByDeveloperName.put(rt.DeveloperName, rt);
                }
            }
            return quotationRecordTypeMapOrderedByDeveloperName;
        }
        set;
    }
    
    private Id getRecordTypeId(String developerName){
        if (quotationRecordTypeMapOrderedByDeveloperName.containsKey(developerName))
            return quotationRecordTypeMapOrderedByDeveloperName.get(developerName).Id;
        return null;
    }
    
    
    private Boolean IsProjectOnHold{
        get{
            if (IsProjectOnHold == null){
                IsProjectOnHold = (quotation.Project__r.Status__c == CornerProjectButtonsController.ONHOLDSTATUS);
            }
            return IsProjectOnHold;
        }
        set;
    }
    
    private void InitMesages(){
        if (IsAgentWF)
            architectDecisionSuccessMessage = ARCHITECTDECISIONSENTSUCCESS_AGENT;
        else{
            architectDecisionSuccessMessage = ARCHITECTDECISIONSENTSUCCESS_DEFAULT;
        }
        
        if (IsMaintenanceOutsideWarrantyWF){
            sendNESDecisionButton = 'Send Decision to Supplier >';
            if (quotation.NES_Decision__c == 'Approved')
                sendNESDecisionSuccessStr = 'Thank you, your decision will be sent to the Supplier.\\nYou are now requested to validate the installation date at the Project level.';
            else
                sendNESDecisionSuccessStr = 'Thank you, your decision will be sent to the Supplier.';
        }else{
            sendNESDecisionButton = 'Send Decision to Architect >';
            sendNESDecisionSuccessStr = 'Thank you, your decision will be sent to the Architect.';
        }
        
    }
    
    public Boolean showSendQuotationForApproval {
        get {
            if (IsProjectOnHold || !(IsAgentWF || IsSalesPromoterWF ))
                return false;
            String hasAccessRights = 'NES Corner Supplier,System Administrator,System Admin';
            return (quotation.RecordTypeId == getRecordTypeId('Supplier_Quotation') && quotation.Quotation_sent_date__c == null && hasAccessRights.contains(currentUserProfile));           
        } set;
    }
    
    public Boolean allowSendQuotationForApproval { 
        get{
            List<Attachment> tmpListAtt = [SELECT Id FROM Attachment WHERE ParentId = :quotation.Id];
            return !(tmpListAtt.isEmpty());
        } set;
    }
    
    public Boolean showSendNESDecision {
        get {
            String hasAccessRights = 'NES Market Local Manager,System Administrator,System Admin';
            if (IsProjectOnHold)
                return false;
            else if (IsAgentWF || IsSalesPromoterWF){
                return (quotation.RecordTypeId == getRecordTypeId('Architect_Quotation') && quotation.Quotation_sent_date__c != null && quotation.NES_Dis_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));         
            }else if(IsMaintenanceOutsideWarrantyWF){
                return (quotation.Quotation_sent_date__c != null && quotation.NES_Dis_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));
            } else
                return false;
        } set;
    }
    
     public Boolean showSendNESDecisionForSiteSurveyQuotation {
        get {
            String hasAccessRights = 'NES Market Local Manager,System Administrator,System Admin';
            if (IsProjectOnHold)
                return false;
            else if (IsAgentWF || IsSalesPromoterWF){
                return ((quotation.RecordTypeId == getRecordTypeId('Site_Survey'))&& quotation.Quotation_sent_date__c != null && quotation.NES_Dis_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));         
            }else
                return false;
        } set;
    }
    
    public Boolean showSendAgentDecisionToArchitect {
        get {
            String hasAccessRights = 'Agent Local Manager,System Administrator,System Admin';
            return (!IsProjectOnHold && quotation.RecordTypeId == getRecordTypeId('Architect_Quotation') && quotation.Quotation_sent_date__c != null && quotation.Agent_Dis_Approval_date__c == null && hasAccessRights.contains(currentUserProfile));           
        } set;
    }
    
    public Boolean allowSendNESDecision { 
        get{
            return (quotation.NES_Decision__c != null && ((quotation.PO_no__c != null && quotation.NES_Decision__c == 'Approved') || (quotation.NES_Decision__c == 'Disapproved' && quotation.NES_Reason__c != null)));
        } set;
    }
    
    public Boolean allowSendNESDecisionForSiteSurveyQuotation { 
        get{
            return (quotation.NES_Decision__c != null 
                    && (
                        (quotation.PO_no__c != null && quotation.NES_Decision__c == 'Approved' && quotation.Project__r.Agreed_Site_Survey_Date__c != null ) 
                        || 
                        (quotation.NES_Decision__c == 'Disapproved' && quotation.NES_Reason__c != null)
                    ));
        } set;
    }
    
    public Boolean allowSendAgentDecisionToArchitect { 
        get{
            return (quotation.Agent_Decision__c != null && ((quotation.PO_no__c != null && quotation.Agent_Decision__c == 'Approved') || (quotation.Agent_Decision__c == 'Disapproved' && quotation.Agent_Reason__c != null)));
        } set;
    }
    
    public Boolean showSendArchitectQuotationForApproval {
        get {
            String hasAccessRights = 'NES Corner Architect,System Administrator,System Admin';
            return (!IsProjectOnHold && quotation.RecordTypeId == getRecordTypeId('Architect_Quotation') && quotation.Quotation_sent_date__c == null && hasAccessRights.contains(currentUserProfile));           
        } set;
    }
    public Boolean allowSendArchitectQuotationForApproval {
        get{
            List<Attachment> tmpListAtt = [SELECT Id FROM Attachment WHERE ParentId = :quotation.Id];
            return !(tmpListAtt.isEmpty());
        } set;
    }
    
    
    public Boolean showSendArchitectDecision {
        get {
            String hasAccessRights = 'NES Corner Architect,System Administrator,System Admin';
            return (!IsProjectOnHold 
                    && quotation.RecordTypeId != getRecordTypeId('Architect_Quotation') 
                    && quotation.RecordTypeId != getRecordTypeId('Site_Survey') 
                    && quotation.Quotation_sent_date__c != null 
                    && quotation.Architect_Decision_Sent_Date__c == null 
                    && hasAccessRights.contains(currentUserProfile));          
        } set;
    }
    public Boolean allowSendArchitectDecision {
        get{
            return (quotation.Architect_Decision__c != null 
                    && (
                        (quotation.Architect_Decision__c == 'Approved') 
                        || 
                        (quotation.Architect_Decision__c == 'Disapproved' && quotation.Architect_Comments__c != null)
                    ));
        } set;
    }
    
    
    public Boolean showApproveSupplierQuotation {
        get {
            String hasAccessRights = 'NES Market Local Manager,System Administrator,System Admin';
            return (!IsProjectOnHold && quotation.RecordTypeId != getRecordTypeId('Architect_Quotation') && quotation.NES_Dis_Approval_date__c == null && quotation.Architect_Decision_Sent_Date__c != null && hasAccessRights.contains(currentUserProfile));            
        } set;
    }
    
    public Boolean allowApproveSupplierQuotation { 
        get{
            List<Attachment> tmpListAtt = [SELECT Id FROM Attachment WHERE ParentId = :quotation.Id];
            return !(quotation.NES_Decision__c == null || (quotation.NES_Decision__c == 'Approved' && (tmpListAtt.isEmpty() || quotation.PO_no__c == null)));
        } set;
    }
    
    public Boolean showApproveSupplierQuotation_Agent {
        get {
            String hasAccessRights = 'Agent Local Manager,System Administrator,System Admin';
            return (!IsProjectOnHold && quotation.RecordTypeId != getRecordTypeId('Architect_Quotation') && quotation.Agent_Dis_Approval_date__c == null && quotation.Architect_Decision_Sent_Date__c != null && hasAccessRights.contains(currentUserProfile));          
        } set;
    }
    
    public Boolean areProjectRequiredFieldsPopulatedBeforeSupplierQuotationApproval{
        get{
            Project__c proj = [Select Id, SIM_Status__c, POS_created_in_backoffice__c,isMassCorner__c from Project__c where Id =: quotation.Project__c];
            if( !proj.isMassCorner__c)
            return proj.SIM_Status__c != null && proj.POS_created_in_backoffice__c != null ;
            else
            return true;
        }
    }
    
    public Boolean allowApproveSupplierQuotation_Agent { 
        get{
            List<Attachment> tmpListAtt = [SELECT Id FROM Attachment WHERE ParentId = :quotation.Id];
            return (quotation.Agent_Decision__c != null 
                && ((quotation.Agent_Decision__c == 'Approved' && (!tmpListAtt.isEmpty()) && quotation.PO_no__c != null)    
                    || (quotation.Agent_Decision__c == 'Disaproved' && quotation.Agent_Reason__c != null)));
        } set;
    }
    
    public QuotationButtonsController(ApexPages.StandardController controller) {
        currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        
        quotation = (Quotation__c)controller.getRecord();
        quotation = [SELECT Id, Send_quotation_for_approval__c, NES_Decision__c, NES_Dis_Approved__c, PO_no__c,  Architect_Decision__c, Architect_Dis_Approval__c, Architect_Comments__c,
                        RecordTypeId, Send_NES_Decision__c, Architect_Decision_Sent_Date__c, NES_Dis_Approval_date__c, Quotation_sent_date__c, Project__c, 
                        Agent_Dis_Approval_date__c, Agent_Decision__c, Agent_Reason__c, Project__r.Workflow_Type__c, Project__r.Supplier_Proposal__c, 
                        Project__r.Proposed_Installation_Date__c, NES_Reason__c, Project__r.Status__c, Project__r.RecordTypeId
                        , Project__r.Proposed_Site_Survey_Date__c
                        , Project__r.Agreed_Site_Survey_Date__c
                        FROM Quotation__c WHERE Id = :quotation.Id];
        InitMesages();
    }
    
    private Id suppQuotationRTypeId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
    private Id suppQuotArchApprovalRTypeId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation for Architect Approval').getRecordTypeId();
    private Id newCornerAgentRTypeId = Schema.sObjectType.Project__c.getRecordTypeInfosByName().get('New Corner (Agent)').getRecordTypeId();
    private Id newCornerRTypeId = Schema.sObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
    
    public PageReference SendQuotationForApproval () {
        if(allowSendQuotationForApproval){
            quotation.Send_quotation_for_approval__c = true;
            update quotation;
        
            WF_SupplierQuotationForApproval();
        }
        return redirectToId(quotation.Id);
    }
    
    public PageReference SendNESDecision() {
        if(allowSendNESDecision){
            quotation.NES_Dis_Approved__c = true;
            
            if(IsMaintenanceOutsideWarrantyWF)
                quotation.Send_NES_Decision__c = true;
            update quotation;
            
            WF_Quotation_NES_Approves();
        }
        
        return redirectToId(quotation.Id);
    }

    public PageReference SendNESDecisionForSiteSurveyQuotation() {
        if (allowSendNESDecisionForSiteSurveyQuotation){
            quotation.NES_Dis_Approved__c = true;
            update quotation;
            
            if (quotation.NES_Decision__c == 'Disapproved'){
                update new Project__c(
                    Id = quotation.Project__c,
                    Approval_Status__c = 'Site Survey Quotation'
                );
            }else if (quotation.NES_Decision__c == 'Approved'){
                update new Project__c(
                    Id = quotation.Project__c,
                    Approval_Status__c = 'Site Survey Completion'
                );
            }
                
        }
        return redirectToId(quotation.Id);
    }
    
    public PageReference SendAgentDecisionToArchitect () {
        quotation.Agent_Dis_Approved__c = true;
        update quotation;
        
        return redirectToId(quotation.Id);
    }
    
    public PageReference SendArchitectQuotationForApproval () {
        quotation.Send_quotation_for_approval__c = true;
        update quotation;
        
        //        
        return redirectToId(quotation.Id);
    } 
    
    public PageReference SendArchitectDecision () {
        quotation.Architect_Dis_Approval__c = true;
        update quotation;
        
        WF_QuotationSupplierApprovedArchitect();
        
        return redirectToId(quotation.Id);
    } 
    
    public PageReference ApproveSupplierQuotation () {
        if(areProjectRequiredFieldsPopulatedBeforeSupplierQuotationApproval && allowApproveSupplierQuotation){
            quotation.Send_NES_Decision__c = true;
            update quotation;
            if (IsMaintenanceOutsideWarrantyWF && quotation.NES_Decision__c == 'Approved')
                return redirectToId(quotation.Project__c);
                
            WF_QuotationSupplierApprovedMarket();
        }
        
        return redirectToId(quotation.Id);
    }
        
    public PageReference ApproveSupplierQuotation_Agent () {
        if(areProjectRequiredFieldsPopulatedBeforeSupplierQuotationApproval && allowApproveSupplierQuotation_Agent){
            quotation.Agent_Dis_Approved__c = true;
            update quotation;
            
            WF_QuotationSupplierApprovedByAgent();
        }
        
        return redirectToId(quotation.Id);
    }
        
    public PageReference BackToProject () {     
        return redirectToId(quotation.Project__c);
    }
    
    private PageReference redirectToId(Id idToRedirectTo){
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('URL','/' + idToRedirectTo);
        curPage.getParameters().put('redir','true');
        curPage.setRedirect(true);
        return curPage;
    }
     
    /*  
    
    THIS IS DONE IN CORNERPROJETBUTTONCONTROLLER
    
    private void WF_SupplierProposeQuotationAndInstallationDateAndPlan(){
        //"WF Maintenance oW: SupplierProposeQuotationAndInstallationDateAndPlan"
        //(Corner Project: Workflow TypeequalsMaintenance outside Warranty) and 
        //(Quotation: Send quotation for approvalequalsTrue) and 
        //(Quotation: Quotation sent dateequalsnull)
        
        if(quotation.Project__r.Workflow_Type__c=='Maintenance outside Warranty' && quotation.Send_quotation_for_approval__c && quotation.Quotation_sent_date__c==null){
            update new Project__c(
                Id = quotation.Project__c,
                Approval_Status__c = 'Supplier Quotation Approval',
                Notify_Proposed_Installation_Date__c = SYSTEM.Now(),
                Notify_Proposed_Installation_Schedule__c = true
            );
        }
    }
    */
    
    private void WF_Quotation_NES_Approves(){
        //"WF Maintenance oW: Quotation NES Approves"
        //(Corner Project: Workflow TypeequalsMaintenance outside Warranty) and 
        //(Quotation: NES (Dis) ApprovedequalsTrue) and 
        //(Quotation: NES (Dis)Approval dateequalsnull) and 
        //(Quotation: NES DecisionequalsApproved) 
        
        if(quotation.Project__r.Workflow_Type__c=='Maintenance outside Warranty' && quotation.NES_Dis_Approved__c && 
            quotation.NES_Dis_Approval_date__c==null && quotation.NES_Decision__c=='Approved'){
            update new Project__c(
                Id = quotation.Project__c,
                Approval_Status__c = 'Supplier Proposal Approval'
            );
        }
    }
    
    private void WF_QuotationSupplierApprovedByAgent(){
        //"WF Agent: QuotationSupplierApprovedByAgent"
        //(Quotation: Agent DecisionequalsApproved) and 
        //(Quotation: Agent (Dis)Approval dateequalsnull) and 
        //(Quotation: Agent (Dis) ApprovedequalsTrue) and
        //(Quotation: Record TypeequalsSupplier Quotation,Supplier Quotation for Architect Approval)
        
        if(quotation.Agent_Decision__c=='Approved' && quotation.Agent_Dis_Approval_date__c==null && quotation.Agent_Dis_Approved__c &&
            (quotation.RecordTypeId==suppQuotationRTypeId || quotation.RecordTypeId==suppQuotArchApprovalRTypeId)){
            update new Project__c(
                Id = quotation.Project__c,
                Approval_Status__c = 'Send Drawings to Architect',
                Notify_Supplier_for_Drawings_Date__c = SYSTEM.Now(),
                Notify_Supplier_for_Drawings__c = true
            );
        }
    }
    
    private void WF_QuotationSupplierApprovedMarket(){
        //"WF Sales Promoter: QuotationSupplierApprovedMarket"
        //(Quotation: NES DecisionequalsApproved) and 
        //(Quotation: NES (Dis)Approval dateequalsnull) and 
        //(Quotation: Record TypeequalsSupplier Quotation,Supplier Quotation for Architect Approval) and 
        //(Quotation: Send NES DecisionequalsTrue) and 
        //(Corner Project: Workflow TypeequalsSales Promoter)
        if(quotation.NES_Decision__c=='Approved' 
            && quotation.NES_Dis_Approval_date__c==null 
            && quotation.Send_NES_Decision__c 
            && quotation.Project__r.Workflow_Type__c=='Sales Promoter' 
            && (quotation.RecordTypeId==suppQuotationRTypeId || quotation.RecordTypeId==suppQuotArchApprovalRTypeId)){
            update new Project__c(
                Id = quotation.Project__c,
                Approval_Status__c = 'Send Drawings to Architect',
                Notify_Supplier_for_Drawings_Date__c = SYSTEM.Now(),
                Notify_Supplier_for_Drawings__c = true
            );
        }
    }
    
    private void WF_QuotationSupplierApprovedArchitect(){
        //"WF Agent: QuotationSupplierApprovedArchitect"
        //(Quotation: Architect DecisionequalsApproved) and 
        //(Quotation: Architect Decision Sent Dateequalsnull) and 
        //(Quotation: Record TypeequalsSupplier Quotation for Architect Approval) and 
        //(Quotation: Architect (Dis)ApprovalequalsTrue) and 
        //(Corner Project: Record TypeequalsNew Corner (Agent))
        
        //"WF Sales Promoter: QuotationSupplierApprovedArchitect"
        //(Quotation: Architect DecisionequalsApproved) and 
        //(Quotation: Architect Decision Sent Dateequalsnull) and 
        //(Quotation: Record TypeequalsSupplier Quotation for Architect Approval) and 
        //(Quotation: Architect (Dis)ApprovalequalsTrue) and 
        //(Corner Project: Record TypeequalsNew Corner) 
        
        if(quotation.Architect_Decision__c=='Approved' && quotation.Architect_Decision_Sent_Date__c==null && quotation.RecordTypeId==suppQuotArchApprovalRTypeId &&
            quotation.Architect_Dis_Approval__c && (quotation.Project__r.RecordTypeId==newCornerAgentRTypeId || quotation.Project__r.RecordTypeId==newCornerRTypeId || quotation.Project__r.RecordTypeId==CornerProjectHelper.newMassCornerRT.Id || quotation.Project__r.RecordTypeId==CornerProjectHelper.newMassCornerAgentRT.Id)){
            update new Project__c(
                Id = quotation.Project__c,
                Approval_Status__c = 'Supplier Quotation Approval'
            );
        }
    }
    
    private void WF_SupplierQuotationForApproval(){
        //"WF Common: SupplierQuotationForApproval"
        //(Quotation: Record TypeequalsSupplier Quotation) and 
        //(Quotation: NES (Dis)Approval dateequalsnull) and 
        //(Quotation: Send quotation for approvalequalsTrue) and 
        //(Corner Project: Workflow TypeequalsSales Promoter,Agent)
        if(quotation.RecordTypeId==suppQuotationRTypeId && quotation.NES_Dis_Approval_date__c==null && quotation.Send_quotation_for_approval__c && 
            (quotation.Project__r.Workflow_Type__c=='Sales Promoter' || quotation.Project__r.Workflow_Type__c=='Agent')){
            update new Project__c(
                Id = quotation.Project__c,
                Approval_Status__c = 'Supplier Quotation Approval (architect)'
            );
        }
    }
}
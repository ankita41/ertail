public with sharing class QuotationEditDispatchController {
	private ApexPages.StandardController controller;
	
	public QuotationEditDispatchController(ApexPages.StandardController controller) {
        this.controller = controller;
    }    
	
	public PageReference getRedir() {
		PageReference newPage = null;
		
		Map<String, String> mapUrlParam = new Map<String, String>();
		mapUrlParam = ApexPages.currentPage().getParameters();
		
		String currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
		
		Quotation__c quotation = (Quotation__c)controller.getRecord();
        quotation = [SELECT Id, Market__c, Architect__c, Nespresso_Contact__c, Supplier__c, RecordTypeId FROM Quotation__c WHERE Id = :quotation.Id];
		
		RecordType archQuotRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Architect_Quotation' AND SobjectType = 'Quotation__c'];
		RecordType suppQuotRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Supplier_Quotation' AND SobjectType = 'Quotation__c'];
		RecordType suppQuotForApprovalRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Supplier_Quotation_for_Architect_Approval' AND SobjectType = 'Quotation__c'];
		RecordType otherQuotRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Other_Quotation' AND SobjectType = 'Quotation__c'];
		
		Boolean hasAccess = true;
		
		if (currentUserProfile == 'NES Corner Supplier' && (UserInfo.getUserId() != quotation.Supplier__c || (quotation.RecordTypeId != suppQuotRT.Id && quotation.RecordTypeId != suppQuotForApprovalRT.Id) ) ) {
			hasAccess = false;
		}
		
		if (currentUserProfile == 'NES Corner Architect' && (UserInfo.getUserId() != quotation.Architect__c || quotation.RecordTypeId == otherQuotRT.Id)) {
			hasAccess = false;
		}
		
		String strUserRole = [SELECT Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name; 
		if (currentUserProfile == 'NES Market Local Manager'  && strUserRole != quotation.Market__c) {
			hasAccess = false;
		}
		
		
		if ( !hasAccess ) {
			
			newPage = new PageReference('/apex/InsufficientPrivileges');
			
		} else {
		    newPage = new PageReference('/' + quotation.Id + '/e');
		    newPage.getParameters().put('nooverride', '1');
		    for (String key : mapUrlParam.keySet()) {
				newPage.getParameters().put(key, mapUrlParam.get(key));
			}
		}
		
		return newPage.setRedirect(true);
	}
}
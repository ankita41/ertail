/*****
*   @Class      :   QuotationHandler.cls
*   @Description:   Support Quotation__c
*					PLEASE LET WITHOUT SHARING
*   @Author     :   Thibauld
*   @Created    :   21 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*	Thibauld		24 JUL 2014		Add Update Market Name   
*	Thibauld		11 AUG 2014		Add Workflow Type    
*	Thibauld		29 SEP 2014		Move ConvertQuotationCurrencyValuesBeforeUpsert in this handler
***/
public without sharing class QuotationHandler{
	public class CustomException extends Exception{}
	
    public static void CopyFromProjectToQuotationBeforeQuotationUpsert(List<Quotation__c> newList){
        //instantiate set to hold unique project record ids
        
        Set<Id> projectIds = new Set<Id>();
        for(Quotation__c q : newList)
        {
            projectIds.add(q.Project__c);
            System.debug(projectIds);
        }
    
        //instantiate map to hold project record ids to their corresponding ownerids
        Map<Id, Project__c> projectMap = new Map<Id, Project__c>([SELECT Id, MarketName__c, Workflow_Type__c, Resp_for_Site_Survey__c, Supplier__c, Architect__c, Project_Manager_HQ__c, NES_Business_Development_Manager__c, Agent_Local_Manager__c, Nespresso_Contact__c, Currency__c FROM Project__c WHERE Id IN: projectIds]);
        
        for (Quotation__c q : newList) 
        {
        	q.MarketName__c = projectMap.get(q.Project__c).MarketName__c;
            if (q.Supplier__c == null) 
            {
                q.Supplier__c = projectMap.get(q.Project__c).Supplier__c;
            }
            if (q.Architect__c == null) 
            {
                q.Architect__c = projectMap.get(q.Project__c).Architect__c;
            }
            if (q.Nespresso_Contact__c == null) 
            {
                q.Nespresso_Contact__c = projectMap.get(q.Project__c).Nespresso_Contact__c;
            }
            if (q.Agent_Local_Manager__c == null) 
            {
                q.Agent_Local_Manager__c = projectMap.get(q.Project__c).Agent_Local_Manager__c;
            }
            if (q.NES_Business_Development_Manager__c == null) 
            {
                q.NES_Business_Development_Manager__c = projectMap.get(q.Project__c).NES_Business_Development_Manager__c;
            }
            if (q.Project_Manager_HQ__c == null) 
            {
                q.Project_Manager_HQ__c = projectMap.get(q.Project__c).Project_Manager_HQ__c;
            }
            if (q.Resp_for_Site_Survey__c == null) 
            {
                q.Resp_for_Site_Survey__c = projectMap.get(q.Project__c).Resp_for_Site_Survey__c;
            }
            if (q.Currency__c == null) 
            {
                q.Currency__c = projectMap.get(q.Project__c).Currency__c;
            }        
            if (q.Workflow_Type__c == null){
            	q.Workflow_Type__c = projectMap.get(q.Project__c).Workflow_Type__c;
            }
        }
    }
    
    //This is called from CornerProjectHandler if the Market has changed
    public static void UpdateMarketName(Map<Id, Project__c> projectsMap){
    	List<Quotation__c> qList = [Select Id, MarketName__c, Project__c from Quotation__c where Project__c in :projectsMap.keySet()];
    	for(Quotation__c q : qList){
    		q.MarketName__c = projectsMap.get(q.Project__c).MarketName__c;
    	}
    	update qList;
    }
	
	public static void RecalculateFormulasOnProjectAfterQuotationACD(List<Quotation__c> quotList){
		Set<Id> projIdSet = new Set<Id>();
		for(Quotation__c quot : quotList){
			projIdSet.add(quot.Project__c);
		}
		CornerProjectHandler.RecalculateQuotationSummaryFields(projIdSet);
	}

	public static void ConvertQuotationCurrencyValuesBeforeUpsert(List<Quotation__c> quotList) {
	    Set<Id> projectIds = new Set<Id>();
	    Set<String> createdYears = new Set<String>();
	    Set<String> localCurrencies = new Set<String>();
	    for(Quotation__c obj: quotList) {
	        if(obj.CreatedDate == null)
	            createdYears.add(String.valueOf(SYSTEM.Today().year()));
	        else
	            createdYears.add(String.valueOf(obj.CreatedDate.year()));
	        
	        localCurrencies.add(obj.Currency__c);
	        projectIds.add(obj.Project__c);
	    }
	
	    CurrencyConverter.PopulateCurrencyConversion(createdYears, localCurrencies);
	    Map<Id, Project__c> mapProjectsPerId = new Map<Id, Project__c>([SELECT Id, Currency__c FROM Project__c WHERE Id IN :projectIds]);
	    String mainCurrency = Currency_Settings__c.getValues('Main Currency').Main_Currency__c;
	    
	    for(Quotation__c q : quotList) {
	        String projCurrency = mapProjectsPerId.get(q.Project__c).Currency__c;
	        
	        q.Design_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Design__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Design_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Design__c), q.Currency__c, projCurrency, q.CreatedDate);
	        
	        q.Executive_Drawing_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Executive_Drawing__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Executive_Drawing_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Executive_Drawing__c), q.Currency__c, projCurrency, q.CreatedDate);
	        
	        q.Follow_up_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Follow_up__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Follow_up_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Follow_up__c), q.Currency__c, projCurrency, q.CreatedDate);
	        
	        q.Furnitures_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Furnitures__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Furnitures_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Furnitures__c), q.Currency__c, projCurrency, q.CreatedDate);
	        
	        q.Installation_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Installation__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Installation_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Installation__c), q.Currency__c, projCurrency, q.CreatedDate);
	        
	        q.Other_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Other__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Other_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Other__c), q.Currency__c, projCurrency, q.CreatedDate);
	        
	        q.Preliminary_Study_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Preliminary_Study__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Preliminary_Study_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Preliminary_Study__c), q.Currency__c, projCurrency, q.CreatedDate);
	        
	        q.Removal_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Removal__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Removal_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Removal__c), q.Currency__c, projCurrency, q.CreatedDate);
	        
	        q.Transportation_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Transportation__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Transportation_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Transportation__c), q.Currency__c, projCurrency, q.CreatedDate);
	        
	        q.Total_in_main_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Total__c), q.Currency__c, mainCurrency, q.CreatedDate);
	        q.Total_in_market_currency__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(q.Total__c), q.Currency__c, projCurrency, q.CreatedDate);
	    }
	}

}
public with sharing class QuotationPageController {
	private String currentUserProfile;
	public Quotation__c quotation {get; set;}
	public String strUserRole {get; set;} 
    
    public Boolean hasAccessToQuotation {
    	get {
    		RecordType archQuotRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Architect_Quotation' AND SobjectType = 'Quotation__c'];
    		RecordType suppQuotRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Supplier_Quotation' AND SobjectType = 'Quotation__c'];
    		RecordType suppQuotForApprovalRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Supplier_Quotation_for_Architect_Approval' AND SobjectType = 'Quotation__c'];
    		RecordType otherQuotRT = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Other_Quotation' AND SobjectType = 'Quotation__c'];
    		
    		Boolean hasAccess = true;
    		
    		if (currentUserProfile == 'NES Corner Supplier' && (UserInfo.getUserId() != quotation.Supplier__c || (quotation.RecordTypeId != suppQuotRT.Id && quotation.RecordTypeId != suppQuotForApprovalRT.Id) ) ) {
    			hasAccess = false;
			}
			
			if (currentUserProfile == 'NES Corner Architect' && (UserInfo.getUserId() != quotation.Architect__c || quotation.RecordTypeId == otherQuotRT.Id)) {
				hasAccess = false;
			}
			
			String strUserRole = [SELECT Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name; 
			if (currentUserProfile == 'NES Market Local Manager'  && strUserRole != quotation.Market__c) {
				hasAccess = false;
			}
    		
    		return hasAccess;
    	} set;
    }
	
	public QuotationPageController(ApexPages.StandardController controller) {
		currentUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
		strUserRole = [SELECT Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name; 
        
        quotation = (Quotation__c)controller.getRecord();
        quotation = [SELECT Id, Market__c, Architect__c, Nespresso_Contact__c, Supplier__c, RecordTypeId FROM Quotation__c WHERE Id = :quotation.Id];
	}
}
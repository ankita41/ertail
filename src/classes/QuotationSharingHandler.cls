/*****
*   @Class      :   QuotationSharingHandler.cls
*   @Description:   Support Quotation__c sharing
*					PLEASE LET WITHOUT SHARING
*   @Author     :   Thibauld
*   @Created    :   30 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
     
***/
public without sharing class QuotationSharingHandler{
	public class CustomException extends Exception{}
	
	public static void ShareQuotations(Map<Id, Quotation__c> newMap){
		List<Quotation__Share> newSharing = CreateSharingRecords(newMap);
		if (newSharing.size() > 0){
			insert newSharing;
		}
	}
	
	public static void RecalculateSharingAfterUpdate(Map<Id, Quotation__c> oldMap, Map<Id, Quotation__c> newMap){
		Map<Id, Quotation__c> cToRecalculate = new Map<Id, Quotation__c>();
		for (Id qId : newMap.keySet()){
			if ((oldMap.get(qId).Architect__c != newMap.get(qId).Architect__c)
				|| (oldMap.get(qId).Supplier__c != newMap.get(qId).Supplier__c)
				|| (oldMap.get(qId).Resp_for_Site_Survey__c != newMap.get(qId).Resp_for_Site_Survey__c)
				|| (oldMap.get(qId).RecordTypeId != newMap.get(qId).RecordTypeId))
				cToRecalculate.put(qId, newMap.get(qId));
		}
		
		if (cToRecalculate.size() > 0) {
			DeleteExistingSharing(cToRecalculate.keySet());
			ShareQuotations(cToRecalculate);
		}
	}
		
	public static void DeleteExistingSharing(Set<Id> qIdSet){
		// Locate all existing sharing records for the project records in the batch.
        // Only records using an Apex sharing reason for this app should be returned. 
        List<Quotation__Share> oldpShrs = [SELECT Id FROM Quotation__Share WHERE ParentId IN 
             :qIdSet AND 
            (RowCause = :Schema.Quotation__Share.rowCause.IsArchitect__c 
            OR RowCause = :Schema.Quotation__Share.rowCause.IsSupplier__c
            OR RowCause = :Schema.Quotation__Share.RowCause.IsRespForSiteSurvey__c)]; 
        
        // Delete the existing sharing records.
       // This allows new sharing records to be written from scratch.
        Delete oldpShrs;
	}
	
	public static List<Quotation__Share> CreateSharingRecords(Map<Id, Quotation__c> cMap){
		Id siteSurveyQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Site Survey').getRecordTypeId();
		Id archQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Architect Quotation').getRecordTypeId();
		Id suppQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
		Id suppForArchQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation for Architect Approval').getRecordTypeId();

		Set<Id> archAuthorizedRTIds = new Set<Id>{archQuotRTId, suppQuotRTId, suppForArchQuotRTId};
		Set<Id> suppAuthorizedRTIds = new Set<Id>{suppQuotRTId, suppForArchQuotRTId};
		
		List<Quotation__Share> newQuotationShrs = new List<Quotation__Share>();
		// Construct new sharing records  
        for(Quotation__c q : cMap.values()){
        	if (q.Architect__c != null && archAuthorizedRTIds.contains(q.RecordTypeId)){
	           newQuotationShrs.add(CreateArchitectSharing(q.Architect__c, q.Id));
        	}
        	if (q.Supplier__c != null && suppAuthorizedRTIds.contains(q.RecordTypeId)){
	           newQuotationShrs.add(CreateSupplierSharing(q.Supplier__c, q.Id));
        	}
        	if(q.Resp_for_Site_Survey__c != null && q.RecordTypeId == siteSurveyQuotRTId){
        		newQuotationShrs.add(CreateRespForSiteSurveySharing(q.Supplier__c, q.Id));
        	}
        }
        return newQuotationShrs;
	}

	private static Quotation__Share CreateArchitectSharing(Id userId, Id pfaId){
		return new Quotation__Share(UserOrGroupId = userId
								, AccessLevel = 'Edit'
								, ParentId = pfaId
								, RowCause = Schema.Quotation__Share.RowCause.IsArchitect__c);
	}
	
	private static Quotation__Share CreateSupplierSharing(Id userId, Id pfaId){
		return new Quotation__Share(UserOrGroupId = userId
								, AccessLevel = 'Edit'
								, ParentId = pfaId
								, RowCause = Schema.Quotation__Share.RowCause.IsSupplier__c);
	}
	
	private static Quotation__Share CreateRespForSiteSurveySharing(Id userId, Id pfaId){
		return new Quotation__Share(UserOrGroupId = userId
								, AccessLevel = 'Edit'
								, ParentId = pfaId
								, RowCause = Schema.Quotation__Share.RowCause.IsRespForSiteSurvey__c);
	}

}
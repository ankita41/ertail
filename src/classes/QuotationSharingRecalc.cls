/*****
*   @Class      :   QuotationSharingRecalc.cls
*   @Description:   Support Quotation batch sharing
*   @Author     :   Thibauld
*   @Created    :   30 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
     
***/
global class QuotationSharingRecalc implements Database.Batchable<sObject> {
    
	// Get email address logged in user
	static String emailAddress = [Select Email From User where Id = : UserInfo.getUserId() limit 1].Email;
    
    // The start method is called at the beginning of a sharing recalculation.
    // This method returns a SOQL query locator containing the records 
    // to be recalculated. 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([Select q.Architect__c
        										, q.Supplier__c
        										, q.Resp_for_Site_Survey__c
        										, q.RecordTypeId
        										, q.Id 
    										From Quotation__c q]);  
    }
        
    // The executeBatch method is called for each chunk of records returned from start.  
    global void execute(Database.BatchableContext BC, List<sObject> scope){
       // Create a map for the chunk of records passed into method.
        Map<ID, Quotation__c> quotationMap = new Map<ID, Quotation__c>((List<Quotation__c>)scope);  
        
        // Create a list of Purchase_Order__Share objects to be inserted.
        List<Quotation__Share> newQuotationShrs = QuotationSharingHandler.CreateSharingRecords(quotationMap);
        
        try {
           // Delete the existing sharing records.
           // This allows new sharing records to be written from scratch.
           QuotationSharingHandler.DeleteExistingSharing(quotationMap.keySet());
            
           // Insert the new sharing records and capture the save result. 
           // The false parameter allows for partial processing if multiple records are 
           // passed into operation. 
           Database.SaveResult[] lsr = Database.insert(newQuotationShrs,false);

           String errorMessage = '';
           // Process the save results for insert.
           for(Database.SaveResult sr : lsr){
               if(!sr.isSuccess()){
                   // Get the first save result error.
                   Database.Error err = sr.getErrors()[0];
                   
                   // Check if the error is related to trivial access level.
                   // Access levels equal or more permissive than the object's default 
                   // access level are not allowed. 
                   // These sharing records are not required and thus an insert exception 
                   // is acceptable. 
                   if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                     &&  err.getMessage().contains('AccessLevel'))){
                       // Error is not related to trivial access level.
                       // Send an email to the Apex project's submitter.
                     errorMessage += 'The CornerProject sharing recalculation threw the following exception: ' + 
                             err.getMessage() + '\n';
                   }
               }
           }
           SendApexSharingRecalculationErrors(errorMessage);
           
        } catch(DmlException e) {
           // Send an email to the Apex project's submitter on failure.
            SendApexSharingRecalculationException(e);
        }
    }
    
    public static void SendApexSharingRecalculationErrors(String errorMessage){
    	if (!''.equals(errorMessage)){
           	 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             String[] toAddresses = new String[] {emailAddress}; 
             mail.setToAddresses(toAddresses); 
             mail.setSubject('Apex Sharing Recalculation Exception');
             mail.setPlainTextBody(errorMessage);
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
           }     
    }
    
    public static void SendApexSharingRecalculationException(Exception e){
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {emailAddress}; 
        mail.setToAddresses(toAddresses); 
        mail.setSubject('Quotation Apex Sharing Recalculation Exception');
        mail.setPlainTextBody(
          'The Apex sharing recalculation threw the following exception: ' + 
                    e.getMessage());
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    // The finish method is called at the end of a sharing recalculation.
    global void finish(Database.BatchableContext BC){  
        // Send an email to the Apex project's submitter notifying of project completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {emailAddress}; 
        mail.setToAddresses(toAddresses); 
        mail.setSubject('Quotation Apex Sharing Recalculation Completed.');
        mail.setPlainTextBody
                      ('The Quotation sharing recalculation finished processing');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}
global without sharing class RECOBatchFileUploadNotificationPerDay implements Database.batchable<RECOFileUploadNotificationHelper>{ 
    
    /*****
*   @Class      :   RECOFileUploadNotificationPerDay.cls
*   @Description:   Sending Notification to feed followers 
*   @Author     :   Promila Boora
*   @Created    :   13 Oct 2015
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
    Promila         03-01-2016      Implements Iterator and used in this batch class,removed old code
*   ----------------------------------------------------------------------------------------------------------------------------
*/
    
  global string query;  
  global List<RECOFileUploadNotificationHelper>allSubsFeeds;
  global String HtmlBody,nCafeHtmlBody;
  global String subject,nCafeSubject;
  
  global RECOBatchFileUploadNotificationPerDay()
  {
       fetchHTMLBodyTemplate();
       fetchHTMLBodyNCafeTemplate();
  }
  
  global Iterable<RECOFileUploadNotificationHelper> start(Database.BatchableContext info) {
       RECOFileUploadNotificationIterator c=new RECOFileUploadNotificationIterator();
       allSubsFeeds=c.allSubsFeeds;
       return allSubsFeeds;
  }     

  
   global void execute(Database.BatchableContext info, List<RECOFileUploadNotificationHelper> scope){
        String insUrl=System.Url.getSalesforceBaseURL().toExternalForm();
        List<Messaging.SingleEmailMessage> email = new List<Messaging.SingleEmailMessage>();
       
        for(RECOFileUploadNotificationHelper recoHelperSubsFeeds:scope)
        {   
               Map<String,List<Attachment_Folder__feed >>recProjFeedsMap=new Map<String,List<Attachment_Folder__feed >>(); // CONTAINS MAP of Project and Respective feeds
               Map<String,List<Attachment_Folder__feed >> folderFeedItemMap=new Map<String,List<Attachment_Folder__feed >>(); // CONTAINS MAP OF FolderId and Respective Feeds
               
               EntitySubscription enSubscriber=[SELECT ParentId,subscriberId, subscriber.name, 
                                                        subscriber.email, subscriber.firstname,subscriber.lastname,Parent.Name FROM 
                                                        EntitySubscription WHERE subscriberId=:recoHelperSubsFeeds.subId limit 1];
               User currentUser=[Select Id,Profile.Name from User where id=:enSubscriber.subscriberId];
               /*******BELOW CODE IS FOR FOLDERS(RETRIEVING FOLDERS,PROJECTS & FEEDS) STARTS HERE******************/ 
                 Map<String,String> recoProjectMap=new Map<String,String>();
                 Map<String,Attachment_Folder__c> recoProjectFoldersMap=new Map<String,Attachment_Folder__c>();
                 for(Attachment_Folder__c attachFolderRecs:[Select id,Name,RECO_Project__c,RECO_Project__r.Name from Attachment_Folder__c where (Name!='Budget & Offer & Cost' and Name!='Budget & Costs') and 
                                                            RECO_Project__c IN :recoHelperSubsFeeds.recProjectFeeds.get(enSubscriber.subscriberId).keySet()]){
                     recoProjectMap.put(attachFolderRecs.RECO_Project__c,attachFolderRecs.RECO_Project__r.Name);
                     recoProjectFoldersMap.put(attachFolderRecs.Id,attachFolderRecs);
                   }
               Map<String,Attachment_Folder__c> recoProjectFoldersMapTemp=new Map<String,Attachment_Folder__c>();
               List<String>recordIds = new List<String>();
               recordIds.addAll(recoProjectFoldersMap.keySet());
               for(UserRecordAccess  uAccess:[SELECT RecordId FROM UserRecordAccess WHERE UserId=:enSubscriber.subscriberId AND HasReadAccess = true AND RecordId IN :recordIds LIMIT 200]){
                   if(recoProjectFoldersMap.containsKey(uAccess.RecordId))
                         recoProjectFoldersMapTemp.put(uAccess.RecordId,recoProjectFoldersMap.get(uAccess.RecordId));
               }
               recordIds.clear();
               if(!recoProjectFoldersMapTemp.isEmpty()){
                   system.debug('==ACCESS=='+recoProjectFoldersMapTemp);
                    recoProjectFoldersMap.clear();
                    recoProjectFoldersMap.putAll(recoProjectFoldersMapTemp);
                    recoProjectFoldersMapTemp.clear();
               }
               for(Attachment_Folder__feed fRec:[Select Id from Attachment_Folder__feed where Type='ContentPost' and createdDate=today and ParentId IN :recoProjectFoldersMap.keySet()]){
                        recordIds.add(fRec.Id); //this list will contains all feed ids
               }
               List<String>recProjFeedIds=new List<String>();
               for(UserRecordAccess  uAccess:[SELECT RecordId FROM UserRecordAccess WHERE UserId=:enSubscriber.subscriberId AND HasReadAccess = true AND RecordId IN :recordIds LIMIT 200]){
                   recProjFeedIds.add(uAccess.RecordId);
               }
               for(Attachment_Folder__feed fRec:[Select Id,ContentFileName,ContentSize,CreatedDate,LinkUrl,ParentId,RelatedRecordId,Title,Type from Attachment_Folder__feed where id IN :recProjFeedIds]){ //Type='ContentPost' and createdDate=today and ParentId IN :recoProjectFoldersMap.keySet()]){
                        List<Attachment_Folder__feed >feeds=new List<Attachment_Folder__feed >();
                        feeds.add(fRec);
                        recordIds.add(fRec.Id); //this list will contains all feed ids
                        if(recProjFeedsMap.containsKey(recoProjectFoldersMap.get(fRec.parentId).RECO_Project__c)){
                              List<Attachment_Folder__feed>allPrevFeeds=recProjFeedsMap.get(recoProjectFoldersMap.get(fRec.parentId).RECO_Project__c);
                              feeds.addAll(allPrevFeeds);
                              recProjFeedsMap.put(recoProjectFoldersMap.get(fRec.parentId).RECO_Project__c,feeds);
                        }
                        else{
                              recProjFeedsMap.put(recoProjectFoldersMap.get(fRec.parentId).RECO_Project__c,feeds); 
                              
                        }
                 }
              recordIds.clear();   
              recProjFeedIds.clear(); 
             /************CODE FOR FOLDERS ENDS HERE******************/ 
                 
             /*******BELOW CODE IS FOR SUBFOLDERS(RETRIEVING SUB-FOLDER,PROJECTS & FEEDS) STARTS HERE******************/ 
                 Map<String,String> recoSubProjectMap=new Map<String,String>();
                 Map<String,Attachment_SubFolder__c> recoProjectSubFoldersMap=new Map<String,Attachment_SubFolder__c>();
                 Map<String,List<Attachment_SubFolder__feed >>recProjSubFeedsMap=new Map<String,List<Attachment_SubFolder__feed >>(); // CONTAINS MAP of Project and Respective feeds
                 for(Attachment_SubFolder__c attachFolderRecs:[Select id,Name,Attachment_Folder__r.Name,Attachment_Folder__r.Id,Attachment_Folder__r.RECO_Project__c,Attachment_Folder__r.RECO_Project__r.Name from Attachment_SubFolder__c where (Attachment_Folder__r.Name!='Budget & Offer & Cost' and Attachment_Folder__r.Name!='Budget & Costs') and 
                                                           Attachment_Folder__r.RECO_Project__c IN :recoHelperSubsFeeds.recProjectFeeds.get(enSubscriber.subscriberId).keySet()]){
                     recoSubProjectMap.put(attachFolderRecs.Attachment_Folder__r.RECO_Project__c,attachFolderRecs.Attachment_Folder__r.RECO_Project__r.Name);
                     recoProjectSubFoldersMap.put(attachFolderRecs.Id,attachFolderRecs);
                 }
               recordIds.addAll(recoProjectSubFoldersMap.keySet());
               Map<String,Attachment_SubFolder__c> recoProjectSubFoldersMapTemp=new Map<String,Attachment_SubFolder__c>();
               for(UserRecordAccess  uAccess:[SELECT RecordId FROM UserRecordAccess WHERE UserId=:enSubscriber.subscriberId AND HasReadAccess = true AND RecordId IN :recordIds LIMIT 200]){
                   if(recoProjectSubFoldersMap.containsKey(uAccess.RecordId))
                         recoProjectSubFoldersMapTemp.put(uAccess.RecordId,recoProjectSubFoldersMap.get(uAccess.RecordId)); // contain map of subfolder records 
               }
               recordIds.clear();
               if(!recoProjectSubFoldersMapTemp.isEmpty()){
                   system.debug('==ACCESS Subfolders=='+recoProjectSubFoldersMapTemp);
                    recoProjectSubFoldersMap.clear();
                    recoProjectSubFoldersMap.putAll(recoProjectSubFoldersMapTemp);
                    recoProjectSubFoldersMapTemp.clear();
               }
               for(Attachment_SubFolder__feed fRec:[Select Id from Attachment_SubFolder__feed where Type='ContentPost' and createdDate=today and ParentId IN :recoProjectSubFoldersMap.keySet()]){
                        recordIds.add(fRec.Id); //this list will contains all feed ids
               }
               recProjFeedIds=new List<String>();
               for(UserRecordAccess  uAccess:[SELECT RecordId FROM UserRecordAccess WHERE UserId=:enSubscriber.subscriberId AND HasReadAccess = true AND RecordId IN :recordIds LIMIT 200]){
                   recProjFeedIds.add(uAccess.RecordId);
               }
               
               
                for(Attachment_SubFolder__feed fRec:[Select Id,ContentFileName,ContentSize,CreatedDate,LinkUrl,ParentId,RelatedRecordId,Title,Type from Attachment_SubFolder__feed where Id IN :recProjFeedIds]){ 
                        List<Attachment_SubFolder__feed >feeds=new List<Attachment_SubFolder__feed >();
                        feeds.add(fRec);
                        if(recProjSubFeedsMap.containsKey(recoProjectSubFoldersMap.get(fRec.parentId).Attachment_Folder__r.RECO_Project__c)){
                              List<Attachment_SubFolder__feed>allPrevFeeds=recProjSubFeedsMap.get(recoProjectSubFoldersMap.get(fRec.parentId).Attachment_Folder__r.RECO_Project__c);
                              feeds.addAll(allPrevFeeds);
                              recProjSubFeedsMap.put(recoProjectSubFoldersMap.get(fRec.parentId).Attachment_Folder__r.RECO_Project__c,feeds);
                        }
                        else{
                              recProjSubFeedsMap.put(recoProjectSubFoldersMap.get(fRec.parentId).Attachment_Folder__r.RECO_Project__c,feeds); 
                        }
                        
                 }
                 
            /*********CODE FOR SUBFOLDERS ENDS HERE******************/    
           /*********CODE FOR SENDING EMAIL STARTS HERE******************/ 
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String body='';
                String subEmail=enSubscriber.subscriber.email;
                String[] toaddress = New String[] {subEmail};
                String contactName=(enSubscriber.subscriber.firstname!=null?enSubscriber.subscriber.firstname:enSubscriber.subscriber.lastname);
                if(currentUser.Profile.Name.containsIgnoreCase('NCAFE')) {
                    nCafeHtmlBody=nCafeHtmlBody.replace('NAME',contactName);
                }
                else{
                    HtmlBody=HtmlBody.replace('NAME',contactName);
                }
                body+='<html><table width="100%">';
                if(!recProjFeedsMap.isEmpty() || !recProjSubFeedsMap.isEmpty()){
                for(String projId : recoHelperSubsFeeds.recProjectFeeds.get(enSubscriber.subscriberId).keySet())
                {
                   integer counter=0;
                   if(recProjFeedsMap.containsKey(projId) || recProjSubFeedsMap.containsKey(projId)){
                       body+='<tr bgcolor="#d9d9d9" width="100%" style="text-align:left;"><b>Project <a href="'+insUrl+'/'+projId+'">'+recoProjectMap.get(projId)+'</a>:</b></a></th></tr>' ;
                   }
                   if(!recProjFeedsMap.isEmpty()){
                     if(recProjFeedsMap.containsKey(projId)){
                       for(Attachment_Folder__feed feedRec: recProjFeedsMap.get(projId)){
                           if(feedRec.ContentFileName!=null){
                           counter++;
                           body+='<tr><td>  '+counter+') <a href="'+insUrl+'/'+feedRec.RelatedRecordId+'">'+feedRec.ContentFileName+'</a>: in the folder <a href="'+insUrl+'/'+feedRec.parentId+'">'+recoProjectFoldersMap.get(feedRec.parentId).Name+'</a></li></td></tr>';
                           }
                       }
                      }
                   }
                   if(!recProjSubFeedsMap.isEmpty()){
                     if(recProjSubFeedsMap.containsKey(projId)){
                      for(Attachment_SubFolder__feed feedRec: recProjSubFeedsMap.get(projId)){
                           if(feedRec.ContentFileName!=null){
                           counter++;
                           body+='<tr><td>  '+counter+') <a href="'+insUrl+'/'+feedRec.RelatedRecordId+'">'+feedRec.ContentFileName+'</a>: in the sub folder <a href="'+insUrl+'/'+feedRec.parentId+'">'+recoProjectSubFoldersMap.get(feedRec.parentId).Name+'</a> of folder <a href="'+insUrl+'/'+recoProjectSubFoldersMap.get(feedRec.parentId).Attachment_Folder__r.Id+'">'+recoProjectSubFoldersMap.get(feedRec.parentId).Attachment_Folder__r.Name+'</a></li></td></tr>';
                           }
                       }
                     }
                   }
                   body+='<tr></tr>';
                }
                }
                body+='</table><br/><br/></html>';
                if(!currentUser.Profile.Name.containsIgnoreCase('NCAFE')){
                    body= HtmlBody.replace('Message',body);
                    mail.setsubject(subject);
                }
                else {
                    body= nCafeHtmlBody.replace('Message',body);
                    mail.setsubject(nCafeSubject);
                }
                mail.setHtmlBody(body);
                mail.setToAddresses(toaddress);
                if(!recProjFeedsMap.isEmpty() || !recProjSubFeedsMap.isEmpty()){
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
               /* if(String.valueof(enSubscriber.subscriberId).contains('005b0000002WUBg') || String.valueof(enSubscriber.subscriberId).contains('0058E000000FNAZ') || String.valueof(enSubscriber.subscriberId).contains('0058E000000EABD') || String.valueof(enSubscriber.subscriberId).contains('0058E000000Den3') || String.valueof(enSubscriber.subscriberId).contains('0058E000000E2zU') || String.valueof(enSubscriber.subscriberId).contains('0058E000000FLwv'))
                {
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }*/
             /*********CODE FOR SENDING EMAIL ENDS HERE******************/ 
        }   
       
   }
   
   
        
   global void finish(Database.BatchableContext info){  
       
   } 
   
   public void fetchHTMLBodyTemplate(){
  
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{'invalid@emailaddr.es'};
        mail.setToAddresses(toAddresses);
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
        mail.setSenderDisplayName('MMPT');
        mail.setTargetObjectId(userinfo.getuserid());
        EMailTemplate em = [SELECT id, FolderId, ApiVersion,  BrandTemplateId, Description, DeveloperName, Encoding, TemplateStyle, TemplateType, HTMLValue, Name, Body, Markup, Subject 
                    FROM EMailTemplate WHERE DeveloperName = 'NotifiyProjectFollowers'
                ];
        EMailTemplate em2 = em.clone(true,false);
        em2.id=null;
        em2.developerName = 'Test' +user.id;
        em2.isactive=true;
        database.insert(em2);
        mail.setTemplateId(em2.Id);
              
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        Database.rollback(sp);
        database.delete(em2);
        
        em=null;
        em2=null;
        subject=mail.getSubject();
        HtmlBody = mail.getHTMLBody();
        HtmlBody = HtmlBody.remove('<td  style=" height:1; background-color:#330000; bLabel:accent3; bEditID:r6st1;"></td>');
        HtmlBody = HtmlBody.replace('<td  style=" height:1; background-color:#330000; bLabel:accent1; bEditID:r2st1;"></td>', '<td  style=" font-size:1%; height:1; background-color:#330000; bLabel:accent1; bEditID:r2st1;"></td>');
        HtmlBody = HtmlBody.replace('<td  style=" height:1; background-color:#330000; bLabel:accent2; bEditID:r4st1;"></td>', '<td  style=" font-size:1%; height:1; background-color:#330000; bLabel:accent2; bEditID:r4st1;"></td>');
       
  }
  public void fetchHTMLBodyNCafeTemplate(){
  
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{'invalid@emailaddr.es'};
        mail.setToAddresses(toAddresses);
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
        mail.setSenderDisplayName('MMPT');
        mail.setTargetObjectId(userinfo.getuserid());
        EMailTemplate em = [SELECT id, FolderId, ApiVersion,  BrandTemplateId, Description, DeveloperName, Encoding, TemplateStyle, TemplateType, HTMLValue, Name, Body, Markup, Subject 
                    FROM EMailTemplate WHERE DeveloperName = 'NCAFE_NotifiyProjectFollowers'
                ];
        EMailTemplate em2 = em.clone(true,false);
        em2.id=null;
        em2.developerName = 'Test' +user.id;
        em2.isactive=true;
        database.insert(em2);
        mail.setTemplateId(em2.Id);
              
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        Database.rollback(sp);
        database.delete(em2);
        
        em=null;
        em2=null;
        nCafeSubject=mail.getSubject();
        nCafeHtmlBody = mail.getHTMLBody();
        nCafeHtmlBody = nCafeHtmlBody.remove('<td  style=" height:1; background-color:#330000; bLabel:accent3; bEditID:r6st1;"></td>');
        nCafeHtmlBody = nCafeHtmlBody.replace('<td  style=" height:1; background-color:#330000; bLabel:accent1; bEditID:r2st1;"></td>', '<td  style=" font-size:1%; height:1; background-color:#330000; bLabel:accent1; bEditID:r2st1;"></td>');
        nCafeHtmlBody = nCafeHtmlBody.replace('<td  style=" height:1; background-color:#330000; bLabel:accent2; bEditID:r4st1;"></td>', '<td  style=" font-size:1%; height:1; background-color:#330000; bLabel:accent2; bEditID:r4st1;"></td>');
       
  }
}
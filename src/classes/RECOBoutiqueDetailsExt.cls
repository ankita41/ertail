/*****
*   @Class      :   RECOBoutiqueDetailsExt.cls
*   @Description:   Extension developed for RECOBoutiqueDetails.page
*   @Author     :   PRIYA
*   @Created    :   18 MAR 2016
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                         
*****/

public virtual class RECOBoutiqueDetailsExt {
    public class CustomException extends Exception {}
    
    //private Id RecordTypeId_Warehouse = Schema.SObjectType.RTCOMBoutique__c.getRecordTypeInfosByName().get('Warehouse').getRecordTypeId();
    
    //private static String FOLDER_COMPLETEDESIGN = 'Complete Design';
    //private static String FOLDER_PRELIMINARYDESIGN = 'Preliminary Design';
    public Id RECOProjectId {get; private set;}
    //private static List<String> orderedFoldersToConsider = new List<String>{FOLDER_COMPLETEDESIGN, FOLDER_PRELIMINARYDESIGN};
    
    public Boutique__c Boutique { get; private set;}
    //public List<RECO_Project__c> BoutiqueProj {get; private set;}
    public Id imageId { get; set;}
    private List<Id> orderedImages = new List<Id>();

    public RECOBoutiqueDetailsExt(ApexPages.StandardController stdController) {
        this.Boutique = (Boutique__c)stdController.getRecord();
        imageId = Boutique.Default_Image__c;
        initOrderedImages();
    }
    
    private void initOrderedImages(){
        if (this.Boutique.Default_Image__c != null)
            orderedImages.add(this.Boutique.Default_Image__c);
        if (this.Boutique.Default_Image_2__c != null)
            orderedImages.add(this.Boutique.Default_Image_2__c);
        if (this.Boutique.Default_Image_3__c != null)
            orderedImages.add(this.Boutique.Default_Image_3__c);
            
        // Request RECO default images
        /*if (Boutique.RECOBoutique__c != null){
            Boutique__c recoBtq = [Select Id, Default_Image__c, Default_Image_2__c, Default_Image_3__c from Boutique__c where Id =: Boutique.RECOBoutique__c];
            if (recoBtq.Default_Image__c != null)
                orderedImages.add(recoBtq.Default_Image__c);
            if (recoBtq.Default_Image_2__c != null)
                orderedImages.add(recoBtq.Default_Image_2__c);
            if (recoBtq.Default_Image_3__c != null)
                orderedImages.add(recoBtq.Default_Image_3__c);
        }*/
    }
    
   /* public Boolean IsWarehouse{
        get{
            return RecordTypeId_Warehouse.equals(Boutique.RecordTypeId);
        }
    }
    
    public Boolean isHQPMorAdmin{
        get{
            return ERTAILMasterHelper.isHQPMorAdmin;
        }
    } 
    
    public Boolean CanEditBoutique{
        get{
            if (CanEditBoutique == null)
                CanEditBoutique = (ERTAILMasterHelper.isHQPMorAdmin || ERTAILMasterHelper.isMPM); 
            return CanEditBoutique;
        }
        private set;
    } */
    
    public List<Boutique__c> BoutiquesInSameMarket{
        get{
            if (BoutiquesInSameMarket == null){
                BoutiquesInSameMarket = [Select Id, Name from Boutique__c where Market__c = :Boutique.Market__c order by Name];
            }
            return BoutiquesInSameMarket;
        }
        private set;
    }
    
    public List<FeedItem> Images{
        get{
            if (Images == null){
                Images = [SELECT Id, 
               ContentFileName, 
               RelatedRecordId, 
               ContentDescription 
                    FROM FeedItem 
                    WHERE ParentId in :new List<Id>{Boutique.Id} 
            AND Type = 'ContentPost'
                    ORDER BY CreatedDate DESC];
                Integer listSize = Images.size();
                
                // Remove images not linked to any related record
                for(Integer i=listSize-1; i>=0; i--){
                    if (Images[i].RelatedRecordId == null){
                        Images.remove(i);
                    }
                }
                
                listSize = Images.size();

                // Reorder to have default images first
                for (Integer i=orderedImages.size()-1; i>=0; i--){
                    Id imgId = orderedImages[i];
                    for(Integer j=listSize-1; j>=0; j--){
                        FeedItem fi = Images[j];
                        if (fi.RelatedRecordId == imgId){
                            if (Images.size() > 1 && j!=0){
                                Images.remove(j);
                                Images.add(0, fi);
                            }
                            break;
                        }
                    }
                }
            
            }
             return Images;
        }
        private set;
    }
    
    public class FeedItemRow{
        public Integer rowNumber {get;set;}
        public List<FeedItem> feedItemList {get; set;}
    
        public FeedItemRow(Integer rowNumber){
            this.rowNumber = rowNumber;
            feedItemList = new List<FeedItem>();
        }
    }
    
    public List<FeedItemRow> imagesRows {
        get{
            if(imagesRows == null){
                imagesRows = new List<FeedItemRow>();
                FeedItemRow currentImageRow;
                Integer rowNumber = 1;
                for(FeedItem item : Images){
                    if(currentImageRow == null){
                        currentImageRow = new FeedItemRow(rowNumber);
                        imagesRows.add(currentImageRow);
                    }
                    
                    currentImageRow.feedItemList.add(item);
                    
                    if(currentImageRow.feedItemList.size() == 4){
                        currentImageRow = null;
                        rowNumber++;
                    }
                }
            }
            return imagesRows;
        }
        private set;
    }
    
    public Integer imagesRowsSize{get{return imagesRows.size();}}
    public Integer documentsRowsSize{get{return documentsRows.size();}}
    
    public List<FeedItemRow> documentsRows {
        get{
            if(documentsRows == null){
                documentsRows = new List<FeedItemRow>();
                FeedItemRow currentDocumentsRow;
                Integer rowNumber = 1;
                for(FeedItem item : RECODocuments){
                    if(currentDocumentsRow == null){
                        currentDocumentsRow = new FeedItemRow(rowNumber);
                        documentsRows.add(currentDocumentsRow);
                    }
                    
                    currentDocumentsRow.feedItemList.add(item);
                    
                    if(currentDocumentsRow.feedItemList.size() == 2){
                        currentDocumentsRow = null;
                        rowNumber++;
                    }
                }
            }
            return documentsRows;
        }
        private set;
    }
    
    
    public FeedItem SelectedDocument {
        get{
            if (SelectedDocument == null) {
                if (RECODocuments.size() > 0)
                    SelectedDocument = RECODocuments[0];
            }
            return SelectedDocument;
        }
        private set;
        
    }
    
    public List<FeedItem> RECODocuments{
        get{
            if (RECODocuments == null){
                RECODocuments = new List<FeedItem>();
                
                         
        List<RECO_Project__c> reProj = [select id from RECO_Project__c where Boutique__c = :Boutique.id];
        if(reProj.size() == 1){
            RECOProjectId = reProj[0].id;
            System.debug('RECOProjectId-------$$$$' +RECOProjectId);
        }
        else {
            List<RECO_Project__c> rProj = [select id 
                            from RECO_Project__c 
                            where Boutique__c = :Boutique.id 
                            and Status__c = 'Completed' 
                            ORDER BY Effective_soft_opening_date__c DESC limit 1];
            if(!rProj.isEmpty())                
                RECOProjectId = rProj[0].id;  
         }
                
                // Organise the mainFolders in a map grouped by Name
                Map<String, Set<Id>> mainFolderIdsGroupedByName = new Map<String, Set<Id>>();
                Set<Id> mainFoldersIdSet = new Set<Id>();
                for (Attachment_Folder__c mainFolder : [SELECT Id, Name 
                                                            FROM Attachment_Folder__c 
                                                            WHERE RECO_Project__c = :RECOProjectId
                                                            and Name = 'Full Design Package'
                                ]){
                    if(!mainFolderIdsGroupedByName.containsKey(mainFolder.Name)){
                        mainFolderIdsGroupedByName.put(mainFolder.Name, new Set<Id>());
                    }
                    mainFolderIdsGroupedByName.get(mainFolder.Name).add(mainFolder.Id);
                    mainFoldersIdSet.add(mainFolder.Id);
                }
                system.debug('======mainFoldersIdSet==============='+mainFoldersIdSet);
                System.debug('mainFolderIdsGroupedByName:' + mainFolderIdsGroupedByName);
                
                // Request the subfolders               
                for(Attachment_Subfolder__c subFolder : [SELECT Name, Attachment_Folder__r.Name FROM Attachment_Subfolder__c WHERE Attachment_Folder__c = :mainFoldersIdSet]){
                    mainFolderIdsGroupedByName.get(subFolder.Attachment_Folder__r.Name).add(subFolder.Id);
                    mainFoldersIdSet.add(subFolder.Id);
                }
                
                System.debug('mainFolderIdsGroupedByName:' + mainFolderIdsGroupedByName);
            
                // Request files from the different folders until a non empty folder is found
               
                   
                        
                        RECODocuments = [
                            SELECT 
                                ContentFileName, 
                                ContentDescription, 
                                ContentSize, 
                                RelatedRecordId, CreatedBy.Name, CreatedDate 
                            FROM FeedItem WHERE ParentId in :mainFoldersIdSet ORDER BY CreatedDate DESC limit 1000
                        ];
                        
                                         
                    
                
            }
            System.debug('RECODocuments:' + RECODocuments);
            return RECODocuments;
        }
        private set;
    }
    

    public List<RECO_Project__c> BoutiqueProj{
    get{
        List<RECO_Project__c>recoPrj=[Select b.id
                       , b.Name
                       , b.Project_Name__c
                       , b.Project_Type__c
                       , b.Status__c
                       , b.Process_step__c
                       , b.Effective_soft_opening_date__c
                     From RECO_Project__c b
                     where b.Boutique__c = :Boutique.id];
        return recoPrj;        
   
    }
    private set;
    
    }
    
   
}
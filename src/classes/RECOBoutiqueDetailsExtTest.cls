/*
*   @Class      :   RECOBoutiqueDetailsExtTest.cls
*   @Description:   Test methods for class RECOBoutiqueDetailsExt.cls
*   @Author     :   PRIYA
*   @Created    :   10 May 2016
*
*   Modification Log:  
*   ------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ------------------------------------------------------------------------------------------------------------------------    
*          
*/

@isTest
private class RECOBoutiqueDetailsExtTest {
    
    static testMethod void myUnitTest() {
 
    Market__c createMarket= new Market__c(Name = 'Belgium', Code__c = 'BE', Currency__c = 'EUR', Zone__c = 'SWE');
    insert createMarket;

    Account createAccount=new Account(Market__c = createMarket.Id, Name = 'Test Account');
    insert createAccount;

    Boutique__c btq = new Boutique__c(Name = 'Test Boutique');
    insert btq;
        
    ApexPages.StandardController stdController = new ApexPages.StandardController(btq);    
    
    Test.startTest();    
    
    RECO_Project__c bProj = new RECO_Project__c(Boutique__c = btq.id, Project_Type__c = 'New');
    insert bProj;
    
    FeedItem post = new FeedItem(parentId = btq.Id);
    post.Body = 'Enter post text here';
    post.LinkUrl = 'http://www.someurl.com';
    post.ContentData = Blob.valueOf('bla bla bla');
    post.ContentFileName = 'sample.pdf';
    insert post;

    Attachment_Folder__c mainFolder = new Attachment_Folder__c(name = 'Test Pictures',RECO_Project__c=bProj.id);
    insert mainFolder;
    FeedItem feedsMain = new FeedItem (ParentId = mainFolder.id,Body = 'Test Subfolders');
    insert feedsMain;
    Attachment_Subfolder__c attsub = new Attachment_Subfolder__c(Attachment_Folder__c = mainFolder.id,name = 'test record');
    insert attsub;
    FeedItem feeds = new FeedItem (ParentId = attsub.id,Body = 'Test Subfolders');
    insert feeds;
    update mainFolder;
    
    RECOBoutiqueDetailsExt ctrl = new RECOBoutiqueDetailsExt(stdController);
	        
    List<Boutique__c> btqList = ctrl.BoutiquesInSameMarket;

    List<FeedItem> Images = ctrl.Images;
    
    System.assertEquals(ctrl.Boutique != null, true);
    
    Integer i = ctrl.imagesRowsSize + ctrl.documentsRowsSize;
    
    FeedItem SelectedDocument  = ctrl.SelectedDocument;
        
    List<RECO_Project__c> btqProjlist = ctrl.BoutiqueProj; 
        
    Test.stopTest();
      
    }
    static testMethod void myUnitTestNew() {
 
    Market__c createMarket= new Market__c(Name = 'Belgium', Code__c = 'BE', Currency__c = 'EUR', Zone__c = 'SWE');
    insert createMarket;

    Account createAccount=new Account(Market__c = createMarket.Id, Name = 'Test Account');
    insert createAccount;

    Boutique__c btq = new Boutique__c(Name = 'Test Boutique');
    insert btq;
  
    ApexPages.StandardController stdController = new ApexPages.StandardController(btq);    
    
    Test.startTest();    
    
    insert new RECO_Project__c(Boutique__c = btq.id, Project_Type__c = 'New');
    insert new RECO_Project__c(Boutique__c = btq.id, Project_Type__c = 'New', Status__c = 'Completed');
    
    FeedItem post1 = new FeedItem(parentId = btq.Id);
    post1.Body = 'Enter post text here1';
    post1.LinkUrl = 'http://www.someurl.com';
    post1.ContentData = Blob.valueOf('bla bla bla');
    post1.ContentFileName = 'sample1.pdf';
    insert post1;
    
    FeedItem post2 = new FeedItem(parentId = btq.Id);
    post2.Body = 'Enter post text here2';
    post2.LinkUrl = 'http://www.someurl.com';
    post2.ContentData = Blob.valueOf('bla bla bla11');
    post2.ContentFileName = 'sample2.pdf';
    insert post2;

    FeedItem post3 = new FeedItem(parentId = btq.Id);
    post3.Body = 'Enter post text here3';
    post3.LinkUrl = 'http://www.someurl.com';
    post3.ContentData = Blob.valueOf('bla bla bla1');
    post3.ContentFileName = 'sample3.pdf';
    insert post3;

    FeedItem post4 = new FeedItem(parentId = btq.Id);
    post4.Body = 'Enter post text here4';
    post4.LinkUrl = 'http://www.someurl.com';
    post4.ContentData = Blob.valueOf('bla bla');
    post4.ContentFileName = 'sample4.pdf';
    insert post4;

    FeedItem post5 = new FeedItem(parentId = btq.Id);
    post5.Body = 'Enter post text here5';
    post5.LinkUrl = 'http://www.someurl.com';
    post5.ContentData = Blob.valueOf('bla bla bl');
    post5.ContentFileName = 'sample5.pdf';
    insert post5;

    RECOBoutiqueDetailsExt ctrl = new RECOBoutiqueDetailsExt(stdController);
        
    List<Boutique__c> btqList = ctrl.BoutiquesInSameMarket;

    List<FeedItem> Images = ctrl.Images;
    
    System.assertEquals(ctrl.Boutique != null, true);
    
    Integer i = ctrl.imagesRowsSize + ctrl.documentsRowsSize;
    
    FeedItem SelectedDocument  = ctrl.SelectedDocument;
        
    List<RECO_Project__c> btqProjlist = ctrl.BoutiqueProj; 
        
    Test.stopTest();
      
    }
}
/*****
*   @Class      :   RECOFileUploadNotificationHelper.cls
*   @Description:   Helper class for sending Notification to project followers 
*   @Author     :   Promila Boora
*   @Created    :   4 Jan 2016
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
global class RECOFileUploadNotificationHelper
{
     global String subId;
     global Map<String,Map<String,List<Id>>> recProjectFeeds; /* Map of FollowerId containing inner map of Project id and respective feed Ids */
     global RECOFileUploadNotificationHelper(String subId,Map<String,Map<String,List<Id>>>recProjectFeeds)
     {
       this.subId=subId;
       this.recProjectFeeds=recProjectFeeds;
     }

}
/*****
*   @Class      :   RECOFileUploadNotificationIterator .cls
*   @Description:   Sending Notification to feed followers 
*   @Author     :   Promila Boora
*   @Created    :   3 Jan 2016
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
global with sharing class RECOFileUploadNotificationIterator implements Iterator<RECOFileUploadNotificationHelper>{
     public Map<String,List<FeedItem>> subsFeedItemMap;
     public Integer recordCounter {get; set;}
     global List<RECOFileUploadNotificationHelper>allSubsFeeds{get;set;}
     public Map<String,EntitySubscription>subscriberIdRec{get;set;} 
     Map<String,RECO_Project__c>AllRecProjects;
     Map<String,Map<String,Attachment_Folder__c>>AllRecProjectFolders;
     public RECOFileUploadNotificationIterator (){ 
        recordCounter =0;
        allSubsFeeds=new List<RECOFileUploadNotificationHelper>();
        AllRecProjects=new Map<String,RECO_Project__c>();
        AllRecProjectFolders=new Map<String,Map<String,Attachment_Folder__c>>();
        Map<String,List<Id>> folderFeedItemMap=new Map<String,List<Id>>(); // CONTAINS MAP OF FolderId and Respective Feeds
        Map<String,List<Id>>recProjFeedsMap=new Map<String,List<Id>>(); // CONTAINS MAP of Project and Respective feeds
        Map<String,List<String>> subscriberProjectMap= new Map<String,List<String>>();  //CONTAINS Map of subscriber and respective projects
        subscriberIdRec=new Map<String,EntitySubscription>();  // CONTAINS map of subsciber id and email                                            
       
        for(Attachment_Folder__feed fRec:[Select Id,ContentFileName,ContentSize,CreatedDate,LinkUrl,ParentId,RelatedRecordId,Title,Type from Attachment_Folder__feed where Type='ContentPost' and createdDate=today ]){
               if(folderFeedItemMap.containsKey(fRec.parentId)){
                    folderFeedItemMap.get(fRec.parentId).add(fRec.Id);
                }
                else{
                    folderFeedItemMap.put(fRec.parentId,new List<String>{fRec.Id}); 
                }
        }
        
        for(Attachment_Subfolder__feed fRec:[Select Id,ContentFileName,ContentSize,CreatedDate,LinkUrl,ParentId,RelatedRecordId,Title,Type from Attachment_Subfolder__feed where Type='ContentPost' and createdDate=today ]){
               if(folderFeedItemMap.containsKey(fRec.parentId)){
                    folderFeedItemMap.get(fRec.parentId).add(fRec.Id);
                }
                else{
                    folderFeedItemMap.put(fRec.parentId,new List<String>{fRec.Id}); 
                }
        }
       
        List<Attachment_Folder__c> attachFolderRecs=[Select id,Name,RECO_Project__c from 
                                                            Attachment_Folder__c where (Name!='Budget & Offers' and Name!='Budget & Costs')and 
                                                            id in:folderFeedItemMap.keySet()];
        for(Attachment_Folder__c att:attachFolderRecs)  
        {
            List<Id>feeds=new List<String>();
            feeds.addAll(folderFeedItemMap.get(att.Id));
            if(recProjFeedsMap.containsKey(att.RECO_Project__c)){
                  List<Id>allPrevFeeds=recProjFeedsMap.get(att.RECO_Project__c);
                  feeds.addAll(allPrevFeeds);
                  recProjFeedsMap.put(att.RECO_Project__c,feeds);
            }
            else{
                  recProjFeedsMap.put(att.RECO_Project__c,feeds); 
            }
        } 
        /***********************/
       
        List<Attachment_SubFolder__c> attachSubFolderRecs=[Select id,Name,Attachment_Folder__r.RECO_Project__c from 
                                                            Attachment_SubFolder__c where (Attachment_Folder__r.Name!='Budget & Offers' and Attachment_Folder__r.Name!='Budget & Costs')and 
                                                            id in:folderFeedItemMap.keySet()];
        for(Attachment_SubFolder__c att:attachSubFolderRecs)  
        {
            List<Id>feeds=new List<String>();
            feeds.addAll(folderFeedItemMap.get(att.Id));
            if(recProjFeedsMap.containsKey(att.Attachment_Folder__r.RECO_Project__c)){
                  List<Id>allPrevFeeds=recProjFeedsMap.get(att.Attachment_Folder__r.RECO_Project__c);
                  feeds.addAll(allPrevFeeds);
                  recProjFeedsMap.put(att.Attachment_Folder__r.RECO_Project__c,feeds);
            }
            else{
                  recProjFeedsMap.put(att.Attachment_Folder__r.RECO_Project__c,feeds); 
                  
            }
        } 
        /***********************/
      
        /**** EntitySubscription starts here********/
        List<EntitySubscription> enSubList=[SELECT ParentId,subscriberId, subscriber.name, 
                                                        subscriber.email, subscriber.firstname,subscriber.lastname,Parent.Name FROM 
                                                        EntitySubscription WHERE ParentId In:
                                                        recProjFeedsMap.keySet()];
        for(EntitySubscription en:enSubList)
        {
            subscriberIdRec.put(en.subscriberId,en);
            if(subscriberProjectMap.containsKey(en.subscriberid)){
                subscriberProjectMap.get(en.subscriberid).add(en.ParentId);
            }
            else{
                subscriberProjectMap.put(en.subscriberId,new List<String>{en.ParentId});
                
            }
        }           
        
         
       /**** EntitySubscription ends here********/
       
      /****** main method to retrieve subscriber,project and feed starts here ******/ 
       subsFeedItemMap=new Map<String,List<FeedItem>>();
       Map<String,Map<String,List<Id>>>recSubsProjectFeedsMap=new Map<String,Map<String,List<Id>>>();
       for(String subsId:subscriberProjectMap.keySet())  //LOOP on Subscribers
       {
        Map<String,List<String>> recProjectFeedsMap=new Map<String,List<String>>();
        for(String projId:subscriberProjectMap.get(subsId)){  /** getting project Ids using subscriber id **/
            if(!recProjFeedsMap.get(projId).isEmpty())
            {
             List<Id>feeds=new List<Id>();
             feeds.addAll(recProjFeedsMap.get(projId));
             if(recSubsProjectFeedsMap.containsKey(subsId)){
                 if(recProjectFeedsMap.containsKey(projId))
                 {
                      List<Id>allPrevFeeds=recProjFeedsMap.get(projId);
                      feeds.addAll(allPrevFeeds);
                      recProjectFeedsMap.put(projId,feeds);
                      recSubsProjectFeedsMap.put(subsId,recProjectFeedsMap);
                 }
                 else
                 {
                      recProjectFeedsMap.put(projId,feeds);
                      recSubsProjectFeedsMap.put(subsId,recProjectFeedsMap);
                 }
             }
             else{
                
                 recProjectFeedsMap.put(projId,feeds);
                 recSubsProjectFeedsMap.put(subsId,recProjectFeedsMap);
               
             }
             
            }
          
        }
           
        RECOFileUploadNotificationHelper subFeeds= new RECOFileUploadNotificationHelper(subsId,recSubsProjectFeedsMap);
        allSubsFeeds.add(subFeeds);
       }
       //system.debug('=========Final subscriber, projects and Feeds====='+recSubsProjectFeedsMap);
       /****** main method to retrieve subscriber,project and feed ends here ******/ 
       
     }    
    
    public boolean hasNext(){ 
       if(recordCounter  >= allSubsFeeds.size()) {
           return false; 
       } else {
           return true; 
       }
    }    

    public RECOFileUploadNotificationHelper next(){  
           if(recordCounter >allSubsFeeds.size()){return null;} 
           recordCounter++; 
           return allSubsFeeds[recordCounter-1]; 
    } 
    
}
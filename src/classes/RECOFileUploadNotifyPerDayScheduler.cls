/*****
*   @Class      :   RECOFileUploadNotifyPerDayScheduler .cls
*   @Description:   Scheduler for RECOBatchFileUploadNotificationPerDay Batch
*   @Author     :   Promila Boora
*   @Created    :   13 Oct 2015
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
global class RECOFileUploadNotifyPerDayScheduler implements Schedulable {

   global void execute(SchedulableContext SC) {
      RECOBatchFileUploadNotificationPerDay schFileUploadNotification= new RECOBatchFileUploadNotificationPerDay(); 
      database.executebatch(schFileUploadNotification,1);
   }
}
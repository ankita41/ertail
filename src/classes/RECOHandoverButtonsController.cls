/*****
*   @Class      :   RECOHandoverButtonsController.cls
*   @Description:   Controller class for the RECOHandoverButtons VF page
*   @Author     :   Greg Baxter
*   @Created    :   23 Oct 2013
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy        09 APR 2014     Fix: page error if no content exists
*   Jacky Uy        11 APR 2014     Change: After the Handover is validated, the Validation Status = Snag List Confirmed
*   Jacky Uy        11 APR 2014     Change: Handover__c.Validation_Status__c==Waiting for Validation should be "Waiting for Snag List Confirmation"
*   Jacky Uy        24 JUL 2014     Added access to all Support Stakeholders
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*                         
*****/

public without sharing class RECOHandoverButtonsController {
    public Handover__c handover {get; set;}
    private final String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
    private final String SYS_ADM = UserConstants.SYS_ADM;
    private final String BTQ_RPM = UserConstants.BTQ_RPM;
    private final String BTQ_NBM = UserConstants.BTQ_NBM;
    private final String BTQ_IRO = UserConstants.BTQ_IRO;
    private final String BTQ_MA = UserConstants.BTQ_MA;
    private final String BTQ_DA = UserConstants.BTQ_DA;
    private final String BTQ_PO = UserConstants.BTQ_PO;
    private final String BTQ_FSupp = UserConstants.BTQ_FSupp;
    private final String BTQ_ViewAll = UserConstants.BTQ_ViewAll;
    private final String NCAFE_RPM = UserConstants.NCAFE_RPM;
    private final String NCAFE_NBM = UserConstants.NCAFE_NBM;
    private final String NCAFE_IRO = UserConstants.NCAFE_IRO;
    private final String NCAFE_MA = UserConstants.NCAFE_MA;
    private final String NCAFE_DA = UserConstants.NCAFE_DA;
    private final String NCAFE_PO = UserConstants.NCAFE_PO;
    private final String NCAFE_FSupp = UserConstants.NCAFE_FSupp;
    private final String NCAFE_ViewAll = UserConstants.NCAFE_ViewAll;
    private String hasAccessRights {get;set;}
    private Id ncafeRTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId(); //fetching NCAFE recordtypeid
    
    public Boolean hasAccessToHandover {
        get {
           if(handover.RECO_Project__r.RecordtypeId==ncafeRTypeId)
           {
              return ((currentUserProfile==NCAFE_RPM && isStakeholder('RPM')) ||
                    (currentUserProfile==NCAFE_RPM && isStakeholder('SRPM')) ||
                    (currentUserProfile==NCAFE_NBM && isStakeholder('NBM')) ||
                    (currentUserProfile==NCAFE_NBM && isStakeholder('SNBM')) ||
                    (currentUserProfile==NCAFE_IRO && isStakeholder('IRO')) ||
                    (currentUserProfile==NCAFE_IRO && isStakeholder('SIRO')) ||
                    (currentUserProfile==NCAFE_PO && isStakeholder('PO')) ||
                    (currentUserProfile==NCAFE_PO && isStakeholder('SPO')) ||
                    (currentUserProfile==NCAFE_MA && isStakeholder('MA')) || 
                    (currentUserProfile==NCAFE_MA && isStakeholder('SMA')) || 
                    (currentUserProfile==NCAFE_DA && isStakeholder('DA')) ||
                    (currentUserProfile==NCAFE_DA && isStakeholder('SDA')) ||
                    (currentUserProfile==NCAFE_FSupp && isStakeholder('FSupp')) ||
                    (currentUserProfile==NCAFE_ViewAll || currentUserProfile==SYS_ADM));
           }
           else{
            return ((currentUserProfile==BTQ_RPM && isStakeholder('RPM')) ||
                    (currentUserProfile==BTQ_RPM && isStakeholder('SRPM')) ||
                    (currentUserProfile==BTQ_NBM && isStakeholder('NBM')) ||
                    (currentUserProfile==BTQ_NBM && isStakeholder('SNBM')) ||
                    (currentUserProfile==BTQ_IRO && isStakeholder('IRO')) ||
                    (currentUserProfile==BTQ_IRO && isStakeholder('SIRO')) ||
                    (currentUserProfile==BTQ_PO && isStakeholder('PO')) ||
                    (currentUserProfile==BTQ_PO && isStakeholder('SPO')) ||
                    (currentUserProfile==BTQ_MA && isStakeholder('MA')) || 
                    (currentUserProfile==BTQ_MA && isStakeholder('SMA')) || 
                    (currentUserProfile==BTQ_DA && isStakeholder('DA')) ||
                    (currentUserProfile==BTQ_DA && isStakeholder('SDA')) ||
                    (currentUserProfile==BTQ_FSupp && isStakeholder('FSupp')) ||
                    (currentUserProfile==BTQ_ViewAll || currentUserProfile==SYS_ADM));
           }
        } set;
    } 
    
    private Boolean isStakeholder(String role){
        if(role=='RPM')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.Retail_project_manager__c);
        else if(role == 'SRPM')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.Other_Retail_project_manager__c);
        else if(role == 'NBM')     
            return (UserInfo.getUserId()==handover.RECO_Project__r.National_boutique_manager__c);
        else if(role == 'SNBM')     
            return (UserInfo.getUserId()==handover.RECO_Project__r.Support_National_Boutique_Manager__c);
        else if(role == 'IRO')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.International_Retail_Operator__c);
        else if(role == 'SIRO')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.Support_International_Retail_Operations__c);
        else if(role == 'PO')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.PO_operator__c);
        else if(role == 'SPO')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.Support_PO_Operator__c);
        else if(role == 'MA')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.Manager_architect__c); 
        else if(role == 'SMA')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.Other_Manager_Architect__c); 
        else if(role == 'DA')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.Design_architect__c); 
        else if(role == 'SDA')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.Other_Design_Architect__c); 
        else if(role == 'FSupp')    
            return (UserInfo.getUserId()==handover.RECO_Project__r.Furniture_Supplier_User__c); 
        else
            return false;
    }
    
    //BUTTONS WILL ONLY DISPLAY IF THE USER IS A STAKEHOLDER
    public Boolean showButtons {
        get {
            if(handover.RECO_Project__r.Workflow_Type__c=='Standard Process')
                return (isStakeholder('SMA') || isStakeholder('MA') || isStakeholder('SNBM') || isStakeholder('NBM') || isStakeholder('SRPM') || isStakeholder('RPM') || UserInfo.getUserId()==handover.Responsible__c || currentUserProfile==SYS_ADM);
            else
                return (isStakeholder('SNBM') || isStakeholder('NBM') || isStakeholder('SRPM') || isStakeholder('RPM') || UserInfo.getUserId()==handover.Responsible__c || currentUserProfile==SYS_ADM);
        } set;
    }
    
    public Boolean showSendForValidation {
        get{
         Boolean showSendForValidationBtn;
            if((handover.RECO_Project__r.Workflow_Type__c=='Standard Process' || handover.RECO_Project__r.Workflow_Type__c=='NCafe Process') && handover.RECO_Project__r.RecordtypeId==ncafeRTypeId){ // Implemented IF for NCAFE users
                hasAccessRights = NCAFE_MA + ',' + NCAFE_RPM + ',' + SYS_ADM;
                showSendForValidationBtn=(!handover.Notify_for_validation__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('MA')));
            }
            else if(handover.RECO_Project__r.Workflow_Type__c=='Standard Process' && handover.RECO_Project__r.RecordtypeId!=ncafeRTypeId){
                hasAccessRights = BTQ_MA + ',' + BTQ_RPM + ',' + SYS_ADM;
                showSendForValidationBtn=(!handover.Notify_for_validation__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('MA')));
            }
            else{
                if(handover.RECO_Project__r.RecordtypeId!=ncafeRTypeId){
                    hasAccessRights = BTQ_NBM + ',' + BTQ_RPM + ',' + SYS_ADM;
                    showSendForValidationBtn=(!handover.Notify_for_validation__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('NBM')));
                }
            }
            return showSendForValidationBtn;
        } set;
    }
    
    public Boolean showValidateReject {
        get{
           if(handover.RECO_Project__r.RecordtypeId==ncafeRTypeId){
            hasAccessRights = NCAFE_RPM + ',' + SYS_ADM;
            return (handover.Validation_status__c=='Waiting for Snag List Confirmation' && !handover.Notify_validation_decision__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('RPM')));
           }
           else{
            hasAccessRights = BTQ_RPM + ',' + SYS_ADM;
            return (handover.Validation_status__c=='Waiting for Snag List Confirmation' && !handover.Notify_validation_decision__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('RPM')));
           }
        } set;
    }
    
    public Boolean showSendForApproval {
        get{
            Boolean showSendForApprovalBtn;
            if(handover.RECO_Project__r.RecordtypeId==ncafeRTypeId && (handover.RECO_Project__r.Workflow_Type__c=='Standard Process' || handover.RECO_Project__r.Workflow_Type__c=='NCafe Process')){
                hasAccessRights = NCAFE_MA + ',' + NCAFE_RPM + ',' + SYS_ADM;
                showSendForApprovalBtn=(handover.Validation_status__c=='Snag List Confirmed' && !handover.Notify_for_approval__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('MA')));
            }
            else if(handover.RECO_Project__r.Workflow_Type__c=='Standard Process' && handover.RECO_Project__r.RecordtypeId!=ncafeRTypeId){
                hasAccessRights = BTQ_MA + ',' + BTQ_RPM + ',' + SYS_ADM;
                showSendForApprovalBtn=(handover.Validation_status__c=='Snag List Confirmed' && !handover.Notify_for_approval__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('MA')));
            }
            else{
                if(handover.RECO_Project__r.RecordtypeId!=ncafeRTypeId){
                hasAccessRights = BTQ_NBM + ',' + BTQ_RPM + ',' + SYS_ADM;
                showSendForApprovalBtn=(handover.Validation_status__c=='Snag List Confirmed' && !handover.Notify_for_approval__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('NBM')));
               }
            }
            return showSendForApprovalBtn;
        } set;
    }
    
    public Boolean showApproveDisapprove {
        get{
           if(handover.RECO_Project__r.RecordtypeId==ncafeRTypeId ){
                hasAccessRights = NCAFE_RPM + ',' + SYS_ADM;
                return (handover.Approval_status__c=='Waiting for approval' && !handover.Notify_approval_decision__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('RPM')));
           }
           else{
                hasAccessRights = BTQ_RPM + ',' + SYS_ADM;
                return (handover.Approval_status__c=='Waiting for approval' && !handover.Notify_approval_decision__c && (hasAccessRights.contains(currentUserProfile) || isStakeholder('RPM')));
            }
        } set;
    }
    
    public Boolean rejectionReasonFilled {
        get{
            return (handover.Reason__c != null);
        } set;
    }
    
    public Boolean hasSnags {
        get{
            Integer nrSnags = [SELECT count() FROM Snag__c WHERE Handover__c = :handover.Id];
            return (nrSnags > 0);
        } set;
    }
    
    public Boolean hasToDoSnags {
        get{
            Integer nrSnags = [SELECT count() FROM Snag__c WHERE Handover__c = :handover.Id AND Status__c = 'To Do'];
            return (nrSnags > 0);
        } set;
    }
    
    public Boolean noDocumentAttached {
        get {
            //SF CONTENT (OLD IMPLEMENTATION)
            /*List<Content_Folder__c> handoverFolder = [SELECT Id FROM Content_Folder__c WHERE Name = 'Visit report & Handover'];
            List<ContentVersion> documents = new List<ContentVersion>();
            
            if(!handoverFolder.isEmpty())
                documents = [SELECT Id FROM ContentVersion WHERE IsLatest = true AND Handover__c = :handover.Id AND Content_Folder__c = :handoverFolder[0].Id];*/
            
            //SF CHATTER
            List<Attachment_Folder__c> folder = [SELECT Id FROM Attachment_Folder__c WHERE Name = 'Visit Reports & Handovers' AND Handover__c = :handover.Id];
            List<FeedItem> documents = new List<FeedItem>();
            
            if(!folder.isEmpty())
                documents = [SELECT Id FROM FeedItem WHERE ParentId = :folder[0].Id AND Type = 'ContentPost'];
            
            return documents.isEmpty();
        } set;
    }

    public ApexPages.StandardController pageController {get; set;}
    private PageReference retHandoverPage {get;set;}
    public string validateTemplateNameMnrArch;
    public string validateTemplateNameNBM;
    public string validateTemplateNameSupp;
    public string approveTemplateName;
    
    public RECOHandoverButtonsController (ApexPages.StandardController con) {
        pageController = con;
        String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
        
        Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Handover__c').getDescribe().fields.getMap();
        String qryStr = 'SELECT RECO_Project__r.Retail_project_manager__c,RECO_Project__r.RecordtypeId, RECO_Project__r.Other_Retail_Project_Manager__c, ' + 
                        'RECO_Project__r.National_boutique_manager__c, RECO_Project__r.Support_National_Boutique_Manager__c, ' +
                        'RECO_Project__r.International_Retail_Operator__c, RECO_Project__r.Support_International_Retail_Operations__c, ' +
                        'RECO_Project__r.Design_architect__c , RECO_Project__r.Other_Design_Architect__c, ' +
                        'RECO_Project__r.PO_operator__c , RECO_Project__r.Support_PO_Operator__c, ' +
                        'RECO_Project__r.Furniture_Supplier_User__c, ' +
                        'RECO_Project__r.Manager_architect__c, RECO_Project__r.Other_Manager_Architect__c, RECO_Project__r.Workflow_Type__c, ';
        for(String s : fieldsMap.keySet()){
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM Handover__c WHERE Id = \'' + con.getRecord().Id + '\' ';
        handover = Database.query(qryStr);
        // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType - Start
        if(handover.RECO_Project__r.RecordtypeId==ncafeRTypeId){
            validateTemplateNameMnrArch='NCAFE_NotifMnrArchitectHandoverValidated';
            validateTemplateNameNBM='NCAFE_NotifyNBMHandoverValidated';
            validateTemplateNameSupp='NCAFE_NotifySupplierHandoverValidated';
            approveTemplateName='NCAFE_NotifyAllHandoverApproved'; 
         }else {
            validateTemplateNameMnrArch='RECO_NotifMnrArchitectHandoverValidated';
            validateTemplateNameNBM='RECO_NotifyNBMHandoverValidated';
            validateTemplateNameSupp='RECO_NotifySupplierHandoverValidated';
            approveTemplateName='RECO_NotifyAllHandoverApproved';
            
         }               
        // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType - End
        retHandoverPage = new PageReference('/' + handover.Id);
        retHandoverPage.setRedirect(true);
    }

    public PageReference backToProject() {
        PageReference retPage = new PageReference('/' + handover.RECO_Project__c);
        retPage.setRedirect(true);
        return retPage;
    }
    
    public PageReference sendForValidation() {        
        handover.Notify_for_validation__c = true;
        update handover;
        return retHandoverPage;
    }
    
    public PageReference validateHandover() {        
        HandoverPDFGenerator pdfGen = new HandoverPDFGenerator(pageController);
         //if(handover.Retail_project_manager__c!=null)
        //    pdfGen.sendPDF(validateTemplateHTMLBody,validateTemplateHTMLSubject,handover.Retail_project_manager__c,true,false);
        //system.debug('===HTML Body==='+validateTemplateNameSupp);
        if(handover.National_boutique_manager__c!=null)
            pdfGen.sendPDF(validateTemplateNameNBM,handover.National_boutique_manager__c,true,false,false);
        
        if(handover.RECO_Project__r.Workflow_Type__c=='Standard Process' && handover.Manager_architect__c!=null)
            pdfGen.sendPDF(validateTemplateNameMnrArch, handover.Manager_architect__c,true,false,false);
            
        if (handover.Supplier_contact__c != null)
            pdfGen.sendPDF(validateTemplateNameSupp,handover.Supplier_contact__c,true,false,false);
            
        if(handover.Responsible__c!=null)
            pdfGen.sendPDF(validateTemplateNameNBM,handover.Responsible__c,false,false,false);
            
        if(handover.Email_Address_1__c!=null)
            pdfGen.sendPDF(validateTemplateNameSupp,handover.Email_Address_1__c,false,true,false);
        
        if(handover.Email_Address_2__c!=null)
            pdfGen.sendPDF(validateTemplateNameSupp, handover.Email_Address_2__c,false,true,false);
            
        if (handover.Other_Manager_Architect__c != null)
            pdfGen.sendPDF(validateTemplateNameMnrArch,handover.Other_Manager_Architect__c,false,false,false);
            
        if (handover.Support_National_Boutique_Manager__c != null)
            pdfGen.sendPDF(validateTemplateNameNBM,handover.Support_National_Boutique_Manager__c,false,false,false);
            
        handover.Validation_status__c = 'Snag List Confirmed';
        handover.Notify_validation_decision__c = true;
        handover.Notify_validation_decision_date__c=SYSTEM.Now();
        update handover;
        return retHandoverPage;
    }
    
    public PageReference rejectHandover() {
        handover.Validation_status__c = 'Rejected';
        handover.Notify_validation_decision__c = true;
        update handover;
        return retHandoverPage;
    }

    public PageReference sendForApproval() {        
        handover.Notify_for_approval__c = true;
        update handover;
        return retHandoverPage;
    }
    
    public PageReference approveHandover() {
        HandoverPDFGenerator pdfGen = new HandoverPDFGenerator(pageController);
        
         if(handover.Retail_project_manager__c!=null)
            pdfGen.sendPDF(approveTemplateName,handover.Retail_project_manager__c,true,false,true);
            
        if(handover.National_boutique_manager__c!=null)
            pdfGen.sendPDF(approveTemplateName,handover.National_boutique_manager__c,true,false,true);
        
        if(handover.RECO_Project__r.Workflow_Type__c=='Standard Process' && handover.Manager_architect__c!=null)
            pdfGen.sendPDF(approveTemplateName,handover.Manager_architect__c,true,false,true);
        
        if(handover.Supplier_contact__c != null)
            pdfGen.sendPDF(approveTemplateName,handover.Supplier_contact__c,true,false,true);
            
        if(handover.Responsible__c!=null)
            pdfGen.sendPDF(approveTemplateName,handover.Responsible__c,false,false,true);
            
        if(handover.Email_Address_1__c!=null)
            pdfGen.sendPDF(approveTemplateName,handover.Email_Address_1__c,false,true,true);
        
        if(handover.Email_Address_2__c!=null)
            pdfGen.sendPDF(approveTemplateName, handover.Email_Address_2__c,false,true,true);
            
        if (handover.Other_Manager_Architect__c != null)
            pdfGen.sendPDF(approveTemplateName,handover.Other_Manager_Architect__c,false,false,true);
            
        if (handover.Support_National_Boutique_Manager__c != null)
            pdfGen.sendPDF(approveTemplateName,handover.Support_National_Boutique_Manager__c,false,false,true);
            
        handover.Approval_status__c = 'Approved';
        handover.Notify_approval_decision__c = true;
        handover.Notify_approval_decision_date__c=SYSTEM.Now(); //added so that workflow can be dectivated for both NCAFE and RECO
        update handover;
        return retHandoverPage;
    }
    
    public PageReference disapproveHandover() {
        handover.Approval_status__c = 'Disapproved';
        handover.Notify_approval_decision__c = true;
        update handover;
        return retHandoverPage;
    }
    
    }
public with sharing class RECOInvoiceButtonsExt {


    private final String SYS_ADM = UserConstants.SYS_ADM;
    private final String BTQ_RPM = UserConstants.BTQ_RPM;
    private final String BTQ_NBM = UserConstants.BTQ_NBM;
    private final String BTQ_MA = UserConstants.BTQ_MA;
    private final String BTQ_PO = UserConstants.BTQ_PO;
    private final String BTQ_Proc = UserConstants.BTQ_Proc;
    private final String BTQ_Fin = UserConstants.BTQ_Fin;
    private final String NCAFE_RPM = UserConstants.NCAFE_RPM;
    private final String NCAFE_NBM = UserConstants.NCAFE_NBM;
    private final String NCAFE_MA = UserConstants.NCAFE_MA;
    private final String NCAFE_PO = UserConstants.NCAFE_PO;
    private final String NCAFE_Proc = UserConstants.NCAFE_Proc;
    private final String NCAFE_Fin = UserConstants.NCAFE_Fin;
    private final String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
    private String hasAccessRights {get;set;}
    private Id ncafeRTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId(); //fetching NCAFE recordtypeid
    public Boolean hasAccessToQuotation {
        get {
          if(offer.RECO_Project__r.RecordtypeId==ncafeRTypeId)
          {
          return ((currentUserProfile==NCAFE_RPM && isStakeholder('RPM')) ||
                    (currentUserProfile==NCAFE_RPM && isStakeholder('SRPM')) ||
                    (currentUserProfile==NCAFE_NBM && isStakeholder('NBM')) ||
                    (currentUserProfile==NCAFE_NBM && isStakeholder('SNBM')) ||
                    ((currentUserProfile==NCAFE_PO || currentUserProfile==BTQ_NBM) && isStakeholder('PO')) ||
                    ((currentUserProfile==NCAFE_PO || currentUserProfile==BTQ_NBM) && isStakeholder('SPO')) ||
                    (currentUserProfile==NCAFE_MA && isStakeholder('MA')) || 
                    (currentUserProfile==NCAFE_MA && isStakeholder('SMA')) || 
                    (currentUserProfile==NCAFE_Proc || currentUserProfile==NCAFE_Fin || currentUserProfile==SYS_ADM));
          }
          else{
            return ((currentUserProfile==BTQ_RPM && isStakeholder('RPM')) ||
                    (currentUserProfile==BTQ_RPM && isStakeholder('SRPM')) ||
                    (currentUserProfile==BTQ_NBM && isStakeholder('NBM')) ||
                    (currentUserProfile==BTQ_NBM && isStakeholder('SNBM')) ||
                    ((currentUserProfile==BTQ_PO || currentUserProfile==BTQ_NBM) && isStakeholder('PO')) ||
                    ((currentUserProfile==BTQ_PO || currentUserProfile==BTQ_NBM) && isStakeholder('SPO')) ||
                    (currentUserProfile==BTQ_MA && isStakeholder('MA')) || 
                    (currentUserProfile==BTQ_MA && isStakeholder('SMA')) || 
                    (currentUserProfile==BTQ_Proc || currentUserProfile==BTQ_Fin || currentUserProfile==SYS_ADM));
               }
        } set;
    } 
    
    private Boolean isStakeholder(String role){
        if(role == 'RPM')    
            return (offer.RECO_Project__r.RecordtypeId!=ncafeRTypeId?currentUserProfile==BTQ_RPM:currentUserProfile==NCAFE_RPM);  //(UserInfo.getUserId()==offer.RECO_Project__r.Retail_project_manager__c);
        else if(role == 'SRPM')    
            return (UserInfo.getUserId()==offer.RECO_Project__r.Other_Retail_project_manager__c);
        else if(role == 'NBM')     
            return (UserInfo.getUserId()==offer.RECO_Project__r.National_boutique_manager__c);
        else if(role == 'SNBM')     
            return (UserInfo.getUserId()==offer.RECO_Project__r.Support_National_Boutique_Manager__c);
        else if(role == 'PO')    
            return (UserInfo.getUserId()==offer.RECO_Project__r.PO_operator__c);
        else if(role == 'SPO')    
            return (UserInfo.getUserId()==offer.RECO_Project__r.Support_PO_Operator__c);
        else if(role == 'MA')    
            return (UserInfo.getUserId()==offer.RECO_Project__r.Manager_architect__c); 
        else if(role == 'SMA')    
            return (UserInfo.getUserId()==offer.RECO_Project__r.Other_Manager_Architect__c); 
        else
            return false;
    }
    
    //BUTTONS WILL ONLY DISPLAY IF THE USER IS A STAKEHOLDER
    public Boolean showButtons {
        get {
            return (isStakeholder('SMA') || isStakeholder('MA') || isStakeholder('PO') || isStakeholder('SPO') || isStakeholder('SNBM') || isStakeholder('NBM') || isStakeholder('SRPM') || isStakeholder('RPM') || currentUserProfile == SYS_ADM);
        } set;
    }
    
    public Boolean isEditable {get; set;}
    public Boolean showSendForApproval {
        get {
         if(offer.RECO_Project__r.RecordtypeId==ncafeRTypeId)
          {
           //hasAccessRights = NCAFE_RPM + ',' + NCAFE_NBM + ',' + NCAFE_MA + ',' + SYS_ADM;
           return (!offer.Notify_for_approval__c );
          }
          else
          {
            return false;
            //hasAccessRights = BTQ_RPM + ',' + BTQ_NBM + ',' + BTQ_MA + ',' + SYS_ADM;
            
            //return (!offer.Notify_for_approval__c && (hasAccessRights.contains(currentUserProfile)|| isStakeholder('MA') || isStakeholder('NBM') || isStakeholder('RPM')));
          }
        } set;
    }
    public Boolean showSendDecision {
        get {
         if(offer.RECO_Project__r.RecordtypeId==ncafeRTypeId)
          {
            id profileId= userinfo.getProfileId();
            system.debug('==============='+invRec.offer__r.createdby.username);
            system.debug('============test==='+Userinfo.getusername());
            String profilName=[select Name from profile where id = :profileId].name;
            return (offer.Notify_for_approval__c && offer.Approval_status__c == 'Waiting for approval' && profilName.contains('Admin'))||(offer.Notify_for_approval__c && offer.Approval_status__c == 'Waiting for approval' && invRec.offer__r.createdby.username==Userinfo.getusername());
          }
          else{
              return false;
          }
          
        } set;
    }
    public Boolean noCostItemsSelected {get;set;}
   /* public Boolean noCostItemsSelected {
        get {
            List<Cost_Item__c> tempCI = [SELECT Id FROM Cost_Item__c WHERE Invoice__c = :offer.Id];
            return tempCI.isEmpty();
        } set;
    }*/
    
    public Boolean noDocumentAttached {
        get {
            //SF CONTENT (OLD IMPLEMENTATION)
            /*List<Content_Folder__c> quotationFolder = [SELECT Id FROM Content_Folder__c WHERE Name = 'Budget & Costs'];
            List<ContentVersion> documents = new List<ContentVersion>();
            
            if(!quotationFolder.isEmpty())
                documents = [SELECT Id FROM ContentVersion WHERE IsLatest = true AND Invoice__c = :offer.Id AND Content_Folder__c = :quotationFolder[0].Id];*/
            
            //SF CHATTER
            List<Attachment_Folder__c> folder = [SELECT Id FROM Attachment_Folder__c WHERE Name = 'Budget & Costs' ];
            //AND Invoice__c = :offer.Id];
            List<FeedItem> documents = new List<FeedItem>();
            
            if(!folder.isEmpty())
                documents = [SELECT Id FROM FeedItem WHERE ParentId = :folder[0].Id AND Type = 'ContentPost'];
            
            return documents.isEmpty();
        } set;
    }
    public Boolean multipleDocumentsAttached {get;set;}
    /*public Boolean multipleDocumentsAttached {
        get {
            //SF CHATTER
            List<Attachment_Folder__c> folder = [SELECT Id FROM Attachment_Folder__c WHERE Name = 'Budget & Costs' AND Invoice__c = :offer.Id];
            List<FeedItem> documents = new List<FeedItem>();
            
            if(!folder.isEmpty())
                documents = [SELECT Id FROM FeedItem WHERE ParentId = :folder[0].Id AND Type = 'ContentPost'];
            
            return (!documents.isEmpty() && documents.size()>1);
        } set;
    }*/
    
    public Boolean refusalReasonEntered {
        get {
            return offer.Reason__c != null;
        } set;
    }
    
    public Invoice__c offer {get;set;}
    private Id projectRTId ;
    private Id expectedRTId;
    private Id validatedRTId; 
    private Id rejectedRTId;
    private Id pendingApprovalRTId;
    private Id draftRTId;
    private PageReference retOfferPage {get;set;}
    String invoiceId;
    Invoice__c invRec;
    public boolean checkAttchmentBoolean{get;set;}
    public void checkAttachment()
    {
        List<Attachment> attRec=[Select id from attachment where parentid=:invoiceId];
        if(!attRec.isEmpty()){
            checkAttchmentBoolean=false;
        }
        else{
            checkAttchmentBoolean=true;
        }
    }    
    
    
    public RECOInvoiceButtonsExt(ApexPages.StandardController con) {
        checkAttchmentBoolean=true;
    
         invoiceId=con.getRecord().Id;
         invRec=[Select id,offer__c,Reason__c ,reco_project__r.owner.username,offer__r.createdby.username,offer__r.createdby.profileId from Invoice__c where id=:invoiceId]; 
         
         checkAttachment();
    
         Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Invoice__c').getDescribe().fields.getMap();
        String qryStr = 'SELECT RECO_Project__r.RecordtypeId,RECO_Project__r.Retail_project_manager__c , RECO_Project__r.Other_Retail_Project_Manager__c, ' + 
                        'RECO_Project__r.National_boutique_manager__c, RECO_Project__r.Support_National_Boutique_Manager__c, ' +
                        'RECO_Project__r.PO_operator__c, RECO_Project__r.Support_PO_Operator__c, ' +
                        'RECO_Project__r.Manager_architect__c, RECO_Project__r.Other_Manager_Architect__c, ' +
                        'RECO_Project__r.Design_architect__c, RECO_Project__r.Other_Design_Architect__c, RECO_Project__r.Workflow_Type__c, ';
        for(String s : fieldsMap.keySet()){
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM Invoice__c WHERE Id = \'' + con.getRecord().Id + '\' ';
        offer = Database.query(qryStr);
        projectRTId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
        expectedRTId = (projectRTId!=offer.RECO_Project__r.RecordtypeId?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Expected').getRecordTypeId()));
        validatedRTId = (projectRTId!=offer.RECO_Project__r.RecordtypeId?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Confirmed').getRecordTypeId()));
        rejectedRTId = (projectRTId!=offer.RECO_Project__r.RecordtypeId?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Rejected').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('NCAFE Rejected').getRecordTypeId()));
        pendingApprovalRTId = (projectRTId!=offer.RECO_Project__r.RecordtypeId?(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Pending approval').getRecordTypeId()):(Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Pending approval').getRecordTypeId()));
        draftRTId = Schema.sObjectType.Invoice__c.getRecordTypeInfosByName().get('Draft').getRecordTypeId();
        isEditable = (offer.RecordTypeId == draftRTId && offer.Approval_status__c != 'Waiting for approval');
        retOfferPage = new PageReference('/' + offer.Id);
        retOfferPage.setRedirect(true);
    }  
    
     public PageReference backToProject() {
        PageReference retPage = new PageReference('/' + offer.RECO_Project__c);
        retPage.setRedirect(true);
        return retPage;
    }
    
    public PageReference sendForApproval(){
        /*List<Cost_Item__c> updCostItems = new List<Cost_Item__c>();
        for(Cost_Item__c c : [SELECT RecordTypeId FROM Cost_Item__c WHERE Invoice__c = :offer.Id AND RecordTypeId != :validatedRTId]) {
            c.RecordTypeId = pendingApprovalRTId;
            updCostItems.add(c);
        }
        update updCostItems;*/
        
        offer.Notify_for_approval__c = true;
        offer.Approval_status__c = 'Waiting for approval';
        offer.Notify_date__c = SYSTEM.Now();
        offer.Reason__c = null;
        update offer;
        return retOfferPage;
    }
    
    public PageReference Approve(){
        /*List<Cost_Item__c> updCostItems = new List<Cost_Item__c>();
        for(Cost_Item__c c : [SELECT RecordTypeId FROM Cost_Item__c WHERE Invoice__c = :offer.Id AND RecordTypeId = :pendingApprovalRTId]) {
            c.RecordTypeId = validatedRTId;
            updCostItems.add(c);
        }
        update updCostItems;*/
        
        offer.Approval_status__c = 'Approved';
        update offer;
        return retOfferPage;
    }    
    
    public PageReference Disapprove(){
      /*  List<Cost_Item__c> updCostItems = new List<Cost_Item__c>();
        for(Cost_Item__c c : [SELECT RecordTypeId FROM Cost_Item__c WHERE Invoice__c = :offer.Id AND RecordTypeId = :pendingApprovalRTId]) {
            c.RecordTypeId = rejectedRTId;
            updCostItems.add(c);
        }
        update updCostItems;*/
        
        offer.Approval_status__c = 'Disapproved';
        update offer;
        return retOfferPage;
    }      

}
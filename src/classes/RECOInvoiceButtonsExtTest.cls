/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RECOInvoiceButtonsExtTest {

   static testMethod void myUnitTest() {
    Test.startTest();
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id);
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        //rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
    rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Draft').getRecordTypeId();
        Offer__c offer = TestHelper.createOffer(rTypeId, bProj.Id);
        offer.Currency__c = 'CHF';
        offer.Supplier_Contact__c = suppCon.Id;
    offer.RECO_Project__c = bProj.Id;
        offer.Supplier__c = suppAcc.Id;
        insert offer;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        for(Cost_Item__c c : newCostItems){
          c.Currency__c = 'CHF';
          c.Offer__c = offer.Id;
        }
       // insert newCostItems; commented by Deepak for SOQL 101 issue during deployment. 
        
        
        PageReference pageRef = Page.RECOOfferButtons;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(offer);
        RECOOfferButtonsExt ext = new RECOOfferButtonsExt(con);
        
        //ApexPages.StandardController con = new ApexPages.StandardController(offer);
        //CostItemsController ext = new CostItemsController(con);
        
        Boolean tempBool = ext.hasAccessToQuotation;
        tempBool = ext.showButtons;
        tempBool = ext.isEditable;
        tempBool = ext.showSendDecision;
        tempBool = ext.noCostItemsSelected;
        tempBool = ext.noDocumentAttached;
        tempBool = ext.refusalReasonEntered;
        
        ext.sendForApproval();
        ext.Disapprove();
        ext.sendForApproval();
        ext.Approve();
        ext.backToProject();
        Test.stopTest();
    }
    /*static testMethod void myUnitTest1() {
    Test.startTest();
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id);
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
    //rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        Offer__c offer = TestHelper.createOffer(rTypeId, bProj.Id);
        offer.Currency__c = 'CHF';
        offer.Supplier_Contact__c = suppCon.Id;
        offer.Supplier__c = suppAcc.Id;
        insert offer;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        for(Cost_Item__c c : newCostItems){
          c.Currency__c = 'CHF';
          c.Offer__c = offer.Id;
        }
       // insert newCostItems; commented by Deepak for SOQL 101 issue during deployment. 
        
        Purchase_Order__c prRec = TestHelper.createPO(offer.Id,bProj.Id);
        insert prRec;
        
        
         
        Invoice__c inv = new Invoice__c();
        inv.Name='5';
        inv.Invoice_Date__c=Date.today();
        inv.Invoice_amount__c=100;
        inv.Currency__c='CHF';
        inv.Description__c='Test';
        inv.RECO_Project__c=bProj.Id;
        inv.Offer__c=offer.Id;
        inv.PO__c=prRec.Id;
        inv.Supplier__c=suppAcc.Id;
        insert inv;
        
        
        PageReference pageRef = Page.RECOOfferButtons;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(inv);
        RECOInvoiceButtonsExt ext = new RECOInvoiceButtonsExt(con);
        
        //ApexPages.StandardController con = new ApexPages.StandardController(offer);
        //CostItemsController ext = new CostItemsController(con);
        
        Boolean tempBool = ext.hasAccessToQuotation;
        tempBool = ext.showButtons;
        tempBool = ext.isEditable;
        tempBool = ext.showSendDecision;
        tempBool = ext.noCostItemsSelected;
        tempBool = ext.noDocumentAttached;
       // tempBool = ext.refusalReasonEntered;
        
        ext.sendForApproval();
        ext.Disapprove();
        ext.sendForApproval();
        ext.Approve();
        ext.backToProject();
        Test.stopTest();
    }*/
}
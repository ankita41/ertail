/*****
*   @Class      :   RECOOfferButtonsExtTest.cls
*   @Description:   Test coverage for the RECOOfferButtonsExt.cls
*   @Author     :   JACKY UY
*   @Created    :   25 AUG 2014
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*                         
*****/

@isTest
private class RECOOfferButtonsExtTest {
    static testMethod void myUnitTest() {
    Test.startTest();
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id);
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        //rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
    rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Draft').getRecordTypeId();
        Offer__c offer = TestHelper.createOffer(rTypeId, bProj.Id);
        offer.Currency__c = 'CHF';
        offer.Supplier_Contact__c = suppCon.Id;
    offer.RECO_Project__c = bProj.Id;
        offer.Supplier__c = suppAcc.Id;
        insert offer;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        for(Cost_Item__c c : newCostItems){
          c.Currency__c = 'CHF';
          c.Offer__c = offer.Id;
        }
       // insert newCostItems; commented by Deepak for SOQL 101 issue during deployment. 
        
        
        PageReference pageRef = Page.RECOOfferButtons;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(offer);
        RECOOfferButtonsExt ext = new RECOOfferButtonsExt(con);
        
        //ApexPages.StandardController con = new ApexPages.StandardController(offer);
        //CostItemsController ext = new CostItemsController(con);
        
        Boolean tempBool = ext.hasAccessToQuotation;
        tempBool = ext.showButtons;
        tempBool = ext.isEditable;
        tempBool = ext.showSendDecision;
        tempBool = ext.noCostItemsSelected;
        tempBool = ext.noDocumentAttached;
        tempBool = ext.refusalReasonEntered;
        
        ext.sendForApproval();
        ext.Disapprove();
        ext.sendForApproval();
        ext.Approve();
        ext.backToProject();
        Test.stopTest();
    }
    static testMethod void myUnitTest1() {
    Test.startTest();
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id);
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
    //rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Draft').getRecordTypeId();
        Offer__c offer = TestHelper.createOffer(rTypeId, bProj.Id);
        offer.Currency__c = 'CHF';
        offer.Supplier_Contact__c = suppCon.Id;
        offer.Supplier__c = suppAcc.Id;
        insert offer;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        for(Cost_Item__c c : newCostItems){
          c.Currency__c = 'CHF';
          c.Offer__c = offer.Id;
        }
       // insert newCostItems; commented by Deepak for SOQL 101 issue during deployment. 
        
        
        PageReference pageRef = Page.RECOOfferButtons;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(offer);
        RECOOfferButtonsExt ext = new RECOOfferButtonsExt(con);
        
        //ApexPages.StandardController con = new ApexPages.StandardController(offer);
        //CostItemsController ext = new CostItemsController(con);
        
        Boolean tempBool = ext.hasAccessToQuotation;
        tempBool = ext.showButtons;
        tempBool = ext.isEditable;
        tempBool = ext.showSendDecision;
        tempBool = ext.noCostItemsSelected;
        tempBool = ext.noDocumentAttached;
        tempBool = ext.refusalReasonEntered;
        
        ext.sendForApproval();
        ext.Disapprove();
        ext.sendForApproval();
        ext.Approve();
        ext.backToProject();
        Test.stopTest();
    }
}
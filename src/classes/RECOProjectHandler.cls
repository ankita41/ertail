/*****
*   @Class      :   RECOProjectHandler.cls
*   @Description:   Handles all RECO_Project__c recurring actions/events.
*   @Author     :   JACKY UY
*   @Created    :   14 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   JACKY UY        17 APR 2014     Fix: Changed the CurrencyConverter constructor to pass a Set of Local Currencies
*   JACKY UY        23 APR 2014     Update: Changed the CurrencyConverter constructor to pass the record's creation year
*   JACKY UY        28 APR 2014     Refactor: Removed unnecessary maps on copySellingSurfaceFromBoutiqueToProject()
*   JACKY UY        28 APR 2014     Refactor: Removed unnecessary lines on autoInsertStakeholderOnProjectCreation()
*   JACKY UY        29 APR 2014     Add: Created the autoCreateBudgetOverviewItems() method
*   JACKY UY        09 MAY 2014     Change: Referenced a static method for Currency Conversion
*   JACKY UY        17 JUN 2014     Change: copySellingSurfaceFromBoutiqueToProject to copy only Currency to BTQ Project
*   JACKY UY        19 JUN 2014     Fix: check if key exists on map on copySellingSurfaceFromBoutiqueToProject() method
*   JACKY UY        30 JUN 2014     Improved Currency Conversion method
*   JACKY UY        30 JUL 2014     Added method for the autocreation of Attachment Folders
*   JACKY UY        07 AUG 2014     Commented out CHF Conversion of Planned OP
*   JACKY UY        22 AUG 2014     Added Grant_Access__c & Notify__c fields on the autoInsertStakeholderOnProjectCreation() method
*   JACKY UY        16 SEP 2014     Added Currency Conversion for Total_Costs_CHF__c
*   JACKY UY        16 SEP 2014     If Project Record Type is Boutique Pop-up Relocation Project, 'Hidden from Reports' field should be TRUE
*   JACKY UY        16 SEP 2014     Changed ErrorOnMultipleOperativePopupProjects() method to check only one 'Not Cancelled' BTQ Pop-up Project per Boutique
*   Koteswararao    25 MArch 2015   Changed  capex amount based on year1 and year2       
***/

public class RECOProjectHandler{
    public static Boolean TriggerStopPopulateBoutiqueGeoLocation = false;
    public static void PopulateProjectFields(List<RECO_Project__c> newList){
        Set<Id> btqIds = new Set<Id>();
        for(RECO_Project__c prj : newList) {
            btqIds.add(prj.Boutique__c);
        }
    
        Map<Id,Boutique__c> mapBTQs = new Map<Id,Boutique__c>([
            SELECT boutique_location_account__r.Market__r.Currency__c FROM Boutique__c WHERE Id IN :btqIds
        ]);
        
        Id popRelocationRTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Pop-up Relocation Project').getRecordTypeId();
        
        for(RECO_Project__c prj : newList) {
            //POPULATE PROJECT CURRENCY
            if(mapBTQs.containsKey(prj.Boutique__c))
                prj.Currency__c = mapBTQs.get(prj.Boutique__c).boutique_location_account__r.Market__r.Currency__c;
                
            //IF RECORD TYPE = BTQ POP-UP RELOCATION, CHECK HIDDEN FROM REPORTS
            if(prj.RecordTypeId == popRelocationRTypeId)
                prj.Hidden_from_Reports__c = TRUE;
        }
    }
    
    public static void convertCapExOnProjectUpsert(List<RECO_Project__c> newList){
        Set<String> createdYears = new Set<String>();
        Set<String> localCurrencies = new Set<String>();
        Datetime projectDate1;
        Datetime projectDate2; 
       // Datetime creationDate;  
            
        for(RECO_Project__c obj: newList) {
        /*Currency update based on the year1 and year2 ---------koteswararao--start-*/
            system.debug('Cutoff_Year_1__c@@'+obj.Cutoff_Year_1__c+'Cutoff_Year_2__c@'+obj.Cutoff_Year_2__c);  
            if(obj.Cutoff_Year_1__c!= null && obj.Cutoff_Year_1__c!='')
                createdYears.add(obj.Cutoff_Year_1__c);

            if(obj.Cutoff_Year_2__c!= null && obj.Cutoff_Year_2__c!='')
                 createdYears.add(obj.Cutoff_Year_2__c);
          /*Currency update based on the year1 and year2 ---------koteswararao--end-*/
            localCurrencies.add(obj.Currency__c);
        }
        createdYears.add(String.valueOf(system.today().year()));
        CurrencyConverter.PopulateCurrencyConversion(createdYears, localCurrencies);
        for(RECO_Project__c obj : newList) {
            /*Currency update based on the year1 ---------koteswararao--start-*/        
            //creationDate = (obj.CreatedDate!=null) ? obj.CreatedDate : SYSTEM.Today();
            if(obj.Cutoff_Year_1__c!= null && obj.Cutoff_Year_1__c!='') 
            {
                projectDate1 = DateTime.newInstanceGMT(Integer.valueof(obj.Cutoff_Year_1__c), 1, 1, 1, 1, 1);
                obj.DF_CapEx_amount_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.DF_CapEx_amount_local__c), obj.Currency__c, 'CHF', projectDate1 );
                obj.CapEx_amount_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.CapEx_amount_local__c), obj.Currency__c, 'CHF', projectDate1 );
                obj.Extra_Capex_Y1_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Extra_Capex_Y1_local__c), obj.Currency__c, 'CHF', projectDate1 );
                obj.Cutoff_amount_CHF_year1__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Cutoff_amount_local_year1__c), obj.Currency__c, 'CHF', projectDate1 );
                obj.Total_Costs_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Total_Costs__c), obj.Currency__c, 'CHF', projectDate1 );
            }
            /*Currency update based on the year1 ---------koteswararao--end-*/            
            /*Currency update based on the year2 ---------koteswararao--Start-*/            
            if(obj.Cutoff_Year_2__c!= null && obj.Cutoff_Year_2__c!='') 
            {
                projectDate2 = DateTime.newInstanceGMT(Integer.valueof(obj.Cutoff_Year_2__c), 1, 1, 1, 1, 1);
                obj.CapEx_Amount_Y2_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.CapEx_Amount_Y2_local__c), obj.Currency__c, 'CHF', projectDate2);
                obj.Extra_Capex_Y2_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Extra_Capex_Y2_local__c), obj.Currency__c, 'CHF', projectDate2);
                //obj.Planned_OP_Y1_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Planned_OP_Y1_local__c), obj.Currency__c, 'CHF', creationDate);
                //obj.Planned_OP_Y2_CHF__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Planned_OP_Y2_local__c), obj.Currency__c, 'CHF', creationDate);
                //obj.Manual_Expected_Cost_Y1_local__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Manual_Expected_Cost_Y1_CHF__c), 'CHF', obj.Currency__c, creationDate);
                //obj.Manual_Expected_Cost_Y2_local__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Manual_Expected_Cost_Y2_CHF__c), 'CHF', obj.Currency__c, creationDate);
                obj.Cutoff_amount_CHF_year2__c = CurrencyConverter.convertCurrencyFromTo(double.valueOf(obj.Cutoff_amount_local_year2__c), obj.Currency__c, 'CHF', projectDate2);
            }
            /*Currency update based on the year2 ---------koteswararao--end-*/
            }
    }
    
    public static void AutoInsertStakeholderOnProjectCreation(List<RECO_Project__c> newList){
        Id rTypeId = Schema.sObjectType.Stakeholder__c.getRecordTypeInfosByName().get('Platform user').getRecordTypeId();
        List<Stakeholder__c> stakeholders = new List<Stakeholder__c>();
        
        for(RECO_Project__c prj : newList) {
            stakeholders.add(new Stakeholder__c(
                Name = UserInfo.getName(), 
                RECO_Project__c = prj.Id, 
                RecordTypeId = rTypeId, 
                Function__c = 'Retail Project Manager', 
                Platform_user__c = UserInfo.getUserId(),
                Grant_Access__c = true,
                Notify__c = true
            ));
        }
        insert stakeholders;
    }
    
    public static void autoAssignRPMOnProjectCreation(List<RECO_Project__c> newList){
        for(RECO_Project__c prj : newList) {
            prj.Retail_project_manager__c = UserInfo.getUserId();
        }
    }
    
    //CREATES THE BUDGET OVERVIEW ITEMS UPON BOUTIQUE PROJECT CREATION
    public static void AutoCreateBudgetOverviewItems(List<RECO_Project__c> newList){
        List<Budget_Overview__c> newBOs = new List<Budget_Overview__c>();
        Schema.DescribeFieldResult estItemLevel1Desc = Estimate_Item__c.Level_1__c.getDescribe();
        Id ncafeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('NCafe Project').getRecordTypeId();
        
        for(RECO_Project__c prj : newList) {
            //CREATES BUDGET ITEMS BASED ON THE Estimate_Item__c.Level_1__c Picklist Values
            for(Schema.PicklistEntry plEntry : estItemLevel1Desc.getPicklistValues()){
                if(plEntry.isActive()){
                	if(ncafeId!=prj.recordTypeId){
		                    if(plEntry.getLabel()!='Other furniture' && plEntry.getLabel()!='Equipments' && plEntry.getLabel()!='Millwork'){
		                    newBOs.add(new Budget_Overview__c(
		                        Unique_Id__c = prj.Id + '-' + plEntry.getLabel(),
		                        Boutique_Project__c = prj.Id,
		                        Budget_Item__c = plEntry.getLabel(),
		                        Estimate_local__c = 0, 
		                        Estimate_CHF__c = 0, 
		                        Confirmed_Cost_local__c = 0, 
		                        Confirmed_Cost_CHF__c = 0, 
		                        Expected_Cost_local__c = 0, 
		                        Expected_Cost_CHF__c = 0
		                    ));
		                }
                	}
                	if(ncafeId==prj.recordTypeId){
	                	if(plEntry.getLabel()=='Other furniture' || plEntry.getLabel()=='Equipments' || plEntry.getLabel()=='Millwork' || plEntry.getLabel()=='Fees' || plEntry.getLabel()=='Building Works'){
		                	newBOs.add(new Budget_Overview__c(
		                        Unique_Id__c = prj.Id + '-' + plEntry.getLabel(),
		                        Boutique_Project__c = prj.Id,
		                        Budget_Item__c = plEntry.getLabel(),
		                        Estimate_local__c = 0, 
		                        Estimate_CHF__c = 0, 
		                        Confirmed_Cost_local__c = 0, 
		                        Confirmed_Cost_CHF__c = 0, 
		                        Expected_Cost_local__c = 0, 
		                        Expected_Cost_CHF__c = 0
		                    ));
	                	}
                	}
                
             }
            }
            
            //CREATES THE 'TOTAL' BUDGET ITEM
            newBOs.add(new Budget_Overview__c(
                Unique_Id__c = prj.Id + '-Total',
                Boutique_Project__c = prj.Id,
                Budget_Item__c = 'Total',
                Estimate_local__c = 0, 
                Estimate_CHF__c = 0, 
                Confirmed_Cost_local__c = 0, 
                Confirmed_Cost_CHF__c = 0, 
                Expected_Cost_local__c = 0, 
                Expected_Cost_CHF__c = 0
            ));
        }
        upsert newBOs Unique_Id__c;
    }
    
    //CREATE ATTACHMENT FOLDERS ON BOUTIQUE PROJECTS
    //RECOProjectHandler.autoCreateAttachmentFolders([SELECT Id FROM RECO_Project__c WHERE Id NOT IN (SELECT RECO_Project__c FROM Attachment_Folder__c) ORDER BY Name LIMIT 500]);
    public static void AutoCreateAttachmentFolders(List<RECO_Project__c> newList){
        List<Attachment_Folder__c> newFolders = new List<Attachment_Folder__c>();
        Map<String, Custom_Folders__c> customFolders = Custom_Folders__c.getAll();
        
        for(RECO_Project__c prj : newList) {
            for(String folderName : customFolders.keySet()){
                if(customFolders.get(folderName).Parent_Object__c=='RECO_Project__c'){
                    newFolders.add(new Attachment_Folder__c(
                        RECO_Project__c = prj.Id,
                        Name = folderName,
                        Folder_Description_long__c = 'Currently Accessible By: Retail Project Manager, '
                    ));
                }
            }
        }
        insert newFolders;
    }
    
    //BLOCKS USERS TO CREATE MULTIPLE BTQ POP-UP PROJECTS WITH STATUS = OPERATIVE
    public static void ErrorOnMultipleOperativePopupProjects(List<RECO_Project__c> newList){
        Id popupRTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Pop-up Project').getRecordTypeId();
        Set<Id> btqIds = new Set<Id>();
        for(RECO_Project__c p : newList){
            if(!btqIds.contains(p.Boutique__c) && p.RecordTypeId==popupRTypeId && p.Status__c!='Cancelled')
                btqIds.add(p.Boutique__c);
            else if(btqIds.contains(p.Boutique__c) && p.RecordTypeId==popupRTypeId && p.Status__c!='Cancelled')
                p.addError('There can only be one BTQ Pop-up Project per Boutique. The other projects must be of Status \'Cancelled\' to save this record.');
        }
        
        if(!btqIds.isEmpty()){
            Set<Id> existingBtqIds = new Set<Id>();
            for(RECO_Project__c p : [SELECT Boutique__c FROM RECO_Project__c WHERE RecordTypeId = :popupRTypeId AND Id NOT IN :newList AND Boutique__c IN :btqIds AND Status__c != 'Cancelled']){
                existingBtqIds.add(p.Boutique__c);
            }
            
            for(RECO_Project__c p : newList){
                if(existingBtqIds.contains(p.Boutique__c) && p.RecordTypeId==popupRTypeId && p.Status__c!='Cancelled')
                    p.addError('There can only be one BTQ Pop-up Project per Boutique. The other projects must be of Status \'Cancelled\' to save this record.');
            }
        }
    }
    
    //UPDATE THE BOUTIQUE POP-UP STATUS ACCORDING TO THE PROJECT STATUS AND RELOCATION TYPE
    //POPULATES THE BOUTIQUE LOCATION OF THE BOUTIQUE POP-UP WHEN THE PROJECT IS COMPLETED WITH RELOCATION TYPE 'NEW LOCATION'
    public static void PopupRelocationUpdateOnBoutique(List<RECO_Project__c> newList){
        Id popupRelocationsRTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Pop-up Relocation Project').getRecordTypeId();
        Map<Id,RECO_Project__c> mapBtqPrjs = new Map<Id,RECO_Project__c>();
        for(RECO_Project__c p : [SELECT Name, Boutique__c, Status__c, Relocation_Type__c FROM RECO_Project__c 
                                    WHERE Id IN :newList AND RecordTypeId = :popupRelocationsRTypeId ORDER BY LastModifiedDate]){
            mapBtqPrjs.put(p.Boutique__c,p);
        }
        
        if(!mapBtqPrjs.isEmpty()){
            //CREATE BOUIQUE LOCATIONS
            Id accRTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
            List<Account> upsBtqLocations = new List<Account>();
            Set<String> btqPrjIds = new Set<String>();
            
            for(RECO_Project__c p : [SELECT Name, Boutique_Location_Street__c, Boutique_Location_City__c, Boutique_Location_State_Province__c, 
                                Boutique_Location_Zip_Postal_Code__c, Boutique_Location_Country__c, Boutique__r.boutique_location_account__r.Market__c 
                            FROM RECO_Project__c WHERE Id IN :mapBtqPrjs.values() AND Status__c='Completed' AND Relocation_Type__c='New Location']){
                upsBtqLocations.add(new Account(
                    RecordTypeId = accRTypeId,
                    Name = 'Pop-up Location: ' + p.Name,
                    BTQ_Pop_up_Location_Project_Id__c = p.Id,
                    BillingStreet = p.Boutique_Location_Street__c,
                    BillingCity = p.Boutique_Location_City__c,
                    BillingState = p.Boutique_Location_State_Province__c,
                    BillingPostalCode = p.Boutique_Location_Zip_Postal_Code__c,
                    BillingCountry = p.Boutique_Location_Country__c,
                    Market__c = p.Boutique__r.boutique_location_account__r.Market__c
                ));
                btqPrjIds.add(p.Id);
            }
            upsert upsBtqLocations BTQ_Pop_up_Location_Project_Id__c;
            
            Map<Id,Id> mapProjectLocation = new Map<Id,Id>();
            for(Account a : [SELECT Id, BTQ_Pop_up_Location_Project_Id__c FROM Account WHERE BTQ_Pop_up_Location_Project_Id__c IN :btqPrjIds]){
                mapProjectLocation.put(a.BTQ_Pop_up_Location_Project_Id__c,a.Id);
            }
            
            //UPDATE BOUTIQUE
            List<Boutique__c> updBtqs = new List<Boutique__c>();
            String tempStatus;
            
            for(Boutique__c b : [SELECT Status__c, Status_Since__c, boutique_location_account__c FROM Boutique__c 
                                    WHERE Id IN :mapBtqPrjs.keySet() AND boutique_location_account__c!=NULL]){
                tempStatus = b.Status__c;
                
                if(mapBtqPrjs.get(b.Id).Status__c=='Ongoing')
                    b.Status__c = 'Not Operative';
                else if(mapBtqPrjs.get(b.Id).Status__c=='Completed'){
                    if(mapBtqPrjs.get(b.Id).Relocation_Type__c=='Warehouse')
                        b.Status__c = 'Not Operative';
                    else if(mapBtqPrjs.get(b.Id).Relocation_Type__c=='New Location'){
                        b.Status__c = 'Operative';
                        if(mapProjectLocation.containsKey(mapBtqPrjs.get(b.Id).Id))
                            b.boutique_location_account__c = mapProjectLocation.get(mapBtqPrjs.get(b.Id).Id);
                    }
                }
                
                if(tempStatus!=b.Status__c)
                    b.Status_Since__c = SYSTEM.Today();
                
                updBtqs.add(b);
            }
            update updBtqs;
        }
    }
    
    
    @Future(callout=true)
    public static void PopulateBoutiqueGeoLocation(Set<Id> accIds){
        AccountHandler.TriggerStopPopulateBoutiqueGeoLocation = true;
        String address;
        RECO_Project__c tempPro;
        List<RECO_Project__c> updAccs = new List<RECO_Project__c>();
        
        for(RECO_Project__c a : [
            SELECT Boutique_Location_Street__c, Boutique_Location_State_Province__c, Boutique_Location_City__c,Geocoding_Zero_Results__c,Boutique_Location_Zip_Postal_Code__c,Boutique_Location_Country__c, Location__Latitude__s, Location__Longitude__s
            FROM RECO_Project__c WHERE Id IN :accIds LIMIT 5
        ]){
                            
            address = '';
            if(a.Boutique_Location_Street__c != null)
                address += a.Boutique_Location_Street__c + ', ';
            if(a.Boutique_Location_City__c != null)
                address += a.Boutique_Location_City__c + ', ';
            if(a.Boutique_Location_State_Province__c != null)
                address += a.Boutique_Location_State_Province__c + ' ';
            if(a.Boutique_Location_Zip_Postal_Code__c != null)
                address += a.Boutique_Location_Zip_Postal_Code__c +', ';
            if(a.Boutique_Location_Country__c != null)
                address += a.Boutique_Location_Country__c;
            
            if(address!=''){
                a.Location__Latitude__s = 0;
                a.Location__Longitude__s = 0;
                a.Geocoding_Zero_Results__c = true;
                tempPro = calloutGeoLocation(a, address);
                if(tempPro.Geocoding_Zero_Results__c && a.Boutique_Location_Country__c!=NULL){
                    tempPro = calloutGeoLocation(a, a.Boutique_Location_Country__c);
                    tempPro.Geocoding_Zero_Results__c = true;
                }
                updAccs.add(tempPro);
            }
        }
        update updAccs;
    }
    
    
     private static RECO_Project__c calloutGeoLocation(RECO_Project__c a, String address){
        String clientID = GoogleMapsSettings__c.getValues('Client ID').Value__c;
        String privateKey = GoogleMapsSettings__c.getValues('Crypto Key').Value__c.replace('-', '+').replace('_', '/');
        Blob privateKeyBlob = EncodingUtil.base64Decode(privateKey);
        
        String url, signature, txt;
        Blob urlBlob, signatureBlob; 
        Http h;
        HttpRequest req;
        HttpResponse res;
        Double lon, lat;
        
        address = EncodingUtil.urlEncode(address, 'UTF-8');
        url = '/maps/api/geocode/json?address=' + address + '&sensor=false&client=' + clientID;
        urlBlob = Blob.valueOf(url);
        signatureBlob = Crypto.generateMac('hmacSHA1', urlBlob, privateKeyBlob);
        signature = EncodingUtil.base64Encode(signatureBlob).replace('+', '-').replace('/', '_');
        
        //CALLOUT
        req = new HttpRequest();
        req.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false&client=' + clientID + '&signature=' + signature);
        req.setMethod('GET');
        req.setTimeout(60000);
        
        h = new Http();
        String resp='';
        if(!Test.isRunningTest()){
            res = h.send(req);
            resp=res.getBody();
        }
        else{
            resp='{"results" : [{"address_components" : [{"long_name" : "122001","short_name" : "122001","types" : [ "postal_code" ]},{"long_name" : "Gurgaon","short_name" : "Gurgaon","types" : [ "locality", "political" ]},{"long_name" : "Gurgaon","short_name" : "Gurgaon","types" : [ "administrative_area_level_2", "political" ]},{"long_name" : "Haryana","short_name" : "HR","types" : [ "administrative_area_level_1", "political" ]},{"long_name" : "India","short_name" : "IN","types" : [ "country", "political" ]}],"formatted_address" : "Gurgaon, Haryana 122001, India","geometry" : {"bounds" : {"northeast" : {"lat" : 28.4890902,"lng" : 77.0662683},"southwest" : {"lat" : 28.4014119,"lng" : 76.9598127}},"location" : {"lat" : 28.4554726,"lng" : 77.0219019},"location_type" : "APPROXIMATE","viewport" : {"northeast" : {"lat" : 28.4890902,"lng" : 77.0662683},"southwest" : {"lat" : 28.4014119,"lng" : 76.9598127}}},"partial_match" : true,"place_id" : "ChIJm523QqkZDTkR06eT1PrbRxM","types" : [ "postal_code" ]}],"status" : "OK"}';
        }
        
        //PARSE RESPONSE
        JSONParser parser = JSON.createParser(resp);
        lat = null;
        lon = null;
        while(parser.nextToken()!=null){
            if((parser.getCurrentToken()==JSONToken.FIELD_NAME) && (parser.getText()=='location')){
                parser.nextToken();
                while(parser.nextToken()!=JSONToken.END_OBJECT){
                    txt = parser.getText();
                    parser.nextToken();
                    if(txt=='lat')
                        lat = parser.getDoubleValue();
                    else if(txt=='lng')
                        lon = parser.getDoubleValue();
                }
            } 
            else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'status')){
                parser.nextToken();
                txt = parser.getText();
                if(txt == 'ZERO_RESULTS')
                    a.Geocoding_Zero_Results__c = true;
            }
        }
        
        //UPDATE COORDINATES
        if(lat!=null && lon!=null){
            a.Location__Latitude__s = lat;
            a.Location__Longitude__s = lon;
            a.Geocoding_Zero_Results__c = false;
        }
        
        return a;
    }
    
    
    
    //public class CustomException extends Exception {}
    //throw new CustomException('' + qryStr);
}
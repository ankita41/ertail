/*****
*   @Class      :   RECOProjectHandlerTest.cls
*   @Description:   Test coverage for the RECOProjectTriggers.trigger/RECOProjectHandler.cls
*   @Author     :   Jacky Uy
*   @Created    :   28 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        28 APR 2014     RECOProjectTriggers.trigger Test Coverage at 100%
*   Jacky Uy        28 APR 2014     RECOProjectHandler.cls Test Coverage at 100%
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
*
*****/

@isTest
private with sharing class RECOProjectHandlerTest{
    //TEST STANDARD BOUTIQUE PROJECT
    static testMethod void unitTest1(){
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.Net_selling_m2__c = 100;
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        List<RECO_Project__c> newBProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        for(RECO_Project__c prj : newBProjs){
            prj.Cutoff_amount_local_year1__c = 100;
            prj.Cutoff_amount_local_year2__c = 100;
            prj.CapEx_amount_local__c = 100; 
            prj.DF_CapEx_amount_local__c = 100;
        }
        insert newBProjs;
        
        List<RECO_Project__c> updBProjs = new List<RECO_Project__c>();
        for(RECO_Project__c prj : [
            SELECT Ref_surface_m2__c, Currency__c, Cutoff_amount_local_year1__c, Cutoff_amount_local_year2__c, CapEx_amount_local__c, DF_CapEx_amount_local__c,
                Cutoff_amount_CHF_year1__c, Cutoff_amount_CHF_year2__c, CapEx_amount_CHF__c, DF_CapEx_amount_CHF__c
            FROM RECO_Project__c WHERE Id IN :newBProjs
        ]){
            //TEST PopulateProjectFields() ON INSERT
            SYSTEM.Assert(prj.Currency__c == 'PHP');
            
            //TEST convertCapExOnProjectUpsert() ON INSERT
           /* SYSTEM.Assert(prj.Cutoff_amount_CHF_year1__c == (prj.Cutoff_amount_local_year1__c * 1.50));
            SYSTEM.Assert(prj.Cutoff_amount_CHF_year2__c == (prj.Cutoff_amount_local_year2__c * 1.50));
            SYSTEM.Assert(prj.CapEx_amount_CHF__c == (prj.CapEx_amount_local__c * 1.50));
            SYSTEM.Assert(prj.DF_CapEx_amount_CHF__c == (prj.DF_CapEx_amount_local__c * 1.50));*/
            
            prj.Cutoff_amount_local_year1__c = 200;
            prj.Cutoff_amount_local_year2__c = 200;
            prj.CapEx_amount_local__c = 200; 
            prj.DF_CapEx_amount_local__c = 200;
            updBProjs.add(prj);
        }
        
        //TEST autoInsertStakeholderOnProjectCreation() ON INSERT
        List<Stakeholder__c> stakeholders = [SELECT Id FROM Stakeholder__c WHERE RECO_Project__c IN :newBProjs];
        SYSTEM.Assert(stakeholders.size() == newBProjs.size());
        
        update updBProjs;
        for(RECO_Project__c prj : [
            SELECT Ref_surface_m2__c, Currency__c, Cutoff_amount_local_year1__c, Cutoff_amount_local_year2__c, CapEx_amount_local__c, DF_CapEx_amount_local__c,
                Cutoff_amount_CHF_year1__c, Cutoff_amount_CHF_year2__c, CapEx_amount_CHF__c, DF_CapEx_amount_CHF__c
            FROM RECO_Project__c WHERE Id IN :newBProjs
        ]){
            //TEST convertCapExOnProjectUpsert() ON UPDATE
          /*  SYSTEM.Assert(prj.Cutoff_amount_CHF_year1__c == (prj.Cutoff_amount_local_year1__c * 1.50));
            SYSTEM.Assert(prj.Cutoff_amount_CHF_year2__c == (prj.Cutoff_amount_local_year2__c * 1.50));
            SYSTEM.Assert(prj.CapEx_amount_CHF__c == (prj.CapEx_amount_local__c * 1.50));
            SYSTEM.Assert(prj.DF_CapEx_amount_CHF__c == (prj.DF_CapEx_amount_local__c * 1.50));*/
        }
    }
    
    //TEST BOUTIQUE POP-UP PROJECT
    static testMethod void unitTest2(){
        TestHelper.createCurrencies();
        TestHelper.createCustomFolders('RECO_Project__c');
        
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.Net_selling_m2__c = 100;
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Pop-up Project').getRecordTypeId();
        List<RECO_Project__c> newBProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        for(RECO_Project__c prj : newBProjs){
            prj.Status__c = 'Planned';
        }
        
        try{
            insert newBProjs;
        }
        catch(Exception e){
            //TEST ErrorOnMultipleOperativePopupProjects() ON INSERT
            SYSTEM.Assert(e.getMessage().contains('There can only be one BTQ Pop-up Project per Boutique.'));
        }
        
        for(RECO_Project__c prj : newBProjs){
            prj.Status__c = 'Cancelled';
        }
        newBProjs[0].Status__c = 'Planned';
        insert newBProjs;
        
        RECO_Project__c newBProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        newBProj.Status__c = 'Planned';
        
        try{
            insert newBProj;
        }
        catch(Exception e){
            //TEST ErrorOnMultipleOperativePopupProjects() ON INSERT
            SYSTEM.Assert(e.getMessage().contains('There can only be one BTQ Pop-up Project per Boutique.'));
        }
        
    }
}
/*****
*   @Class      :   RECOProjectSharingHandler.cls
*   @Description:   Handles all RECO_Project__c sharing actions/events.
*   @Author     :   Thibauld
*   @Created    :   30 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------  
*   Jacky Uy        03 SEP 2014     Added Sharing for Furniture Supplier (User)
*  
**/

public without sharing class RECOProjectSharingHandler{
    public static void ShareRECOProjectRecords(Map<Id, RECO_Project__c> projMap){
        List<RECO_Project__Share> pSharesToCreate = CreateSharingRecords(projMap);      
        System.debug('insert pSharesToCreate:' + pSharesToCreate);                                                      
        if (pSharesToCreate.size() > 0){
            //insert pSharesToCreate;
            Database.SaveResult[] lsr = Database.insert(pSharesToCreate,false);

            }
    }
    
    public static void RecalculateSharingAfterUpdate(Map<Id, RECO_Project__c> oldMap, Map<Id, RECO_Project__c> newMap){
        Map<Id, RECO_Project__c> projMap = new Map<Id, RECO_Project__c>();
        
        for(Id projId : newMap.keySet()){
            if ((oldMap.get(projId).Support_PO_Operator__c != newMap.get(projId).Support_PO_Operator__c)
                || (oldMap.get(projId).Support_National_Boutique_Manager__c != newMap.get(projId).Support_National_Boutique_Manager__c)
                || (oldMap.get(projId).Support_International_Retail_Operations__c != newMap.get(projId).Support_International_Retail_Operations__c)
                || (oldMap.get(projId).Other_Retail_Project_Manager__c != newMap.get(projId).Other_Retail_Project_Manager__c)
                || (oldMap.get(projId).Other_Manager_Architect__c != newMap.get(projId).Other_Manager_Architect__c)
                || (oldMap.get(projId).Other_Design_Architect__c != newMap.get(projId).Other_Design_Architect__c)
                || (oldMap.get(projId).Retail_project_manager__c != newMap.get(projId).Retail_project_manager__c)
                || (oldMap.get(projId).PO_operator__c != newMap.get(projId).PO_operator__c)
                || (oldMap.get(projId).National_boutique_manager__c != newMap.get(projId).National_boutique_manager__c)
                || (oldMap.get(projId).International_Retail_Operator__c != newMap.get(projId).International_Retail_Operator__c)
                || (oldMap.get(projId).Manager_architect__c != newMap.get(projId).Manager_architect__c)
                || (oldMap.get(projId).Design_architect__c != newMap.get(projId).Design_architect__c)
                || (oldMap.get(projId).Furniture_Supplier_User__c != newMap.get(projId).Furniture_Supplier_User__c))
                projMap.put(projId, newMap.get(projId));
        }
        System.debug('projMap:' + projMap);
        if (projMap.size() > 0){
            DeleteExistingSharing(projMap.keySet());
            ShareRECOProjectRecords(projMap);
        }
    }
    
    public static void DeleteExistingSharing(Set<Id> projIdSet){
        // Locate all existing sharing records for the project records in the batch.
        // Only records using an Apex sharing reason for this app should be returned. 
        List<RECO_Project__Share> oldProjectShrs = [SELECT Id FROM RECO_Project__Share WHERE ParentId IN :projIdSet AND 
             RowCause in (:Schema.RECO_Project__Share.rowCause.IsRetailProjectManager__c 
             ,:Schema.RECO_Project__Share.rowCause.IsNationalBoutiqueManager__c
             ,:Schema.RECO_Project__Share.rowCause.IsInternationalRetailOperations__c
             ,:Schema.RECO_Project__Share.rowCause.IsManagerArchitect__c
             ,:Schema.RECO_Project__Share.rowCause.IsDesignArchitect__c
             ,:Schema.RECO_Project__Share.rowCause.IsPOOperator__c 
             ,:Schema.RECO_Project__Share.rowCause.IsFurnitureSupplier__c)]; 
        
        System.debug('delete oldProjectShrs:' + oldProjectShrs);
        
        // Delete the existing sharing records.
       // This allows new sharing records to be written from scratch.
        Delete oldProjectShrs;
        
    }
    
    public static List<RECO_Project__Share> CreateSharingRecords(Map<Id, RECO_Project__c> projectMap){
        List<RECO_Project__Share> newprojectShrs = new List<RECO_Project__Share>();
        // Construct new sharing records  
        for(RECO_Project__c proj : projectMap.values()){
            if (proj.Retail_project_manager__c != null){
                newprojectShrs.add(CreateRetailProjectManagerSharing(proj.Retail_project_manager__c, proj.Id));
            }
            if (proj.Other_Retail_Project_Manager__c != null){
                newprojectShrs.add(CreateRetailProjectManagerSharing(proj.Other_Retail_Project_Manager__c, proj.Id));
            }
            if (proj.National_boutique_manager__c != null){
                newprojectShrs.add(CreateNationalBoutiqueManagerSharing(proj.National_boutique_manager__c, proj.Id));
            }
            if (proj.Support_National_Boutique_Manager__c != null){
                newprojectShrs.add(CreateNationalBoutiqueManagerSharing(proj.Support_National_Boutique_Manager__c, proj.Id));
            }
            if (proj.PO_operator__c != null){
                newprojectShrs.add(CreatePOOperatorSharing(proj.PO_operator__c, proj.Id));
            }
            if (proj.Support_PO_Operator__c != null){
                newprojectShrs.add(CreatePOOperatorSharing(proj.Support_PO_Operator__c, proj.Id));
            }
            if (proj.International_Retail_Operator__c != null){
                newprojectShrs.add(CreateInternationalRetailOperationsSharing(proj.International_Retail_Operator__c, proj.Id));
            }
            if (proj.Support_International_Retail_Operations__c != null){
                newprojectShrs.add(CreateInternationalRetailOperationsSharing(proj.Support_International_Retail_Operations__c, proj.Id));
            }
            if (proj.Manager_architect__c != null){
                newprojectShrs.add(CreateManagerArchitectSharing(proj.Manager_architect__c, proj.Id));
            }
            if (proj.Other_Manager_Architect__c != null){
                newprojectShrs.add(CreateManagerArchitectSharing(proj.Other_Manager_Architect__c, proj.Id));
            }
            if (proj.Design_architect__c != null){
                newprojectShrs.add(CreateDesignArchitectSharing(proj.Design_architect__c, proj.Id));
            }
            if (proj.Other_Design_Architect__c != null){
                newprojectShrs.add(CreateDesignArchitectSharing(proj.Other_Design_Architect__c, proj.Id));
            }
            if (proj.Furniture_Supplier_User__c != null){
                newprojectShrs.add(CreateFurnitureSharing(proj.Furniture_Supplier_User__c, proj.Id));
            }
        }
        return newprojectShrs;
    }
    
    private static RECO_Project__Share CreateFurnitureSharing(Id userId, Id projId){
        return new RECO_Project__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Read'
                                        , ParentId = projId
                                        , RowCause = Schema.RECO_Project__Share.RowCause.IsFurnitureSupplier__c);
    }
    
    private static RECO_Project__Share CreateRetailProjectManagerSharing(Id userId, Id projId){
        return new RECO_Project__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = projId
                                        , RowCause = Schema.RECO_Project__Share.RowCause.IsRetailProjectManager__c);
    }

    private static RECO_Project__Share CreateNationalBoutiqueManagerSharing(Id userId, Id projId){
        return new RECO_Project__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = projId
                                        , RowCause = Schema.RECO_Project__Share.RowCause.IsNationalBoutiqueManager__c);
    }
    
    private static RECO_Project__Share CreateInternationalRetailOperationsSharing(Id userId, Id projId){
        return new RECO_Project__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = projId
                                        , RowCause = Schema.RECO_Project__Share.RowCause.IsInternationalRetailOperations__c);
    }

    private static RECO_Project__Share CreateManagerArchitectSharing(Id userId, Id projId){
        return new RECO_Project__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = projId
                                        , RowCause = Schema.RECO_Project__Share.RowCause.IsManagerArchitect__c);
    }
    
    private static RECO_Project__Share CreateDesignArchitectSharing(Id userId, Id projId){
        return new RECO_Project__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = projId
                                        , RowCause = Schema.RECO_Project__Share.RowCause.IsDesignArchitect__c);
    }
    
    private static RECO_Project__Share CreatePOOperatorSharing(Id userId, Id projId){
        return new RECO_Project__Share(UserOrGroupId = userId
                                        , AccessLevel = 'Edit'
                                        , ParentId = projId
                                        , RowCause = Schema.RECO_Project__Share.RowCause.IsPOOperator__c);
    }
}
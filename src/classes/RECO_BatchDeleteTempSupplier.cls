global class RECO_BatchDeleteTempSupplier implements Database.Batchable<sObject>{
	   
	   
	 // Start Method
       global Database.QueryLocator start(Database.BatchableContext BC){
       		String recordTypeId=Schema.SObjectType.Account.RecordTypeInfosByName.get('Supplier Temp').RecordTypeId;
        	String status='No Action';
        	return Database.getQueryLocator('Select id ,status__c from account where recordTypeId=:recordTypeId and status__c=:status and Days_from_Created_Date__c>10 ');
        }
      
      // Execute Logic
       global void execute(Database.BatchableContext BC, List<account>scope){
              // Logic to be Executed batch wise      
              system.debug('========scope===='+scope);
              delete scope;
       }
      
       global void finish(Database.BatchableContext BC){
            // Logic to be Executed at finish
       }

}
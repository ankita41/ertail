global class RECO_BatchDeleteTempSupplierScheduler implements Schedulable {

    global void execute(SchedulableContext SC) {

      RECO_BatchDeleteTempSupplier M = new RECO_BatchDeleteTempSupplier();
            database.executebatch(M);
      
   }
    
}
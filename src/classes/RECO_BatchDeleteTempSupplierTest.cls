/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RECO_BatchDeleteTempSupplierTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        
        Id rTypeIdSup = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier Temp').getRecordTypeId();
        Account accSupp = TestHelper.createAccount(rTypeIdSup, market.Id);
        insert accSupp;
        accSupp.Status__c='No Action';
        update accSupp;
        
        
        Test.StartTest();
        RECO_BatchDeleteTempSupplier testsche = new RECO_BatchDeleteTempSupplier();
        Database.executebatch(testsche);
        
        
        
       
            RECO_BatchDeleteTempSupplierScheduler sh1 = new RECO_BatchDeleteTempSupplierScheduler();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest(); 
    
}

}
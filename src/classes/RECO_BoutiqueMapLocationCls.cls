public with sharing class RECO_BoutiqueMapLocationCls {
    public Boutique__c boutRec {get; set;}
    public String address {get; set;}
    public RECO_BoutiqueMapLocationCls(ApexPages.StandardController stdController) {
         boutRec = (Boutique__c)stdController.getRecord();  
         boutRec =[Select id,boutique_location_account__c from Boutique__c where id=:boutRec.id];      
    }
    
    public List<LocationWrapper> locationList {
        get{
            Map<Id,Account> mapLocations = new Map<Id,Account>([
                SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Location__Latitude__s, Location__Longitude__s
                FROM Account 
                WHERE RecordType.Name = 'Boutique Location' 
                AND Id IN (SELECT boutique_location_account__c FROM Boutique__c WHERE boutique_location_account__c!=NULL) 
                AND Geocoding_Zero_Results__c=FALSE
            ]);
            
            system.debug('======size============='+mapLocations.size());
            
            
            Account accBouLocation=[SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, 
                                               Location__Latitude__s, Location__Longitude__s
                                               FROM Account where id=:boutRec.boutique_location_account__c];
            
            Set<Id> recIds=new Set<Id>();
            Id relocationProject=Schema.SObjectType.RECO_Project__c.RecordTypeInfosByName.get('Boutique Pop-up Relocation Project').RecordTypeId;
            Id relocationBouPopUp=Schema.SObjectType.RECO_Project__c.RecordTypeInfosByName.get('Boutique Pop-up Project').RecordTypeId;
            recIds.add(relocationBouPopUp);
            recIds.add(relocationProject);


 
            Account location;
            String tmpAddress = '';
            List<LocationWrapper> tmpLocationList = new List<LocationWrapper>();
            Set<Id> recTypeId=new Set<Id>();
            Id btqLocRTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Pop-up Relocation Project').getRecordTypeId();
            recTypeId.add(btqLocRTypeId );
            List<RECO_Project__c> blocHisList=[Select id,status__c,Boutique_Location_City__c,Boutique_Location_Country__c,Boutique_Location_Zip_Postal_Code__c,Boutique_Location_State_Province__c,Boutique_Location_Street__c,
                                                                      Location__Latitude__s,Location__Longitude__s,Boutique__c from RECO_Project__c
                                                                      where Boutique__c=:boutRec.Id and recordTypeId =: recIds];
            
            system.debug('===========blocHisList========='+blocHisList.size());
            
            tmpAddress = (accBouLocation.BillingStreet != null ? (accBouLocation.BillingStreet + '<br/>') : '') + 
                            (accBouLocation.BillingCity!= null ? (accBouLocation.BillingCity + ', ') : '') + 
                            (accBouLocation.BillingPostalCode!= null ? (accBouLocation.BillingPostalCode + '') : '') + '<br/>' + 
                            (accBouLocation.BillingCountry!= null ? accBouLocation.BillingCountry : '');
                            
            tmpLocationList.add(new LocationWrapper(tmpAddress, double.valueOf(accBouLocation.Location__Latitude__s), double.valueOf(accBouLocation.Location__Longitude__s), '', 'www.google.com',accBouLocation.Id,'','true',''));
            
            //if(blocHisList.size()>1){
                List<Account> tempStrList=new List<Account>();
                String tempLatest='false';
                for(RECO_Project__c locationRec : blocHisList){
                       /* if(tempStrList.isEmpty()){
                            tempStrList.add(accBouLocation);
                            tempLatest='true';
    
                            tmpAddress = (accBouLocation.BillingStreet != null ? (accBouLocation.BillingStreet + '<br/>') : '') + 
                                (accBouLocation.BillingCity!= null ? (accBouLocation.BillingCity + ', ') : '') + 
                                (accBouLocation.BillingPostalCode!= null ? (accBouLocation.BillingPostalCode + '') : '') + '<br/>' + 
                                (accBouLocation.BillingCountry!= null ? accBouLocation.BillingCountry : '');
                                
                                tmpLocationList.add(new LocationWrapper(tmpAddress, double.valueOf(accBouLocation.Location__Latitude__s), double.valueOf(accBouLocation.Location__Longitude__s), '', 'www.google.com',accBouLocation.Id,'',tempLatest));
                        }
                        else{*/
                        
                        if(accBouLocation.BillingStreet!=locationRec.Boutique_Location_Street__c ||
                                            accBouLocation.BillingCity!=locationRec.Boutique_Location_City__c ||
                                            accBouLocation.BillingPostalCode!=locationRec.Boutique_Location_Zip_Postal_Code__c ||
                                            accBouLocation.BillingCountry!=locationRec.Boutique_Location_Country__c){
                            tempLatest='false';
                            tmpAddress = (locationRec.Boutique_Location_Street__c != null ? (locationRec.Boutique_Location_Street__c + '<br/>') : '') + 
                                (locationRec.Boutique_Location_City__c != null ? (locationRec.Boutique_Location_City__c + ', ') : '') + 
                                (locationRec.Boutique_Location_Zip_Postal_Code__c != null ? (locationRec.Boutique_Location_Zip_Postal_Code__c + '') : '') + '<br/>' + 
                                (locationRec.Boutique_Location_Country__c != null ? locationRec.Boutique_Location_Country__c : '');
                                
                                tmpLocationList.add(new LocationWrapper(tmpAddress, double.valueOf(locationRec.Location__Latitude__s), double.valueOf(locationRec.Location__Longitude__s), '', 'www.google.com',locationRec.Id,'',tempLatest,locationRec.status__c));
                            }
                       // }
                        
        
                    
                }
            //}
            
            return tmpLocationList;
        } set;
    }
    
    
    
    public String locationData {
        get{
            String tmpString = '[';
            for(LocationWrapper lw : locationList) {
                tmpString += (tmpString == '[' ? '' : ', ') + '[\'' + StaticFunctions.jsencode(lw.name) + '\',' + lw.locationLat + ',' + lw.locationLong + ',\'' + StaticFunctions.jsencode(lw.address) + '\',\'' + StaticFunctions.jsencode(lw.imageURL) + '\',\'' + StaticFunctions.jsencode(lw.recordId) + '\',\'' + StaticFunctions.jsencode(lw.typology) + '\',\'' + StaticFunctions.jsencode(lw.latestRec) + '\',\'' + StaticFunctions.jsencode(lw.status)+'\']';
            }
            tmpString += ']';
            return tmpString;
        } set;
    }
    
    public class LocationWrapper {
        public String address {get; set;}
        public String name {get; set;}
        public String imageURL {get; set;}
        public Double locationLat {get; set;}
        public Double locationLong {get; set;}
        public Id recordId {get; set;}
        public String typology{get;set;}
        public String latestRec{get;set;}
        public String status{get; set;}
       
        
        public LocationWrapper(String address, Double locationLat, Double locationLong, String name, String imageURL, Id recordId,String typology,String latestRec,String status) {
            this.address = address;
            this.name = name;
            this.imageURL = imageURL;
            this.locationLat = locationLat;
            this.locationLong = locationLong; 
            this.recordId = recordId;
            this.typology=typology;
            this.latestRec=latestRec;
            this.status=status;
            
        }
    }

}
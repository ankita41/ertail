/** @Class      :   RECO_CreateBoutiqueProjectTest
*   @Description:   Test Class for RECO_CreateBoutiqueProject.trigger
*   @Author     :   Himanshu Palerwal
*   @Created    :   12 Jan 2016
*

*                         
*****/
@isTest(seeAllData=true)
private class RECO_CreateBoutiqueProjectTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Country_Mapping__c cou=new Country_Mapping__c();
        cou.name='USA';
        cou.Short_Form__c='US';
        insert cou;
        
        /*GoogleMapsSettings__c gSett=new GoogleMapsSettings__c();
        gSett.name='Client ID';
        gSett.Value__c='gme-nestlenespressosa';
        //insert gSett;
        
        GoogleMapsSettings__c gSett1=new GoogleMapsSettings__c();
        gSett1.name='Crypto Key';
        gSett1.Value__c='FePuq8NIxp3L1_VAHPNwoNLKbQs=';
        insert gSett1;*/
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        acc.BillingStreet='Test street';
        acc.billingCountry='United States';
        acc.billingCity='NJ';
        acc.billingState='Test State';
        acc.BillingPostalCode='34343';
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.boutique_location_account__c=acc.id;
        
        btq.Net_selling_m2__c = 100;
        insert btq;
                
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        List<RECO_Project__c> newBProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        for(RECO_Project__c prj : newBProjs){
            prj.Cutoff_amount_local_year1__c = 100;
            prj.Cutoff_amount_local_year2__c = 100;
            prj.CapEx_amount_local__c = 100; 
            prj.DF_CapEx_amount_local__c = 100;
        }
        insert newBProjs;
        
        RTCOMMarket__c erMarket = new RTCOMMarket__c();
        erMarket.name = 'Test Ertail Market';
        erMarket.RECOMarket__c=market.id;
        insert erMarket;
        
        RTCOMBoutique__c erBou = new RTCOMBoutique__c();
        erBou.name='Test Ertail Boutique';
        erBou.RTCOMMarket__c=erMarket.id;
        erBou.RECOBoutique__c=btq.id;
        insert erBou;
        
        Test.startTest();
         
        btq.status__c='Closed';
        update btq;
        btq.status__c='Opened';
        update btq;
        delete btq;
        Test.stopTest();
       
    }
}
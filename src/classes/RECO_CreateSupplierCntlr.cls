/** @Class      :   RECO_CreateSupplierCntlr
*   @Description:   Creater supplier using Force.com site
*   @Author     :   Himanshu Palerwal
*   @Created    :   15 Jan 2015
*   @Updated    :   Priya : Added Code for Ncafe supplier creation form

*                         
*****/

public with sharing class RECO_CreateSupplierCntlr {
public list<contact> contactList {get;set;}
public string companyName{get;set;}
public string mainPhone{get;set;}
public string fax{get;set;}
public string websiteURL{get;set;}
public string vatNo{get;set;}
public string description{get;set;}
public string streetNo{get;set;}
public string city{get;set;}
public string country{get;set;}
public string postalCode{get;set;}
public boolean showSection{get;set;}
public boolean InfoSent{get;set;}

List<Contact> insertConList = new List<Contact>();
String userId{get;set;}
public boolean reco{get;set;}
public boolean Ncafe{get;set;}

public RECO_CreateSupplierCntlr()
{
    userId=ApexPages.currentPage().getParameters().get('userId');
    if(String.isNotBlank(userId)){
        User userRec=[Select Id,Name,PRofile.Name,Email from User where Id=:userId];
        if(userRec.Profile.Name.contains('NCAFE')){
            reco=false;
            ncafe=true;
        }
        else{
            reco=true;
            ncafe=false;
        }
    }
    
    showSection=true;
    InfoSent=false;
    contactList = new list<contact>();
    contactList.add(new contact());
}

public void addContact() 
{
    contactList.add(new contact());
}

public Boolean validateEmail(String email) {
    Boolean res = true;
    String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
    Pattern MyPattern = Pattern.compile(emailRegex);
    Matcher MyMatcher = MyPattern.matcher(email);

    if (!MyMatcher.matches()) 
        res = false;
    return res; 
}


public pagereference insertSupplierAccount()
{
    
    Account acc;
   Boolean error=false;
   if(String.isNotBlank(companyName) && String.isNotBlank(streetNo) && String.isNotBlank(city) && String.isNotBlank(city) && String.isNotBlank(postalCode)){
        acc=new Account();
        acc.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier Temp').getRecordTypeId();
        acc.name=companyName;
        acc.phone=mainPhone;
        acc.fax=fax;
        acc.website=websiteURL;
        acc.billingstreet=streetNo;
        acc.billingcity=city;
        acc.billingCountry=country;
        acc.billingPostalCode=postalCode;
        insert acc;
    }
    else{
        error=true;
    }
    
    for(Contact con : contactList)
    {
        if(error==false && String.isNotBlank(con.Lastname) && String.isNotBlank(con.email) 
                        && String.isNotBlank(con.firstname)  && String.isNotBlank(con.phone))
        {
            if((validateEmail(con.email)==true)){
                Contact conInsert=new Contact();
                conInsert.accountId=acc.id;
                conInsert.firstname=con.firstname;
                conInsert.lastname=con.Lastname;
                conInsert.email=con.email;
                conInsert.phone=con.phone;
                insertConList.add(conInsert);
            }
            else{
                 ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter email in valid format!');
                ApexPages.addMessage(myMsg); 
                return null;
            }
        }
        else{
            ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'Please fill required fields!');
            ApexPages.addMessage(myMsg); 
            return null;
        }
    }
    
    if(!insertConList.isEmpty()){
        insert insertConList;
    }
    
    String userName = UserInfo.getUserName();
    User activeUser = [Select Email From User where Username = : userName limit 1];
    String userEmail = activeUser.Email;
    User userRec=[Select Id,Name,Email,Profile.Name from User where Id=:userId];
    EmailTemplate et;
    if(userRec.Profile.Name.contains('NCAFE')) {
         et = [SELECT Id, HtmlValue,BrandTemplateId, Subject FROM EmailTemplate WHERE DeveloperName = :'NCAFE_confirmationtoRPM'];
    } 
    else {
        et = [SELECT Id, HtmlValue,BrandTemplateId, Subject FROM EmailTemplate WHERE DeveloperName = :'RECO_confirmationtoRPM'];
    }    
    //
    String insURL=System.Label.Site_Label; 
    /** Fetching Header of template starts here****/   
                
        BrandTemplate brand_value=[Select value from BrandTemplate  where id=:et.BrandTemplateId];
        String bHeader,bFooter,brandvalue=brand_value.value;
        integer i=brandvalue.indexOf('<img');
        integer j=brandvalue.indexOf('</img>');
        bHeader=brandvalue.subString(i,j+6);
        bHeader= bHeader.replace('">','" src=');
        if(bHeader.contains('https')){
            bHeader=bHeader.replace('<![CDATA[','\"');
        }
        else{
            bHeader=bHeader.replace('<![CDATA[','\"'+insUrl);
        }
        bHeader=bHeader.replace(']]','\" ');
    /** Fetching Header of template ends here****/ 
    
    Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
    String htmlBody = et.HtmlValue;
    if (htmlBody != null) {
            htmlBody = htmlBody.replace('{!User.Name}', userRec.Name); 
            htmlBody = htmlBody.replace('{!Account.Name}', companyName); 
            htmlBody = htmlBody.replace('{!Account.Phone}', mainPhone);   
            htmlBody = htmlBody.replace('{!Contact.Name}', contactList[0].firstname);
            htmlBody = htmlBody.replace('{!Contact.Email}', contactList[0].email);
            htmlBody = htmlBody.replace('{!Link}', '</b> <a href="'+insUrl+'/'+acc.Id+'">'+'Click here </a> ');
            htmlBody = htmlBody.replace(']]>', '');
    }
     bHeader='<style>p{margin-top:0px; margin-bottom:0px;}</style><body class="setupTab"  style=" background-color:#CCCCCC; bEditID:bHeaderst1; bLabel:body;"><center ><table cellpadding="0" width="500" cellspacing="0" id="topTable" height="450" ><tr valign="top" ><td  style=" background-color:#FFFFFF; bEditID:r1st1; bLabel:header; vertical-align:top; height:100; text-align:left;">'+bHeader+'</td></tr><tr valign="top" ><td  style=" background-color:#330000; bEditID:r2st1; bLabel:accent1; height:1;"></td></tr>';
                 bHeader+='<tr valign="top" ><td styleInsert="1" height="300"  style=" background-color:#FFFFFF; bEditID:r3st1; color:#000000; bLabel:main; font-size:12pt; font-family:arial;">';
                 bFooter='</td></tr><tr valign="top" ><td  style=" background-color:#330000; bEditID:r4st1; bLabel:accent2; height:1;"></td></tr><tr valign="top" ><td  style=" background-color:#FFFFFF; bEditID:r5st1; bLabel:footer; vertical-align:top; height:100; text-align:left;"></td></tr><tr valign="top" ><td  style=" background-color:#330000; bEditID:r6st1; bLabel:accent3; height:1;"></td></tr></table></center><br><br>';
                 htmlBody=bHeader+htmlBody+bFooter;
    htmlBody=htmlBody.replace('<![CDATA[','');
    
    
    // Use Organization Wide Address  
    List<OrgWideEmailAddress> owa = [select id, Address,DisplayName from OrgWideEmailAddress where address=:userRec.email];
    if(!owa.isEmpty()) {
         mail.setOrgWideEmailAddressId(owa[0].id);
    } 
    else{
         List<OrgWideEmailAddress> owaHi = [select id, Address,DisplayName from OrgWideEmailAddress where address=:'retail@nespresso.com'] ;
         mail.setOrgWideEmailAddressId(owaHi[0].id);
    }
    
    mail.setHtmlBody(htmlBody);
    mail.setToAddresses(new list<string>{userRec.email});
    mail.setSubject(et.Subject);
    //mail.setSenderDisplayName('Supplier Registration');
    Messaging.sendEmail(new Messaging.Singleemailmessage[] { mail});
    
    EmailTemplate etSupplierConf;
    if(userRec.Profile.Name.contains('NCAFE')) {
            etSupplierConf = [SELECT Id, HtmlValue, Subject FROM EmailTemplate WHERE DeveloperName = :'NCAFE_supplierconfirmation'];
    } 
    else {
            etSupplierConf = [SELECT Id, HtmlValue, Subject FROM EmailTemplate WHERE DeveloperName = :'RECO_supplierconfirmation'];
        }       
    Messaging.Singleemailmessage mailSupplier = new Messaging.Singleemailmessage();
    String htmlBodySupp = etSupplierConf.HtmlValue;
    if (htmlBodySupp!= null) {
            htmlBodySupp = htmlBodySupp.replace('{!Account.Name}', companyName); 
            htmlBodySupp = htmlBodySupp.replace('{!Account.Phone__c}', mainPhone);   
            htmlBodySupp = htmlBodySupp.replace('{!Contact.Name}', contactList[0].firstname);
            htmlBodySupp = htmlBodySupp.replace('{!Contact.Email}', contactList[0].email);
            htmlBodySupp = htmlBodySupp.replace(']]>', '');
            
    }
    htmlBodySupp =bHeader+htmlBodySupp+bFooter;
    htmlBodySupp=htmlBodySupp.replace('<![CDATA[','');
    
    
    // Use Organization Wide Address  
    //List<OrgWideEmailAddress> owa = [select id, Address,DisplayName from OrgWideEmailAddress where address=:userEmail];
    if(!owa.isEmpty()) {
         mailSupplier.setOrgWideEmailAddressId(owa[0].id);
    } 
    else{
         List<OrgWideEmailAddress> owaHi = [select id, Address,DisplayName from OrgWideEmailAddress where address=:'retail@nespresso.com'] ;
         mailSupplier.setOrgWideEmailAddressId(owaHi[0].id);
    }
    
    mailSupplier.setHtmlBody(htmlBodySupp);
    mailSupplier.setToAddresses(new list<string>{contactList[0].email});
    mailSupplier.setSubject(etSupplierConf.Subject);
    //mailSupplier.setSenderDisplayName('Supplier Registration');
    Messaging.sendEmail(new Messaging.Singleemailmessage[] { mailSupplier});
    showSection=false;
    InfoSent=true;
    return null;
    
}
  public void removeContact(){
      if(contactList.size()>1){
          contactList.remove(contactList.size()-1);          
      }
  }   

}
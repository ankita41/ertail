public with sharing class RECO_FeedItemHelper {
    
    public static void assignFolderPermission(Id mainFolderId,Id FeedId)
    {
        Id selectedFolder;
        List<Attachment_Folder__c> mainFolder = [SELECT Name,Handover__c, Offer__c,Offer__r.RECO_Project__c,
                                            Offer__r.Approval_status__c,Offer__r.RECO_Project__r.Other_Retail_Project_Manager__c,
                                            Offer__r.RECO_Project__r.Retail_Project_Manager__c, Offer__r.EmailsenttoRPMforapprovedofferchkbox__c,
                                            Offer__r.RECO_Project__r.Support_National_boutique_manager__c,
                                            Offer__r.RECO_Project__r.National_boutique_manager__c,Offer__r.RECO_Project__r.PO_operator__c, 
                                            Offer__r.RECO_Project__r.PO_operator__r.Name, Offer__r.RECO_Project__r.Support_PO_Operator__c, 
                                            Offer__r.RECO_Project__r.Support_PO_Operator__r.Name FROM Attachment_Folder__c WHERE Id = :mainFolderId];
        
        if(!mainFolder.isEmpty()){
            FeedItem newFeedcontentforPrj= new FeedItem();
            selectedFolder=mainFolder[0].Id;  
            system.debug('=======selectedFolder=========='+selectedFolder);
            system.debug('=======mainFolder.Id=========='+mainFolder[0].Id);
            system.debug('=======mainFolder.Offer__c=========='+mainFolder[0].Offer__c);
            FeedItem newFeedContent;
                if(selectedFolder==mainFolder[0].Id ){
                    shareFileToStakeholders(FeedId,mainFolder[0].Id,selectedFolder);
                    system.debug('==========inside====='); 
                }
    
            /**** PRIYA ? 23 Feb 2016 - Code added for Req # 117 (R2) ****/
            if(mainfolder[0].name=='Budget & Costs')
            {
        
                if(mainFolder[0].Offer__r.RECO_Project__c!=null){
                   FeedItem preFeed = [SELECT ContentData, ContentFileName, ParentId FROM FeedItem WHERE Id=:FeedId];
                   newFeedContent=null;
                    newFeedcontentforPrj.ContentFileName=preFeed.ContentFileName;
                    newFeedcontentforPrj.ContentData=preFeed.ContentData;
                    
                    List<Attachment_Folder__c> projFolder=[Select id,name,RECO_Project__c from Attachment_Folder__c where name='Budget & Offer & Cost' and RECO_Project__c = :mainFolder[0].Offer__r.RECO_Project__c];
                    if(!projFolder.isEmpty()){
                      newFeedcontentforPrj.ParentId = projFolder[0].Id; 
                      insert newFeedcontentforPrj ;
                       shareFileToStakeholders(newFeedcontentforPrj.Id,mainFolder[0].Id,selectedFolder);
                       newFeedcontentforPrj=null;
                    }
                  }
              Offer__c offerObj = [SELECT Approval_status__c,EmailsenttoRPMforapprovedofferchkbox__c FROM Offer__c WHERE Id=:mainFolder[0].Offer__r.Id];
              if(offerObj.Approval_status__c=='Approved')
                {
                    offerObj.EmailsenttoRPMforapprovedofferchkbox__c=True;
                    update offerObj;
                }
           
            } 
        }
        /**** Code ends for Req # 117 (R2) ****/
            
    }

    
    public static void shareFileToStakeholders(Id feedId,Id mainFolderId,Id selectedFolder){
        Attachment_Folder__c mainFolder = [SELECT Name,Handover__c, Offer__c,Offer__r.RECO_Project__c,
                                            Offer__r.Approval_status__c,Offer__r.RECO_Project__r.Other_Retail_Project_Manager__c,
                                            Offer__r.RECO_Project__r.Retail_Project_Manager__c, Offer__r.EmailsenttoRPMforapprovedofferchkbox__c,
                                            Offer__r.RECO_Project__r.Support_National_boutique_manager__c,
                                            Offer__r.RECO_Project__r.National_boutique_manager__c,Offer__r.RECO_Project__r.PO_operator__c, 
                                            Offer__r.RECO_Project__r.PO_operator__r.Name, Offer__r.RECO_Project__r.Support_PO_Operator__c, 
                                            Offer__r.RECO_Project__r.Support_PO_Operator__r.Name FROM Attachment_Folder__c WHERE Id = :mainFolderId];
                                            
            system.debug('======mainnnn======='+mainFolder);
                                            
        FeedItem f = [SELECT RelatedRecordId,ParentId FROM FeedItem WHERE Id = :feedId];
        if(!Test.IsRunningTest()){
        ContentDocument cd = [SELECT Id, ParentId, PublishStatus FROM ContentDocument WHERE LatestPublishedVersionId = :f.RelatedRecordId];
        List<ContentDocumentLink> newCDLs = new List<ContentDocumentLink>();
        //throw new CustomException('FeedItem=' + f + ' ; cd=' + cd);
        if(mainFolder.Offer__r.RECO_Project__r.PO_operator__c!=NULL && mainFolder.Offer__r.RECO_Project__r.PO_operator__c != UserInfo.getUserId()){
            newCDLs.add(new ContentDocumentLink(
                LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.PO_operator__c,
                ContentDocumentId = cd.Id,
                ShareType = 'C'
            ));
        }

        if(mainFolder.Offer__r.RECO_Project__r.Support_PO_Operator__c!=NULL && mainFolder.Offer__r.RECO_Project__r.Support_PO_Operator__c != UserInfo.getUserId()){
            newCDLs.add(new ContentDocumentLink(
                LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.Support_PO_Operator__c,
                ContentDocumentId = cd.Id,
                ShareType = 'C'
            ));
        }
    /*******PROMILA BOORA****Code for Req.#99 starts here to share file with NBM starts here*********/ 
        if(mainFolder.Offer__r.RECO_Project__r.National_boutique_manager__c!=NULL && mainFolder.Offer__r.RECO_Project__r.National_boutique_manager__c!= UserInfo.getUserId()){
            newCDLs.add(new ContentDocumentLink(
                LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.National_boutique_manager__c,
                ContentDocumentId = cd.Id,
                ShareType = 'C'
            ));
        }
        if(mainFolder.Offer__r.RECO_Project__r.Support_National_boutique_manager__c!=NULL && mainFolder.Offer__r.RECO_Project__r.Support_National_Boutique_Manager__c!= UserInfo.getUserId()){
              newCDLs.add(new ContentDocumentLink(
                LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.Support_National_Boutique_Manager__c,
                ContentDocumentId = cd.Id,
                ShareType = 'C'
            ));
        }
        if(mainFolder.Offer__r.RECO_Project__r.Retail_project_manager__c!=NULL && mainFolder.Offer__r.RECO_Project__r.Retail_project_manager__c!= UserInfo.getUserId()){
            newCDLs.add(new ContentDocumentLink(
                LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.Retail_project_manager__c,
                ContentDocumentId = cd.Id,
                ShareType = 'C'
            ));
        }
        if(mainFolder.Offer__r.RECO_Project__r.Other_Retail_project_manager__c!=NULL && mainFolder.Offer__r.RECO_Project__r.Other_Retail_project_manager__c!= UserInfo.getUserId()){
            newCDLs.add(new ContentDocumentLink(
                LinkedEntityId = mainFolder.Offer__r.RECO_Project__r.Other_Retail_project_manager__c,
                ContentDocumentId = cd.Id,
                ShareType = 'C'
            ));
        }

        String AdminGroupId=System.Label.System_Admin_Group;
        if(AdminGroupId!=NULL){
            newCDLs.add(new ContentDocumentLink(
                LinkedEntityId = AdminGroupId,
                ContentDocumentId = cd.Id,
                ShareType = 'C'
            ));
        }
        /*******Code for Req.#99 to share file with NBM ends here*********/         

        /*List<Attachment_Folder__Share> afsList = [SELECT UserOrGroupId FROM Attachment_Folder__Share WHERE Parent.Name = 'Budget & Costs' AND ParentId = :mainFolder.Id AND RowCause = 'IsStakeholder__c'];
    
    Set<Id> userIds = new Set<Id>();
    for(Attachment_Folder__Share afs : afsList){
        userIds.add(afs.UserOrGroupId);
    }
    
    Map<Id,User> mapUserProfile = new Map<Id,User>([SELECT Id, Profile.Name FROM User WHERE Id IN :userIds]);
    String poProfile = UserConstants.BTQ_PO;
    
    List<ContentDocumentLink> newCDLs = new List<ContentDocumentLink>();
    for(Attachment_Folder__Share afs : afsList){
        if(afs.UserOrGroupId!=UserInfo.getUserId() && mapUserProfile.get(afs.UserOrGroupId).Profile.Name==poProfile){
            newCDLs.add(new ContentDocumentLink(
                LinkedEntityId = afs.UserOrGroupId,
                ContentDocumentId = cd.Id,
                ShareType = 'C'
            ));
        }
    }*/
    
    /*List<ContentDocumentLink> newCDLs = new List<ContentDocumentLink>();
    for(Attachment_Folder__Share afs : [SELECT UserOrGroupId FROM Attachment_Folder__Share WHERE ParentId = :mainFolder.Id AND RowCause = 'IsStakeholder__c']){
        if(afs.UserOrGroupId!=UserInfo.getUserId()){
            newCDLs.add(new ContentDocumentLink(
                LinkedEntityId = afs.UserOrGroupId,
                ContentDocumentId = cd.Id,
                ShareType = 'V'
            ));
        }
    }*/
   /**** PROMILA BOORA - 17/3/16 - Commented line 191 and used 192 to insert sharing for all users except inactive user ***/
   // insert newCDLs;
   Database.SaveResult[] lsr = Database.insert(newCDLs,false); 
   system.debug('==========lsr=========='+lsr);
   system.debug('==========lsrcdlls=========='+newCDLs);
   }
   else
   {
  
   
   }
}
    
}
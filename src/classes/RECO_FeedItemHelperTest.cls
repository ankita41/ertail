/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RECO_FeedItemHelperTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account accBouLocation = TestHelper.createAccount(rTypeId, market.Id);
        insert accBouLocation;
        
        String tmpAddress = (accBouLocation.BillingStreet != null ? (accBouLocation.BillingStreet + '<br/>') : '') + 
                            (accBouLocation.BillingCity!= null ? (accBouLocation.BillingCity + ', ') : '') + 
                            (accBouLocation.BillingPostalCode!= null ? (accBouLocation.BillingPostalCode + '') : '') + '<br/>' + 
                            (accBouLocation.BillingCountry!= null ? accBouLocation.BillingCountry : '');
        
        
        Id rTypeIdSup = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account accSupp = TestHelper.createAccount(rTypeIdSup, market.Id);
        insert accSupp;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, accBouLocation.Id);
        btq.Net_selling_m2__c = 100;
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Pop-up Relocation Project').getRecordTypeId();
        List<RECO_Project__c> newBProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        for(RECO_Project__c prj : newBProjs){
            prj.Cutoff_amount_local_year1__c = 100;
            prj.Cutoff_amount_local_year2__c = 100;
            prj.CapEx_amount_local__c = 100; 
            prj.DF_CapEx_amount_local__c = 100;
            
        }
        insert newBProjs;
        
        Offer__c offer = new Offer__c(RECO_Project__c = newBProjs[0].Id);
        insert offer;
         
    	Attachment_Folder__c mainFolder = new Attachment_Folder__c(name = 'Budget & Costs',Offer__c=offer.Id,RECO_Project__c=newBProjs[0].id);
        insert mainFolder;
        FeedItem feedsMain = new FeedItem (ParentId = mainFolder.id,Body = 'Test Subfolders');
        insert feedsMain;
        Attachment_Subfolder__c attsub = new Attachment_Subfolder__c(Attachment_Folder__c = mainFolder.id,name = 'test record');
        insert attsub;
        FeedItem feeds = new FeedItem (ParentId = attsub.id,Body = 'Test Subfolders');
        insert feeds;
        update mainFolder;
    }
}
public with sharing class RECO_InviceEditWithCostItemsExt {
    public Invoice__c varOffer {get;set;}
    public List<Cost_Item__c> costItems {get;set;}
    public Map<Id,Boolean> mapCostItemOfferLink {get;set;} 
    public Invoice__c recName {get;set;}
    public boolean showError;
    private String qryStr {get;set;}
    
    public Boolean isAdmin{
        get{
            return (UserConstants.CURRENT_USER.Profile.Name == UserConstants.SYS_ADM);
        } set;
    }
    public Purchase_Order__c poIns{get;set;}
    public RECO_InviceEditWithCostItemsExt(ApexPages.StandardController con) {
         poIns=new Purchase_Order__c ();                        
         Boolean sandboxBool=[SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
         String poId='';
         if(sandboxBool==true){
             poId=ApexPages.currentPage().getParameters().get('CF00Nb000000AWIoK_lkid');
         }
         else{
            poId=ApexPages.currentPage().getParameters().get('CF00Nb000000AWIoK_lkid');
         }
         
                                                                                                
         if(String.isNotBlank(poId)){
             poIns=[Select id,name,RECO_Project__c,Offer__c,Offer__r.Supplier__c,Offer__r.Supplier__r.name from Purchase_Order__c where id=:poId];
         }
        
         if(con.getRecord().Id!=null){
            varOffer = (Invoice__c)con.getRecord();
            queryOffer();
            //queryCostItems(); 
        }
        else{
            varOffer = (Invoice__c)con.getRecord();
            //added follwoing code if the freezecost items are checked in project level ,new offer should be checked by default
            if(varoffer != null && varOffer.RECO_Project__c != null){
             List<RECO_Project__c> objList=[SELECT Id,Freeze_Cost_Items__c  from  RECO_Project__c where id= :varOffer.RECO_Project__c];
             if(objList!= null && objList.size()>0){
                 RECO_Project__c obj=objList[0];
                 if(obj != null)
                     varoffer.Extra_cost__c=obj.Freeze_Cost_Items__c;
             }
            
            }
            //system.debug('varOffer @@'+varOffer.RECO_Project__c+''+varOffer.RECO_Project__r.Freeze_Cost_Items__c );
            
        }
         if(con.getRecord().Id==null){
             varoffer.RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Draft').getRecordTypeId();
         }
        contactListRerender();
    }
    
    
     public list<selectoption> contactSelectOption{get;set;}
    public void contactListRerender()
    {
        contactSelectOption=new List<selectoption>();
        contactSelectOption.add(new SelectOption('','--None--'));
       // system.debug('=====test==========='+varOffer.Supplier__c);
        if(String.isNotBlank(varOffer.Supplier__c)){
        
            List<Contact> conList=[Select id,name from contact where accountid=:varOffer.Supplier__c];
            if(!conList.isEmpty()){
                
                for(contact con : conList)
                {
                     contactSelectOption.add(new SelectOption(con.id,con.name));
                }
            }
        }
    }
    
    
    private void queryOffer(){
        Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Invoice__c').getDescribe().fields.getMap();
        String qryStr = 'SELECT RECO_Project__r.Name,Offer__r.supplier__c,Offer__r.supplier__r.name, ';
        for(String s : fieldsMap.keySet()) {
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM Invoice__c WHERE Id = \'' + varOffer.Id + '\' ';
        varOffer = Database.query(qryStr);
    }
    
   

    public PageReference save(){
    
       
        Double totalSelectedCost = 0;
        List<Cost_Item__c> updCostItems = new List<Cost_Item__c>();

    //Code starts for REQ#119 (R2) by PRIYA
    showError = false;
     if(varOffer.Id!=null){
        recName = [SELECT RecordTypeId,supplier__c FROM Invoice__c where id = :varOffer.Id];
        if( varOffer.Approval_status__c == 'Approved' && recName.RecordTypeId != varOffer.RecordTypeId )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record type can not be changed for Approved offer'));
            showError = true;
            return null;
        }
    }
    
        //Code ends by PRIYA
            
        if(varOffer.Id!=null){
            
           /* for(Cost_Item__c c : costItems){
                if(mapCostItemOfferLink.get(c.Id)){
                    c.Invoice__c = varOffer.Id;
                    totalSelectedCost = totalSelectedCost + c.Validated_amount__c;
                    if(varOffer.Approval_status__c == 'Approved') c.RecordTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Confirmed').getRecordTypeId();                    
                }
                else
                    c.Invoice__c = NULL;
                    
                updCostItems.add(c);
            }*/
            
            /*for(Cost_Item__c c : [SELECT Invoice__c FROM Cost_Item__c WHERE Invoice__c = :varOffer.Id AND Currency__c != :varOffer.Currency__c]){
                c.Invoice__c = NULL;
                updCostItems.add(c);
            }*/
            //--start--Modifed the exsting code to fix the save issue --            
            showError=false;
            varOffer.Total_Selected_Items__c = totalSelectedCost;
            if( varoffer.Total_Amount_excl_VAT__c == totalSelectedCost){
                 showError = false;
            }else if(costItems != null && costItems .size()>0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, the offer could not be saved because the total amount does not match the total of the selected items.'));
                showError = true;
            }
             //--end--Modifed the exsting code to fix the save issue --   
        update updCostItems;
        }

        Contact con;
        if(varOffer.Supplier_Contact__c!=null){
            con = [SELECT AccountId FROM Contact WHERE Id = :varOffer.Supplier_Contact__c];
            varOffer.Supplier__c = con.AccountId;
        }
        
        
        //poIns=[Select id,name,RECO_Project__c,Offer__c from Purchase_Order__c where id=:poId];
        if(poIns.Offer__c!=null){
            varOffer.offer__c=poIns.Offer__c;
        }
         if(poIns.Offer__r.Supplier__c!=null){
            varOffer.supplier__c=poIns.Offer__r.Supplier__c;
        }
        if(poIns.RECO_Project__c!=null){
            varOffer.RECO_Project__c=poIns.RECO_Project__c;
        }
        if(poIns.Id!=null){
            varOffer.po__c=poIns.Id; 
        }
        
        upsert varOffer;  
        
        system.debug('=======check sobject Type====='+varOffer.getSobjectType());

        
        ApexPages.currentPage().getParameters().put('Id',varOffer.id);
        String hostVal  = ApexPages.currentPage().getHeaders().get('Host');
        String urlVal = Apexpages.currentPage().getUrl();
        String URLL = 'https://' + hostVal+ urlVal;
        urll=urll.replaceall('AJAXREQUEST=_viewRoot','id='+varOffer.id);
                          
        queryOffer();
        //queryCostItems();
        //String url='https://' + ApexPages.currentPage().getHeaders().get('Host') + ApexPages.currentPage().getUrl()+'?id='+varOffer.id;
        
        
        system.debug('====urlll============='+urll);
        
        PageReference redirectPage = new PageReference('/p/attach/NoteAttach?pid='+varOffer.id+'&parentname='+varOffer.Name+'&retURL=/apex/RECOInvoiceButtons?id='+varOffer.id);
        //Decide if you want to keep the viewState
        redirectPage.setRedirect(true);//true = discard the viewState
        //return the PageReference
        return redirectPage;
        
        
        /*PageReference redirectPage = new PageReference('/apex/RECOInvoiceButtons?id='+varOffer.id);
        //Decide if you want to keep the viewState
        redirectPage.setRedirect(true);//true = discard the viewState
        //return the PageReference
        return redirectPage;*/
        
    }
    
    public PageReference close(){
        if(varOffer.Id!=NULL)
            return new PageReference('/' + varOffer.Id);
        else{
            if(String.isNotBlank(varOffer.RECO_Project__c)){
                return new PageReference('/' + varOffer.RECO_Project__c);
            }
            else{
                return new PageReference('/' + poIns.RECO_Project__c);
            }
        }
    }
    
    public PageReference saveClose(){
        save();
        if (!showError){
        return close();
        }else return null;
    }

}
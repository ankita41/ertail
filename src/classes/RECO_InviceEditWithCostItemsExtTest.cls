/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RECO_InviceEditWithCostItemsExtTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id);
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        for(Cost_Item__c c : newCostItems){
            c.Currency__c = 'CHF';
        }
        insert newCostItems;
        
        Test.startTest();
        PageReference pageRef = Page.OfferEditWithCostItems;
        Test.setCurrentPage(pageRef);
        
        String rTypeIdOff = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        Offer__c offer = new Offer__c(RECO_Project__c = bProj.Id,recordTypeId=rTypeIdOff);
        insert offer;
        
        Purchase_Order__c prRec = TestHelper.createPO(offer.Id,bProj.Id);
        insert prRec;
        
        
        Invoice__c inv = new Invoice__c();
        inv.Name='5';
        inv.Invoice_Date__c=Date.today();
        inv.Invoice_amount__c=100;
        inv.Currency__c='CHF';
        inv.Description__c='Test';
        inv.RECO_Project__c=bProj.Id;
        inv.Offer__c=offer.Id;
        inv.PO__c=prRec.Id;
        inv.Supplier__c=suppAcc.Id;
        insert inv;
        
        
        
        ApexPages.StandardController con = new ApexPages.StandardController(inv);
        RECO_InviceEditWithCostItemsExt ext = new RECO_InviceEditWithCostItemsExt(con);
        ext.varOffer=inv;
        ext.varOffer.Currency__c = 'CHF';
        ext.varOffer.Supplier_Contact__c = suppCon.Id;
        ext.close();
        ext.saveClose();
        ext.save();
        
        try
        {
        ext.mapCostItemOfferLink.put(ext.costItems[0].Id, true);
        ext.saveClose();
        SYSTEM.Assert([SELECT Offer__c FROM Cost_Item__c WHERE Id = :ext.costItems[0].Id].Offer__c == inv.Id);
        }
        catch(Exception e){}
        Test.stopTest();
    }
    
    static testMethod void myUnitTestNewInv() {
        // TO DO: implement unit test
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id);
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        insert bProj;
        
        rTypeId = Schema.sObjectType.Cost_Item__c.getRecordTypeInfosByName().get('Expected').getRecordTypeId();
        List<Cost_Item__c> newCostItems = TestHelper.createCostItems(rTypeId, bProj.Id);
        for(Cost_Item__c c : newCostItems){
            c.Currency__c = 'CHF';
        }
        insert newCostItems;
        
        Test.startTest();
        PageReference pageRef = Page.OfferEditWithCostItems;
        Test.setCurrentPage(pageRef);
        
        String rTypeIdOff = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        Offer__c offer = new Offer__c(RECO_Project__c = bProj.Id,recordTypeId=rTypeIdOff);
        insert offer;
        
        Purchase_Order__c prRec = TestHelper.createPO(offer.Id,bProj.Id);
        insert prRec;
        
        
        Invoice__c inv = new Invoice__c();
        inv.Name='5';
        inv.Invoice_Date__c=Date.today();
        inv.Invoice_amount__c=100;
        inv.Currency__c='CHF';
        inv.Description__c='Test';
        inv.RECO_Project__c=bProj.Id;
        inv.Offer__c=offer.Id;
        inv.PO__c=prRec.Id;
        inv.Supplier__c=suppAcc.Id;
        //insert inv;
        
        
        
        ApexPages.StandardController con = new ApexPages.StandardController(inv);
        RECO_InviceEditWithCostItemsExt ext = new RECO_InviceEditWithCostItemsExt(con);
        ext.varOffer=inv;
        ext.varOffer.Currency__c = 'CHF';
        ext.varOffer.Supplier_Contact__c = suppCon.Id;
        ext.close();
        ext.saveClose();
        ext.save();
        
        try
        {
        ext.mapCostItemOfferLink.put(ext.costItems[0].Id, true);
        ext.saveClose();
        SYSTEM.Assert([SELECT Offer__c FROM Cost_Item__c WHERE Id = :ext.costItems[0].Id].Offer__c == inv.Id);
        }
        catch(Exception e){}
        Test.stopTest();
    }
    
}
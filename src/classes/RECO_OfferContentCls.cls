public with sharing class RECO_OfferContentCls {
public string pageName {get;set;}
    public RECO_OfferContentCls(ApexPages.StandardController controller) {
        pageName = ApexPages.CurrentPage().getUrl();  
        // IT ALWAYS STARTS WITH /APEX/ SO REMOVE IT
        pageName = pageName.replaceFirst('/apex/','');         
        pageName = EncodingUtil.urlEncode(pageName, 'UTF-8');  
        // %3F IS THE VALUE OF THE QUESTION MARK IN UTF-8 
        string[] pageNameExtra = pageName.split('%3F',0);   
        // SO YOU MAY SPLIT THE STRING AT THAT POINT
          // FOR THIS PURPOSE YOU ONLY NEED THE FIRST
        // IN THE RESULTING ARRAY
        pageName = pageNameExtra[0]; 
        // HERE IS YOUR PRODUCT  
        system.debug('pageName-->'+pageName);                       
       
    }

}
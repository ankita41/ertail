/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RECO_OfferContentClsTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Country_Mapping__c cou=new Country_Mapping__c();
        cou.name='USA';
        cou.Short_Form__c='US';
        insert cou;
        
        GoogleMapsSettings__c gSett=new GoogleMapsSettings__c();
        gSett.name='Client ID';
        gSett.Value__c='gme-nestlenespressosa';
        insert gSett;
        
        GoogleMapsSettings__c gSett1=new GoogleMapsSettings__c();
        gSett1.name='Crypto Key';
        gSett1.Value__c='FePuq8NIxp3L1_VAHPNwoNLKbQs=';
        insert gSett1;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        acc.BillingStreet='Test street';
        acc.billingCountry='USA';
        acc.billingCity='NJ';
        acc.billingState='Test State';
        acc.BillingPostalCode='34343';
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.boutique_location_account__c=acc.id;
        btq.Net_selling_m2__c = 100;
        insert btq;
        
        update btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        List<RECO_Project__c> newBProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        for(RECO_Project__c prj : newBProjs){
            prj.Cutoff_amount_local_year1__c = 100;
            prj.Cutoff_amount_local_year2__c = 100;
            prj.CapEx_amount_local__c = 100; 
            prj.DF_CapEx_amount_local__c = 100;
        }
        insert newBProjs;
        
        rTypeId = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        Offer__c off = TestHelper.createOffer(rTypeId,newBProjs[0].Id);
        insert off;
        
        Test.setCurrentPage(Page.OfferContent);
        ApexPages.StandardController sc = new ApexPages.StandardController(off);
        RECO_OfferContentCls cls=new RECO_OfferContentCls(sc);
        cls.pageName='test';
    }
}
/** @Class      :   RECO_POOffersCls
*   @Description:   Class for showing PO and Offers in PDF format
*   @Author     :   Himanshu Palerwal
*   @Created    :   12 Dec 2015
*

*                         
*****/

public with sharing class RECO_POOffersCls {
    public decimal grandTotalCHF{get;set;}
    public decimal grandTotalEUR{get;set;}
    
    public RECO_POOffersCls(ApexPages.StandardController controller) {
    bouProjectId=ApexPages.CurrentPage().getparameters().get('Id');
    wrapperClassShowOfferList=new list<wrapperClassShowOfferPO>();
    offerIdPurOrListMap=new Map<Id,List<Purchase_Order__c>>();
    showOffersPO();
    }

public String bouProjectId{get;set;} 
public Map<Id,List<Purchase_Order__c>> offerIdPurOrListMap{get;set;}
public list<wrapperClassShowOfferPO> wrapperClassShowOfferList{get;set;}
public Map<Id,Offer__c> idOfferMap{get;set;}
public RECO_POOffersCls ()
{
    
}

public void showOffersPO()
{
    List<Purchase_Order__c> purOrderList=[Select p.SystemModstamp, p.Supplier__c, p.Supplier_Contact__c, p.RECO_Project__c, p.PO_Notification_Sent__c, 
                                                 p.PO_Notification_Sent_Date__c, p.OwnerId, p.Offer__c, p.Name, 
                                                 p.LastModifiedDate, p.LastModifiedById, p.LastActivityDate, p.Issue_date__c, p.IsMine__c, p.IsDeleted, 
                                                 p.Invoice_No__c, p.Id, p.Description__c, p.Currency__c, p.CreatedDate, p.CreatedById, p.Billing_address__c, 
                                                 p.Amount_local__c, p.Amount__c, p.Amount_Offer_Currency__c, p.Amount_EUR__c, p.Amount_CHF__c 
                                                 From Purchase_Order__c p where RECO_Project__c=:bouProjectId];
    for(Purchase_Order__c pur : purOrderList)
    {
        if(offerIdPurOrListMap.containsKey(pur.Offer__c)){
            offerIdPurOrListMap.get(pur.Offer__c).add(pur);
        }
        else{
            offerIdPurOrListMap.put(pur.Offer__c,new List<Purchase_Order__c>{pur});
        }
    }
    grandTotalCHF=0;
    grandTotalEUR=0;
    
    for(String key : offerIdPurOrListMap.keyset())
    {
        if(!offerIdPurOrListMap.get(key).isEmpty()){
            List<Purchase_Order__c> purOrderTotalList=offerIdPurOrListMap.get(key);
            decimal tempCHFTotal=0;
            decimal tempEURTotal=0;
            for(Purchase_Order__c pu : purOrderTotalList)
            {
                tempCHFTotal+=pu.Amount_CHF__c;
                //Change the code as per ticket# RM0025628181 - start
                tempEURTotal+=pu.Amount_local__c;
                //tempEURTotal+=pu.Amount_EUR__c;
                // //Change the code as per ticket# RM0025628181 - End
                
                grandTotalCHF+=pu.Amount_CHF__c;
                //Change the code as per ticket# RM0025628181 - start
                grandTotalEUR+=pu.Amount_local__c;
                //grandTotalEUR+=pu.Amount_EUR__c;
                //Change the code as per ticket# RM0025628181 - end
               
            }    
            wrapperClassShowOfferList.add(new wrapperClassShowOfferPO(key,offerIdPurOrListMap.get(key),tempCHFTotal,tempEURTotal));
        }
    
    }
    //Change the code(added two values[o.Supplier__r.Name, o.RECO_Project__r.Currency__c] in query) as per ticket# RM0025628181 - start
     idOfferMap=new Map<Id,Offer__c>([Select o.isMine__c, o.VAT_No__c, o.Unique_External_Id__c, o.Total_Selected_Items__c, o.Total_Cost_m2__c, o.Total_Amount_of_Related_POs__c,     
                                     o.Total_Amount_local__c, o.Total_Amount_excl_VAT__c, o.Total_Amount_EUR__c, o.Total_Amount_CHF__c, o.SystemModstamp, 
                                     o.Support_Retail_Project_Manager__c, o.Support_PO_Operator__c, o.Support_National_Boutique_Manager__c, o.Supplier__c, 
                                     o.Supplier_Contact__c, o.Retail_project_manager__c, o.Record_Type_Name__c, o.RecordTypeId, o.Receive_date__c, o.Reason__c, 
                                     o.RECO_Project__c, o.Proforma_No__c, o.PO_operator__c, o.PO_List__c, o.Other_Manager_Architect__c, o.Other_Design_Architect__c, 
                                     o.Offer_approved__c, o.Offer__c, o.Notify_for_approval__c, o.Notify_date__c, o.National_boutique_manager__c, o.Name, 
                                     o.Manager_architect__c, o.LastModifiedDate, o.LastModifiedById, o.LastActivityDate, 
                                     o.IsDeleted, o.Id, o.Extra_cost__c, o.Document_date__c, o.Dis_Approval_date__c, o.Design_Architect__c, o.Currency__c, 
                                     o.CreatedDate, o.CreatedById, o.Billing_address__c, o.Balance__c, o.Approval_status__c, o.Supplier__r.Name, o.RECO_Project__r.Currency__c From Offer__c o
                                     where id in:offerIdPurOrListMap.keyset()]);
    
	} 
 //Change the code as per ticket# RM0025628181 - End
public class wrapperClassShowOfferPO{
public String OfferId{get;set;}
public list<Purchase_Order__c> purOrderList{get;set;}
public decimal totalAmountCHF{get;set;}
public decimal totalAmountEUR{get;set;}

public wrapperClassShowOfferPO(String OfferId,list<Purchase_Order__c> purOrderList,decimal totalAmountCHF,decimal totalAmountEUR)
{
    this.OfferId=OfferId;
    this.purOrderList=purOrderList;
    this.totalAmountCHF=totalAmountCHF;
    this.totalAmountEUR=totalAmountEUR;
}
}
}
/** @Class      :   RECO_RejectSupplier
*   @Description:   Class called from button to delete temp supplier record
*   @Author     :   Himanshu Palerwal
*   @Created    :   15 Jan 2015
*

*                         
*****/
global without sharing class RECO_RejectSupplier{

    WebService static void deleteSupplier(String Id) {
         delete[Select id from Account where id=:Id];
    }
}
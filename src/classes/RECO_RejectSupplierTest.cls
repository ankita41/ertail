/** @Class      :   RECO_RejectSupplierTest
*   @Description:   Test class for RECO_RejectSupplier
*   @Author     :   Himanshu Palerwal
*   @Created    :   15 Jan 2015
*

*                         
*****/
@isTest
private class RECO_RejectSupplierTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier Temp').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        RECO_RejectSupplier rejSupp=new RECO_RejectSupplier();
        RECO_RejectSupplier.deleteSupplier(acc.Id);
    }
}
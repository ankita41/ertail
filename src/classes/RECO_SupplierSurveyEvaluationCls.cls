public with sharing class RECO_SupplierSurveyEvaluationCls {

public Map<String,List<showSurveyQues>> quesSurveyMap{get;set;}
public Map<String,showNegaPosiComm> commentsSurveyMap{get;set;}
public RECO_Project__c bp{get;set;}
public Boutique__c b{get;set;}
public Supplier_Survey_Evaluation__c suppSEv{get;set;}
public String BouProId{get;set;}
public Handover__c hO{get;set;}
public String accSuppId;
public RECO_SupplierSurveyEvaluationCls()
{
    showError=false;
    suppSEv=new Supplier_Survey_Evaluation__c();
    quesSurveyMap=new Map<String,List<showSurveyQues>>();
    commentsSurveyMap=new Map<String,showNegaPosiComm>();
    showSurveyQuesWrapList=new List<showSurveyQues>();
    showNegaPosiCommWrapList=new List<showNegaPosiComm>();
    BouProId=ApexPages.currentPage().getParameters().get('id') ;
    accSuppId=ApexPages.currentPage().getParameters().get('accId') ;
    questionsQuery();
    if(String.isNotBlank(BouProId)){
        bp=[Select Furniture_Supplier__c,Boutique__c,Retail_project_manager__r.name,Manager_architect__r.name,  BTQ_Location_Address__c, Manager_architect__c,Handover_Date__c From RECO_Project__c where id=:BouProId];
        b=[Select Concept__c,RecordTypeId,boutique_location_account__r.name,typology__c From Boutique__c where id=:bp.Boutique__c];
        //hO=[Select Visit_date__c, RecordType.Name, Retail_project_manager__c,Retail_project_manager__r.Name,National_boutique_manager__r.name, National_boutique_manager__c From Handover__c where RECO_Project__c=:BouProId ORDER BY CreatedDate limit 1];
        if(String.isNotBlank(bp.Manager_architect__c)){
        suppSEv.Supplier_Project_Manager_1__c=bp.Manager_architect__c;
        suppSEv.Boutique_Project__c=BouProId;
    }
    }    
    if(String.isNotBlank(accSuppId)){
        suppSEv.Supplier_Name__c=accSuppId;
    }
    reRenderProjectDetails();
    
}

public void reRenderProjectDetails()
{
    if(String.isNotBlank(suppSEv.Boutique_Project__c)){
        bp=[Select Furniture_Supplier__c,Boutique__c,  BTQ_Location_Address__c,Retail_project_manager__r.name,Manager_architect__r.name, Manager_architect__c,Handover_Date__c From RECO_Project__c where id=:suppSEv.Boutique_Project__c];
        b=[Select Concept__c,RecordTypeId,boutique_location_account__r.name,typology__c From Boutique__c where id=:bp.Boutique__c];
       
    }
}

public void questionsQuery()
{
    List<SupplierSurveyQues__c> suppQuestList=SupplierSurveyQues__c.getall().values();
    suppQuestList.sort();
    for(SupplierSurveyQues__c sup : suppQuestList)
    {
        
        commentsSurveyMap.put(sup.Section_Heading__c,new showNegaPosiComm('0','','',sup.Section_Color__c));
        List<showSurveyQues> tempshowSurveyQuesWrapList=new List<showSurveyQues>();
        tempshowSurveyQuesWrapList.add(new showSurveyQues(sup.Name,sup.Survey_Ques__c,'','',''));
        if(quesSurveyMap.containsKey(sup.Section_Heading__c)){
            quesSurveyMap.get(sup.Section_Heading__c).addall(tempshowSurveyQuesWrapList);
        }
        else{
            quesSurveyMap.put(sup.Section_Heading__c,tempshowSurveyQuesWrapList);
        }
    }
}
public Boolean showError{get;set;}
public pagereference saveSurvey()
{
    showError=false;
    //Supplier_Survey_Evaluation__c suppEval=new Supplier_Survey_Evaluation__c();
    if(String.isNotBlank(BouProId)){
        suppSEv.Boutique_Project__c=BouProId;
    }
    //suppEval.Supplier_Name__c=suppSev.Supplier_Name__c;
    upsert suppSEv;
    
    List<Supplier_Evaluation_Ans__c> upsertSuppEvalAnsList=new List<Supplier_Evaluation_Ans__c >();
    for(String keyMain : quesSurveyMap.keySet())
    {
        if(!quesSurveyMap.get(keyMain).isEmpty()){
            List<showSurveyQues> tempWrapperShowSurList=quesSurveyMap.get(keyMain);
            showNegaPosiComm shNegPosComm= commentsSurveyMap.get(keyMain);
            system.debug('========shNegPosComm======='+shNegPosComm);
            if(keyMain.contains('PROJECT MANAGEMENT')){
                system.debug('===============inside=====');
                if(String.isNotBlank(shNegPosComm.avgScore))
                    suppSEv.PM_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
          
            if(keyMain.contains('LOGISTICS')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppSEv.Logistics_and_Installation_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
           
            if(keyMain.contains('OFFERS')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppSEv.Offers_and_Costs_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
          
            if(keyMain.contains('HANDOVER')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppSEv.Handover_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
           
            if(keyMain.contains('MILLWORK')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppSEv.Quality_of_Millwork_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
           
            if(keyMain.contains('TIMING')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppSEv.Timing_Flexibility_Availability_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
            
            if(keyMain.contains('SHE')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppSEv.SHE_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
           
            for(showSurveyQues suvQues : tempWrapperShowSurList)
            {
                Supplier_Evaluation_Ans__c supp=new Supplier_Evaluation_Ans__c();
                supp.Supplier_Survey_Evaluation__c=suppSEv.Id;
                supp.Survey_Question__c=suvQues.subQuestions;
                if(String.isNotBlank(suvQues.subScore) &&  Decimal.valueof(suvQues.subScore) <>0){
                    if(Decimal.valueof(suvQues.subScore)>=1 && Decimal.valueof(suvQues.subScore)<=6){
                        supp.Answers_score__c=decimal.valueof(suvQues.subScore);
                    }
                    else {
                        showError=true;
                        return null;
                    }  
                }
                //[START]MANSIMAR - ADDED SERVER SIDE VALIDATION for comments size 
                //truncate if even after client side validatio size is bigger than 255
                if(shNegPosComm.negativeComments !=null && shNegPosComm.negativeComments.length() > 255){
                supp.Negative_Comments__c=shNegPosComm.negativeComments.substring(0,255);
                }
                else {
                  supp.Negative_Comments__c=shNegPosComm.negativeComments;
                }
                if(shNegPosComm.positiveComments !=null && shNegPosComm.positiveComments.length() > 255){
                supp.Positive_Comments__c=shNegPosComm.positiveComments.substring(0,255);
                }
                else {
                supp.Positive_Comments__c=shNegPosComm.positiveComments;
                }
                // [END]MANSIMAR - ADDED SERVER SIDE VALIDATION for comments size
                supp.Header_Question__c=keyMain ;
                supp.Sub_Header_Color__c=shNegPosComm.color;
                supp.Sub_Header_Sequence_No__c=suvQues.serialNoSubques;
                upsertSuppEvalAnsList.add(supp);
            }
            
           
        }
    }
    system.debug('==========suppEval======='+suppSEv);
    update suppSEv;
    if(!upsertSuppEvalAnsList.isEmpty()){
        upsert upsertSuppEvalAnsList;
    }
    
    String returnUrl='';
    if(String.isNotBlank(accSuppId)){
        returnUrl='/'+accSuppId;
    }
    else{
        returnUrl='/'+BouProId;
    }
    return new pagereference(returnUrl);
    
    
}
public pagereference backPage()
{
    String returnUrl='';
    if(String.isNotBlank(accSuppId)){
        returnUrl='/'+accSuppId;
    }
    else{
        returnUrl='/'+BouProId;
    }
    return new pagereference(returnUrl);
}
public string keyMain{get;set;}
public void calculateAvgMeth()
{
    system.debug('========keyMain====='+keyMain);
    if(String.isNotBlank(keyMain)){
        List<showSurveyQues> tempWrapperShowSurList=quesSurveyMap.get(keyMain);
        decimal totalAvg=0;
        Integer totalCountRec=0;
        for(showSurveyQues sup : tempWrapperShowSurList)
        {
            if(String.isNotBlank(sup.subScore)){
                totalCountRec=totalCountRec+1;
                totalAvg=totalAvg+decimal.valueof(sup.subScore);
            }
        }
        showNegaPosiComm shNegPosComm= commentsSurveyMap.get(keyMain);
        if(totalCountRec!=0)                        //Priya: Added to solve the Divide by 0 error
        shNegPosComm.avgScore=String.valueof((totalAvg/totalCountRec).setscale(1));
        commentsSurveyMap.put(keyMain,shNegPosComm);
        //shNegPosComm.avgScore=String.valueof((totalAvg/tempWrapperShowSurList.size()));
        
    }
}

public List<showSurveyQues> showSurveyQuesWrapList{get;set;}
public class showSurveyQues{
    public String serialNoSubques{get;set;}
    public String subQuestions{get;set;}
    public String subScore{get;set;}
    public String positiveComments{get;set;}
    public String negativeComments{get;set;}
    
    public showSurveyQues(String serialNoSubques,String subQuestions,String subScore,String positiveComments,String negativeComments)
    {
        this.serialNoSubques=serialNoSubques;
        this.subQuestions=subQuestions;
        this.subScore=subScore;
        this.positiveComments=positiveComments;
        this.negativeComments=negativeComments;
    } 
}

public List<showNegaPosiComm> showNegaPosiCommWrapList{get;set;}
public class showNegaPosiComm{
    public String avgScore{get;set;}
    public String positiveComments{get;set;}
    public String negativeComments{get;set;}
    public String color{get;set;}
    
    public showNegaPosiComm(String avgScore,String positiveComments,String negativeComments,String color)
    {
        this.avgScore=avgScore;
        this.positiveComments=positiveComments;
        this.negativeComments=negativeComments;
        this.color=color;
    } 
}



}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RECO_SupplierSurveyEvaluationClsTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
       TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        
        Id rTypeIdSup = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account accSupp = TestHelper.createAccount(rTypeIdSup, market.Id);
        insert accSupp;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.Net_selling_m2__c = 100;
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        List<RECO_Project__c> newBProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        for(RECO_Project__c prj : newBProjs){
            prj.Cutoff_amount_local_year1__c = 100;
            prj.Cutoff_amount_local_year2__c = 100;
            prj.CapEx_amount_local__c = 100; 
            prj.DF_CapEx_amount_local__c = 100;
        }
        insert newBProjs;
        
        
        SupplierSurveyQues__c suppQues=new SupplierSurveyQues__c();
        suppQues.Name='1.1';
        suppQues.Header_Color__c='#569124';
        suppQues.Section_Color__c='#90FF33';
        suppQues.Section_Heading__c='1. PROJECT MANAGEMENT & COMMUNICATION';
        suppQues.Survey_Ques__c='Coordination and Communication with Nespresso and other suppliers / consultants';
        insert suppQues;
        
        Supplier_Survey_Evaluation__c supp=new Supplier_Survey_Evaluation__c();
        supp.Boutique_Project__c=newBProjs[0].Id;
        supp.Supplier_Name__c=accSupp.id;
        insert supp;
        
       ApexPages.currentPage().getParameters().put('Id',newBProjs[0].Id); 
       ApexPages.currentPage().getParameters().put('accId',accSupp.Id); 
       RECO_SupplierSurveyEvaluationCls cls=new RECO_SupplierSurveyEvaluationCls();
       cls.backPage();
       cls.keyMain='1. PROJECT MANAGEMENT & COMMUNICATION';
       cls.showSurveyQuesWrapList.add(new RECO_SupplierSurveyEvaluationCls.showSurveyQues('1.1','Coordination and Communication with Nespresso and other suppliers / consultants','3','test +','test -'));
       cls.quesSurveyMap.put('1. PROJECT MANAGEMENT & COMMUNICATION',cls.showSurveyQuesWrapList);
       cls.calculateAvgMeth();
       cls.saveSurvey();
       
    }
}
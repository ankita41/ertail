public with sharing class RECO_SupplierSurveyEvaluationViewCls {

    public Map<String,List<showSurveyQues>> quesSurveyMap{get;set;}
    public Map<String,showNegaPosiComm> commentsSurveyMap{get;set;}
    public RECO_Project__c bp{get;set;}
    public Boutique__c b{get;set;}
    public Supplier_Survey_Evaluation__c suppSEv{get;set;}
    public String BouProId{get;set;}
    public Handover__c hO{get;set;}
    public String accSuppId;
    public String tempCreatedDate{get;set;}
    public String tempLastModifiedDate{get;set;}
    public RECO_SupplierSurveyEvaluationViewCls(ApexPages.StandardController controller) {
        showError=false;
        quesSurveyMap=new Map<String,List<showSurveyQues>>();
        commentsSurveyMap=new Map<String,showNegaPosiComm>();
        showSurveyQuesWrapList=new List<showSurveyQues>();
        showNegaPosiCommWrapList=new List<showNegaPosiComm>();
        BouProId=ApexPages.currentPage().getParameters().get('id') ;
        accSuppId=ApexPages.currentPage().getParameters().get('accId') ;
        questionsQuery();
        if(String.isNotBlank(BouProId)){
            Supplier_Survey_Evaluation__c suppSur=[Select id,Supplier_Project_Manager_1__c,Supplier_Name__r.Name,Handover_Date__c,Boutique_Project__c,Type_of_works_picklist__c, CreatedDate, LastModifiedDate, LastModifiedBy.Name from Supplier_Survey_Evaluation__c where id=:BouProId];
            suppSEv=suppSur;
            tempCreatedDate=String.valueof(suppSEv.createddate.date());
            tempLastModifiedDate=String.valueof(suppSEv.LastModifiedDate.date());
            if(string.isNotEmpty(suppSur.Boutique_Project__c)){
                bp=[Select Furniture_Supplier__c,Boutique__c,Manager_architect__c, Retail_project_manager__r.name,Manager_architect__r.name, BTQ_Location_Address__c, Handover_Date__c From RECO_Project__c where id=:suppSur.Boutique_Project__c];
                b=[Select Concept__c,RecordTypeId,boutique_location_account__r.name,typology__c From Boutique__c where id=:bp.Boutique__c];
                
                List<Handover__c> tempHandList=[Select Visit_date__c, RecordType.Name, Retail_project_manager__c,Retail_project_manager__r.Name,National_boutique_manager__r.name, National_boutique_manager__c From Handover__c where RECO_Project__c=:suppSur.Boutique_Project__c ORDER BY CreatedDate limit 1];
                if(!tempHandList.isEmpty()){
                    ho=tempHandList[0];
                }
            }
        }
         if(String.isNotBlank(accSuppId)){
                suppSEv.Supplier_Name__c=accSuppId;
        }
         reRenderProjectDetails();
    }
    
    
public void reRenderProjectDetails()
{
    if(String.isNotBlank(suppSEv.Boutique_Project__c)){
        bp=[Select Furniture_Supplier__c,Boutique__c,  BTQ_Location_Address__c,Retail_project_manager__r.name,Manager_architect__r.name, Manager_architect__c,Handover_Date__c From RECO_Project__c where id=:suppSEv.Boutique_Project__c];
        b=[Select Concept__c,RecordTypeId,boutique_location_account__r.name,typology__c From Boutique__c where id=:bp.Boutique__c];
    }
}
public void questionsQuery()
{
    List<SupplierSurveyQues__c> suppQuestList=SupplierSurveyQues__c.getall().values();
    List<Supplier_Survey_Evaluation__c> suppSurEvalList=[Select id,Boutique_Project__c,PM_Score__c,Logistics_and_Installation_Score__c,
                                                                        Offers_and_Costs_Score__c,Handover_Score__c,Quality_of_Millwork_Score__c,
                                                                        Timing_Flexibility_Availability_Score__c,SHE_Score__c,(Select id,Answers_score__c,Header_Question__c,
                                                                                        Negative_Comments__c,Positive_Comments__c,Sub_Header_Sequence_No__c,
                                                                                           Sub_Header_Color__c,Supplier_Survey_Evaluation__c,
                                                                                           Survey_Question__c from Supplier_Evaluation_Ans__r order
                                                                                           by Sub_Header_Sequence_No__c) from Supplier_Survey_Evaluation__c
                                                                                           where id=:BouProId];
    
    for(Supplier_Survey_Evaluation__c s : suppSurEvalList)
    { 
        for(Supplier_Evaluation_Ans__c sup : s.Supplier_Evaluation_Ans__r)
        {
            
            String tempAvgScore='0';
            if(sup.Header_Question__c.contains('PROJECT MANAGEMENT')){
                tempAvgScore=String.valueof(s.PM_Score__c);
            }
            else if(sup.Header_Question__c.contains('LOGISTICS')){
                tempAvgScore=String.valueof(s.Logistics_and_Installation_Score__c);
                
            }
            else if(sup.Header_Question__c.contains('OFFERS')){
                tempAvgScore=String.valueof(s.Offers_and_Costs_Score__c);
              
            }
            else if(sup.Header_Question__c.contains('HANDOVER')){
                tempAvgScore=String.valueof(s.Handover_Score__c);
               
            }
            else if(sup.Header_Question__c.contains('MILLWORK')){
                tempAvgScore=String.valueof(s.Quality_of_Millwork_Score__c);
               
            }
            else if(sup.Header_Question__c.contains('TIMING')){
                tempAvgScore=String.valueof(s.Timing_Flexibility_Availability_Score__c);
              
            }
            else if(sup.Header_Question__c.contains('SHE')){
                tempAvgScore=String.valueof(s.SHE_Score__c);
              
            }
            else{
                tempAvgScore='0';
            }
            
            
            
            commentsSurveyMap.put(sup.Header_Question__c,new showNegaPosiComm(tempAvgScore,sup.Positive_Comments__c,sup.Negative_Comments__c,sup.Sub_Header_Color__c));
            List<showSurveyQues> tempshowSurveyQuesWrapList=new List<showSurveyQues>();
            tempshowSurveyQuesWrapList.add(new showSurveyQues(sup.Sub_Header_Sequence_No__c,sup.Survey_Question__c ,String.valueof(sup.Answers_score__c),'',''));
            if(quesSurveyMap.containsKey(sup.Header_Question__c)){
                quesSurveyMap.get(sup.Header_Question__c).addall(tempshowSurveyQuesWrapList);
            }
            else{
                quesSurveyMap.put(sup.Header_Question__c,tempshowSurveyQuesWrapList);
            }
            
            
            
            
           /* commentsSurveyMap.put(sup.Section_Heading__c,new showNegaPosiComm('0','','',sup.Section_Color__c));
            List<showSurveyQues> tempshowSurveyQuesWrapList=new List<showSurveyQues>();
            tempshowSurveyQuesWrapList.add(new showSurveyQues(sup.Name,sup.Survey_Ques__c,'','',''));
            if(quesSurveyMap.containsKey(sup.Section_Heading__c)){
                quesSurveyMap.get(sup.Section_Heading__c).addall(tempshowSurveyQuesWrapList);
            }
            else{
                quesSurveyMap.put(sup.Section_Heading__c,tempshowSurveyQuesWrapList);
            }*/
        }
    }
}


public boolean showError{get;set;}
public pagereference updateSurvey()
{
    showError=false;
    update suppSEv;
    Map<String,Supplier_Evaluation_Ans__c> suppEvalScoreMap=new Map<String,Supplier_Evaluation_Ans__c>();
    Map<String,List<Supplier_Evaluation_Ans__c>> suppEvalCommMap=new Map<String,List<Supplier_Evaluation_Ans__c>>();
    List<Supplier_Evaluation_Ans__c> suppEvalAnsList=[Select id,Negative_Comments__c,Header_Question__c,Header_Avg_Score__c,Positive_Comments__c,Sub_Header_Sequence_No__c,Answers_score__c from Supplier_Evaluation_Ans__c where Supplier_Survey_Evaluation__c=:BouProId];
    
    for(Supplier_Evaluation_Ans__c sea : suppEvalAnsList)
    {
        suppEvalScoreMap.put(sea.Sub_Header_Sequence_No__c,sea);
        
        if(suppEvalCommMap.containsKey(sea.Header_Question__c)){
            suppEvalCommMap.get(sea.Header_Question__c).add(sea);
        }
        else
        {
            suppEvalCommMap.put(sea.Header_Question__c,new List<Supplier_Evaluation_Ans__c>{sea});
        }
    }
    List<Supplier_Evaluation_Ans__c> updateScoreList=new List<Supplier_Evaluation_Ans__c>();
    List<Supplier_Evaluation_Ans__c> updateCommentsList=new List<Supplier_Evaluation_Ans__c>();
    Supplier_Survey_Evaluation__c suppEval=new Supplier_Survey_Evaluation__c(id=BouProId); 
    for(String keyMain : quesSurveyMap.keySet())
    {
    
   
        if(!quesSurveyMap.get(keyMain).isEmpty()){
            List<showSurveyQues> tempWrapperShowSurList=quesSurveyMap.get(keyMain);
            showNegaPosiComm shNegPosComm= commentsSurveyMap.get(keyMain);
            
            
            //suppEval=new Supplier_Survey_Evaluation__c(id=BouProId);    
             system.debug('================avgscore======'+shNegPosComm.avgScore);                   
            if(keyMain.contains('PROJECT MANAGEMENT')){
                system.debug('===============inside=====');
                if(String.isNotBlank(shNegPosComm.avgScore))
                    suppEval.PM_Score__c=decimal.valueof(shNegPosComm.avgScore);
                    system.debug('================avgscore==inseied pm===='+shNegPosComm.avgScore);  
            }
          
            if(keyMain.contains('LOGISTICS')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppEval.Logistics_and_Installation_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
           
            if(keyMain.contains('OFFERS')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppEval.Offers_and_Costs_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
          
            if(keyMain.contains('HANDOVER')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppEval.Handover_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
           
            if(keyMain.contains('MILLWORK')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppEval.Quality_of_Millwork_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
           
            if(keyMain.contains('TIMING')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppEval.Timing_Flexibility_Availability_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }
            
            if(keyMain.contains('SHE')){
                if(String.isNotBlank(shNegPosComm.avgScore))
                suppEval.SHE_Score__c=decimal.valueof(shNegPosComm.avgScore);
            }       
             
            for(showSurveyQues showIns : tempWrapperShowSurList) 
            {
                if(suppEvalScoreMap.containsKey(showIns.serialNoSubques)){
                    Supplier_Evaluation_Ans__c suEvalAnsIns=suppEvalScoreMap.get(showIns.serialNoSubques);
                    //Mansimar -Added zero check in below if to avoid show error 
                    if(String.isNotBlank(showIns.subScore)  &&  Decimal.valueof(showIns.subScore) <>0){
                        if(Decimal.valueof(showIns.subScore)>=1 && Decimal.valueof(showIns.subScore)<=6){
                            suEvalAnsIns.Answers_score__c=decimal.valueof(showIns.subScore);
                        }
                        else{
                            showError=true;
                            return null;
                        }
                    }
                    else{
                        //suEvalAnsIns.Answers_score__c=Decimal.valueof('0');        //Priya : Added to avoid adding 0.0 in all scores
                    }
                    updateScoreList.add(suEvalAnsIns);
                }  
                
                if(suppEvalCommMap.containsKey(keyMain)){
                    List<Supplier_Evaluation_Ans__c> suppEvalAnsList1=suppEvalCommMap.get(keyMain);
                    for(Supplier_Evaluation_Ans__c evalAns:suppEvalAnsList1)
                    {
                        //[START]MANSIMAR - ADDED SERVER SIDE VALIDATION for comments size 
                        //truncate if even after client side validatio size is bigger than 255
                        if(shNegPosComm.negativeComments !=null && shNegPosComm.negativeComments.length() > 255){
                        evalAns.Negative_Comments__c=shNegPosComm.negativeComments.substring(0,255);
                        }
                        else {
                          evalAns.Negative_Comments__c=shNegPosComm.negativeComments;
                        }
                        if(shNegPosComm.positiveComments !=null && shNegPosComm.positiveComments.length() > 255){
                        evalAns.Positive_Comments__c=shNegPosComm.positiveComments.substring(0,255);
                        }
                        else {
                        evalAns.Positive_Comments__c=shNegPosComm.positiveComments;
                        }
                        
                        // [END]MANSIMAR - ADDED SERVER SIDE VALIDATION for comments size
                         //Mansimar - Commneted direct assignment as updated above in server side validation 
                        //evalAns.Negative_Comments__c=shNegPosComm.negativeComments;
                        //evalAns.Positive_Comments__c=shNegPosComm.positiveComments;
                        //if(String.isNotBlank(shNegPosComm.avgScore))
                            //evalAns.Header_Avg_Score__c=decimal.valueof(shNegPosComm.avgScore);
                        updateCommentsList.add(evalAns);
                        
                        
                       
                    }
                }
                
            }
            
        }
    }
    
    if(!updateScoreList.isEmpty()){
        update updateScoreList;
    }
    if(!updateCommentsList.isEmpty()){
        Map<Id,Supplier_Evaluation_Ans__c> deDupAnsMap=new Map<Id,Supplier_Evaluation_Ans__c>();
        for(Supplier_Evaluation_Ans__c su : updateCommentsList)
        {
            deDupAnsMap.put(su.Id,su);
        }
        update deDupAnsMap.values();
    }
    update suppEval;
    system.debug('=============suppEval========'+suppEval);
    
    String returnUrl='';
    returnUrl=ApexPages.currentPage().getParameters().get('retURL');
    if(String.isNotBlank(returnUrl)){
        returnUrl=returnUrl;
    }
    else{
        returnUrl='';
        if(String.isNotBlank(accSuppId)){
            returnUrl='/'+accSuppId;
        }
        else if(String.isNotBlank(suppSEv.Boutique_Project__c)){
            returnUrl='/'+suppSEv.Boutique_Project__c;
        }
        else if(String.isNotBlank(suppSEv.Supplier_Name__c)){
            returnUrl='/'+suppSEv.Supplier_Name__c;
        }
    }
    return new pagereference(returnUrl);
   
    
}

public string keyMain{get;set;}
public void calculateAvgMeth()
{
    system.debug('========keyMain====='+keyMain);
    if(String.isNotBlank(keyMain)){
        List<showSurveyQues> tempWrapperShowSurList=quesSurveyMap.get(keyMain);
        Decimal totalAvg=0;
        Integer totalCountRec=0;
        for(showSurveyQues sup : tempWrapperShowSurList)
        {
            if(String.isNotBlank(sup.subScore)){
                totalCountRec=totalCountRec+1;
                totalAvg=totalAvg+Decimal.valueof(sup.subScore);
            }
        }
        showNegaPosiComm shNegPosComm= commentsSurveyMap.get(keyMain);
        if(totalCountRec!=0)                                 //Priya: Added to solve the Divide by 0 error
        shNegPosComm.avgScore=String.valueof((totalAvg/totalCountRec).setScale(1));
        system.debug('==========shNegPosComm.avgScore====='+shNegPosComm.avgScore);
        commentsSurveyMap.put(keyMain,shNegPosComm);
        //shNegPosComm.avgScore=String.valueof((totalAvg/tempWrapperShowSurList.size()));
        
    }
}

public pagereference backPage()
{
   
    
    
    
    String returnUrl='';
    returnUrl=ApexPages.currentPage().getParameters().get('retURL');
    if(String.isNotBlank(returnUrl)){
        returnUrl=returnUrl;
    }
    else{
        returnUrl='';
        if(String.isNotBlank(accSuppId)){
            returnUrl='/'+accSuppId;
        }
        else if(String.isNotBlank(suppSEv.Boutique_Project__c)){
            returnUrl='/'+suppSEv.Boutique_Project__c;
        }
        else if(String.isNotBlank(suppSEv.Supplier_Name__c)){
            returnUrl='/'+suppSEv.Supplier_Name__c;
        }
    }
    
    
    
    return new pagereference(returnUrl);
}

public List<showSurveyQues> showSurveyQuesWrapList{get;set;}
public class showSurveyQues{
    public String serialNoSubques{get;set;}
    public String subQuestions{get;set;}
    public String subScore{get;set;}
    public String positiveComments{get;set;}
    public String negativeComments{get;set;}
    
    public showSurveyQues(String serialNoSubques,String subQuestions,String subScore,String positiveComments,String negativeComments)
    {
        this.serialNoSubques=serialNoSubques;
        this.subQuestions=subQuestions;
        this.subScore=subScore;
        this.positiveComments=positiveComments;
        this.negativeComments=negativeComments;
    } 
}

public List<showNegaPosiComm> showNegaPosiCommWrapList{get;set;}
public class showNegaPosiComm{
    public String avgScore{get;set;}
    public String positiveComments{get;set;}
    public String negativeComments{get;set;}
    public String color{get;set;}
    
    public showNegaPosiComm(String avgScore,String positiveComments,String negativeComments,String color)
    {
        this.avgScore=avgScore;
        this.positiveComments=positiveComments;
        this.negativeComments=negativeComments;
        this.color=color;
    } 
}



}
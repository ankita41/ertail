public with sharing class RECO_SupplierSurveyReportCls {
    public Supplier_Survey_Evaluation__c suppSuvEval{get;set;}
    public string dateFilter{get;set;}
    
    public RECO_SupplierSurveyReportCls (){
        suppSuvEval=new Supplier_Survey_Evaluation__c ();
    }
    
    public pagereference Redirect2Report()
    {
        String redirectUrl='https://eu2.salesforce.com/00Ob0000003oS5u';
        
        List<Account> accList;
        if(String.isNotBlank(suppSuvEval.Supplier_Name__c) && String.isNotBlank(dateFilter)){
            redirectUrl='https://eu2.salesforce.com/00Ob0000003oS5v';
            accList=[Select id,name from Account where id=:suppSuvEval.Supplier_Name__c];
            redirectUrl=redirectUrl+'?pv1='+accList[0].name+'&colDt_q='+dateFilter;
        }
        if(String.isNotBlank(suppSuvEval.Supplier_Name__c) && String.isBlank(dateFilter)){   
            accList=[Select id,name from Account where id=:suppSuvEval.Supplier_Name__c];
            redirectUrl='https://eu2.salesforce.com/00Ob0000003oS5v';
            redirectUrl=redirectUrl+'?pv1='+accList[0].name;
        }
         if(String.isBlank(suppSuvEval.Supplier_Name__c) && String.isNotBlank(dateFilter)){
            redirectUrl='https://eu2.salesforce.com/00Ob0000003oS5u';
            redirectUrl=redirectUrl+'?colDt_q='+dateFilter;
        }
       
        return new pagereference(redirectUrl);
    }
}
/** @Class      :   Reco_CopyCostItemsCntlr.cls
*   @Description:   Approve Estimate Items on Reco Project
*   @Author     :   Himanshu Palerwal
*   @Created    :  15 Dec 2015
*

*                         
*****/
public with sharing class Reco_CopyCostItemsCntlr {
    public RECO_Project__c recoProjectIns{get;set;}
    public boolean showError{get;set;}
    public string selectedVal{get;set;}
    public List<Estimate_Item__c> estiItemList{get;set;}
    public Reco_CopyCostItemsCntlr(ApexPages.StandardController controller) {
        showError=false;
        recoProjectIns= (RECO_Project__c)controller.getRecord();
        estiItemList=new List<Estimate_Item__c>();
        wrapperEstimateList=new List<wrapperEstimateItemsShow>();
        showEstimatedItems();
        selectedVal = 'No';
    }
    
    
    public void showEstimatedItems()
    {
        estiItemList=[Select e.SystemModstamp, e.RecordTypeId, e.RECO_Project__c, e.Project_Currency__c,
                        e.Private__c, e.Percentage_Total_cost__c, e.Name, e.Level_3__c, e.Level_2__c,
                        e.Level_1__c, e.LastViewedDate, e.LastReferencedDate, e.LastModifiedDate,
                        e.LastModifiedById, e.IsPrivate__c, e.IsDeleted, e.Id, e.Estimated_Item__c,
                        e.Do_Not_Convert__c, e.Description__c, e.Currency__c, e.CreatedDate, e.CreatedById,
                        e.Cost_m2_local__c, e.Cost_m2_EUR__c, e.Cost_m2_CHF__c, e.Amount_local__c, e.Amount__c,
                        e.Amount_EUR__c, e.Amount_CHF__c From Estimate_Item__c e where RECO_Project__c=:recoProjectIns.id];
        for(Estimate_Item__c est: estiItemList)
        {
            wrapperEstimateList.add(new wrapperEstimateItemsShow(est,false));
        }
    }
    
    public void clickToSelectAll() {
        if(wrapperEstimateList <> null && wrapperEstimateList.size() <> 0) {
            for(wrapperEstimateItemsShow wrapObj:wrapperEstimateList) {
                wrapObj.isSelected = true; 
            }
        }    
    }
    
    public void clickToSelectNone() {
        if(wrapperEstimateList <> null && wrapperEstimateList.size() <> 0) {
            for(wrapperEstimateItemsShow wrapObj:wrapperEstimateList) {
                wrapObj.isSelected = false; 
            }
        }    
    }
    public void copyCostItems() {
        showError=false;
        try {
            List<Id> selEstimaItemListId=new List<Id>();
            List<Id> allEstimaItemListId=new List<Id>(); // RM0022143917 (all estimate items should be approved regardless of any selection)
            for(wrapperEstimateItemsShow wr :wrapperEstimateList) {
                allEstimaItemListId.add(wr.estimateItem.id);
                if(wr.isSelected==true){
                    selEstimaItemListId.add(wr.estimateItem.id);
                } 
            }
            if(selectedVal=='No'){
                // RM0022143917 (all estimate items should be approved regardless of any selection)
                Reco_CopyCostItmfromEstimatedItmHlpr.createCostItems(recoProjectIns.id,selectedVal,selEstimaItemListId,allEstimaItemListId);   
                
            } else {
                if(selEstimaItemListId.isEmpty()){
                   showError=true;
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select at least 1 Estimated Item for which information to be copied in to respective Cost Item OR Select "No" From "Copy Information To Cost Items" dropdown.')); 
                } else {
                    // RM0022143917 (all estimate items should be approved regardless of any selection)
                    Reco_CopyCostItmfromEstimatedItmHlpr.createCostItems(recoProjectIns.id,selectedVal,selEstimaItemListId,allEstimaItemListId);
                }
            }
         } catch(Exception ex){
             ApexPages.addMessages(ex);
         }
    }
    
    public list<wrapperEstimateItemsShow> wrapperEstimateList{get;set;}
    public class wrapperEstimateItemsShow
    {
        public Estimate_Item__c estimateItem{get;set;}
        public boolean isSelected{get;set;}
        public wrapperEstimateItemsShow(Estimate_Item__c estimateItem,boolean isSelected)
        {
            this.estimateItem=estimateItem;
            this.isSelected=isSelected;
        }
    }
}
/** @Class      :   Reco_CopyCostItemsCntlrTest.cls
*   @Description:   Test Class for Reco_CopyCostItemsCntlr
*   @Author     :   Himanshu Palerwal
*   @Created    :  15 Dec 2015
*

*                         
*****/
@isTest
private class Reco_CopyCostItemsCntlrTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.Net_selling_m2__c = 100;
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        List<RECO_Project__c> newBProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        for(RECO_Project__c prj : newBProjs){
            prj.Cutoff_amount_local_year1__c = 100;
            prj.Cutoff_amount_local_year2__c = 100;
            prj.CapEx_amount_local__c = 100; 
            prj.DF_CapEx_amount_local__c = 100;
        }
        insert newBProjs;
        
        
        rTypeId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        List<Estimate_Item__c> estimaItems=TestHelper.createEstimateItems(rTypeId,newBProjs[0].Id);
        insert estimaItems;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(newBProjs[0]);
        Reco_CopyCostItemsCntlr recoItemsIns=new Reco_CopyCostItemsCntlr(sc);
        recoItemsIns.estiItemList.addall(estimaItems);
        Reco_CopyCostItemsCntlr.wrapperEstimateItemsShow wr=new Reco_CopyCostItemsCntlr.wrapperEstimateItemsShow(estimaItems[0],true);
        recoItemsIns.wrapperEstimateList.add(new Reco_CopyCostItemsCntlr.wrapperEstimateItemsShow(estimaItems[0],true));
        recoItemsIns.showEstimatedItems();
        recoItemsIns.clickToSelectNone();
        recoItemsIns.clickToSelectAll();
        recoItemsIns.selectedVal='No';
        recoItemsIns.copyCostItems();
        recoItemsIns.selectedVal='Yes - Copy only description';
        recoItemsIns.copyCostItems();
        recoItemsIns.selectedVal='Yes - Copy amount and description';
        recoItemsIns.copyCostItems();
    }
}
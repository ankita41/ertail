/** @Class      :   Reco_CopyCostItmfromEstimatedItmHlpr.cls
*   @Description:   Helper class for Reco_CopyCostItmfromEstimatedItems
*   @Author     :   Himanshu Palerwal
*   @Created    :   15 Dec 2015
*    @update    :   6-6-2016, Deepak mehta - RM0022143917 (all estimate items should be approved regardless of any selection) 
       
*****/

public with sharing class Reco_CopyCostItmfromEstimatedItmHlpr {
    // RM0022143917 (all estimate items should be approved regardless of any selection)
    public static void createCostItems(String Id,String selectedVal,List<Id> estimateItemIds,List<Id> allestimateItemIds)
    {
        try{
            // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (commented below variable and added new)
           //String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
           Boolean isNcafeProject ;
           Id approvedRTId;
           
            // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (commented below code) 
           /*if(currentUserProfile.contains('NCAFE')){
             approvedRTId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('NCAFE Approved').getRecordTypeId();
           }
           else
           {
             approvedRTId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
           }*/
            List<Cost_Item__c> cstItemInsertList=new List<Cost_Item__c>();
            if(!estimateItemIds.isEmpty()){
                
                List<Estimate_Item__c> estimateItemList=[Select Id, Name, RecordTypeId, RECO_PRoject__R.RecordType.name,
                                                                    RECO_Project__c, Amount_CHF__c, Amount_EUR__c, Amount_local__c, Cost_m2_CHF__c, 
                                                                    Cost_m2_EUR__c, Cost_m2_local__c, Description__c, Do_Not_Convert__c, Estimated_Item__c, 
                                                                    Level_1__c, Level_2__c, Level_3__c, Percentage_Total_cost__c, Amount__c, Currency__c, 
                                                                    IsPrivate__c, Project_Currency__c, Private__c From Estimate_Item__c where RECO_Project__c=:id 
                                                                    and id in : estimateItemIds]; 
               
                // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType - Start
                isNcafeProject=estimateItemList.get(0).RECO_PRoject__R.recordtype.name.contains('NCafe');
                
                if(isNcafeProject) { 
                    approvedRTId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('NCAFE Approved').getRecordTypeId();
                    }
                     else
                    {
                    approvedRTId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
                    }
                // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType - End
                
                if(selectedVal=='No') {
                    for(Estimate_Item__c esItemRec: estimateItemList) {
                        Cost_Item__c cstItem=new Cost_Item__c();
                        cstItem.RECO_Project__c=Id;
                                               
                        // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (Changed below if condition)
                        //if(currentUserProfile.contains('NCAFE')){
                        if(isNcafeProject){    
                            cstItem.recordTypeId=Schema.SObjectType.Cost_Item__c.RecordTypeInfosByName.get('NCAFE Expected').RecordTypeId;
                        }
                        else
                        {
                            cstItem.recordTypeId=Schema.SObjectType.Cost_Item__c.RecordTypeInfosByName.get('Expected').RecordTypeId;
                        }
                        cstItem.Level_1__c=esItemRec.Level_1__c;
                        cstItem.Level_2__c=esItemRec.Level_2__c;
                        cstItem.private__c=esItemRec.private__c;
                        cstItem.Expected_amount__c=0.0;                // Priya : Added for RM0021508505
                        cstItem.Currency__c=esItemRec.Currency__c;     // Priya : Added for RM0021508505
                        cstItemInsertList.add(cstItem);
                    }
                    
                }  else if(selectedVal=='Yes - Copy only description') {
                       for(Estimate_Item__c esItemRec: estimateItemList) {
                            Cost_Item__c cstItem=new Cost_Item__c();
                            cstItem.RECO_Project__c=Id;
                           
                            //Mansimar-commented userprofile and added project variable check
                            //if(currentUserProfile.contains('NCAFE')){
                           if(isNcafeProject){   
                               cstItem.recordTypeId=Schema.SObjectType.Cost_Item__c.RecordTypeInfosByName.get('NCAFE Expected').RecordTypeId;
                            }
                            else
                            {
                                cstItem.recordTypeId=Schema.SObjectType.Cost_Item__c.RecordTypeInfosByName.get('Expected').RecordTypeId;
                            }
                            cstItem.Level_1__c=esItemRec.Level_1__c;
                            cstItem.Level_2__c=esItemRec.Level_2__c;
                            cstItem.Description__c=esItemRec.Description__c;
                            cstItem.private__c=esItemRec.private__c;
                            cstItem.Expected_amount__c=0.0;                        // Priya : Added for RM0021508505
                            cstItem.Currency__c=esItemRec.Currency__c;             // Priya : Added for RM0021508505
                            cstItemInsertList.add(cstItem);
                        }
                 }  else if(selectedVal=='Yes - Copy amount and description') {
                       for(Estimate_Item__c esItemRec: estimateItemList) {
                            Cost_Item__c cstItem=new Cost_Item__c();
                            cstItem.RECO_Project__c=Id;
                            // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (Changed below if condition)
                            //if(currentUserProfile.contains('NCAFE')){
                           if(isNcafeProject){   
                                cstItem.recordTypeId=Schema.SObjectType.Cost_Item__c.RecordTypeInfosByName.get('NCAFE Expected').RecordTypeId;
                            }
                            else
                            {
                                cstItem.recordTypeId=Schema.SObjectType.Cost_Item__c.RecordTypeInfosByName.get('Expected').RecordTypeId;
                            }
                            cstItem.Level_1__c=esItemRec.Level_1__c;
                            cstItem.Level_2__c=esItemRec.Level_2__c;
                            cstItem.Description__c=esItemRec.Description__c;
                            cstItem.private__c=esItemRec.private__c;
                            cstItem.Expected_amount__c=esItemRec.Amount__c;
                            cstItem.Currency__c=esItemRec.Currency__c;
                            cstItemInsertList.add(cstItem);
                        }
                  }                                                       
                
                if(!cstItemInsertList.isEmpty()){
                    insert cstItemInsertList;
                }
            }
            // RM0022143917 (all estimate items should be approved regardless of any selection)    
            List<Estimate_Item__c> tmpList = [SELECT Id, RecordTypeId FROM Estimate_Item__c WHERE RECO_Project__c = :Id AND RecordTypeId <> :approvedRTId
                                                     and id in : allestimateItemIds];
            for (Estimate_Item__c item : tmpList){
                item.RecordTypeId = approvedRTId;
            }
            update tmpList;
            
            RECO_Project__c proj=new RECO_Project__c(id=id);
            proj.Cost_estimate_approval_status__c = 'Approved';
            proj.Cost_estimate_approval_date__c = SYSTEM.Now();
            proj.Estimated_cost_approved_by__c= UserInfo.getUserId();
            proj.Process_step__c = 'Project Development';
            update proj;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
         }
    }
}
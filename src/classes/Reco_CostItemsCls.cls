/*****
*   @Class      :   Reco_CostItemsCls.cls
*   @Description:   Apex Class for Reco_CostItems
*                   
*   @Author     :   Himanshu Palerwal
*   @Created    :   02 NOV 2015
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
     
***/
public with sharing class Reco_CostItemsCls {
    private RECO_Project__c boutPro;
    public string selectedItemId{get;set;} 
    public Integer sizeList{get;set;}
    public string val{get;set;}
    public Reco_CostItemsCls(ApexPages.StandardController controller) {
         boutPro= (RECO_Project__c)controller.getRecord();
         val=ApexPages.currentPage().getParameters().get('val');
    }
 public List<Cost_Item__c> getEstimatedItems() 
 {
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        Boolean whereCondition;
        String boutiqueID=boutPro.id;
        if(profileName!=Label.Design_Architect_Profile && profileName!=Label.Furniture_Supplier_Profile && profileName!=Label.Manager_Architect_Profile ){
             
            if(String.isBlank(val)){                      
                 return database.query('Select c.Validated_amount_local__c, c.Validated_amount__c, c.Validated_amount_EUR__c, c.Validated_amount_CHF__c,'+ 
                                    'c.Valid__c, c.Unique_External_Id__c, c.SystemModstamp, c.Supplier__c, c.Start_date__c, c.RecordTypeId,' +
                                    'c.RECO_Project__c, c.Percentage_Total_cost__c, c.Offer__c, c.Offer_Approved_Year__c, c.Name, c.Local_Currency__c,'+ 
                                    'c.Level_3__c, c.Level_2__c, c.Level_1__c, c.LastViewedDate, c.LastReferencedDate, c.LastModifiedDate,c.RecordType.Name, '+
                                    'c.LastModifiedById, c.LastActivityDate, c.IsPrivate__c, c.IsDeleted, c.Id, c.Extra_cost__c, c.Expected_amount_local__c, '+
                                    'c.Expected_amount__c, c.Expected_amount_EUR__c, c.Expected_amount_CHF__c, c.End_delivery_date__c, c.Do_Not_Convert__c, '+
                                    'c.Description__c, c.Cutoff_Years__c, c.Currency__c, c.CreatedDate, c.CreatedById, c.Cost_m2_local__c, c.Cost_m2_EUR__c, '+
                                    'c.Cost_m2_CHF__c, c.Cost_Item__c,private__c, c.Amount__c From Cost_Item__c c where RECO_Project__c=:boutiqueID limit 5');
            }
            else{
                return database.query('Select c.Validated_amount_local__c, c.Validated_amount__c, c.Validated_amount_EUR__c, c.Validated_amount_CHF__c,'+ 
                                    'c.Valid__c, c.Unique_External_Id__c, c.SystemModstamp, c.Supplier__c, c.Start_date__c, c.RecordTypeId,' +
                                    'c.RECO_Project__c, c.Percentage_Total_cost__c, c.Offer__c, c.Offer_Approved_Year__c, c.Name, c.Local_Currency__c,'+ 
                                    'c.Level_3__c, c.Level_2__c, c.Level_1__c, c.LastViewedDate, c.LastReferencedDate, c.LastModifiedDate,c.RecordType.Name, '+
                                    'c.LastModifiedById, c.LastActivityDate, c.IsPrivate__c, c.IsDeleted, c.Id, c.Extra_cost__c, c.Expected_amount_local__c, '+
                                    'c.Expected_amount__c, c.Expected_amount_EUR__c, c.Expected_amount_CHF__c, c.End_delivery_date__c, c.Do_Not_Convert__c, '+
                                    'c.Description__c, c.Cutoff_Years__c, c.Currency__c, c.CreatedDate, c.CreatedById, c.Cost_m2_local__c, c.Cost_m2_EUR__c, '+
                                    'c.Cost_m2_CHF__c, c.Cost_Item__c,private__c, c.Amount__c From Cost_Item__c c where RECO_Project__c=:boutiqueID');
            
            }
        }
        else{
             whereCondition=false;
             
             
             if(String.isBlank(val)){
                 return database.query('Select c.Validated_amount_local__c, c.Validated_amount__c, c.Validated_amount_EUR__c, c.Validated_amount_CHF__c,'+ 
                                    'c.Valid__c, c.Unique_External_Id__c, c.SystemModstamp, c.Supplier__c, c.Start_date__c, c.RecordTypeId,' +
                                    'c.RECO_Project__c, c.Percentage_Total_cost__c, c.Offer__c, c.Offer_Approved_Year__c, c.Name, c.Local_Currency__c,'+ 
                                    'c.Level_3__c, c.Level_2__c, c.Level_1__c, c.LastViewedDate, c.LastReferencedDate, c.LastModifiedDate,c.RecordType.Name, '+
                                    'c.LastModifiedById, c.LastActivityDate, c.IsPrivate__c, c.IsDeleted, c.Id, c.Extra_cost__c, c.Expected_amount_local__c, '+
                                    'c.Expected_amount__c, c.Expected_amount_EUR__c, c.Expected_amount_CHF__c, c.End_delivery_date__c, c.Do_Not_Convert__c, '+
                                    'c.Description__c, c.Cutoff_Years__c, c.Currency__c, c.CreatedDate, c.CreatedById, c.Cost_m2_local__c, c.Cost_m2_EUR__c, '+
                                    'c.Cost_m2_CHF__c, c.Cost_Item__c,private__c, c.Amount__c From Cost_Item__c c where RECO_Project__c=:boutiqueID and private__c=:whereCondition limit 5');
             }
             else{
                 return database.query('Select c.Validated_amount_local__c, c.Validated_amount__c, c.Validated_amount_EUR__c, c.Validated_amount_CHF__c,'+ 
                                    'c.Valid__c, c.Unique_External_Id__c, c.SystemModstamp, c.Supplier__c, c.Start_date__c, c.RecordTypeId,' +
                                    'c.RECO_Project__c, c.Percentage_Total_cost__c, c.Offer__c, c.Offer_Approved_Year__c, c.Name, c.Local_Currency__c,'+ 
                                    'c.Level_3__c, c.Level_2__c, c.Level_1__c, c.LastViewedDate, c.LastReferencedDate, c.LastModifiedDate,c.RecordType.Name, '+
                                    'c.LastModifiedById, c.LastActivityDate, c.IsPrivate__c, c.IsDeleted, c.Id, c.Extra_cost__c, c.Expected_amount_local__c, '+
                                    'c.Expected_amount__c, c.Expected_amount_EUR__c, c.Expected_amount_CHF__c, c.End_delivery_date__c, c.Do_Not_Convert__c, '+
                                    'c.Description__c, c.Cutoff_Years__c, c.Currency__c, c.CreatedDate, c.CreatedById, c.Cost_m2_local__c, c.Cost_m2_EUR__c, '+
                                    'c.Cost_m2_CHF__c, c.Cost_Item__c,private__c, c.Amount__c From Cost_Item__c c where RECO_Project__c=:boutiqueID and private__c=:whereCondition');
             }
        }
 }
 public pageReference deleteItem()
 {
    try {
            delete [select id from Cost_Item__c where id=:selectedItemId];
            return new pageReference('/apex/RECOProject?id='+boutPro.id);
    }
    catch(dmlexception e) { }
    return null;
 }
}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Reco_CostItemsClsTest {

    public static testMethod void myUnitTestAdmin() {
        // TO DO: implement unit test 
        
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.Net_selling_m2__c = 100;
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        List<RECO_Project__c> newBProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        for(RECO_Project__c prj : newBProjs){
            prj.Cutoff_amount_local_year1__c = 100;
            prj.Cutoff_amount_local_year2__c = 100;
            prj.CapEx_amount_local__c = 100; 
            prj.DF_CapEx_amount_local__c = 100;
        }
        insert newBProjs;
        
        rTypeId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        List<Estimate_Item__c> estimaItems=TestHelper.createEstimateItems(rTypeId,newBProjs[0].Id);
        insert estimaItems;
        
        ApexPages.StandardController sc = new ApexPages.standardController(newBProjs[0]);
        Reco_CostItemsCls reCls=new Reco_CostItemsCls(sc);
        reCls.val='test';
        reCls.getEstimatedItems();
        reCls.deleteItem();
        
        reCls.val='';
        reCls.getEstimatedItems();
    }
    
     public static testMethod void myUnitTestProfile() {
        // TO DO: implement unit test
         List<RECO_Project__c> newBProjs=new List<RECO_Project__c>();
         User u1 = new User();
        System.runAs(new User(Id = Userinfo.getUserId())) {
        Test.startTest();
         Profile p = [SELECT Id FROM Profile WHERE Name='NES RECO Design Architect'];
        
         u1 = new User(Alias = 'standt1',Country='United Kingdom',Email='demo1@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='dprobertdemo1@camfed.org');
        insert u1;
        Test.stopTest();
        
         
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.Net_selling_m2__c = 100;
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        newBProjs = TestHelper.createBoutiqueProjects(rTypeId, btq.Id);
        for(RECO_Project__c prj : newBProjs){
            prj.Cutoff_amount_local_year1__c = 100;
            prj.Cutoff_amount_local_year2__c = 100;
            prj.CapEx_amount_local__c = 100; 
            prj.DF_CapEx_amount_local__c = 100;
        }
        insert newBProjs;
        
        rTypeId = Schema.sObjectType.Estimate_Item__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        List<Estimate_Item__c> estimaItems=TestHelper.createEstimateItems(rTypeId,newBProjs[0].Id);
        insert estimaItems;
        }
        System.runAs(u1){
	        ApexPages.StandardController sc = new ApexPages.standardController(newBProjs[0]);
	        Reco_CostItemsCls reCls=new Reco_CostItemsCls(sc);
	        reCls.val='test';
	        reCls.getEstimatedItems();
	        reCls.deleteItem();
	        
	        reCls.val='';
	        reCls.getEstimatedItems();
        }
    }
}
/** @Class      :   RECO_CreateSupplierCntlr
*   @Description:   Creater supplier using Force.com site
*   @Author     :   Himanshu Palerwal
*   @Created    :   3rd August 2016
*   @Updated    :   Priya : Added Code for Ncafe supplier creation form

*                         
*****/

@isTest
private class Reco_CreateSupplierCntlrTest{

    static testMethod void myUnitTest() {
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        market.Currency__c = 'PHP';
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        List<Contact> conULi=new List<Contact>();
        Contact conRec=TestHelper.createContact();
        conRec.accountId=acc.Id;
        conRec.email='test@gmail.com';
        conRec.firstname='Test First Name';
        conRec.lastname='Test last Name';
        conRec.phone='99999';
        upsert conRec;
        conULi.add(conRec);
        
        ApexPages.currentPage().getParameters().put('userid', userinfo.getuserId());
        Reco_CreateSupplierCntlr sendCls=new Reco_CreateSupplierCntlr();
        sendCls.addContact();
        sendCls.companyName='Test Company Name';
        sendCls.streetNo='124 Test street';
        sendCls.city='Test Company Name';
        sendCls.country='Test Company Name';
        sendCls.postalCode='Test Company Name';
        sendCls.mainPhone='9999';
        sendCls.contactList=conULi;
        sendCls.insertSupplierAccount();
    }
    
}
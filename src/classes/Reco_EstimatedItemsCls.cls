/*****
*   @Class      :   Reco_EstimatedItemsCls.cls
*   @Description:   Apex Class for Reco_EstimatedItems
*                                       
*   @Author     :   Himanshu Palerwal
*   @Created    :   30 OCT 2015
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
     
***/
public with sharing class Reco_EstimatedItemsCls {
    private RECO_Project__c boutPro;
    public string selectedItemId{get;set;}
    public Integer sizeList{get;set;}
    public string val{get;set;}
    public Reco_EstimatedItemsCls(ApexPages.StandardController controller) {
        boutPro= (RECO_Project__c)controller.getRecord();
        val=ApexPages.currentPage().getParameters().get('val');

    }
    
    
    public List<Estimate_Item__c> getEstimatedItems() 
    {
        sizeList=0;
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        Boolean whereCondition;
        String boutiqueID=boutPro.id;
        if(profileName!=Label.Design_Architect_Profile && profileName!=Label.Furniture_Supplier_Profile && profileName!=Label.Manager_Architect_Profile ){
                
        
            if(String.isBlank(val)){
                 return database.query('Select e.SystemModstamp, e.RecordTypeId, e.RECO_Project__c, e.Project_Currency__c,' +
                           'e.Private__c, e.Percentage_Total_cost__c, e.Name, e.Level_3__c, e.Level_2__c,'+
                           'e.Level_1__c, e.LastViewedDate, e.LastReferencedDate, e.LastModifiedDate,'+
                           'e.LastModifiedById, e.IsPrivate__c, e.IsDeleted, e.Id, e.Estimated_Item__c,'+ 
                           'e.Do_Not_Convert__c, e.Description__c, e.Currency__c, e.CreatedDate, e.CreatedById,'+
                           'e.Cost_m2_local__c, e.Cost_m2_EUR__c, e.Cost_m2_CHF__c, e.Amount_local__c, e.Amount__c,'+ 
                           'e.Amount_EUR__c, e.Amount_CHF__c From Estimate_Item__c e where RECO_Project__c=:boutiqueID limit 5');          
           }
           else{
               return database.query('Select e.SystemModstamp, e.RecordTypeId, e.RECO_Project__c, e.Project_Currency__c,' +
                           'e.Private__c, e.Percentage_Total_cost__c, e.Name, e.Level_3__c, e.Level_2__c,'+
                           'e.Level_1__c, e.LastViewedDate, e.LastReferencedDate, e.LastModifiedDate,'+
                           'e.LastModifiedById, e.IsPrivate__c, e.IsDeleted, e.Id, e.Estimated_Item__c,'+ 
                           'e.Do_Not_Convert__c, e.Description__c, e.Currency__c, e.CreatedDate, e.CreatedById,'+
                           'e.Cost_m2_local__c, e.Cost_m2_EUR__c, e.Cost_m2_CHF__c, e.Amount_local__c, e.Amount__c,'+ 
                           'e.Amount_EUR__c, e.Amount_CHF__c From Estimate_Item__c e where RECO_Project__c=:boutiqueID');      
           
           }
        }
        else{
            whereCondition=false;
            if(String.isBlank(val)){
                return database.query('Select e.SystemModstamp, e.RecordTypeId, e.RECO_Project__c, e.Project_Currency__c,' +
                       'e.Private__c, e.Percentage_Total_cost__c, e.Name, e.Level_3__c, e.Level_2__c,'+
                       'e.Level_1__c, e.LastViewedDate, e.LastReferencedDate, e.LastModifiedDate,'+
                       'e.LastModifiedById, e.IsPrivate__c, e.IsDeleted, e.Id, e.Estimated_Item__c,'+ 
                       'e.Do_Not_Convert__c, e.Description__c, e.Currency__c, e.CreatedDate, e.CreatedById,'+
                       'e.Cost_m2_local__c, e.Cost_m2_EUR__c, e.Cost_m2_CHF__c, e.Amount_local__c, e.Amount__c,'+ 
                       'e.Amount_EUR__c, e.Amount_CHF__c From Estimate_Item__c e where RECO_Project__c=:boutiqueID and private__c=:whereCondition limit 5');
            }
            else{
                 return database.query('Select e.SystemModstamp, e.RecordTypeId, e.RECO_Project__c, e.Project_Currency__c,' +
                           'e.Private__c, e.Percentage_Total_cost__c, e.Name, e.Level_3__c, e.Level_2__c,'+
                           'e.Level_1__c, e.LastViewedDate, e.LastReferencedDate, e.LastModifiedDate,'+
                           'e.LastModifiedById, e.IsPrivate__c, e.IsDeleted, e.Id, e.Estimated_Item__c,'+ 
                           'e.Do_Not_Convert__c, e.Description__c, e.Currency__c, e.CreatedDate, e.CreatedById,'+
                           'e.Cost_m2_local__c, e.Cost_m2_EUR__c, e.Cost_m2_CHF__c, e.Amount_local__c, e.Amount__c,'+ 
                           'e.Amount_EUR__c, e.Amount_CHF__c From Estimate_Item__c e where RECO_Project__c=:boutiqueID and private__c=:whereCondition');      
            
            }
        }
    }
    
    public pageReference deleteItem()
    {
        try{
                delete [select id from Estimate_Item__c where id=:selectedItemId];
                return new pageReference('/apex/RECOProject?id='+boutPro.id);
        }
                catch(dmlexception e) { }
                return null;
    }
    
    public void redirectToStandRList()
    {
        
    }
    
    public pagereference ManageEstimatedItems()
    {
        return new pagereference('/apex/ManageEstimateItems?id='+boutPro.Id+'&obj=Estimate_Item__c&order=CreatedDate');
        
    }
}
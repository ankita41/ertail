/** @Class      :   Reco_SavePDFToFolder
*   @Description:   Class used to save pdf in Budget Overview folder
*   @Author     :   Himanshu Palerwal
*   @Created    :   17 Dec 2015
*

*                         
*****/

public with sharing class Reco_SavePDFToFolder {

@future(callout=true)
public static void createPDF(String parentId)
{
        Attachment_Folder__c attFol=[Select id from Attachment_Folder__c where RECO_Project__c=:parentId and name=:'Budget & Offer & Cost'];
        Pagereference budgetPDF = Page.RECO_BudgetOverviewDetailsPdf;
        budgetPDF.getParameters().put('id',parentId);
                
        FeedItem newFile=new FeedItem();
        newFile = new FeedItem();
        if(!Test.isRunningTest()){
            newFile.ContentData = budgetPDF.getContent();
        }
        else{
            newFile.ContentData=blob.valueof('test');
        }
        newFile.ContentFileName = 'BudgetOverviewPDF '+Date.today().day()+'-'+Date.today().month()+'-'+Date.today().year()+'.pdf';
        newFile.ParentId = attFol.id;
        insert newFile;
}
}
/** @Class      :   RECO_RejectSupplier
*   @Description:   Class used to create Supplier record in Account object
*   @Author     :   Himanshu Palerwal
*   @Created    :   15 Jan 2015
*

*                         
*****/
public with sharing class Reco_SendSupplierEmailCls {
public List<Contact> conList{get;set;}
public boolean showSection{get;set;}
public boolean InfoSent{get;set;}
public String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
public boolean reco{get;set;}
public boolean Ncafe{get;set;}

public Reco_SendSupplierEmailCls ()
{
    if(currentUserProfile.contains('NCAFE')){
        reco=false;
        ncafe=true;
    }
    else{
        reco=true;
        ncafe=false;
    }
    conList=new List<Contact>();
    if(!Test.isRunningTest()){
        conList.add(new contact());
    }
     showSection=true;
    InfoSent=false;
}
public void addContact() 
{
    conList.add(new contact());
}
  public void removeContact(){
      if(conList.size()>1){
          conList.remove(conList.size()-1);          
      }
} 


public pagereference sendSupplierEmail()
{
    String insUrl=System.Url.getSalesforceBaseURL().toExternalForm();
    String userName = UserInfo.getUserName();
    User activeUser = [Select Email,name From User where Username = : userName limit 1];
    String userEmail = activeUser.Email;
    EmailTemplate et;
    if(currentUserProfile.contains('NCAFE')) {
       et = [SELECT Id, HtmlValue,BrandTemplateId, Subject FROM EmailTemplate WHERE DeveloperName = :'NCAFE_sendformtosupplier'];
    }
    else {
       et = [SELECT Id, HtmlValue,BrandTemplateId, Subject FROM EmailTemplate WHERE DeveloperName = :'RECO_sendformtosupplier'];
    }   
     /** Fetching Header of template starts here****/   
                
        BrandTemplate brand_value=[Select value from BrandTemplate  where id=:et.BrandTemplateId];
        String bHeader,bFooter,brandvalue=brand_value.value;
        integer i=brandvalue.indexOf('<img');
        integer j=brandvalue.indexOf('</img>');
        bHeader=brandvalue.subString(i,j+6);
        bHeader= bHeader.replace('">','" src=');
        if(bHeader.contains('https')){
            bHeader=bHeader.replace('<![CDATA[','\"');
        }
        else{
            bHeader=bHeader.replace('<![CDATA[','\"'+insUrl);
        }
        bHeader=bHeader.replace(']]','\" ');
        /** Fetching Header of template ends here****/ 
    List<Messaging.Singleemailmessage> emailList=new List<Messaging.Singleemailmessage>();
    for(Contact con : conList){
        if(String.isNotBlank(con.firstname) && String.isNotBlank(con.email)){
            if(validateEmail(con.email)==true){
                Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
                String htmlBody = et.HtmlValue;
                if (htmlBody != null) {
                        htmlBody = htmlBody.replace('{!Contact.Name}', con.firstname);  
                        //htmlBody = htmlBody.replace('{!Contact.Link}', 'http://uat-nespresso.cs87.force.com/CreateSupplier?userId='+userinfo.getuserId());
                        //htmlBody = htmlBody.replace('{!Contact.Link}', '</b> <a href="http://uat-nespresso.cs87.force.com/CreateSupplier?userId'+'='+userinfo.getuserId()+'">'+'Click here </a> ');
                        htmlBody = htmlBody.replace('{!Contact.Link}', '</b> <a href="'+System.Label.Force_com_site_URL+'='+userinfo.getuserId()+'">'+'Click here </a> ');  
                        htmlBody = htmlBody.replace('{!RECO_Project__c.Retail_project_manager__r.Name}',activeUser.name);
                        htmlBody = htmlBody.replace('{!RECO_Project__c.Retail_project_manager__r.Email}', userEmail);
                        htmlBody=htmlBody.replace('<![CDATA[','');
                        htmlBody = htmlBody.replace(']]>', '');
                }
                 bHeader='<style>p{margin-top:0px; margin-bottom:0px;}</style><body class="setupTab"  style=" background-color:#CCCCCC; bEditID:bHeaderst1; bLabel:body;"><center ><table cellpadding="0" width="500" cellspacing="0" id="topTable" height="450" ><tr valign="top" ><td  style=" background-color:#FFFFFF; bEditID:r1st1; bLabel:header; vertical-align:top; height:100; text-align:left;">'+bHeader+'</td></tr><tr valign="top" ><td  style=" background-color:#330000; bEditID:r2st1; bLabel:accent1; height:1;"></td></tr>';
                 bHeader+='<tr valign="top" ><td styleInsert="1" height="300"  style=" background-color:#FFFFFF; bEditID:r3st1; color:#000000; bLabel:main; font-size:12pt; font-family:arial;">';
                 bFooter='</td></tr><tr valign="top" ><td  style=" background-color:#330000; bEditID:r4st1; bLabel:accent2; height:1;"></td></tr><tr valign="top" ><td  style=" background-color:#FFFFFF; bEditID:r5st1; bLabel:footer; vertical-align:top; height:100; text-align:left;"></td></tr><tr valign="top" ><td  style=" background-color:#330000; bEditID:r6st1; bLabel:accent3; height:1;"></td></tr></table></center><br><br>';
                 htmlBody=bHeader+htmlBody+bFooter;
                if(!bHeader.contains('https')){
                    
                }
                
                
                // Use Organization Wide Address  
                List<OrgWideEmailAddress> owa = [select id, Address,DisplayName from OrgWideEmailAddress where address=:userEmail];
                if(!owa.isEmpty()) {
                     mail.setOrgWideEmailAddressId(owa[0].id);
                } 
                else{
                     List<OrgWideEmailAddress> owaHi = [select id, Address,DisplayName from OrgWideEmailAddress where address=:'retail@nespresso.com'] ;
                     mail.setOrgWideEmailAddressId(owaHi[0].id);
                }
                
                mail.setHtmlBody(htmlBody);
                mail.setToAddresses(new list<string>{con.email});
                if(currentUserProfile.contains('NCAFE')){
                    mail.setSubject('Ncafe Supplier Registration Request');
                }
                else{
                    mail.setSubject('Nespresso Supplier Registration Request');
                }
                emailList.add(mail);
            }
            else{
                 ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter email in valid format!');
                ApexPages.addMessage(myMsg); 
                return null;
            }
       }
       else{
           ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'Please fill required fields!');
            ApexPages.addMessage(myMsg); 
            return null;
       }
    }
    if(!emailList.isEmpty()){
        Messaging.sendEmail(emailList);
    }
    showSection=false;
    InfoSent=true;
    return null;
}

public Boolean validateEmail(String email) {
    Boolean res = true;
    String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
    Pattern MyPattern = Pattern.compile(emailRegex);
    Matcher MyMatcher = MyPattern.matcher(email);
    if (!MyMatcher.matches()) 
        res = false;
    return res; 
}

}
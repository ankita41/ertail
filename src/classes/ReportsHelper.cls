/*****

*   @Class      :   ReportsHelper.cls
*   @Description:   Couple of static methods to assist with the reports controllers
*   @Author     :   Thibauld
*   @Created    :   03 SEP 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
************/
public without sharing class ReportsHelper {
	private static String PREFIX_IMAGELINK = '/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId=';
	
	public static String CreateImageLink(Id imageId){
    	if (imageId != null)
    		return PREFIX_IMAGELINK + imageId;
    	else
    		return '';
    }
    
	public static Date GetPreviousMonthStart(Date currentDate){
		return Date.newInstance(currentDate.year(), currentDate.month(), 1).addMonths(-1);
	}

	public static Date GetPreviousQuarterStart(Date currentDate){
		return GetQuarterStart(currentDate.addMonths(-3));
	}
	
	public static Date GetPreviousQuarterEnd(Date currentDate){
		return GetQuarterEnd(currentDate.addMonths(-3));
	}

	public static Date GetQuarterStart(Date currentDate){
		Integer currentMonth = currentDate.month();
		if (currentMonth >= 10){
			return Date.newInstance(currentDate.year(), 10, 1);
		}else if (currentMonth >= 7){
			return Date.newInstance(currentDate.year(), 7, 1);
		}else if (currentMonth >= 4){
			return Date.newInstance(currentDate.year(), 4, 1);
		}else {
			return Date.newInstance(currentDate.year(), 1, 1);
		}
	}
	
	public static Date GetQuarterEnd(Date currentDate){
		Integer currentMonth = currentDate.month();
		if (currentMonth >= 10){
			return Date.newInstance(currentDate.year() +1, 1, 1);
		}else if (currentMonth >= 7){
			return Date.newInstance(currentDate.year(), 10, 1);
		}else if (currentMonth >= 4){
			return Date.newInstance(currentDate.year(), 7, 1);
		}else {
			return Date.newInstance(currentDate.year(), 4, 1);
		}
	}
	
	public static Boolean IsLastQuarter(Date currentDate){
		return currentDate.month() >= 10;
	}
	
	public static String GetQuarterNumberStr(Date currentDate){
		Integer currentMonth = currentDate.month();
		if (currentMonth >= 10){
			return '4th';
		}else if (currentMonth >= 7){
			return '3rd';
		}else if (currentMonth >= 4){
			return '2nd';
		}else {
			return '1st';
		}
	}
	          
    public static Integer GetQuarterNumber(Date dateToTest){
        if (dateToTest == null) return null;
        if (dateToTest.month() < 4) return 0;
        if (dateToTest.month() < 7) return 1;
        if (dateToTest.month() < 10) return 2;
        return 3;
    }

	// return the Quarter as a String (Q1, Q2 Q3...)
	public String GetQuarterStr(Date dateToTest) {
		if (dateToTest == null) return null;
        if (dateToTest.month() < 4) return 'Q1';
        if (dateToTest.month() < 7) return 'Q2';
        if (dateToTest.month() < 10) return 'Q3';
        return 'Q4';
	}

}
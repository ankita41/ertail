global class ScheduledContentDownloadCleanup implements Schedulable{
   global void execute(SchedulableContext SC) {
      ContentDownloadController.RemoveTemporaryData();
   }
}
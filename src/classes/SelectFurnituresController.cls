public with sharing class SelectFurnituresController {
	public Project__c project {get; set;}
	public List<FurnitureItemWrapper> lstAllFurnitures { get; set;}
	
	private Map<Id,Furnitures_Inventory__c> mapFurnituresInventoryPerFurniture;
	
	
	public SelectFurnituresController (ApexPages.StandardController stdController) {
		project = (Project__c)stdController.getRecord();
		
		mapFurnituresInventoryPerFurniture = new Map<Id,Furnitures_Inventory__c>();
		List<Furnitures_Inventory__c> tmpListFI = [SELECT Id, Furniture__c, Quantity__c, Note__c FROM Furnitures_Inventory__c WHERE Project__c = :project.Id];
		for (Furnitures_Inventory__c fi : tmpListfI) {
			mapFurnituresInventoryPerFurniture.put(fi.Furniture__c, fi);
		}
		
		lstAllFurnitures = new List<FurnitureItemWrapper>();
		List<Furniture__c> tmpListFurn = [SELECT Id, Name FROM Furniture__c WHERE Active__c = true];
		for (Furniture__c f : tmpListFurn) {
			Integer tmpQty = 0;
			String tmpNote = '';
			if (mapFurnituresInventoryPerFurniture.get(f.Id) != null) {
				tmpQty = integer.valueOf(mapFurnituresInventoryPerFurniture.get(f.Id).Quantity__c);
				tmpNote = mapFurnituresInventoryPerFurniture.get(f.Id).Note__c;
			}
			
			lstAllFurnitures.add(new FurnitureItemWrapper(f,tmpQty,tmpNote));
		}
	}
	
	public PageReference SaveSelection() {
		List<Furnitures_Inventory__c> newFurnitureInventory = new List<Furnitures_Inventory__c>();
		List<Furnitures_Inventory__c> toDeleteFurnitureInventory = new List<Furnitures_Inventory__c>();
		
		for (FurnitureItemWrapper fWr : lstAllFurnitures) {
			if (fWr.quantity > 0) {
				Id tmpFiId = null;
				if (mapFurnituresInventoryPerFurniture.get(fWr.furnitureItem.Id) != null) {
					tmpFiId = mapFurnituresInventoryPerFurniture.get(fWr.furnitureItem.Id).Id;
				}
				
				newFurnitureInventory.add(new Furnitures_Inventory__c(Id = tmpFiId, Furniture__c = fWr.furnitureItem.Id, Project__c = project.Id, Quantity__c = fWr.quantity, Note__c = fWr.note));
			} else if (fWr.quantity == 0) {
				if (mapFurnituresInventoryPerFurniture.get(fWr.furnitureItem.Id) != null) {
					toDeleteFurnitureInventory.add(mapFurnituresInventoryPerFurniture.get(fWr.furnitureItem.Id));
				}
			}
		}
		
		if (! newFurnitureInventory.isEmpty()) {
			upsert newFurnitureInventory;
		}
		
		if (! toDeleteFurnitureInventory.isEmpty()) {
			delete toDeleteFurnitureInventory;
		}
		
		PageReference retPage = new PageReference('/' + project.Id);
        retPage.setRedirect(true);
        return retPage;
	}
	
	public PageReference Cancel() {
		PageReference retPage = new PageReference('/' + project.Id);
		retPage.setRedirect(true);
		return retPage;
	}
	
	public class FurnitureItemWrapper {
        public Furniture__c furnitureItem{get; set;}
        public Integer quantity {get; set;}
        public String note {get; set;}
        
        public FurnitureItemWrapper(Furniture__c fi, Integer qty, String txt) {
            furnitureItem = fi;
            quantity = qty;
            note = txt;
        }
    }
}
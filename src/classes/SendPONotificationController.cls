/*****
*   @Class      :   SendPONotificationController.cls
*   @Description:   Controller class for the SendPONotification VF page 
*   @Author     :   XXX
*   @Created    :   XXX
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy        11 AUG 2014     Incorporated the PO Notification Sent field to be updated when the notification is sent.
*   Jacky Uy        12 AUG 2014     Included PO PDF Attachments.
*   Jacky Uy        12 SEP 2014     Improved Code on Constructor and Send() methods.
*   Jacky Uy        15 SEP 2014     Added checking on the total file attachment size of 5MB before sending.
*   Promila         22 Dec 2015     Added Support Stakeholders in CC of notification email
*	Nada Bokova		26 SEP 2017		Added replacement of the name in the email signature (ticket RM0027168509)
*****/

public without sharing class SendPONotificationController {
    public Purchase_Order__c po {get;set;}
    public Purchase_Order__c o {get;set;}
    public String supplier {get;set;}
    public String suppliername;
    public String CC {get;set;}
    public String additionalrecipient {get;set;}
    private Id Toid;
    public EMailTemplate em {get;set;}
    public String idtemplate {get;set;}
    public String subject {get;set;}
    public List<ContentVersion> cv;
    public boolean error {get;set;}
    public String title {get;set;}
    public String idtitle {get;set;}
    public String mailHTMLbody {get;set;}
    
    public List<Attachment> poAttachments {get;set;}
    private Id cvId {get;set;}

    public SendPONotificationController(ApexPages.StandardController controller) {
        String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
        Map<String, Schema.SobjectField> fieldsMap = Schema.getGlobalDescribe().get('Purchase_Order__c').getDescribe().fields.getMap();
        String qryStr = 'SELECT RECO_Project__r.Project_Name__c, RECO_Project__r.Name, RECO_Project__r.RecordtypeId ,RECO_Project__r.Recordtype.name, Owner.Email, Owner.FirstName, Owner.Name, Owner.LastName, Owner.Phone, Owner.Title, Offer__r.Supplier_Contact__c, Offer__r.Supplier_Contact__r.Name, Offer__r.Supplier_Contact__r.FirstName, Offer__r.Supplier_Contact__r.Email, Offer__r.National_Boutique_Manager__r.Name, Offer__r.National_Boutique_Manager__r.Email, Offer__r.Retail_Project_Manager__r.Name, Offer__r.Retail_Project_Manager__r.Email, Offer__r.Manager_Architect__r.Name, Offer__r.Manager_Architect__r.Email,RECO_Project__r.Other_Design_Architect__r.Name,RECO_Project__r.Other_Design_Architect__r.Email,RECO_Project__r.Other_Manager_Architect__r.Name,RECO_Project__r.Other_Manager_Architect__r.Email,RECO_Project__r.Support_National_Boutique_Manager__r.Name,RECO_Project__r.Support_National_Boutique_Manager__r.Email,RECO_Project__r.Support_PO_Operator__r.Name,RECO_Project__r.Support_PO_Operator__r.Email,RECO_Project__r.Other_Retail_Project_Manager__r.Name,RECO_Project__r.Other_Retail_Project_Manager__r.Email, RECO_Project__r.Manager_architect__r.Name,RECO_Project__r.Manager_architect__r.Email,RECO_Project__r.National_boutique_manager__r.Name,RECO_Project__r.National_boutique_manager__r.Email,RECO_Project__r.Retail_project_manager__r.Name,RECO_Project__r.Retail_project_manager__r.Email,';
        for(String s : fieldsMap.keySet()){
            if(fieldsMap.get(s).getDescribe().isAccessible())
                qryStr = qryStr + s + ', ';
        }
        qryStr = qryStr.removeEnd(', ') + ' FROM Purchase_Order__c WHERE Id = \'' + ApexPages.CurrentPage().getParameters().get('id') + '\' ';
        
        cv = new List<ContentVersion>();
        po = Database.query(qryStr);
        
        if(po.Offer__r.Supplier_Contact__c == null) {
            error = true; 
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'No supplier contact was found for the offer related to the purchase order. Please update the related offer.')); 
            return; 
        }
            
        //CHECK IF THERE IS ONLY ONE DOCUMENT ATTACHED TO THE ASSOCIATED OFFER
        List<Attachment_Folder__c> folder = [SELECT Id FROM Attachment_Folder__c WHERE Name = 'Budget & Costs' AND Offer__c = :po.offer__c];
        List<FeedItem> documents = new List<FeedItem>();
        if(!folder.isEmpty())
            documents = [SELECT Id, RelatedRecordId FROM FeedItem WHERE ParentId = :folder[0].Id AND Type = 'ContentPost'];
            
        if(documents.size()>1) {
            error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'More than one file is linked to the related Offer folder \"Budget & Costs\". Please delete other files to correct this issue, as only one document can be linked to the Offer.'));
            return;
        }
        
        else if(documents.size()==0) {
            error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'No document was found for the Offer related to this Purchase Order.'));
            return;
        }
        
        else if(documents.size()==1){
            o = new Purchase_Order__c();
            cvId = documents[0].RelatedRecordId;
            cv = database.query('SELECT Id, Title,FirstPublishLocationId FROM ContentVersion WHERE Id = :cvId AND FirstPublishLocationId!=null');
            system.debug('========cvvvvvv======'+cv[0].FirstPublishLocationId);
            system.debug('========cvvvvvv==11===='+cv[0]);
            
            if(!cv.isEmpty()){
                title = cv[0].Title;
                idtitle = cv[0].Id;
                
                supplier = po.Offer__r.Supplier_Contact__r.Name;
                suppliername = po.Offer__r.Supplier_Contact__r.FirstName; 
                toid = po.Offer__r.Supplier_Contact__c;
                
                CC = '';
                //CC += po.Offer__r.Retail_Project_Manager__r.Name; /* Commented for issue of NULL */
                //CC = CC + ', ' + (po.Offer__r.Manager_Architect__c!=null?(po.Offer__r.Manager_Architect__r.Name+ ', '):'')  + (po.Offer__r.National_Boutique_Manager__c!=null?(po.Offer__r.National_Boutique_Manager__r.Name + ', '):''); /* Commented for issue of NULL */
                CC += po.RECO_Project__r.Retail_Project_Manager__r.Name; /* Added for issue of NULL */
                CC = CC + ', ' + (po.RECO_Project__r.Manager_Architect__c!=null?(po.RECO_Project__r.Manager_Architect__r.Name+ ', '):'')  + (po.RECO_Project__r.National_Boutique_Manager__c!=null?(po.RECO_Project__r.National_Boutique_Manager__r.Name + ', '):''); /* Added for issue of NULL */
                CC += po.Owner.Name;
                /*******Added Support Stakeholders in receipient list 22/11/2015 starts here **********/
                /*if(po.RECO_Project__r.Other_Design_Architect__r.Name!=null )      //Priya: Removed Design Architect from mail list
                {
                 CC+= ', '+po.RECO_Project__r.Other_Design_Architect__r.Name;
                }*/
                if(po.RECO_Project__r.Other_Manager_Architect__r.Name!=null )
                {
                 CC+=', '+po.RECO_Project__r.Other_Manager_Architect__r.Name;
                }
                if(po.RECO_Project__r.Support_National_Boutique_Manager__r.Name!=null )
                {
                 CC+=', '+po.RECO_Project__r.Support_National_Boutique_Manager__r.Name;
                }
                if(po.RECO_Project__r.Support_PO_Operator__r.Name!=null )
                {
                 CC+=', '+po.RECO_Project__r.Support_PO_Operator__r.Name;
                }
                if(po.RECO_Project__r.Other_Retail_Project_Manager__r.Name!=null )
                {
                 CC+=', '+po.RECO_Project__r.Other_Retail_Project_Manager__r.Name;
                }
                /*******Added Support Stakeholders in receipient list 22/11/2015 ends here **********/
                // ticket#RM0026301599 : NCAFE profile check replaced with NCAFE Project RecordType (Changed below if condition)
                //if(currentUserProfile.contains('NCAFE'))
                if(po.RECO_Project__r.Recordtype.name.contains('NCafe')){
                em = [
                    SELECT id, FolderId, ApiVersion,  BrandTemplateId, Description, DeveloperName, Encoding, TemplateStyle, TemplateType, HTMLValue, Name, Body, Markup, Subject 
                    FROM EMailTemplate WHERE DeveloperName = 'NCAFE_NotifPOoperatorPOnumber'
                ];
                }
                else{
                em = [
                    SELECT id, FolderId, ApiVersion,  BrandTemplateId, Description, DeveloperName, Encoding, TemplateStyle, TemplateType, HTMLValue, Name, Body, Markup, Subject 
                    FROM EMailTemplate WHERE DeveloperName = 'RECO_NotifPOoperatorPOnumber'
                ];
               } 
                poAttachments = [SELECT Name FROM Attachment WHERE ParentId = :po.Id AND IsDeleted = FALSE];
            }
            else{
                error = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The document attached to the Offer needs to be shared manually by the Owner.'));
                return;
            }
        }
    }
    
    public PageReference Send() {
        if(!cv.isEmpty()){
            List<String> dest = new List<String>();
            List<String> destCC = new List<String>();
            List<String> destCC2 = new List<String>();
            List<String> addCC = new List<String>();
            
            dest.add(po.Offer__r.Supplier_Contact__r.Email);
            
            if(po.RECO_Project__r.Retail_Project_Manager__r.Email != null){
                destCC.add(po.RECO_Project__r.Retail_Project_Manager__r.Email); 
                destCC2.add(po.RECO_Project__r.Retail_Project_Manager__r.Email);
            }
            if(po.RECO_Project__r.Manager_Architect__r.Email != null){
                destCC.add(po.RECO_Project__r.Manager_Architect__r.Email); 
                destCC2.add(po.RECO_Project__r.Manager_Architect__r.Email);
            }
            if(po.RECO_Project__r.National_Boutique_Manager__r.Email != null){  
                destCC.add(po.RECO_Project__r.National_Boutique_Manager__r.Email); 
                destCC2.add(po.RECO_Project__r.National_Boutique_Manager__r.Email);
            }
            if(po.Owner.Email!=null){
                destCC.add(po.Owner.Email); 
                destCC2.add(po.Owner.Email);
            }
            /*******Added Support Stakeholders in receipient list 22/11/2015 starts here **********/
            /*if(po.RECO_Project__r.Other_Design_Architect__r.Email!=null ){                //Priya: Removed Design Architect from mail list
                 destCC.add(po.RECO_Project__r.Other_Design_Architect__r.Email); 
                // destCC2.add(po.RECO_Project__r.Other_Design_Architect__r.Email);
                }*/             
            if(po.RECO_Project__r.Other_Manager_Architect__r.Email!=null ){
                 destCC.add(po.RECO_Project__r.Other_Manager_Architect__r.Email); 
                 //destCC2.add(po.RECO_Project__r.Other_Manager_Architect__r.Email);
                }
            if(po.RECO_Project__r.Support_National_Boutique_Manager__r.Email!=null ){
                 destCC.add(po.RECO_Project__r.Support_National_Boutique_Manager__r.Email); 
                 //destCC2.add(po.RECO_Project__r.Support_National_Boutique_Manager__r.Email);
                }
            if(po.RECO_Project__r.Support_PO_Operator__r.Email!=null ){
                 destCC.add(po.RECO_Project__r.Support_PO_Operator__r.Email); 
                 //destCC2.add(po.RECO_Project__r.Support_PO_Operator__r.Email);
                }
            if(po.RECO_Project__r.Other_Retail_Project_Manager__r.Email!=null ){
                 destCC.add(po.RECO_Project__r.Other_Retail_Project_Manager__r.Email); 
                 //destCC2.add(po.RECO_Project__r.Other_Retail_Project_Manager__r.Email);
                }
              /*******Added Support Stakeholders in receipient list 22/11/2015 ends here **********/  
            if(additionalrecipient!=null && additionalrecipient.length()!=0){
                destCC.add(additionalrecipient); 
                addCC.add(additionalrecipient);
            }
            if(o.Supplier_Contact__c!=null) {
                Contact [] c = [SELECT Email FROM Contact WHERE id = :o.Supplier_Contact__c]; 
                if(c.size()!=0 && c[0].email!=null)  
                    destCC.add(c[0].email); addCC.add(c[0].email);
            }  
                    
            //MAIL CREATION
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();     
            mail.setHTMLBody(mailHTMLbody);
            mail.setsubject(subject); 
            mail.setTargetObjectId(po.Offer__r.Supplier_Contact__c);
            
            dest = new List<String>();
            mail.setToAddresses(dest);
            mail.setCcAddresses(destCC);
            mail.saveAsActivity = true;
            mail.setwhatid(po.id);
            
            //MAIL ATTACHMENTS
            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            Integer totalFileSize = 0;
            
            //OFFER ATTACHMENT
            cv = database.query('SELECT VersionData, PathOnClient, ContentSize FROM ContentVersion WHERE Id = :cvId');
            SYSTEM.DEBUG('cv: ' + cv);
            attach.setInline(false);
            attach.setBody(cv[0].VersionData);
            attach.setFileName(cv[0].PathOnClient);
            attachments.add(attach);
            totalFileSize += cv[0].ContentSize;
            
            //PO ATTACHMENTS
            for(Attachment a : [SELECT Name, Body, BodyLength FROM Attachment WHERE ParentId = :po.Id AND IsDeleted = FALSE]){
                attach = new Messaging.EmailFileAttachment();
                attach.setInline(false);
                attach.setBody(a.Body);
                attach.setFileName(a.Name);
                attachments.add(attach);
                totalFileSize += a.BodyLength;
            }
            
            //CHECK TOTAL FILE SIZE NOT TO EXCEED 5MB
            if(totalFileSize>5242880){
                cv=null; 
                em=null; 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'The allowed maximum Total Size of All Files is 5MB. You need to remove some attachments to send the email.'));
                return null;
            }
            
            mail.setFileAttachments(attachments);
            
            try {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                cv = null;
                em = null;
                Task a = [SELECT id,description FROM Task WHERE whatid= :po.id ORDER BY lastmodifieddate DESC][0];
                system.debug('==========aa======'+a);
                system.debug('==========po.id======'+po.id);
                 if(a.description.contains('Additional To') && a.description.contains('CC')){
                    String temp = a.description.substring(a.description.indexOf('Additional To'),a.description.indexOf('CC'));
                    
                    String addr;
                    if(addCC.size()==0)
                        a.description = a.description.replace(temp, 'Additional To: ');
                    else
                        addr = addCC[0]; if(addCC.size()>1)    addr = addr + ', ' +addCC[1]; a.description = a.description.replace(temp, 'Additional To: ' +addr);
                    
                    String addr2 ='';
                    String temp2 = a.description.substring(a.description.indexOf('CC'),a.description.indexOf('BCC'));
                    for(integer i=0;i<destCC2.size();i++) {
                        if(i!=destCC2.size()-1)    addr2 = addr2 + destCC2[i] + ', ';
                        else addr2 = addr2 + destCC2[i];
                    }
                    
                    a.description = a.description.replace(temp2, '\nCC: ' +addr2+ '\n');
                    a.description= a.description.replace('Additional To', 'Additional CC');
                    database.update(a);
                  } 
                    //UPDATE PO NOTIFICATION SENT DATE
                    po.PO_Notification_Sent__c = true;
                    po.PO_Notification_Sent_Date__c = SYSTEM.Today();
                    update po;
                    
                    PageReference p = new PageReference('/' +po.id);
                    return p;
                
                
            }
            catch(Exception e) {
                cv=null; 
                em=null; 
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getmessage())); 
                return null;
            }
        }
        return null;
    }
    
    public void Template() {
        if(error==true) return; 
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{'invalid@emailaddr.es'};
        mail.setToAddresses(toAddresses);
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
        mail.setSenderDisplayName('MMPT');
        mail.setTargetObjectId(userinfo.getuserid());
        
        EMailTemplate em2 = em.clone(true,false);
        em2.id=null;
        em2.developerName = 'Test' +po.id;
        em2.isactive=true;
        database.insert(em2);
              
        //DYNAMIC FIELDS
        Schema.DescribeSObjectResult objSchema = Purchase_Order__c.sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objSchema.fields.getMap();
        map<String,Integer> fieldlist = new map<String,Integer>();
        for(String fieldName : fieldMap.keySet()){
                System.debug('###FIELD NAME: '+fieldName);
                fieldlist.put(fieldname,1);      
            }
        
        //Nada Bokova: Added per ticket RM0027168509 to replace User name in the signature
        em2.HtmlValue = em2.HtmlValue.replace('{!User.Name}', UserInfo.getName());

        /////NEW MODIFICATIONS
        em2.htmlValue = em2.htmlValue.replace('{!Offer__c.Supplier_Contact__c}', po.offer__r.Supplier_Contact__r.Name);
        em2.htmlValue = em2.htmlValue.replace('{!RECO_Project__c.Project_Name__c}', po.RECO_Project__r.Project_Name__c);
        ///////////END NEW MODIFICATIONS
          
        while(em2.htmlValue.indexOf('{!Purchase_Order__c.')!=-1) {
            String mg;
            mg = em2.htmlValue.substringBetween('{!Purchase_Order__c.','}');
            System.debug('###MERGE: ' +mg);
 
            if(fieldlist.containskey(mg)!=null) {

                if(mg.indexOf('OwnerFullName')!=-1 || mg.indexOf('OwnerEmail')!=-1 || mg.indexOf('OwnerFirstName')!=-1 || mg.indexOf('OwnerId')!=-1 || mg.indexOf('OwnerLastName')!=-1 || mg.indexOf('OwnerPhone')!=-1 || mg.indexOf('OwnerTitle')!=-1)     {
                    
                    if(po.Owner.Name != null)    em2.htmlvalue= em2.htmlvalue.replace('{!Purchase_Order__c.OwnerFullName}', po.Owner.Name);
                    
                    if(po.Owner.Email != null)    em2.htmlvalue= em2.htmlvalue.replace('{!Purchase_Order__c.OwnerEmail}', po.Owner.Email);
                        
                    if(po.Owner.FirstName != null)    em2.htmlvalue= em2.htmlvalue.replace('{!Purchase_Order__c.OwnerFirstName}', po.Owner.FirstName);
                        
                    if(po.OwnerId != null)    em2.htmlvalue= em2.htmlvalue.replace('{!Purchase_Order__c.OwnerId}', po.OwnerId);
                        
                    if(po.Owner.LastName != null)    em2.htmlvalue= em2.htmlvalue.replace('{!Purchase_Order__c.OwnerLastName}', po.Owner.LastName);
                        
                    if(po.Owner.Phone != null)    em2.htmlvalue= em2.htmlvalue.replace('{!Purchase_Order__c.OwnerPhone}', po.Owner.Phone);
                        
                    if(po.Owner.Title != null)    em2.htmlvalue= em2.htmlvalue.replace('{!Purchase_Order__c.OwnerTitle}', po.Owner.Title);
                    
                    continue;
                }
                
                try {
                    if(po.get(mg)!=null) {
                        String temp = String.valueOf(po.get(mg));
                        //DATETIME MANAGEMENT
                        if(temp.indexOf('00:00:00')!=-1)  {
                            temp = temp.replace(' 00:00:00',''); String [] format = temp.split('-'); String temp2 = format[2] + '-' + format[1] + '-' + format[0]; 
                        }
                        
                        if(mg == 'RECO_Project__c')    em2.htmlValue = em2.htmlValue.replace('{!Purchase_Order__c.' + mg, po.RECO_Project__r.Name);
                   
                        em2.htmlValue = em2.htmlValue.replace('{!Purchase_Order__c.' + mg, temp);
                    }
                    else em2.htmlValue = em2.htmlValue.replace('{!Purchase_Order__c.' + mg, ''); 
                }
                catch(Exception e) {
                    try { if(mg.indexOf('Id')!=-1) { String t = mg; t = t.replace('Id',''); t = String.valueOf(po.get(t)); em2.htmlValue = em2.htmlValue.replace('{!Purchase_Order__c.' + mg, t); }
                        
                        else { String t = mg; t = t.replace('__c','__r.Name'); t = String.valueOf(po.get(t)); em2.htmlValue = em2.htmlValue.replace('{!Purchase_Order__c.' + mg, t);}
                        }
                    catch(Exception ec) { ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getmessage()));}    

                }
            }   
        } 
        
        em2.htmlValue = em2.htmlValue.replaceAll('}','');   
        
        ////SUBJECT
        while(em2.subject.indexOf('{!Purchase_Order__c.')!=-1) {
            String mg;
            em2.subject = em2.subject.replace('{!RECO_Project__c.Project_Name__c}', po.RECO_Project__r.Project_Name__c);
            em2.subject= em2.subject.replace('{!Purchase_Order__c.Supplier__c}', po.supplier__c);
            mg = em2.subject.substringBetween('{!Purchase_Order__c.','}');
        } 
        
        em2.subject = em2.subject.replaceAll('}','');
        
        update em2;
        mail.setTemplateId(em2.Id);
              
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        Database.rollback(sp);
        database.delete(em2);
        
        em=null;
        em2=null;
        
        String mailTextBody = mail.getPlainTextBody();
        mailHtmlBody = mail.getHTMLBody();
        subject = mail.getSubject();
        if(mailHTMLBody <> null)
        {
            mailHTMLBody = mailHTMLBody.remove('<td  style=" height:1; background-color:#330000; bLabel:accent3; bEditID:r6st1;"></td>');
            mailHTMLBody = mailHTMLBody.replace('<td  style=" height:1; background-color:#330000; bLabel:accent1; bEditID:r2st1;"></td>', '<td  style=" font-size:1%; height:1; background-color:#330000; bLabel:accent1; bEditID:r2st1;"></td>');
            mailHTMLBody = mailHTMLBody.replace('<td  style=" height:1; background-color:#330000; bLabel:accent2; bEditID:r4st1;"></td>', '<td  style=" font-size:1%; height:1; background-color:#330000; bLabel:accent2; bEditID:r4st1;"></td>');
        }
       /* String currentUserProfile = UserConstants.CURRENT_USER.Profile.Name;
        if(currentUserProfile.contains('NCAFE'))
          mailHTMLBody=mailHTMLBody.replace('Nespresso International Retail Development Team','NCafe Retail Development Team');*/
    }
    
    public class CustomException extends Exception {}
    //throw new CustomException('' + qryStr);
}
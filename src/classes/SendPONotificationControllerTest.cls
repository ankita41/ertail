/*****
*   @Class      :   SendPONotificationControllerTest.cls
*   @Description:   Test coverage for the SendPONotificationController.cls
*   @Author     :   JACKY UY
*   @Created    :   18 SEP 2014
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*
* 
*****/

@isTest
public class SendPONotificationControllerTest{
    public static testMethod void unitTest(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
        
            TestHelper.createCurrencies();
            Market__c market = TestHelper.createMarket();
            insert market;
            
            Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
            Account acc = TestHelper.createAccount(rTypeId, market.Id);
            insert acc;
            
            rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
            insert suppAcc;
            
            Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test',email='test1@testmail.com', AccountId = suppAcc.Id);
            insert suppCon;
        
            rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
            Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
            insert btq;
            
            rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
            RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
            bProj.Currency__c = 'CHF';
            bProj.National_boutique_manager__c = TestHelper.NationalBoutiqueManager.Id;
            bProj.Manager_architect__c = TestHelper.ManagerArchitect.Id;
            bProj.Other_Manager_Architect__c  = TestHelper.TradeManager.id ;
            bProj.Support_National_Boutique_Manager__c = TestHelper.SalesPromoter.id ;
            bProj.Support_PO_Operator__c =  TestHelper.POOperator.id ;
            bProj.Other_Retail_Project_Manager__c =  TestHelper.CornerArchitect.id ;
            
            insert bProj;
            
            rTypeId = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Draft').getRecordTypeId();
            Offer__c offer = TestHelper.createOffer(rTypeId, bProj.Id);
            offer.Currency__c = 'CHF';
            offer.Approval_status__c = 'Approved';
            insert offer;
            
            Attachment_Folder__c newFolder = new Attachment_Folder__c(
                Name = 'Budget & Costs',
                Offer__c = offer.Id
            );
            insert newFolder;
            
            Purchase_Order__c po = TestHelper.createPO(offer.Id, bProj.Id);
            insert po;
            
            Attachment newAttach = new Attachment(
                Name = 'Test Attachment',
                ParentId = po.Id,
                Body = Blob.valueOf('Test Attachment')
            );
            insert newAttach;
            
            Test.startTest();
            PageReference pageRef = Page.SendPoNotification;
            pageRef.getParameters().put('id', po.id);
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController stdCon = new ApexPages.StandardController(po);
            
            //TEST WHERE SUPPLIER CONTACT IS NULL
            SendPONotificationController con = new SendPONotificationController(stdCon);
            SYSTEM.Assert(ApexPages.getMessages()[0].getDetail().contains('No supplier contact was found for the offer related to the purchase order. Please update the related offer.'));
            
            offer.Supplier__c = suppAcc.Id;
            offer.Supplier_Contact__c = suppCon.Id;
            update offer;
            
            /*Task po_Task_Obj = new Task() ;
            po_Task_Obj.description = 'Additional To:tt@testmail.com CC:t@testmail.com' ;
            po_Task_Obj.whatid = offer.Id ;
            insert po_Task_Obj ;
            */
            //TEST WHERE THERE ARE NO DOCUMENTS UPLOADED ON THE OFFER
            con = new SendPONotificationController(stdCon);
            SYSTEM.Assert(ApexPages.getMessages()[1].getDetail().contains('No document was found for the Offer related to this Purchase Order.'));
            
            FeedItem newFeed = new FeedItem(
                ParentId = newFolder.Id,
                ContentFileName = 'Test_File.jpg',
                ContentData = Blob.valueOf('Unit Testing'),
                Type = 'ContentPost'
            );
            insert newFeed;
            
            
            EmailTemplate validEmailTemplate = new EmailTemplate();
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'name';
            validEmailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();
            validEmailTemplate.Subject = 'test' ;
            //validEmailTemplate.Description = 'test' 
            //validEmailTemplate.Encoding
            //validEmailTemplate.TemplateStyle 
            
            String HTMLvalue = 'Dear {!Offer__c.Supplier_Contact__c},\n' ;

            HTMLvalue = HTMLvalue +'The offer here attached has been validated and the related Purchase Order number is {!Purchase_Order__c.Name}.\n';
            HTMLvalue = HTMLvalue + '<td  style=" height:1; background-color:#330000; bLabel:accent3; bEditID:r6st1;"></td>' ;
            /*HTMLvalue = HTMLvalue +'Offer details\n' ;
            HTMLvalue = HTMLvalue +'Project Name: {!RECO_Project__c.Project_Name__c}\n' ;
            HTMLvalue = HTMLvalue +'Supplier: {!Purchase_Order__c.Supplier__c}\n' ;
            HTMLvalue = HTMLvalue +'Total amount (excl. VAT): {!Purchase_Order__c.Amount__c}\n' ;
            HTMLvalue = HTMLvalue +'Currency: {!Purchase_Order__c.Currency__c}\n' ;
            HTMLvalue = HTMLvalue +'Notes: {!Purchase_Order__c.Description__c}\n' ;


            HTMLvalue = HTMLvalue +'Please do not forget to remind this purchase order number on the invoice.\n' ;
            HTMLvalue = HTMLvalue +'For any difference between final invoice and related PO please inform us before invoicing.\n' ;

            HTMLvalue = HTMLvalue +'Best regards,\n' ;
            HTMLvalue = HTMLvalue +'{!Purchase_Order__c.OwnerFullName} &\n' ;
            HTMLvalue = HTMLvalue +'Nespresso International Retail Development Team \n' ;*/
            validEmailTemplate.HTMLValue = HTMLvalue ;
            validEmailTemplate.Body = '<td  style=" height:1; background-color:#330000; bLabel:accent3; bEditID:r6st1;"></td>' ;
            //validEmailTemplate.Markup, 
            //validEmailTemplate.Subject 
            
            insert validEmailTemplate;
            
            //TEST WHERE THERE IS ONE DOCUMENT UPLOADED ON THE OFFER
            con = new SendPONotificationController(stdCon);
            con.additionalrecipient = 'test@test.com';
            con.mailHTMLbody = 'tt@testmail.com --- <td  style=" height:1; background-color:#330000; bLabel:accent3; bEditID:r6st1;"></td>' ;
            con.o.Supplier_Contact__c = suppCon.Id;
            //con.template();
            
            con.Send();
            con.em = validEmailTemplate ;
            con.template();
            
            Test.stopTest();
        }
    }
    
    public static testMethod void unitTest1(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
        
            TestHelper.createCurrencies();
            Market__c market = TestHelper.createMarket();
            insert market;
            
            Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
            Account acc = TestHelper.createAccount(rTypeId, market.Id);
            insert acc;
            
            rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
            insert suppAcc;
            
            Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test',email='test1@testmail.com', AccountId = suppAcc.Id);
            insert suppCon;
        
            rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
            Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
            insert btq;
            
            rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
            RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
            bProj.Currency__c = 'CHF';
            bProj.National_boutique_manager__c = TestHelper.NationalBoutiqueManager.Id;
            bProj.Manager_architect__c = TestHelper.ManagerArchitect.Id;
            bProj.Other_Manager_Architect__c  = TestHelper.TradeManager.id ;
            bProj.Support_National_Boutique_Manager__c = TestHelper.SalesPromoter.id ;
            bProj.Support_PO_Operator__c =  TestHelper.POOperator.id ;
            bProj.Other_Retail_Project_Manager__c =  TestHelper.CornerArchitect.id ;
            
            insert bProj;
            
            rTypeId = Schema.sObjectType.Offer__c.getRecordTypeInfosByName().get('Draft').getRecordTypeId();
            Offer__c offer = TestHelper.createOffer(rTypeId, bProj.Id);
            offer.Currency__c = 'CHF';
            offer.Approval_status__c = 'Approved';
            insert offer;
            
            Attachment_Folder__c newFolder = new Attachment_Folder__c(
                Name = 'Budget & Costs',
                Offer__c = offer.Id
            );
            insert newFolder;
            
            Purchase_Order__c po = TestHelper.createPO(offer.Id, bProj.Id);
            insert po;
            
            Attachment newAttach = new Attachment(
                Name = 'Test Attachment',
                ParentId = po.Id,
                Body = Blob.valueOf('Test Attachment')
            );
            insert newAttach;
            
            Test.startTest();
            PageReference pageRef = Page.SendPoNotification;
            pageRef.getParameters().put('id', po.id);
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController stdCon = new ApexPages.StandardController(po);
            
            //TEST WHERE SUPPLIER CONTACT IS NULL
            SendPONotificationController con = new SendPONotificationController(stdCon);
            SYSTEM.Assert(ApexPages.getMessages()[0].getDetail().contains('No supplier contact was found for the offer related to the purchase order. Please update the related offer.'));
            
            offer.Supplier__c = suppAcc.Id;
            offer.Supplier_Contact__c = suppCon.Id;
            update offer;
            
            /*Task po_Task_Obj = new Task() ;
            po_Task_Obj.description = 'Additional To:tt@testmail.com CC:t@testmail.com' ;
            po_Task_Obj.whatid = offer.Id ;
            insert po_Task_Obj ;
            */
            //TEST WHERE THERE ARE NO DOCUMENTS UPLOADED ON THE OFFER
            con = new SendPONotificationController(stdCon);
            SYSTEM.Assert(ApexPages.getMessages()[1].getDetail().contains('No document was found for the Offer related to this Purchase Order.'));
            List<FeedItem> lstFeedItem = new List<FeedItem>() ;
            
            FeedItem newFeed = new FeedItem(
                ParentId = newFolder.Id,
                ContentFileName = 'Test_File.jpg',
                ContentData = Blob.valueOf('Unit Testing'),
                Type = 'ContentPost'
            );
            lstFeedItem.add(newFeed) ;
            
            /*FeedItem newFeed1 = new FeedItem(
                ParentId = newFolder.Id,
                ContentFileName = 'Test_File.jpg',
                ContentData = Blob.valueOf('Unit Testing'),
                Type = 'ContentPost'
            );
            lstFeedItem.add(newFeed1) ;
            */
            insert lstFeedItem;
            
            
            //TEST WHERE THERE IS ONE DOCUMENT UPLOADED ON THE OFFER
            con = new SendPONotificationController(stdCon);
            con.additionalrecipient = 'test@test.com';
            con.idtemplate = '' ;
            
            con.o.Supplier_Contact__c = suppCon.Id;
            
            
            con.Send();
            
            Test.stopTest();
        }
    }
}
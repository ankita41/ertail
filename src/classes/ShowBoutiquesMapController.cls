/*****
*   @Class      :   ShowBoutiquesMapController.cls
*   @Description:   Controller for the ShowBoutiquesMap page
*   @Author     :   Jacky Uy
*   @Created    :   26 AUG 2014
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*	Jacky Uy		26 AUG 2014		Created Boutique Interactive Map
*                         
*****/

public without sharing class ShowBoutiquesMapController {
    public List<LocationWrapper> locationList {
        get{
            Map<Id,Account> mapLocations = new Map<Id,Account>([
                SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Location__Latitude__s, Location__Longitude__s
                FROM Account 
                WHERE RecordType.Name = 'Boutique Location' 
                AND Id IN (SELECT boutique_location_account__c FROM Boutique__c WHERE boutique_location_account__c!=NULL) 
                AND Geocoding_Zero_Results__c=FALSE
            ]);
            
            Account location;
            String tmpAddress = '';
            List<LocationWrapper> tmpLocationList = new List<LocationWrapper>();
            
            for(Boutique__c b : [SELECT Name, boutique_location_account__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL]){
            	if(mapLocations.containsKey(b.boutique_location_account__c)){
	                location = mapLocations.get(b.boutique_location_account__c);
	                tmpAddress = (location.BillingStreet != null ? (location.BillingStreet + '<br/>') : '') + 
	                        (location.BillingCity != null ? (location.BillingCity + ', ') : '') + 
	                        (location.BillingPostalCode != null ? (location.BillingPostalCode + '') : '') + '<br/>' + 
	                        (location.BillingCountry != null ? location.BillingCountry : '');
	                tmpLocationList.add(new LocationWrapper(tmpAddress, double.valueOf(location.Location__Latitude__s), double.valueOf(location.Location__Longitude__s), b.Name, b.Default_Image__c, b.Id));
            	}
            }
            
            return tmpLocationList;
        } set;
    }
    
    public String locationData {
        get{
            String tmpString = '[';
            for(LocationWrapper lw : locationList) {
                tmpString += (tmpString == '[' ? '' : ', ') + '[\'' + StaticFunctions.jsencode(lw.name) + '\',' + lw.locationLat + ',' + lw.locationLong + ',\'' + StaticFunctions.jsencode(lw.address) + '\',\'' + StaticFunctions.jsencode(lw.imageURL) + '\',\'' + StaticFunctions.jsencode(lw.recordId) + '\']';
            }
            tmpString += ']';
            return tmpString;
        } set;
    }
    
    public class LocationWrapper {
        public String address {get; set;}
        public String name {get; set;}
        public String imageURL {get; set;}
        public Double locationLat {get; set;}
        public Double locationLong {get; set;}
        public Id recordId {get; set;}
        
        public LocationWrapper(String address, Double locationLat, Double locationLong, String name, String imageURL, Id recordId) {
            this.address = address;
            this.name = name;
            this.imageURL = imageURL;
            this.locationLat = locationLat;
            this.locationLong = locationLong; 
            this.recordId = recordId;
        }
    }
}
/*****
*   @Class      :   ShowBoutiquesMapControllerTest.cls
*   @Description:   Test coverage for the ShowBoutiquesMapController.cls
*   @Author     :   JACKY UY
*   @Created    :   18 SEP 2014
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*	JACKY UY		18 SEP 2014		Test Coverage at 100%
* 
*****/

@isTest
private class ShowBoutiquesMapControllerTest {
    static testMethod void myUnitTest() {
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        acc.Geocoding_Zero_Results__c = false;
        insert acc;
        
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.boutique_location_account__c = acc.Id;
        btq.Status__c = 'Opened';
        insert btq;
        
    	PageReference pageRef = Page.ShowBoutiquesMap;
        Test.setCurrentPage(pageRef);
        ShowBoutiquesMapController con = new ShowBoutiquesMapController();
        String locData = con.locationData;
    }
}
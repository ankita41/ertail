/*
*   @Class      :   ShowBoutiquesMapControllerTestCntlr.cls
*   @Description:   A geocoded page showing all Boutique Locations
*   @Author     :   XXX
*   @Created    :   XXX
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        26 AUG 2014     Created Boutique Interactive Map
*   Himanshu P      16th Mar 2016   Filters Google Map/Markers
*                         
*/

public with sharing class ShowBoutiquesMapControllerTestCntlr {
 public List<SelectOption> options{get;set;}
 public string selectedVal{get;set;}
 public List<SelectOption> marketOpts {get;set;}
 public String markets {get;set;}
 public List<SelectOption> zoneOpts {get;set;}
 public String zone {get;set;}
 
 public ShowBoutiquesMapControllerTestCntlr() 
 {
     queryMarkets();
     markets ='[All]'; 
     selectedVal='[All]';
     zoneOpts = new List<SelectOption>();
     zone='All';
 }
 public List<LocationWrapper> locationList {
        get{
            Map<Id,Account> mapLocations = new Map<Id,Account>([
                SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Location__Latitude__s, Location__Longitude__s
                FROM Account 
                WHERE RecordType.Name = 'Boutique Location' 
                AND Id IN (SELECT boutique_location_account__c FROM Boutique__c WHERE boutique_location_account__c!=NULL) 
                AND Geocoding_Zero_Results__c=FALSE
            ]); 
         
        /**********************************Added for Requirement 71*Himanshu*************************************************/    
        
         
   
        
        zoneOpts = new List<SelectOption>();
        zoneOpts.add(new SelectOption('AOA', 'AOA'));
        zoneOpts.add(new SelectOption('AMS', 'AMS'));
        zoneOpts.add(new SelectOption('CCO', 'CCO'));
        zoneOpts.add(new SelectOption('EUR', 'EUR'));
    
        
        
        options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Boutique__c.typology__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple){
        if(f.getLabel()!='Voltaire')
          options.add(new SelectOption(f.getLabel(), f.getvalue()));
        else
          options.add(new SelectOption('Ncafe', 'Ncafe'));    
        } 
            
        Account location;
        String tmpAddress = '';
        List<LocationWrapper> tmpLocationList = new List<LocationWrapper>();
        
        List<String> tempTypo=new List<String>();
        String tempTypologyString='';
        if(String.isnotBlank(selectedVal)){
            for(String se : selectedVal.split(','))
            {
                 se=se.replaceall('\\[','');
                 se=se.replaceall('\\]','');
                 se=se.trim(); 
                 tempTypo.add(se);
            }
        }
        List<String> tempMarkets=new List<String>();
         if(markets!=null && markets!='[]'){
           if(!markets.split(',').isEmpty()){
                for(String se : markets.split(','))
                {
                     se=se.replaceall('\\[','');
                     se=se.replaceall('\\]','');
                     se=se.trim(); 
                     tempMarkets.add(se);
                }
            }
         }
         system.debug('==zone======'+zone);
         system.debug('==selectedVal======'+selectedVal);
         system.debug('==markets======'+markets);
            List<Boutique__c> boutiqueList=new List<Boutique__c>();
            
          
           if(zone!='All' && !markets.contains('All') && !selectedVal.contains('All')){
                if(selectedVal!='[]' && markets!='[]' && zone!='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c,Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and typology__c IN :tempTypo and boutique_location_account__r.Market__r.Name in:tempMarkets and boutique_location_account__r.Market__r.zone__c=:zone];
                }
                
                else if(selectedVal!='[]' && markets=='[]' && zone!='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and typology__c IN :tempTypo and boutique_location_account__r.Market__r.zone__c=:zone];
                }
                
                else if(selectedVal=='[]' && markets!='[]' && zone!='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and boutique_location_account__r.Market__r.Name in:tempMarkets and boutique_location_account__r.Market__r.zone__c=:zone];
                }
               
                else if(selectedVal=='[]' && markets=='[]' && zone!='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and boutique_location_account__r.Market__r.zone__c=:zone];
                }
            }
            else if(zone=='All' && markets!='[All]' && selectedVal!='[All]'){
                if(String.isBlank(selectedVal) && String.isBlank(markets) && zone=='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL];
                }
            
                else if(selectedVal!='[]' && markets!='[]' && zone=='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c,Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and typology__c IN :tempTypo and boutique_location_account__r.Market__r.Name in:tempMarkets ];
                }
                
                else if(selectedVal!='[]' && markets=='[]' && zone=='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and typology__c IN :tempTypo ];
                }
                
                else if(selectedVal=='[]' && markets!='[]' && zone=='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and boutique_location_account__r.Market__r.Name in:tempMarkets ];
                }
               
                else if(selectedVal=='[]' && markets=='[]' && zone=='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL ];
                }
            }
             else if(zone!='All' && markets=='[All]' && selectedVal!='[All]'){

                if(selectedVal!='[]' && markets=='[All]' && zone!='[]'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c,Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and typology__c IN :tempTypo and boutique_location_account__r.Market__r.zone__c=:zone ];
                }
                
                else if(selectedVal!='[]' && markets=='[All]' && zone=='[]'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and typology__c IN :tempTypo ];
                }
                
                else if(selectedVal=='[]' && markets=='[All]' && zone!='[]'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and boutique_location_account__r.Market__r.zone__c=:zone ];
                }
               
                else if(selectedVal=='[]' && markets=='[All]' && zone=='[]'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL ];
                }
            }
            
            else if(zone!='All' && markets!='[All]' && selectedVal=='[All]'){

                if(markets!='[]' && selectedVal=='[All]' && zone!='[]'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c,Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and boutique_location_account__r.Market__r.Name in:tempMarkets and boutique_location_account__r.Market__r.zone__c=:zone ];
                }
                
                else if(markets!='[]' && selectedVal=='[All]' && zone=='[]'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and boutique_location_account__r.Market__r.Name in:tempMarkets];
                }
                
                else if(markets=='[]' && selectedVal=='[All]' && zone!='[]'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and boutique_location_account__r.Market__r.zone__c=:zone ];
                }
               
                else if(markets=='[]' && selectedVal=='[All]' && zone=='[]'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL ];
                }
            }
            
            
            else if(zone=='All' && markets!='[All]' && selectedVal=='[All]'){

                if(markets!='[]' && selectedVal=='[All]' && zone=='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c,Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and boutique_location_account__r.Market__r.Name in:tempMarkets  ];
                }
                
               
            }
            
            else if(zone=='All' && markets=='[All]' && selectedVal!='[All]'){

               if(selectedVal!='[]' && markets=='[All]' && zone=='All'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c,Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and typology__c IN :tempTypo  ];
                }
              
            }
            
            else if(zone!='All' && markets=='[All]' && selectedVal=='[All]'){

                if(markets=='[All]' && selectedVal=='[All]' && zone!='[]'){
                    boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name,boutique_location_account__r.Market__r.zone__c,Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL and  boutique_location_account__r.Market__r.zone__c=:zone ];
                }
                
                
            }
            
            
             else if(zone=='All' && markets.contains('[All]') && selectedVal.contains('[All]')){
                system.debug('=======test all===========');
                boutiqueList=[SELECT Name,typology__c, boutique_location_account__c,boutique_location_account__r.Market__r.Name, Default_Image__c FROM Boutique__c WHERE Status__c IN ('Opened','To be closed-reallocated','Operative') AND boutique_location_account__c!=NULL];
              
            }
            
            for(Boutique__c b : boutiqueList){
                if(mapLocations.containsKey(b.boutique_location_account__c)){
                    location = mapLocations.get(b.boutique_location_account__c);
                    tmpAddress = (location.BillingStreet != null ? (location.BillingStreet + '<br/>') : '') + 
                            (location.BillingCity != null ? (location.BillingCity + ', ') : '') + 
                            (location.BillingPostalCode != null ? (location.BillingPostalCode + '') : '') + '<br/>' + 
                            (location.BillingCountry != null ? location.BillingCountry : '');
                    tmpLocationList.add(new LocationWrapper(tmpAddress, double.valueOf(location.Location__Latitude__s), double.valueOf(location.Location__Longitude__s), b.Name, b.Default_Image__c, b.Id,b.typology__c));
                }
            }
            
            return tmpLocationList;
        } set;
    }
    
    public String locationData {
        get{
            String tmpString = '[';
            for(LocationWrapper lw : locationList) {
                tmpString += (tmpString == '[' ? '' : ', ') + '[\'' + StaticFunctions.jsencode(lw.name) + '\',' + lw.locationLat + ',' + lw.locationLong + ',\'' + StaticFunctions.jsencode(lw.address) + '\',\'' + StaticFunctions.jsencode(lw.imageURL) + '\',\'' + StaticFunctions.jsencode(lw.recordId) + '\',\'' + StaticFunctions.jsencode(lw.typology) +'\']';
            }
            tmpString += ']';
            return tmpString;
        } set;
    }
    
    public void queryMarkets(){
        String qryStr = 'SELECT Name FROM Market__c ';
        marketOpts = new List<SelectOption>();
        
        for(Market__c m : Database.query(qryStr)){
            marketOpts.add(new SelectOption(m.Name, m.Name));
        }
        marketOpts.sort();
        
    }
    
    
    private String quoteKeySet(List<String> mapKeySet)
    {
        String newSetStr = '' ;
        for(String str : mapKeySet)
            newSetStr += '\'' + str + '\',';

        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;
        return newSetStr;

    }
    
    public class LocationWrapper {
        public String address {get; set;}
        public String name {get; set;}
        public String imageURL {get; set;}
        public Double locationLat {get; set;}
        public Double locationLong {get; set;}
        public Id recordId {get; set;}
        public String typology{get;set;}
        
        public LocationWrapper(String address, Double locationLat, Double locationLong, String name, String imageURL, Id recordId,String typology) {
            this.address = address;
            this.name = name;
            this.imageURL = imageURL;
            this.locationLat = locationLat;
            this.locationLong = locationLong; 
            this.recordId = recordId;
            this.typology=typology;
        }
    }
}
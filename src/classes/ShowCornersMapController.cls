/*
*   @Class        :    ShowCornersMapController.cls
*   @Description    :   Controller class for the ShowCornersMap VF page
*   @Author        :   XXXXX
*   @Created        :   17 JUL 2013
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer        Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy         11 APR 2014     Fixed page error 'LimitException: Too many query rows: 50001'
*                         
*/

public with sharing class ShowCornersMapController {
    public List<CornerWrapper> lstCorners {
        get {
            List<CornerWrapper> tmpCornerWrapperList = new List<CornerWrapper>();
            Map <Id,Account> mapAccountsPerAccountId = new Map<Id,Account>([
                SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry
                FROM Account 
                WHERE RecordType.Name = 'Point of Sales' 
                AND Id IN (SELECT Point_of_Sale__c FROM Corner__c)
            ]);
            Map <Id,Project__c> mapLastProjectsPerCornerId = new Map<Id,Project__c>();
            for(Project__c p : [SELECT Id, Corner__c FROM Project__c ORDER BY LastModifiedDate DESC]) {
                if (mapLastProjectsPerCornerId.get(p.Corner__c) == null)
                    mapLastProjectsPerCornerId.put(p.Corner__c,p);
            } 
            
            List<Id> lstMainImgFileAreaIds = new List<Id>();
            Map<Id,Project_File_Area__c> mapMainImgFileAreasPerProjectId = new Map<Id,Project_File_Area__c>();
            for(Project_File_Area__c pfa : [SELECT Id, Project__c FROM Project_File_Area__c WHERE Main_Project_Image__c = true]) {
                mapMainImgFileAreasPerProjectId.put(pfa.Project__c, pfa);
                lstMainImgFileAreaIds.add(pfa.Id);
            }
            
            Map<Id,Attachment> mapAttachmentsPerFileArea = new Map<Id,Attachment>();
            for (Attachment a : [SELECT Id, ParentId FROM Attachment WHERE ParentId IN :lstMainImgFileAreaIds]) {
                mapAttachmentsPerFileArea.put(a.ParentId, a);
            }
            
            String NoImageSrc;  // No_Image_Logo by default // '/servlet/servlet.FileDownload?file=' + 
            try {
                Document doc = [SELECT Id FROM Document WHERE DeveloperName = 'No_Image_Logo'];
                NoImageSrc = String.valueOf(doc.Id);  // No_Image_Logo by default
            } catch(Exception e) {
                NoImageSrc = ''; // for testing purposes
            }
            
            for (Corner__c corner : [SELECT Id, Name, Point_of_Sale__c, Location__Latitude__s, Location__Longitude__s FROM Corner__c WHERE Location__Latitude__s != null AND Geocoding_Zero_Results__c = false AND IsMine__c = 'Yes']) {
                Account pos = mapAccountsPerAccountId.get(corner.Point_of_Sale__c);
                String imageSrc = NoImageSrc;
                String tmpAddress = '';
                
                if (pos != null) {
                    tmpAddress = (pos.BillingStreet != null ? (pos.BillingStreet + '<br/>') : '') + 
                        (pos.BillingCity != null ? (pos.BillingCity + ', ') : '') + 
                        (pos.BillingPostalCode != null ? (pos.BillingPostalCode + '') : '') + '<br/>' + 
                        (pos.BillingCountry != null ? pos.BillingCountry : '');
                        
                    if (mapLastProjectsPerCornerId.get(corner.Id) != null) {
                        Id projectId = mapLastProjectsPerCornerId.get(corner.Id).Id;
                        if (mapMainImgFileAreasPerProjectId.get( projectId ) != null) {
                            Id fileAreaId = mapMainImgFileAreasPerProjectId.get( projectId ).Id;
                            if (mapAttachmentsPerFileArea.get(fileAreaId) != null) {
                                imageSrc = String.valueOf(mapAttachmentsPerFileArea.get(fileAreaId).Id); // '/servlet/servlet.FileDownload?file=' + 
                            }
                        }
                    }
                }
                
                tmpCornerWrapperList.add(new CornerWrapper(tmpAddress, double.valueOf(corner.Location__Latitude__s), double.valueOf(corner.Location__Longitude__s), (pos != null ? pos.Name : ''), imageSrc, corner.Id));
            }
            
            return tmpCornerWrapperList;
        } set;
    }
    
    public String CornerData {
        get {
            String tmpString = '[';
            for (CornerWrapper cw : lstCorners) {
                tmpString += (tmpString == '[' ? '' : ', ') + '[\'' + StaticFunctions.jsencode(cw.Name) + '\',' + cw.locationLat + ',' + cw.locationLong + ',\'' + StaticFunctions.jsencode(cw.Address) + '\',\'' + StaticFunctions.jsencode(cw.ImageURL) + '\',\'' + StaticFunctions.jsencode(cw.cornerId) + '\']';
            }
            tmpString += ']';
            return tmpString;
        } set;
    }
    
    public class CornerWrapper {
        public String Address {get; set;}
        public String Name {get; set;}
        public String ImageURL {get; set;}
        public Double locationLat {get; set;}
        public Double locationLong {get; set;}
        public Id cornerId {get; set;}
        
        public CornerWrapper(String pAddress, Double Lat, Double Lon, String pName, String pImageURL, Id cId) {
            Address = pAddress;
            Name = pName;
            ImageURL = pImageURL;
            locationLat = Lat;
            locationLong = Lon; 
            cornerId = cId;
        }
    }
}
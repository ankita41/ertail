/*****TO BE DELETED (JUY 08/09) TRANSFERRED TO ManageStakeholdersController.cls
*   @Class      :   StakeholderHandler.cls
*   @Description:   Handles all Stakeholder__c recurring actions/events.
*   @Author     :   Jacky Uy
*   @Created    :   14 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*	Thibauld		10 JUL 2014		Add: Other_Retail_Project_Manager
*	Jacky Uy		23 JUL 2014		Added More Support Stakeholders
*	Jacky Uy		06 AUG 2014		Transferred the copyStakeholderToBTQProject method to the ManageStakholdersController
*	Jacky Uy		06 AUG 2014		Add: addManualSharing() and delManualSharing() methods
*                         
*****/

public with sharing class StakeholderHandler{
	//CASCADE SETTING OF THE MANUAL SHARING OF RELATED OBJECTS WHEN A STAKEHOLDER IS ADDED
	public static void addManualSharing(List<Stakeholder__c> newList){
		/*Set<Id> btqPrjIds = new Set<Id>();
		Set<Id> stakeholderIds = new Set<Id>();
		for(Stakeholder__c s : newList){
			btqPrjIds.add(s.RECO_Project__c);
			stakeholderIds.add(s.Platform_user__c);
		}*/
		
        //BOUTIQUE PROJECTS
        /*List<RECO_Project__Share> newSharingList1 = new List<RECO_Project__Share>();
        for(RECO_Project__c p : [SELECT Id, OwnerId FROM RECO_Project__c WHERE Id IN :btqPrjIds]){
        	for(Id sid : stakeholderIds){
        		if(p.OwnerId==UserInfo.getUserId() && p.OwnerId!=sid){
        			newSharingList1.add(new RECO_Project__Share(
		                ParentId = p.Id,
		                AccessLevel = 'Edit',
		                UserOrGroupId = sid
		            ));
        		}
        	}
        }
        insert newSharingList1;*/
        
        //COST ITEMS
        /*List<Cost_Item__c> costItemsList = [SELECT RECO_Project__c, OwnerId, IsPrivate__c FROM Cost_Item__c WHERE RECO_Project__c IN :btqPrjIds];
        if(!costItemsList.isEmpty())
        	CostItemHandler.AddManualSharing(costItemsList);*/
        
        //ESTIMATE ITEMS
        /*List<Estimate_Item__Share> newSharingList3 = new List<Estimate_Item__Share>();
        for(Estimate_Item__c c : [SELECT Id, RECO_Project__r.OwnerId FROM Estimate_Item__c WHERE RECO_Project__c IN :btqPrjIds AND IsPrivate__c = false]){
        	for(Id sid : stakeholderIds){
        		if(c.RECO_Project__r.OwnerId==UserInfo.getUserId() && c.RECO_Project__r.OwnerId!=sid){
        			newSharingList3.add(new Estimate_Item__Share(
		                ParentId = c.Id,
		                AccessLevel = 'Edit',
		                UserOrGroupId = sid
		            ));
        		}
        	}
        }
        insert newSharingList3;*/
        
        //PURCHASE ORDERS
        /*List<Purchase_Order__Share> newSharingList4 = new List<Purchase_Order__Share>();
        for(Purchase_Order__c c : [SELECT Id, RECO_Project__r.OwnerId FROM Purchase_Order__c WHERE RECO_Project__c IN :btqPrjIds]){
        	for(Id sid : stakeholderIds){
        		if(c.RECO_Project__r.OwnerId==UserInfo.getUserId() && c.RECO_Project__r.OwnerId!=sid){
        			newSharingList4.add(new Purchase_Order__Share(
		                ParentId = c.Id,
		                AccessLevel = 'Edit',
		                UserOrGroupId = sid
		            ));
        		}
        	}
        }
        insert newSharingList4;*/
        
        //ATTACHMENT FOLDERS
        /*List<Attachment_Folder__c> folderList = [SELECT RECO_Project__c, Name, OwnerId FROM Attachment_Folder__c WHERE RECO_Project__c IN :btqPrjIds];
        if(!folderList.isEmpty())
        	AttachmentFolderHandler.AddManualSharing(folderList);*/
	}
	
	//CASCADE SETTING OF THE MANUAL SHARING OF RELATED OBJECTS WHEN A STAKEHOLDER IS REMOVED
	public static void delManualSharing(List<Stakeholder__c> oldList){
        /*Set<Id> btqPrjIds = new Set<Id>();
		Set<Id> stakeholderIds = new Set<Id>();
		for(Stakeholder__c s : oldList){
			btqPrjIds.add(s.RECO_Project__c);
			stakeholderIds.add(s.Platform_user__c);
		}*/
		
		//BOUTIQUE PROJECT
        //delete [SELECT Id FROM RECO_Project__Share WHERE RowCause!='Owner' AND ParentId IN :btqPrjIds AND UserOrGroupId IN :stakeholderIds];
        
        //COST ITEMS
        /*List<Cost_Item__c> costItemsList = [SELECT RECO_Project__c, OwnerId, IsPrivate__c FROM Cost_Item__c WHERE RECO_Project__c IN :btqPrjIds];
        if(!costItemsList.isEmpty())
        	CostItemHandler.DelManualSharing(costItemsList);*/
        
        //ESTIMATE ITEMS
        /*List<Estimate_Item__c> estItems = [SELECT Id FROM Estimate_Item__c WHERE RECO_Project__c IN :btqPrjIds AND IsPrivate__c = false];
        delete [SELECT Id FROM Estimate_Item__Share WHERE RowCause!='Owner' AND ParentId IN :estItems AND UserOrGroupId IN :stakeholderIds];*/
        
        //PURCHASE ORDERS
        //List<Purchase_Order__c> purchaseOrders = [SELECT Id FROM Purchase_Order__c WHERE RECO_Project__c IN :btqPrjIds];
        //delete [SELECT Id FROM Purchase_Order__Share WHERE RowCause!='Owner' AND ParentId IN :purchaseOrders AND UserOrGroupId IN :stakeholderIds];
        
	}
}
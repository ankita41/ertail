/*****
*   @Class      :   TestBoutiqueNetworkController.cls
*   @Description:   Test coverage for the BoutiqueNetworkController.cls
*   @Author     :   Priya
*   @Created    :   26 APR 2016
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*
*
*****/
@isTest
private with sharing class TestBoutiqueNetworkController{
    
    static testMethod void myUnitTest() {

    Test.startTest();

    Market__c createMarket= new Market__c(Name = 'Belgium', Code__c = 'BE', Currency__c = 'EUR', Zone__c = 'SWE');
    insert createMarket;

    Account createAccount=new Account(Market__c = createMarket.Id, 
      //RecordTypeId = rTypeId,
            Name = 'Test Account'
        );
    insert createAccount;
    
    ApexPages.StandardController stdCtrl = new ApexPages.StandardController(createMarket);
    
    BoutiqueNetworkController ctrl = new BoutiqueNetworkController(stdCtrl);
    
    Ctrl.filter();
      
    List<Boutique__c> BoutiqueList = Ctrl.BoutiqueList;

    List<SelectOption> typologyOptions = Ctrl.typologyOptions;
      
    Ctrl.filterBoutiques();
    Test.stopTest();
    }

    static testMethod void myUnitTestNew() {

      
    Profile p = [SELECT Id FROM Profile WHERE Name='NES NCAFE Retail Project Manager'];
    User thisUser  = new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='ncafeuser@nesorg.com');
    insert thisUser;

    System.runAs(thisUser) {

    Market__c createMarket= new Market__c(Name = 'Belgium', Code__c = 'BE', Currency__c = 'EUR', Zone__c = 'SWE');
    insert createMarket;

    Account createAccount=new Account(Market__c = createMarket.Id,
            //RecordTypeId = rTypeId,
            Name = 'Test Account'
        );
    insert createAccount;

    Id rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('NCafe').getRecordTypeId();
    Boutique__c btq = TestHelper.createBoutique(rTypeId, createAccount.Id);
    insert btq;

    ApexPages.StandardController stdCtrl = new ApexPages.StandardController(createMarket);
    
    BoutiqueNetworkController ctrl = new BoutiqueNetworkController(stdCtrl);
    
    Ctrl.filter();

    List<Boutique__c> BoutiqueList = Ctrl.BoutiqueList;

    List<SelectOption> typologyOptions = Ctrl.typologyOptions;
    
    Ctrl.filterBoutiques();
    
    }
    
    }
}
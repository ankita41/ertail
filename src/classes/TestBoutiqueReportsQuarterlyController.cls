@isTest(seeAllData=true)
public with sharing class TestBoutiqueReportsQuarterlyController {

    static testMethod void test(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            // create custom settings
            /*
            Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency', Main_Currency__c = 'CHF');
            insert mycs;
            
            GoogleMapsSettings__c gMap1 = new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa');
            insert gMap1;
            
            GoogleMapsSettings__c gMap2 = new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs=');
            insert gMap2;

            // create content folders
            Content_Folder__c f1 = new Content_Folder__c(Name = 'Preliminary information');
            insert f1;
            
            // Create users
            User RetailPM = TestRECOHelper.getRetailProjectManager();
            User ManagerArch = TestRECOHelper.getManagerArchitect();
            User NationalBM = TestRECOHelper.getNationalBoutiqueManager();
            User DesignArch = TestRECOHelper.getDesignArchitect();
            User PoOperator = TestRECOHelper.getPoOperator();
            User MarketDirector =  TestRECOHelper.getMarketDirector();
            
            // Create Market
            Market__c market = new Market__c(Name = 'France', Code__c = 'FR', Currency__c = 'EUR', zone__c='France');
            insert market;
            //Market__c market = [SELECT Name, Code__c, Currency__c, Zone__c FROM Market__c WHERE Code__c = 'FR'];
            
            // Create Market
            Market__c marketAOA = new Market__c(Name = 'China', Code__c = 'CH', Currency__c = 'EUR', zone__c='AOA');
            insert marketAOA;
            //Market__c marketAOA = [SELECT Name, Code__c, Currency__c, Zone__c FROM Market__c WHERE Code__c = 'FR'];
            
            // Create Market
            Market__c marketAMS = new Market__c(Name = 'Brasil', Code__c = 'BR', Currency__c = 'EUR', zone__c='AMS');
            insert marketAMS;
            //Market__c marketAMS = [SELECT Name, Code__c, Currency__c, Zone__c FROM Market__c WHERE Code__c = 'FR'];
            
            
            // Create currency exchange rates
            String tmpYear = String.valueOf(Date.today().year());
            Currency__c exrate = new Currency__c (From__c = 'CHF', To__c = 'EUR', Rate__c = 0.8, Year__c = tmpYear );
            insert exrate;
            
            Currency__c exrate2 = new Currency__c (From__c = 'EUR', To__c = 'CHF', Rate__c = 1.2, Year__c = tmpYear );
            insert exrate2;
            
            // Create Business Owner Account
            Account BusinessOwnerAcc = new Account(Name = 'Test Business Owner', Nessoft_ID__c = 'testid1234121185', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Business_Owner'),
                    Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', BillingPostalCode = 'zip', BillingCountry = 'France');
            insert BusinessOwnerAcc;
            
            // Create Global Key account
            Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Global_Key_Account'));
            insert globalKeyAcc;
            
            // Create Boutique Location Account
            Account boutiqueLocationAcc = new Account(Name = 'Test B Location', Nessoft_ID__c = 'testid12345121185', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Boutique_Location'), 
                    ParentId = globalKeyAcc.Id, Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'France');
            insert boutiqueLocationAcc;
            
            // Create Boutique Location Account
            Account boutiqueLocationAccAOA = new Account(Name = 'Test B Location', Nessoft_ID__c = 'testid11111121185', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Boutique_Location'), 
                    ParentId = globalKeyAcc.Id, Market__c = marketAOA.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'China');
            insert boutiqueLocationAccAOA;
            
            // Create Boutique Location Account
            Account boutiqueLocationAccAMS = new Account(Name = 'Test B Location', Nessoft_ID__c = 'testid122222121185', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Boutique_Location'), 
                    ParentId = globalKeyAcc.Id, Market__c = marketAMS.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'Brasil');
            insert boutiqueLocationAccAMS;
            
            Boutique__c boutique;
            boutique = new Boutique__c(Name = ' Test Boutique', Status__c = 'Opened', Typology__c = 'Boutique', Concept__c = 'Concept 2010', 
                    Business_owner__c = BusinessOwnerAcc.Id, boutique_location_account__c = boutiqueLocationAcc.Id, Net_selling_m2__c = 250);
            insert boutique;
            
            Boutique__c closedBoutique;
            boutique = new Boutique__c(Name = ' Test Boutique', Status__c = 'Closed', Close_Date__c = Date.today().addDays(-1), Typology__c = 'Boutique', Concept__c = 'Concept 2010', 
                    Business_owner__c = BusinessOwnerAcc.Id, boutique_location_account__c = boutiqueLocationAcc.Id, Net_selling_m2__c = 250);
            insert boutique;
            
            Boutique__c boutiqueAOA;
            boutiqueAOA = new Boutique__c(Name = ' Test Boutique', Status__c = 'Opened', Typology__c = 'Boutique', Concept__c = 'Concept 2010', 
                    Business_owner__c = BusinessOwnerAcc.Id, boutique_location_account__c = boutiqueLocationAcc.Id, Net_selling_m2__c = 250);
            insert boutiqueAOA;
            
            Boutique__c boutiqueAMS;
            boutiqueAMS = new Boutique__c(Name = ' Test Boutique', Status__c = 'Planned', Typology__c = 'Boutique', Concept__c = 'Concept 2010', 
                    Business_owner__c = BusinessOwnerAcc.Id, boutique_location_account__c = boutiqueLocationAcc.Id, Net_selling_m2__c = 250);
            insert boutiqueAMS;
            
            RECO_Project__c project;
            
            System.runAs (RetailPM) {
                project = new RECO_Project__c( Status__c = 'Planned', 
                        Process_step__c = 'Stakeholders definition', Boutique__c = boutique.Id, Project_Name__c = 'Description', Expected_soft_opening_date__c = Date.today(), 
                        Boutique_CapEx_request_status__c = 'To be done', Project_Type__c = 'Minor refurbishment');
                insert project;
                
                project = new RECO_Project__c( Status__c = 'Planned', 
                        Process_step__c = 'Stakeholders definition', Boutique__c = boutique.Id, Project_Name__c = 'Description', Expected_soft_opening_date__c = Date.today(), 
                        Boutique_CapEx_request_status__c = 'To be done', Project_Type__c = 'Minor refurbishment');
                insert project;
                
                project = new RECO_Project__c( Status__c = 'Planned', 
                        Process_step__c = 'Stakeholders definition', Boutique__c = boutiqueAOA.Id, Project_Name__c = 'Description', Expected_soft_opening_date__c = Date.today(), Effective_soft_opening_date__c = Date.today(), 
                        Boutique_CapEx_request_status__c = 'To be done', Project_Type__c = 'New');
                insert project;
                
                project = new RECO_Project__c( Status__c = 'Planned', 
                        Process_step__c = 'Stakeholders definition', Boutique__c = boutiqueAMS.Id, Project_Name__c = 'Description', Expected_soft_opening_date__c = Date.today(), Effective_soft_opening_date__c = Date.today(), 
                        Boutique_CapEx_request_status__c = 'To be done', Project_Type__c = 'Extension');
                insert project;
            }
            */
            
                        
            Test.startTest();
            
            BoutiqueReportsQuarterlyController brc = new BoutiqueReportsQuarterlyController();
            brc.getNbBoutiquesPerCountrytable();            
            brc.AllZones.getPieData();
        
            
            // Test BoutiqueReportHelper
            System.assertEquals('1st', ReportsHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 1, 1)));
            System.assertEquals('2nd', ReportsHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 4, 1)));
            System.assertEquals('3rd', ReportsHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 7, 1)));
            System.assertEquals('4th', ReportsHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 10, 1)));
            
            System.assertEquals(Date.newInstance(Date.today().Year(), 1, 1), ReportsHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 1, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 4, 1), ReportsHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 4, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 7, 1), ReportsHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 7, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 10, 1), ReportsHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 10, 15)));
            
            System.assertEquals(0, ReportsHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 1, 15)));
            System.assertEquals(1, ReportsHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 4, 15)));
            System.assertEquals(2, ReportsHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 7, 15)));
            System.assertEquals(3, ReportsHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 10, 15)));

            System.assertEquals(Date.newInstance(Date.today().Year(), 4, 1), ReportsHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 1, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 7, 1), ReportsHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 4, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 10, 1), ReportsHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 7, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year()+1, 1, 1), ReportsHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 10, 15)));

            System.assertEquals('12\'345', BoutiqueReportHelper.FormatInteger(12345));
            
            // Test BoutiqueReportsDispatchController
            BoutiqueReportsDispatchController c = new BoutiqueReportsDispatchController();
            List<BoutiqueReportsDispatchController.Quarter> qList = c.Quarters;
            String firstQuarterLabel = qList[0].getQuarterLabel();        
            Integer yearStr = c.Year;
            String monthStr = c.MonthStr;
            Test.stopTest();
        }
    }
    static testMethod void testNCafe(){
        Profile p = [SELECT Id FROM Profile WHERE Name='NES NCAFE Retail Project Manager'];
        User thisUser  = new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='ncafeuser@nesorg.com');

        System.runAs ( thisUser ) {
            // create custom settings
            /*
            Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency', Main_Currency__c = 'CHF');
            insert mycs;
            
            GoogleMapsSettings__c gMap1 = new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa');
            insert gMap1;
            
            GoogleMapsSettings__c gMap2 = new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs=');
            insert gMap2;

            // create content folders
            Content_Folder__c f1 = new Content_Folder__c(Name = 'Preliminary information');
            insert f1;
            
            // Create users
            User RetailPM = TestRECOHelper.getRetailProjectManager();
            User ManagerArch = TestRECOHelper.getManagerArchitect();
            User NationalBM = TestRECOHelper.getNationalBoutiqueManager();
            User DesignArch = TestRECOHelper.getDesignArchitect();
            User PoOperator = TestRECOHelper.getPoOperator();
            User MarketDirector =  TestRECOHelper.getMarketDirector();
            
            // Create Market
            Market__c market = new Market__c(Name = 'France', Code__c = 'FR', Currency__c = 'EUR', zone__c='France');
            insert market;
            //Market__c market = [SELECT Name, Code__c, Currency__c, Zone__c FROM Market__c WHERE Code__c = 'FR'];
            
            // Create Market
            Market__c marketAOA = new Market__c(Name = 'China', Code__c = 'CH', Currency__c = 'EUR', zone__c='AOA');
            insert marketAOA;
            //Market__c marketAOA = [SELECT Name, Code__c, Currency__c, Zone__c FROM Market__c WHERE Code__c = 'FR'];
            
            // Create Market
            Market__c marketAMS = new Market__c(Name = 'Brasil', Code__c = 'BR', Currency__c = 'EUR', zone__c='AMS');
            insert marketAMS;
            //Market__c marketAMS = [SELECT Name, Code__c, Currency__c, Zone__c FROM Market__c WHERE Code__c = 'FR'];
            
            
            // Create currency exchange rates
            String tmpYear = String.valueOf(Date.today().year());
            Currency__c exrate = new Currency__c (From__c = 'CHF', To__c = 'EUR', Rate__c = 0.8, Year__c = tmpYear );
            insert exrate;
            
            Currency__c exrate2 = new Currency__c (From__c = 'EUR', To__c = 'CHF', Rate__c = 1.2, Year__c = tmpYear );
            insert exrate2;
            
            // Create Business Owner Account
            Account BusinessOwnerAcc = new Account(Name = 'Test Business Owner', Nessoft_ID__c = 'testid1234121185', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Business_Owner'),
                    Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', BillingPostalCode = 'zip', BillingCountry = 'France');
            insert BusinessOwnerAcc;
            
            // Create Global Key account
            Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Global_Key_Account'));
            insert globalKeyAcc;
            
            // Create Boutique Location Account
            Account boutiqueLocationAcc = new Account(Name = 'Test B Location', Nessoft_ID__c = 'testid12345121185', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Boutique_Location'), 
                    ParentId = globalKeyAcc.Id, Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'France');
            insert boutiqueLocationAcc;
            
            // Create Boutique Location Account
            Account boutiqueLocationAccAOA = new Account(Name = 'Test B Location', Nessoft_ID__c = 'testid11111121185', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Boutique_Location'), 
                    ParentId = globalKeyAcc.Id, Market__c = marketAOA.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'China');
            insert boutiqueLocationAccAOA;
            
            // Create Boutique Location Account
            Account boutiqueLocationAccAMS = new Account(Name = 'Test B Location', Nessoft_ID__c = 'testid122222121185', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Boutique_Location'), 
                    ParentId = globalKeyAcc.Id, Market__c = marketAMS.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'Brasil');
            insert boutiqueLocationAccAMS;
            
            Boutique__c boutique;
            boutique = new Boutique__c(Name = ' Test Boutique', Status__c = 'Opened', Typology__c = 'Boutique', Concept__c = 'Concept 2010', 
                    Business_owner__c = BusinessOwnerAcc.Id, boutique_location_account__c = boutiqueLocationAcc.Id, Net_selling_m2__c = 250);
            insert boutique;
            
            Boutique__c closedBoutique;
            boutique = new Boutique__c(Name = ' Test Boutique', Status__c = 'Closed', Close_Date__c = Date.today().addDays(-1), Typology__c = 'Boutique', Concept__c = 'Concept 2010', 
                    Business_owner__c = BusinessOwnerAcc.Id, boutique_location_account__c = boutiqueLocationAcc.Id, Net_selling_m2__c = 250);
            insert boutique;
            
            Boutique__c boutiqueAOA;
            boutiqueAOA = new Boutique__c(Name = ' Test Boutique', Status__c = 'Opened', Typology__c = 'Boutique', Concept__c = 'Concept 2010', 
                    Business_owner__c = BusinessOwnerAcc.Id, boutique_location_account__c = boutiqueLocationAcc.Id, Net_selling_m2__c = 250);
            insert boutiqueAOA;
            
            Boutique__c boutiqueAMS;
            boutiqueAMS = new Boutique__c(Name = ' Test Boutique', Status__c = 'Planned', Typology__c = 'Boutique', Concept__c = 'Concept 2010', 
                    Business_owner__c = BusinessOwnerAcc.Id, boutique_location_account__c = boutiqueLocationAcc.Id, Net_selling_m2__c = 250);
            insert boutiqueAMS;
            
            RECO_Project__c project;
            
            System.runAs (RetailPM) {
                project = new RECO_Project__c( Status__c = 'Planned', 
                        Process_step__c = 'Stakeholders definition', Boutique__c = boutique.Id, Project_Name__c = 'Description', Expected_soft_opening_date__c = Date.today(), 
                        Boutique_CapEx_request_status__c = 'To be done', Project_Type__c = 'Minor refurbishment');
                insert project;
                
                project = new RECO_Project__c( Status__c = 'Planned', 
                        Process_step__c = 'Stakeholders definition', Boutique__c = boutique.Id, Project_Name__c = 'Description', Expected_soft_opening_date__c = Date.today(), 
                        Boutique_CapEx_request_status__c = 'To be done', Project_Type__c = 'Minor refurbishment');
                insert project;
                
                project = new RECO_Project__c( Status__c = 'Planned', 
                        Process_step__c = 'Stakeholders definition', Boutique__c = boutiqueAOA.Id, Project_Name__c = 'Description', Expected_soft_opening_date__c = Date.today(), Effective_soft_opening_date__c = Date.today(), 
                        Boutique_CapEx_request_status__c = 'To be done', Project_Type__c = 'New');
                insert project;
                
                project = new RECO_Project__c( Status__c = 'Planned', 
                        Process_step__c = 'Stakeholders definition', Boutique__c = boutiqueAMS.Id, Project_Name__c = 'Description', Expected_soft_opening_date__c = Date.today(), Effective_soft_opening_date__c = Date.today(), 
                        Boutique_CapEx_request_status__c = 'To be done', Project_Type__c = 'Extension');
                insert project;
            }
            */
            
                        
            Test.startTest();
            BoutiqueReportsQuarterlyController brc = new BoutiqueReportsQuarterlyController();
            brc.pickMonth='12';
            brc.pickYear='2016';
            brc.BoutiqueReportsControllerInit();
            brc.getNbBoutiquesPerCountrytable();            
            brc.AllZones.getPieData();
        
            
            // Test BoutiqueReportHelper
            System.assertEquals('1st', ReportsHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 1, 1)));
            System.assertEquals('2nd', ReportsHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 4, 1)));
            System.assertEquals('3rd', ReportsHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 7, 1)));
            System.assertEquals('4th', ReportsHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 10, 1)));
            
            System.assertEquals(Date.newInstance(Date.today().Year(), 1, 1), ReportsHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 1, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 4, 1), ReportsHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 4, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 7, 1), ReportsHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 7, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 10, 1), ReportsHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 10, 15)));
            
            System.assertEquals(0, ReportsHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 1, 15)));
            System.assertEquals(1, ReportsHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 4, 15)));
            System.assertEquals(2, ReportsHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 7, 15)));
            System.assertEquals(3, ReportsHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 10, 15)));

            System.assertEquals(Date.newInstance(Date.today().Year(), 4, 1), ReportsHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 1, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 7, 1), ReportsHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 4, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year(), 10, 1), ReportsHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 7, 15)));
            System.assertEquals(Date.newInstance(Date.today().Year()+1, 1, 1), ReportsHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 10, 15)));

            System.assertEquals('12\'345', BoutiqueReportHelper.FormatInteger(12345));
            
            // Test BoutiqueReportsDispatchController
            BoutiqueReportsDispatchController c = new BoutiqueReportsDispatchController();
            List<BoutiqueReportsDispatchController.Quarter> qList = c.Quarters;
            String firstQuarterLabel = qList[0].getQuarterLabel();        
            Integer yearStr = c.Year;
            String monthStr = c.MonthStr;
            Test.stopTest();
        }
    }
    
}
/*****
*   @Class      :   TestBoutiqueWorkloadController_PM.cls
*   @Description:   Test coverage of the TestBoutiqueWorkloadController_PM.cls
*   @Author     :   TPA
*   @Created    :   25 03 2015
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*****/
@isTest(seeAllData=false)
public with sharing class TestBoutiqueWorkloadController_PM {
    static testMethod void test(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        btq.Typology__c = 'Boutique';
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        bProj.Project_Type__c = 'New';
        insert bProj;
        
        
        System.runAs ( thisUser ) {
            
            Test.startTest();
            
            BoutiqueWorkloadController_PM bwcpm = new BoutiqueWorkloadController_PM();    
            List<SelectOption> yo = bwcpm.yearsOpts;
            yo = bwcpm.rpmOpts;
            bwcpm.selectedYears = '[2014,2015,2016,2017]';
            bwcpm.selectedProjectTypes = '[New,Relocation,Extension,Closure,Concept Upgrade,Major Refurbishment,Minor Refurbishment]';
            bwcpm.selectedStatus = '[Open,Completed,Cancelled,Closure,Feasibility,Furniture Installation,Handover,Planned,Tender,Construction,Design]';
            bwcpm.selectedBoutiqueTypes = '[Boutique,Boutique Popup Indoor,Boutique Popup Outdoor,Boutique bar,Boutique corner,Boutique in mall,Boutique in shop,N Cube]';
            
            bwcpm.queryProjectsData();            
            
            BoutiqueWorkloadController_PM.ProjectsWrapper pw = bwcpm.projects;
            List<String> sortRPM = pw.SortedRPM;
            Map<String, BoutiqueWorkloadController_PM.ProjectsForRetailProjectManagerWrapper> projectsForRetailProjectManagers = pw.ProjectsForRetailProjectManagers;
            Integer nbProjects = pw.NbProjects;
                        
            bwcpm.printPage();
                             
            Test.stopTest();
        }
    }
    
    
}
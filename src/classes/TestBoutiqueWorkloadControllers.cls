@isTest(seeAllData=true)
public with sharing class TestBoutiqueWorkloadControllers {
    static testMethod void test(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            
            Test.startTest();
            
            BoutiqueWorkloadController_Supplier bwcs = new BoutiqueWorkloadController_Supplier();
            bwcs.getCompanyOpts();
            bwcs.selectedCompanyTypes = '[FS,GC,DA,MA]';
            bwcs.queryProjectsForCompanyTypes();
            bwcs.getYearOpts();
            bwcs.selectedYears = '[2015]';
            bwcs.filterProjectTypesTypologiesStatusAndCompanies();
            bwcs.getProjectTypeOpts();
            bwcs.selectedProjectTypes = '[New,Relocation,Extension,Closure,Concept Upgrade,Major Refurbishment,Minor Refurbishment]';
            bwcs.filterTypologiesAndCompanies();
            bwcs.getTypologyOpts();
            bwcs.selectedTypologies = '[Boutique,Boutique Popup Indoor,Boutique Popup Outdoor,Boutique bar,Boutique corner,Boutique in mall,Boutique in shop,N Cube]';
            bwcs.selectedStatus = '[Open,Completed,Cancelled,Closure,Feasibility,Furniture Installation,Handover,Planned,Tender,Construction,Design]';
            bwcs.filterCompanies();
            bwcs.filterProjects();
            bwcs.getStatusOpts();
            bwcs.printPage();
            bwcs.refresh();
            
            Map<String, Set<String>> ac = bwcs.availableCompaniesForSelectedYearsProjectTypesTypologies;
            String str = bwcs.SelectedCompanyTypesStr;
            str = bwcs.SelectedYearsStr;
            str = bwcs.SelectedProjectTypesStr;
            str = bwcs.SelectedStatusStr;
            
            List<Integer> sy = bwcs.filteredProjects.SortedYears;
            Integer i = bwcs.filteredProjects.NbProjects;
            i = bwcs.filteredProjects.NbUnassignedProjects;
            
            for (BoutiqueWorkloadController_Supplier.ProjectsForYearWrapper pfyw : bwcs.filteredProjects.ProjectsForYear.values()){
                i = pfyw.Year;
                i = pfyw.NbProjects;
                i = pfyw.NbUnassignedProjects;
                i = pfyw.NbMonths;
                List<String> sompanies = pfyw.SortedCompanies;
                List<Integer> sMonths = pfyw.SortedMonths;
                
                for(BoutiqueWorkloadController_Supplier.ProjectsForMonthWrapper pfmw  : pfyw.UnassignedProjectsForMonths.values()){
                    Date d = pfmw.OpeningMonth;
                    i = pfmw.NbProjects;
                    sompanies = pfmw.SortedStatus;
                    
                    for (BoutiqueWorkloadController_Supplier.ProjectsForStatusWrapper pfsw : pfmw.ProjectsForStatus.values()){
                        String status = pfsw.Status;
                        i = pfsw.NbProjects;
                        sompanies = pfsw.SortedCountries;
                        
                        for(BoutiqueWorkloadController_Supplier.ProjectsForCountryWrapper pfcw : pfsw.ProjectsForCountries.values()){
                            String n = pfcw.CountryName;
                            sompanies = pfcw.SortedCities;
                            i = pfcw.NbProjects;
                            
                            for(BoutiqueWorkloadController_Supplier.ProjectsForCityWrapper pfccw : pfcw.ProjectsForCities.values()){
                                n = pfccw.CityName;
                                List<Id> sProjects = pfccw.SortedProjects;
                                i = pfccw.NbProjects;
                                
                                for(BoutiqueWorkloadController_Supplier.ProjectWrapper p : pfccw.Projects.values()){
                                    Id ii = p.ProjectId;
                                    String s = p.ProjectName;
                                    ii = p.CompanyId;
                                    s = p.CompanyName;
                                    Double db = p.FSCost;
                                    db = p.FSCostPerSqm;
                                    db = p.GCCost;
                                    db = p.GCCostPerSqm;
                                    db = p.DACost;
                                    db = p.DACostPerSqm;
                                    db = p.MACost;
                                    db = p.MACostPerSqm;
                                    s = p.Status;
                                    d = p.OpeningDate;
                                    s = p.ProjectType;
                                    s = p.Country;
                                    s = p.City;
                                    s = p.BoutiqueName;
                                    ii = p.BoutiqueId;
                                    s = p.TypologyShort;
                                    Decimal dd = p.SqrM2;
                                    dd = p.SumOfAmounts;
                                    dd = p.SumOfCostPerSqm;
                                    s = p.Typology;
                                }
                            }
                        }

                    }
                }

            }
                        
            bwcs.selectedCompanyTypes = '[All]';
           // bwcs.queryProjectsForCompanyTypes();
            bwcs.getYearOpts();
            bwcs.selectedYears = '[' + (Date.Today().Year() - 1) + ']';
            bwcs.filterProjectTypesTypologiesStatusAndCompanies();
            bwcs.getProjectTypeOpts();
            bwcs.selectedProjectTypes = '[All]';
            bwcs.filterTypologiesAndCompanies();
            bwcs.getTypologyOpts();
            bwcs.selectedTypologies = '[All]';
            bwcs.selectedStatus = '[All]';
            bwcs.filterCompanies();
           // bwcs.filterProjects();
            Test.stopTest();
        }
    }
    
}
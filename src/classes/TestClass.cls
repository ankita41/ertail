/*
*   @Class      :   TestClass.cls
*   @Description:   XXX
*   @Author     :   XXX
*   @Created    :   XXX
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy        08 MAY 2014
*   Thibauld        20 MAY 2014    Remove Amounts from Forecast                       
*/

@isTest  //(SeeAllData=true)
private class TestClass {
    static testMethod void test() {
        User thisUser = TestHelper.CurrentUser;
        System.runAs ( thisUser ) {
            // create custom settings
            Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
            mycs.Main_Currency__c = 'CHF';
            insert mycs;
              
            List<GoogleMapsSettings__c> gms = new List<GoogleMapsSettings__c>();
            GoogleMapsSettings__c mycs2 = new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa');
            gms.add(mycs2);
            
            GoogleMapsSettings__c mycs3 = new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs=');
            gms.add(mycs3);
            
            insert gms;
            
// Create users
            Map<String, Profile> mapProfilePerProfileName = new Map<String, Profile>();
            List<Profile> lstProfiles = [SELECT Id, Name FROM Profile];
            for (Profile p : lstProfiles) {
                mapProfilePerProfileName.put(p.Name, p);
            }
            
            Profile p = mapProfilePerProfileName.get('NES Corner Supplier'); 
            List<User> userList = new List<User>();
            User supp = new User(Alias = 'supp', Email='supplier@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com');
            userList.add(supp);
            
            p = mapProfilePerProfileName.get('NES Corner Architect'); 
            User arch = new User(Alias = 'arch', Email='arch@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com');
            userList.add(arch);
            
            p = mapProfilePerProfileName.get('NES Market Local Manager'); 
            UserRole r = [SELECT Id FROM UserRole WHERE Name = 'Germany'];
            User mngr = new User(Alias = 'mngr', Email='mngr@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='mngr@testorg.com');
            userList.add(mngr);
            //mngr = [SELECT Id, Name FROM User WHERE Username = 'market.nespresso@gmail.com.uat'];
            
            p = mapProfilePerProfileName.get('NES Market Sales Promoter');  
            User sale = new User(Alias = 'sale', Email='sale@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='sale@testorg.com');
            userList.add(sale);
            
            p = mapProfilePerProfileName.get('NES Trade/Proc Manager'); 
            User trad = new User(Alias = 'trad', Email='trad@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='trad@testorg.com');
            userList.add(trad);
            insert userList;
            
            // Create Market
            List<Market__c> marketList = new List<Market__c>();
            Market__c market = new Market__c(Name = 'Germany', Code__c = 'GER', NES_Project_Manager__c = mngr.Id, 
                    Default_Architect__c = arch.Id, Default_Supplier__c = supp.Id, Currency__c = 'USD');
            marketList.add(market);
            
            Market__c market2 = new Market__c(Name = 'Hungary', Code__c = 'HU', NES_Project_Manager__c = mngr.Id, 
                    Default_Architect__c = arch.Id, Default_Supplier__c = supp.Id, Currency__c = 'HUF');
            marketList.add(market2);
            
            insert marketList;
            
            // Create forecasts
            List<Objective__c> objList = new List<Objective__c>();
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(Date.today().addYears(-3).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(Date.today().addYears(-2).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(Date.today().addYears(-1).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = market.Id
                            , year__c = String.valueOf(Date.today().year())
                            , New_Members_Market_or_Region_Growth__c = 1
                            , Monthly_Average_Consumption__c = 2
                            , Caps_Cost_in_LC__c = 3
                            , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            insert(objList);
            
            // Create Global Key account (to use as parent for the POS Group)
            Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Key Account').getRecordTypeId();
            Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = rtId, Market__c = market.Id );
            insert globalKeyAcc;
            
            // Create POS Group account (to use as parent for the POS Local)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Group').getRecordTypeId();
            Account posGroupAcc = new Account(Name = 'Test POS Group Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = globalKeyAcc.Id);
            insert posGroupAcc;
            
            // Create POS Local account (to use as parent for the POS)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Local').getRecordTypeId();
            Account posLocalAcc = new Account(Name = 'Test POS Local Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = posGroupAcc.Id);
            insert posLocalAcc;
            
            // Create POS Account
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
            Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                    Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'country');
            insert posAcc;
            
            // Create POS Contacts
            List<Contact> contactList = new List<Contact>();
            Contact storeElectrician = new Contact (LastName = 'Electrician', AccountId = posAcc.Id, Function__c = 'Store Electrician');
            contactList.add(storeElectrician);
            Contact storeContact = new Contact (LastName = 'StContact', AccountId = posAcc.Id, Function__c = 'Store Contact');
            contactList.add(storeContact);
            Contact storeContact2 = new Contact (LastName = 'StContact2', AccountId = posAcc.Id, Function__c = 'Store Contact');
            contactList.add(storeContact2);
            insert contactList;
            
            // Create Corner - Removed 30 MAY 2014 as Corner are now automatically created
/*
            Corner__c corner = new Corner__c(Point_of_Sale__c = posAcc.Id, Unit__c = 'cm', Height_under_ceiling__c = 1000, 
                    Walls_material__c = 'abc', Floor_material__c = 'abc', Electricity_coming_from__c = 'Floor', 
                    Water_supply_needed__c = 'Yes');
            insert corner;
*/            
            // Create currency exchange rates
            List<Currency__c> currencyList = new List<Currency__c>();
            String tmpYear = String.valueOf(Date.today().year());
            Currency__c exrate = new Currency__c (From__c = 'CHF', To__c = 'USD', Rate__c = 1.1, Year__c = tmpYear );
            currencyList.add(exrate);
            
            Currency__c exrate2 = new Currency__c (From__c = 'USD', To__c = 'CHF', Rate__c = 0.9, Year__c = tmpYear );
            currencyList.add(exrate2);
            
            Currency__c exrate3 = new Currency__c (From__c = 'EUR', To__c = 'CHF', Rate__c = 1.2, Year__c = tmpYear );
            currencyList.add(exrate3);
            
            Currency__c exrate4 = new Currency__c (From__c = 'USD', To__c = 'EUR', Rate__c = 1.3, Year__c = tmpYear );
            currencyList.add(exrate4);
            
            Currency__c exrate5 = new Currency__c (From__c = 'CHF', To__c = 'USD', Rate__c = 1.1, Year__c = tmpYear );
            currencyList.add(exrate5);
            
            insert currencyList;
            
            // Create document(s)
            Folder docFolder = [SELECT Id FROM Folder WHERE Name = 'Snag Images'];
            Document noImgDoc = new Document(Name='Test doc', DeveloperName = 'No_Image_Logo_test', FolderId = docFolder.Id, Body = blob.valueOf(''));
            insert noImgDoc;
            
        
            // Create Project without existing corner (create the corner automatically)
            rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
            Project__c proj2 = new Project__c (
                RecordTypeId = rtId, 
                Nb_of_machines_in_display__c = '1', 
                Nb_of_machines_in_stock__c = '2', 
                Floor__c = 'No', 
                Old_NES_Corner_to_remove__c = 'No', 
                Ecolaboration_module__c = 'No', 
                LCD_screen__c = 'No', 
                Visual__c = 'No', 
                Estimated_Budget__c = 10000, 
                NES_Sales_Promoter__c = sale.Id, 
                Store_Contact__c = storeContact2.Id, 
                Targeted_Installation_Date__c = date.newinstance(2013, 20, 9), 
                Height_non_linear__c = 1000, 
                Width_non_linear__c = 1000, 
                Length_non_linear__c = 1000, 
                Corner_Type_r__c = 'Island Corner', 
                Corner_Point_of_Sale_r__c = posAcc.Id, 
                Corner_Unit_r__c = 'cm', 
                Height_under_ceiling_r__c = 1000, 
                Walls_material_r__c = 'b', Floor_material_r__c = 'c', 
                Corner_Generation_r__c = '2011', 
                Electricity_coming_from_r__c = 'Floor', 
                Water_supply_needed_r__c = 'No',
                Key_Account__c = posLocalAcc.Id);
            insert proj2;
            
            // get newly inserted project, check the new corner has been created and filled with the given values
            proj2 = [SELECT Id, Corner__c FROM Project__c WHERE Id = :proj2.Id];
            Corner__c newcorner = [SELECT Id, Corner_Type__c, Point_of_Sale__c FROM Corner__c WHERE Id = :proj2.Corner__c];
            
            System.assertEquals(newcorner.Corner_Type__c, 'Island Corner'); 
            System.assertEquals(newcorner.Point_of_Sale__c, posAcc.Id);
            
            // Create Project without existing corner and without new corner fields (test errors)
            try {
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
                Project__c proj3 = new Project__c (
                    RecordTypeId = rtId, 
                    Nb_of_machines_in_display__c = '1', 
                    Nb_of_machines_in_stock__c = '2', 
                    Floor__c = 'No', 
                    Old_NES_Corner_to_remove__c = 'No', 
                    Ecolaboration_module__c = 'No', 
                    LCD_screen__c = 'No', 
                    Visual__c = 'No', 
                    Estimated_Budget__c = 10000, 
                    NES_Sales_Promoter__c = sale.Id, 
                    Store_Contact__c = storeContact2.Id, 
                    Targeted_Installation_Date__c = date.newinstance(2013, 20, 9), 
                    Height_non_linear__c = 1000, 
                    Width_non_linear__c = 1000, 
                    Length_non_linear__c = 1000, 
                    Corner_Type_r__c = 'Island Corner');
                insert proj3;
            } catch (Exception e) {
            }
            
            // Create Project without existing corner and without new corner fields (test errors)
            try {
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
                Project__c proj3 = new Project__c (RecordTypeId = rtId, Nb_of_machines_in_display__c = '20', Nb_of_machines_in_stock__c = '10', 
                        Floor__c = 'No', Old_NES_Corner_to_remove__c = 'No', Ecolaboration_module__c = 'No', LCD_screen__c = 'No', 
                        Visual__c = 'No', Estimated_Budget__c = 10000, NES_Sales_Promoter__c = sale.Id, Store_Contact__c = storeContact2.Id, 
                        Targeted_Installation_Date__c = date.newinstance(2013, 20, 9), Height_non_linear__c = 1000, Width_non_linear__c = 1000, 
                        Length_non_linear__c = 1000);
                insert proj3;
            } catch (Exception e) {
            }
            
            Test.startTest();
        ///////////////////////////////////////////////////
        // TEST Project_File_Area triggers CopyDefaultValuesFromProject, CheckThereIsOnlyOneMainProjectImage
            //system.runAs(mngr) {
                rtId = Schema.SObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Photos').getRecordTypeId();
                
                Project_File_Area__c file1 = new Project_File_Area__c(Name = 'test', Project__c = proj2.Id, Main_Project_Image__c = true);
                insert file1;
                
                Project_File_Area__c file2 = new Project_File_Area__c(Name = 'test2', Project__c = proj2.Id, Main_Project_Image__c = false);
                insert file2;
            
            // check default values have been copied from the project
                file1 = [SELECT Id, Architect__c, Main_Project_Image__c, Supplier__c FROM Project_File_Area__c WHERE Id = :file1.Id];
                System.assertEquals(file1.Main_Project_Image__c, true);
                System.assertEquals(file1.Architect__c, arch.Id);
                System.assertEquals(file1.Supplier__c, supp.Id);
                
                Project_File_Area__c file3 = new Project_File_Area__c(Name = 'test2', Project__c = proj2.Id, Main_Project_Image__c = true);
                insert file3;
                
                // check the new main image is the only main image
                file1 = [SELECT Id, Main_Project_Image__c FROM Project_File_Area__c WHERE Id = :file1.Id];
                file2 = [SELECT Id, Main_Project_Image__c FROM Project_File_Area__c WHERE Id = :file2.Id];
                file3 = [SELECT Id, Main_Project_Image__c FROM Project_File_Area__c WHERE Id = :file3.Id];
                System.assertEquals(file1.Main_Project_Image__c, false);
                System.assertEquals(file2.Main_Project_Image__c, false);
                System.assertEquals(file3.Main_Project_Image__c, true);  
            //}
            
        ///////////////////////////////////////////////////
        // TEST Quotation trigger CopyDefaultValuesFromProjectToQuotation
            
            rtId = Schema.SObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
            Quotation__c quot = new Quotation__c(Proforma_no__c = '1', Project__c = proj2.Id, Furnitures__c = 100, 
                    Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD');
            insert quot;
            
            quot = [SELECT Id, Total_in_main_currency__c, Supplier__c, Architect__c, Nespresso_Contact__c FROM Quotation__C WHERE Id = :quot.Id];
            
            System.assertEquals(quot.Total_in_main_currency__c, 270);
            System.assertEquals(quot.Supplier__c, supp.Id);
            System.assertEquals(quot.Architect__c, arch.Id);
            System.assertEquals(quot.Nespresso_Contact__c, mngr.Id);
            
        ///////////////////////////////////////////////////
        // TEST Handover Report triggers CopyDefaultValuesFromProjectToHandoverReport, CreateHandoverChecklist
            Handover_Report__c hr = new Handover_Report__c(Installer_Arrival_Date__c = Date.Today(), Installer_Arrival_Time__c = '12:00', Installer_Departure_Date__c = Date.Today(), Installer_Departure_Time__c = '12:00', 
                    Project__c = proj2.Id, General_cleaning__c = 'Yes', Supplier_on_time__c = 'Yes', Installation_feedback__c = 'a', 
                    Installation_area_cleared_before__c = 'Yes');
            try {
                insert hr;
                // fails because no furniture inventory
            } catch (Exception e) {}
            
            Furniture__c furn = new Furniture__c(Name = 'test furniture', Active__c = true, Completed_items__c = true);
            insert furn;
            
            Furnitures_Inventory__c finv = new Furnitures_Inventory__c(Furniture__c = furn.Id, Project__c = proj2.Id, Quantity__c = 1);
            insert finv;
            
            insert hr;
            
            hr = [SELECT Id, Supplier__c, Architect__c, Nespresso_Contact__c FROM Handover_Report__c WHERE Id = :hr.Id];
            System.assertEquals(hr.Supplier__c, supp.Id);
            System.assertEquals(hr.Architect__c, arch.Id);
            System.assertEquals(hr.Nespresso_Contact__c, mngr.Id);
            
            List<Handover_Checklist__c> cklist = [SELECT Id FROM Handover_Checklist__c WHERE Handover_Report__c = :hr.Id];
            System.assertEquals(cklist.size(), 1);
            
            Test.stopTest();
        }
    }
}
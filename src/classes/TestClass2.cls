@isTest  (SeeAllData=true)
private class TestClass2 {
    static testMethod void test() {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        	// create custom settings
      		/*
      		Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
		    mycs.Main_Currency__c = 'CHF';
		    insert mycs;
		      
		    GoogleMapsSettings__c mycs2 = new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa');
		    insert mycs2;
		    
		    GoogleMapsSettings__c mycs3 = new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs=');
		    insert mycs3;
        	
            // Create users
            Profile p = [SELECT Id FROM Profile WHERE Name='NES Corner Supplier'];
            User supp = new User(Alias = 'supp', Email='supplier@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com');
            insert supp;
            
            p = [SELECT Id FROM Profile WHERE Name='NES Corner Architect']; 
            User arch = new User(Alias = 'arch', Email='arch@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com');
            insert arch;
            
            p = [SELECT Id FROM Profile WHERE Name='NES Market Local Manager']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name = 'Germany'];
            User mngr = new User(Alias = 'mngr', Email='mngr@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='mngr@testorg.com');
            insert mngr;
            //mngr = [SELECT Id, Name FROM User WHERE Username = 'market.nespresso@gmail.com.uat'];
            
            p = [SELECT Id FROM Profile WHERE Name='NES Market Sales Promoter']; 
            User sale = new User(Alias = 'sale', Email='sale@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='sale@testorg.com');
            insert sale;
            
            p = [SELECT Id FROM Profile WHERE Name='NES Trade/Proc Manager']; 
            User trad = new User(Alias = 'trad', Email='trad@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='trad@testorg.com');
            insert trad;
            
            // Create Market
            Market__c market = new Market__c(Name = 'Germany', Code__c = 'GER', NES_Project_Manager__c = mngr.Id, 
                    Default_Architect__c = arch.Id, Default_Supplier__c = supp.Id, Currency__c = 'USD');
            insert market;
            
            Market__c market2 = new Market__c(Name = 'Hungary', Code__c = 'HU', NES_Project_Manager__c = mngr.Id, 
                    Default_Architect__c = arch.Id, Default_Supplier__c = supp.Id, Currency__c = 'HUF');
            insert market2;
           
            // Create Global Key account (to use as parent for the POS Group)
            Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Key Account').getRecordTypeId();
            Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = rtId);
            insert globalKeyAcc;
            
            // Create POS Group account (to use as parent for the POS Local)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Group').getRecordTypeId();
            Account posGroupAcc = new Account(Name = 'Test POS Group Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = globalKeyAcc.Id);
            insert posGroupAcc;
            
            // Create POS Local account (to use as parent for the POS)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Local').getRecordTypeId();
            Account posLocalAcc = new Account(Name = 'Test POS Local Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = posGroupAcc.Id);
            insert posLocalAcc;
            
            // Create POS Account
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
            Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                    Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'country');
            insert posAcc;
            
            // Create POS Contacts
            Contact storeElectrician = new Contact (LastName = 'Electrician', AccountId = posAcc.Id, Function__c = 'Store Electrician');
            insert storeElectrician;
            Contact storeContact = new Contact (LastName = 'StContact', AccountId = posAcc.Id, Function__c = 'Store Contact');
            insert storeContact;
            Contact storeContact2 = new Contact (LastName = 'StContact2', AccountId = posAcc.Id, Function__c = 'Store Contact');
            insert storeContact2;
            
            // Create Corner
            Corner__c corner = new Corner__c(Point_of_Sale__c = posAcc.Id, Unit__c = 'cm');
            insert corner;
            
            // Create currency exchange rates
            Currency__c exrate = new Currency__c (From__c = 'CHF', To__c = 'USD', Rate__c = 1.1, Year__c = '2013');
            insert exrate;
            
            Currency__c exrate2 = new Currency__c (From__c = 'USD', To__c = 'CHF', Rate__c = 0.9, Year__c = '2013');
            insert exrate2;
            
            Currency__c exrate3 = new Currency__c (From__c = 'EUR', To__c = 'CHF', Rate__c = 1.2, Year__c = '2013');
            insert exrate3;
            
            Currency__c exrate4 = new Currency__c (From__c = 'USD', To__c = 'EUR', Rate__c = 1.3, Year__c = '2013');
            insert exrate4;
            
            Currency__c exrate5 = new Currency__c (From__c = 'CHF', To__c = 'USD', Rate__c = 1.1, Year__c = '2013');
            insert exrate5;
            
            ///////////////////////////////////////////////////
            // TEST Project triggers AutoFillFields, SelectExistingOrCreateNewCorner
            
            // Create Project with existing corner
            rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
            Project__c proj1 = new Project__c (RecordTypeId = rtId, Nb_of_machines_in_display__c = '1', Nb_of_machines_in_stock__c = '2', 
                    Floor__c = 'No', Old_NES_Corner_to_remove__c = 'No', Ecolaboration_module__c = 'No', LCD_screen__c = 'No', 
                    Visual__c = 'No', Estimated_Budget__c = 10000, NES_Sales_Promoter__c = sale.Id, Store_Contact__c = storeContact2.Id, 
                    Targeted_Installation_Date__c = date.newinstance(2013, 20, 9), Corner__c = corner.Id);
            insert proj1;
            
            // get newly inserted project, check the empty fields have been filled
            Project__c proj = [SELECT Store_Contact__c, Store_Electrician__c, Nespresso_Contact__c, Architect__c, Supplier__c, 
                    Corner_Point_of_Sale_r__c, Walls_material_r__c FROM Project__c WHERE Id = :proj1.Id];
            
            
            
        
        ///////////////////////////////////////////////////
        // TEST Project_File_Area triggers CopyDefaultValuesFromProject, CheckThereIsOnlyOneMainProjectImage
            system.runAs(mngr) {
                rtId = Schema.SObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Photos').getRecordTypeId();
                
                Project_File_Area__c file1 = new Project_File_Area__c(Name = 'test', Project__c = proj.Id, Main_Project_Image__c = true);
                insert file1;
                
                Project_File_Area__c file2 = new Project_File_Area__c(Name = 'test2', Project__c = proj.Id, Main_Project_Image__c = false);
                insert file2;
            
            // check default values have been copied from the project
                file1 = [SELECT Id, Architect__c, Main_Project_Image__c, Supplier__c FROM Project_File_Area__c WHERE Id = :file1.Id];
                
                
                Project_File_Area__c file3 = new Project_File_Area__c(Name = 'test2', Project__c = proj.Id, Main_Project_Image__c = true);
                insert file3;
                
                // check the new main image is the only main image
                file1 = [SELECT Id, Main_Project_Image__c FROM Project_File_Area__c WHERE Id = :file1.Id];
                file2 = [SELECT Id, Main_Project_Image__c FROM Project_File_Area__c WHERE Id = :file2.Id];
                file3 = [SELECT Id, Main_Project_Image__c FROM Project_File_Area__c WHERE Id = :file3.Id];
                  
            }
            
        ///////////////////////////////////////////////////
        // TEST Quotation trigger CopyDefaultValuesFromProjectToQuotation
            
            rtId = Schema.SObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
            Quotation__c quot = new Quotation__c(Proforma_no__c = '1', Project__c = proj.Id, Furnitures__c = 100, 
                    Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD');
            insert quot;
            
            quot = [SELECT Id, Total_in_main_currency__c, Supplier__c, Architect__c, Nespresso_Contact__c FROM Quotation__C WHERE Id = :quot.Id];
            
            
        ///////////////////////////////////////////////////
        // TEST Handover Report triggers CopyDefaultValuesFromProjectToHandoverReport, CreateHandoverChecklist
            Handover_Report__c hr = new Handover_Report__c(Arrival_time__c = datetime.now(), Departure_time__c = datetime.now(), 
                    Project__c = proj.Id, General_cleaning__c = 'Yes', Supplier_on_time__c = 'Yes', Installation_feedback__c = 'a', 
                    Installation_area_cleared_before__c = 'Yes');
            try {
                insert hr;
                // fails because no furniture inventory
            } catch (Exception e) {}
            
            Furniture__c furn = new Furniture__c(Name = 'test furniture', Active__c = true, Completed_items__c = true);
            insert furn;
            
            Furnitures_Inventory__c finv = new Furnitures_Inventory__c(Furniture__c = furn.Id, Project__c = proj.Id, Quantity__c = 1);
            insert finv;
            
            try {
                insert hr;
                
            } catch (Exception e) {}
            
            
        ///////////////////////////////////////////////////
        // TEST Objective trigger ConvertDefaultCurrency
            Objective__c obj = new Objective__c(Amount__c = 10000, Year__c = '2013', Market__c = market.Id);
            insert obj;
            
            obj = [SELECT Id, Amount_in_default_currency__c FROM Objective__c WHERE Id = :obj.Id];
            
           
            
        ///////////////////////////////////////////////////
        // TEST Display Corner / Project Image controllers
            
            // corner image
            PageReference pageRef = Page.DisplayCornerImage;
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController std = new ApexPages.StandardController(corner);
            DisplayCornerImageExtension controller = new DisplayCornerImageExtension(std);
            
            //System.assertEquals(controller.imageSrc, imgSrc); - won't work bc of different document DeveloperName
            
            //project image
            pageRef = Page.DisplayProjectImage;
            Test.setCurrentPage(pageRef);
            
            std = new ApexPages.StandardController(proj);
            DisplayProjectImageExtension controller2 = new DisplayProjectImageExtension(std);
            
            //System.assertEquals(controller2.imageSrc, imgSrc);
            
        ///////////////////////////////////////////////////
        // TEST Handover Checklist controller
            pageRef = Page.Handover_Checklist;
            Test.setCurrentPage(pageRef);
            
            std = new ApexPages.StandardController(hr);
            HandoverChecklistExtension controller3 = new HandoverChecklistExtension(std);
            
            //System.assertEquals(controller3.checklist.size(), 1);
            
            pageRef = controller3.saveHandoverChecklist();
            String nextPage = pageRef.getUrl();
            //System.assertEquals('/' + hr.Id, nextPage);
            
        ///////////////////////////////////////////////////
        // TEST Corners Map controller
            pageRef = Page.ShowCornersMap;
            Test.setCurrentPage(pageRef);
            
            ShowCornersMapController controller4 = new ShowCornersMapController();  
        */
        }
    }
}
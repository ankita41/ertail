/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCornerPriceEstimationController  {

    static testMethod void myUnitTest() {
        Test.startTest(); 
        
          Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
            mycs.Main_Currency__c = 'CHF';
            insert mycs;
             List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
                gmsList.add(new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa'));
                gmsList.add(new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs='));
                insert gmsList;
                
                insert new CustomRedirection__c (Name = 'QuotationObjId', Value__c = '01Ib0000000R72V');
            User usr = [Select id from User where Id = :UserInfo.getUserId()];
            System.runAs(usr){
                /*List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
                gmsList.add(new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa'));
                gmsList.add(new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs='));
                insert gmsList;
                */
              //  insert new CustomRedirection__c (Name = 'QuotationObjId', Value__c = '01Ib0000000R72V');
                
                Map<String, Id> profileMap = new Map<String, Id>();
                for (Profile p : [SELECT Id, Name FROM Profile])
                    profileMap.put(p.Name, p.Id);
                
                
                List<User> usersList = new List<User>();
                usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
               insert usersList;
               
               Corner_Products__c obj= new Corner_Products__c(Name='test product',Description__c='This is test corner products ',Type__c='Accessories');
               insert obj;
               Corner_Products__c obj1= new Corner_Products__c(Name='test product2',Description__c='This is test corner products ',Type__c='Accessories');
               insert obj1;
               Corner_Price_List__c objprice= new Corner_Price_List__c(Name='test pricelist',Description__c='test ',Supplier__c=usersList[0].id);
               insert objprice;
               List<Product_In_Pricelist__c> priceList=new List<Product_In_Pricelist__c>();
               priceList.add(new Product_In_Pricelist__c(Name='test pricelist',Corner_Price_List__c=objprice.id,Corner_Products__c=obj.id,Price__c=24.55));
               priceList.add(new Product_In_Pricelist__c( Name= 'test price 2',Corner_Price_List__c=objprice.id,Corner_Products__c=obj1.id,Price__c=26.55));

               insert priceList;
               
              /* Corner_Products__c obj1= new Corner_Products__c(Name='test product2',Description__c='This is test corner products ',Type__c='Accessories');
               insert obj1;
               List<Pricelist__c> priceList1=new List<Pricelist__c>();
               priceList1.add(new Pricelist__c(Name='test pricelist',Corner_Products__c=obj1.id,Product_Price__c=24.55,Supplier_Name__c=usersList[0].id));
               priceList1.add(new Pricelist__c(Name='test pricelist1',Corner_Products__c=obj1.id,Product_Price__c=26.55,Supplier_Name__c=usersList[0].id));

               insert priceList1; */
               List<Market__c> marketList = new List<Market__c>();
                Market__c market = new Market__c(Name = 'Germany', Code__c = 'GER', 
                              // NES_Project_Manager__c = mngr.Id,Default_Architect__c = arch.Id, 
                               Default_Supplier__c = usersList[0].Id, Currency__c ='EUR');
                marketList.add(market);
                insert marketList;
               CornerPriceEstimationController calobj=new CornerPriceEstimationController();
               calobj.selectedCategory ='Accessories';
               calobj.selectedSupplier=usersList[0].Id;
               calobj.queryForProducts();
               calobj.filterproducts();
               system.debug('calobj.pricelistWrapper'+calobj.pricelistWrapper);
               
               if(calobj.pricelistWrapper != null && calobj.pricelistWrapper.supplierproductmap != null && !calobj.pricelistWrapper.supplierproductmap.isempty()){
                 List<CornerPriceEstimationController.productWrapper> listwrapper= new List<CornerPriceEstimationController.productWrapper>();
                   listwrapper=calobj.pricelistWrapper.supplierproductmap.get('Testing');
                   system.debug('listwrapper@@@'+listwrapper);
                   for( CornerPriceEstimationController.productWrapper testobj:listwrapper){
                       testobj.isselected=true;
                       testobj.selectedQty='1';
                   }
                   calobj.addselectedProducts();
                   
               }
               
               system.debug('calobj.pricelistWrapper1'+calobj.pricelistWrapper1);
               if(calobj.pricelistWrapper1 != null && calobj.pricelistWrapper1.productPriceList!= null && calobj.pricelistWrapper1.productPriceList.size()>0){
                 List<CornerPriceEstimationController.productWrapper> listwrapper= new List<CornerPriceEstimationController.productWrapper>();
                   listwrapper=calobj.pricelistWrapper1.productPriceList;
                   system.debug('listwrapper@@@'+listwrapper);
                   for( CornerPriceEstimationController.productWrapper testobj:listwrapper){
                       testobj.isselected=false;
                       testobj.selectedQty='1';
                   }
                  // calobj.addselectedProducts();
                 calobj.pricelistWrapper1.productPriceList=  listwrapper;
               }
               calobj.delselectedProducts();
               //calobj.
             //  calobj.addselectedProducts();
               List<SelectOption> li=calobj.categoryOpts ;
               calobj.selectedSupplier=usersList[0].Id ;
               CornerPriceEstimationController  calobj1=new CornerPriceEstimationController ();
               calobj1.selectedCategory='Accessories';
               calobj1.selectedProducts ='[test product,test product2]';
               //calobj1.querysuppliersForProducts();
               
               try{
              CornerPriceEstimationController.pricelistWrapper pricewrapper= new CornerPriceEstimationController.pricelistWrapper();
               calobj1.filterproducts();
              pricewrapper= calobj1.pricelistWrapper;              
               //pricewrapper.supplierproductmap=supplierMap;
              // calobj1.pricelistWrapper=pricewrapper;
               system.debug('pricewrapper11@@'+pricewrapper+'calobj1.pricelistWrapper'+calobj1.pricelistWrapper);
               //calobj1.addselectedProducts();
               calobj1.addNewProducts();
              // calobj1.delselectedProducts();
               calobj1.newSearch();
               }catch(Exception e){}
          }
        Test.StopTest();
    }
}
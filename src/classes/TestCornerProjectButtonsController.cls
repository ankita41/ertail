/*****

*   @Class      :   TestCornerProjectButtonsController.cls
*   @Description:   Test methods for the Project Corner Buttons
*   @Author     :   Thibauld
*   @Created    :   30 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Thibauld        23 JUl 2014     Add MarketName in  account to prevent not required queries on Account insert
***/

@isTest
public with sharing class TestCornerProjectButtonsController {
    static testMethod void test() {
            // create custom settings
            Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
            mycs.Main_Currency__c = 'CHF';
            insert mycs;
            
            User usr = [Select id from User where Id = :UserInfo.getUserId()];
            System.runAs(usr){
                List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
                gmsList.add(new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa'));
                gmsList.add(new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs='));
                insert gmsList;
                
                insert new CustomRedirection__c (Name = 'QuotationObjId', Value__c = '01Ib0000000R72V');
                
                Map<String, Id> profileMap = new Map<String, Id>();
                for (Profile p : [SELECT Id, Name FROM Profile])
                    profileMap.put(p.Name, p.Id);
                
                
                List<User> usersList = new List<User>();
                usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
                            
                usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
                            
                UserRole r = [SELECT Id FROM UserRole WHERE Name = 'Germany'];
                usersList.add(new User(Alias = 'mngr', Email='mngr@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Local Manager'), UserRoleId = r.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='mngr@testorg.com'));
    
                //mngr = [SELECT Id, Name FROM User WHERE Username = 'market.nespresso@gmail.com.uat'];
                
                usersList.add(new User(Alias = 'sale', Email='sale@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Sales Promoter'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='sale@testorg.com'));
                
                usersList.add(new User(Alias = 'trad', Email='trad@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Trade/Proc Manager'),  
                            TimeZoneSidKey='America/Los_Angeles', UserName='trad@testorg.com'));
                            
                insert usersList;
                
                Id suppId = usersList[0].Id;
                Id archId = usersList[1].Id;
                Id mgrId = usersList[2].Id;
                Id saleId = usersList[3].Id;
                Id tradId = usersList[4].Id;
                User mngr = usersList[2];
                
                // Create Market
                
                Market__c market = new Market__c(Name = 'Germany', Code__c = 'GER', NES_Project_Manager__c = mgrId, 
                        Default_Architect__c = archId, Default_Supplier__c = suppId, Currency__c = 'USD');
                
                
                Market__c market2 = new Market__c(Name = 'Hungary', Code__c = 'HU', NES_Project_Manager__c = mgrId, 
                        Default_Architect__c = archId, Default_Supplier__c = suppId, Currency__c = 'HUF');
                insert new List<Market__c>{market, market2};
                
                            // Create forecasts
                List<Objective__c> objList = new List<Objective__c>();
                objList.add(new Objective__c(Market__c = market.Id
                                            , year__c = String.valueOf(Date.today().addYears(-1).year())
                                            , New_Members_Market_or_Region_Growth__c = 1
                                            , Monthly_Average_Consumption__c = 2
                                            , Caps_Cost_in_LC__c = 3
                                            , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
                objList.add(new Objective__c(Market__c = market.Id
                                , year__c = String.valueOf(Date.today().year())
                                , New_Members_Market_or_Region_Growth__c = 1
                                , Monthly_Average_Consumption__c = 2
                                , Caps_Cost_in_LC__c = 3
                                , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
                insert(objList);
                
                // Create Global Key account (to use as parent for the POS Group)
                Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Key Account').getRecordTypeId();
                Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = rtId, Market__c = market.Id, MarketName__c = market.Name);
                insert globalKeyAcc;
                
                // Create POS Group account (to use as parent for the POS Local)
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Group').getRecordTypeId();
                Account posGroupAcc = new Account(Name = 'Test POS Group Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = globalKeyAcc.Id, MarketName__c = market.Name);
                insert posGroupAcc;
                
                // Create POS Local account (to use as parent for the POS)
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Local').getRecordTypeId();
                Account posLocalAcc = new Account(Name = 'Test POS Local Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = posGroupAcc.Id, MarketName__c = market.Name);
                insert posLocalAcc;
                
                // Create POS Account
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
                Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                        Market__c = market.Id, MarketName__c = market.Name, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                        BillingPostalCode = 'zip', BillingCountry = 'country');
                insert posAcc;
                
                List<Project__c> projList = new List<Project__c>();
                
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
                Id rtMoWId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Maintenance Outside Warranty').getRecordTypeId();
                Id rtMuWId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Maintenance Under Warranty').getRecordTypeId();
                Id rtAgentId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner (Agent)').getRecordTypeId();
                
                Project__c proj = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Sales Promoter');
                proj.MarketName__c = 'Germany';
                projList.add(proj);
                
                Project__c proj2 = TestHelper.CreateCornerProject(rtMoWId, posAcc.Id, posLocalAcc.Id, 'Maintenance outside Warranty');
                proj2.MarketName__c = 'Germany';
                proj2.Approval_Status__c = 'Waiting for Supplier Proposal';
                proj2.Supplier_Proposal__c = 'This is my proposal';
                proj2.Proposed_Installation_Date__c = Date.Today();
                projList.add(proj2);
                
                Project__c proj3 = TestHelper.CreateCornerProject(rtMuWId, posAcc.Id, posLocalAcc.Id, 'Maintenance under Warranty');
                proj3.MarketName__c = 'Germany';
                projList.add(proj3);
                
                Project__c proj4 = TestHelper.CreateCornerProject(rtAgentId, posAcc.Id, posLocalAcc.Id, 'Agent');
                proj.MarketName__c = 'Germany';
                projList.add(proj4);
                
                insert projList;
                
                rtId = Schema.SObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Photos').getRecordTypeId();
                
                Project_File_Area__c file;
                //system.runAs(mngr) {
                    file = new Project_File_Area__c(Name = 'test', Project__c = proj.Id, Main_Project_Image__c = true, RecordTypeId = rtId);
                    insert file;
                //}
        // Test.startTest();       
                rtId = Schema.SObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
                List<Quotation__c> quotList = new List<Quotation__c>();
                Quotation__c quot = new Quotation__c(Proforma_no__c = '1', Project__c = proj.Id, Furnitures__c = 100, 
                        Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD');
                quotList.add(quot);
                
                Quotation__c quot2 = new Quotation__c(Proforma_no__c = '1', Project__c = proj2.Id, Furnitures__c = 100, 
                        Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD');
                quotList.add(quot2);
                
                insert quotList;
                
                List<Attachment> attList = new List<Attachment>();
                Attachment attach=new Attachment();    
                attach.Name='Unit Test Attachment';
                Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                attach.body=bodyBlob;
                attach.parentId=quot.id;
                attList.add(attach);
                
                Attachment attach2=new Attachment();    
                attach2.Name='Unit Test Attachment';
                attach2.body=bodyBlob;
                attach2.parentId=quot2.id;
                attList.add(attach2);
                
                Attachment attach3=new Attachment();    
                attach3.Name='Unit Test Attachment';
                attach3.body=bodyBlob;
                attach3.parentId=file.id;
                attList.add(attach3);

                insert attList;
                                
                Handover_Report__c hr = new Handover_Report__c(Installer_Arrival_Date__c = Date.Today(), Installer_Arrival_Time__c = '12:00', Installer_Departure_Date__c = Date.Today(), Installer_Departure_Time__c = '12:00', 
                        Project__c = proj.Id, General_cleaning__c = 'Yes', Supplier_on_time__c = 'Yes', Installation_feedback__c = 'a', 
                        Installation_area_cleared_before__c = 'Yes');
                
                Furniture__c furn = new Furniture__c(Name = 'test furniture', Active__c = true, Completed_items__c = true);
                insert furn;
                
                Furnitures_Inventory__c finv = new Furnitures_Inventory__c(Furniture__c = furn.Id, Project__c = proj.Id, Quantity__c = 1);
                insert finv;
                
                insert hr;
            
        
        ///////////////////////////////////////////////////
 Test.startTest(); 
        // TEST CornerProjectButtonsController
            ApexPages.StandardController std = new ApexPages.StandardController(proj);
            CornerProjectButtonsController cpbc = new CornerProjectButtonsController(std);
            
            System.assertEquals(true, cpbc.showInitiate);
            System.assertEquals(false, cpbc.showInitiateAgent);
            System.assertEquals(true, cpbc.showEdit);
            System.assertEquals(true, cpbc.showEdit);
            System.assertEquals(false, cpbc.showSendDraftRequest);
            System.assertEquals(false, cpbc.showSendInstallationDateConfirmation);
            System.assertEquals(false, cpbc.showSendInstallationDateProposal);
            System.assertEquals(false, cpbc.showCloseProject);
            System.assertEquals(true, cpbc.showCancelProject);
            System.assertEquals(false, cpbc.allowCloseProject);
            System.assertEquals(true, cpbc.showSetOnHold);
            System.assertEquals(false, cpbc.showUnsetOnHold);
            System.assertEquals(false, cpbc.showConvertToDraft);
            System.assertEquals(false, cpbc.showConvertToNewRequest);
            System.assertEquals(false, cpbc.showSendDraftAgentRequest);
            System.assertEquals(false, cpbc.showValidateData);
            System.assertEquals(false, cpbc.showSendInstallationDateConfirmation_Agent);
            System.assertEquals(false, cpbc.showSendInstallationDateConfirmation_Maintenance);
            System.assertEquals(false, cpbc.allowSendInstallationDateConfirmation);
            System.assertEquals(false, cpbc.showCloseProject_Maintenance);
            System.assertEquals(true, cpbc.allowCancelProject);
            System.assertEquals(false, cpbc.allowInitiate);
            System.assertEquals(false, cpbc.showSendQuotationAndInstallationDateAndPlanForApproval);
                        
            PageReference pageRef = cpbc.ConvertToDraft();
            pageRef = cpbc.ConvertToNewRequest();
            pageRef = cpbc.SendDraftRequest();
            pageRef = cpbc.SendSiteSurveyToMarket(); //newly added code
            pageRef = cpbc.InitiateProject();
            pageRef = cpbc.ValidateData();
            pageRef = cpbc.EditProject();
            pageRef = cpbc.SendInstallationDateProposal();
            pageRef = cpbc.SendInstallationDateConfirmation();
            pageRef = cpbc.SendQuotationForApproval();
            pageRef = cpbc.CloseProject();
            pageRef = cpbc.CancelProject();
            
            pageRef = cpbc.SetOnHold();
            pageRef = cpbc.UnsetOnHold();
            
            ApexPages.StandardController stdoW = new ApexPages.StandardController(proj2);
            CornerProjectButtonsController cpbcoW = new CornerProjectButtonsController(stdoW);
            System.assertEquals(true, cpbcoW.showSendQuotationAndInstallationDateAndPlanForApproval);
            Boolean b = cpbcoW.allowSendQuotationAndInstallationDateAndPlanForApproval;
        
        
        Test.StopTest();
        }
    }
    
   
}
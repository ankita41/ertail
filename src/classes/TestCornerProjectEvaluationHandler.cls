/*****
*   @Class      :   TestCornerProjectEvaluationHandler.cls
*   @Description:   Handles all tests for TestCornerProjectEvaluationHandler.
*   @Author     :   Thibauld
*   @Created    :   28 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Thibauld        23 JUL 2014     Add MarketName__c in Accounts to prevent additional queries from Account trigger                   
***/
@isTest
public with sharing class TestCornerProjectEvaluationHandler {
    public class CustomException extends Exception {}
    
    static testMethod void test() {
          Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
            mycs.Main_Currency__c = 'CHF';
            insert mycs;
        
            GoogleMapsSettings__c mycs2 = new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa');
            GoogleMapsSettings__c mycs3 = new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs=');
            insert new List<GoogleMapsSettings__c> {mycs2, mycs3};      
        
        SYSTEM.runAs(TestHelper.CurrentUser){
            TestHelper.createCurrencies();
            
            // Get Account "Point of Sales" and Project Corner "Draft New Corner" record Types
            Id accRTId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
            Id cornerProjectRTId = Schema.sObjectType.Project__c.getRecordTypeInfosByName().get('Draft New Corner').getRecordTypeId();
            
            // Create Market
            List<Market__c> marketList = new List<Market__c>();
            Market__c newMarket = new Market__c(Name = 'Germany', Code__c = 'GER', Currency__c = 'USD');
            marketList.add(newMarket);
            
            insert marketList;
            
            // Create forecasts
            List<Objective__c> objList = new List<Objective__c>();
            objList.add(new Objective__c(Market__c = marketList[0].Id
                                        , year__c = String.valueOf(Date.today().addYears(-1).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = marketList[0].Id
                            , year__c = String.valueOf(Date.today().year())
                            , New_Members_Market_or_Region_Growth__c = 1
                            , Monthly_Average_Consumption__c = 2
                            , Caps_Cost_in_LC__c = 3
                            , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            insert(objList);
            
            // Create Global Key account (to use as parent for the POS Group)
            Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Key Account').getRecordTypeId();
            Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = rtId, Market__c = newMarket.Id, MarketName__c = newMarket.Name);
            insert globalKeyAcc;
                        
            // Create POS Group account (to use as parent for the POS Local)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Group').getRecordTypeId();
            Account posGroupAcc = new Account(Name = 'Test POS Group Acc', RecordTypeId = rtId, Market__c = newMarket.Id, MarketName__c = newMarket.Name, ParentId = globalKeyAcc.Id);
            insert posGroupAcc;
            
            // Create POS Local account (to use as parent for the POS)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Local').getRecordTypeId();
            Account posLocalAcc = new Account(Name = 'Test POS Local Acc', RecordTypeId = rtId, Market__c = newMarket.Id, MarketName__c = newMarket.Name, ParentId = posGroupAcc.Id);
            insert posLocalAcc;
            
            // Create POS Account
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
            Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                    Market__c = newMarket.Id, MarketName__c = newMarket.Name, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'country');
            insert posAcc;
            
            // test creation of a Corner Project with a targeted Installation date in the past does NOT create a Forecast record
            Date today = Date.today();
                 
            rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Corner Forecast').getRecordTypeId();
            Project__c proj1 = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Sales Promoter');
            
            rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Corner Forecast (Agent)').getRecordTypeId();
            Project__c proj2 = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Agent');
            
           // Test.startTest(); //code commented
            insert new List<Project__c> {proj1, proj2};

            ///////////////////////////////////////////////////////
            /// Test UpdateProjectCornerEvaluationAfterFileAreaUpsert
            Id faExecutiveDrawingsRTId = Schema.sObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Executive Drawings').getRecordTypeId();
            Id faSupplierDrawingRTId = Schema.sObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Supplier Drawings').getRecordTypeId();
            Id faLayoutProposalRTId = Schema.sObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Layout Proposal').getRecordTypeId();

            //////
            // Validate Tech_Drawing_submission_date__c is empty
            
            Test.startTest();//code added
            List<Corner_Project_Evaluation__c> cpeList = [Select Id, Tech_Drawing_submission_date__c from Corner_Project_Evaluation__c where Corner_Project__c in (:proj1.Id)];//, :proj2.Id)];
            System.assertEquals(1, cpeList.size());
            System.assertEquals(null, cpeList[0].Tech_Drawing_submission_date__c);
            Project_File_Area__c pfa1 = new Project_File_Area__c(RecordTypeId = faExecutiveDrawingsRTId, Project__c = proj1.Id, File_sent_date__c = today);
            insert new List<Project_File_Area__c> {pfa1};
           /* System.assertEquals(2, cpeList.size());
            System.assertEquals(null, cpeList[0].Tech_Drawing_submission_date__c);
            System.assertEquals(null, cpeList[1].Tech_Drawing_submission_date__c);
            Project_File_Area__c pfa1 = new Project_File_Area__c(RecordTypeId = faExecutiveDrawingsRTId, Project__c = proj1.Id, File_sent_date__c = today);
            Project_File_Area__c pfa1a = new Project_File_Area__c(RecordTypeId = faExecutiveDrawingsRTId, Project__c = proj2.Id, File_sent_date__c = today);
            insert new List<Project_File_Area__c> {pfa1, pfa1a};*/
            Test.stopTest(); //code added
            // Validate Tech_Drawing_submission_date__c updated
           /* cpeList = [Select Id, Tech_Drawing_submission_date__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
            System.assertEquals(today, cpeList[0].Tech_Drawing_submission_date__c);
            System.assertEquals(today, cpeList[1].Tech_Drawing_submission_date__c);
            //////
            // Validate Tech_Drawing_submission_date__c and Approved_Supplier_Shop_Fitting_Drawing__c empty
            cpeList = [Select Id, Supplier_Shop_Fittings_Drawings_Sent__c, Approved_Supplier_Shop_Fitting_Drawing__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
            
            System.assertEquals(null, cpeList[0].Supplier_Shop_Fittings_Drawings_Sent__c);
            System.assertEquals(null, cpeList[0].Approved_Supplier_Shop_Fitting_Drawing__c);            
            System.assertEquals(null, cpeList[1].Supplier_Shop_Fittings_Drawings_Sent__c);
            System.assertEquals(null, cpeList[1].Approved_Supplier_Shop_Fitting_Drawing__c);    
            Project_File_Area__c pfa2 = new Project_File_Area__c(RecordTypeId = faSupplierDrawingRTId, Project__c = proj1.Id, File_sent_date__c = today, Architect_Decision__c = 'Approved', Architect_Decision_Sent_Date__c = today);
            Project_File_Area__c pfa2a = new Project_File_Area__c(RecordTypeId = faSupplierDrawingRTId, Project__c = proj2.Id, File_sent_date__c = today, Architect_Decision__c = 'Approved', Architect_Decision_Sent_Date__c = today);
            insert new List<Project_File_Area__c>{pfa2, pfa2a};
            
            // Validate Tech_Drawing_submission_date__c and Approved_Supplier_Shop_Fitting_Drawing__c now populated
 
            cpeList = [Select Id, Supplier_Shop_Fittings_Drawings_Sent__c, Approved_Supplier_Shop_Fitting_Drawing__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(today, cpeList[0].Supplier_Shop_Fittings_Drawings_Sent__c);
            System.assertEquals(today, cpeList[0].Approved_Supplier_Shop_Fitting_Drawing__c);           
            System.assertEquals(today, cpeList[1].Supplier_Shop_Fittings_Drawings_Sent__c);
            System.assertEquals(today, cpeList[1].Approved_Supplier_Shop_Fitting_Drawing__c);   
            
            /////
            // Validate Layout_Approved_Date__c and Standard_Non_Standard__c empty

            cpeList = [Select Id, Layout_Approved_Date__c, Standard_Non_Standard__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(null, cpeList[0].Layout_Approved_Date__c);
            System.assertEquals(null, cpeList[0].Standard_Non_Standard__c);         
            System.assertEquals(null, cpeList[1].Layout_Approved_Date__c);
            System.assertEquals(null, cpeList[1].Standard_Non_Standard__c); 

                        
            Project_File_Area__c pfa3 = new Project_File_Area__c(RecordTypeId = faLayoutProposalRTId, Project__c = proj1.Id, NES_Dis_or_Approval_date__c = today, Approval_Status__c = 'Approved by NES', Standardization__c = 'Standard', NES_Decision__c = 'Approved');
            Project_File_Area__c pfa3a = new Project_File_Area__c(RecordTypeId = faLayoutProposalRTId, Project__c = proj2.Id, Agent_Dis_Approval_date__c = today, Approval_Status__c = 'Approved by Agent', Standardization__c = 'Standard', Agent_Decision__c = 'Approved');
            insert new List<Project_File_Area__c>{pfa3, pfa3a};
            
            // Validate Layout_Approved_Date__c and Standard_Non_Standard__c now populated

            cpeList = [Select Id, Layout_Approved_Date__c, Standard_Non_Standard__c, Corner_Project__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(today, cpeList[0].Layout_Approved_Date__c);
            System.assertEquals('Standard', cpeList[0].Standard_Non_Standard__c);
            System.assertEquals(today, cpeList[1].Layout_Approved_Date__c);
            System.assertEquals('Standard', cpeList[1].Standard_Non_Standard__c);

            ///////////////////////////////////////////////////////
            /// Test UpdateProjectCornerEvaluationAfterQuotationUpsert
            Id supplierQuotationRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
            
            // Validate Supplier_Quote_Sent__c is empty     

            cpeList = [Select Id, Supplier_Quote_Sent__c, Date_PO_Number_was_sent_to_Supplier__c, Supplier_PO_N__c, Supplier_Quotation_Validated__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(null, cpeList[0].Supplier_Quote_Sent__c);
            System.assertEquals(null, cpeList[0].Date_PO_Number_was_sent_to_Supplier__c);
            System.assertEquals(null, cpeList[0].Supplier_PO_N__c);
            System.assertEquals(null, cpeList[0].Supplier_Quotation_Validated__c);
            System.assertEquals(null, cpeList[1].Supplier_Quote_Sent__c);
            System.assertEquals(null, cpeList[1].Date_PO_Number_was_sent_to_Supplier__c);
            System.assertEquals(null, cpeList[1].Supplier_PO_N__c);
            System.assertEquals(null, cpeList[1].Supplier_Quotation_Validated__c);
                                    
            Quotation__c quot1 = new Quotation__c(Project__c = proj1.Id
                                                    , Quotation_sent_date__c = today
                                                    , RecordTypeId = supplierQuotationRTId
                                                    , Approval_Status__c = 'Approved by NES'
                                                    , PO_no__c = '123456'
                                                    , NES_Dis_Approval_date__c = today
                                                    , Architect_Dis_Approval__c = true
                                                    , Architect_Decision__c = 'Approved'
                                                    , Architect_Decision_Sent_Date__c = today);
            Quotation__c quot1a = new Quotation__c(Project__c = proj2.Id
                                                    , Quotation_sent_date__c = today
                                                    , RecordTypeId = supplierQuotationRTId
                                                    , Approval_Status__c = 'Approved by Agent'
                                                    , PO_no__c = '123456'
                                                    , Agent_Dis_Approval_date__c = today
                                                    , Architect_Dis_Approval__c = true
                                                    , Architect_Decision__c = 'Approved'
                                                    , Architect_Decision_Sent_Date__c = today);
            insert new List<Quotation__c> {quot1, quot1a};
            
            // Validate Supplier_Quote_Sent__c is empty     

            cpeList = [Select Id, Supplier_Quote_Sent__c, Date_PO_Number_was_sent_to_Supplier__c, Supplier_PO_N__c, Supplier_Quotation_Validated__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(today, cpeList[0].Supplier_Quote_Sent__c);
            System.assertEquals(today, cpeList[0].Date_PO_Number_was_sent_to_Supplier__c);
            System.assertEquals('123456', cpeList[0].Supplier_PO_N__c);
            System.assertEquals(today, cpeList[0].Supplier_Quotation_Validated__c);
            System.assertEquals(today, cpeList[1].Supplier_Quote_Sent__c);
            System.assertEquals(today, cpeList[1].Date_PO_Number_was_sent_to_Supplier__c);
            System.assertEquals('123456', cpeList[1].Supplier_PO_N__c);
            System.assertEquals(today, cpeList[1].Supplier_Quotation_Validated__c);         
        
                    
            
            Id architectQuotationRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Architect Quotation').getRecordTypeId();
            
            // Validate Architect_Quote_Sent__c, Archi_PO_N__c, Date_PO_Number_was_sent_to_Architect__c, Arch_Quotation_Approved__c are empty       

            cpeList = [Select Id, Architect_Quote_Sent__c, Archi_PO_N__c, Date_PO_Number_was_sent_to_Architect__c, Arch_Quotation_Approved__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(null, cpeList[0].Architect_Quote_Sent__c);
            System.assertEquals(null, cpeList[0].Archi_PO_N__c);
            System.assertEquals(null, cpeList[0].Date_PO_Number_was_sent_to_Architect__c);
            System.assertEquals(null, cpeList[0].Arch_Quotation_Approved__c);
            System.assertEquals(null, cpeList[1].Architect_Quote_Sent__c);
            System.assertEquals(null, cpeList[1].Archi_PO_N__c);
            System.assertEquals(null, cpeList[1].Date_PO_Number_was_sent_to_Architect__c);
            System.assertEquals(null, cpeList[1].Arch_Quotation_Approved__c);

            Quotation__c quot2 = new Quotation__c(Project__c = proj1.Id
                                                    , Quotation_sent_date__c = today
                                                    , RecordTypeId = architectQuotationRTId
                                                    , Approval_Status__c = 'Approved by NES'
                                                    , PO_no__c = '123456'
                                                    , NES_Dis_Approval_date__c = today);
            Quotation__c quot2a = new Quotation__c(Project__c = proj2.Id
                                                    , Quotation_sent_date__c = today
                                                    , RecordTypeId = architectQuotationRTId
                                                    , Approval_Status__c = 'Approved by Agent'
                                                    , PO_no__c = '123456'
                                                    , Agent_Dis_Approval_date__c = today);
            insert new List<Quotation__c> {quot2, quot2a};
            
            cpeList = [Select Id, Architect_Quote_Sent__c, Archi_PO_N__c, Date_PO_Number_was_sent_to_Architect__c, Arch_Quotation_Approved__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(today, cpeList[0].Architect_Quote_Sent__c);
            System.assertEquals('123456', cpeList[0].Archi_PO_N__c);
            System.assertEquals(today, cpeList[0].Date_PO_Number_was_sent_to_Architect__c);
            System.assertEquals(today, cpeList[0].Arch_Quotation_Approved__c);
            System.assertEquals(today, cpeList[1].Architect_Quote_Sent__c);
            System.assertEquals('123456', cpeList[1].Archi_PO_N__c);
            System.assertEquals(today, cpeList[1].Date_PO_Number_was_sent_to_Architect__c);
            System.assertEquals(today, cpeList[1].Arch_Quotation_Approved__c);          
            
            ///////////////////////////////////////////////////////
            /// Test UpdateProjectCornerEvaluationAfterHandoverReportUpsert
            
            Furniture__c furn = new Furniture__c(Name = 'test furniture', Active__c = true, Completed_items__c = true);
            Furniture__c furna = new Furniture__c(Name = 'test furniture', Active__c = true, Completed_items__c = true);
            insert new List<Furniture__c>{furn, furna};
            
            Furnitures_Inventory__c finv = new Furnitures_Inventory__c(Furniture__c = furn.Id, Project__c = proj1.Id, Quantity__c = 1);
            Furnitures_Inventory__c finva = new Furnitures_Inventory__c(Furniture__c = furn.Id, Project__c = proj2.Id, Quantity__c = 1);
            insert new List<Furnitures_Inventory__c> {finv, finva};
            
            // Validate Disapproved_HR_Date__c is empty
            cpeList = [Select Id, Disapproved_HR_Date__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(null, cpeList[0].Disapproved_HR_Date__c);
            System.assertEquals(null, cpeList[1].Disapproved_HR_Date__c);
            
            Handover_Report__c hr = new Handover_Report__c(Project__c = proj1.Id, NES_Dis_Approval_date__c = today);
            Handover_Report__c hra = new Handover_Report__c(Project__c = proj2.Id, NES_Dis_Approval_date__c = today);
            insert new List<Handover_Report__c> {hr, hra};*/
            
            // Validate Disapproved_HR_Date__c is empty ...below code commented
           /* cpeList = [Select Id, Disapproved_HR_Date__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(today, cpeList[0].Disapproved_HR_Date__c);
            System.assertEquals(today, cpeList[1].Disapproved_HR_Date__c);         
            
            ///////////////////////////////////////////////////////
            /// Test UpdateProjectCornerEvaluationAfterActionPlanUpsert
                            
            // Validate Disapproved_HR_Date__c is empty
            cpeList = [Select Id, Action_Plan_Date__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(null, cpeList[0].Action_Plan_Date__c);
            System.assertEquals(null, cpeList[1].Action_Plan_Date__c);
        
            Handover_Action_Plan__c hap = new Handover_Action_Plan__c(Handover_Report__c = hr.Id, Action_plan_sent_date__c = today);
            Handover_Action_Plan__c hapa = new Handover_Action_Plan__c(Handover_Report__c = hra.Id, Action_plan_sent_date__c = today);
            insert new List<Handover_Action_Plan__c> {hap, hapa};
            
            // Validate Disapproved_HR_Date__c is empty
            cpeList = [Select Id, Action_Plan_Date__c from Corner_Project_Evaluation__c where Id in (:cpeList[0].Id, :cpeList[1].Id)];
                
            System.assertEquals(today, cpeList[0].Action_Plan_Date__c);
            System.assertEquals(today, cpeList[1].Action_Plan_Date__c);*/
            
           // Test.stopTest();  //code commented
        }
        
    }
    

}
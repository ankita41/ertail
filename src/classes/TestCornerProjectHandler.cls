/*****

*   @Class      :   TestCornerProjectHandler.cls
*   @Description:   Test methods for the Corner Project Handler
*   @Author     :   Thibauld
*   @Created    :   13 AUG 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
***/

@isTest
public with sharing class TestCornerProjectHandler {
// Test for the TestCornerProjectHandler class    
    static testMethod void testApexSharing(){
       
         // create custom settings
            Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
            mycs.Main_Currency__c = 'CHF';
            insert mycs;
            
            User usr = [Select id from User where Id = :UserInfo.getUserId()];
            System.runAs(usr){
                List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
                gmsList.add(new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa'));
                gmsList.add(new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs='));
                insert gmsList;
                
                insert new CustomRedirection__c (Name = 'QuotationObjId', Value__c = '01Ib0000000R72V');
                
                Map<String, Id> profileMap = new Map<String, Id>();
                for (Profile p : [SELECT Id, Name FROM Profile])
                    profileMap.put(p.Name, p.Id);
                
                
                List<User> usersList = new List<User>();
                usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
                
                usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
                            
                UserRole r = [SELECT Id FROM UserRole WHERE Name = 'Germany'];
                usersList.add(new User(Alias = 'mngr', Email='mngr@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Local Manager'), UserRoleId = r.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='mngr@testorg.com'));
    
                //mngr = [SELECT Id, Name FROM User WHERE Username = 'market.nespresso@gmail.com.uat'];
                
                usersList.add(new User(Alias = 'sale', Email='sale@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Sales Promoter'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='sale@testorg.com'));
                
                usersList.add(new User(Alias = 'trad', Email='trad@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Trade/Proc Manager'),  
                            TimeZoneSidKey='America/Los_Angeles', UserName='trad@testorg.com'));
                            
                usersList.add(new User(Alias = 'bdm', Email='bdm@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Business Development Manager'),  
                            TimeZoneSidKey='America/Los_Angeles', UserName='bdm@testorg.com'));

                usersList.add(new User(Alias = 'supp2', Email='supplier2@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='supplier2@testorg.com'));
                            
                insert usersList;
                
                Id suppId = usersList[0].Id;
                Id archId = usersList[1].Id;
                Id mgrId = usersList[2].Id;
                Id saleId = usersList[3].Id;
                Id tradId = usersList[4].Id;
                Id bdmId = usersList[5].Id;
                Id supp2Id = usersList[6].Id;
                User mngr = usersList[2];
                
                // Create Market
                
                Market__c market = new Market__c(Name = 'Germany', Code__c = 'GER', NES_Project_Manager__c = mgrId, 
                        Default_Architect__c = archId, Default_Supplier__c = suppId, Currency__c = 'USD',
                        NES_Business_Development_Manager__c = bdmId, Project_Manager_HQ__c = mgrId);
                
                
                Market__c market2 = new Market__c(Name = 'Hungary', Code__c = 'HU', NES_Project_Manager__c = mgrId, 
                        Default_Architect__c = archId, Default_Supplier__c = suppId, Currency__c = 'HUF');
                insert new List<Market__c>{market, market2};
                
                            // Create forecasts
                List<Objective__c> objList = new List<Objective__c>();
                objList.add(new Objective__c(Market__c = market.Id
                                            , year__c = String.valueOf(Date.today().addYears(-1).year())
                                            , New_Members_Market_or_Region_Growth__c = 1
                                            , Monthly_Average_Consumption__c = 2
                                            , Caps_Cost_in_LC__c = 3
                                            , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
                objList.add(new Objective__c(Market__c = market.Id
                                , year__c = String.valueOf(Date.today().year())
                                , New_Members_Market_or_Region_Growth__c = 1
                                , Monthly_Average_Consumption__c = 2
                                , Caps_Cost_in_LC__c = 3
                                , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
                
                 objList.add(new Objective__c(Market__c = market.Id
                                , year__c = String.valueOf(Date.today().addYears(1).year())
                                , New_Members_Market_or_Region_Growth__c = null
                                , Monthly_Average_Consumption__c = 2
                                , Caps_Cost_in_LC__c = 3
                                , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
                insert(objList);
                
                // Create Global Key account (to use as parent for the POS Group)
                Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Key Account').getRecordTypeId();
                Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = rtId, Market__c = market.Id, MarketName__c = market.Name);
                insert globalKeyAcc;
                
                 
                // Create POS Group account (to use as parent for the POS Local)
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Group').getRecordTypeId();
                Account posGroupAcc = new Account(Name = 'Test POS Group Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = globalKeyAcc.Id, MarketName__c = market.Name);
                insert posGroupAcc;
                
                 
                // Create POS Local account (to use as parent for the POS)
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Local').getRecordTypeId();
                Account posLocalAcc = new Account(Name = 'Test POS Local Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = posGroupAcc.Id, MarketName__c = market.Name);
                insert posLocalAcc;
                
                 
                // Create POS Account
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
                Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                        Market__c = market.Id, MarketName__c = market.Name, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                        BillingPostalCode = 'zip', BillingCountry = 'country');
                insert posAcc;
                
                
                // Create POS Account
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
                Account posAcc2 = new Account(Name = 'Test POS2', Nessoft_ID__c = 'testid1235', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                        Market__c = market.Id, MarketName__c = market.Name, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                        BillingPostalCode = 'zip', BillingCountry = 'country');
                insert posAcc2;
                
                // Create POS Contacts
                List<Contact> contactList = new List<Contact>();
                Contact storeElectrician = new Contact (LastName = 'Electrician', AccountId = posAcc.Id, Function__c = 'Store Electrician');
                contactList.add(storeElectrician);
                Contact storeContact = new Contact (LastName = 'StContact', AccountId = posAcc.Id, Function__c = 'Store Contact');
                contactList.add(storeContact);
                Contact storeContact2 = new Contact (LastName = 'StContact2', AccountId = posAcc.Id, Function__c = 'Store Contact');
                contactList.add(storeContact2);
                insert contactList;
                 Test.startTest();                
               
                
                List<Project__c> projList = new List<Project__c>();
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
                Project__c proj = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Sales Promoter');
                proj.MarketName__c = 'Germany';
                proj.POS_Name__c = 'TestName';
                proj.Targeted_Installation_Date__c = Date.today();
                proj.Currency__c = 'CHF';
                projList.add(proj);
                
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
                Project__c proj2 = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Sales Promoter');
                proj2.MarketName__c = 'Germany';
                proj2.POS_Name__c = 'TestName';
                proj2.Targeted_Installation_Date__c = Date.today().addYears(1);
                proj2.Currency__c = 'CHF';
                projList.add(proj2);
                
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
                Project__c proj3 = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Sales Promoter');
                proj3.MarketName__c = 'Germany';
                proj3.POS_Name__c = 'TestName';
                proj3.Targeted_Installation_Date__c = Date.today().addYears(2);
                proj3.Currency__c = 'CHF';
                projList.add(proj3);
                
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
                Project__c proj5 = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Sales Promoter');
                proj5.MarketName__c = 'Germany';
                proj5.POS_Name__c = 'TestName';
                proj5.Targeted_Installation_Date__c = Date.today().addYears(-1);
                proj5.Currency__c = 'CHF';
                projList.add(proj5);
                
                Database.insert(projList, false);
                //insert projList;
                
                // Create Maintenance Outside Warranty project
               proj5 = [Select Corner__c from Project__c where Id=:proj5.Id];
                
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Maintenance Outside Warranty').getRecordTypeId();
                Project__c proj4 = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Maintenance Outside Warranty');
                proj4.MarketName__c = 'Germany';
                proj4.POS_Name__c = 'TestName';
                proj4.Targeted_Installation_Date__c = Date.today().addYears(-1);
                proj4.Currency__c = null;
                proj4.Corner__c = proj5.Corner__c;
                
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Maintenance Under Warranty').getRecordTypeId();
                Project__c proj6 = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Maintenance Under Warranty');
                proj6.MarketName__c = 'Germany';
                proj6.POS_Name__c = 'TestName';
                proj6.Targeted_Installation_Date__c = Date.today().addYears(-1);
                proj6.Currency__c = null;
                proj6.Corner__c = proj5.Corner__c;
                insert new List<Project__c>{proj4, proj6};

                // Create Handover Report
               /*  Handover_Report__c hr = new Handover_Report__c(Installer_Arrival_Date__c = Date.Today(), Installer_Arrival_Time__c = '12:00', Installer_Departure_Date__c = Date.Today(), Installer_Departure_Time__c = '12:00', 
                        Project__c = proj.Id, General_cleaning__c = 'Yes', Supplier_on_time__c = 'Yes', Installation_feedback__c = 'a', 
                        Installation_area_cleared_before__c = 'Yes');
                
                Furniture__c furn = new Furniture__c(Name = 'test furniture', Active__c = true, Completed_items__c = true);
                insert furn;
                
                Furnitures_Inventory__c finv = new Furnitures_Inventory__c(Furniture__c = furn.Id, Project__c = proj.Id, Quantity__c = 1);
                insert finv;
                
                insert hr;
                
                List<Quotation__c> quotList = new List<Quotation__c>();
                
                rtId = Schema.SObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
                Quotation__c quot = new Quotation__c(Proforma_no__c = '1', Project__c = proj.Id, Furnitures__c = 100, 
                        Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD'
                        , Approval_Status__c = 'Approved by NES', Total_in_market_currency__c = 100, Total_in_main_currency__c = 100);
                quotList.add(quot);
                
                rtId = Schema.SObjectType.Quotation__c.getRecordTypeInfosByName().get('Architect Quotation').getRecordTypeId();
                Quotation__c quot2 = new Quotation__c(Proforma_no__c = '1', Project__c = proj.Id, Furnitures__c = 100, 
                        Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD'
                        , Approval_Status__c = 'Approved by NES', Total_in_market_currency__c = 100, Total_in_main_currency__c = 100);
                quotList.add(quot2);
                
                rtId = Schema.SObjectType.Quotation__c.getRecordTypeInfosByName().get('Other Quotation').getRecordTypeId();
                Quotation__c quot3 = new Quotation__c(Proforma_no__c = '1', Project__c = proj.Id, Furnitures__c = 100, 
                        Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD'
                        , Approval_Status__c = 'Approved by NES', Total_in_market_currency__c = 100, Total_in_main_currency__c = 100);
                quotList.add(quot3);
                
                insert quotList;
                
               proj.Corner_Point_of_Sale_r__c = posAcc2.Id;
                proj.MarketName__c = 'Toto';
                update proj;
                */
                rtId = Schema.SObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Executive Drawings').getRecordTypeId();
                Project_File_Area__c file1 = new Project_File_Area__c(Name = 'test', Project__c = proj.Id, File_sent_date__c = Datetime.Now());
                
                rtId = Schema.SObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Layout Proposal').getRecordTypeId();
                Project_File_Area__c file2 = new Project_File_Area__c(Name = 'test2', Project__c = proj.Id, File_sent_date__c = Datetime.Now());
                
                rtId = Schema.SObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Supplier Drawings').getRecordTypeId();
                Project_File_Area__c file3 = new Project_File_Area__c(Name = 'test', Project__c = proj.Id, File_sent_date__c = Datetime.Now(), Architect_Decision__c='Approved', Architect_Decision_Sent_Date__c = Datetime.Now());

                insert new List<Project_File_Area__c>{file1, file2, file3};
                
                System.assert(CornerProjectHandler.InstallationInformationPopulated(proj.Id)==false);
                
                 CornerProjectHandler.RecalculateFileAreaSummaryFields(new Set<Id>{proj.Id});
                CornerProjectHandler.RecalculateHandoverReportSummaryFields(new Set<Id>{proj.Id});
                CornerProjectHandler.RecalculateQuotationSummaryFields(new Set<Id>{proj.Id});
                
                CornerProjectHandler.SetForecastLinks(new List<Project__c>{proj});
                CornerProjectHandler.ConvertCurrencies(new List<Project__c>{proj});
                
                // test PushProjectFieldsToCornerOnProjectClosure
               proj.Approval_Status__c = 'Closed';
                update proj;

                // Test Market Sharing update
                market2.Default_Supplier__c = supp2Id;
                update market2;

                Test.StopTest();
            }
    }

}
@isTest
private class TestCornerProjectSharingRecalc {
   class CustomException extends Exception {}
   
    // Test for the projectProjectSharingRecalc class    
    static testMethod void testApexSharing(){
    	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {
    	
    	
	       // Instantiate the class implementing the Database.Batchable interface.     
	        cornerProjectSharingRecalc recalc = new CornerProjectSharingRecalc();
	        
	        Map<String, Id> profileMap = new Map<String, Id>();
	            for (Profile p : [SELECT Id, Name FROM Profile])
	                profileMap.put(p.Name, p.Id);
	        
	        // Select users for the test.
	        List<User> usersList = new List<User>();
	        usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
	                    
	        usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
	        insert usersList;
	        
	        Id suppId = usersList[0].Id;
	        Id archId = usersList[1].Id;
	        
	        // Insert some test project records.                 
	        List<Project__c> testprojects = new List<Project__c>();
	        for (Integer i=0;i<5;i++) {
	        Project__c j = new Project__c();
	            j.Supplier__c = suppId;
	            j.Architect__c = archId;
	            j.POS_Name__c = 'TestName';
	            testprojects.add(j);
	        }
	        insert testprojects;
	        
	        Test.startTest();
	        
	        // Invoke the Batch class.
	        String projectId = Database.executeBatch(recalc);
	        
	        Test.stopTest();
	        
	        // Get the Apex project and verify there are no errors.
	        AsyncApexJob aaj = [Select JobType, TotalJobItems, JobItemsProcessed, Status, 
	                            CompletedDate, CreatedDate, NumberOfErrors 
	                            from AsyncApexJob where Id = :projectId];
	        System.assertEquals(0, aaj.NumberOfErrors);
	      
	        // This query returns projects and related sharing records that were inserted       
	        // by the batch project's execute method.     
	        List<Project__c> projects = [SELECT Id, Architect__c, Supplier__c, 
	            (SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause FROM Shares 
	            WHERE (RowCause = :Schema.project__Share.rowCause.IsSupplier__c OR 
	            RowCause = :Schema.Project__Share.rowCause.IsArchitect__c))
	            FROM Project__c];       
	        
	        // Validate that Apex managed sharing exists on projects.     
	        for(Project__c project : projects){
	            // Two Apex managed sharing records should exist for each project
	            // when using the Private org-wide default. 
	            System.assert(project.Shares.size() == 2);
	            
	            for(Project__Share projectshr : project.Shares){
	               // Test the sharing record for Architect on project.             
	                if(projectshr.RowCause == Schema.project__Share.RowCause.IsArchitect__c){
	                    System.assertEquals(projectshr.UserOrGroupId,project.Architect__c);
	                    System.assertEquals('Edit', projectshr.AccessLevel);
	                }
	                // Test the sharing record for Supplier on project.
	                else if(projectshr.RowCause == Schema.project__Share.RowCause.IsSupplier__c){
	                    System.assertEquals(projectshr.UserOrGroupId,project.Supplier__c);
	                    System.assertEquals('Edit', projectshr.AccessLevel);
	                }
	            }
	        }
	        
	        CornerProjectSharingRecalc.SendApexSharingRecalculationErrors('This is the error');
	        CornerProjectSharingRecalc.SendApexSharingRecalculationException(new CustomException('This is the exception'));
    	}
    }
}
@isTest(seeAllData=true)
public with sharing class TestCornerReportsController {
	static testMethod void test(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        	Test.startTest();
        	
        	CornerReportsController crc = new CornerReportsController();
        	CornerReportsController.ZonesGraph1Wrapper zgw1 = crc.ZonesGraph1;        	
        	CornerReportsController.ZonesGraph2Wrapper zgw2 = crc.ZonesGraph2;
        	List<CornerReportsController.Project> cptd = crc.CornerPhotosToDisplayList;
        	if (cptd.size() >0){
        		CornerReportsController.Project proj = cptd[0];
        		String name = proj.Name;
				String posName = proj.POSName;
				String marketName = proj.MarketName;
				Datetime actualInstallationDate = proj.ActualInstallationDate;
				Id posId = proj.PosId;
				Id marketId = proj.MarketId;
				Decimal totalCostCHF = proj.TotalCostCHF;
				Decimal totalCostForecastCHF = proj.TotalCostForecastCHF;
				String zone = proj.Zone;
				Boolean isNewCorner = proj.IsNewCorner;
				Boolean isCancelled = proj.IsCancelled;
				Boolean isOnGoing = proj.IsOnGoing;
				Boolean displayImages = proj.displayImages;
        	}
        	       			
        	
        	List<CornerReportsController.PieWrapper> ogppd = crc.OnGoingProjectsPieData;
        	Integer top = crc.TotalOnGoingProjects;
        	List<CornerReportsController.PieWrapper> icpd = crc.InstalledCornersYTDPieData;
        	Integer tip = crc.TotalInstalledProjects;
        	List<CornerReportsController.PieWrapper> fcpd = crc.ForecastedCornersPieData;
        	Integer tfc = crc.TotalForecastedCorners;
        	String gc = crc.GlobalComment;
        	crc.printPage();
        	crc.setGlobalComment();
        	crc.setZoneComment();
        	
        	
        	/*
        	// Test BoutiqueReportHelper
        	System.assertEquals('1st', BoutiqueReportHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 1, 1)));
        	System.assertEquals('2nd', BoutiqueReportHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 4, 1)));
        	System.assertEquals('3rd', BoutiqueReportHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 7, 1)));
        	System.assertEquals('4th', BoutiqueReportHelper.GetQuarterNumberStr(Date.newInstance(Date.today().Year(), 10, 1)));
        	
        	System.assertEquals(Date.newInstance(Date.today().Year(), 1, 1), BoutiqueReportHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 1, 15)));
        	System.assertEquals(Date.newInstance(Date.today().Year(), 4, 1), BoutiqueReportHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 4, 15)));
        	System.assertEquals(Date.newInstance(Date.today().Year(), 7, 1), BoutiqueReportHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 7, 15)));
        	System.assertEquals(Date.newInstance(Date.today().Year(), 10, 1), BoutiqueReportHelper.GetQuarterStart(Date.newInstance(Date.today().Year(), 10, 15)));
        	
        	System.assertEquals(0, BoutiqueReportHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 1, 15)));
        	System.assertEquals(1, BoutiqueReportHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 4, 15)));
        	System.assertEquals(2, BoutiqueReportHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 7, 15)));
        	System.assertEquals(3, BoutiqueReportHelper.GetQuarterNumber(Date.newInstance(Date.today().Year(), 10, 15)));

        	System.assertEquals(Date.newInstance(Date.today().Year(), 4, 1), BoutiqueReportHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 1, 15)));
        	System.assertEquals(Date.newInstance(Date.today().Year(), 7, 1), BoutiqueReportHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 4, 15)));
        	System.assertEquals(Date.newInstance(Date.today().Year(), 10, 1), BoutiqueReportHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 7, 15)));
        	System.assertEquals(Date.newInstance(Date.today().Year()+1, 1, 1), BoutiqueReportHelper.GetQuarterEnd(Date.newInstance(Date.today().Year(), 10, 15)));

        	BoutiqueReportHelper.CreateImageLink(project.Id);
        	System.assertEquals('12\'345', BoutiqueReportHelper.FormatInteger(12345));
        	
        	// Test BoutiqueReportsDispatchController
        	BoutiqueReportsDispatchController c = new BoutiqueReportsDispatchController();
        	List<BoutiqueReportsDispatchController.Quarter> qList = c.Quarters;
			String firstQuarterLabel = qList[0].getQuarterLabel();        
        	Integer yearStr = c.Year;
        	String monthStr = c.MonthStr;
        	*/
            Test.stopTest();
        }
	}
}
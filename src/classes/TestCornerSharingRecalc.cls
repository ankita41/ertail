@isTest
private class TestCornerSharingRecalc {
    class CustomException extends Exception {}   
    // Test for the CornerSharingRecalc class    
    static testMethod void testApexSharing(){
    	
    	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {
    	
	       // Instantiate the class implementing the Database.Batchable interface.     
	        CornerSharingRecalc recalc = new CornerSharingRecalc();
	        
	        List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
	        gmsList.add(new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa'));
	        gmsList.add(new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs='));
	        insert gmsList;
	        
	        Map<String, Id> profileMap = new Map<String, Id>();
	            for (Profile p : [SELECT Id, Name FROM Profile])
	                profileMap.put(p.Name, p.Id);
	        
	        // Select users for the test.
	        List<User> usersList = new List<User>();
	        usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
	                    
	        usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
	        insert usersList;
	        
	        Id suppId = usersList[0].Id;
	        Id archId = usersList[1].Id;
	        
	        Market__c market = new Market__c(Name = 'Germany', Code__c = 'GER', Currency__c = 'USD');
	        insert market;
	        
	        // Create POS Account
	        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
	        Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, 
	                Market__c = market.Id, MarketName__c = market.Name, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
	                BillingPostalCode = 'zip', BillingCountry = 'country');
	        insert posAcc;
	        
	        // Insert some test corner records.                 
	        List<Corner__c> testcorners = new List<Corner__c>();
	        for (Integer i=0;i<5;i++) {
	        Corner__c j = new Corner__c();
	            j.Supplier__c = suppId;
	            j.Architect__c = archId;
	            j.Point_of_Sale__c = posAcc.Id;
	            testcorners.add(j);
	        }
	        insert testcorners;
	        
	        Test.startTest();
	        
	        // Invoke the Batch class.
	        String cornerId = Database.executeBatch(recalc);
	        
	        Test.stopTest();
	        
	        // Get the Apex corner and verify there are no errors.
	        AsyncApexJob aaj = [Select JobType, TotalJobItems, JobItemsProcessed, Status, 
	                            CompletedDate, CreatedDate, NumberOfErrors 
	                            from AsyncApexJob where Id = :cornerId];
	        System.assertEquals(0, aaj.NumberOfErrors);
	      
	        // This query returns corners and related sharing records that were inserted       
	        // by the batch corner's execute method.     
	        List<Corner__c> corners = [SELECT Id, Architect__c, Supplier__c, 
	            (SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause FROM Shares 
	            WHERE (RowCause = :Schema.corner__Share.rowCause.IsSupplier__c OR 
	            RowCause = :Schema.corner__Share.rowCause.IsArchitect__c))
	            FROM Corner__c];       
	        
	        // Validate that Apex managed sharing exists on corners.     
	        for(Corner__c corner : corners){
	            // Two Apex managed sharing records should exist for each corner
	            // when using the Private org-wide default. 
	            System.assert(corner.Shares.size() == 2);
	            
	            for(corner__Share cornershr : corner.Shares){
	               // Test the sharing record for Architect on corner.             
	                if(cornershr.RowCause == Schema.Corner__Share.RowCause.IsArchitect__c){
	                    System.assertEquals(cornershr.UserOrGroupId,corner.Architect__c);
	                    System.assertEquals('Edit', cornershr.AccessLevel);
	                }
	                // Test the sharing record for Supplier on corner.
	                else if(cornershr.RowCause == Schema.Corner__Share.RowCause.IsSupplier__c){
	                    System.assertEquals(cornershr.UserOrGroupId,corner.Supplier__c);
	                    System.assertEquals('Edit', cornershr.AccessLevel);
	                }
	            }
	        }
	        
	        CornerSharingRecalc.SendApexSharingRecalculationErrors('This is the error');
	        CornerSharingRecalc.SendApexSharingRecalculationException(new CustomException('This is the exception'));
	    }
    }
}
/*****
*   @Class      :   TestCreactAllCornerProjectEvaluation.cls
*   @Description:   Handles all tests for TestCreactAllCornerProjectEvaluation.
*   @Author     :   Thibauld
*   @Created    :   05 JUNE 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*                      
***/
@isTest
public with sharing class TestCreateAllCornerProjectEvaluation {
    public class CustomException extends Exception {}
    
    static testMethod void test() {

            ///////////////////////////////////////////////////////
            /// Test CreateAllCornerProjectEvaluation
            
            // Delete all the CornerProjectEvaluations
            delete [Select Id from Corner_Project_Evaluation__c];
            
            //Count the number of existing corner projects
            Integer nbProjects = [Select count() from Project__c where RecordTypeId = :CornerProjectHelper.cornerForecastAgentRT.Id or RecordTypeId = :CornerProjectHelper.cornerForecastRT.Id];
            
            // Recreate all the corner Project Evaluations
            CornerProjectEvaluationHandler.CreateAllCornerProjectEvaluation();
            
            // Validate the Corner Project evaluations have been created
            Integer nbCPECreated = [Select Count() from Corner_Project_Evaluation__c];
            System.assertEquals(nbProjects, nbCPECreated);
    }
}
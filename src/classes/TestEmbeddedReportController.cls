@isTest
public with sharing class TestEmbeddedReportController {
    static testMethod void test() {
    	//Report r = [Select Id from Report limit 1];
    	
		EmbeddedReportController erc = new EmbeddedReportController();
		//erc.report = '00O17000000FF36'; //r.Id;
    	EmbeddedReportController.GetBaseUrlForInstance();
    	erc.getThePanel();
    	erc.loadReport();
    	String s = erc.baseURL;
    	erc.baseURL = 'test';
    	EmbeddedReportController.Row r = new EmbeddedReportController.Row();
    	List<EmbeddedReportController.Cell> cells = r.cells;
    	
    	Map<String, Object> obj = new Map<String, Object>{'label' => 'label', 'value' => 'value', 'groupings' => new List<Object>{}};
    	
    	EmbeddedReportController.Cell c = new EmbeddedReportController.Cell(obj);
		s = c.label;
		c.label = 'test';
		s = c.value;
		
		EmbeddedReportController.Grouping grp = new EmbeddedReportController.Grouping(
			new Map<String, Object>{'label' => 'label', 'value' => 'value', 'key' => 'key', 'groupings' => new List<Object>{obj}}
			, new Map<String, Object>{'key!T' => new Map<String, Object>{ 'rows' => new List<Object>{ new Map<String, Object>{ 'dataCells' => new List<Object>{ obj}}} }});
			
        List<EmbeddedReportController.Grouping> groupings = grp.groupings;
        List<EmbeddedReportController.Row> rows = grp.rows;
        String key = grp.key;
        String label = grp.label;
        String value = grp.value;
        
        HttpRequest req = erc.BuildHTTPRequest();
        Boolean test = erc.IsValidId('test');
        test = erc.IsValidId('00O17000000FF36');
        erc.addChildComponents(grp, 0);
        
        Component.Apex.OutputText o = erc.constructHtmlText(groupings);
    }
}
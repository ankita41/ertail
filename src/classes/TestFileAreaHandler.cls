/*****

*   @Class      :   TestFileAreaHandler.cls
*   @Description:   Test methods for the File Area Handler
*   @Author     :   Thibauld
*   @Created    :   13 AUG 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
***/

@isTest
public with sharing class TestFileAreaHandler {
// Test for the FileAreaSharingRecalc class    
    static testMethod void testApexSharing(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {
       
	        Map<String, Id> profileMap = new Map<String, Id>();
	            for (Profile p : [SELECT Id, Name FROM Profile])
	                profileMap.put(p.Name, p.Id);
	        
	        // Select users for the test.
	        List<User> usersList = new List<User>();
	        usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
	                    
	        usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
	        
	        usersList.add(new User(Alias = 'bdm', Email='bdm@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Business Development Manager'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='bdm@testorg.com'));
	        
	        usersList.add(new User(Alias = 'agent', Email='agent@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('Agent Local Manager'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='agent@testorg.com'));
	                    
	        usersList.add(new User(Alias = 'lM', Email='localManager@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Local Manager'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='localManager@testorg.com'));
	                    
	        insert usersList;
	        
	        Id suppId = usersList[0].Id;
	        Id archId = usersList[1].Id;
			Id bdmId = usersList[2].Id;
			Id agentId = usersList[3].Id;
			Id localManagerId = usersList[4].Id;
			
			// Insert some test project records.                 
	        Project__c j = new Project__c();
	        j.Supplier__c = suppId;
	        j.Architect__c = archId;
	        j.Agent_Local_Manager__c = agentId;
	        j.NES_Business_Development_Manager__c = bdmId;
	        j.Nespresso_Contact__c = localManagerId;
	        j.POS_Name__c = 'TestName';
	        insert j;
			
	        // Insert some test RECO project records.                 
	        List<Project_File_Area__c> testpfas = new List<Project_File_Area__c>();
	        for (Integer i=0;i<5;i++) {
	        	Project_File_Area__c p = new Project_File_Area__c();
				p.Project__c = j.Id;
	            testpfas.add(p);
	        }
	        insert testpfas;
	    }
    }
}
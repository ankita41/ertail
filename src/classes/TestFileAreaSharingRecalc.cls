/*****
*   @Class      :   TestFileAreaSharingRecalc.cls
*   @Description:   Test class for FileAreaSharingRecalc
*   @Author     :   Thibauld
*   @Created    :   30 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestFileAreaSharingRecalc {
   class CustomException extends Exception {}

    // Test for the FileAreaSharingRecalc class    
    static testMethod void testApexSharing(){
    	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {
	       // Instantiate the class implementing the Database.Batchable interface.     
	        FileAreaSharingRecalc recalc = new FileAreaSharingRecalc();
	        
	        Map<String, Id> profileMap = new Map<String, Id>();
	            for (Profile p : [SELECT Id, Name FROM Profile])
	                profileMap.put(p.Name, p.Id);
	        
	        // Select users for the test.
	        List<User> usersList = new List<User>();
	        usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
	                    
	        usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
	        
	        usersList.add(new User(Alias = 'bdm', Email='bdm@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Business Development Manager'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='bdm@testorg.com'));
	        
	        usersList.add(new User(Alias = 'agent', Email='agent@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('Agent Local Manager'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='agent@testorg.com'));
	                    
	        usersList.add(new User(Alias = 'lM', Email='localManager@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Local Manager'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='localManager@testorg.com'));
	                    
	        insert usersList;
	        
	        Id suppId = usersList[0].Id;
	        Id archId = usersList[1].Id;
			Id bdmId = usersList[2].Id;
			Id agentId = usersList[3].Id;
			Id localManagerId = usersList[4].Id;
			
	        // Insert some test RECO project records.                 
	        List<Project_File_Area__c> testpfas = new List<Project_File_Area__c>();
	        for (Integer i=0;i<5;i++) {
	        	Project_File_Area__c p = new Project_File_Area__c();
	          	p.Architect__c = archId;
				p.Supplier__c = suppId;
				p.Nespresso_Contact__c = localManagerId;
				p.NES_Business_Development_Manager__c = bdmId;
				p.Agent_Local_Manager__c = agentId;
	            testpfas.add(p);
	        }
	        insert testpfas;
	
	        Test.startTest();
	        
	        // Invoke the Batch class.
	        String projectId = Database.executeBatch(recalc);
	        
	        Test.stopTest();
	        
	        // Get the Apex project and verify there are no errors.
	        AsyncApexJob aaj = [Select JobType, TotalJobItems, JobItemsProcessed, Status, 
	                            CompletedDate, CreatedDate, NumberOfErrors 
	                            from AsyncApexJob where Id = :projectId];
	        System.assertEquals(0, aaj.NumberOfErrors);
	      
	        // This query returns projects and related sharing records that were inserted       
	        // by the batch project's execute method.     
	        List<Project_File_Area__c> pfas = [Select p.Supplier__c
	        										, p.Nespresso_Contact__c
	        										, p.NES_Business_Development_Manager__c
	        										, p.Architect__c
	        										, p.Agent_Local_Manager__c
										            , (SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause FROM Shares 
										            WHERE (RowCause = :Schema.Project_File_Area__Share.rowCause.IsArchitect__c 
										            OR RowCause = :Schema.Project_File_Area__Share.rowCause.IsSupplier__c
										            OR RowCause = :Schema.Project_File_Area__Share.rowCause.IsProjectManager__c
										            OR RowCause = :Schema.Project_File_Area__Share.rowCause.IsAgentLocalManager__c
										            OR RowCause = :Schema.Project_File_Area__Share.rowCause.IsBusinessDevelopmentManager__c))
										            FROM Project_File_Area__c p];       
	        
	        // Validate that Apex managed sharing exists on purchaseOrders.     
	        for(Project_File_Area__c pfa : pfas){
	            // 5 Apex managed sharing records should exist for each project
	            // when using the Private org-wide default. 
	            System.assert(pfa.Shares.size() == 5);
	            
	            for(Project_File_Area__Share pfaShr : pfa.Shares){
	                // Test the sharing record for Retail Project Manager.             
	                if(pfaShr.RowCause == Schema.project_File_Area__Share.RowCause.IsProjectManager__c){
	                    System.assert(pfaShr.UserOrGroupId == pfa.Nespresso_Contact__c);
	                    System.assertEquals(pfaShr.AccessLevel,'Edit');
	                }
	                 // Test the sharing record for Architect          
	                else if(pfaShr.RowCause == Schema.project_File_Area__Share.RowCause.IsArchitect__c){
	                    System.assert(pfaShr.UserOrGroupId == pfa.Architect__c);
	                    System.assertEquals(pfaShr.AccessLevel,'Edit');
	                }
	                 // Test the sharing record for Supplier.             
	                if(pfaShr.RowCause == Schema.project_File_Area__Share.RowCause.IsSupplier__c){
	                    System.assert(pfaShr.UserOrGroupId == pfa.Supplier__c);
	                    System.assertEquals(pfaShr.AccessLevel,'Edit');
	                }
	                 // Test the sharing record for Agent Local Manager           
	                if(pfaShr.RowCause == Schema.project_File_Area__Share.RowCause.IsAgentLocalManager__c){
	                    System.assert(pfaShr.UserOrGroupId == pfa.Agent_Local_Manager__c);
	                    System.assertEquals(pfaShr.AccessLevel,'Edit');
	                }
	                 // Test the sharing record for Business Development Manager             
	                if(pfaShr.RowCause == Schema.project_File_Area__Share.RowCause.IsBusinessDevelopmentManager__c){
	                    System.assert(pfaShr.UserOrGroupId == pfa.NES_Business_Development_Manager__c);
	                    System.assertEquals(pfaShr.AccessLevel,'Edit');
	                }
	            }
	        }
	        FileAreaSharingRecalc.SendApexSharingRecalculationErrors('This is the error');
	        FileAreaSharingRecalc.SendApexSharingRecalculationException(new CustomException('This is the exception'));        
	    }
    	}
}
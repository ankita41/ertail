/*****

*   @Class      :   TestHandoverReportHandler.cls
*   @Description:   Test methods for the Handover Report Handler
*   @Author     :   Thibauld
*   @Created    :   13 AUG 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
***/

@isTest
public with sharing class TestHandoverReportHandler {
   
    // Test for the  class    
    static testMethod void testApexSharing(){       
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Map<String, Id> profileMap = new Map<String, Id>();
                for (Profile p : [SELECT Id, Name FROM Profile])
                    profileMap.put(p.Name, p.Id);
            
            // Select users for the test.
            List<User> usersList = new List<User>();
            usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
                        TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
                        
            usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
                        TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
            insert usersList;
            
            Id suppId = usersList[0].Id;
            Id archId = usersList[1].Id;
            
            // Insert some test project records.                 
            Project__c j = new Project__c();
            j.Supplier__c = suppId;
            j.Architect__c = archId;
            j.POS_Name__c = 'TestName';
            insert j;
            
            // Insert some test RECO project records.                 
            List<Handover_Report__c> testhrs = new List<Handover_Report__c>();
            for (Integer i=0;i<5;i++) {
                Handover_Report__c p = new Handover_Report__c();
                p.Project__c = j.Id;
                testhrs.add(p);
            }
            try{
            insert testhrs;}catch(Exception ex ){}
        }
    }
}
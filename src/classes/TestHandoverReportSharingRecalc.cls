/*****
*   @Class      :   TestHandoverReportSharingRecalc.cls
*   @Description:   Test class for HandoverReportSharingRecalc
*   @Author     :   Thibauld
*   @Created    :   31 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestHandoverReportSharingRecalc {
   	class CustomException extends Exception{}
   
    // Test for the FileAreaSharingRecalc class    
    static testMethod void testApexSharing(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs ( thisUser ) {
	       // Instantiate the class implementing the Database.Batchable interface.     
	        HandoverReportSharingRecalc recalc = new HandoverReportSharingRecalc();
	        
	        Map<String, Id> profileMap = new Map<String, Id>();
	            for (Profile p : [SELECT Id, Name FROM Profile])
	                profileMap.put(p.Name, p.Id);
	        
	        // Select users for the test.
	        List<User> usersList = new List<User>();
	        usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
	                    
	        usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
	        insert usersList;
	        
	        Id suppId = usersList[0].Id;
	        Id archId = usersList[1].Id;
	        
	        // Insert some test RECO project records.                 
	        List<Handover_Report__c> testhrs = new List<Handover_Report__c>();
	        for (Integer i=0;i<5;i++) {
	        	Handover_Report__c p = new Handover_Report__c();
	          	p.Architect__c = archId;
				p.Supplier__c = suppId;
	            testhrs.add(p);
	        }
	        insert testhrs;
	
	        Test.startTest();
	        
	        // Invoke the Batch class.
	        String projectId = Database.executeBatch(recalc);
	        
	        Test.stopTest();
	        
	        // Get the Apex project and verify there are no errors.
	        AsyncApexJob aaj = [Select JobType, TotalJobItems, JobItemsProcessed, Status, 
	                            CompletedDate, CreatedDate, NumberOfErrors 
	                            from AsyncApexJob where Id = :projectId];
	        System.assertEquals(0, aaj.NumberOfErrors);
	      
	        // This query returns projects and related sharing records that were inserted       
	        // by the batch project's execute method.     
	        List<Handover_Report__c> hrs = [Select p.Supplier__c
	        										, p.Architect__c
	        										, p.Agent_Local_Manager__c
										            , (SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause FROM Shares 
										            WHERE (RowCause = :Schema.Handover_Report__Share.rowCause.IsArchitect__c 
										            OR RowCause = :Schema.Handover_Report__Share.rowCause.IsSupplier__c))
										            FROM Handover_Report__c p];       
	        
	        // Validate that Apex managed sharing exists on Handover reports.     
	        for(Handover_Report__c hr : hrs){
	            // 5 Apex managed sharing records should exist for each project
	            // when using the Private org-wide default. 
	            System.assert(hr.Shares.size() == 2);
	            
	            for(Handover_Report__Share hrShr : hr.Shares){
	                 // Test the sharing record for Architect          
	                if(hrShr.RowCause == Schema.Handover_Report__Share.RowCause.IsArchitect__c){
	                    System.assert(hrShr.UserOrGroupId == hr.Architect__c);
	                    System.assertEquals(hrShr.AccessLevel,'Edit');
	                }
	                 // Test the sharing record for Supplier.             
	                if(hrShr.RowCause == Schema.Handover_Report__Share.RowCause.IsSupplier__c){
	                    System.assert(hrShr.UserOrGroupId == hr.Supplier__c);
	                    System.assertEquals(hrShr.AccessLevel,'Edit');
	                }
	            }
	        }
	        HandoverReportSharingRecalc.SendApexSharingRecalculationErrors('This is the error');
	        HandoverReportSharingRecalc.SendApexSharingRecalculationException(new CustomException('This is the exception')); 
	    }
    }
}
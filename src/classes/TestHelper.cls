/*****
*   @Class      :   TestHelper.cls
*   @Description:   Handles the creation of test records.
*   @Author     :   Jacky Uy
*   @Created    :   22 APR 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Jacky Uy        08 MAY 2014     Add: Trade Corner User Profiles
*   Jacky Uy        03 JUN 2014     Add: Estimate Item Currency & Amount fields
*   Jacky Uy        17 SEP 2014     Add: CreateCustomFolders()
*
*****/

@isTest
public with sharing class TestHelper{
    private static final Integer RECORD_COUNT = 20;
    
    public static User CurrentUser{
        get{ 
            if(CurrentUser == null)
                CurrentUser = [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
            
            return CurrentUser;
        }
        set{ CurrentUser = value; }
    }
    
    //BOUTIQUE PROFILES
    public static User RetailProjectManager{
        get{ 
            if(RetailProjectManager == null)
                RetailProjectManager = createUser('rpm', 'NES Retail Development', 'NES RECO Retail Project Manager');
            
            return RetailProjectManager;
        }
        set{ RetailProjectManager = value; }
    }
    
    public static User NationalBoutiqueManager{
        get{ 
            if(NationalBoutiqueManager == null)
                NationalBoutiqueManager = createUser('nbm', 'NES Retail Development', 'NES RECO National Boutique Manager');
            
            return NationalBoutiqueManager;
        }
        set{ NationalBoutiqueManager = value; }
    }
    
    public static User ManagerArchitect{
        get{ 
            if(ManagerArchitect == null)
                ManagerArchitect = createUser('ma', 'NES Retail Development', 'NES RECO Manager Architect');
            
            return ManagerArchitect;
        }
        set{ ManagerArchitect = value; }
    }
    
    public static User DesignArchitect{
        get{ 
            if(DesignArchitect == null)
                DesignArchitect = createUser('da', 'NES Retail Development', 'NES RECO Design Architect');
            
            return DesignArchitect;
        }
        set{ DesignArchitect = value; }
    }
    
    public static User MarketDirector{
        get{ 
            if(MarketDirector == null)
                MarketDirector = createUser('md', 'NES Retail Development', 'NES RECO Market Director');
            
            return MarketDirector;
        }
        set{ MarketDirector = value; }
    }
    
    public static User POOperator{
        get{ 
            if(POOperator == null)
                POOperator = createUser('po', 'NES Retail Development', 'NES RECO PO Operator');
            
            return POOperator;
        }
        set{ POOperator = value; }
    }
    
    //TRADE CORNER PROFILES
    public static User MarketManager{
        get{ 
            if(MarketManager == null)
                MarketManager = createUser('mktmgr', 'NES Market Local Manager', 'NES Market Local Manager');
            
            return MarketManager;
        }
        set{ MarketManager = value; }
    }
    
    public static User CornerArchitect{
        get{ 
            if(CornerArchitect == null)
                CornerArchitect = createUser('corArch', 'NES Corner Architect', 'NES Corner Architect');
            
            return CornerArchitect;
        }
        set{ CornerArchitect = value; }
    }
    
    public static User SalesPromoter{
        get{ 
            if(SalesPromoter == null)
                SalesPromoter = createUser('saleProm', 'NES Market Sales Promoter', 'NES Market Sales Promoter');
            
            return SalesPromoter;
        }
        set{ SalesPromoter = value; }
    }
    
    public static User TradeManager{
        get{ 
            if(TradeManager == null)
                TradeManager = createUser('trdMgr', 'NES Trade/Proc Manager', 'NES Trade/Proc Manager');
            
            return TradeManager;
        }
        set{ TradeManager = value; }
    }
    
    public static User Supplier{
        get{ 
            if(Supplier == null)
                Supplier = createUser('supp', 'NES Corner Supplier', 'NES Corner Supplier');
            
            return Supplier;
        }
        set{ Supplier = value; }
    }
    
    private static User createUser(String name, String role, String profile){
        UserRole tmpRole = new UserRole(Name = role);
        insert tmpRole;
        
        User tmpUser = new User(
            Alias = name, 
            Email = name + '@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'Testing',
            FirstName = name,
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey = 'en_US', 
            ProfileId = mapProfileIdsPerName().get(profile), 
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = name + '@testorg.com', 
            UserRoleId = tmpRole.Id
        );
        insert tmpUser;
        
        return tmpUser;
    }
    
    private static Map<String,Id> mapProfileIdsPerName(){
        Map<String,Id> mapProfileIdPerName = new Map<String,Id>();
        for(Profile p : [SELECT Id, Name FROM Profile]){
            mapProfileIdPerName.put(p.Name, p.Id);
        }
        return mapProfileIdPerName;
    }
    
    public static void createCurrencies(){
        List<Currency__c> newObjs = new List<Currency__c>();
        String[] currencies = new String[]{'AUD','CHF','CNY','EUR','GBP','HKD','JPY','PHP','SGD','USD'};
        
        for(Integer i=0; i<currencies.size()-1; i++){
            for(Integer j=0; j<currencies.size()-1; j++){
                if(i==j){
                    newObjs.add(new Currency__c(Year__c = '2013', From__c = currencies[i], To__c = currencies[j], Rate__c = 1.00));
                    newObjs.add(new Currency__c(Year__c = '2014', From__c = currencies[i], To__c = currencies[j], Rate__c = 1.00));
                    newObjs.add(new Currency__c(Year__c = '2015', From__c = currencies[i], To__c = currencies[j], Rate__c = 1.00));
                }
                else{
                    newObjs.add(new Currency__c(Year__c = '2013', From__c = currencies[i], To__c = currencies[j], Rate__c = 1.50));
                    newObjs.add(new Currency__c(Year__c = '2014', From__c = currencies[i], To__c = currencies[j], Rate__c = 1.50));
                    newObjs.add(new Currency__c(Year__c = '2015', From__c = currencies[i], To__c = currencies[j], Rate__c = 1.50));
                }
            }
        }
        insert newObjs;
    }
    
    public static void createCustomFolders(String parent){
        List<Custom_Folders__c> newCustomFolders = new List<Custom_Folders__c>();
        for(Integer i=0; i<20; i++){
            newCustomFolders.add(new Custom_Folders__c(
                Name = 'Folder ' + i,
                Parent_Object__c = parent,
                Folder_Number__c='\''+i+'\'' //Added new field 9th May 2016
            ));
        }
        insert newCustomFolders;
    }
        
    public static Market__c createMarket(){
        return new Market__c(
            Name = 'Belgium', 
            Code__c = 'BE1', 
            Currency__c = 'EUR',
            Zone__c = 'SWE'
        );
    }
    
    public static Account createAccount(Id rTypeId, Id marketId){
        return new Account(
            Market__c = marketId,
            RecordTypeId = rTypeId,
            Name = 'Test Account'
        );
    }
    
     public static Contact createContact(){
        return new Contact(
            firstname='Test',
            lastname='Test last name'
        );
    }
    
    public static Boutique__c createBoutique(Id rTypeId, Id accId){
        return new Boutique__c(
            boutique_location_account__c = accId,
            RecordTypeId = rTypeId,
            Name = 'Test Boutique'
        );
    }
    
    public static RECO_Project__c createBoutiqueProject(Id rTypeId, Id btqId){
        return new RECO_Project__c(
            Boutique__c = btqId,
            RecordTypeId = rTypeId,
            Status__c = 'Planned',
            Process_step__c = 'Stakeholders Definition',
            Project_Name__c = 'Test Boutique Project',
            Expected_soft_opening_date__c = SYSTEM.Today().addMonths(6),
            Boutique_CapEx_request_status__c = 'To be done'
        );
    }
    
    public static List<RECO_Project__c> createBoutiqueProjects(Id rTypeId, Id btqProjId){
        List<RECO_Project__c> newObjs = new List<RECO_Project__c>();
        for(Integer i=0; i<RECORD_COUNT; i++){
            newObjs.add(createBoutiqueProject(rTypeId, btqProjId));
        }
        return newObjs;
    }
    
    public static Handover__c createHandover(Id rTypeId, Id btqProjId){
        return new Handover__c(
            RecordTypeId = rTypeId,
            RECO_Project__c = btqProjId,
            Responsible__c = UserInfo.getUserId()
        );
    }
    
    public static Offer__c createOffer(Id rTypeId, Id btqProjId){
        return new Offer__c(
            RecordTypeId = rTypeId,
            RECO_Project__c = btqProjId,
            Proforma_No__c = 'Test 123',
            Total_Amount_excl_VAT__c = 1000,
            Currency__c = 'CHF'
        );
    }
    
    public static Purchase_Order__c createPO(Id offerId, Id btqProjId){
        return new Purchase_Order__c(
            Offer__c = offerId,
            RECO_Project__c = btqProjId,
            Name = 'Test PO',
            Currency__c = 'CHF',
            Amount__c = 1000
        );
    }

    public static Estimate_Item__c createEstimateItem(Id rTypeId, Id btqProjId){
        return new Estimate_Item__c(
            RecordTypeId = rTypeId,
            RECO_Project__c = btqProjId,
            Level_1__c = 'Fees',
            Level_2__c = 'Design',
            Currency__c = 'EUR',
            Amount__c = 1000
        );
    }
    
    public static List<Estimate_Item__c> createEstimateItems(Id rTypeId, Id btqProjId){
        List<Estimate_Item__c> newObjs = new List<Estimate_Item__c>();
        for(Integer i=0; i<RECORD_COUNT; i++){
            newObjs.add(createEstimateItem(rTypeId, btqProjId));
        }
        return newObjs;
    }

    public static Cost_Item__c createCostItem(Id rTypeId, Id btqProjId){
        return new Cost_Item__c(
            RecordTypeId = rTypeId,
            RECO_Project__c = btqProjId,
            Level_1__c = 'Fees',
            Level_2__c = 'Design',
            Currency__c = 'EUR',
            Expected_amount__c = 1000
        );
    }
    
    public static List<Cost_Item__c> createCostItems(Id rTypeId, Id btqProjId){
        List<Cost_Item__c> newObjs = new List<Cost_Item__c>();
        for(Integer i=0; i<RECORD_COUNT; i++){
            newObjs.add(createCostItem(rTypeId, btqProjId));
        }
        return newObjs;
    }
    
    public static Project__c CreateCornerProject(Id rTypeId, Id posId, Id keyAccountId, String workflowType){
        return new Project__c(
            RecordTypeId = rTypeId
            , Corner_Point_of_Sale_r__c = posId
            , Currency__c = 'EUR'
            , Estimated_Budget__c = 0
            , Architect_invoiced_cost__c = 0
            , Supplier_Invoiced_Cost__c = 0
            , Other_Invoiced_Cost__c = 0
            , Nb_of_machines_in_display__c = '1'
            , Nb_of_machines_in_stock__c = '2'
            , Floor__c = 'No'
            , Old_NES_Corner_to_remove__c = 'No'
            , Ecolaboration_module__c = 'No'
            , LCD_screen__c = 'No'
            , Visual__c = 'No'
            , Corner_Type_r__c = 'Island Corner'
            , Height_non_linear__c = 10
            , Width_non_linear__c = 10
            , Length_non_linear__c = 10
            , Corner_Unit_r__c = 'cm'
            , Height_under_ceiling_r__c = 1000
            , Walls_material_r__c = 'b', Floor_material_r__c = 'c'
            , Corner_Generation_r__c = '2011'
            , Electricity_coming_from_r__c = 'Floor'
            , Water_supply_needed_r__c = 'No'
            , Workflow_Type__c = workflowType
            , Key_Account__c = keyAccountId
            , Targeted_Installation_Date__c = Date.today()
        );
    }

}
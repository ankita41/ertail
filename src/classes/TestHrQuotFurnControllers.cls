@isTest
public with sharing class TestHrQuotFurnControllers {
    static testMethod void test() {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            // create custom settings
            List<Currency_Settings__c> csList = new List<Currency_Settings__c>();
            Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
            mycs.Main_Currency__c = 'CHF';
            csList.add(mycs);
            insert csList;            
            
            List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
            GoogleMapsSettings__c mycs2 = new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa');
            gmsList.add(mycs2);
            
            GoogleMapsSettings__c mycs3 = new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs=');
            gmsList.add(mycs3);

            insert gmsList;
                        
            CustomRedirection__c mycs4 = new CustomRedirection__c (Name = 'QuotationObjId', Value__c = '01Ib0000000R72V');
            insert mycs4;
            
            // Create users
            Map<String, Profile> mapProfilePerProfileName = new Map<String, Profile>();
            List<Profile> lstProfiles = [SELECT Id, Name FROM Profile];
            for (Profile p : lstProfiles) {
                mapProfilePerProfileName.put(p.Name, p);
            }
            
            Profile p = mapProfilePerProfileName.get('NES Corner Supplier'); 
            List<User> userList = new List<User>();
            User supp = new User(Alias = 'supp', Email='supplier@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com');
            userList.add(supp);
            
            p = mapProfilePerProfileName.get('NES Corner Architect'); 
            User arch = new User(Alias = 'arch', Email='arch@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com');
            userList.add(arch);
            
            p = mapProfilePerProfileName.get('NES Market Local Manager'); 
            UserRole r = [SELECT Id FROM UserRole WHERE Name = 'Germany'];
            User mngr = new User(Alias = 'mngr', Email='mngr@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='mngr@testorg.com');
            userList.add(mngr);
            //mngr = [SELECT Id, Name FROM User WHERE Username = 'market.nespresso@gmail.com.uat'];
            
            p = mapProfilePerProfileName.get('NES Market Sales Promoter');  
            User sale = new User(Alias = 'sale', Email='sale@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='sale@testorg.com');
            userList.add(sale);
            
            p = mapProfilePerProfileName.get('NES Trade/Proc Manager'); 
            User trad = new User(Alias = 'trad', Email='trad@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='trad@testorg.com');
            userList.add(trad);
            
            // Create Market
            List<Market__c> marketList = new List<Market__c>();
            Market__c market = TestHelper.createMarket();
            market.Name = 'Germany';
            market.Code__c = 'GER'; 
            market.NES_Project_Manager__c = mngr.Id;
            market.Default_Architect__c = arch.Id; 
            market.Default_Supplier__c = supp.Id; 
            market.Currency__c = 'USD';
            marketList.add(market);
            
            Market__c market2 = TestHelper.createMarket();
            market2.Name = 'Hungary';
            market2.Code__c = 'HU';
            market2.NES_Project_Manager__c = mngr.Id;
            market2.Default_Architect__c = arch.Id;
            market2.Default_Supplier__c = supp.Id; 
            market2.Currency__c = 'HUF';
            marketList.add(market2);
            
            insert marketList;
            
            // Create forecasts
            List<Objective__c> objList = new List<Objective__c>();
             objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(Date.today().addYears(-3).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(Date.today().addYears(-2).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(Date.today().addYears(-1).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = market.Id
                            , year__c = String.valueOf(Date.today().year())
                            , New_Members_Market_or_Region_Growth__c = 1
                            , Monthly_Average_Consumption__c = 2
                            , Caps_Cost_in_LC__c = 3
                            , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            insert(objList);            
            
            // Create Global Key account (to use as parent for the POS Group)
            List<Account> accList = new List<Account>();
            Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Key Account').getRecordTypeId();
            Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = rtId, Market__c = market.Id);
            accList.add(globalKeyAcc);
            
            // Create POS Group account (to use as parent for the POS Local)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Group').getRecordTypeId();
            Account posGroupAcc = new Account(Name = 'Test POS Group Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = globalKeyAcc.Id);
            accList.add(posGroupAcc);
            
            // Create POS Local account (to use as parent for the POS)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Local').getRecordTypeId();
            Account posLocalAcc = new Account(Name = 'Test POS Local Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = posGroupAcc.Id);
            accList.add(posLocalAcc);
            
            // Create POS Account
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
            Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                    Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'country');
            accList.add(posAcc);
            
            insert accList;
            
            // Create POS Contacts
            Contact storeElectrician = new Contact (LastName = 'Electrician', AccountId = posAcc.Id, Function__c = 'Store Electrician');
            insert storeElectrician;
            
            // Create Corner
            Corner__c corner = new Corner__c(Point_of_Sale__c = posAcc.Id, Unit__c = 'cm', Height_under_ceiling__c = 1000, 
                    Walls_material__c = 'abc', Floor_material__c = 'abc', Electricity_coming_from__c = 'Floor', 
                    Water_supply_needed__c = 'Yes');
            insert corner;
            
            // Create currency exchange rates
            Currency__c exrate = new Currency__c (From__c = 'CHF', To__c = 'USD', Rate__c = 1.1, Year__c = '2013');
            insert exrate;
            
            // Create document(s)
            Folder docFolder = [SELECT Id FROM Folder WHERE Name = 'Snag Images'];
            Document noImgDoc = new Document(Name='Test doc', DeveloperName = 'No_Image_Logo_test', FolderId = docFolder.Id, Body = blob.valueOf(''));
            insert noImgDoc;
            
            // Create Project with existing corner
            rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
            Project__c proj1 = new Project__c (RecordTypeId = rtId, Nb_of_machines_in_display__c = '1', Nb_of_machines_in_stock__c = '2', 
                    Floor__c = 'No', Old_NES_Corner_to_remove__c = 'No', Ecolaboration_module__c = 'No', LCD_screen__c = 'No', 
                    Visual__c = 'No', Estimated_Budget__c = 10000, NES_Sales_Promoter__c = sale.Id, 
                    Targeted_Installation_Date__c = date.newinstance(2013, 20, 9), Height_non_linear__c = 1000, Width_non_linear__c = 1000, 
                    Length_non_linear__c = 10, Corner__c = corner.Id);
            insert proj1;
            
            Project__c proj = [SELECT Store_Contact__c, Store_Electrician__c, Nespresso_Contact__c, Architect__c, Supplier__c, 
                    Corner_Point_of_Sale_r__c, Walls_material_r__c FROM Project__c WHERE Id = :proj1.Id];
            
            rtId = Schema.SObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Photos').getRecordTypeId();
            
            Project_File_Area__c file;
            //system.runAs(mngr) {
                file = new Project_File_Area__c(Name = 'test', Project__c = proj.Id, Main_Project_Image__c = true, RecordTypeId = rtId);
                insert file;
            //}

            Handover_Report__c hr = new Handover_Report__c(Installer_Arrival_Date__c = Date.Today(), Installer_Arrival_Time__c = '12:00', Installer_Departure_Date__c = Date.Today(), Installer_Departure_Time__c = '12:00', 
                    Project__c = proj.Id, General_cleaning__c = 'Yes', Supplier_on_time__c = 'Yes', Installation_feedback__c = 'a', 
                    Installation_area_cleared_before__c = 'Yes');
            
            Furniture__c furn = new Furniture__c(Name = 'test furniture', Active__c = true, Completed_items__c = true);
            insert furn;
            
            Furnitures_Inventory__c finv = new Furnitures_Inventory__c(Furniture__c = furn.Id, Project__c = proj.Id, Quantity__c = 1);
            insert finv;
            
            insert hr;
            
        ///////////////////////////////////////////////////
        // TEST HandoverButtonsController
        Test.startTest(); //code added
            ApexPages.StandardController std = new ApexPages.StandardController(hr);
            HandoverButtonsController hbc = new HandoverButtonsController(std);
            
            System.assertEquals(hbc.showSendForValidation, true);
            System.assertEquals(hbc.allowSendForValidation, true);
            System.assertEquals(hbc.showSendDecision, false);
            System.assertEquals(hbc.allowSendDecision, false);
            
            PageReference pageRef = hbc.SendForValidation();
            pageRef = hbc.SendDecision();
            pageRef = hbc.BackToProject();
            
        ///////////////////////////////////////////////////
        // TEST Handover PDF Generator
            ApexPages.StandardController std1 = new ApexPages.StandardController(hr);
            CornerHandoverPDFGenerator  controller1 = new CornerHandoverPDFGenerator (std1);
            Test.StopTest();// code added
            
        }
    }
}
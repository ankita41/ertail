/*****
*   @Class      :   TestImagesSnippetController.cls
*   @Description:   Test coverage for the ImagesSnippetController.cls
*   @Author     :   Thibauld
*   @Created    :   25 JUN 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*
*
*****/
@isTest
private with sharing class TestImagesSnippetController{
    static testMethod void unitTest(){
    	TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
        gmsList.add(new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa'));
        gmsList.add(new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs='));
        insert gmsList;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        Corner__c corner = new Corner__c(Point_of_Sale__c = acc.Id);
        insert corner;
        
        PageReference pageRef = Page.CornerImages;
        pageRef.getParameters().put('id', corner.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController con = new ApexPages.StandardController(corner);
        ImagesSnippetController ext = new ImagesSnippetController();
        
        ext.obj = con.getRecord();
        
        ext.newImage.ParentId = corner.Id;
        ext.newImage.ContentFileName = 'Test_File1.jpg';
        ext.newImage.ContentData = Blob.valueOf('Unit Testing');
        ext.addImage();
        
        ext.obj = con.getRecord();
        SYSTEM.Assert(ext.images.size() == 1);
        
        ext.newImage.ParentId = corner.Id;
        ext.newImage.ContentFileName = 'Test_File2.jpg';
        ext.newImage.ContentData = Blob.valueOf('Unit Testing');
        ext.addImage();
        ext.obj = con.getRecord();
        SYSTEM.Assert(ext.images.size() == 2);
        
        ext.newImage.ParentId = corner.Id;
        ext.newImage.ContentFileName = 'Test_File3.jpg';
        ext.newImage.ContentData = Blob.valueOf('Unit Testing');
        ext.addImage();
        ext.obj = con.getRecord();
        SYSTEM.Assert(ext.images.size() == 3);
        
        pageRef.getParameters().put('docId', ext.images[0].RelatedRecordId);
        pageRef = ext.setDefaultImage();
        pageRef.getParameters().put('docId', ext.images[1].RelatedRecordId);
        pageRef = ext.setDefaultImage2();
        pageRef.getParameters().put('docId', ext.images[2].RelatedRecordId);
        pageRef = ext.setDefaultImage3();
        pageRef.getParameters().put('docId', ext.images[2].RelatedRecordId);
        pageRef = ext.setDefaultImage2();
        pageRef.getParameters().put('docId', ext.images[2].RelatedRecordId);
        pageRef = ext.setDefaultImage();
        pageRef.getParameters().put('docId', ext.images[1].RelatedRecordId);
        pageRef = ext.setDefaultImage();
        
        SYSTEM.Assert([SELECT Default_Image__c FROM Corner__c WHERE Id = :corner.Id].Default_Image__c == ext.images[1].RelatedRecordId);
        
        pageRef.getParameters().put('feedId', ext.images[2].Id);
        pageRef.getParameters().put('docId', ext.images[2].RelatedRecordId);
        Test.setCurrentPage(pageRef);
        ext.delImage();
        ext.obj = con.getRecord();
        SYSTEM.Assert(ext.images.size() == 2);
        
        pageRef.getParameters().put('feedId', ext.images[1].Id);
        pageRef.getParameters().put('docId', ext.images[1].RelatedRecordId);
        Test.setCurrentPage(pageRef);
        ext.delImage();
        ext.obj = con.getRecord();
        SYSTEM.Assert(ext.images.size() == 1);
        
        pageRef.getParameters().put('feedId', ext.images[0].Id);
        pageRef.getParameters().put('docId', ext.images[0].RelatedRecordId);
        Test.setCurrentPage(pageRef);
        ext.delImage();
        ext.obj = con.getRecord();
        SYSTEM.Assert(ext.images.size() == 0);
    }
}
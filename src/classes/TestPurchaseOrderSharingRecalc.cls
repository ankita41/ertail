/*****
*   @Class      :   TestPurchaseOrderSharingRecalc.cls
*   @Description:   Test class for PurchaseOrderSharingRecalc
*   @Author     :   Thibauld
*   @Created    :   29 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestPurchaseOrderSharingRecalc {
   
    // Test for the PurchaseOrderSharingRecalc class    
    static testMethod void testApexSharing(){
       // Instantiate the class implementing the Database.Batchable interface.     
        purchaseOrderSharingRecalc recalc = new PurchaseOrderSharingRecalc();
        
        // Select users for the test.
        List<User> users = [SELECT Id FROM User WHERE IsActive = true LIMIT 2];
        ID User1Id = users[0].Id;
        ID User2Id = users[1].Id;
        
        // Insert some test RECO project records.                 
        List<RECO_Project__c> testprojects = new List<RECO_Project__c>();
        for (Integer i=0;i<5;i++) {
            RECO_Project__c p = new RECO_Project__c();
            p.Support_PO_Operator__c = User2Id;
            p.Support_National_Boutique_Manager__c = User2Id;
            p.Support_International_Retail_Operations__c = User2Id;
            p.Other_Retail_Project_Manager__c = User2Id;
            p.Other_Manager_Architect__c = User2Id;
            p.Other_Design_Architect__c = User2Id;
            p.Retail_project_manager__c = User1Id;
            p.PO_operator__c = User1Id;
            p.National_boutique_manager__c = User1Id;
            p.International_Retail_Operator__c = User1Id;
            p.Manager_architect__c = User1Id;
            p.Design_architect__c = User1Id;
            testprojects.add(p);
        }
        insert testprojects;
        
        // Create Supplier account
        Account suppAccount = new Account (Name = 'Test supplier', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Supplier'));
        insert suppAccount;

        Id approvedOfferRTId = Schema.SObjectType.Offer__c.getRecordTypeInfosByName().get('Approved').getRecordTypeId();
        List<Offer__c> offerList = new List<Offer__c>();
        
        for(RECO_Project__c p : testprojects){
            Offer__c offer = new Offer__c(Proforma_No__c = 'test 123', RECO_Project__c = p.Id, Total_Amount_excl_VAT__c = 1000, Currency__c = 'EUR', 
                    Supplier__c = suppAccount.Id, RecordTypeId = approvedOfferRTId);
            offerList.add(offer);
        }
        insert offerList;
        
        List<Purchase_Order__c> poList = new List<Purchase_Order__c>();
        for(Offer__c offer : offerList){
            poList.add(new Purchase_Order__c(Name = 'test-po-123', Offer__c = offer.Id, RECO_Project__c = offer.RECO_Project__c, Currency__c = 'EUR', 
                Amount__c = 1000, Description__c = 'description abc'));
        }
        insert poList;

	poList[0].Description__c = 'xyz';
	update poList[0];
                
        Test.startTest();
        
        // Invoke the Batch class.
        String projectId = Database.executeBatch(recalc);
        
        Test.stopTest();
        
        // Get the Apex project and verify there are no errors.
        AsyncApexJob aaj = [Select JobType, TotalJobItems, JobItemsProcessed, Status, 
                            CompletedDate, CreatedDate, NumberOfErrors 
                            from AsyncApexJob where Id = :projectId];
        System.assertEquals(0, aaj.NumberOfErrors);
      
        // This query returns projects and related sharing records that were inserted       
        // by the batch project's execute method.     
        List<Purchase_Order__c> purchaseOrders = [SELECT Id, po.RECO_Project__r.Support_PO_Operator__c
                                                , po.RECO_Project__r.Support_National_Boutique_Manager__c
                                                , po.RECO_Project__r.Support_International_Retail_Operations__c
                                                , po.RECO_Project__r.Other_Retail_Project_Manager__c
                                                , po.RECO_Project__r.Other_Manager_Architect__c
                                                , po.RECO_Project__r.Other_Design_Architect__c
                                                , po.RECO_Project__r.Retail_project_manager__c
                                                , po.RECO_Project__r.PO_operator__c
                                                , po.RECO_Project__r.National_boutique_manager__c
                                                , po.RECO_Project__r.International_Retail_Operator__c
                                                , po.RECO_Project__r.Manager_architect__c
                                                , po.RECO_Project__r.Design_architect__c
                                                , po.RECO_Project__c 
                                                , (SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause FROM Shares 
                                                WHERE (RowCause = :Schema.Purchase_Order__Share.rowCause.IsRetailProjectManager__c 
                                                 OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsNationalBoutiqueManager__c
                                                 OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsInternationalRetailOperations__c
                                                 OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsManagerArchitect__c
                                                 OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsDesignArchitect__c
                                                 OR RowCause = :Schema.Purchase_Order__Share.rowCause.IsPOOperator__c))
                                                FROM Purchase_Order__c po];        
        // Validate that Apex managed sharing exists on purchaseOrders.     
        for(Purchase_Order__c po : purchaseOrders){
            // 12 Apex managed sharing records should exist for each project
            // when using the Private org-wide default. 
            //System.assertEquals(12, po.Shares.size());
            
            for(Purchase_Order__Share purchaseOrdershr : po.Shares){
                // Test the sharing record for Retail Project Manager.             
                if(purchaseOrdershr.RowCause == Schema.purchase_Order__Share.RowCause.IsRetailProjectManager__c){
                    System.assert((purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.Retail_project_manager__c) || (purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.Other_Retail_Project_Manager__c));
                    System.assertEquals(purchaseOrdershr.AccessLevel,'Edit');
                }
                 // Test the sharing record for National Boutique Manager           
                else if(purchaseOrdershr.RowCause == Schema.purchase_Order__Share.RowCause.IsNationalBoutiqueManager__c){
                    System.assert((purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.National_boutique_manager__c) || (purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.Support_National_Boutique_Manager__c));
                    System.assertEquals(purchaseOrdershr.AccessLevel,'Edit');
                }
                 // Test the sharing record for International_Retail_Operator__c.             
                if(purchaseOrdershr.RowCause == Schema.purchase_Order__Share.RowCause.IsInternationalRetailOperations__c){
                    System.assert((purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.International_Retail_Operator__c) || (purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.Support_International_Retail_Operations__c));
                    System.assertEquals(purchaseOrdershr.AccessLevel,'Edit');
                }
                 // Test the sharing record for Manager Architect           
                if(purchaseOrdershr.RowCause == Schema.purchase_Order__Share.RowCause.IsManagerArchitect__c){
                    System.assert((purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.Manager_architect__c) || (purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.Other_Manager_Architect__c));
                    System.assertEquals(purchaseOrdershr.AccessLevel,'Edit');
                }
                 // Test the sharing record for Design Architect             
                if(purchaseOrdershr.RowCause == Schema.purchase_Order__Share.RowCause.IsDesignArchitect__c){
                    System.assert((purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.Design_architect__c) || (purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.Other_Design_Architect__c));
                    System.assertEquals(purchaseOrdershr.AccessLevel,'Edit');
                }
                 // Test the sharing record for PO Operator.             
                if(purchaseOrdershr.RowCause == Schema.purchase_Order__Share.RowCause.IsPOOperator__c){
                    System.assert((purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.PO_operator__c) || (purchaseOrdershr.UserOrGroupId == po.RECO_Project__r.Support_PO_Operator__c));
                    System.assertEquals(purchaseOrdershr.AccessLevel,'Edit');
                }
            }
        }
    }
}
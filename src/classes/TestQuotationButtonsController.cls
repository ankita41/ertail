@isTest
public with sharing class TestQuotationButtonsController {
    static testMethod void test() {
          User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            // create custom settings
            List<Currency_Settings__c> csList = new List<Currency_Settings__c>();
            Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
            mycs.Main_Currency__c = 'CHF';
            csList.add(mycs);
            insert csList;            
            
            List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
            GoogleMapsSettings__c mycs2 = new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa');
            gmsList.add(mycs2);
            
            GoogleMapsSettings__c mycs3 = new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs=');
            gmsList.add(mycs3);

            insert gmsList;
                        
            CustomRedirection__c mycs4 = new CustomRedirection__c (Name = 'QuotationObjId', Value__c = '01Ib0000000R72V');
            insert mycs4;
            
            // Create users
            Map<String, Profile> mapProfilePerProfileName = new Map<String, Profile>();
            List<Profile> lstProfiles = [SELECT Id, Name FROM Profile];
            for (Profile p : lstProfiles) {
                mapProfilePerProfileName.put(p.Name, p);
            }
            
            Profile p = mapProfilePerProfileName.get('NES Corner Supplier'); 
            List<User> userList = new List<User>();
            User supp = new User(Alias = 'supp', Email='supplier@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com');
            userList.add(supp);
            
            p = mapProfilePerProfileName.get('NES Corner Architect'); 
            User arch = new User(Alias = 'arch', Email='arch@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com');
            userList.add(arch);
            
            p = mapProfilePerProfileName.get('NES Market Local Manager'); 
            UserRole r = [SELECT Id FROM UserRole WHERE Name = 'Germany'];
            User mngr = new User(Alias = 'mngr', Email='mngr@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='mngr@testorg.com');
            userList.add(mngr);
            //mngr = [SELECT Id, Name FROM User WHERE Username = 'market.nespresso@gmail.com.uat'];
            
            p = mapProfilePerProfileName.get('NES Market Sales Promoter');  
            User sale = new User(Alias = 'sale', Email='sale@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='sale@testorg.com');
            userList.add(sale);
            
            p = mapProfilePerProfileName.get('NES Trade/Proc Manager'); 
            User trad = new User(Alias = 'trad', Email='trad@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='trad@testorg.com');
            userList.add(trad);
            
            // Create Market
            List<Market__c> marketList = new List<Market__c>();
            Market__c market = new Market__c(Name = 'Germany', Code__c = 'GER', NES_Project_Manager__c = mngr.Id, 
                    Default_Architect__c = arch.Id, Default_Supplier__c = supp.Id, Currency__c = 'USD');
            marketList.add(market);
            
            Market__c market2 = new Market__c(Name = 'Hungary', Code__c = 'HU', NES_Project_Manager__c = mngr.Id, 
                    Default_Architect__c = arch.Id, Default_Supplier__c = supp.Id, Currency__c = 'HUF');
            marketList.add(market2);
            
            insert marketList;
            
            // Create forecasts
            List<Objective__c> objList = new List<Objective__c>();
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(Date.today().addYears(-3).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(Date.today().addYears(-2).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(Date.today().addYears(-1).year())
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = market.Id
                            , year__c = String.valueOf(Date.today().year())
                            , New_Members_Market_or_Region_Growth__c = 1
                            , Monthly_Average_Consumption__c = 2
                            , Caps_Cost_in_LC__c = 3
                            , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            insert(objList);            
            
            // Create Global Key account (to use as parent for the POS Group)
            List<Account> accList = new List<Account>();
            Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Key Account').getRecordTypeId();
            Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = rtId, Market__c = market.Id);
            accList.add(globalKeyAcc);
            
            // Create POS Group account (to use as parent for the POS Local)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Group').getRecordTypeId();
            Account posGroupAcc = new Account(Name = 'Test POS Group Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = globalKeyAcc.Id);
            accList.add(posGroupAcc);
            
            // Create POS Local account (to use as parent for the POS)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Local').getRecordTypeId();
            Account posLocalAcc = new Account(Name = 'Test POS Local Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = posGroupAcc.Id);
            accList.add(posLocalAcc);
            
            // Create POS Account
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
            Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                    Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'country');
            accList.add(posAcc);
            
            insert accList;
            
            // Create POS Contacts
            Contact storeElectrician = new Contact (LastName = 'Electrician', AccountId = posAcc.Id, Function__c = 'Store Electrician');
            insert storeElectrician;
            
            // Create Corner
            Corner__c corner = new Corner__c(Point_of_Sale__c = posAcc.Id, Unit__c = 'cm', Height_under_ceiling__c = 1000, 
                    Walls_material__c = 'abc', Floor_material__c = 'abc', Electricity_coming_from__c = 'Floor', 
                    Water_supply_needed__c = 'Yes');
            insert corner;
            
            // Create currency exchange rates
            Currency__c exrate = new Currency__c (From__c = 'CHF', To__c = 'USD', Rate__c = 1.1, Year__c = String.valueOf(Date.today().year()));
            insert exrate;
            
            // Create document(s)
            Folder docFolder = [SELECT Id FROM Folder WHERE Name = 'Snag Images'];
            Document noImgDoc = new Document(Name='Test doc', DeveloperName = 'No_Image_Logo_test', FolderId = docFolder.Id, Body = blob.valueOf(''));
            insert noImgDoc;
         
            // Create Project with existing corner
            rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
            Project__c proj1 = new Project__c (RecordTypeId = rtId, Nb_of_machines_in_display__c = '1', Nb_of_machines_in_stock__c = '2', 
                    Floor__c = 'No', Old_NES_Corner_to_remove__c = 'No', Ecolaboration_module__c = 'No', LCD_screen__c = 'No', 
                    Visual__c = 'No', Estimated_Budget__c = 10000, NES_Sales_Promoter__c = sale.Id, 
                    Targeted_Installation_Date__c = date.newinstance(2013, 20, 9), Height_non_linear__c = 1000, Width_non_linear__c = 1000, 
                    Length_non_linear__c = 10, Corner__c = corner.Id);
            
            rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner (Agent)').getRecordTypeId();
            Project__c proj2 = new Project__c (RecordTypeId = rtId, Nb_of_machines_in_display__c = '1', Nb_of_machines_in_stock__c = '2', 
                    Floor__c = 'No', Old_NES_Corner_to_remove__c = 'No', Ecolaboration_module__c = 'No', LCD_screen__c = 'No', 
                    Visual__c = 'No', Estimated_Budget__c = 10000, NES_Sales_Promoter__c = sale.Id, 
                    Targeted_Installation_Date__c = date.newinstance(2013, 20, 9), Height_non_linear__c = 1000, Width_non_linear__c = 1000, 
                    Length_non_linear__c = 10, Corner__c = corner.Id);
                    
            rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Maintenance Outside Warranty').getRecordTypeId();
            Project__c proj3 = new Project__c (RecordTypeId = rtId, Nb_of_machines_in_display__c = '1', Nb_of_machines_in_stock__c = '2', 
                    Floor__c = 'No', Old_NES_Corner_to_remove__c = 'No', Ecolaboration_module__c = 'No', LCD_screen__c = 'No', 
                    Visual__c = 'No', Estimated_Budget__c = 10000, NES_Sales_Promoter__c = sale.Id, 
                    Targeted_Installation_Date__c = date.newinstance(2013, 20, 9), Height_non_linear__c = 1000, Width_non_linear__c = 1000, 
                    Length_non_linear__c = 10, Corner__c = corner.Id);
            
            insert new List<Project__c>{proj1, proj2, proj3};
                        
            rtId = Schema.SObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
            Quotation__c quot = new Quotation__c(Proforma_no__c = '1', Project__c = proj1.Id, Furnitures__c = 100, RecordTypeId = rtId,
                    Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD');
            
            Quotation__c quot2 = new Quotation__c(Proforma_no__c = '1', Project__c = proj2.Id, Furnitures__c = 100, RecordTypeId = rtId,
                    Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD');
                    
            Quotation__c quot3 = new Quotation__c(Proforma_no__c = '1', Project__c = proj3.Id, Furnitures__c = 100, RecordTypeId = rtId,
                    Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD');
            
            Test.startTest();   
            insert new List<Quotation__c> {quot, quot2, quot3};

                    ///////////////////////////////////////////////////
            // TEST SALES PROMOTER
            ApexPages.StandardController std = new ApexPages.StandardController(quot);
            QuotationButtonsController qbc = new QuotationButtonsController(std);
            
            Boolean b = qbc.showSendNESDecisionForSiteSurveyQuotation;
            b = qbc.allowSendNESDecisionForSiteSurveyQuotation;
            b = qbc.areProjectRequiredFieldsPopulatedBeforeSupplierQuotationApproval;
            
            System.assertEquals(qbc.showSendQuotationForApproval, true);
            System.assertEquals(qbc.allowSendQuotationForApproval, false);
            System.assertEquals(qbc.showSendNESDecision, false);
            System.assertEquals(qbc.allowSendNESDecision, false);
            System.assertEquals(qbc.showSendArchitectQuotationForApproval, false);
            System.assertEquals(qbc.allowSendArchitectQuotationForApproval, false);
            System.assertEquals(qbc.showSendArchitectDecision, false);
            System.assertEquals(qbc.allowSendArchitectDecision, false);
            System.assertEquals(qbc.showApproveSupplierQuotation, false);
            System.assertEquals(qbc.allowApproveSupplierQuotation, false);
            
            PageReference pageRef = qbc.SendQuotationForApproval();
            
            pageRef = qbc.SendNESDecision();
            pageRef = qbc.SendArchitectQuotationForApproval();
            pageRef = qbc.SendArchitectDecision();
            pageRef = qbc.ApproveSupplierQuotation();
            pageRef = qbc.SendNESDecisionForSiteSurveyQuotation();
            
            pageRef = qbc.BackToProject();
             Test.stopTest();
            // ON HOLD
            proj1.Status__c = CornerProjectButtonsController.ONHOLDSTATUS;
            update proj1;
            std = new ApexPages.StandardController(quot);
            qbc = new QuotationButtonsController(std);
            
            System.assertEquals(false, qbc.showSendQuotationForApproval);
            System.assertEquals(false, qbc.showSendNESDecision);
            System.assertEquals(false, qbc.showSendNESDecisionForSiteSurveyQuotation);
            System.assertEquals(false, qbc.showSendAgentDecisionToArchitect);
            System.assertEquals(false, qbc.showSendArchitectQuotationForApproval);
            System.assertEquals(false, qbc.showSendArchitectDecision);
            System.assertEquals(false, qbc.showApproveSupplierQuotation);
            System.assertEquals(false, qbc.showApproveSupplierQuotation_Agent);
                        
            /// AGENT
            std = new ApexPages.StandardController(quot2);
            qbc = new QuotationButtonsController(std);
            
            b = qbc.allowApproveSupplierQuotation_Agent;
            
            System.assertEquals(qbc.showApproveSupplierQuotation_Agent, false);
            System.assertEquals(qbc.showSendQuotationForApproval, true);
            System.assertEquals(qbc.allowSendQuotationForApproval, false);
            System.assertEquals(qbc.showSendAgentDecisionToArchitect, false);
            System.assertEquals(qbc.allowSendAgentDecisionToArchitect, false);
            
            pageRef = qbc.ApproveSupplierQuotation_Agent();
            
            /// MAINTENANCE OUTSIDE WARRANTY
            std = new ApexPages.StandardController(quot3);
            qbc = new QuotationButtonsController(std);
            
            b = qbc.allowApproveSupplierQuotation_Agent;
            
            System.assertEquals(qbc.showSendNESDecision, false);
            
            pageRef = qbc.SendNESDecision();
            pageRef = qbc.ApproveSupplierQuotation();
            
                ///////////////////////////////////////////////////
        // TEST SelectFurnituresController
            std = new ApexPages.StandardController(proj1);
            SelectFurnituresController sfc = new SelectFurnituresController(std);
            
            pageRef = sfc.SaveSelection();
            pageRef = sfc.Cancel();
            
           
        }
    }

}
/*****

*   @Class      :   TestQuotationHandler.cls
*   @Description:   Test methods for the Quotation Hnadler
*   @Author     :   Thibauld
*   @Created    :   13 AUG 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
***/

@isTest
public with sharing class TestQuotationHandler {
	
    // Test for the projectProjectSharingRecalc class    
    static testMethod void testApexSharing(){
    	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {

	        Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
	        mycs.Main_Currency__c = 'CHF';
	        insert mycs;
	        
	        Map<String, Id> profileMap = new Map<String, Id>();
	            for (Profile p : [SELECT Id, Name FROM Profile])
	                profileMap.put(p.Name, p.Id);
	        
	        // Select users for the test.
	        List<User> usersList = new List<User>();
	        usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
	                    
	        usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
	        insert usersList;
	        
	        Id suppId = usersList[0].Id;
	        Id archId = usersList[1].Id;
	        
	        // Insert some test project records.                 
	        Project__c j = new Project__c();
	        j.Supplier__c = suppId;
	        j.Architect__c = archId;
	        j.POS_Name__c = 'TestName';
	        insert j;
	        
			Id archQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Architect Quotation').getRecordTypeId();
			Id suppQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
			Id suppForArchQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation for Architect Approval').getRecordTypeId();
	
			Set<Id> archAuthorizedRTIds = new Set<Id>{archQuotRTId, suppQuotRTId, suppForArchQuotRTId};
			Set<Id> suppAuthorizedRTIds = new Set<Id>{suppQuotRTId, suppForArchQuotRTId};
	        
	        // Insert some test RECO project records.                 
	        List<Quotation__c> testqs = new List<Quotation__c>();
	    	// quotation shared with Architect and Supplier
	    	Quotation__c q1 = new Quotation__c();
			q1.Currency__c = 'CHF';
			q1.Project__c = j.Id;
			q1.RecordTypeId = suppQuotRTId;
	        testqs.add(q1);
	    	
	    	Quotation__c q2 = new Quotation__c();
			q2.Currency__c = 'CHF';
			q2.Project__c = j.Id;
			q2.RecordTypeId = archQuotRTId;
	        testqs.add(q2);
	    
	        insert testqs;
	
	
	    }
    }
}
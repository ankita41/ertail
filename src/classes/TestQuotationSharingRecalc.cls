/*****
*   @Class      :   TestQuotationSharingRecalc.cls
*   @Description:   Test class for QuotationSharingRecalc
*   @Author     :   Thibauld
*   @Created    :   30 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestQuotationSharingRecalc {
   class CustomException extends Exception{}
   
    // Test for the FileAreaSharingRecalc class    
    static testMethod void testApexSharing(){
    	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {

	       // Instantiate the class implementing the Database.Batchable interface.     
	        QuotationSharingRecalc recalc = new QuotationSharingRecalc();
	        
	        Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
	        mycs.Main_Currency__c = 'CHF';
	        insert mycs;
	        
	        Map<String, Id> profileMap = new Map<String, Id>();
	            for (Profile p : [SELECT Id, Name FROM Profile])
	                profileMap.put(p.Name, p.Id);
	        
	        // Select users for the test.
	        List<User> usersList = new List<User>();
	        usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
	                    
	        usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
	                    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                    LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
	                    TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
	        insert usersList;
	        
	        Id suppId = usersList[0].Id;
	        Id archId = usersList[1].Id;
	        
	        // Insert some test project records.                 
	        Project__c j = new Project__c();
	        j.Supplier__c = suppId;
	        j.Architect__c = archId;
	        j.POS_Name__c = 'TestName';
	        insert j;
	        
			Id archQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Architect Quotation').getRecordTypeId();
			Id suppQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
			Id suppForArchQuotRTId = Schema.sObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation for Architect Approval').getRecordTypeId();
	
			Set<Id> archAuthorizedRTIds = new Set<Id>{archQuotRTId, suppQuotRTId, suppForArchQuotRTId};
			Set<Id> suppAuthorizedRTIds = new Set<Id>{suppQuotRTId, suppForArchQuotRTId};
	        
	        // Insert some test RECO project records.                 
	        List<Quotation__c> testqs = new List<Quotation__c>();
	    	// quotation shared with Architect and Supplier
	    	Quotation__c q1 = new Quotation__c();
	      	q1.Architect__c = archId;
			q1.Supplier__c = suppId;
			q1.Resp_for_Site_Survey__c = archId;
			q1.Currency__c = 'CHF';
			q1.Project__c = j.Id;
			q1.RecordTypeId = suppQuotRTId;
	        testqs.add(q1);
	    
	    	
	    	Quotation__c q2 = new Quotation__c();
	      	q2.Architect__c = archId;
			q2.Supplier__c = suppId;
			q2.Resp_for_Site_Survey__c = suppId;
			q2.Currency__c = 'CHF';
			q2.Project__c = j.Id;
			q2.RecordTypeId = archQuotRTId;
	        testqs.add(q2);
	    
	        insert testqs;
	
	        Test.startTest();
	        
	        // Invoke the Batch class.
	        String quotationId = Database.executeBatch(recalc);
	        
	        Test.stopTest();
	        
	        // Get the Apex project and verify there are no errors.
	        AsyncApexJob aaj = [Select JobType, TotalJobItems, JobItemsProcessed, Status, 
	                            CompletedDate, CreatedDate, NumberOfErrors 
	                            from AsyncApexJob where Id = :quotationId];
	        System.assertEquals(0, aaj.NumberOfErrors);
	      
	        // This query returns projects and related sharing records that were inserted       
	        // by the batch project's execute method.     
	        List<Quotation__c> qs = [Select p.Supplier__c
	        										, p.Architect__c
										            , (SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause FROM Shares 
										            WHERE (RowCause = :Schema.Quotation__Share.rowCause.IsArchitect__c 
										            OR RowCause = :Schema.Quotation__Share.rowCause.IsSupplier__c
										           ))
										            FROM Quotation__c p];       
	        
	        // Validate that Apex managed sharing exists on purchaseOrders.     
	        for(Quotation__c q : qs){
	            // 1 Apex managed sharing records should exist for suppliers quotation
	            // when using the Private org-wide default. 
	            if (q.Id == q1.Id){
	            	System.assertEquals(2, q.Shares.size());
	            }
	            // 1 Apex managed sharing records should exist for architects quotation
	            // when using the Private org-wide default. 
	            else if (q.Id == q2.Id){
	            	System.assertEquals(1, q.Shares.size());
	            }
	            
	            for(Quotation__Share qShr : q.Shares){
	                 // Test the sharing record for Architect          
	                if(qShr.RowCause == Schema.Quotation__Share.RowCause.IsArchitect__c){
	                    System.assert(qShr.UserOrGroupId == q.Architect__c);
	                    System.assertEquals(qShr.AccessLevel,'Edit');
	                }
	                 // Test the sharing record for Supplier.             
	                if(qShr.RowCause == Schema.Quotation__Share.RowCause.IsSupplier__c){
	                    System.assert(qShr.UserOrGroupId == q.Supplier__c);
	                    System.assertEquals(qShr.AccessLevel,'Edit');
	                }
	
	            }
	        }
	        
	        QuotationSharingRecalc.SendApexSharingRecalculationErrors('This is the error');
	        QuotationSharingRecalc.SendApexSharingRecalculationException(new CustomException('This is the exception'));
	    }
    }
}
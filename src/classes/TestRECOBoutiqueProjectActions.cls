/*****
*   @Class      :   TestRECOBoutiqueProjectActions.cls
*   @Description:   Test coverage of the RECOProjectButtonsController.cls
*   @Author     :   Greg Baxter
*   @Created    :   23 Oct 2013
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy        14 AUG 2014     Re-wrote Test Class
*   Jacky Uy        22 AUG 2014     Change: BTQ Project Record Type from 'Standard Project' to 'Boutique Standard Project'
* 
*****/
 
@isTest
private class TestRECOBoutiqueProjectActions {
    static testMethod void test() {
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        bProj.Project_Type__c = 'New';
        insert bProj;
        
        Test.startTest();
        
        ApexPages.StandardController stdCon = new ApexPages.StandardController(bProj);
        RECOProjectButtonsController con = new RECOProjectButtonsController(stdCon);
        
        Boolean isTrue = con.hasAccessToProject;
        isTrue = con.showButtons;
        isTrue = con.isNcafeProj;
        isTrue = con.allStakeholdersPresent;
        isTrue = con.showInitiate;
        isTrue = con.showInformationCompleted;
        isTrue = con.showApproveBTQStartupBtn;
        isTrue = con.showSubmitEstCostToNBM;
        isTrue = con.showSubmitEstCostToRPM;  
        isTrue = con.showApproveRejectEstimatedCostsbyRPM;
        isTrue = con.showApproveRejectEstimatedCosts;
        isTrue = con.showApproveRejectEstimatedCostsLight;
        isTrue = con.showCostDefinedBtn;
        isTrue = con.rejectionReasonFilled;
        isTrue = con.allCostItemsConfirmedOrCanceled;
        isTrue = con.CapexCutoffFilled;
        isTrue = con.ClosureRejectionReasonFilled;
        isTrue = con.showLayoutValidationByRPM;
        isTrue = con.showProposeClosureByRPM;
        isTrue = con.showClosureValidationByMA;
        isTrue = con.showClosureValidationByNBM;
        isTrue = con.showClosureValidationByPO;
        isTrue = con.showClosureValidationByIRO;
        isTrue = con.showApproveDisapproveClosureByRPM;
        isTrue = con.showFreezeCostItems;
        isTrue = con.showUnfreezeCostItems;
        isTrue = con.noDocumentAttached;
        isTrue = con.allHandoversApproved;
        isTrue = con.noAsBuiltDocumentAttached;
        
        Integer tempInt = con.estimatedCostItemsCount;
        
        con.initiate();
        con.informationCompleted();
        con.approveBTQStartup();
        con.submitCostForApprovalToNBQ();
        con.submitCostForApprovalToRPM();
        con.defineApprovedCosts();
        con.rejectCosts();
        con.approveCosts();
        con.ProposeClosureByRPM();               
        con.ClosureValidationByMA();             
        con.ClosureValidationByNBM();                
        con.ClosureValidationByPO();
        con.ClosureValidationByIRO();
        con.DisapproveClosureByRPM();    
        con.ApproveClosureByRPM();
        con.FreezeCostItems();               
        con.UnfreezeCostItems();
        con.layoutValidationByRPM();
        con.editRecoProject();
        con.saveRecoProject();
                    
        Test.stopTest();
    }
}
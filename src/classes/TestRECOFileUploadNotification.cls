/*****
*   @Class      :   TestRECOFileUploadNotification.cls
*   @Description:   Test coverage for RECOBatchFileUploadNotificationPerDay.cls,RECOFileUploadNotifyPerDayScheduler.cls
                    ,RECOFileUploadNotificationIterator.cls, RECOFileUploadNotificationHelper.cls
*   @Author     :   Promila Boora
*   @Created    :   5 Feb 2016
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Promila Boora        5 Feb 2016     RECOFileUploadNotificationHelper.cls Test Coverage at 100%
*   Promila Boora        5 Feb 2016     RECOFileUploadNotificationIterator.cls Test Coverage at 76%
*   Promila Boora        5 Feb 2016     RECOFileUploadNotifyPerDayScheduler.cls Test Coverage at 100%
*   Promila Boora        5 Feb 2016     RECOBatchFileUploadNotificationPerDay.cls Test Coverage at 94%
*****/

@isTest
private with sharing class TestRECOFileUploadNotification{
 static testMethod void RECOFileUploadNotificationPerDayTest() {
    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id, Email = 'suppliercontact@test.com');
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        bProj.Workflow_Type__c = 'Light Process';
        bProj.Retail_project_manager__c = UserInfo.getUserId();
        bProj.National_boutique_manager__c = UserInfo.getUserId();
        bProj.Manager_architect__c = UserInfo.getUserId();
        insert bProj;
        EntitySubscription en=new EntitySubscription(ParentId=bProj.id,subscriberId=UserInfo.getUserId());
        insert en;
        Attachment_Folder__c mainFolder = new Attachment_Folder__c (Name='Pictures',RECO_Project__c=bProj.Id);
        insert mainFolder ;
        Attachment_Folder__c mainFolder1 = new Attachment_Folder__c (Name='Preliminary Information',RECO_Project__c=bProj.Id);
        insert mainFolder1 ;
        FeedItem post = new FeedItem();
        post.body = 'Testing of Feeds';
        post.parentid = mainFolder.Id;
        insert post;
        Attachment_SubFolder__c mainSubFolder = new Attachment_SubFolder__c (Name='Pictures',Attachment_Folder__c=mainFolder.Id);
        insert mainSubFolder ;
        FeedItem postSub = new FeedItem();
        postSub.body = 'Testing of Feeds';
        postSub.parentid = mainSubFolder.Id;
        insert postSub;
        FeedItem feedfolder2= new FeedItem(parentid = mainFolder1.Id,ContentFileName='test.txt',body='Testing file',ContentData=blob.valueof('test'));  
        insert feedfolder2;
        FeedItem feedfold1= new FeedItem(parentid = mainFolder.Id,ContentFileName='test.txt',body='Testing file',ContentData=blob.valueof('test'));  
        insert feedfold1;
        FeedItem feedfold2= new FeedItem(parentid = mainFolder.Id,ContentFileName='test11.txt',body='Testing file11',ContentData=blob.valueof('test11'));  
        insert feedfold2;
        FeedItem feedSub= new FeedItem(parentid = mainSubFolder.Id,ContentFileName='test1.txt',body='Testing file1',ContentData=blob.valueof('test1'));  
        insert feedSub;
        FeedItem feedSub1= new FeedItem(parentid = mainSubFolder.Id,ContentFileName='test21.txt',body='Testing file21',ContentData=blob.valueof('test21'));  
        insert feedSub1;
        Test.startTest();
        RECOBatchFileUploadNotificationPerDay schFileUploadNotification= new RECOBatchFileUploadNotificationPerDay(); 
        database.executebatch(schFileUploadNotification,1);
        Test.stopTest();
        }
      }
      
      static testMethod void RECOFileUploadNotificationIteratorTest() {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id, Email = 'suppliercontact@test.com');
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        bProj.Workflow_Type__c = 'Light Process';
        bProj.Retail_project_manager__c = UserInfo.getUserId();
        bProj.National_boutique_manager__c = UserInfo.getUserId();
        bProj.Manager_architect__c = UserInfo.getUserId();
        insert bProj;
        EntitySubscription en=new EntitySubscription(ParentId=bProj.id,subscriberId=UserInfo.getUserId());
        insert en;
        Attachment_Folder__c mainFolder = new Attachment_Folder__c (Name='Pictures',RECO_Project__c=bProj.Id);
        insert mainFolder ;
        FeedItem post = new FeedItem();
        post.body = 'Testing of Feeds';
        post.parentid = mainFolder.Id;
        insert post;
        Attachment_SubFolder__c mainSubFolder = new Attachment_SubFolder__c (Name='Pictures',Attachment_Folder__c=mainFolder.Id);
        insert mainSubFolder ;
        FeedItem postSub = new FeedItem();
        postSub.body = 'Testing of Feeds';
        postSub.parentid = mainSubFolder.Id;
        insert postSub;
       // FeedItem feed= new FeedItem(parentid = mainFolder.Id,ContentFileName='test.txt',body='Testing file',ContentData=blob.valueof('test'));  
       // insert feed;
        Test.startTest();
        RECOFileUploadNotificationIterator schFileUploadNotification= new RECOFileUploadNotificationIterator(); 
        Test.stopTest();
        }
      }
      
      static testMethod void RECOFileUploadNotifyPerDaySchedulerTest() {

            Test.startTest();
            String CRON_EXP = '0 0 0 1 1 ? 2019';  
            String jobId = System.schedule('testScheduledApex', CRON_EXP, new RECOFileUploadNotifyPerDayScheduler() );
            CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression); 
            System.assertEquals(0, ct.TimesTriggered);
            System.assertEquals('2019-01-01 00:00:00', String.valueOf(ct.NextFireTime));
            Test.stopTest();
    }
    static testmethod void RECOFileUploadNotificationHelperTest()
    {
        Test.startTest();
        String subId='Test';
        EntitySubscription entitySub=new EntitySubscription();
        Map<String,Map<String,List<Id>>> recProjectFeeds=new Map<String,Map<String,List<Id>>>();
        RECOFileUploadNotificationHelper rec=new RECOFileUploadNotificationHelper(subId,recProjectFeeds);
        Test.stopTest();
    }
}
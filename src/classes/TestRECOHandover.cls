/*****
*   @Class      :   RECOHandoverButtonsControllerTest.cls
*   @Description:   Test Coverage for the RECOHandoverButtonsController.cls
*   @Author     :   Greg Baxter
*   @Created    :   23 Oct 2013
*
*   Modification Log :  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy        18 SEP 2014     Revised Code
*                        
*****/

@isTest
private class TestRECOHandover {
    static testMethod void unitTest1() {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {
	    	TestHelper.createCurrencies();
	        Market__c market = TestHelper.createMarket();
	        insert market;
	        
	        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
	        Account acc = TestHelper.createAccount(rTypeId, market.Id);
	        insert acc;
	        
	        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
	        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
	        insert suppAcc;
	        
	        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id);
	        insert suppCon;
	    
	        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
	        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
	        insert btq;
	        
	        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
	        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
	        bProj.Currency__c = 'CHF';
	        bProj.Workflow_Type__c = 'Standard Process';
	        insert bProj;
	            
	        Test.startTest();
	        
	        Handover__c hr = new Handover__c(
	          Visit_date__c = Date.today(), 
	          Responsible__c = TestHelper.ManagerArchitect.Id, 
	          Supplier_contact__c = suppCon.Id, 
	          RECO_Project__c = bProj.Id
	        );
	        insert hr;
	        
	        // Test RECOHandoverButtonsController
	        ApexPages.StandardController std = new ApexPages.StandardController(hr);
	        RECOHandoverButtonsController controller = new RECOHandoverButtonsController(std);
	        
	        Boolean temp = controller.hasAccessToHandover;
	        temp = controller.showButtons;
	        temp = controller.showSendForValidation;
	        temp = controller.showValidateReject;
	        temp = controller.showSendForApproval;
	        temp = controller.showApproveDisapprove;
	        temp = controller.rejectionReasonFilled;
	        temp = controller.hasSnags;
	        temp = controller.hasToDoSnags;
	        
	        PageReference pageRef = controller.sendForValidation();
	        pageRef = controller.rejectHandover();
	        pageRef = controller.disapproveHandover();
	        pageRef = controller.backToProject();
	    
	        Snag__c snag = new Snag__c(Status__c = 'Done', Criticality__c = 'Normal', Area__c = 'Facade', Element__c = 'door', Handover__c = hr.Id);
	        insert snag;
	        
	        bProj.Status__c = 'Completed';
	        update bProj;
	        delete hr;
	        
	        Test.stopTest();
    	}
    }
    
    static testMethod void unitTest2() {
        TestHelper.createCurrencies();
        Market__c market = TestHelper.createMarket();
        insert market;
        
        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
        Account acc = TestHelper.createAccount(rTypeId, market.Id);
        insert acc;
        
        rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account suppAcc = TestHelper.createAccount(rTypeId, market.Id);
        insert suppAcc;
        
        Contact suppCon = new Contact(LastName = 'Supplier Contact', FirstName = 'Test', AccountId = suppAcc.Id, Email = 'suppliercontact@test.com');
        insert suppCon;
    
        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
        insert btq;
        
        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
        bProj.Currency__c = 'CHF';
        bProj.Workflow_Type__c = 'Light Process';
        bProj.Retail_project_manager__c = UserInfo.getUserId();
        bProj.National_boutique_manager__c = UserInfo.getUserId();
        bProj.Manager_architect__c = UserInfo.getUserId();
        insert bProj;
            
        Test.startTest();
        
        Handover__c hr = new Handover__c(
          Visit_date__c = Date.today(), 
          Responsible__c = UserInfo.getUserId(), 
          Supplier_contact__c = suppCon.Id, 
          RECO_Project__c = bProj.Id
        );
        insert hr;
        
        ApexPages.StandardController std = new ApexPages.StandardController(hr);
        RECOHandoverButtonsController controller = new RECOHandoverButtonsController(std);
        
        Boolean temp = controller.hasAccessToHandover;
        temp = controller.showButtons;
        temp = controller.showSendForValidation;
        temp = controller.showValidateReject;
        temp = controller.showSendForApproval;
        temp = controller.showApproveDisapprove;
        temp = controller.rejectionReasonFilled;
        temp = controller.hasSnags;
        temp = controller.hasToDoSnags;
        
        PageReference pageRef = controller.sendForValidation();
        pageRef = controller.validateHandover();
        pageRef = controller.rejectHandover();
        pageRef = controller.sendForApproval();
        pageRef = controller.approveHandover();
        pageRef = controller.disapproveHandover();
        pageRef = controller.backToProject();
    
        Snag__c snag = new Snag__c(Status__c = 'Done', Criticality__c = 'Normal', Area__c = 'Facade', Element__c = 'door', Handover__c = hr.Id);
        insert snag;
        
        bProj.Status__c = 'Completed';
        update bProj;
        delete hr;
        
        Test.stopTest();
    }
}
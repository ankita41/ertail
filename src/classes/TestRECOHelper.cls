public with sharing class TestRECOHelper {
	public static map<String,Id> ProfileIdsPerName (){
        map<String,Id> mapProfileIdPerName = new map<String,id>();
        
        for(Profile p : [SELECT Id, Name FROM Profile]){
			mapProfileIdPerName.put(p.Name, p.Id);
        }
        
        return mapProfileIdPerName;
    }
    
    public static map<String,Id> AccountRTIdsPerDevName (){
        map<String,Id> mapRTIdPerName = new map<String,id>();
        
        set<String> developerNames = new set<String>{'Business_Owner' ,'Architect_Agency', 'Boutique_Location', 'Contractor','Global_Key_Account', 'Nespresso','POS_Group',
        												'POS_Local','Point_of_Sales','Real_Estate_Agency','Supplier'}; 
                
        for(RecordType RT : [SELECT Id,DeveloperName FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName IN: developerNames]){
                    mapRTIdPerName.put(RT.developername, RT.id);
        }
        
        return mapRTIdPerName;
    }
    
    public static map<String,Id> BoutiqueProjectRTIdsPerDevName (){
        map<String,Id> mapRTIdPerName = new map<String,id>();
        
        for(RecordType RT : [SELECT Id,DeveloperName FROM RecordType WHERE SObjectType = 'RECO_Project__c']){
                    mapRTIdPerName.put(RT.DeveloperName, RT.id);
        }
        
        return mapRTIdPerName;
    }
    
	public static User getRetailProjectManager(){
		UserRole tmpRole = new UserRole(Name = 'NES Retail Development');
		insert tmpRole;
		
		User tmpUser = new User(Alias = 'rpm', Email='retail@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = ProfileIdsPerName().get('NES RECO Retail Project Manager'), 
                        TimeZoneSidKey='America/Los_Angeles', UserName='retail@testorg.com', UserRoleId = tmpRole.Id);
        insert tmpUser;
        
		return tmpUser;
    }
    
	public static User getNationalBoutiqueManager(){
		UserRole tmpRole = new UserRole(Name = 'NES Retail Development');
		insert tmpRole;
		
		User tmpUser = new User(Alias = 'nbm', Email='national@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = ProfileIdsPerName().get('NES RECO National Boutique Manager'), 
                        TimeZoneSidKey='America/Los_Angeles', UserName='national@testorg.com', UserRoleId = tmpRole.Id);
        insert tmpUser;
        
		return tmpUser;
    }
    
	public static User getManagerArchitect(){
		UserRole tmpRole = new UserRole(Name = 'NES Retail Development');
		insert tmpRole;
		
		User tmpUser = new User(Alias = 'ma', Email='marchitect@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = ProfileIdsPerName().get('NES RECO Manager Architect'), 
                        TimeZoneSidKey='America/Los_Angeles', UserName='marchitect@testorg.com', UserRoleId = tmpRole.Id);
        insert tmpUser;
        
		return tmpUser;
    }
    
	public static User getDesignArchitect(){
		UserRole tmpRole = new UserRole(Name = 'NES Retail Development');
		insert tmpRole;
		
		User tmpUser = new User(Alias = 'da', Email='darchitect@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = ProfileIdsPerName().get('NES RECO Design Architect'), 
                        TimeZoneSidKey='America/Los_Angeles', UserName='darchitect@testorg.com', UserRoleId = tmpRole.Id);
        insert tmpUser;
        
		return tmpUser;
    }
    
	public static User getMarketDirector(){
		UserRole tmpRole = new UserRole(Name = 'NES Retail Development');
		insert tmpRole;
		
		User tmpUser = new User(Alias = 'md', Email='market@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = ProfileIdsPerName().get('NES RECO Market Director'), 
                        TimeZoneSidKey='America/Los_Angeles', UserName='market@testorg.com', UserRoleId = tmpRole.Id);
        insert tmpUser;
        
		return tmpUser;
    }
    
	public static User getPoOperator(){
		UserRole tmpRole = new UserRole(Name = 'NES Retail Development');
		insert tmpRole;
		
		User tmpUser = new User(Alias = 'pop', Email='poperator@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = ProfileIdsPerName().get('NES RECO PO Operator'), 
                        TimeZoneSidKey='America/Los_Angeles', UserName='poperator@testorg.com', UserRoleId = tmpRole.Id);
        insert tmpUser;
        
		return tmpUser;
    }
}
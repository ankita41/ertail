/*****
*   @Class      :   TestRECOProjectSharingRecalc.cls
*   @Description:   Test class for RECOProjectSharingRecalc
*   @Author     :   Thibauld
*   @Created    :   30 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestRECOProjectSharingRecalc {
   	class CustomException extends Exception {}
   
    // Test for the projectsharingRecalc class    
    static testMethod void testApexSharing(){
       // Instantiate the class implementing the Database.Batchable interface.     
        RECOProjectSharingRecalc recalc = new RECOProjectSharingRecalc();
        
        // Select users for the test.
        List<User> users = [SELECT Id FROM User WHERE IsActive = true LIMIT 2];
        ID User1Id = users[0].Id;
        ID User2Id = users[1].Id;
        
        // Insert some test RECO project records.                 
        List<RECO_Project__c> testprojects = new List<RECO_Project__c>();
        for (Integer i=0;i<5;i++) {
            RECO_Project__c p = new RECO_Project__c();
            p.Support_PO_Operator__c = User2Id;
            p.Support_National_Boutique_Manager__c = User2Id;
            p.Support_International_Retail_Operations__c = User2Id;
            p.Other_Retail_Project_Manager__c = User2Id;
            p.Other_Manager_Architect__c = User2Id;
            p.Other_Design_Architect__c = User2Id;
            p.Retail_project_manager__c = User1Id;
            p.PO_operator__c = User1Id;
            p.National_boutique_manager__c = User1Id;
            p.International_Retail_Operator__c = User1Id;
            p.Manager_architect__c = User1Id;
            p.Design_architect__c = User1Id;
            testprojects.add(p);
        }
        insert testprojects;
                
        Test.startTest();
        
        // Invoke the Batch class.
        String projectId = Database.executeBatch(recalc);
        
        Test.stopTest();
        
        // Get the Apex project and verify there are no errors.
        AsyncApexJob aaj = [Select JobType, TotalJobItems, JobItemsProcessed, Status, 
                            CompletedDate, CreatedDate, NumberOfErrors 
                            from AsyncApexJob where Id = :projectId];
        System.assertEquals(0, aaj.NumberOfErrors);
      
        // This query returns projects and related sharing records that were inserted       
        // by the batch project's execute method.     
        List<RECO_Project__c> projects = [SELECT Id, p.Support_PO_Operator__c
                                                , p.Support_National_Boutique_Manager__c
                                                , p.Support_International_Retail_Operations__c
                                                , p.Other_Retail_Project_Manager__c
                                                , p.Other_Manager_Architect__c
                                                , p.Other_Design_Architect__c
                                                , p.Retail_project_manager__c
                                                , p.PO_operator__c
                                                , p.National_boutique_manager__c
                                                , p.International_Retail_Operator__c
                                                , p.Manager_architect__c
                                                , p.Design_architect__c
                                                , (SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause FROM Shares 
                                                WHERE (RowCause = :Schema.RECO_Project__Share.rowCause.IsRetailProjectManager__c 
                                                 OR RowCause = :Schema.RECO_Project__Share.rowCause.IsNationalBoutiqueManager__c
                                                 OR RowCause = :Schema.RECO_Project__Share.rowCause.IsInternationalRetailOperations__c
                                                 OR RowCause = :Schema.RECO_Project__Share.rowCause.IsManagerArchitect__c
                                                 OR RowCause = :Schema.RECO_Project__Share.rowCause.IsDesignArchitect__c
                                                 OR RowCause = :Schema.RECO_Project__Share.rowCause.IsPOOperator__c))
                                                FROM RECO_Project__c p];       
        
        // Validate that Apex managed sharing exists on projects.     
        for(RECO_Project__c p : projects){
            // 12 Apex managed sharing records should exist for each project
            // when using the Private org-wide default. 
            System.assert(p.Shares.size() == 12);
            
            for(RECO_Project__Share projectshr : p.Shares){
                // Test the sharing record for Retail Project Manager.             
                if(projectshr.RowCause == Schema.RECO_Project__Share.RowCause.IsRetailProjectManager__c){
                    System.assert((projectshr.UserOrGroupId == p.Retail_project_manager__c) || (projectshr.UserOrGroupId == p.Other_Retail_Project_Manager__c));
                    System.assertEquals(projectshr.AccessLevel,'Edit');
                }
                 // Test the sharing record for National Boutique Manager           
                else if(projectshr.RowCause == Schema.RECO_Project__Share.RowCause.IsNationalBoutiqueManager__c){
                    System.assert((projectshr.UserOrGroupId == p.National_boutique_manager__c) || (projectshr.UserOrGroupId == p.Support_National_Boutique_Manager__c));
                    System.assertEquals(projectshr.AccessLevel,'Edit');
                }
                 // Test the sharing record for Market Director.             
                if(projectshr.RowCause == Schema.RECO_Project__Share.RowCause.IsInternationalRetailOperations__c){
                    System.assert((projectshr.UserOrGroupId == p.International_Retail_Operator__c) || (projectshr.UserOrGroupId == p.Support_International_Retail_Operations__c));
                    System.assertEquals(projectshr.AccessLevel,'Edit');
                }
                 // Test the sharing record for Manager Architect           
                if(projectshr.RowCause == Schema.RECO_Project__Share.RowCause.IsManagerArchitect__c){
                    System.assert((projectshr.UserOrGroupId == p.Manager_architect__c) || (projectshr.UserOrGroupId == p.Other_Manager_Architect__c));
                    System.assertEquals(projectshr.AccessLevel,'Edit');
                }
                 // Test the sharing record for Design Architect             
                if(projectshr.RowCause == Schema.RECO_Project__Share.RowCause.IsDesignArchitect__c){
                    System.assert((projectshr.UserOrGroupId == p.Design_architect__c) || (projectshr.UserOrGroupId == p.Other_Design_Architect__c));
                    System.assertEquals(projectshr.AccessLevel,'Edit');
                }
                 // Test the sharing record for PO Operator.             
                if(projectshr.RowCause == Schema.RECO_Project__Share.RowCause.IsPOOperator__c){
                    System.assert((projectshr.UserOrGroupId == p.PO_operator__c) || (projectshr.UserOrGroupId == p.Support_PO_Operator__c));
                    System.assertEquals(projectshr.AccessLevel,'Edit');
                }
            }
        }
        RECOProjectSharingRecalc.SendApexSharingRecalculationErrors('This is the error');
        RECOProjectSharingRecalc.SendApexSharingRecalculationException(new CustomException('This is the exception')); 
    }
}
/***
 *	Tests:
 * 		- project stakeholders
 */

@isTest
private class TestRECOStakeholders {
	static testMethod void test() {
	    /*User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];
		System.runAs ( thisUser ) {
			// create custom settings
	    	Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
		    mycs.Main_Currency__c = 'CHF';
		    insert mycs;
		    
		    // create content folders
		    Content_Folder__c f1 = new Content_Folder__c(Name = 'Preliminary information');
		    insert f1;
		    
		    // Create users
		    User RetailPM = TestRECOHelper.getRetailProjectManager();
	        User ManagerArch = TestRECOHelper.getManagerArchitect();
	        User NationalBM = TestRECOHelper.getNationalBoutiqueManager();
	        User DesignArch = TestRECOHelper.getDesignArchitect();
	        User PoOperator = TestRECOHelper.getPoOperator();
	        User MarketDirector =  TestRECOHelper.getMarketDirector();
	        
	        // Create Market
	        Market__c market = new Market__c(Name = 'France', Code__c = 'FR', Currency__c = 'EUR');
	        insert market;
	        
	        // Create currency exchange rates
            Currency__c exrate = new Currency__c (From__c = 'CHF', To__c = 'EUR', Rate__c = 0.8, Year__c = '2013');
            insert exrate;
            
            Currency__c exrate2 = new Currency__c (From__c = 'EUR', To__c = 'CHF', Rate__c = 1.2, Year__c = '2013');
            insert exrate2;
            
	        // Create boutique
	        Boutique__c boutique;
	        System.runAs ( RetailPM ) {
		        // Create Business Owner Account
		        Account BusinessOwnerAcc = new Account(Name = 'Test Business Owner', Nessoft_ID__c = 'testid1234', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Business_Owner'),
		        		Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', BillingPostalCode = 'zip', BillingCountry = 'country');
		        insert BusinessOwnerAcc;
		        
		        // Create Global Key account
		        Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Global_Key_Account'));
		        insert globalKeyAcc;
		        
		        // Create Boutique Location Account
		        Account boutiqueLocationAcc = new Account(Name = 'Test B Location', Nessoft_ID__c = 'testid12340', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Boutique_Location'), 
		        		ParentId = globalKeyAcc.Id, Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
		                BillingPostalCode = 'zip', BillingCountry = 'country');
		        insert boutiqueLocationAcc;
		        
		        // Create Supplier account
		        Account suppAccount = new Account (Name = 'Test supplier', RecordTypeId = TestRECOHelper.AccountRTIdsPerDevName().get('Supplier'));
		        insert suppAccount;
	        	
		        boutique = new Boutique__c(Name = ' Test Boutique', Status__c = 'Planned', Typology__c = 'Boutique', Concept__c = 'Concept 2010', 
		        		Business_owner__c = BusinessOwnerAcc.Id, boutique_location_account__c = boutiqueLocationAcc.Id, Net_selling_m2__c = 250);
	    		insert boutique;
	        
	    		RECO_Project__c project = new RECO_Project__c(RecordTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId(), Status__c = 'Planned', 
		    				Process_step__c = 'Stakeholders definition', Boutique__c = boutique.Id, Project_Name__c = 'Description', Expected_soft_opening_date__c = Date.today(), 
		    				Boutique_CapEx_request_status__c = 'To be done');
		        insert project;
	        
		        Test.startTest();	        
		        
		        // create stakeholders
		        RecordType platformUserRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Stakeholder__c' AND DeveloperName = 'Platform_user'];
		        
		        Stakeholder__c sh_ma = new Stakeholder__c(Name = 'TBD', RecordTypeId = platformUserRT.Id, Platform_user__c =  ManagerArch.Id, Function__c = 'Manager architect', RECO_Project__c = project.Id);
		        insert sh_ma;
		        
		        Stakeholder__c sh_nbm = new Stakeholder__c(Name = 'TBD', RecordTypeId = platformUserRT.Id, Platform_user__c =  NationalBM.Id, Function__c = 'National boutique manager', RECO_Project__c = project.Id);
		        insert sh_nbm;
		        
		        Stakeholder__c sh_po = new Stakeholder__c(Name = 'TBD', RecordTypeId = platformUserRT.Id, Platform_user__c =  PoOperator.Id, Function__c = 'PO operator', RECO_Project__c = project.Id);
		        insert sh_po;
		        
		        Stakeholder__c sh_da = new Stakeholder__c(Name = 'TBD', RecordTypeId = platformUserRT.Id, Platform_user__c =  DesignArch.Id, Function__c = 'Design architect', RECO_Project__c = project.Id);
		        insert sh_da;
		        
		        Stakeholder__c sh_md = new Stakeholder__c(Name = 'TBD', RecordTypeId = platformUserRT.Id, Platform_user__c =  MarketDirector.Id, Function__c = 'Market director', RECO_Project__c = project.Id);
		        insert sh_md;
		        
		        RecordType draftRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Offer__c' AND DeveloperName = 'Draft'];
		        Offer__c offer = new Offer__c(Proforma_No__c = 'test 123', RECO_Project__c = project.Id, Total_Amount_excl_VAT__c = 1000, Currency__c = 'EUR', 
		        		Supplier__c = suppAccount.Id, RecordTypeId = draftRT.Id);
		        insert offer;
		        
		        offer = [SELECT Id, Retail_project_manager__c, Manager_architect__c, PO_operator__c, National_boutique_manager__c FROM Offer__c WHERE Id = :offer.Id];
				//System.assertEquals(ManagerArch.Id, offer.Manager_architect__c);
				//System.assertEquals(RetailPM.Id, offer.Retail_project_manager__c);
				//System.assertEquals(PoOperator.Id, offer.PO_operator__c);
				//System.assertEquals(NationalBM.Id, offer.National_boutique_manager__c);
				
				project = [SELECT Id, Manager_architect__c, Market_director__c, National_boutique_manager__c, PO_operator__c FROM RECO_Project__c WHERE Id = :project.Id];
		        //System.assertEquals(ManagerArch.Id, project.Manager_architect__c);
				//System.assertEquals(MarketDirector.Id, project.Market_director__c);
				//System.assertEquals(PoOperator.Id, project.PO_operator__c);
				//System.assertEquals(NationalBM.Id, project.National_boutique_manager__c);
		        
		        delete sh_md;
		        project = [SELECT Id, Manager_architect__c, Market_director__c, National_boutique_manager__c, PO_operator__c FROM RECO_Project__c WHERE Id = :project.Id];
		        //System.assertEquals(ManagerArch.Id, project.Manager_architect__c);
				//System.assertEquals(null, project.Market_director__c);
				//System.assertEquals(PoOperator.Id, project.PO_operator__c);
				//System.assertEquals(NationalBM.Id, project.National_boutique_manager__c);
		        
		        update sh_da;
		        
		        Test.stopTest();
	        }
		}*/
	}
}
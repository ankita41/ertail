/*****
*   @Class      :   TestRECO_UpdateFileCount.cls
*   @Description:   Test coverage for RECO_UpdateFileCountAttFolder,RECO_UpdateFileCountAttSubfolder trigger
*   @Author     :   Promila Boora
*   @Created    :   5 Feb 2016
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Promila Boora        5 Feb 2016     RECO_UpdateFileCountAttFolder trigger Test Coverage at 
*   Promila Boora        5 Feb 2016     RECO_UpdateFileCountAttSubfolder trigger Test Coverage at 

*****/

@isTest(seeAllData=true)
private with sharing class TestRECO_UpdateFileCount{
  class CustomException extends Exception{}
  static testMethod void RECORECO_UpdateFileCountAttFolder () {
       
       SYSTEM.runAs(TestHelper.CurrentUser){
           
            
            Attachment_Folder__c mainFolder=[Select id from Attachment_Folder__c where RECO_Project__c!=null and Name='Pictures' limit 1];
           /* EntitySubscription es = new EntitySubscription (
            ParentId = mainFolder.id,
            SubscriberId = Userinfo.getuserId()
            );
            insert es;*/
           /* FeedItem post = new FeedItem();
            post.body = 'Test folders';
            post.parentid = mainFolder.Id;
            insert post;*/
            Test.startTest();
            AttachmentFolderFilesExt attch=new AttachmentFolderFilesExt(new ApexPages.StandardController(mainFolder));
            attch.newFeedContent.ContentFileName='Test.txt';
            attch.newFeedContent.ContentData=blob.valueof('Test');
            //attch.addFile();
            update mainFolder;
            
            Test.stopTest();
 
   }
 }
 
 static testMethod void TestRECO_UpdateFileCountAttSubfolder() {
       
       SYSTEM.runAs(TestHelper.CurrentUser){
           
            TestHelper.createCurrencies();
	        Market__c market = TestHelper.createMarket();
	        insert market;
	        
	        Id rTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Boutique Location').getRecordTypeId();
	        Account acc = TestHelper.createAccount(rTypeId, market.Id);
	        insert acc;
	    
	        rTypeId = Schema.sObjectType.Boutique__c.getRecordTypeInfosByName().get('Boutique').getRecordTypeId();
	        Boutique__c btq = TestHelper.createBoutique(rTypeId, acc.Id);
	        insert btq;
	        
	        rTypeId = Schema.sObjectType.RECO_Project__c.getRecordTypeInfosByName().get('Boutique Standard Project').getRecordTypeId();
	        RECO_Project__c bProj = TestHelper.createBoutiqueProject(rTypeId, btq.Id);
	        bProj.Currency__c = 'CHF';
	        insert bProj;
	        
	        Offer__c offer = new Offer__c(RECO_Project__c = bProj.Id);
	        insert offer;
           
            Attachment_Folder__c mainFolder = new Attachment_Folder__c(name = 'Sngas',Offer__c=offer.Id,RECO_Project__c=bProj.id);
        	insert mainFolder;
            
           // Attachment_Folder__c mainFolder=[Select id from Attachment_Folder__c where RECO_Project__c!=null and Name='Snags' limit 1];
            Attachment_Subfolder__c attsub = new Attachment_Subfolder__c(Attachment_Folder__c = mainFolder.id,name = 'test record');
            insert attsub;
            FeedItem feeds = new FeedItem (ParentId = attsub.id,Body = 'Test Subfolders');
            insert feeds;
            
            FeedItem feedsMain = new FeedItem (ParentId = mainFolder.id,Body = 'Test Subfolders');
            insert feedsMain;
            
            update mainFolder;
            Test.startTest();
            AttachmentFolderFilesExt attch=new AttachmentFolderFilesExt(new ApexPages.StandardController(mainFolder));
            attch.newFeedContent.ContentFileName='Test.txt';
            attch.newFeedContent.ContentData=blob.valueof('Test');
            //attch.addFile();
            
            
            Map<Id,Boolean> fileCheckBoxMap = new Map<Id,Boolean>();
            List<FeedItem >files = [SELECT Body,ContentFileName, ContentDescription, ContentSize, RelatedRecordId, CreatedBy.Name, CreatedDate 
            FROM FeedItem WHERE ParentId = :mainFolder.Id ORDER BY CreatedDate DESC];        
            List<FeedItem> tempFeedItemList=new List<FeedItem>();
            for(FeedItem feed : files)
            {
                if(feed.ContentSize>0){
                    tempFeedItemList.add(feed);
                }  
            }
            if(!files.isEmpty()){
                files.clear();
                files.addall(tempFeedItemList);
            }
            
            
            for(FeedItem f : files){
                fileCheckBoxMap.put(f.Id,false);
            }
            attch.transferToFolderId=attsub.Id;
            attch.fileCheckBoxMap.putAll(fileCheckBoxMap);
            attch.transferFiles();
           /* EntitySubscription es = new EntitySubscription (
            ParentId = attsub .id,
            SubscriberId = Userinfo.getuserId()
            );
            insert es;
            FeedItem feed = new FeedItem (ParentId = attsub.id,Body = 'Test Subfolders');
            insert feed;*/
            update attsub;
            Test.stopTest();
 
   }
 }
 }
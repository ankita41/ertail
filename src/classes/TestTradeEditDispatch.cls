/*****
*   @Class      :   TestTradeEditDispatch.cls
*   @Description:   Tests - 
*   @Author     :   XXXXX
*   @Created    :   XXXXX
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   Thibauld        05 JUN 2014     Change: Set Global Key Account Market
*******/
@isTest
public with sharing class TestTradeEditDispatch {
    static testMethod void test() {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            // create custom settings
            Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
            mycs.Main_Currency__c = 'CHF';
            insert mycs;
            
            GoogleMapsSettings__c mycs2 = new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa');
            insert mycs2;
            
            GoogleMapsSettings__c mycs3 = new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs=');
            insert mycs3;
            
            CustomRedirection__c mycs4 = new CustomRedirection__c (Name = 'QuotationObjId', Value__c = '01Ib0000000R72V');
            insert mycs4;
            
            // Create users
            Map<String, Profile> mapProfilePerProfileName = new Map<String, Profile>();
            List<Profile> lstProfiles = [SELECT Id, Name FROM Profile];
            for (Profile p : lstProfiles) {
                mapProfilePerProfileName.put(p.Name, p);
            }
            
            Profile p = mapProfilePerProfileName.get('NES Corner Supplier'); 
            User supp = new User(Alias = 'supp', Email='supplier@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com');
            insert supp;
            
            p = mapProfilePerProfileName.get('NES Corner Architect'); 
            User arch = new User(Alias = 'arch', Email='arch@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com');
            insert arch;
            
            p = mapProfilePerProfileName.get('NES Market Local Manager'); 
            UserRole r = [SELECT Id FROM UserRole WHERE Name = 'Germany'];
            User mngr = new User(Alias = 'mngr', Email='mngr@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='mngr@testorg.com');
            insert mngr;
            //mngr = [SELECT Id, Name FROM User WHERE Username = 'market.nespresso@gmail.com.uat'];
            
            p = mapProfilePerProfileName.get('NES Market Sales Promoter');  
            User sale = new User(Alias = 'sale', Email='sale@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='sale@testorg.com');
            insert sale;
            
            p = mapProfilePerProfileName.get('NES Trade/Proc Manager'); 
            User trad = new User(Alias = 'trad', Email='trad@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='trad@testorg.com');
            insert trad;
            
            // Create Market
            Market__c market = new Market__c(Name = 'Germany', Code__c = 'GER', NES_Project_Manager__c = mngr.Id, 
                    Default_Architect__c = arch.Id, Default_Supplier__c = supp.Id, Currency__c = 'USD');
            insert market;
            
            Market__c market2 = new Market__c(Name = 'Hungary', Code__c = 'HU', NES_Project_Manager__c = mngr.Id, 
                    Default_Architect__c = arch.Id, Default_Supplier__c = supp.Id, Currency__c = 'HUF');
            insert market2;
            
                        // Create forecasts
            List<Objective__c> objList = new List<Objective__c>();
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(2013)
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(2014)
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4)); 
            objList.add(new Objective__c(Market__c = market.Id
                                        , year__c = String.valueOf(2015)
                                        , New_Members_Market_or_Region_Growth__c = 1
                                        , Monthly_Average_Consumption__c = 2
                                        , Caps_Cost_in_LC__c = 3
                                        , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));                          
            objList.add(new Objective__c(Market__c = market.Id
                            , year__c = String.valueOf(Date.today().year())
                            , New_Members_Market_or_Region_Growth__c = 1
                            , Monthly_Average_Consumption__c = 2
                            , Caps_Cost_in_LC__c = 3
                            , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
            insert(objList);  
            
            // Create Global Key account (to use as parent for the POS Group)
            Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Key Account').getRecordTypeId();
            Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = rtId, Market__c = market.Id);
            insert globalKeyAcc;
            
            // Create POS Group account (to use as parent for the POS Local)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Group').getRecordTypeId();
            Account posGroupAcc = new Account(Name = 'Test POS Group Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = globalKeyAcc.Id);
            insert posGroupAcc;
            
            // Create POS Local account (to use as parent for the POS)
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Local').getRecordTypeId();
            Account posLocalAcc = new Account(Name = 'Test POS Local Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = posGroupAcc.Id);
            insert posLocalAcc;
            
            // Create POS Account
            rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
            Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                    Market__c = market.Id, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                    BillingPostalCode = 'zip', BillingCountry = 'country');
            insert posAcc;
            
            // Create POS Contacts
            Contact storeElectrician = new Contact (LastName = 'Electrician', AccountId = posAcc.Id, Function__c = 'Store Electrician');
            insert storeElectrician;
            
            // Create Corner
            Corner__c corner = new Corner__c(Point_of_Sale__c = posAcc.Id, Unit__c = 'cm', Height_under_ceiling__c = 1000, 
                    Walls_material__c = 'abc', Floor_material__c = 'abc', Electricity_coming_from__c = 'Floor', 
                    Water_supply_needed__c = 'Yes');
            insert corner;
            
            // Create currency exchange rates
            Currency__c exrate = new Currency__c (From__c = 'CHF', To__c = 'USD', Rate__c = 1.1, Year__c = '2013');
            insert exrate;
            
            // Create document(s)
            Folder docFolder = [SELECT Id FROM Folder WHERE Name = 'Snag Images'];
            Document noImgDoc = new Document(Name='Test doc', DeveloperName = 'No_Image_Logo_test', FolderId = docFolder.Id, Body = blob.valueOf(''));
            insert noImgDoc;
            
            // Create Project with existing corner
            rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
            Project__c proj1 = new Project__c (RecordTypeId = rtId, Nb_of_machines_in_display__c = '1', Nb_of_machines_in_stock__c = '2', 
                    Floor__c = 'No', Old_NES_Corner_to_remove__c = 'No', Ecolaboration_module__c = 'No', LCD_screen__c = 'No', 
                    Visual__c = 'No', Estimated_Budget__c = 10000, NES_Sales_Promoter__c = sale.Id, 
                    Targeted_Installation_Date__c = date.newinstance(2014, 20, 9), Height_non_linear__c = 1000, Width_non_linear__c = 1000, 
                    Length_non_linear__c = 10, Corner__c = corner.Id);
            insert proj1;
            
            Project__c proj = [SELECT Store_Contact__c, Store_Electrician__c, Nespresso_Contact__c, Architect__c, Supplier__c, 
                    Corner_Point_of_Sale_r__c, Walls_material_r__c FROM Project__c WHERE Id = :proj1.Id];
            
            rtId = Schema.SObjectType.Project_File_Area__c.getRecordTypeInfosByName().get('Photos').getRecordTypeId();
            
            Project_File_Area__c file;
            //system.runAs(mngr) {
                file = new Project_File_Area__c(Name = 'test', Project__c = proj.Id, Main_Project_Image__c = true, RecordTypeId = rtId);
                insert file;
            //}
            
            rtId = Schema.SObjectType.Quotation__c.getRecordTypeInfosByName().get('Supplier Quotation').getRecordTypeId();
            Quotation__c quot = new Quotation__c(Proforma_no__c = '1', Project__c = proj.Id, Furnitures__c = 100, RecordTypeId = rtId,
                    Installation__c = 100, Transportation__c = 100, Total__c = 300, Currency__c = 'USD');
            insert quot;
            
            Handover_Report__c hr = new Handover_Report__c(Installer_Arrival_Date__c = Date.Today(), Installer_Arrival_Time__c = '12:00', Installer_Departure_Date__c = Date.Today(), Installer_Departure_Time__c = '12:00', 
                    Project__c = proj.Id, General_cleaning__c = 'Yes', Supplier_on_time__c = 'Yes', Installation_feedback__c = 'a', 
                    Installation_area_cleared_before__c = 'Yes');
            
            Furniture__c furn = new Furniture__c(Name = 'test furniture', Active__c = true, Completed_items__c = true);
            insert furn;
            
            Furnitures_Inventory__c finv = new Furnitures_Inventory__c(Furniture__c = furn.Id, Project__c = proj.Id, Quantity__c = 1);
            insert finv;
            
            insert hr;
            
            Test.startTest();
            
        ///////////////////////////////////////////////////
        // TEST QuotationPageController
            //System.runAs(mngr) 
            {
                ApexPages.StandardController std = new ApexPages.StandardController(quot);
                QuotationPageController controller = new QuotationPageController(std);
                
                System.assertEquals(true, controller.hasAccessToQuotation);
            }
        
        ///////////////////////////////////////////////////
        // TEST QuotationEditDispatchController
            //System.runAs(mngr) 
            {
                ApexPages.StandardController std = new ApexPages.StandardController(quot);
                QuotationEditDispatchController controller = new QuotationEditDispatchController(std);
                
                PageReference pageRef = controller.getRedir();
                String pageUrl = pageRef.getUrl();
                System.assertNotEquals('/apex/InsufficientPrivileges', pageUrl);
            }
        
        ///////////////////////////////////////////////////
        // TEST HandoverEditDispatchController
            //System.runAs(mngr) 
            {
                ApexPages.StandardController std = new ApexPages.StandardController(hr);
                HandoverEditDispatchController controller = new HandoverEditDispatchController(std);
                
                PageReference pageRef = controller.getRedir();
                String pageUrl = pageRef.getUrl();
                System.assertNotEquals('/apex/InsufficientPrivileges', pageUrl);
            }
            
        ///////////////////////////////////////////////////
        // TEST CornerProjectController
            //System.runAs(mngr) 
            {
                ApexPages.StandardController std = new ApexPages.StandardController(proj);
                CornerProjectController controller = new CornerProjectController(std);
                
                System.assertEquals(true, controller.hasAccessToProject);
                System.assertEquals(true, controller.hasDelRights);
                System.assertEquals(true, controller.hasAccessToQuotations);
                System.assertEquals(false, controller.noQuotations);
            }
            
        ///////////////////////////////////////////////////
        // TEST CornerProjectEditDispatchController
            //System.runAs(mngr) 
            {
                ApexPages.StandardController std = new ApexPages.StandardController(proj);
                CornerProjectEditDispatchController controller = new CornerProjectEditDispatchController(std);
                
                PageReference pageRef = controller.getRedir();
                String pageUrl = pageRef.getUrl();
                System.assertNotEquals('/apex/InsufficientPrivileges', pageUrl);
            }
            
        ///////////////////////////////////////////////////
        // TEST HandoverChecklistExtension 
            //System.runAs(mngr) 
            {
                ApexPages.StandardController std = new ApexPages.StandardController(hr);
                HandoverChecklistExtension controller = new HandoverChecklistExtension(std);
                
                System.assertEquals(true, controller.hasAccessToHandover);
                
                controller.saveHandoverChecklist();
            }
            
            Test.stopTest();
        }
    }
}
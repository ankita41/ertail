/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestUpdateCornerProjects {

    static testMethod void myUnitTest() {
        Test.startTest(); 
       Currency_Settings__c mycs = new Currency_Settings__c(Name = 'Main Currency');
            mycs.Main_Currency__c = 'CHF';
            insert mycs;
             List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
                gmsList.add(new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa'));
                gmsList.add(new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs='));
                insert gmsList;
                
                insert new CustomRedirection__c (Name = 'QuotationObjId', Value__c = '01Ib0000000R72V');
            User usr = [Select id from User where Id = :UserInfo.getUserId()];
            System.runAs(usr){
                /*List<GoogleMapsSettings__c> gmsList = new List<GoogleMapsSettings__c>();
                gmsList.add(new GoogleMapsSettings__c(Name = 'Client ID', Value__c = 'gme-nestlenespressosa'));
                gmsList.add(new GoogleMapsSettings__c(Name = 'Crypto Key', Value__c = 'FePuq8NIxp3L1_VAHPNwoNLKbQs='));
                insert gmsList;
                */
              //  insert new CustomRedirection__c (Name = 'QuotationObjId', Value__c = '01Ib0000000R72V');
                
                Map<String, Id> profileMap = new Map<String, Id>();
                for (Profile p : [SELECT Id, Name FROM Profile])
                    profileMap.put(p.Name, p.Id);
                
                
                List<User> usersList = new List<User>();
                usersList.add(new User(Alias = 'supp', Email='supplier@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Supplier'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='supplier@testorg.com'));
                            
                usersList.add(new User(Alias = 'arch', Email='arch@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Corner Architect'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='arch@testorg.com'));
                            
                UserRole r = [SELECT Id FROM UserRole WHERE Name = 'Germany'];
                usersList.add(new User(Alias = 'mngr', Email='mngr@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Local Manager'), UserRoleId = r.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='mngr@testorg.com'));
    
                //mngr = [SELECT Id, Name FROM User WHERE Username = 'market.nespresso@gmail.com.uat'];
                
                usersList.add(new User(Alias = 'sale', Email='sale@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Market Sales Promoter'), 
                            TimeZoneSidKey='America/Los_Angeles', UserName='sale@testorg.com'));
                
                usersList.add(new User(Alias = 'trad', Email='trad@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileMap.get('NES Trade/Proc Manager'),  
                            TimeZoneSidKey='America/Los_Angeles', UserName='trad@testorg.com'));
                            
                insert usersList;
                
                Id suppId = usersList[0].Id;
                Id archId = usersList[1].Id;
                Id mgrId = usersList[2].Id;
                Id saleId = usersList[3].Id;
                Id tradId = usersList[4].Id;
                User mngr = usersList[2];
                
                // Create Market
                
                Market__c market = new Market__c(Name = 'Germany', Code__c = 'GER', NES_Project_Manager__c = mgrId, 
                        Default_Architect__c = archId, Default_Supplier__c = suppId, Currency__c = 'USD');
                
                
                Market__c market2 = new Market__c(Name = 'Hungary', Code__c = 'HU', NES_Project_Manager__c = mgrId, 
                        Default_Architect__c = archId, Default_Supplier__c = suppId, Currency__c = 'HUF');
                insert new List<Market__c>{market, market2};
                
                            // Create forecasts
                List<Objective__c> objList = new List<Objective__c>();
                objList.add(new Objective__c(Market__c = market.Id
                                            , year__c = String.valueOf(Date.today().addYears(-1).year())
                                            , New_Members_Market_or_Region_Growth__c = 1
                                            , Monthly_Average_Consumption__c = 2
                                            , Caps_Cost_in_LC__c = 3
                                            , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
                objList.add(new Objective__c(Market__c = market.Id
                                , year__c = String.valueOf(Date.today().year())
                                , New_Members_Market_or_Region_Growth__c = 1
                                , Monthly_Average_Consumption__c = 2
                                , Caps_Cost_in_LC__c = 3
                                , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
                objList.add(new Objective__c(Market__c = market.Id
                                , year__c = String.valueOf(Date.today().addYears(1).year())
                                , New_Members_Market_or_Region_Growth__c = 1
                                , Monthly_Average_Consumption__c = 2
                                , Caps_Cost_in_LC__c = 3
                                , Avg_Capsule_sales_price_in_LC_ex_VAT__c = 4));
                insert(objList);
                
                // Create Global Key account (to use as parent for the POS Group)
                Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Key Account').getRecordTypeId();
                Account globalKeyAcc = new Account(Name = 'Test Global Key Acc', RecordTypeId = rtId, Market__c = market.Id, MarketName__c = market.Name);
                insert globalKeyAcc;
                
                // Create POS Group account (to use as parent for the POS Local)
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Group').getRecordTypeId();
                Account posGroupAcc = new Account(Name = 'Test POS Group Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = globalKeyAcc.Id, MarketName__c = market.Name);
                insert posGroupAcc;
                
                // Create POS Local account (to use as parent for the POS)
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POS Local').getRecordTypeId();
                Account posLocalAcc = new Account(Name = 'Test POS Local Acc', RecordTypeId = rtId, Market__c = market.Id, ParentId = posGroupAcc.Id, MarketName__c = market.Name);
                insert posLocalAcc;
                
                // Create POS Account
                rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Point of Sales').getRecordTypeId();
                Account posAcc = new Account(Name = 'Test POS', Nessoft_ID__c = 'testid1234', RecordTypeId = rtId, ParentId = posLocalAcc.Id, 
                        Market__c = market.Id, MarketName__c = market.Name, BillingStreet = 'street', BillingCity = 'city', BillingState = 'state', 
                        BillingPostalCode = 'zip', BillingCountry = 'country');
                insert posAcc;
                
                List<Project__c> projList = new List<Project__c>();
                
                rtId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner').getRecordTypeId();
                Id rtMoWId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Maintenance Outside Warranty').getRecordTypeId();
                Id rtMuWId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Maintenance Under Warranty').getRecordTypeId();
                Id rtAgentId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('New Corner (Agent)').getRecordTypeId();
                
                Project__c proj = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Sales Promoter');
                //proj.Cancellation_Request_Date__c = null; 
                proj.MarketName__c = 'Germany';
                proj.agreed_Installation_Date__c=system.today().addDays(-1);
                insert proj;
                Project__c proj1 = TestHelper.CreateCornerProject(rtId, posAcc.Id, posLocalAcc.Id, 'Sales Promoter');
                //proj.Cancellation_Request_Date__c = null; 
                proj1.MarketName__c = 'Germany';
                proj1.Approval_Status__c='Handover Report';
                proj1.agreed_Installation_Date__c=system.today().addDays(-1);
                insert proj1 ;
                
                List<Linked_POS__c> newLNKList = new List<Linked_POS__c>();
            newLNKList.add(new Linked_POS__c(
                            Key_Account__c = posLocalAcc .Id
                          , Corner_Point_of_Sale__c = posAcc.Id
                           , Forecast__c = objList[2].Id
                          , Corner_Project__c = proj1.Id,agreed_Installation_Date__c=system.today().addDays(-1)
                          , Targeted_Installation_Date__c = date.newinstance(2015, 21, 7)));
           insert  newLNKList ;
        UpdateCornerProjects batch= new UpdateCornerProjects ();
        database.executebatch(batch);
        UpdateMassCornerProjects batch1= new UpdateMassCornerProjects();
        database.executebatch(batch1);
        Test.stopTest();
        }
    }
}
public without sharing class TestingGraph {
    public String reportYear {get;set;}
    
    public List<SelectOption> zoneOpts {get;set;}
    public String zone {get;set;}
    public List<SelectOption> marketOpts {get;set;}
    public String markets {get;set;}
    public List<SelectOption> yearOpts {get;set;}
    public String businessOwner {get;set;}
    
    public String qryStr {get;set;}
    public List<MarketWrapper> marketGraphPrjType {get;set;} //req. 141
    public List<MarketWrapper> marketGraphTypoCapex {get;set;} //req. 141
   
    public TestingGraph (){
        zone = 'All';
        queryZones();
        queryMarkets();
        queryYears();
        businessOwner = 'All';
    }
    
    private void queryYears(){
        Integer currYear = SYSTEM.now().year();
        /*--CHANGE DONE BY CHIRAG INC# RM0018253702 --*/
        //reportYear = String.valueOf(currYear);
        
        yearOpts = new List<SelectOption>();
        yearOpts.add(new SelectOption(String.valueOf(currYear-3), String.valueOf(currYear-3)));//CHANGE DONE BY KK INC# RM0020618571
        yearOpts.add(new SelectOption(String.valueOf(currYear-2), String.valueOf(currYear-2)));//CHANGE DONE BY KK INC# RM0020618571
        yearOpts.add(new SelectOption(String.valueOf(currYear-1), String.valueOf(currYear-1)));
        yearOpts.add(new SelectOption(String.valueOf(currYear), String.valueOf(currYear))); //CHANGE DONE BY CHIRAG INC# RM0018253702
        yearOpts.add(new SelectOption(String.valueOf(currYear+1), String.valueOf(currYear+1)));    }
    
    private void queryZones(){
        /*Set<String> distinctZones = new Set<String>();
        for(Market__c m : [SELECT Zone__c FROM Market__c]){
            distinctZones.add(m.Zone__c);
        }
        
        zoneOpts = new List<SelectOption>();
        for(String s : distinctZones){
            zoneOpts.add(new SelectOption(s, s));
        }
        zoneOpts.sort();*/
        
        zoneOpts = new List<SelectOption>();
        zoneOpts.add(new SelectOption('AOA', 'AOA'));
        zoneOpts.add(new SelectOption('AMS', 'AMS'));
        zoneOpts.add(new SelectOption('CCO', 'CCO'));
        zoneOpts.add(new SelectOption('EUR', 'EUR'));
    }
    
    public void queryMarkets(){
        String qryStr = 'SELECT Name FROM Market__c ';
        marketOpts = new List<SelectOption>();
        
        //if(zone!='All')
            //qryStr = qryStr + 'WHERE Zone__c = \'' + zone + '\' ';
            
        if(zone == 'AOA' || zone == 'AMS')
            qryStr = qryStr + 'WHERE Zone__c = \'' + zone + '\' ';
        else if(zone == 'CCO')
            qryStr = qryStr + 'WHERE Name IN (\'France\',\'Germany\',\'Switzerland\') ';
        else if(zone == 'EUR')
            qryStr = qryStr + 'WHERE Zone__c IN (\'NCE\',\'SWE\') AND Name NOT IN (\'France\',\'Germany\',\'Switzerland\') ';
            
        for(Market__c m : Database.query(qryStr)){
            marketOpts.add(new SelectOption(m.Name, m.Name));
        }
        marketOpts.sort();
        
        markets = 'All';
        marketGraphPrjType = new List<MarketWrapper>(); //Req. 141
        marketGraphTypoCapex = new List<MarketWrapper>(); //Req. 141
        
    }
    
    public void queryMarketData(){
       
        /********************Code for Typology Graph of Req. 141 starts here**********************************************************/
        marketGraphTypoCapex = new List<MarketWrapper>(); //Req. 141
        List<MarketWrapper>marketGraphTypoCapexAll = new List<MarketWrapper>(); //Req. 141
        qryStr = 'SELECT Boutique__r.typology__c typo,SUM(Total_CapEx_Y1_CHF__c) FROM RECO_Project__c ' +
            'WHERE Cutoff_Year_1__c = :reportYear AND Market__c!=NULL  AND RecordType.Name Not IN (\'Boutique Pop-up Relocation Project\')' +  
            
             //Do not show projects without Capex cost Approved
            'AND Boutique_CapEx_request_status__c =\'Approved\' '+    
        
            //Do not show projects without costs
            'AND (NOT(Planned_OP_Y1_CHF__c = NULL AND Total_CapEx_Y1_CHF__c = 0 AND Expected_Project_Cost_Y1_CHF_f__c = 0)) ' +
            
            //DO NOT INCLUDE PROJECTS THAT ARE HIDDEN FROM REPORTS
            'AND Hidden_from_Reports__c = FALSE ';
            qryStr = filterZoneMarket(qryStr, 'Market__c', 'Boutique__r.boutique_location_account__r.Market__r.Zone__c');
            qryStr = filterAgent(qryStr);
            qryStr = qryStr + ' GROUP BY Boutique__r.typology__c';
  
            system.debug('---query---'+qryStr);  
          
            for(AggregateResult ar : Database.query(qryStr)){
                system.debug('==Name=='+ ar.get('typo'));
                system.debug('==Total Capex=='+Decimal.valueOf(Double.valueOf(ar.get('expr0')))) ;
                marketGraphTypoCapexAll.add(new MarketWrapper(string.valueOf(ar.get('typo')),Decimal.valueOf(Double.valueOf(ar.get('expr0')))));
            }          
         qryStr = 'SELECT Boutique__r.typology__c typo,SUM(Total_CapEx_Y2_CHF__c) FROM RECO_Project__c ' +
            'WHERE Cutoff_Year_1__c = :reportYear AND Market__c!=NULL  AND RecordType.Name Not IN (\'Boutique Pop-up Relocation Project\')' +  
            
             //Do not show projects without Capex cost Approved
            ' AND Boutique_CapEx_request_status__c =\'Approved\' '+    
        
            //Do not show projects without costs
           'AND (NOT(Planned_OP_Y2_CHF__c = NULL AND Total_CapEx_Y2_CHF__c = 0 AND Expected_Project_Cost_Y2_CHF_f__c = 0)) ' +
            
            //DO NOT INCLUDE PROJECTS THAT ARE HIDDEN FROM REPORTS
            'AND Hidden_from_Reports__c = FALSE ';
            qryStr = filterZoneMarket(qryStr, 'Market__c', 'Boutique__r.boutique_location_account__r.Market__r.Zone__c');
            qryStr = filterAgent(qryStr);
            qryStr = qryStr + ' GROUP BY Boutique__r.typology__c';
  
            system.debug('---query---'+qryStr); 
            for(AggregateResult ar : Database.query(qryStr)){
                 system.debug('==Name=='+ ar.get('typo'));
                 system.debug('==2Total Capex=='+Decimal.valueOf(Double.valueOf(ar.get('expr0')))) ;
                 marketGraphTypoCapexAll.add(new MarketWrapper(string.valueOf(ar.get('typo')),Decimal.valueOf(Double.valueOf(ar.get('expr0')))));
            }  
           Map<String,MarketWrapper> mapTypoCapex= new Map<String,MarketWrapper>();
            for(MarketWrapper mw : marketGraphTypoCapexAll){
               if(mapTypoCapex.containsKey(mw.prjType)){
                  Decimal totalCapexNew=mapTypoCapex.get(mw.prjType).totalCapex+mw.totalCapex;
                  mapTypoCapex.put(mw.prjType,new MarketWrapper(mw.prjType+'\nCHF '+totalCapexNew.intValue().format(),totalCapexNew));
               }
               else{
                  mapTypoCapex.put(mw.prjType,new MarketWrapper(mw.prjType+'\nCHF '+mw.totalCapex.intValue().format(),mw.totalCapex));
               }
            }
           if(!mapTypoCapex.isEmpty()){
               marketGraphTypoCapex.addAll(mapTypoCapex.values());
              /* marketGraphTypoCapex=new List<MarketWrapper>(); //Req. 141
               marketGraphTypoCapex.add(new MarketWrapper('a',23.14));
               marketGraphTypoCapex.add(new MarketWrapper('b',24.15)); */
           }
           system.debug('===FINAL DATA OF TYPOLOGY==='+marketGraphTypoCapex);   
        /********************Code for Typology Graph of Req. 141 ends here**********************************************************/
       
    }
    
    private String filterZoneMarket(String qryStr, String marketField, String zoneField){
        if(!markets.contains('All')){
            qryStr = qryStr + 'AND ' + marketField + ' IN (';
            
            String[] tempStr = markets.replace('[','').replace(']','').trim().split(',');
            for(String s : tempStr){
                qryStr = qryStr + '\'' + s.replaceAll('\'','\\\\\'').trim() + '\',';
            }
            qryStr = qryStr.removeEnd(',') + ') ';
        }
        
        //else if(zone!='All')
            //qryStr = qryStr + 'AND ' + zoneField + ' = :zone ';
            
        else if(zone == 'AOA' || zone == 'AMS')
            qryStr = qryStr + 'AND ' + zoneField + ' = :zone ';
        else if(zone == 'CCO')
            qryStr = qryStr + 'AND ' + marketField + ' IN (\'France\',\'Germany\',\'Switzerland\') ';
        else if(zone == 'EUR')
            qryStr = qryStr + 'AND ' + zoneField + ' IN (\'NCE\',\'SWE\') AND ' + marketField + ' NOT IN (\'France\',\'Germany\',\'Switzerland\') ';
            
        return qryStr;
    }
    
    private String filterAgent(String qryStr){
        if(businessOwner == 'Nespresso')
            qryStr = qryStr + 'AND Boutique__r.Business_owner__r.Name = \'Nespresso\' '; 
        else if(businessOwner == 'Agents')
            qryStr = qryStr + 'AND Boutique__r.Business_owner__r.Name != \'Nespresso\' '; 
        
        return qryStr;
    }
    
   
    
    public class MarketWrapper{
        public Decimal totalCapEx {get;set;}
        public String prjType{get;set;} // Req. 141
        
        public MarketWrapper(String prjType,Decimal totalCapEx){ // Req 141
        //this.market=market;
        this.prjType=prjType;
        this.totalCapEx=totalCapEx;
        }
        
       
    }
}
global class UpdateCornerProjects implements Database.Batchable<sObject>{
   global String Query;
  

   global Database.QueryLocator start(Database.BatchableContext BC){
       Query ='Select id,p.Supplier__c, p.Status__c,Notify_Passed_Agreed_Installation_Date__c,Notify_Passed_Agreed_Date__c,isBatchUpdate__c From Project__c p '+
               ' where p.status__C!=\'Closed\' and p.status__C!=\'Cancelled\' and isMassCorner__c=false and p.Agreed_Installation_Date__c!= null  '+
               ' and  p.Agreed_Installation_Date__c <=today and p.Handover_Submission_Date2__c = null '+
               ' and p.Approval_Status__c not IN ( \'closed\',\'Close Project\',\'Cancelled\',\'on hold\',\'draft\',\'Waiting for repair validation\',\'Review Handover Report\') '+ 
               ' and p.RecordType.name not in (\'Maintenance Outside Warranty\',\'Corner Cancelled\',\'Corner Closed\') ';
              // ' and p.isBatchUpdate__c =false ';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, 
                       List<Project__c> scope){
      for(Project__c s : scope){
          s.isBatchUpdate__c=true;
          s.Notify_Passed_Agreed_Installation_Date__c=false;
          s.Notify_Passed_Agreed_Date__c= null;
      }      
      update scope;
   }

   global void finish(Database.BatchableContext BC){

   }

}
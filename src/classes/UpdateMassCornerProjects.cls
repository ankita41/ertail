global class UpdateMassCornerProjects implements Database.Batchable<sObject>{
   global String Query;
  

   global Database.QueryLocator start(Database.BatchableContext BC){
       Query ='Select  l.Agreed_Installation_Date__c, l.Corner_Project__r.Status__c,l.Corner_Project__r.isBatchUpdate__c, ' +
                ' l.Actual_Installation_Date__c From Linked_POS__c l  ' +
               ' where l.Agreed_Installation_Date__c!=null and l.Corner_Project__c != null and l.Corner_Project__r.status__C!=\'Closed\' '+
               ' and l.Corner_Project__r.Approval_Status__c in (\'Handover Report\',\'Waiting for Installation\') '+ 
               ' and l.Agreed_Installation_Date__c <= today and l.Corner_Project__r.isBatchUpdate__c =false';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, 
                       List<Linked_POS__c> scope){
       Set<String> btqProjects= new set<String>();
       List<Project__c> projectList= new List<Project__c>();
      for(Linked_POS__c s : scope){
         if(s.Corner_Project__c != null && !btqProjects.contains(s.Corner_Project__c) ){
              btqProjects.add(s.Corner_Project__c);
              projectList.add(new Project__c(id=s.Corner_Project__c,isBatchUpdate__c=true));
          }
          
         // s.isBatchUpdate__c=true;
      }     
      system.debug('btqProjects@@'+btqProjects) ;
      if(projectList != null && projectList.size()>0){
          update projectList;
      }
     // update scope;
   }

   global void finish(Database.BatchableContext BC){

   }

}
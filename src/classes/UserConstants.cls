/*****
*   @Class      :   UserConstants.cls
*   @Description:   Stores User Information as Constants throughout the org.
*   @Author     :   Jacky Uy
*   @Created    :   06 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*   JACKY UY        05 SEP 2014     Added new Profiles
*   Chirag          20 MAY 2015     Added new Profile
*
*****/

public class UserConstants{
    //USER PROFILES
    public static final String SYS_ADM = 'System Admin';
    public static final String BTQ_RPM = 'NES RECO Retail Project Manager';
    public static final String BTQ_NBM = 'NES RECO National Boutique Manager';
    public static final String BTQ_IRO = 'NES RECO International Retail Operations';
    public static final String BTQ_MA = 'NES RECO Manager Architect';
    public static final String BTQ_DA = 'NES RECO Design Architect';
    public static final String BTQ_MD = 'NES RECO Market Director';
    public static final String BTQ_PO = 'NES RECO PO Operator';
    public static final String BTQ_ViewAll = 'NES RECO View All User';
    public static final String BTQ_FSupp = 'NES RECO Furniture Supplier';
    public static final String BTQ_PropMgr = 'NES RECO Property Manager';
    public static final String BTQ_Mktng = 'NES RECO Marketing';
    public static final String BTQ_Proc = 'NES RECO Procurement';
    public static final String BTQ_Fin = 'NES RECO Finance';
    public static final String BTQ_Ceo = 'RECO CEO and Reports'; // Added by Chirag Ref INC : RM0018468205
    /** Below code is added for NCAFE **/
    public static final String NCAFE_RPM = 'NES NCAFE Retail Project Manager';
    public static final String NCAFE_NBM = 'NES NCAFE National Boutique Manager';
    public static final String NCAFE_IRO = 'NES NCAFE International Retail Operations';
    public static final String NCAFE_MA = 'NES NCAFE Manager Architect';
    public static final String NCAFE_DA = 'NES NCAFE Design Architect';
    public static final String NCAFE_MD = 'NES NCAFE Market Director';
    public static final String NCAFE_PO = 'NES NCAFE PO Operator';
    public static final String NCAFE_ViewAll = 'NES NCAFE View All User';
    public static final String NCAFE_FSupp = 'NES NCAFE Furniture Supplier';
    public static final String NCAFE_PropMgr = 'NES NCAFE Property Manager';
    public static final String NCAFE_Mktng = 'NES NCAFE Marketing';
    public static final String NCAFE_Proc = 'NES NCAFE Procurement';
    public static final String NCAFE_Fin = 'NES NCAFE Finance';
    public static final String NCAFE_Ceo = 'RECO NCAFE and Reports';
    public static boolean firstRun = true;
    //GET CURRENT USER INFO
    public static User CURRENT_USER{
        get{ 
            if(CURRENT_USER == null)
                CURRENT_USER = [SELECT Id,Name,Profile.Name, UserRole.Name,DoNotDisplayMessageAgain__c FROM User WHERE Id = :UserInfo.getUserId()];
            
            return CURRENT_USER;
        }
        set{ CURRENT_USER = value; }
    }
}
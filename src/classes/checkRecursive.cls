/*
*   @Class      :   checkRecursive.cls
*   @Description:   
*   @Author     :   Himanshu Palerwal
*   @Created    :   17 Dec 2015
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date        Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*/

public Class checkRecursive{
    public  static boolean run = true;
    public static boolean runOnce(){ 
    if(run){
     run=false;
     return true;
    }else{
        return run;
    }
    }
}
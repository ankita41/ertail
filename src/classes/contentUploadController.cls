/*
*   @Class      :   contentUploadController.cls
*   @Description:   Controller class of the contentUpload VF page
*   @Author     :   XXXXX
*   @Created    :   XXX
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer   Date        Description
*   ----------------------------------------------------------------------------------------------------------------------------    
*   Jacky Uy    17 APR 2014 Fix: max view state limit error
*   Jacky Uy    02 MAY 2014 Fix: Unable to upload files because the content origin was set to Chatter
*   Jacky Uy    02 JUN 2014 Fix: Added File Size limitation
*                         
*/

public with sharing class contentUploadController {
    public List<contentFileWrapper> files {get; set;}
    public static final Integer NUM_FILES_TO_ADD = 5;
    
    public Id folderId {
        get {return ApexPages.currentPage().getParameters().get('folderId');} set;
    }
    public Id projectId {
        get {return ApexPages.currentPage().getParameters().get('projectId');} set;
    }
    public String projectName {
        get {
            RECO_Project__c tmpProj = [SELECT Name FROM RECO_Project__c WHERE Id = :projectId];
            return tmpProj.Name;
        } set;
    }
    public Id offerId {
        get {return ApexPages.currentPage().getParameters().get('offerId');}  set;
    }
    public String offerName {
        get {
            Offer__c tmpOffer = [SELECT Name FROM Offer__c WHERE Id = :offerId];
            return tmpOffer.Name;
        } set;
    }
    public Id handoverId {
        get{return ApexPages.currentPage().getParameters().get('handoverId');}  set;
    }
    public String handoverName {
        get {
            Handover__c tmpHandover = [SELECT Name FROM Handover__c WHERE Id = :handoverId];
            return tmpHandover.Name;
        } set;
    }
    public String breadCrumbs {
        get {return ApexPages.currentPage().getParameters().get('bc');} set;
    }
    public Id libId {
        get {return ApexPages.currentPage().getParameters().get('libId');} set;
    }
    public Boolean uploadDone {
        get {return ApexPages.currentPage().getParameters().get('uploadDone') == '1';} set;
    }
    
    public contentUploadController() {
        initiateFileList();
    }
    
    private void initiateFileList(){
        files = new List<contentFileWrapper>();
        for (Integer i = 0; i < NUM_FILES_TO_ADD; i++){
            files.add(new contentFileWrapper());
        }
    }
    
    public PageReference go() {
        for(contentFileWrapper file : files) {
            if(file.fileBody!=null && file.fileSize <= 5242880){
                //try {
                    ContentVersion v = new ContentVersion();
                    v.versionData = file.fileBody;
                    v.PathOnClient = file.fileName;
                    v.Description = file.description;
                    insert v;
                    
                    file.fileBody = null;
                    v = [SELECT Id, ContentDocumentId, Content_Folder__c, Project__c FROM ContentVersion WHERE Id = :v.Id];
                    
                    ContentDocument doc = [SELECT Id, ParentId FROM ContentDocument WHERE Id = :v.ContentDocumentId];
                    ContentWorkspaceDoc cwDoc = new ContentWorkspaceDoc(ContentDocumentId = doc.Id, ContentWorkspaceId = libId);
                    insert cwDoc;
                    
                    v.Content_Folder__c = folderId;
                    if (projectId != null) {
                        v.Project__c = projectId;
                    } else if (offerId != null) {
                        v.Offer__c = offerId;
                        v.Project__c = [SELECT RECO_Project__c FROM Offer__c WHERE Id = :offerId ].RECO_Project__c;
                    } else if (handoverId != null) {
                        v.Handover__c = handoverId;
                        v.Project__c = [SELECT RECO_Project__c FROM Handover__c WHERE Id = :handoverId ].RECO_Project__c;
                    }
                    //update v;
                    v = new ContentVersion();
                //} 
                /*catch (Exception e) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
                    file.fileBody = null;
                    files.clear();
                    initiateFileList();
                    return null;
                }*/
            }
            else if(file.fileBody!=null && file.fileSize > 5242880){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'For Boutique related files, a maximum size of 5MB each is allowed. One of your file is ' + Decimal.valueOf(file.fileSize/1048576).setScale(1) + 'MB in size.'));
                file.fileBody = null;
                files.clear();
                initiateFileList();
                return null;
            }
        }
        initiateFileList();
        return new PageReference('/apex/contentUpload?uploadDone=1&refresh=true');
    }
    
    public class contentFileWrapper {
        public blob fileBody {get;set;}
        public String fileName {get;set;}
        public String description {get;set;}
        public Integer fileSize {get;set;}
        public contentFileWrapper(){}
    }
}
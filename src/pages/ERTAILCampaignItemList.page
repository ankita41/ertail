<apex:page standardController="Campaign__c" extensions="ERTAILCampaignItemListExt" sidebar="false" >
    <c:ERTAILStyle />
    <style>
        .newCampaignItemWithCreativity .requiredInput{
            display:inline!important;
        }
        .newCampaignItemWithCreativity label{
            margin-right:10px;
        }
        .newCampaignItemWithoutCreativity .requiredInput{
            display:inline!important;
        }
        .newCampaignItemWithoutCreativity label{
            margin-right:10px;
        }
        td.campaignItemNotUsed {
            color:red!important;
            font-weight:bold;
        }
        .campaignItemNotUsedImg{
            visibility:hidden;
            display: inline-block;
            float: right;
            margin-right: 10px;
        }
        .campaignItemNotUsed .campaignItemNotUsedImg{
            visibility:visible;
        }

        .exclude{
            position:relative;
            width:21px;
            height:20px;
            overflow:hidden;
        }
        
        .exclude img{
            left:-1px;
            position:absolute;
        }
        .exclude:hover img{
            left:-22px;
        }
        
        .delete{
            position:relative;
            width:21px;
            height:20px;
            overflow:hidden;
        }
        
        .delete img{
            position:absolute;
        }
        .delete:hover img{
            left:-22px;
        }
            
        .list {
            color: #ffffff;
        }   
        
        .list td{
            vertical-align:middle!important;
            position: relative;
            min-height: 1px;
            padding-right: 15px;
            padding-left: 15px;
        }
        
        .list tr:nth-child(even){
            background-color : #f9f9f9;
        }
        
        .list tr:nth-child(odd){
            background-color : #f2f2f2;
        }
        
        .list th{
            vertical-align:middle!important;
            background-color : #402806;
            color : #ffffff!important;
            border-top: 0 none;
            vertical-align: middle;
            border-bottom: 2px solid #ddd;
        }
        
        .hide{
            display: none; 
        }
    </style>
    
<div id="pages">    
    <apex:sectionHeader title="Campaign Items" subtitle="{!Campaign__c.Name}">
        <div class="ptBreadcrumb">&nbsp;&#171;&nbsp;<a href="/{!Campaign__c.Id}">Back to Campaign: {!Campaign__c.Name}</a></div>
    </apex:sectionHeader>
    <apex:form >
        <apex:pageBlock title="Campaign Detail">
            <apex:pageBlockButtons >
                <!-- <apex:commandButton action="{!manageOrders}" value="Manage Orders" immediate="true"/> -->
                <apex:actionStatus id="buttonsStatus">
                    <apex:facet name="start"><apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/></apex:facet>
                </apex:actionStatus>
            </apex:pageBlockButtons>
            <apex:pageBlockSection >
                <apex:outputField value="{!Campaign__c.Name}" />
                <apex:outputField value="{!Campaign__c.Scope__c}" />
                <apex:outputField value="{!Campaign__c.Launch_Date__c}" />
                <apex:outputField value="{!Campaign__c.End_Date__c}" />
                <apex:outputField value="{!Campaign__c.Description__c}" />
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
    <apex:pageBlock title="Campaign Items"> 
        <apex:pageBlockSection title="Show Windows" columns="1" id="CampaignItemWithCreativityResults">
            <apex:outputPanel styleClass="newCampaignItemWithCreativity" layout="block">
                <apex:form >
                   <apex:outputText value="You cannot select this Campaign Item as Scope Limitation is In-Store Display only" id="showmsg" style="font-weight:bold; color:red;" rendered="{!Showerrorinstore}"/><br/>
                    <apex:outputLabel value="New Campaign Item (Show Window)" />
                    <apex:inputField value="{!newCampaignItemWithCreativity.Space__c}" required="true"/>
                    &nbsp;
                    <apex:outputLabel value="number" />
                    <apex:selectList value="{!itemsToCreate}" size="1">
                        <apex:selectOption itemValue="1" itemLabel="1"/>
                        <apex:selectOption itemValue="2" itemLabel="2"/>
                        <apex:selectOption itemValue="3" itemLabel="3"/>
                        <apex:selectOption itemValue="4" itemLabel="4"/>
                    </apex:selectList>
                    &nbsp;
                    <apex:commandButton value="Create" action="{!saveNewCampaignItemWithCreativity}" rerender="newCampaignItemWithCreativity, CampaignItemWithCreativityResults" status="saveNewCampaignItemWithCreativityStatus"/>
                    <apex:actionStatus id="saveNewCampaignItemWithCreativityStatus">
                            <apex:facet name="start"><apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/></apex:facet>
                    </apex:actionStatus>
                </apex:form>    
            </apex:outputPanel>

            <apex:form >
                <apex:outputPanel >
                    <apex:actionRegion >
                    
                        <apex:actionFunction name="rerenderCampaignItemWithCreativityResults" action="{!refreshLists}" immediate="true" rerender="CampaignItemWithCreativityResults" status="deleteShowWindowCampaignItemStatus"/>

                        <apex:actionFunction name="deleteShowWindowCampaignItem" action="{!DeleteCampaignItem}" immediate="true" rerender="CampaignItemWithCreativityResults" status="deleteShowWindowCampaignItemStatus">
                            <apex:param name="campaignItemId" value=""/>
                        </apex:actionFunction>
                    </apex:actionRegion>
    
                    <apex:actionStatus id="deleteShowWindowCampaignItemStatus">
                        <apex:facet name="start">
                            <apex:outputPanel >
                                <apex:outputText value="Deleting Show Window Campaign Item "/>
                                <apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/>
                            </apex:outputPanel>
                        </apex:facet>
                    </apex:actionStatus>
                </apex:outputPanel>
                <script>
                    $(function() {
                        $(".showWindowCampaignItemDelete").on( "click", function(event) {
                            deleteShowWindowCampaignItem($(this).attr('campaignItemId'));
                        });
                        
                        $(".ShowWindowCampaignItemExcludedMarket").on( "click", function(event) {
                            var width = Math.min(1200,window.screen.availWidth);
                            var height = Math.min(800,window.screen.availHeight);
                            var left = (window.screen.availWidth - width)/2;
                            var top = (window.screen.availHeight - height)/2

                            var child = window.open('/apex/ERTAILCampaignItemExclusion?Id='+$(this).attr('campaignItemId'),'','width='+ width +',height=' + height + ',left=' + left + ',top=' + top,!1);
                        
                            var timer = setInterval(checkChild, 500);
    
                            function checkChild() {
                                if (child.closed) {
                                    clearInterval(timer);
                                    rerenderCampaignItemWithCreativityResults();
                                }
                            }   
                        });
                        
                        
                    });
                </script>

                <apex:dataTable value="{!campaignItemsWithCreativityResults}" var="campaignItem" id="theTable1" styleClass="list" style="vertical-align:center;">
                    <apex:column >
                        <apex:facet name="header">Alias</apex:facet>
                        <apex:outputField value="{!campaignItem.Alias__c}"/>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Name</apex:facet>
                        <apex:outputText value="{!campaignItem.Name}"/>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Space</apex:facet>
                        <apex:outputField value="{!campaignItem.Space__c}"/>
                    </apex:column>
                    <apex:column style="text-align:right;">
                        <apex:facet name="header">Creativity</apex:facet>
                        <apex:outputText value="{!campaignItem.Creativity__c}" style="margin-right:10px;"/>
                    </apex:column>
                    <apex:column styleClass="{!IF(quantitiesToOrder[campaignItem]==0, 'campaignItemNotUsed', '')}" style="text-align:right;">
                        <apex:facet name="header">Nb time used</apex:facet>
                        <apex:outputText value="{!quantitiesToOrder[campaignItem]}" style="margin-right:10px;"/>
                        <img src="/img/func_icons/util/alert16.gif" class="campaignItemNotUsedImg" title="Campaign Item is not used"/>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Excluded Markets</apex:facet>
                        <div style="text-align:right;">
                            <apex:outputPanel styleclass="ShowWindowCampaignItemExcludedMarket" html-campaignItemId="{!campaignItem.Id}" title="Exclude Markets">
                                <div style="vertical-align:top;display:inline-block;padding-top:4px;cursor:pointer;" title="{!IF(campaignItem.Campaign_Item_Exclusions__r.size > 0, excludedMarketsPerCampaignId[campaignItem.Id], '')}">{!campaignItem.Campaign_Item_Exclusions__r.size}</div>
                                <div class="exclude" style="display:inline-block;" >
                                    <img src="/img/func_icons/util/customize20.gif" />
                                </div>
                            </apex:outputPanel>
<!--                        <div style="display:inline-block;" >4</div> -->
                        </div>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Origin</apex:facet>
                        <apex:outputText value="{!campaignItem.Origine__c}"/>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Delete</apex:facet>
                        <apex:outputPanel styleclass="showWindowCampaignItemDelete" html-campaignItemId="{!campaignItem.Id}" title="Delete Campaign Item">
                            <div class="delete">
                                <img src="/img/func_icons/util/trash20.gif" />
                            </div>
                        </apex:outputPanel>
                    </apex:column>
                </apex:dataTable>
            </apex:form>
        </apex:pageBlockSection>
        
        <apex:pageBlockSection title="In Store" columns="1" id="CampaignItemWithoutCreativityResults">
    
            <apex:outputPanel styleClass="newCampaignItemWithoutCreativity">
                <apex:form >
                    <apex:outputText value="You cannot select this Campaign Item as Scope Limitation is In-Store Display only" id="showmsg" style="font-weight:bold; color:red;" rendered="{!Showerror}"/><br/>
                    <apex:outputLabel value="New Campaign Item (In-Store)" />
                    <apex:inputField value="{!newCampaignItemWithoutCreativity.Space__c}" required="true"/>
                    &nbsp;
                    <apex:outputLabel value="Name" />
                    <apex:inputText value="{!campaignItemName}"/>
                    &nbsp;
                    <apex:commandButton value="Create" action="{!saveNewCampaignItemWithoutCreativity}" rerender="newCampaignItemWithoutCreativity, CampaignItemWithoutCreativityResults" status="saveNewCampaignItemWithoutCreativityStatus"/>
                    <apex:actionStatus id="saveNewCampaignItemWithoutCreativityStatus">
                        <apex:facet name="start"><apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/></apex:facet>
                    </apex:actionStatus>
                
                
                </apex:form>    
            </apex:outputPanel>
            
            <apex:form >
                <apex:outputPanel >
                    <apex:actionRegion >
                        <apex:actionFunction name="updateInStoreCampaignItemName" action="{!updateInStoreCampaignItemName}" rerender="CampaignItemWithoutCreativityResults" status="updateInStoreCampaignItemNameStatus">
                            <apex:param name="campaignItemId" value=""/>
                            <apex:param name="campaignItemName" value=""/>
                        </apex:actionFunction>
    
                        <apex:actionFunction name="deleteInStoreCampaignItem" action="{!DeleteCampaignItem}" immediate="true" rerender="CampaignItemWithoutCreativityResults" status="deleteInStoreCampaignItemStatus">
                            <apex:param name="campaignItemId" value=""/>
                        </apex:actionFunction>
                    </apex:actionRegion>
    
                    <apex:actionStatus id="updateInStoreCampaignItemNameStatus">
                        <apex:facet name="start">
                            <apex:outputPanel >
                                <apex:outputText value="Updating In Store Campaign Item Name "/>
                                <apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/>
                            </apex:outputPanel>
                        </apex:facet>
                    </apex:actionStatus>

                    <apex:actionStatus id="deleteInStoreCampaignItemStatus">
                        <apex:facet name="start">
                            <apex:outputPanel >
                                <apex:outputText value="Deleting In Store Campaign Item "/>
                                <apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/>
                            </apex:outputPanel>
                        </apex:facet>
                    </apex:actionStatus>
                </apex:outputPanel>

                <script>
                    $(function() {
                            
                        $(".inStoreCampaignItemName").on( "click", function(event) {
                            $(" .inStoreCampaignItemInputField", this).show();
                            $(" .inStoreCampaignItemOutputPanel", this).hide();
                        });
                        
                        $(".inStoreCampaignItemInputField").on( "change", function(event) {
                            updateInStoreCampaignItemName($(this).attr('campaignItemId'), $(this).val());
                        });

                        $(".inStoreCampaignItemDelete").on( "click", function(event) {
                            deleteInStoreCampaignItem($(this).attr('campaignItemId'));
                        });
                        
                    });
                </script>
                <apex:repeat value="{!inStoreWrappers}" var="inStoreWrapper">
                    <br/>
                    <h2>{!inStoreWrapper.cat}</h2>
                    <apex:dataTable value="{!inStoreWrapper.campaignItems}" var="campaignItem" id="theTable2" rowClasses="odd,even" styleClass="list" style="vertical-align:center;">
                        <apex:column width="15%">
                            <apex:facet name="header">Alias</apex:facet>
                            <apex:outputField value="{!campaignItem.Alias__c}"/>
                        </apex:column>
                        <apex:column width="15%" styleClass="editableBoutique inStoreCampaignItemName">
                            <apex:facet name="header">Name</apex:facet>
                            <apex:outputPanel >
                                <apex:outputPanel styleclass="inStoreCampaignItemOutputPanel">
                                    <apex:outputField value="{!campaignItem.Name}"/>
                                </apex:outputPanel>
                                <apex:inputField styleclass="inStoreCampaignItemInputField hide" html-campaignItemId="{!campaignItem.Id}" value="{!campaignItem.Name}" />
                                <div class="creativityIcon"/>
                            </apex:outputPanel>
                        </apex:column>              
                        <apex:column width="45%">
                            <apex:facet name="header">Space</apex:facet>
                            <apex:outputField value="{!campaignItem.Space__c}"/>
                        </apex:column>
                        <apex:column width="10%" styleClass="{!IF(quantitiesToOrder[campaignItem]==0, 'campaignItemNotUsed', '')}" style="text-align:right;">
                            <apex:facet name="header">Nb time used</apex:facet>
                            <apex:outputText value="{!quantitiesToOrder[campaignItem]}" style="margin-right:10px;"/>
                            <img src="/img/func_icons/util/alert16.gif" class="campaignItemNotUsedImg" title="Campaign Item is not used"/>
                        </apex:column>
                        <apex:column width="10%">
                            <apex:facet name="header">Origin</apex:facet>
                            <apex:outputText value="{!campaignItem.Origine__c}"/>
                        </apex:column>
                        <apex:column width="5%">
                            <apex:facet name="header">Delete</apex:facet> 
                            <apex:outputPanel styleclass="inStoreCampaignItemDelete" html-campaignItemId="{!campaignItem.Id}" title="Delete Campaign Item">
                                <div class="delete">
                                    <img src="/img/func_icons/util/trash20.gif" />
                                </div>
                            </apex:outputPanel>
                        </apex:column>
                    </apex:dataTable>
                </apex:repeat>
            </apex:form>
        </apex:pageBlockSection>
    </apex:pageBlock>
    </div>
</apex:page>
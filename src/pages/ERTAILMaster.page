<apex:page standardController="Campaign__c" extensions="ERTAILMasterController" sidebar="false" showHeader="{!showHeader}">
<c:ERTAILStyle_Creativity />
<c:ERTAILGraphsStyle />
<style>
    .flipHeader{
        margin:2px 0px;
        cursor:s-resize;
        height:4px;
    }
    .flipMenu{
        cursor:url("/img/cursors/col-collapse.cur"), pointer;
        width:1px;
    }
    .flipMenu.collapsed{
        cursor:url("/img/cursors/col-expand.cur"), pointer;
    }
    .flipMenu:hover, .flipHeader:hover{
        background-color:lightgray;
    }
    .noSidebarCell, .sidebarCell .fixed{
        padding-right:0px;
        padding-top:0px;
    }
    .bPageTitle .metadata{
        margin-top:0;
    }
    .pbTitle{
        cursor:pointer;
    }
    .selectRadioMenu{
        margin: 0 0 30px;
        padding: 0 0 0 25px;
    }
    .selectRadioMenu label:before {
        content: '\f105';
        font-family: 'FontAwesome';
        color: #b0b0b0;
        margin-right: 10px;
        font-size: 18px;
        display: inline-block;
        vertical-align: middle;
    }
    .selectRadioMenu label {
        font-size: 13px;
        font-weight: bold;
        padding: 2px 0;
        color: #444;        
        margin-left: 1.5em;
        margin-bottom:0;
        cursor:pointer;
    }
    .selectRadioMenu input[type="radio"]:checked + label {
        color: #d16d18;
    }
    .selectRadioMenu input[type="radio"]:checked + label:before {
        padding-left: 10px;
        color:#616161;
    }
    .selectRadioMenu label:hover{
        color: #d16d18;
    }
    .container{
        width:100%!important;
    }
    .col-content{
        width:auto !important;
    }
    .chatterexpando{
        overflow-y:auto!important;
        max-width: 1100px!important;
        max-height:300px;
    }
    .chatterexpando .feedpage {
        width:100%!important;
    }
    .button.disabled {
        opacity: 0.65; 
        cursor: not-allowed;
    }
    body{
        font-size:1.1em!important;
    }
</style>
<script>
$(document).ready(function() {
    $(".flipMenu").click(function() {
        $(".sidebarCell").toggle();
        $(".flipMenu").toggleClass("collapsed");
    });

    $(".flipHeader").click(function() {
        $(".bPageHeader").toggle();
    });
    $(".phHeader").hide();
});
</script>

<apex:outputPanel layout="block" style="position: absolute; right: 10px; top: 12px;" rendered="{!NOT(showHeader)}">
     <a href="?Id={!Campaign__c}&showHeader=true" target="_top">
     <img width="120px" src="https://c.cs20.content.force.com/servlet/servlet.ImageServer?id=015m0000000DNfK&oid=00Dm00000001bqM&lastMod=1415025133000"/>
 </a>
</apex:outputPanel>
<div class="flipHeader">
    &nbsp;
</div>
<div class="container detail">
    <div class="row">
        <table><tr><td style="vertical-align:top;" rowspan="2">
            <div class="col-menu col-xs-3 col-xs-height">
               <a class="back" href="/{!IF(OR(ISAgent,AND(ISMPM,Items.size=1)), CampaignMarket.Id, Campaign__c.Id)}" title="{!Campaign__c.Name}"><i class="fa fa-angle-left"></i>{!Campaign__c.Name}</a> <apex:form >
                    <h4 class="col-menu-title">
                        Screens
                        <span style="float:right;">
                            <apex:actionStatus id="screenStatus">
                                <apex:facet name="start"><apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/></apex:facet>
                            </apex:actionStatus>
                        </span>
                    </h4>                   
                    <apex:selectRadio value="{!selectedScreen}" layout="pageDirection" styleClass="selectRadioMenu">
                        <apex:actionSupport action="{!refresh}" event="onclick" status="screenStatus"/>
                        <apex:selectOptions value="{!screenOptions}"/>
                    </apex:selectRadio>
                    <apex:outputPanel id="viewsMenu">
                        <apex:outputPanel rendered="{!AND(OR(selectedScreen == 'Creativity', selectedScreen == 'Quantity'), viewOptions.size > 0)}">
                            <h4 class="col-menu-title">
                                Views
                                <span style="float:right;">
                                    <apex:actionStatus id="viewStatus">
                                        <apex:facet name="start"><apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/></apex:facet>
                                    </apex:actionStatus>
                                </span>
                            </h4>
                            <apex:selectRadio value="{!selectedView}" layout="pageDirection" styleClass="selectRadioMenu">
                                <apex:actionSupport action="{!refresh}" event="onclick" status="viewStatus"/>
                                <apex:selectOptions value="{!viewOptions}"/>
                            </apex:selectRadio>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <apex:outputPanel id="marketsMenu">
                        <apex:outputPanel rendered="{!AND(OR(isHQPMorAdmin,isProductionAgency, isCreativeAgency,isMPM), OR(selectedScreen == 'Creativity', selectedScreen == 'Quantity', selectedScreen == 'Installation', selectedScreen == 'Delivery', selectedScreen == 'Fixture'))}">
                            <h4 class="col-menu-title">
                                Markets
                                <span style="float:right;">
                                    <apex:actionStatus id="marketStatus">
                                        <apex:facet name="start"><apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/></apex:facet>
                                    </apex:actionStatus>
                                </span>
                            </h4>
                            <apex:selectRadio value="{!selectedMarketId}" layout="pageDirection" styleClass="selectRadioMenu">
                                <apex:actionSupport action="{!refresh}" event="onclick" oncomplete="document.body.scrollTop = document.documentElement.scrollTop = 0;" status="marketStatus"/>
                                <apex:selectOptions value="{!items}" />
                            </apex:selectRadio>
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:form>
            </div>
        </td>
        <td style="vertical-align:top;">
            <apex:outputPanel layout="block" style="padding:0 30px;" id="chatterBlock">
                <chatter:feedWithFollowers entityId="{!feedId}" />
            </apex:outputPanel>
        </td>
        </tr>
        <tr>
        <td style="vertical-align:top;" id="mainPage">
            <apex:outputPanel layout="none" id="mainPage">
                <c:ERTAILQuantity con="{!Controller}" rendered="{!AND(selectedScreen=='Quantity', OR(isAdmin, isHQPM, isMPM, isAgent, isCreativeAgency, isProductionAgency))}"/>
                <c:ERTAILCreativity con="{!Controller}" rendered="{!AND(selectedScreen=='Creativity', OR(isAdmin, isHQPM, isMPM, isAgent, isCreativeAgency, isProductionAgency))}"/>
                <c:ERTAILFixtureFurnitures con="{!Controller}" rendered="{!AND(selectedScreen=='Fixture', OR(isAdmin, isHQPM, isMPM, isAgent))}"/>
                <c:ERTAILVisuals campId="{!Campaign__c.Id}" rendered="{!AND(selectedScreen=='Visuals', OR(isAdmin, isHQPM, isCreativeAgency))}"/>
                <c:ERTAILCosts campId="{!Campaign__c.Id}" rendered="{!AND(selectedScreen=='Costs', OR(isAdmin, isHQPM, isProductionAgency, isMPM, isAgent))}"/>
                <c:ERTAILDeliveryInstallationFee campId="{!Campaign__c.Id}" feesType="df" market="{!selectedMarketId}" rendered="{!AND(selectedScreen=='Delivery', OR(isAdmin, isHQPM, isMPM, isAgent, isProductionAgency))}"/>
                <c:ERTAILDeliveryInstallationFee campId="{!Campaign__c.Id}" feesType="if" market="{!selectedMarketId}" rendered="{!AND(selectedScreen=='Installation', OR(isAdmin, isHQPM, isMPM, isAgent, isProductionAgency))}"/>
                <c:ERTAILCampaignAgencyFee campId="{!Campaign__c.Id}" rendered="{!AND(selectedScreen=='Agency Fees', OR(isAdmin, isHQPM, isProductionAgency))}"/>
            </apex:outputPanel>
        </td></tr></table>
    </div>
</div>
<apex:form rendered="{!1==2}">
    <!-- Used to save us from requesting the Campaign fields -->
    <apex:inputHidden value="{!Campaign__c.Scope__c}" />
    <apex:inputHidden value="{!Campaign__c.Launch_Date__c}" />
    <apex:inputHidden value="{!Campaign__c.End_Date__c}" />
    <apex:inputHidden value="{!Campaign__c.Description__c}" />
    <apex:inputHidden value="{!Campaign__c.Creativity_Step__c}" />
    <apex:inputHidden value="{!Campaign__c.Quantity_Step__c}" />
</apex:form>
</apex:page>
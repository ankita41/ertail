<apex:page StandardController="RTCOMBoutique__c" extensions="ERTAILWarehouseDetailsExt" sidebar="false" tabStyle="Warehouse_List__tab">
    <c:ERTAILStyle />
    <c:ERTAILGraphsStyle />    
    <style>
    .col-menu-list {
        height: 100%;
        overflow: auto;
        padding:0;
    }
    .col-menu-list li a{
        overflow:hidden;
    }
    .col-menu-list a:hover, .col-menu-list a:focus{
        color: #d16d18;
        text-decoration: none;  
    }
    .item .col-md-3.active img{
        border: 3px solid #d16d18;
    }
    th{
        white-space: normal;
    }
    .container .panel-title a:hover, .container .panel-title a:focus {
        color: #d16d18;
        text-decoration: none;
    }
    .state.grey {
        background-color: #a3a3a3;
    }
    .state.green {
        background-color: #64a115;
    }
    .state.red {
        background-color: #cb3615;
    }
    .state.orange {
        background-color: orange;
    }
    .state {
        height: 15px;
        width: 15px;
    }
    #btqImageLink:hover{
        text-decoration:none;
    }
    .col-xs-1{
        width:auto;
    }
    </style>
    <apex:form >
    <div class="container detail" style="width:1170px!important;">
      <div class="row">
        <div class="col-menu col-xs-3 col-xs-height">
          <a class="back" href="/apex/ERTAILWarehousePerMarket?Id={!RTCOMBoutique__c.RTCOMMarket__c}"><i class="fa fa-angle-left"></i>Market: {!RTCOMBoutique__c.RTCOMMarket__r.Name}</a>
          <h4 class="col-menu-title">Warehouses</h4>
          <ul class="col-menu-list">
            <apex:repeat value="{!BoutiquesInSameMarket}" var="btq">
                <li class="{!IF(btq.Id == RTCOMBoutique__c.Id, 'active', '')}"><a href="/apex/ERTAILWarehouseDetails?Id={!btq.Id}"><apex:outputText value="{!btq.Name}"/></a></li>
            </apex:repeat>
          </ul>
        </div>
        <div class="col-content col-xs-9 col-xs-height">
          <h3>{!RTCOMBoutique__c.Name}</h3>
          <span class="subtitle">Warehouse</span>
          <div class="panel-group" id="accordion">
            <div class="panel panel-default panel-without-bg">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" href="#collapseOne">Details<i class="indicator glyphicon glyphicon-minus  pull-right"></i></a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                  <div class="col-md-7 col-xs-12" id="btqImageBlock" style="width:58.3333%;">
                    <div class="main-media">
                        <a target="_blank" href="#" id="btqImage">
                            <apex:outputPanel rendered="{!RTCOMBoutique__c.DefaultImageFormula_ID__c != ''}">
                                <img border="0" alt="Thumbnail Not Yet Available" src="{!'/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId='+RTCOMBoutique__c.DefaultImageFormula_ID__c}" />
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!RTCOMBoutique__c.DefaultImageFormula_ID__c == ''}">
                                <img style="height:300px;width:400px;" border="0" alt="Thumbnail Not Yet Available" src="{!URLFOR($Resource.Missing_Btq)}" />                       
                            </apex:outputPanel>
<!--                            <img class="img-cover" border="0" alt="Thumbnail Not Yet Available" src="{!IF(RTCOMBoutique__c.DefaultImageFormula_ID__c != '' ,'/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId='+RTCOMBoutique__c.DefaultImageFormula_ID__c, URLFOR($Resource.Missing_Btq))}" /> -->
<!--                             <img border="0" alt="Thumbnail Not Yet Available" src="/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId={!RTCOMBoutique__c.DefaultImageFormula_ID__c}" /> -->
                        </a>
                      <a id="btqImageLink" href="/{!RTCOMBoutique__c.DefaultImageFormula_ID__c}" target="_blank">
                        <span class="state {!CASE(RTCOMBoutique__c.StatusFormula__c, 'Opened', 'green', 'Planned', 'gray', 'To be closed', 'orange', 'Closed', 'red', 'black')}" title="{!RTCOMBoutique__c.RECOStatus__c}"></span>
                        &nbsp;<span class="glyphicon glyphicon-search"></span>&nbsp;
                     </a>
                    </div>
                    <div class="carousel slide media-carousel" id="media">
                      <div class="carousel-inner">
                        <apex:repeat value="{!imagesRows}" var="imagesRow">
                            <div class="item{!IF(imagesRow.rowNumber == 1,' active','')}">
                                <div class="row">
                                    <apex:repeat value="{!imagesRow.feedItemList}" var="image">
                                        <div class="col-xs-3">
                                            <apex:outputLink value="javascript:changeBoutiqueImage('{!image.RelatedRecordId}');" styleClass="thumbnail">
                                                <apex:image styleClass="img-cover" title="{!image.ContentFileName}" url="/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB120BY90&versionId={!image.RelatedRecordId}" alt="Thumbnail Not Yet Available"/>
                                            </apex:outputLink>
                                        </div>
                                    </apex:repeat>
                                    <br/>
                                </div>
                                <div class="row" style="text-align:center;color:#999;">
                                    {!imagesRow.rowNumber}/{!imagesRowsSize}
                                </div>  
                            </div>                        
                        </apex:repeat>
                      </div>
                      <a data-slide="prev" href="#media" class="left carousel-control"><i class="fa fa-angle-left"></i></a>
                      <a data-slide="next" href="#media" class="right carousel-control"><i class="fa fa-angle-right"></i></a>
                    </div>
                  </div>
                  <div class="col-md-5 col-xs-12" style="width:41.6667%;">
                    <h5>Information<apex:outputPanel layout="none" rendered="{!CanEditBoutique}"><a href="/{!RTCOMBoutique__c.Id}?retURL=/apex/ERTAILWarehouseDetails?Id={!RTCOMBoutique__c.Id}" class="pull-right"><span class="glyphicon glyphicon-pencil"></span></a></apex:outputPanel></h5>
                    <div class="form-group">
                      <label class="control-label">Market /</label>
                      <p class="form-control-static"><apex:outputField value="{!RTCOMBoutique__c.RTCOMMarket__c}"/></p>
                    </div>
                    <apex:outputPanel layout="none"  rendered="{!IsWarehouse}">
                        <div class="form-group">
                          <label class="control-label">{!$ObjectType.RTCOMBoutique__c.fields.Delivery_Address_Formula__c.label} /</label>
                          <p class="form-control-static"><apex:outputText value="{!RTCOMBoutique__c.Delivery_Address_Formula__c}" escape="false"/></p>
                        </div>
                    </apex:outputPanel>
                    <apex:outputPanel layout="none"  rendered="{!NOT(IsWarehouse)}">
                    <div class="form-group">
                      <label class="control-label">RECO Boutique /</label>
                      <p class="form-control-static"><apex:outputField value="{!RTCOMBoutique__c.RECOBoutique__c}"/></p>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Typology /</label>
                      <p class="form-control-static"><apex:outputField value="{!RTCOMBoutique__c.Typology__c}"/></p>
                    </div>
                    <div class="form-group">
                      <label class="control-label">{!$ObjectType.RTCOMBoutique__c.fields.Country_English__c.label} /</label>
                      <p class="form-control-static">{!RTCOMBoutique__c.Country_English__c}</p>
                    </div>
                    <div class="form-group">
                      <label class="control-label">{!$ObjectType.RTCOMBoutique__c.fields.City_English__c.label} /</label>
                      <p class="form-control-static">{!RTCOMBoutique__c.City_English__c}</p>
                    </div>
                    <div class="form-group">
                      <label class="control-label">{!$ObjectType.RTCOMBoutique__c.fields.Address__c.label} /</label>
                      <p class="form-control-static"><apex:outputText value="{!RTCOMBoutique__c.Address__c}" escape="false"/></p>
                    </div>
                    </apex:outputPanel>
                  </div>
                </div>
              </div>
            </div>
            <apex:outputPanel layout="block" styleClass="panel panel-default" rendered="{!NOT(ISNULL(RTCOMBoutique__c.RECO_Opening_Project__c))}">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" href="#collapseTwo">RECO Documents: Floor plans, elevations, ...<i class="indicator glyphicon glyphicon-minus  pull-right"></i></a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="panel-body">
                  <div class="col-xs-12 col-md-7" style="width:58.3333%;">
                  <font color="red">Select file for Preview</font>
                    <a id="docImageLink" href="/{!RTCOMBoutique__c.RECO_Opening_Project__c}" target="_blank">
                        <div class="main-plan" id="docImage">
                          <apex:image styleClass="img-cover" alt="" url="{!'/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId=' + RTCOMBoutique__c.RECO_Opening_Project__c}" />
                        </div>
                     </a>
                  </div>
                  <div class="col-xs-12 col-md-5" style="width:41.6667%;">
                    <div class="carousel slide media-carousel" id="media2">
                        <div class="carousel-inner">
                            <apex:repeat value="{!documentsRows}" var="documentsRow">
                                <div class="item{!IF(documentsRow.rowNumber == 1,' active','')}">
                                    <div class="row">
                                        <apex:repeat value="{!documentsRow.feedItemList}" var="document">
                                                <apex:outputLink value="javascript:changeSelectedDocumentImage('{!document.RelatedRecordId}');" styleClass="media-item media-left">
                                                    <img alt="" src="/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB120BY90&versionId={!document.RelatedRecordId}"/>
                                                </apex:outputLink>
                                                <div class="media-body">
                                                    <h4 class="media-heading">{!document.ContentFileName}</h4>
                                                    <p>{!document.ContentDescription}</p>
                                                </div>
                                                <br/>
                                        </apex:repeat>
                                        <div style="text-align:center;color:#999;">
                                        {!documentsRow.rowNumber}/{!documentsRowsSize}
                                        </div>
                                    </div>
                                </div>
                            </apex:repeat>
                        </div>
                        <a data-slide="prev" href="#media2" class="left carousel-control"><i class="fa fa-angle-left"></i></a>
                        <a data-slide="next" href="#media2" class="right carousel-control"><i class="fa fa-angle-right"></i></a>
                    </div>
                    <apex:outputPanel layout="block" rendered="{!isHQPMorAdmin}">
                        <h5 style="border-bottom:none;">
                            <br/>
                            <a class="pull-right" href="/{!RECOProjectId}#head_01Bb0000007Dkea_ep_j_id0_j_id29" target="_blank" title="RECO folders"><span class="glyphicon glyphicon-folder-open"></span></a>
                        </h5>
                    </apex:outputPanel>
                  </div>
                </div>
              </div>
            </apex:outputPanel>
            <apex:outputPanel styleClass="panel panel-default" rendered="{!NOT(IsWarehouse)}" layout="block">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" href="#collapseThree">RECO information<i class="indicator glyphicon glyphicon-minus pull-right"></i></a>
                </h4>
              </div>
              <div id="collapseThree" class="panel-collapse collapse in">
                <div class="panel-body">
                  <div class="col-xs-4">
                    <div class="form-group">
                      <label class="control-label">{!$ObjectType.Boutique__c.fields.Typology__c.label}</label>
                      <p class="form-control-static">{!RTCOMBoutique__c.RECOTypology__c }</p>
                    </div>
                    <div class="form-group">
                      <label class="control-label">{!$ObjectType.Boutique__c.fields.Expected_Closure_Date__c.label}</label>
                      <p class="form-control-static"><apex:outputText value="{0,date,dd MMM yyyy}"><apex:param value="{!RTCOMBoutique__c.RECOExpected_Closure_Date__c }" /></apex:outputText></p>
                    </div>
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                      <label class="control-label">{!$ObjectType.Boutique__c.fields.Concept__c.label}</label>
                      <p class="form-control-static">{!RTCOMBoutique__c.RECOConcept__c }</p>
                    </div>
                    <div class="form-group">
                      <label class="control-label">{!$ObjectType.Boutique__c.fields.Close_Date__c.label}</label>
                      <p class="form-control-static"><apex:outputText value="{0,date,dd MMM yyyy}"><apex:param value="{!RTCOMBoutique__c.RECOClose_Date__c }" /></apex:outputText></p>
                    </div>
                  </div>
                  <div class="col-xs-4">
                    <div class="form-group">
                      <label class="control-label">{!$ObjectType.RTCOMBoutique__c.fields.Opening_Date__c.label}</label>
                      <p class="form-control-static"><apex:outputText value="{0,date,dd MMM yyyy}"><apex:param value="{!RTCOMBoutique__c.Opening_Date__c }" /></apex:outputText></p>
                    </div>
                    <div class="form-group">
                      <label class="control-label">&nbsp;</label>
                      <p class="form-control-static">&nbsp;</p>
                    </div>
                  </div>
                </div>
              </div>
            </apex:outputPanel>
            <apex:outputPanel styleClass="panel panel-default" rendered="{!NOT(IsWarehouse)}" layout="block">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" href="#collapseFour">Show Windows<i class="indicator glyphicon glyphicon-minus pull-right"></i></a>
                </h4>
              </div>
              <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body">
                  <div class="col-xs-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <!-- <th>Name</th> -->
                          <th>{!$ObjectType.Boutique_Space__c.fields.Space__c.Label}</th>
                          <th>{!$ObjectType.Boutique_Space__c.fields.Hanging_System_Available__c.Label}</th>
                          <th>{!$ObjectType.Boutique_Space__c.fields.Closed_facade__c.Label}</th>
                          <th>{!$ObjectType.Boutique_Space__c.fields.Plug_Available__c.Label}</th>
                          <th>{!$ObjectType.Boutique_Space__c.fields.Width__c.Label}</th>
                          <th>{!$ObjectType.Boutique_Space__c.fields.Depth__c.Label}</th>
                          <th>{!$ObjectType.Boutique_Space__c.fields.Height__c.Label}</th>
                          <th>{!$ObjectType.Boutique_Space__c.fields.Creativity_Order__c.Label}</th>
                          <th></th>
                          <th>{!$ObjectType.Boutique_Space__c.fields.Decoration_Allowed__c.Label}</th>
                          <apex:outputPanel layout="none" rendered="{!CanEditBoutique}"><th>Action</th></apex:outputPanel>
                        </tr>
                      </thead>
                      <tbody>
                        <apex:repeat value="{!ShowWindows}" var="sw">
                        <tr>
                          <!-- <td style="white-space:nowrap;"><apex:outputText value="{!sw.Name}" /></td> -->
                          <td><apex:outputField value="{!sw.Space__c}" /></td>
                          <td style="text-align:center;"><i class="fa  {!IF(sw.Hanging_System_Available__c, 'fa-check-square-o', '')}"></i></td>
                          <td style="text-align:center;"><i class="fa  {!IF(sw.Closed_facade__c, 'fa-check-square-o', '')}"></i></td>
                          <td style="text-align:center;"><i class="fa  {!IF(sw.Plug_Available__c, 'fa-check-square-o', '')}"></i></td>
                          <td style="text-align:right;"><apex:outputText value="{!sw.Width__c}" /></td>
                          <td style="text-align:right;"><apex:outputText value="{!sw.Depth__c}" /></td>
                          <td style="text-align:right;"><apex:outputText value="{!sw.Height__c}" /></td>
                          <td style="text-align:right;"><apex:outputText value="{!sw.Creativity_Order__c}" /></td>
                          <td><apex:outputLink value="javascript:void(0);" title="{!sw.Comment__c}" rendered="{!NOT(ISNULL(sw.Comment__c))}"><span class="fa fa-comment"></span></apex:outputLink></td>
                          <td style="text-align:center;"><i class="fa  {!CASE(sw.Decoration_Allowed__c, 'Yes', 'fa-check-square-o', '')}"></i></td>
                          <apex:outputPanel layout="none" rendered="{!CanEditBoutique}"><th scope="row"  style="text-align:center;"><a href="/{!sw.Id}/e?retURL=/apex/ERTAILWarehouseDetails?Id={!RTCOMBoutique__c.Id}"><span class="glyphicon glyphicon-pencil"></span></a></th></apex:outputPanel>
                        </tr>
                        </apex:repeat>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </apex:outputPanel>
            
            <apex:outputPanel styleClass="panel panel-default" rendered="{!NOT(IsWarehouse)}" layout="block">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" href="#collapseFive">In Stores<i class="indicator glyphicon glyphicon-minus pull-right"></i></a>
                </h4>
              </div>
              <div id="collapseFive" class="panel-collapse collapse in">
                <div class="panel-body">
                  <div class="col-xs-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <!-- <th>Name</th> -->
                          <th>Space</th>
                          <th>Quantity</th>
                          <th>Decoration Allowed</th>
                          <!-- <th>Height</th> -->
                          <!-- <th>Width</th> -->
                          <!-- <th>Depth</th> -->
                          <th></th>
                          <apex:outputPanel layout="none" rendered="{!CanEditBoutique}"><th>Action</th></apex:outputPanel>
                        </tr>
                      </thead>
                      <tbody>
                        <apex:repeat value="{!InStores}" var="is">
                        <tr>
                          <!-- <td style="white-space:nowrap;"><apex:outputText value="{!is.Name}" /></td> -->
                          <td><apex:outputField value="{!is.Space__c}" /></td>
                          <td style="text-align:right;"><apex:outputText value="{!is.Space_Quantity__c}" /></td>
                          <td style="text-align:center;"><i class="fa  {!CASE(is.Decoration_Allowed__c, 'Yes', 'fa-check-square-o', '')}"></i></td>
                          <!-- <td style="text-align:right;"><apex:outputText value="{!is.Height__c}" /></td> -->
                          <!-- <td style="text-align:right;"><apex:outputText value="{!is.Width__c}" /></td> -->
                          <!-- <td style="text-align:right;"><apex:outputText value="{!is.Depth__c}" /></td> -->
                          <td><apex:outputLink value="javascript:void(0);" title="{!is.Comment__c}" rendered="{!NOT(ISNULL(is.Comment__c))}"><span class="fa fa-comment"></span></apex:outputLink></td>
                          <apex:outputPanel layout="none" rendered="{!CanEditBoutique}"><th scope="row" style="text-align:center;"><a href="/{!is.Id}/e?retURL=/apex/ERTAILWarehouseDetails?Id={!RTCOMBoutique__c.Id}"><span class="glyphicon glyphicon-pencil"></span></a></th></apex:outputPanel>
                        </tr>
                        </apex:repeat>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </apex:outputPanel>
            
          </div>
        </div>
      </div>
    </div> <!-- /container -->
    <apex:inputHidden value="{!RTCOMBoutique__c.Default_Image__c}" rendered="{!1 == 2}" />
    <apex:inputHidden value="{!RTCOMBoutique__c.Default_Image_2__c}" rendered="{!1 == 2}" />
    <apex:inputHidden value="{!RTCOMBoutique__c.Default_Image_3__c}" rendered="{!1 == 2}" />
    <apex:inputHidden value="{!RTCOMBoutique__c.RECO_Opening_Project__c}" rendered="{!1 == 2}" /> 
    </apex:form>
    <script>
    
    function changeBoutiqueImage(imageId){
        $('#btqImage').html('<img border="0" alt="Thumbnail Not Yet Available" src="/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId=' + imageId + '">');
        $('#btqImageLink').attr("href", "/" + imageId);
        $('.col-md-3').removeClass('active');
        $('#' + imageId).addClass('active');
    }
    
    function changeSelectedDocumentImage(docId){
        $('#docImage').html('<img border="0" alt="" src="/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId=' + docId + '">');
        $('#docImageLink').attr("href", "/" + docId);
        $('.col-md-5 .media').removeClass('active');
        $('#' + docId).addClass('active');
    }
    
    $(function() {
      $('#accordion').on('hidden.bs.collapse', toggleChevron);
      $('#accordion').on('shown.bs.collapse', toggleChevron);
      $('#media, #media2').carousel({
        pause: true,
        interval: false,
      });
      
        columnSameHeight();
          imageCover();
    });
      $( window ).resize(function() {
          columnSameHeight();
          imageCover();
        });
    </script>
</apex:page>
<!--
*   @Class      :   ManageStakeholders.page
*   @Description:   Page to create, update and delete Stakeholder records
*   @Author     :   Jacky Uy
*   @Created    :   14 MAY 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*	Jacky Uy		23 JUL 2014		Added More Support Stakeholders and Grouped Stakeholders by Function
*                         
-->

<apex:page controller="ManageStakeholdersController">
<apex:sectionHeader title="Manage Stakeholders" subtitle="{!parentName}"/>
<apex:form >
<div class="message infoM3" style="margin-bottom:15px;padding:2px;">
	<table class="messageTable" style="margin: 0px; padding: 0px;" border="0" cellspacing="0" cellpadding="0">
	<tbody>
		<tr valign="top">
			<td>
			    <img title="INFO" class="msgIcon" alt="INFO" src="/s.gif"/>
			</td>
			<td class="messageCell">
				<div class="messageText">
					Note that the following recipients receives an email notification when the project is initiated and/or closed:<br/>
					<apex:outputText style="font-weight:bold;" value="{!notifiedUsers}"/>
				</div>
		    </td>
		</tr>
	</tbody>
	</table>
</div>
<apex:pageBlock id="pgBlk" mode="maindetail">
    <apex:messages style="color:#ff0000;"/>
    
    <!-- MAIN STAKEHOLDERS -->
    <apex:pageBlockTable value="{!mainStakeholders}" var="item">
        <apex:column style="width:30%;" headerValue="Main Stakeholder">
            <apex:outputText value="{!mapStakeholders[item].stakeholder.Function__c}"/>
        </apex:column>
        <apex:column headerValue="Stakeholder">
            <apex:inputField style="width:250px;" value="{!mapStakeholders[item].stakeholder.Platform_user__c}"/>
        </apex:column>
        <apex:column >
        	<apex:facet name="header">
        		{!$ObjectType.Stakeholder__c.fields.Grant_Access__c.Label}&nbsp;
        		<span><img src="/s.gif" alt="Help" class="helpIcon" title="{!$ObjectType.Stakeholder__c.fields.Grant_Access__c.InlineHelpText}"/></span>
        	</apex:facet>
            <apex:inputCheckbox disabled="{!IF(OR(CONTAINS(mapStakeholders[item].stakeholder.Function__c,'Architect'),CONTAINS(mapStakeholders[item].stakeholder.Function__c,'Furniture')),'false','true')}" style="width:100px;" value="{!mapStakeholders[item].stakeholder.Grant_Access__c}" onclick="checkNotify(this);"/>
        </apex:column>
        <apex:column >
        	<apex:facet name="header">
        		{!$ObjectType.Stakeholder__c.fields.Notify__c.Label}&nbsp;
        		<span><img src="/s.gif" alt="Help" class="helpIcon" title="{!$ObjectType.Stakeholder__c.fields.Notify__c.InlineHelpText}"/></span>
        	</apex:facet>
            <apex:inputCheckbox disabled="true" style="width:100px;" value="{!mapStakeholders[item].stakeholder.Notify__c}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Stakeholder__c.fields.Notified_Date__c.Label}">
            <apex:outputField style="width:250px;" value="{!mapStakeholders[item].stakeholder.Notified_Date__c}"/>
        </apex:column>
        <!--
        <apex:column headerValue="Generate Tasks">
            <apex:inputField value="{!mapStakeholders[item].stakeholder.Generate_tasks__c}"/>
        </apex:column>
        -->
    </apex:pageBlockTable><br/>
    
    <!-- SUPPORT STAKEHOLDERS -->
    <apex:pageBlockTable value="{!supportStakeholders}" var="item">
        <apex:column style="width:30%;" headerValue="Support Stakeholder">
            <apex:outputText value="{!mapStakeholders[item].stakeholder.Function__c}"/>
        </apex:column>
        <apex:column headerValue="Stakeholder">
            <apex:inputField style="width:250px;" value="{!mapStakeholders[item].stakeholder.Platform_user__c}"/>
        </apex:column>
        <apex:column >
        	<apex:facet name="header">
        		{!$ObjectType.Stakeholder__c.fields.Grant_Access__c.Label}&nbsp;
        		<span><img src="/s.gif" alt="Help" class="helpIcon" title="{!$ObjectType.Stakeholder__c.fields.Grant_Access__c.InlineHelpText}"/></span>
        	</apex:facet>
            <apex:inputCheckbox style="width:100px;" value="{!mapStakeholders[item].stakeholder.Grant_Access__c}" onclick="checkNotify(this);"/>
        </apex:column>
        <apex:column >
        	<apex:facet name="header">
        		{!$ObjectType.Stakeholder__c.fields.Notify__c.Label}&nbsp;
        		<span><img src="/s.gif" alt="Help" class="helpIcon" title="{!$ObjectType.Stakeholder__c.fields.Notify__c.InlineHelpText}"/></span>
        	</apex:facet>
            <apex:inputCheckbox disabled="true" style="width:100px;" value="{!mapStakeholders[item].stakeholder.Notify__c}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Stakeholder__c.fields.Notified_Date__c.Label}">
            <apex:outputField style="width:250px;" value="{!mapStakeholders[item].stakeholder.Notified_Date__c}"/>
        </apex:column>
        <!--
        <apex:column headerValue="Generate Tasks">
            <apex:inputField value="{!mapStakeholders[item].stakeholder.Generate_tasks__c}"/>
        </apex:column>
        -->
    </apex:pageBlockTable><br/>
    
    <!-- SUPPLIERS -->
    <apex:pageBlockTable value="{!supplierStakeholders}" var="item">
        <apex:column style="width:30%;" headerValue="Supplier">
            <apex:outputText value="{!mapStakeholders[item].stakeholder.Function__c}"/>
        </apex:column>
        <apex:column headerValue="Stakeholder">
            <apex:inputField style="width:250px;" value="{!mapStakeholders[item].stakeholder.Contact__c}"/>
        </apex:column>
        <apex:column >
        	<apex:facet name="header">
        		{!$ObjectType.Stakeholder__c.fields.Grant_Access__c.Label}&nbsp;
        		<span><img src="/s.gif" alt="Help" class="helpIcon" title="{!$ObjectType.Stakeholder__c.fields.Grant_Access__c.InlineHelpText}"/></span>
        	</apex:facet>
        	<apex:inputCheckbox disabled="true" style="width:100px;" value="{!mapStakeholders[item].stakeholder.Grant_Access__c}"/>
        </apex:column>
        <apex:column >
        	<apex:facet name="header">
        		{!$ObjectType.Stakeholder__c.fields.Notify__c.Label}&nbsp;
        		<span><img src="/s.gif" alt="Help" class="helpIcon" title="{!$ObjectType.Stakeholder__c.fields.Notify__c.InlineHelpText}"/></span>
        	</apex:facet>
            <apex:inputCheckbox disabled="{!IF(CONTAINS(mapStakeholders[item].stakeholder.Function__c,'Furniture'),'false','true')}" style="width:100px;" value="{!mapStakeholders[item].stakeholder.Notify__c}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Stakeholder__c.fields.Notified_Date__c.Label}">
            <apex:outputField style="width:250px;" value="{!mapStakeholders[item].stakeholder.Notified_Date__c}"/>
        </apex:column>
        <!--
        <apex:column headerValue="Generate Tasks">
            <apex:inputField value="{!mapStakeholders[item].stakeholder.Generate_tasks__c}"/>
        </apex:column>
        -->
    </apex:pageBlockTable><br/>
    
    <apex:outputPanel >
        <apex:commandButton action="{!save}" reRender="pgBlk" status="processStatus" value="Save"/>&nbsp;
        <apex:commandButton action="{!saveClose}" value="Save & Close"/>&nbsp;
        <apex:commandButton action="{!close}" immediate="true" value="Close"/>&nbsp;
        <apex:actionStatus id="processStatus">
            <apex:facet name="start"><apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/></apex:facet>
        </apex:actionStatus>
    </apex:outputPanel><br/>&nbsp;
</apex:pageBlock>
</apex:form>
<script>
	function checkNotify(obj){
		var objName = obj.name;
	    var strSplit = objName.split(':');
	    //alert('objName: ' + objName);
	    
	    strSplit.splice(-1,1);
	    var strPrefix = strSplit.join(':');
	    //alert('strPrefix: ' + strPrefix);
	    
		var inputs = document.getElementsByTagName('input');
		for(var i=0; i<inputs.length; i++) {
		    if(inputs[i].type == 'checkbox' && inputs[i].name.search(strPrefix)>-1){
		    	//alert(1);
		        inputs[i].checked = obj.checked;
		    }
		}
	}
</script>
</apex:page>
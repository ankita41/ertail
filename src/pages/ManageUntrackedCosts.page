<!--
*   @Class      :   ManageUntrackedCosts.page
*   @Description:   Page to create, update and delete Untracked Cost records
*   @Author     :   Jacky Uy
*   @Created    :   31 JUL 2014
*
*   Modification Log:  
*   ----------------------------------------------------------------------------------------------------------------------------
*   Developer       Date            Description
*   ----------------------------------------------------------------------------------------------------------------------------
*	JACKY UY		06 AUG 2014		Change controller.
*	JACKY UY		17 AUG 2014		Change Market lookup inputField to a selectList due to bug when rerendering the page.
*	JACKY UY		08 SEP 2014		Changed controller.
*	JACKY UY		08 SEP 2014		Added Filter by Year.
*                         
-->

<apex:page controller="ManageUntrackedCostsController">
<style>
    .bPageBlock .requiredInput .requiredBlock {top:5px;bottom:5px;}
</style>
<apex:sectionHeader title="Manage Records" subtitle="Untracked Costs"/>
<apex:form >
<apex:pageBlock id="pgBlk" mode="maindetail">
    <b>Filter by Year:</b>
    <apex:selectList size="1" value="{!filterYear}" required="true" style="margin:5px;">
        <apex:selectOptions value="{!yearOpts}"/>
        <apex:actionSupport event="onchange" action="{!queryObjs}" rerender="pgBlk" status="filterStatus"/>
    </apex:selectList>
    <apex:actionStatus id="filterStatus">
        <apex:facet name="start"><apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/></apex:facet>
    </apex:actionStatus>
    
    <br/><apex:messages style="color:#ff0000;"/><br/>
    
    <apex:variable var="counter" value="{!0}"/>
    <apex:pageBlockTable value="{!objList}" var="item">
        <apex:column style="text-align:center;">
            <apex:commandLink action="{!delRow}" immediate="true" status="processStatus">
                <apex:image url="/img/func_icons/remove12_on.gif" title="Remove Row"/>
                <apex:param name="index" value="{!counter}"/>
            </apex:commandLink>
        	<apex:variable var="counter" value="{!counter+1}"/>
        </apex:column>
        
        <!-- FIELDS -->
        <apex:column headerValue="{!$ObjectType.Untracked_Cost__c.fields.Year__c.Label}">
            <apex:inputField style="width:100px;" value="{!item.obj['Year__c']}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Untracked_Cost__c.fields.Market__c.Label}">
            <apex:selectList size="1" value="{!item.obj['Market__c']}" required="true">
                <apex:selectOption itemValue="" itemLabel="- Select Market -"/>
                <apex:selectOptions value="{!marketOpts}"/>
            </apex:selectList>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Untracked_Cost__c.fields.Description__c.Label}">
            <apex:inputField style="width:200px;" value="{!item.obj['Description__c']}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Untracked_Cost__c.fields.Planned_OP__c.Label}">
            <apex:inputField style="text-align:right;width:100px;" value="{!item.obj['Planned_OP__c']}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Untracked_Cost__c.fields.Latest_DF__c.Label}">
            <apex:inputField style="text-align:right;width:100px;" value="{!item.obj['Latest_DF__c']}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Untracked_Cost__c.fields.Latest_DF_Last_Modified_Date__c.Label}">
            <apex:inputField value="{!item.obj['Latest_DF_Last_Modified_Date__c']}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Untracked_Cost__c.fields.Spent__c.Label}">
            <apex:inputField style="text-align:right;width:100px;" value="{!item.obj['Spent__c']}"/>
        </apex:column>
    </apex:pageBlockTable><br/>
    
    <!-- BUTTONS -->
    <apex:outputPanel >
        <apex:commandButton action="{!addRow}" immediate="true" reRender="pgBlk" status="processStatus" value="Add Row"/>&nbsp;
        <apex:commandButton action="{!save}" reRender="pgBlk" status="processStatus" value="Save"/>&nbsp;
        <apex:commandButton action="{!saveClose}" value="Save & Close"/>&nbsp;
        <apex:commandButton action="{!close}" immediate="true" value="Close"/>&nbsp;
        <apex:actionStatus id="processStatus">
            <apex:facet name="start"><apex:image style="margin:0 0 -5px 5px;height:20px;width:20px;" value="/img/loading32.gif"/></apex:facet>
        </apex:actionStatus>
    </apex:outputPanel>
</apex:pageBlock>
</apex:form>
</apex:page>